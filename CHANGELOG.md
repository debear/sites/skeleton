## Build 1439 - e6788037 - 2025-03-06 - Security: Laravel CVE
* e6788037: Update Laravel framework to resolve CVE-2025-27515 (_Thierry Draper_)

## Build 1438 - 036658d2 - 2025-03-06 - Hotfix: CI Test Content Configuration
* 036658d2: Preserve, and then reest, core content config between PHPUnit feature test calls (_Thierry Draper_)

## Build 1437 - 8c9bf63d - 2025-02-28 - Feature: Form Redirecting
* 8c9bf63d: Allow for a customisable form redirect, based on logic applied within the post-processing (_Thierry Draper_)

## Build 1436 - 9ca652f7 - 2025-02-26 - Hotfix: Dropdown JS Selecting
* 9ca652f7: Fix client-side dropdown selection handling when first applied (_Thierry Draper_)

## Build 1435 - 7034296a - 2025-02-25 - Improvement: Subsite Endpoints
* 7034296a: Apply the new section endpoint/config code split (_Thierry Draper_)
* c39ad0d2: Allow for a separation between subsite config key and public endpoint (_Thierry Draper_)

## Build 1433 - be7e50f5 - 2025-02-24 - SysAdmin: Database Sync Pruning
* be7e50f5: Add a new column to handle pruning local database copies before syncing from remote (_Thierry Draper_)

## Build 1432 - 57673b99 - 2025-02-22 - SoftDep: February 2025
* 57673b99: Fix a stray usage of PHPs deprecated ${var} (_Thierry Draper_)
* 87d7d80e: Update PHP.ini locations to the new docker image location (_Thierry Draper_)
* 8c669218: Resolve browser complaint for markup being generated inside our subnav mobile options (_Thierry Draper_)
* 79183515: Typecast session getters to satisfy Phan since latest updates (_Thierry Draper_)
* ef2aca72: Migrate config getting to the framework Config class (_Thierry Draper_)
* aecbb42b: Auto: Composer Update for February 2025 (_Thierry Draper_)

## Build 1427 - d6d12ad1 - 2025-01-25 - Improvement: Subsite Config
* d6d12ad1: Decouple the subsite config key from its URL endpoint (_Thierry Draper_)

## Build 1426 - a0c3a6bb - 2025-01-17 - SoftDep: January 2025
* (Auto-commits only)

## Build 1426 - d250c747 - 2025-01-13 - Improvement: Test Logging
* d250c747: Better log slow database queries if the stack trace is not available (_Thierry Draper_)
* f9d0d725: Expand the bespoke logging to apply to any non-live environment, not just CI (_Thierry Draper_)

## Build 1424 - c35bf31a - 2024-12-31 - Improvement: API Processing Object Re-use
* c35bf31a: Allow passing of custom data to the API processing methods to allow for re-use of existing objects (_Thierry Draper_)

## Build 1423 - e5f4d2af - 2024-12-29 - Improvement: Slow Query Logging
* e5f4d2af: Capture slow query instances for future debugging (_Thierry Draper_)

## Build 1422 - 4b856c03 - 2024-12-15 - SoftDep: December 2024
* (Auto-commits only)

## Build 1422 - 36e5f827 - 2024-11-16 - SoftDep: November 2024
* (Auto-commits only)

## Build 1422 - 9763e940 - 2024-10-21 - SoftDep: October 2024
* (Auto-commits only)

## Build 1422 - ca3ebf80 - 2024-09-15 - SoftDep: September 2024
* (Auto-commits only)

## Build 1422 - c5677aa0 - 2024-09-15 - SysAdmin: CI Tweaks
* c5677aa0: Fix first run setup for downloading seed data (_Thierry Draper_)
* 6e42cf97: Apply some minor CI script tidying and improvements (_Thierry Draper_)

## Build 1420 - 4cf054ef - 2024-09-01 - Hotfix: Javascript Reports
* 4cf054ef: Fix the null optional chaining within the bug handler processing (_Thierry Draper_)
* 9eaf6a2e: Add the crossorigin attribute to our scripts for improved CORS handling (_Thierry Draper_)

## Build 1418 - b189128a - 2024-08-30 - SysAdmin: Twitter Domain
* b189128a: Migrate Twitter links to the new domain (_Thierry Draper_)

## Build 1417 - 65e440e8 - 2024-08-19 - SoftDep: August 2024
* (Auto-commits only)

## Build 1417 - 1517b061 - 2024-08-13 - Hotfix: News Link Schema
* 1517b061: Widen the possible width of the news article link columns (_Thierry Draper_)

## Build 1416 - 722fc6d5 - 2024-08-09 - SysAdmin: MariaDB to MySQL Migration
* 722fc6d5: Handle the boolean data type difference in JSON_CONTAINS in MySQL (_Thierry Draper_)
* 51a42e7c: Handle the NO_ZERO_DATE sql_mode in MySQL (_Thierry Draper_)
* 9c04c3d4: Migrate our CI from MariaDB to MySQL (_Thierry Draper_)

## Build 1413 - d1352246 - 2024-07-17 - Hotfix: SAST Reporting
* d1352246: Fix the SAST report download sort ordering (_Thierry Draper_)

## Build 1412 - e3a2b444 - 2024-07-17 - Feature: CI Server Sync Job
* e3a2b444: Add the schema for the server sync database (_Thierry Draper_)

## Build 1411 - db730855 - 2024-07-16 - Hotfix: Phan generics exclusions
* db730855: Allow bespoke Phan exclusions following increase used of generics within Laravel (_Thierry Draper_)

## Build 1410 - 1f69d867 - 2024-07-14 - SoftDep: July 2024
* (Auto-commits only)

## Build 1410 - d18f7b59 - 2024-06-23 - SysAdmin: Composer Deployment
* d18f7b59: Switch the composer install/autoload wrapper into autoloader only (_Thierry Draper_)

## Build 1409 - 3ac98391 - 2024-06-21 - Hotfix: Local CI Job Parsing
* 3ac98391: Fix the local CI job parser to exclude comments after the allow_failure flag (_Thierry Draper_)

## Build 1408 - ccf3f8b5 - 2024-06-18 - SoftDep: June 2024
* ccf3f8b5: Auto: Composer Update for June 2024 (_Thierry Draper_)
* 46b3b4e5: Move the HiBP composer package version pattern back to a release-based pattern (_Thierry Draper_)

## Build 1407 - d7594b88 - 2024-06-17 - Hotfix: CI Downloads
* d7594b88: Fix the request sent to determine what CI data files need downloading (_Thierry Draper_)
* 860a8d53: Improve the location of the CI download trust store cert (_Thierry Draper_)

## Build 1405 - af4255ff - 2024-06-01 - Improvement: Logging Timezones
* af4255ff: Standardise the Laravel log file to the app timezone (_Thierry Draper_)
* c0403c94: Propagate the app timezone logging logic to the log file filename (_Thierry Draper_)

## Build 1403 - ab0a2cde - 2024-05-29 - Improvement: App Logging
* ab0a2cde: Log all the secondary request info in a single line for simpler grokking (_Thierry Draper_)
* 07ea3a71: Move loggers from singletons to statics to improve their request scope (_Thierry Draper_)
* c944b4cb: Fix resolution of the incomplete, base api, sub-part endpoint (_Thierry Draper_)
* 22587896: Improve the request logging, in particular in determining the correct response code (_Thierry Draper_)
* 6e3420f5: Move the application logs to a more appropriate, and better configurable, directory (_Thierry Draper_)

## Build 1398 - 0b6505e6 - 2024-05-18 - SoftDep: May 2024
* (Auto-commits only)

## Build 1398 - a6f7ec82 - 2024-05-13 - Hotfix: CI Security Job Rules
* a6f7ec82: Re-work our CI job rules to ensure we do not run on tag pushes and SAST jobs do not on master (_Thierry Draper_)

## Build 1397 - 195fa06a - 2024-05-13 - Hotfix: CI Security Job Order
* 195fa06a: Re-order the security CI jobs to resolve the earlier dependency complexities (_Thierry Draper_)

## Build 1396 - d7c7fe19 - 2024-05-13 - Security: CI Jobs
* d7c7fe19: Modifications following output of the initial SAST artifacts (_Thierry Draper_)
* 88d5b5a5: Switch our localci SAST exclusion to the new emulator (_Thierry Draper_)
* b26ba67f: Implement a local SAST emulator script using the GitLab docker images (_Thierry Draper_)
* 20865f91: Implement an initial SAST reporting tool (_Thierry Draper_)
* 66af5f6b: Migrate our only/except job logic to the preferred rules format (_Thierry Draper_)
* 39ffaa2c: Incorporate the GitLab managed SAST pipeline job (_Thierry Draper_)
* ca4f5bec: Add some native tool dependency vulnerability scanning within our CI (_Thierry Draper_)

## Build 1389 - 16e91df2 - 2024-04-27 - Improvement: CI Downloads
* 16e91df2: Switch CI downloads from wget to curl (_Thierry Draper_)

## Build 1388 - df79040d - 2024-04-24 - Improvement: JavaScript Formatting
* df79040d: Apply some minor eslint hint tweaks to the JavaScript (_Thierry Draper_)
* b1a59210: Disable the eslint unused disabled rule checked whilst linting, as it conflicts with the standards check (_Thierry Draper_)
* fb24ff29: Migrate to eslints flat config format (_Thierry Draper_)

## Build 1385 - f375516a - 2024-04-21 - SoftDep: April 2024
* f375516a: Convert a now-invalid Carbon instantiation in v3 (_Thierry Draper_)
* d6e22b00: Auto: Composer Update for April 2024 (_Thierry Draper_)

## Build 1384 - 4d501683 - 2024-04-20 - Improvement: AJAX Helpers
* 4d501683: Move the AJAX response helpers from the handler to being bound into the response object (_Thierry Draper_)

## Build 1383 - b33099cf - 2024-04-18 - Improvement: Client-side Processing
* b33099cf: Introduce a transient in-page part of the local storage, and update the targeted version of ECMAScript in CI to v13 / 2022 (_Thierry Draper_)
* a7639b51: Include a generalised message formatter for AJAX responses based on HTTP response code (_Thierry Draper_)

## Build 1381 - 4c7e3a9e - 2024-04-17 - Improvement: Login Error Responses
* 4c7e3a9e: Render more suitable error messages to failed login attempts (_Thierry Draper_)

## Build 1380 - cc6186d1 - 2024-04-17 - Hotfix: Favicon loading
* cc6186d1: Fix how we convert the domain to the static site to account for the gTLD in live (_Thierry Draper_)

## Build 1379 - 001a957d - 2024-04-17 - SysAdmin: Favicon loading
* 001a957d: Move the favicon out of the docroot and into a static resource (_Thierry Draper_)

## Build 1378 - e6033474 - 2024-04-14 - Improvement: Email style tags
* e6033474: Correctly process nonce attributes within <style tags in the online email viewer (_Thierry Draper_)
* f4bc6028: Strip the <style tags from the text-only version of an email (_Thierry Draper_)

## Build 1376 - 89e6145a - 2024-04-03 - SysAdmin: Disable Universal Analytics urchins
* 89e6145a: Disable the expiring Univeral Analytics urchins having moved to GAv4 (_Thierry Draper_)

## Build 1375 - ada2dae9 - 2024-03-30 - Hotfix: Logging Timezone
* ada2dae9: Standardise timezone of logging responses to the server tz (_Thierry Draper_)

## Build 1374 - bc14dc1c - 2024-03-17 - SoftDep: Laravel 11 and March 2024 Update
* bc14dc1c: Switch the Laravel database driver to the new MariaDB-specific driver (_Thierry Draper_)
* fb700bcd: Upgrade dependencies to Laravel 11 (and also PHPUnit 11) (_Thierry Draper_)
* b773bbf0: Override the Eloquent getDates() method to re-include custom date fields that are now defined in the $casts property (_Thierry Draper_)
* cab1a3bc: Switch use of the now-removed $dates property in Eloquent to its $casts equivalent (_Thierry Draper_)
* 9206d556: Remove a previously manual AuthServiceProvider process that is automated as of Laravel 10 (_Thierry Draper_)
* 9291c288: Upgrade dependencies to Laravel 10 (and also PHPUnit 10) (_Thierry Draper_)

## Build 1368 - 5f70dc20 - 2024-03-16 - SysAdmin: Hosting Fixes
* 5f70dc20: Widen the news article link column to account for now non-silent errors (_Thierry Draper_)

## Build 1367 - 0f8d317c - 2024-03-09 - SysAdmin: New Hosting Paths
* 0f8d317c: Apply minor path tweaks to the new hosted environment (_Thierry Draper_)

## Build 1366 - 3c4bea28 - 2024-03-06 - Feature: AJAX Response Headers
* 3c4bea28: Pass AJAX response headers to the response handler (_Thierry Draper_)

## Build 1365 - 077f3e34 - 2024-02-17 - SoftDep: February 2024
* (Auto-commits only)

## Build 1365 - 527e63a4 - 2024-02-04 - Hotfix: Manifests
* 527e63a4: Tweak the default manifest file following browser errors around the related apps field (_Thierry Draper_)

## Build 1364 - df55db2a - 2024-02-04 - Feature: Local Storage Wrapper
* df55db2a: Implement a localStorage wrapper with cache expiration logic (_Thierry Draper_)

## Build 1363 - 82a27a45 - 2024-02-02 - Hotfix: Navigation Link Colours
* 82a27a45: Fix the colour of visited a tags within the (dark background) nav (_Thierry Draper_)

## Build 1362 - 9c490eae - 2024-01-31 - Hotfix: Mobile Login Toggle
* 9c490eae: Perform some minor code tidying (_Thierry Draper_)
* 1c538bf1: Fix toggling of the Login box on the mobile menu (_Thierry Draper_)

## Build 1360 - c7ff0060 - 2024-01-31 - Improvement: Secondary API Rendering
* c7ff0060: Only attempt to render an API version of a cached secondary object if the row has it set (_Thierry Draper_)

## Build 1359 - b5b656fb - 2024-01-27 - Improvement: Refactoring JavaScript
* b5b656fb: Allow undefined objects be passed to the event attaching method to simplify instantiation (_Thierry Draper_)
* 5c9171e9: Apply the latest round of micro-optimisations and small improvements (_Thierry Draper_)
* 8c7fce23: Remove the compat polyfills that do not appear to be required any more (_Thierry Draper_)
* 26bb11a5: Revert PHPCS "linting" enforced JavaScript standards for curly brace positioning (_Thierry Draper_)
* dd01f642: Replace the JavaScript linter with something less enforcing of (clunky) standards (_Thierry Draper_)
* 4ce6fd11: Move to greater use of the arrow functions (_Thierry Draper_)
* c33554f2: Apply some micro-optimisation and code simplification (_Thierry Draper_)
* 3c57e15e: Refine how and which JavaScript globals are set on every page request (_Thierry Draper_)
* db95ccc5: Make better use of template literals in string expressions (_Thierry Draper_)
* 6384aecc: Move away from the centralised DOM querySelector parent node argument (_Thierry Draper_)
* f7643430: Remove unused and duplicated (via other trivial means) JavaScript from the page (_Thierry Draper_)
* e7e1df38: Streamline the keypress event handling (_Thierry Draper_)
* c4bce400: Replace use of the deprecated popup opening shorthand that is only used once (_Thierry Draper_)
* 5b346d09: Replace use of the deprecated Input shorthands that can be achieved simply in other existing methods (_Thierry Draper_)
* 3eae1c4c: Replace use of the deprecated DOM.get* methods in favour of the generalised query selector (_Thierry Draper_)
* 0b969b34: Remove handling of server-side generated JavaScript events (_Thierry Draper_)
* 099bab8a: Move offline toggling JavaScript events from server-side (rendered in the HTML) to client-side (_Thierry Draper_)
* 06003a53: Remove handling of server-side JavaScript event links within the pagination framework (_Thierry Draper_)
* 289c379a: Move article expand/contract JavaScript events from server-side (rendered in the HTML) to client-side (_Thierry Draper_)
* bca575ee: Move email tag JavaScript events from server-side (rendered in the HTML) to client-side (_Thierry Draper_)
* 806799ed: Move nav account JavaScript events from server-side (rendered in the HTML) to client-side (_Thierry Draper_)
* 36d5da17: Move standalone page JavaScript events from server-side (rendered in the HTML) to client-side (_Thierry Draper_)

## Build 1337 - cb327668 - 2024-01-19 - Improvement: JavaScript Null Operators
* cb327668: Make use of the optional chaining JS operator to simplify the code (_Thierry Draper_)
* 072cf3b3: Make use of the nullish coalescing operator to simplify the code (_Thierry Draper_)
* 1b9f4aa2: Update eslints base ECMAScript version to allow for null-handling simplification (_Thierry Draper_)

## Build 1334 - 3107fb86 - 2024-01-14 - SoftDep: January 2024
* (Auto-commits only)

## Build 1334 - 2909cfe6 - 2023-12-17 - SoftDep: December 2023
* (Auto-commits only)

## Build 1334 - d740813b - 2023-11-27 - CI: Standardised CSS Standards
* d740813b: Apply our standard 120-char width for CSS standards rather than the default 80 (_Thierry Draper_)

## Build 1333 - 4f4fd838 - 2023-11-19 - SysAdmin: File-based session handling
* 4f4fd838: Allow for encrypted session files (but preserving the current default behaviour) (_Thierry Draper_)
* a1fb05cd: Store file-based sessions outside the repo path (_Thierry Draper_)

## Build 1331 - db982039 - 2023-11-18 - Composer: November 2023
* (Auto-commits only)

## Build 1331 - 91215da9 - 2023-11-18 - Feature: Session Handling configurability
* 91215da9: Store the browser info in an alternative location in Report URIs given the toggleable session aggregation (_Thierry Draper_)
* 0881e481: Allow the session stat aggregation to be toggled on or off (_Thierry Draper_)
* 96c45a36: Move away from the SessionHandler the secondary methods that do not relate to the core handling of sessions (_Thierry Draper_)

## Build 1328 - e75f07bc - 2023-11-15 - Improvement: Simplified Lockfile Columns
* e75f07bc: Remove the superfluous lockfile upload column (_Thierry Draper_)

## Build 1327 - 87f35f4f - 2023-11-15 - Improvement: Tweets with Media
* 87f35f4f: Allow for syncing and uploading of media with Tweets (_Thierry Draper_)

## Build 1326 - e34463a4 - 2023-11-14 - Hotfix: Visited Link Styling
* e34463a4: Standardise visited link styles with regular links (_Thierry Draper_)

## Build 1325 - 7b95292a - 2023-10-20 - Improvement: Phan
* 7b95292a: Include a missing model for integration lockfile stamps (_Thierry Draper_)
* 08e52071: Improve the way the Phan test loads, parses and excludes code from other repos (_Thierry Draper_)

## Build 1323 - ae771647 - 2023-10-14 - Hotfix: ORM Date Casting
* ae771647: Fix how we cast date only fields when app timezone is before the database timezone (_Thierry Draper_)

## Build 1322 - b4e463a6 - 2023-10-14 - Composer: October 2023 Outdated
* b4e463a6: Upgrade outdated composer library (_Thierry Draper_)

## Build 1321 - 10e6a3bd - 2023-10-14 - Composer: October 2023
* (Auto-commits only)

## Build 1321 - 4b3b677a - 2023-10-14 - Improvement: Fantasy Rollover
* 4b3b677a: Allow environmental overrides of API rate limits (_Thierry Draper_)
* 05483149: Remove the explicit use of the current database name in the Twitter comms stored procedure (_Thierry Draper_)

## Build 1319 - 4a34ae0b - 2023-10-12 - Improvement: Twitter Logo to X
* 4a34ae0b: Replace the Twitter logo with the new X logo (_Thierry Draper_)

## Build 1318 - bced645a - 2023-10-03 - Hotfix: Add Faker to CI Job
* bced645a: Add the new Faker library to the Phan source list for PHP standards CI job (_Thierry Draper_)

## Build 1317 - d1d37000 - 2023-10-03 - Composer: Faker Name Provider
* d1d37000: Add another provider for faker name generation (_Thierry Draper_)

## Build 1316 - 9ee77911 - 2023-09-30 - Hotfix: Twitter Sending Error Handling
* 9ee77911: Fix the error handling logic in the Twitter sending database procedure (_Thierry Draper_)

## Build 1315 - 78164db1 - 2023-09-24 - Feature: Two-way Syncable Tweets
* 78164db1: Migrate the database routine generated Tweets to a two-way syncable table (_Thierry Draper_)

## Build 1314 - 09d08d58 - 2023-09-24 - Hotfix: Twitter Card rendering
* 09d08d58: Fix Twitter Card rendering, as twitter:image:src is no longer supported (_Thierry Draper_)

## Build 1313 - 60ff612a - 2023-09-18 - Composer: September 2023
* (Auto-commits only)

## Build 1313 - 699aac06 - 2023-08-31 - Hotfix: Dropdown JavaScript Template Literals
* 699aac06: Fix some broken template literals in the Dropdown JS (_Thierry Draper_)

## Build 1312 - 935320ba - 2023-08-30 - Feature: Open Graph handling
* 935320ba: Switch to a general purpose symlink-to-dir CI converter, rather than targetting specific instances (_Thierry Draper_)
* 69f14564: Provide additional customisations for OG/Twitter meta tags (_Thierry Draper_)
* 57ca3cde: Implement a new exception class for external command execution (_Thierry Draper_)

## Build 1309 - e34bd15a - 2023-08-05 - Composer: August 2023
* (Auto-commits only)

## Build 1309 - 260b9adc - 2023-07-23 - Improvement: Deprecate the Expect-CT header
* 260b9adc: Disable handling of the deprecated Expect-CT header (_Thierry Draper_)

## Build 1308 - 48a28be5 - 2023-07-23 - Improvement: JavaScript Micro-optimisations
* 48a28be5: Make some micro-optimisations to the core JavaScript code (_Thierry Draper_)

## Build 1307 - f7519af4 - 2023-07-15 - Composer: July 2023
* (Auto-commits only)

## Build 1307 - 7fe3778a - 2023-07-13 - SysAdmin: CI Symlink Conversion
* 7fe3778a: Centralise the symlink-to-empty-dir conversion for CI jobs (_Thierry Draper_)

## Build 1306 - acc74440 - 2023-07-13 - Hotfix: String Encoding
* acc74440: Fix the string encoding method so legitimate strings are encoded, but binary data not mangled (_Thierry Draper_)

## Build 1305 - fbc99d5b - 2023-07-08 - Feature: Artisan command handling
* fbc99d5b: Allow subsite config being loaded via specified argument (_Thierry Draper_)
* 10e70cea: Include a Random Name Generator Composer class for Fantasy team name generation (_Thierry Draper_)
* 2e49fc4d: Include a symlink to Fantasy artisan commands (_Thierry Draper_)
* 42887d17: Write a basic, exemplar, artisan command to list local customised configuration (_Thierry Draper_)
* 66014bcd: Dynamically load artisan commands over our sub-site symlinks (_Thierry Draper_)

## Build 1300 - 0030d8b3 - 2023-07-01 - Hotfix: ORM Random Method
* 0030d8b3: Allow the ORM random() method to continue processing when it has received a size argument greater than its row count (_Thierry Draper_)

## Build 1299 - 3ede2cf4 - 2023-07-01 - Hotfix: Email Rendering CSP
* 3ede2cf4: Apply minor micro-optimisations to class layouts (_Thierry Draper_)
* d048a2d8: Add an appropriate nonce attribute to avoid CSP violations when viewing a sent email (_Thierry Draper_)

## Build 1297 - d14b2dea - 2023-06-28 - Hotfix: User Activity Session
* d14b2dea: Correctly store the session_id in the activity log (_Thierry Draper_)

## Build 1296 - 0507ba23 - 2023-06-28 - Improvement: Generalised Activity Log
* 0507ba23: Micro-optimise the Forms class to reduce line count (_Thierry Draper_)
* c032da6c: Allow auditing of ORM and Eloquent objects whilst post-processing an API form (_Thierry Draper_)
* 9cbca6a8: Allow auditing of ORM and Eloquent objects as an internal form post-processing action (_Thierry Draper_)
* 1e8b6bbe: Mask certain ORM fields when rendered for (potentially) external facing processing (_Thierry Draper_)
* 1a8f3830: Introduce an audit generation method within ORM and Eloquent classes (_Thierry Draper_)
* 5bdc5916: Move the wrapper-creation of audit log entries to UserActivityLog itself (_Thierry Draper_)
* d84b822c: Correctly stop encoding the detail field of User Activity Logs (_Thierry Draper_)
* 2d4cde3e: Merge the use-case specific activity log blocked field into the detail JSON (_Thierry Draper_)
* 4d70265d: Switch the User Activity Log away from an ENUM type for greater audit flexibility (_Thierry Draper_)

## Build 1287 - 3b3a01e5 - 2023-06-17 - Composer: June 2023
* (Auto-commits only)

## Build 1287 - 05d5b557 - 2023-06-15 - Hotfix: ORM sortBy with callables
* 05d5b557: Fix the distinction in ORM sortBy is_callable, as it could erroneously include string fields (_Thierry Draper_)

## Build 1286 - 3f528ccb - 2023-06-15 - Improvement: Twitter database processing
* 3f528ccb: Add generalised Twitter processing database methods (_Thierry Draper_)
* 144dd41b: Allow Retweet references in the Twitter table to store a NULL body field (_Thierry Draper_)

## Build 1284 - 50c29545 - 2023-06-04 - Hotfix: ORM Object Handling
* 50c29545: Remove duplicated is-actually-a-closure checks when sorting ORM data (_Thierry Draper_)
* dc6c1499: Prevent ORM object merging from double-encoding strings (_Thierry Draper_)

## Build 1282 - 136658e - 2023-05-26 - Improvement: CI Actions
* 136658e: Define a repo order for re-importing CI test data files when specified in multiple repos in a Refresh run (_Thierry Draper_)
* 0a20244: Stub the callout to the live password breach provider in CI for offline running (_Thierry Draper_)

## Build 1280 - 157b653 - 2023-05-14 - Composer: May 2023
* 157b653: Resolve Browscap standards issue following League/Flysystem updates (_Thierry Draper_)
* afac830: Add the log-fixing script to the setup of the PHP linting CI job (_Thierry Draper_)

## Build 1278 - a03fc48 - 2023-05-10 - Hotfix: Empty Pagination Counts
* a03fc48: Correct pagination counts when zero rows are available (_Thierry Draper_)

## Build 1277 - d350cdc - 2023-05-02 - SysAdmin: Deprecations
* d350cdc: Replace deprecated use of ${var} with {$var} (_Thierry Draper_)
* 6a88e2f: Replace use of fgrep with grep given it is being flagged overtly as obsolete (_Thierry Draper_)

## Build 1275 - cca14d7 - 2023-04-15 - Composer: April 2023
* cca14d7: Fix parsing of Highcharts default options when resource setup called externally (_Thierry Draper_)

## Build 1274 - 71a6e7c - 2023-03-16 - Improvement: Highcharts Customisations
* 71a6e7c: Allow a little more customisation and module use within Highcharts (_Thierry Draper_)

## Build 1273 - 386d23d - 2023-03-11 - Hotfix: Phan Exception
* 386d23d: Remove the fruitcake/laravel-cors exception within the Phan config (_Thierry Draper_)

## Build 1272 - b230758 - 2023-03-11 - Composer: March 2023
* b230758: Fix minor (and non-impacting) method name casing (_Thierry Draper_)
* 59a64c8: Remove the fruitcake/laravel-cors package, which is now a standard part of the Laravel framework (_Thierry Draper_)

## Build 1270 - e5522e1 - 2023-03-01 - Improvement: CSS Standards to Prettier
* e5522e1: Fix CSS standards according to the new Prettier rules (_Thierry Draper_)
* c96d9b7: Switch the deprecated Stylelint CSS standards to Prettier (_Thierry Draper_)

## Build 1268 - d75a8cf - 2023-02-28 - Hotfix: Ajax handling
* d75a8cf: Improve Ajax handling and repsonse parsing (_Thierry Draper_)

## Build 1267 - da72bd4 - 2023-02-28 - Improvement: Alternative Timezones
* da72bd4: Allow for parsing and processing of times in alternative timezones to system/app/database/user (_Thierry Draper_)

## Build 1266 - dcc6afd - 2023-02-12 - Composer: February 2023
* (Auto-commits only)

## Build 1266 - 55cfb85 - 2023-01-31 - Improvement: ORM and Unit Test
* 55cfb85: Update how POST/PUT/PATCH data is passed around in unit tests (_Thierry Draper_)
* cddc7ac: Include an ORM pagination mechanism and querying capability (_Thierry Draper_)
* 1df54f7: Split the Redirect Checks in the middleware to allow improved testing (_Thierry Draper_)
* 6b23a7d: Apply minor formatting tidying (_Thierry Draper_)
* 0d270b9: Strip the unused EncryptCookies wrapper (_Thierry Draper_)

## Build 1261 - a794043 - 2023-01-15 - Composer: January 2023
* (Auto-commits only)

## Build 1261 - 4073625 - 2023-01-08 - SysAdmin: CI Docker Images from Container Registry
* 4073625: Switch the CI jobs to images from our Container Registry (_Thierry Draper_)

## Build 1260 - a33b892 - 2023-01-05 - Improvement: MySQL Linting
* a33b892: Switch the CI MariaDB collation to latin1_general (_Thierry Draper_)
* 9e418cc: Include the CI job for linting setup scripts (_Thierry Draper_)

## Build 1258 - 84e9653 - 2022-12-18 - Composer: December 2022
* (Auto-commits only)

## Build 1258 - 35fbf9a - 2022-11-19 - Composer: November 2022
* (Auto-commits only)

## Build 1258 - b226062 - 2022-11-15 - Improvement: Switch Analytics to GA4
* b226062: Switch Google Analytics to dual-running of UA (v3) and GA4 (_Thierry Draper_)

## Build 1257 - 31e1b2a - 2022-10-16 - Composer: October 2022
* (Auto-commits only)

## Build 1257 - e2f4ece - 2022-09-18 - Composer: September 2022
* (Auto-commits only)

## Build 1257 - efb8384 - 2022-09-14 - Improvement: Third Party Resources
* efb8384: Install wget as a CI environment dependency (_Thierry Draper_)
* ca7e3cc: Remove HIBP tests from dev mode, to allow for offline development (_Thierry Draper_)
* a4adc04: Minor Resource loading tidy (_Thierry Draper_)
* f8dd415: Move Google Font files from upstream link to local files (_Thierry Draper_)
* b055ded: Move Font Awesome files from upstream link to local files (_Thierry Draper_)
* 7747a4b: Add the Highcharts accessibility code at their suggestion (_Thierry Draper_)

## Build 1251 - 5063275 - 2022-08-14 - Composer: August 2022
* (Auto-commits only)

## Build 1251 - 53d44be - 2022-08-05 - Improvement: Database input encoding
* 53d44be: Stop double-encoding input from the database (_Thierry Draper_)

## Build 1250 - 3c89a0e - 2022-08-02 - Feature: Article link parsing
* 3c89a0e: Add article link parsing in the same way image parsing occurs (_Thierry Draper_)

## Build 1249 - 22ad46e - 2022-08-01 - ORM: API value type setting
* 22ad46e: Provide a standardised mechanism for API value type setting (_Thierry Draper_)

## Build 1248 - edb84de - 2022-07-30 - Feature: ORM Database name getter
* edb84de: Add a method for ORM objects to determine their own database name (_Thierry Draper_)

## Build 1247 - ba10155 - 2022-07-20 - SysAdmin: Make the .htaccess neutral
* ba10155: Remove, rather than comment out, default htaccess rules we do not use (_Thierry Draper_)
* 2aff3d7: Remove direct references to PHP version used in prod (_Thierry Draper_)

## Build 1245 - a8f136c - 2022-07-19 - SysAdmin: Backport for PHP 8.0
* a8f136c: Update the composer file to allow PHP 8.0, as well as the previous 8.1 (_Thierry Draper_)

## Build 1244 - 4b720a4 - 2022-07-17 - Hotfix: Local config file loading
* 4b720a4: Standardise which local config files (_env, _ci), and where into the array they are loaded (_Thierry Draper_)

## Build 1243 - 42c8cec - 2022-07-16 - Composer: July 2022
* (Auto-commits only)

## Build 1243 - 69b147d - 2022-07-15 - Improvement: Fantasy Record Breakers game view
* 69b147d: Apply some minor cody tidying (_Thierry Draper_)
* 1df1e09: Increase the scope of PHPMDs TooManyMethods whitelist (_Thierry Draper_)
* 7ea9a72: Update configuration site loading for use within Unit, not Feature, tests (_Thierry Draper_)
* fe1456a: Prune hidden fields in API transformations after post-processing, not before (_Thierry Draper_)

## Build 1239 - e2948cc - 2022-06-19 - Composer: June 2022
* (Auto-commits only)

## Build 1239 - 0cff237 - 2022-06-08 - SysAdmin: Revert to native Blade data displaying
* 0cff237: Restore cleansing of user input at source, with corresponding XSS reflection unit test and fixes (_Thierry Draper_)
* c9fd4ab: Re-factor our Blade template variable echoing to use the appropriate native Laravel mechanism (_Thierry Draper_)
* 0bdb2fb: Move common DeBear helper classes to the alias list for use in Blade templates (_Thierry Draper_)
* d3f5c7c: Revert use of @print and @config in Blade templates with the native Laravel method (_Thierry Draper_)
* 764e855: Restore Blade linting using a new dependency (_Thierry Draper_)

## Build 1234 - b331a51 - 2022-05-31 - Improvement: Policy checks in middleware
* b331a51: Add a new middleware for policy checking, removing that action from the controllers (_Thierry Draper_)

## Build 1233 - 3523850 - 2022-05-27 - Improvement: Default Forms source
* 3523850: Remove the SymLinks required for fantasy events, which are no longer in use (_Thierry Draper_)
* e485592: Improve a code coverage workaround for bug reporting that is only required on live (_Thierry Draper_)
* 02ed926: Fix how the InternalCache is reset between unit tests (_Thierry Draper_)
* 02469d9: Add the InternalCache as an option for loading default form data (_Thierry Draper_)

## Build 1229 - db4491b - 2022-05-27 - CI: PHP Code Coverage streamlining
* db4491b: Make the PHP version dependency check of Phan dynamic (_Thierry Draper_)
* 928b83a: Streamline the PHP Code Coverage output, and consider output less than 100% as a failure (_Thierry Draper_)

## Build 1227 - 2a14567 - 2022-05-26 - CI: Report PHP Unit code coverage on master
* 2a14567: Propagate the PHP Unit code coverage report to the deploy stage for its result to be tagged against the master branch (_Thierry Draper_)

## Build 1226 - ea10048 - 2022-05-22 - CI: Switch Code Coverage Driver
* ea10048: Switch Code Coverage from using XDebug to PCOV (_Thierry Draper_)

## Build 1225 - 8066ce5 - 2022-05-22 - Improvement: Unit Tests as part of Code Coverage
* 8066ce5: Add the GitLab code coverage regexp pattern, previously configured within the UI (_Thierry Draper_)
* e172fbc: Move internal logs during localci runs to our new, separate, location (_Thierry Draper_)
* e4c48d1: Make refinements to ensure 100% code coverage on localci runs where cached files may already exist (_Thierry Draper_)
* a5f03e9: Fix the existing unit tests to achieve 100% code coverage (_Thierry Draper_)
* 0901d94: Add a unit test for our new unthrottled Unit Test methods (_Thierry Draper_)
* ee5a40a: Include appropriate PHPUnit code in our code coverage tests (_Thierry Draper_)
* b1065be: Include a manner to make unthrottled Unit Test calls for an API (_Thierry Draper_)

## Build 1218 - 7bf078d - 2022-05-22 - Improvement: Laravel 9
* 7bf078d: Make Phan tweaks following the upgrade to Laravel 9 (_Thierry Draper_)
* 2f5a491: Switch our error handling unit test to use the Symfony error handler (_Thierry Draper_)
* 9f61191: Upgrade BrowscapPHP dependencies following version upgrade (_Thierry Draper_)
* 65fc3df: Make session handler signature changes from Laravel 9 upgrade (_Thierry Draper_)
* 6473a8d: Upgrade to Laravel 9 (and its composer dependencies) (_Thierry Draper_)

## Build 1213 - 350dabd - 2022-05-21 - Feature: Mobile password toggle
* 350dabd: Include a mobile login password show/hide field toggle (_Thierry Draper_)

## Build 1212 - 3965991 - 2022-05-21 - Hotfix: Mobile Dropdown value
* 3965991: Correctly update the (hidden) select field when setting a dropdown value via JavaScript (_Thierry Draper_)

## Build 1211 - ad2e28a - 2022-05-21 - Feature: Forms API
* ad2e28a: Include object pre-processing before rendering object as an API record (_Thierry Draper_)
* 29b7a5b: Extend the form module processing to include passing data from an API (_Thierry Draper_)
* 56747a2: Setup the current User object on API requests, based initially on the token's user (_Thierry Draper_)
* 06904b4: Standardise generation of validation failure in API responses (_Thierry Draper_)
* 7fd46a9: Standardise handling of default form field type being text input (_Thierry Draper_)
* 97ec2ee: Allow multiple, logic'd, scope combinations on an API route (_Thierry Draper_)
* 94828d0: Move definition of API rate limits to config (_Thierry Draper_)
* 814eee7: Return API errors as JSON, not HTML (_Thierry Draper_)
* de7356e: Exclude the /api/ part of a URL when loading sub-section config (_Thierry Draper_)
* ff3a879: Ensure API tokens are unique by their token reference (_Thierry Draper_)

## Build 1201 - 18d68fb - 2022-05-21 - Hotfix: Debug panel policy tree
* 18d68fb: Fix rendering of the policy tree in the Debug panel (_Thierry Draper_)

## Build 1200 - 23334ef - 2022-05-21 - Hotfix: Benchmark duplicate process key
* 23334ef: Remove duplicate benchmark process key (_Thierry Draper_)

## Build 1199 - b21e2c9 - 2022-05-21 - Improvement: String interpolation
* b21e2c9: Process session values in string interpolation (_Thierry Draper_)

## Build 1198 - b9c0915 - 2022-05-21 - Improvement: Forms
* b9c0915: Add symlinks for Fantasy sub-site event management (_Thierry Draper_)
* dd78b5e: Allow dispatching of events during form processing (_Thierry Draper_)
* 38aec07: Allow loading of default form data from transient session (_Thierry Draper_)
* 131aff8: Dispatch a UserLogin event when a user logs in (_Thierry Draper_)
* 052af39: Restore Event/Listeners with some initial setup (_Thierry Draper_)
* af47825: Correctly hold on to internal ORM object when an object is updated (_Thierry Draper_)
* 8bb5504: Add the ability to have transient storage, flushed on login/logout (_Thierry Draper_)
* 5a909fd: Allow an email template's message to be null, as its main body is handled in the wrapper (_Thierry Draper_)
* 7e07aa7: Move the defintion of an email app from code to the email's template (_Thierry Draper_)
* e7ac933: Allow a form dropdown to be disabled by default (_Thierry Draper_)
* a939f21: Make a minor code quality improvement to form model data processing (_Thierry Draper_)
* ea5d508: Correctly differentiate an empty form value from a value that resolves to false (_Thierry Draper_)
* 0d81959: Improve form Checkbox handling via JavaScript (_Thierry Draper_)
* 6b62249: Add the ability to return a standard non-redirect response when checking Form restrictions (_Thierry Draper_)
* b1bb7b4: Add some symlinks to the Fantasy subsite for forms and email wrappers (_Thierry Draper_)

## Build 1183 - fa70baa - 2022-05-21 - Hotfix: Password policy conformity
* fa70baa: Fix storing of password policy conformity (_Thierry Draper_)

## Build 1182 - f972b75 - 2022-05-21 - Improvement: Middleware
* f972b75: Allow time over-rides to be specified by config rather than in-code call (_Thierry Draper_)
* 7a164f3: Move some of our core middleware processing to earlier in the flow (_Thierry Draper_)

## Build 1180 - 0d31cec - 2022-05-21 - Feature: Ajax Headers
* 0d31cec: Correct minor typo in comment for unit test action wrappers (_Thierry Draper_)
* f9a5094: Allow Ajax requests to pass additional headers (_Thierry Draper_)

## Build 1178 - 9f96e9c - 2022-05-21 - SysAdmin: PHP 8
* e4f6629: Allow global _env file from the skeleton, not just local sites (_Thierry Draper_)
* 650a719: Upgrade Font Awesome to v6 (_Thierry Draper_)
* fbddc62: Use a new stylelint rule modifier to prevent recent false negatives (_Thierry Draper_)
* ce5cadf: Add a new CI step to lint Blade templates (_Thierry Draper_)
* f4e6943: Move away from our Arrays::hasIndex wrapper (_Thierry Draper_)
* e0ca669: Improve null handling, including PHP 8s nullsafe operator (_Thierry Draper_)
* 7280171: Implement PHP 8s mixed return types (_Thierry Draper_)
* 77d1e6e: Implement PHP 8s new union param/return types (_Thierry Draper_)
* 6eb0e0f: Add a symlink to the Sports PHPUnit extensions for more appropriate namespacing (_Thierry Draper_)
* d25a643: Apply the final fixes suggested by Phan (_Thierry Draper_)
* 5c29f5c: Apply several type-hinting related fixes (_Thierry Draper_)
* 390d937: Remove unused imported classes (_Thierry Draper_)
* 70aaf03: Switch from Laravel's on-the-fly class facades at the root level to their actual full path (_Thierry Draper_)
* b39db7b: Add a PHP Standards CI job to run phan tests adapted from their --init-level=3 (_Thierry Draper_)
* 0053604: Fix code coverage fallout from PHP 8 migration (_Thierry Draper_)
* 1eb8f94: Make the functionality tweaks from PHP 8 migration (_Thierry Draper_)
* 032c648: Include mixed class method paramter type hints from PHP 8 (_Thierry Draper_)
* 6884bd6: Fix the HibP constructor now requiring an argument, post-PHP 8 upgrade (_Thierry Draper_)
* 0d866ed: Migrate composer dependencies to PHP 8 equivalents (_Thierry Draper_)

## Build 1159 - a747cb7 - 2021-11-20 - Hotfix: Core Request Processing Middleware
* a747cb7: Add a new middleware to correctly define and handle 'core' request processing previously defined via config (_Thierry Draper_)

## Build 1158 - 1559759 - 2021-11-14 - SysAdmin: Compress Merges
* 1559759: Start gzipping merged content, behind the scenes (_Thierry Draper_)

## Build 1157 - 2a24960 - 2021-11-13 - Composer: November 2021
* (Auto-commits only)

## Build 1157 - a75a592 - 2021-11-12 - CI: CSS Property Standards
* a75a592: Revert ORM object re-use when the full data subset is requested (_Thierry Draper_)
* b4ec3b1: Update CSS property rules to reflect the new alphabetical standards (_Thierry Draper_)
* 965a33c: Add new stylelint standards test for alphabetical CSS property ordering (_Thierry Draper_)

## Build 1154 - 90654e5 - 2021-11-10 - Feature: API Setup
* 90654e5: Add a mechanism to allow Feature Unit Test headers from being generically modified (_Thierry Draper_)
* 6dbe654: Add an API Token validation middleware for API requests (_Thierry Draper_)
* 050e54b: Add the core logic to define and apply rate limiting rules to API routes (_Thierry Draper_)
* 367c271: Create a new API-version of the HTTP status code wrapper process for API responses (_Thierry Draper_)
* e074cef: Allow additional per-instance ORM fields from being hidden when converted to an API row (_Thierry Draper_)
* 50e237d: Enable bespoke per-row post-processing of an ORM object whilst rendering as an API row (_Thierry Draper_)
* 10e764b: Correctly (and consistently) handle ORM sub-object creation internal state passing (for things like sorting) (_Thierry Draper_)
* ae6c20b: Stop creating a new ORM subset object if the requested keys match the full resultset (_Thierry Draper_)
* c737dd2: Add support (with an unfortunate pre-req) for API exports of Secondary model data (_Thierry Draper_)
* ab0cf2f: Make the DeBear-processing middleware compatible with both web and, now, API routes (_Thierry Draper_)
* 434f6a2: Add DeBear ORM logic for API representation of a model (_Thierry Draper_)
* e5a4d79: Setup the API routing, following the same subsite process as the web routing (_Thierry Draper_)

## Build 1142 - 622a665 - 2021-10-29 - Feature: Handling Config and DateTime unit test tweaks
* 622a665: Remove a recently deprecated stylelint rule (_Thierry Draper_)
* dfbd3f3: Use the new standardised config/date/time unit test tweak methods (_Thierry Draper_)
* 93eb752: Include standard methods for tweaking config/date/time in PHPUnit tests (_Thierry Draper_)

## Build 1139 - 676e143 - 2021-10-18 - Hotfix: Multi-Repo PHPUnit Test Data
* 676e143: Re-import both skeleton and repo test data for the same table between PHPUnit tests (_Thierry Draper_)

## Build 1138 - 61d7683 - 2021-10-16 - Composer: October 2021
* (Auto-commits only)

## Build 1138 - 4d8258b - 2021-09-18 - Composer: September 2021


## Build 1140 - e2d2929 - 2021-09-16 - SysAdmin: Fantasy Setup
* 0aa932c: Merge branch 'sysadmin/fantasy-setup' (_Thierry Draper_)
* e2d2929: Standardise the sitemap subsite parser (_Thierry Draper_)
* f808c19: Simplify arguments in the string pluralisation method (_Thierry Draper_)
* 8d07928: Setup Fantasy SymLinks (_Thierry Draper_)

## Build 1136 - a4f0376 - 2021-09-04 - Improvement: String Pluralisation
* a4f0376: Simplify arguments in the string pluralisation method (_Thierry Draper_)

## Build 1135 - d67da0e - 2021-08-24 - Improvement: Resource Merging
* d67da0e: Make resource merging a publically accessible method for external merging (_Thierry Draper_)
* af5a46f: Fully remove the old concept of resource 'modes' for separate desktop/mobile CSS/JS (_Thierry Draper_)
* a18b20a: Allow for pre-processed CSS/JS minification, with external sources (e.g., Highcharts) still minified (_Thierry Draper_)
* 45d56c0: Generalise resource prepending for future option passing (_Thierry Draper_)

## Build 1131 - 456cf3f - 2021-08-13 - Composer: August 2021
* (Auto-commits only)

## Build 1131 - e6fda14 - 2021-08-12 - Hotfix: PHPUnit XML .env spec
* e6fda14: Correct the PHPUnit test XML .env spec session domain to include subdomains (_Thierry Draper_)

## Build 1130 - 83ecebf - 2021-08-12 - Improvement: Carbonise Dates
* 83ecebf: Convert timezone handling to Carbon from the in-house class (_Thierry Draper_)
* ccab5a4: Fix setting the user's timezone details in the setup process (_Thierry Draper_)

## Build 1128 - 83b93a0 - 2021-08-07 - Hotfix: Database Date Saving
* 83b93a0: Ensure saved dates are stored in the correct timezone for the database (_Thierry Draper_)

## Build 1127 - 9d1bd1e - 2021-08-03 - Improvement: Error page handling
* 9d1bd1e: Make necessary changes to ensure config fully loaded when rendering error pages (_Thierry Draper_)
* aaacc7a: Slim the error wrapper templates (_Thierry Draper_)

## Build 1125 - e8eb231 - 2021-07-31 - Hotfix: Navigation Homepages
* e8eb231: Distinguish between top-level and subsite homepages in the navigation (_Thierry Draper_)

## Build 1124 - e354809 - 2021-07-31 - Hotfix: Font Awesome CSPs
* e354809: Revert FontAwesome to the more CSP-compatible CDN version than kit (_Thierry Draper_)
* 205e7be: Enable full CSP in all modes, given Sports failures with only report mode in dev (_Thierry Draper_)

## Build 1122 - 487505d - 2021-07-27 - SysAdmin: Standardised test helper methods
* 487505d: Updated bash test helper methods, in-line with other repos (_Thierry Draper_)

## Build 1121 - e02b2a7 - 2021-07-20 - Improvement: Multiple News App CSP Updates
* e02b2a7: Allow multiple news apps from being passed to the CSP updater (_Thierry Draper_)

## Build 1120 - c91fd8f - 2021-07-17 - Composer: July 2021
* (Auto-commits only)

## Build 1120 - 884c46d - 2021-07-17 - Hotfix: Meta Descrips with custom data
* 884c46d: Fix building of page Meta Descrips being passed custom data (_Thierry Draper_)

## Build 1119 - 3e1f2f4 - 2021-07-08 - Hotfix: ORM Secondary Handling
* 3e1f2f4: Skip ORM objects loading secondary data when no data in original query (_Thierry Draper_)

## Build 1118 - 6892a49 - 2021-07-08 - Improvement: Scrolltables
* 6892a49: Properly handle multiple scrolltables on the same page, not all displayed immediately (_Thierry Draper_)
* 0cd1700: Allow scrolltables to adapt down to mobile, rather than to a single value (_Thierry Draper_)

## Build 1116 - 7ce6d03 - 2021-06-21 - Composer: June 2021
* (Auto-commits only)

## Build 1116 - eb934e4 - 2021-05-15 - Composer: May 2021
* (Auto-commits only)

## Build 1116 - 171ed21 - 2021-05-09 - Improvement: SET column handling in ORM
* 171ed21: Convert database SET columns into boolean objects (_Thierry Draper_)

## Build 1115 - d6c04e1 - 2021-05-06 - Hotfix: Page Title Breadcrumbs
* d6c04e1: Improve PageTitle handling when multiple calls made in a single Unit Test (_Thierry Draper_)

## Build 1114 - 5e0281a - 2021-04-30 - CI: Pipeline Tweaks
* 5e0281a: Move the CI deploy timeout from Project to Job (_Thierry Draper_)
* fc97c48: Make use of the CI's needs: option to streamline job and stage links (_Thierry Draper_)

## Build 1112 - b11d7ba - 2021-04-30 - Hotfix: PHPUnit Test Error Handling
* b11d7ba: Ensure app.debug is seen as a boolean (in particular from CI) (_Thierry Draper_)

## Build 1111 - 1eee668 - 2021-04-17 - Composer: April 2021
* 1eee668: Switch Compoships usage to a trait, as required in 2.1 (_Thierry Draper_)

## Build 1110 - 0b87e2d - 2021-04-15 - Improvement: Multiple ORM
* 0b87e2d: Correctly handle creating empty ORM sub-objects (_Thierry Draper_)
* 67f0314: Allow for chainable ORM object merging (_Thierry Draper_)
* aacc488: Remove ill-used internal ORM property storing the sub-keys (_Thierry Draper_)
* d6391db: Allow ORM objects to specify default secondary objects to load (_Thierry Draper_)
* 0e144de: Add an ORM object to create a reversed instance of the original data (_Thierry Draper_)

## Build 1105 - 01c6a7c - 2021-04-15 - Improvement: Highcharts Configuration
* 01c6a7c: Externally accessible Highcharts configuration (_Thierry Draper_)

## Build 1104 - d5a33e3 - 2021-04-15 - Hotfix: Specific grammar pluralisation
* d5a33e3: Apply specific grammar fixes in the string pluralisation (_Thierry Draper_)

## Build 1103 - 0a4c0f7 - 2021-03-13 - Composer: March 2021
* (Auto-commits only)

## Build 1103 - e36d2c5 - 2021-03-06 - Improvement: Automatic ORM Total Row Count
* e36d2c5: Automatically include an ORM query's total row count (_Thierry Draper_)

## Build 1102 - 14652ae - 2021-03-06 - Hotfix: ScrollTable Offset Calculation
* 14652ae: Fix how the scrolltable offset is calculated (_Thierry Draper_)

## Build 1101 - 147c962 - 2021-03-06 - Improvement: Mobile Pagination
* 147c962: Make the pagination mobile friendly (plus some minor helper tidying) (_Thierry Draper_)

## Build 1100 - 702c5ce - 2021-02-13 - Composer: February 2021
* (Auto-commits only)

## Build 1100 - bcccc7d - 2021-02-12 - Hotfix: News Schema Tweaks
* bcccc7d: Standardise News schema from latest fixes (_Thierry Draper_)

## Build 1099 - 3b9a60d - 2021-02-07 - Hotfix: PwnedPassword Instantiation
* 3b9a60d: Fix how the PwnedPassword class is accessed for production (_Thierry Draper_)

## Build 1098 - 6e8a457 - 2021-02-04 - Hotfix: Permissions-Policy separator
* 6e8a457: Fix the Permissions-Policy header by using the correct separator (_Thierry Draper_)

## Build 1097 - 2a08d26 - 2021-01-21 - Feature: Weather Forecast ORM object
* 2a08d26: Add ORM objects for extracting weather forecasts (_Thierry Draper_)

## Build 1096 - c80fb6a - 2021-01-16 - Composer: Replace deprecated Patchwork/Utf8
* c80fb6a: Replace the deprecated Patchwork/Utf8 with Symfony/String (_Thierry Draper_)

## Build 1095 - 97740fe - 2021-01-16 - Composer: January 2021
* (Auto-commits only)

## Build 1095 - 7b96e47 - 2021-01-16 - Improvement: Google Analytics to gtag.js
* 7b96e47: Move Google Analytics from analytics.js to gtag.js (_Thierry Draper_)

## Build 1094 - f185895 - 2021-01-12 - Hotfix: random() on empty ORM object
* f185895: Correctly handle calling random() on an ORM empty object (_Thierry Draper_)

## Build 1093 - eab5dab - 2021-01-11 - Feature: Password Policy
* eab5dab: Reverse the Code Coverage workarounds following Xdebug bug fixes (_Thierry Draper_)
* b723146: Skip form processing of empty fields that are not required (i.e., no value implies no change) (_Thierry Draper_)
* fef8b0d: Fix the Model to Array converstion which does not localise date/times correctly (_Thierry Draper_)
* 17765fc: Rename the ObjectCache as InternalCache, as it actually stores more than just objects (_Thierry Draper_)
* 7c6df5d: Create a new Blade directive to shorten rendering config values (_Thierry Draper_)
* ea7f51f: Validate user's passwords match our new policy (on login only) (_Thierry Draper_)
* 9cf3b2f: Determine if a password complies with our new policy (_Thierry Draper_)
* 8b1b221: Include a composer library for performing Have I Been Pwned? API calls (_Thierry Draper_)

## Build 1085 - ab73f94 - 2021-01-03 - CI: Only deploy on master branch
* ab73f94: Only deploy during CI/AD of the master branch (_Thierry Draper_)

## Build 1084 - 30291c9 - 2021-01-02 - Hotfix: SubNav Separator Click Handler
* 30291c9: Fix the SubNav click handler when clicking the separators (_Thierry Draper_)

## Build 1083 - f5c4597 - 2021-01-02 - Hotfix: Env Config Loadtime
* f5c4597: Move loading env config to before subsite config (_Thierry Draper_)

## Build 1082 - dff829c - 2021-01-02 - Improvement: Dynamically Reload Dropdowns
* dff829c: Allow for re-loading of dropdowns on a page after dynamic changes (_Thierry Draper_)

## Build 1081 - 42db373 - 2020-12-26 - Hotfix: String Codification
* 42db373: Correctly handle multiple non-alphanumeric characters in string codification (_Thierry Draper_)

## Build 1080 - 733e59c - 2020-12-26 - SysAdmin: Replace Abandoned Faker Module
* 733e59c: Replace the abandoned fzaninotto/faker module with fakerphp/faker (_Thierry Draper_)

## Build 1079 - d5f8cc2 - 2020-12-20 - Composer: December 2020
* (Auto-commits only)

## Build 1079 - efad13d - 2020-12-05 - Improvement: Various for Sports Major League Game Page
* efad13d: Run isset() tests on accessor output (_Thierry Draper_)
* dd18acc: Expose use of the Highcharts annotations library (_Thierry Draper_)
* 235da9e: Allow the SubNav widgets to include icons in the content (_Thierry Draper_)
* 3b5fc60: Convert incorrect tabs for whitespaces in to space characters (_Thierry Draper_)

## Build 1075 - 53dda0c - 2020-12-05 - Hotfix: Widen News Columns
* 53dda0c: Fixes (and workarounds) for the switch from Xdebug 2 to 3 (_Thierry Draper_)
* 639753a: Widen News database columns to reflect latest data (_Thierry Draper_)

## Build 1073 - 805bf8b - 2020-11-14 - Composer: November 2020
* (Auto-commits only)

## Build 1073 - e185a8c - 2020-10-16 - Hotfix: Mobile Page Chrome
* e185a8c: Fix mobile header layout (_Thierry Draper_)

## Build 1072 - 7c6322d - 2020-10-16 - Improvement: Sub-site manifest.json
* 7c6322d: Tidy the manifest.json file and add sub-site capability (_Thierry Draper_)

## Build 1071 - 07b7bfb - 2020-10-11 - Improvement: Security Header Updates
* 07b7bfb: Remove the CSP reporting, given potential for exploits (and somewhat lack of usefulness to-date?) (_Thierry Draper_)
* d27fc8b: Remove the X-XSS-Protection header, which is redundant in modern browser (_Thierry Draper_)
* a236876: Switch the deprecated Feature-Policy header to its Permissions-Policy replacement (_Thierry Draper_)

## Build 1068 - 5733e90 - 2020-10-08 - Composer: Upgrade to Laravel 8
* 5733e90: Updates following the PHPMD 2.9 upgrade (_Thierry Draper_)
* 46055ec: Update PHPUnit assertions used following the upgrade to PHPUnit 9 (_Thierry Draper_)
* c42549b: Update our CI rules following the Laravel 8 (+deps) upgrade (_Thierry Draper_)
* 91a456a: Upgrade from Laravel 7 to 8 (_Thierry Draper_)

## Build 1064 - 8fe776e - 2020-10-07 - Improvements: SubNav and Calendar
* 8fe776e: Fix the calendar centre aligning, added in the use-case not skeleton (_Thierry Draper_)
* f3e1106: Allow the SubNav responsive switch between tabs and dropdown to be customisable (_Thierry Draper_)

## Build 1062 - 6f85e7c - 2020-10-02 - Improvement: Repositories Code Coverage
* 6f85e7c: Add/Update unit tests, and make appropriate fixes, to improve the skeleton's code coverage of the Repositories folder (_Thierry Draper_)

## Build 1061 - 0b08aed - 2020-09-26 - Improvement: Migrations to Schema
* 0b08aed: Switch from Migrations to Schemas updated via schema-sync (_Thierry Draper_)

## Build 1060 - 59d88f4 - 2020-09-19 - Composer: September 2020
* (Auto-commits only)

## Build 1060 - 871d601 - 2020-09-18 - CI: Merge PHP Standards Jobs
* 871d601: Merge the PHP Mess Detector and Standards tests into a single job (_Thierry Draper_)

## Build 1059 - 7d43acc - 2020-09-18 - Improvement: HTTP Status Wrapper
* 7d43acc: Start using generic (named) wrappers for sending particular HTTP status codes (_Thierry Draper_)

## Build 1058 - 8388076 - 2020-09-16 - Feature: Calendar Widget
* 8388076: Add the <sup line-height fix to the CSS reset (_Thierry Draper_)
* 69eb999: Add a new calendar widget (_Thierry Draper_)

## Build 1056 - 5920c60 - 2020-09-16 - Improvement: SubNav Handlers
* 5920c60: Minor tweaks to the SubNav setup and handlers (_Thierry Draper_)

## Build 1055 - 7f5799f - 2020-09-16 - Feature: Toggleable Content
* 7f5799f: New JavaScript click handler for toggling content (_Thierry Draper_)

## Build 1054 - a926d41 - 2020-09-16 - Hotfix: Feature-Policy Header List
* a926d41: Fixes to the Feature-Policy headers for policies that have not been adopted (_Thierry Draper_)

## Build 1053 - 97da7ac - 2020-09-16 - Improvement: CI Helpers
* 97da7ac: Database timezone setter for use in CI tests (_Thierry Draper_)
* 3954485: Move the GeoIP test specs to an external source, loaded during the CI job (_Thierry Draper_)
* a499136: Allow CI-specific config to overwrite standard config (_Thierry Draper_)

## Build 1050 - cc8ae8a - 2020-08-15 - Composer: August 2020
* cc8ae8a: Fix the GeoIP test for Google's DNS (_Thierry Draper_)
* 978ebb8: Fix the User Permission CI test due to expanding Sports dev (_Thierry Draper_)

## Build 1048 - f18476a - 2020-07-13 - Hotfix: Zero Date Handling
* f18476a: Consider 0000-00-00 dates as NULL to protect from Carbon issues (_Thierry Draper_)

## Build 1047 - b8fa4d9 - 2020-07-13 - Hotfix: Pruned User method still in use
* b8fa4d9: Restore a User model method that was pruned but still in use by the My user account processes (_Thierry Draper_)

## Build 1046 - dc6ef3e - 2020-07-12 - Composer: July 2020
* (Auto-commits only)

## Build 1046 - 4b05f69 - 2020-07-12 - Improvement: HTTP Code Coverage
* 4b05f69: Improve PHPUnit efficiency to avoid CI processing oom's (_Thierry Draper_)
* 73fa14b: Add/Update unit tests, and make appropriate fixes, to improve the skeleton's code coverage of the Http folder (_Thierry Draper_)

## Build 1044 - 24425e2 - 2020-06-29 - Hotfix: Database Charset/Collation
* 24425e2: State to Laravel that our database charset/collation is actually latin1 (_Thierry Draper_)

## Build 1043 - 917d6bf - 2020-06-28 - Hotfix: Font Awesome Content-Security-Policy Rules
* 917d6bf: Generalise the Font Awesome Content-Security-Policy rules to resolve a recent remote change (_Thierry Draper_)

## Build 1042 - f1c6241 - 2020-06-23 - Improvement: Model Code Coverage
* f1c6241: Limit the width of the session_id column in the database to Laravel's 40-char max (_Thierry Draper_)
* 9aef689: Add/Update unit tests, and make appropriate fixes, to improve the skeleton's code coverage of the Eloquent models (_Thierry Draper_)

## Build 1040 - 3887ca3 - 2020-06-20 - SysAdmin: Symlink Tidy
* 3887ca3: Remove a footer link symlink which causes unnecessary release noise (_Thierry Draper_)

## Build 1039 - b281cbd - 2020-06-20 - Improvement: Customisable Sitemap Template
* b281cbd: Allow per-instance customisable sitemap template (_Thierry Draper_)

## Build 1038 - 5065ac0 - 2020-06-17 - Hotfix: PHPUnit Test Locale
* 5065ac0: Fix PHPUnit tests on CI as the locale does not match dev/prod environments (_Thierry Draper_)

## Build 1037 - 6833940 - 2020-06-16 - Hotfix: HTML5 Character Decoding
* 6833940: Expand the HTML decoding to include all encoded characters (_Thierry Draper_)

## Build 1036 - 56083b2 - 2020-06-14 - Composer: June 2020
* (Auto-commits only)

## Build 1036 - c5820e3 - 2020-06-13 - SysAdmin: External Release Logic
* c5820e3: Move the entire release logic to its own external script (_Thierry Draper_)

## Build 1035 - 96550b2 - 2020-06-13 - SysAdmin: Fix Release Script
* 96550b2: Fix the release script's repo->dir determination (_Thierry Draper_)

## Build 1034 - cc4ced5 - 2020-06-13 - SysAdmin: Reduce Release Overlap
* cc4ced5: Reduce the overlap time between releases when performing the release (_Thierry Draper_)

## Build 1033 - 4777a24 - 2020-06-08 - Improvement: Age Display Helper
* 4777a24: Add second-to-hours/minutes/days/years display helper methods (_Thierry Draper_)

## Build 1032 - fb1521e - 2020-06-06 - Hotfix: Google Fonts Loading CSPs
* fb1521e: Fix the CSP issues caused by the Google Fonts loading change (_Thierry Draper_)

## Build 1031 - 682bb6f - 2020-06-06 - Improvement: Loading Google Fonts
* 682bb6f: Improve the loading of Google Fonts (_Thierry Draper_)

## Build 1030 - 6f4c7d2 - 2020-06-06 - Improvement: PSR-4-like Standards Check
* 6f4c7d2: Add PSR-4 like standards checks, given recent class and file name mis-matches (_Thierry Draper_)

## Build 1029 - f1c3a48 - 2020-06-05 - Improvement: CI Code Coverage
* f1c3a48: Add/Update unit tests, and make appropriate fixes, to improve the skeleton's code coverage (_Thierry Draper_)
* 4b41224: Remove the pollyfilled PHP functions we no longer need / can use (_Thierry Draper_)

## Build 1027 - 06fa715 - 2020-06-02 - Improvement: Customisable SortTable Logic and Display
* 06fa715: Enable customisable SortTable sort logic and rank display (_Thierry Draper_)

## Build 1026 - 96010b1 - 2020-06-02 - CI: Local PHPUnit Dev Setup
* 96010b1: Add scripts to setup a local dev environment for working on PHPUnit tests (_Thierry Draper_)

## Build 1025 - 384791c - 2020-05-31 - Feature: DeBearORM - A Simplified ORM
* 384791c: Unit tests for the new DeBearORM (_Thierry Draper_)
* fb966a2: Split the symlinks to the Sports models between Eloquent and DeBearORMs (_Thierry Draper_)
* 6af3967: Add new DeBearORM wrappers for the Skeleton models (_Thierry Draper_)
* 3795db3: Create a new internal ORM for optimised model lookups (_Thierry Draper_)

## Build 1021 - bcd9903 - 2020-05-27 - Improvement: Sports Optimisations
* bcd9903: Add a callStatic wrapper to the base Eloquent model implementation (_Thierry Draper_)
* eec3120: Make corrupt Benchmark start/stop's non-fatal, rendering cause in the debug panel (_Thierry Draper_)
* e8c12b2: Add Blade directives for Benchmark start/stop (_Thierry Draper_)
* e8d3a6f: Automatically propagate the section code within the Sitemap/Navigation items (_Thierry Draper_)
* cccdd6f: Add a new test for an existing, but null, array index (_Thierry Draper_)

## Build 1016 - 193ddd0 - 2020-05-17 - Composer: May 2020
* (Auto-commits only)

## Build 1016 - ca72d4b - 2020-04-21 - Composer: April 2020
* (Auto-commits only)

## Build 1016 - 41b2dfb - 2020-04-15 - Hotfix: RefreshDatabase Skeleton CI Paths
* 41b2dfb: Fix how the RefreshDatabase class finds the skeleton files on the remote CI (_Thierry Draper_)

## Build 1015 - 9f2b9a9 - 2020-04-14 - Improvement: Unit Tests
* 9f2b9a9: Include a symlink to helpers for the My subdomain (_Thierry Draper_)
* c611959: Allow the domain of a unit test to be customised as part of a test assertion (_Thierry Draper_)

## Build 1013 - 3464daa - 2020-04-12 - Composer: Laravel 7
* 3464daa: Upgrade to Laravel 7, with the corresponding required changes (_Thierry Draper_)

## Build 1012 - 9a66ee1 - 2020-04-11 - Improvement: Weather Forecast syncing
* 9a66ee1: Improve how weather forecasts are defined and synchronised (_Thierry Draper_)

## Build 1011 - 0fca239 - 2020-04-07 - Improvement: Codification for URLs
* 0fca239: Add a standardised method for strings to be converted to URL-compatible codified versions (_Thierry Draper_)

## Build 1010 - e83c849 - 2020-04-07 - Improvement: Dynamic PHPUnit Setup Components
* e83c849: Standarise and split the PHPUnit setup script into dynamic components (_Thierry Draper_)

## Build 1009 - 9ebaf22 - 2020-04-06 - Hotfix: PHPUnit Error Handling
* 9ebaf22: Ensure the new PHPUnit run script errors on failure at the appropriate stage (_Thierry Draper_)

## Build 1008 - d070452 - 2020-04-04 - Composer: Replacing PHP Linter
* d070452: Replace a deprecated PHP Linting package with its replacement (_Thierry Draper_)

## Build 1007 - 4879d25 - 2020-04-04 - Improvement: PHPUnit Efficiency
* 4879d25: Split the PHPUnit run in to a more efficient process (_Thierry Draper_)

## Build 1006 - ae16f94 - 2020-04-04 - Hotfix: Fixes Flagged By Unit Tests
* ae16f94: Make changes to allow the php://input to be customisable per individual unit tests (_Thierry Draper_)
* 030ab43: Fix Dropdown Search lower casing so it runs after non-latin1 character conversion (_Thierry Draper_)
* 7e93a33: Make the RefreshDatabase part of the Feature unit tests optional (_Thierry Draper_)
* b2102fb: Widen the news source link column to match an ad hoc change to production (_Thierry Draper_)
* 09dac21: Interpolate meta descrips at source, rather than when custom linked (_Thierry Draper_)

## Build 1001 - 4bf799c - 2020-03-14 - Composer: March 2020
* (Auto-commits only)

## Build 1001 - 4087d56 - 2020-03-11 - Improvement: Dropdown Searching
* 4087d56: Fix the dropdown JS click handler when selecting a non-ID'd item (_Thierry Draper_)
* dae07e7: Add minor dropdown searching usability improvements (_Thierry Draper_)

## Build 999 - e0671f9 - 2020-02-22 - Hotfix: Blade Template Code Styling
* e0671f9: Fix syntactically valid, but accidental use of foreach with no braces (_Thierry Draper_)

## Build 998 - cfdb59b - 2020-02-16 - Improvement: Advanced Dropdown Options
* cfdb59b: Allow dropdown options from being searchable (in non-mobile views) if required (_Thierry Draper_)
* 2760802: Enablable / Disablable dropdowns (_Thierry Draper_)

## Build 996 - d823d12 - 2020-02-14 - Composer: February 2020
* (Auto-commits only)

## Build 996 - e74feb9 - 2020-02-13 - Hotfix: Mobile and Table Social Footer Links
* e74feb9: Fix how the (now enabled-by-default) social media links appear in mobile/tablet views (_Thierry Draper_)

## Build 995 - ca1a4ca - 2020-02-11 - Improvement: Reusable JavaScript Setup Handlers
* ca1a4ca: Add a new page layout template that renders the main content only, for when an @extends must be called (_Thierry Draper_)
* a1072ff: Move the ScrollTable and SubNav setup logic to a re-usable method (triggered post re-draw) (_Thierry Draper_)

## Build 993 - 8469163 - 2020-02-10 - Improvement: System Status Footer Link
* 8469163: Have the default page Twitter logo/link be the support account (_Thierry Draper_)
* c934c5d: Add an off-site footer link to the status sub-site on all pages (_Thierry Draper_)

## Build 991 - 7f7dfa9 - 2020-02-06 - Improvement: Sitemap Controllers
* 7f7dfa9: Move the XML sitemap logic to a trait (_Thierry Draper_)
* 1812348: Sitemap toggling fixes to better handle more than two level deep links (_Thierry Draper_)
* bfbd31f: Make the Sitemap page title section compatible automatically (_Thierry Draper_)
* 76fccde: Standardise URL building between Sitemap and Navigation templates (_Thierry Draper_)
* 2faa16b: Sitemap/Navigation nesting fix (_Thierry Draper_)
* 898f7ed: Move the core of the Sitemap production to a trait we can use in other areas (_Thierry Draper_)
* 1769f91: Another sitemap simplification, as the concept of 'section' sitemaps is dealt with within config (_Thierry Draper_)

## Build 984 - dded1b6 - 2020-02-03 - Improvement: Simplifying Sitemap Configuration
* dded1b6: Simplifying sitemap configuration, given we're duplicating config amongst nav and sitemap (_Thierry Draper_)

## Build 983 - 13b2d30 - 2020-02-02 - Hotfix: Benchmark Load Order
* 13b2d30: Removal of default Laravel-autoloaded but unused providers and middleware (_Thierry Draper_)
* 95f92dc: Fix the loading of the new internal benchmarking module (_Thierry Draper_)

## Build 981 - 2cde1d0 - 2020-02-01 - Improvement: Non-intrusive Benchmark Module
* 2cde1d0: Remove the addition of policies within benchmark/logging config definitions (due to load-order mismatch) (_Thierry Draper_)
* 353183c: Allow local environment config to be defined and loaded outside of git (_Thierry Draper_)
* 3a4d290: Allow the Query debug panel (via internal database logging) from being accessible on live sites (_Thierry Draper_)
* d325e28: Re-work the internal benchmarking process using usable homebrew logic, and include as a potential debug widget (_Thierry Draper_)
* 08d1f82: Add, for testing purposes, a simple blank Blade template with our page chrome (_Thierry Draper_)

## Build 976 - 2557888 - 2020-01-23 - SysAdmin: Remove Session Hits
* 2557888: Remove the individual session hits table, which we're not really using and takes up lots of space (_Thierry Draper_)

## Build 975 - 5d551eb - 2020-01-21 - Hotfix: Twitter Text-to-Blob Migrations
* 5d551eb: Cheeky re-write of the TWITTER text-to-blob conversions, as the proper migration way errors (_Thierry Draper_)

## Build 974 - 94ab89c - 2020-01-20 - Feature: Twitter Direct Messages
* 94ab89c: Schema for sending Direct Messages through Twitter (with minor corresponding Tweet optimisation too) (_Thierry Draper_)

## Build 973 - 565e797 - 2020-01-18 - Hotfix: Laravel Logging Format
* 565e797: Ask Laravel to write its logs with a date suitable to our archiving process (_Thierry Draper_)

## Build 972 - 7eadeac - 2020-01-17 - Feature: Tweet Templates
* 7eadeac: Twitter database schema tweaks, including a new 'template' table (similar to the email templates), and some minor Eloquent refinements in other migrations (_Thierry Draper_)

## Build 971 - 007cddb - 2020-01-17 - Hotfix: OpenGraph URLs
* 007cddb: Explicitly add the scheme to the OpenGraph URL, as the Twitter cards don't seem to like the implicit '//domain' (_Thierry Draper_)

## Build 970 - 06b4e64 - 2020-01-15 - Hotfix: PHPUnit Database Refresh
* 06b4e64: Fix the PHPUnit database refresh so it pulls the source data from the correct repo's vendor/ folder (_Thierry Draper_)

## Build 969 - 5ff0b3c - 2020-01-12 - Composer: January 2020
* (Auto-commits only)

## Build 969 - d5937c8 - 2020-01-10 - Improvement: Timezone Handling
* d5937c8: SysAdmin: Minor tidying / clarification / house-keeping (_Thierry Draper_)
* aec360e: Re-jig the way our timezone support is handled, and include shortcut getters to database and user times (_Thierry Draper_)

## Build 967 - 1ad44d0 - 2020-01-08 - Hotfix: Arrays::merge() Handle Native array_merge()
* 1ad44d0: Handle native array_merge() within our special Arrays::merge() method (_Thierry Draper_)

## Build 966 - 7e16d97 - 2020-01-05 - Hotfix: Cookie Usage Page Copy
* 7e16d97: Allow CMS content to include interpolatable variables (_Thierry Draper_)
* 9eeb487: Move the Cookie Usage content to the CMS (allowing the site to be passed explicitly, as this content is site-wide) (_Thierry Draper_)
* 546e69e: Cookie usage page copy improvements (_Thierry Draper_)
* 4a76086: The auto footer height sizing is not compatible with modal pages (where there is no footer) (_Thierry Draper_)
* e2303d1: Fix the modal container z-index, allowing it to appear above the site footer (_Thierry Draper_)

## Build 961 - 254e79a - 2020-01-01 - Improvement: Aliasable sub-sites
* 254e79a: New subsite: status.debear (for platform service status) (_Thierry Draper_)
* 7320957: Allow subsites to have aliases, so we can have slightly different versions within similar codebases (_Thierry Draper_)

## Build 959 - 8fcd24e - 2019-12-27 - Hotfix: Mobile Footer Height
* 8fcd24e: Ensure the content footer padding on mobile matches the actual footer height (_Thierry Draper_)

## Build 958 - bf5c628 - 2019-12-25 - Hotfix: PHPUnit CI Job Shell Errors
* bf5c628: Fix tput/clear errors during the PHPUnit CI job (_Thierry Draper_)

## Build 957 - 38b737e - 2019-12-23 - Hotfix: Scroller Responsiveness
* 38b737e: Improve the responsive handling of the scroller setup and changes to the viewport (_Thierry Draper_)

## Build 956 - 70a9e19 - 2019-12-18 - Improvement: ScrollTable Shading
* 70a9e19: Add shading to ScrollTable content that has additional content available (but hidden) (_Thierry Draper_)

## Build 955 - 0f4cb0c - 2019-12-15 - Composer: December 2019
* (Auto-commits only)

## Build 955 - 7fd1b85 - 2019-12-13 - Hotfix: ScrollTable JS Fixes
* 7fd1b85: Minor ScrollTable JS fixes, in particular around embedding CSP-unfriendly style attributes in to the markup (_Thierry Draper_)

## Build 954 - e86c42d - 2019-12-13 - Feature: Client-side Number-to-Bytes Converter
* e86c42d: Number-to-Bytes JS linting fix (_Thierry Draper_)
* 149ba3b: Client-side version of the number-to-bytes converter (_Thierry Draper_)

## Build 952 - 2dd3e24 - 2019-12-13 - Improvement: SubNav Tabs Below Content
* 2dd3e24: Add the ability to render subnav tabs below content (_Thierry Draper_)

## Build 951 - d5e246a - 2019-12-10 - Improvement: ScrollTable Generalisations
* d5e246a: Move the mobile-view handling of scrolltable column switching out of the sorttable widget and back in to the scrolltable (_Thierry Draper_)
* c833568: Add the concept of 'summary' columns to the scrolltable, which live to the right table (but are actually part of the main column) (_Thierry Draper_)
* 170b5ee: Enforce the scrolltable top row's bottom margin as a standard part of the widget, not in the implementation (_Thierry Draper_)
* 8d310b3: Allow the mobile-only scrolltable dropdown arrow be optional (e.g., when only a single column/option is available) (_Thierry Draper_)

## Build 947 - 430bbaf - 2019-12-09 - SysAdmin: Widen email app column width
* 430bbaf: Widen the width of the email app column to better account for its usage (_Thierry Draper_)
* eb30254: Group all the database migrations into a file-per-section, rather than file-per-table (_Thierry Draper_)

## Build 945 - 00b2359 - 2019-12-01 - Feature: Mobile Select
* 00b2359: Generic styling for a mobile styled select box with native options (_Thierry Draper_)

## Build 944 - a7d40b2 - 2019-11-30 - Feature: JavaScript Table Sorting
* a7d40b2: Generic table sorting logic JavaScript widget (_Thierry Draper_)

## Build 943 - 14a77e5 - 2019-11-29 - Feature: Scrollable Table widget
* 14a77e5: A new widget based on the Motorsports standings for scrollable table handling (_Thierry Draper_)

## Build 942 - 8339f20 - 2019-11-20 - SysAdmin: Prune Dead Symlinks
* 8339f20: Prune dead symlinks added for future proofing in favour of an add-when-required approach (_Thierry Draper_)

## Build 941 - 3d3809d - 2019-11-19 - CI: PHP Comments Standards
* 3d3809d: Fall-out from the new PHP comments standards being enforced (_Thierry Draper_)
* b0fc337: Extend our PSR-12 standards check to include the format of comments (_Thierry Draper_)

## Build 939 - ecb53eb - 2019-11-16 - Composer: November 2019
* (Auto-commits only)

## Build 939 - 37fc9c2 - 2019-11-14 - CI: YAML Fixes and Improvements
* 37fc9c2: CI YAML fixes and improvements, such as moving the PHP/MySQL versions used to GitLab (_Thierry Draper_)

## Build 938 - a20cd15 - 2019-11-10 - Improvement: Native Mobile Dropdowns
* a20cd15: Programmatically convert the custom dropdowns to native <select's in mobile view (with their better native handling) (_Thierry Draper_)
* f81f86e: Determine the viewport size and add to the body tag as a class (like we have section, os, etc) (_Thierry Draper_)

## Build 936 - c2ac034 - 2019-11-08 - Improvements: Dropdown Handling
* c2ac034: Fix the JavaScript dropdown processing scenario where the selected value wasn't actually changed (and so no processing should be performed) (_Thierry Draper_)
* 9301708: Allow setting of a dropdown in JavaScript via the required option's ID, rather than entire <li object (_Thierry Draper_)
* a106f4c: Simplify dropdown option definitions to just be an appropriate label (_Thierry Draper_)

## Build 933 - 259cedd - 2019-11-07 - Improvement: Input JavaScript Array Helpers
* 259cedd: Some additional Input JavaScript array helpers (_Thierry Draper_)

## Build 932 - 05298ee - 2019-11-06 - Improvement: JavaScript SubNav events
* 05298ee: Throw some JavaScript events when about-to and have-just switched SunNav tabs (_Thierry Draper_)

## Build 931 - 3eccd2c - 2019-11-05 - Composer: Laravel 6 Upgrade
* 3eccd2c: Updated parent method declaration within feature test updated in Laravel 6 (_Thierry Draper_)
* 2f81450: Explicitly state the non-integer primary key types within models (_Thierry Draper_)
* 338fd95: Switch from using the Input:: facade to Request::, as it was removed in Laravel 6 (_Thierry Draper_)
* b5744d9: Upgrade Laravel from 5.8 to 6 (which also updated other libs too) (_Thierry Draper_)

## Build 927 - d738729 - 2019-10-23 - Composer: October 2019
* d738729: PSR-12 whitespace fixes (_Thierry Draper_)

## Build 926 - 4f5e8a1 - 2019-10-20 - Hotfix: Default Dropdown Item
* 4f5e8a1: Default dropdown options need to be built as an instance of our new DropdownItem class (_Thierry Draper_)

## Build 925 - 4bb74a6 - 2019-10-19 - Hotifx: CSS Standards CI Stage
* 4bb74a6: Fix the CSS Standards CI job so it runs in the correct group (_Thierry Draper_)

## Build 924 - 5b3e53b - 2019-10-16 - Improvement: CSS Linting
* 5b3e53b: CSS standards tweaks, plus some modifications to the ruleset having seen them in action (_Thierry Draper_)
* 35f6312: CSS linting tweaks, plus some modifications to the ruleset having seen them in action (_Thierry Draper_)
* 50484a8: Switch the CSS Linting test to use stylelint, and split in to both linting and standards (_Thierry Draper_)

## Build 921 - 335ce1e - 2019-10-11 - Hotfix: SubNav Non-Items
* 335ce1e: Fix the SubNav click handler so it only processes when a sub nav item was clicked (_Thierry Draper_)

## Build 920 - af019de - 2019-10-10 - Improvement: Dropdown Widget
* af019de: De-couple the dropdown widget from the Forms module, and allow richer content in the items (_Thierry Draper_)

## Build 919 - 759d8e9 - 2019-10-09 - Hotfix: Navigation 100% Width
* 759d8e9: Fix how we make the navigation 100% wide to help against browser rounding errors (_Thierry Draper_)

## Build 918 - fb5b281 - 2019-09-30 - Hotfix: Form Auto Login
* fb5b281: Form module config lost by session regeneration when logging via a Forms 'login' action (_Thierry Draper_)

## Build 917 - c7fa878 - 2019-09-27 - SysAdmin: PHP 7.3 on Production
* c7fa878: Switch to PHP 7.3 on production servers (_Thierry Draper_)

## Build 916 - f2f32fc - 2019-09-27 - Hotfix: Handling Cookie Prefs over HTTP/1.0
* f2f32fc: Fix how we parse the preferences cookie if we've failed to load it properly (_Thierry Draper_)

## Build 915 - f2c9be2 - 2019-09-27 - Hotfix: Form Validation Min/Max
* f2c9be2: There should be no (set) default min/max in form validation, unless explicitly defined within the widget (_Thierry Draper_)

## Build 914 - 9551ce8 - 2019-09-26 - Hotfix: SubNav Unknown Fragment Identifier Parsing
* 9551ce8: Fix the new SubNav tab loading widget from failing when an unknown fragment identifier is passed (_Thierry Draper_)

## Build 913 - a3f1a54 - 2019-09-25 - Composer: September 2019
* (Auto-commits only)

## Build 913 - c95c994 - 2019-09-14 - CI: Improve Code Coverage
* c95c994: Fix the BrowsCap internal hash test, which needed a salt added to the unit test env vars (_Thierry Draper_)
* ba1bf65: Re-jig the Unit test layouts to match the filesystem layout of the code we're testing (_Thierry Draper_)
* 14b7ba8: Unit tests (and corresponding tweaks / fixes) for the remaining helper classes (_Thierry Draper_)
* 8ef3eba: Unit Tests and corresponding fixes to get the code coverage reports up to 100% for those Helper modules currently being referenced (_Thierry Draper_)

## Build 909 - 6442f1c - 2019-09-04 - Improvement: Split Config Files
* 6442f1c: Remove the Polish GeoIP tests, as they're too unreliable (_Thierry Draper_)
* 21ee5a5: Fix the order of PHPUnit string assertion, so any failures are labelled the correct way round (_Thierry Draper_)
* 06c8e50: One of our GeoIP unit tests for Poland has reverted from Krakow to Warsaw (_Thierry Draper_)
* ffca12c: Re-jig the DeBear config, so it's in broken down (i.e., smaller) sub-files rather than one a single ever-growing array (_Thierry Draper_)

## Build 905 - 63ab044 - 2019-09-02 - CI: Fix 'Clean' Local Runs
* 63ab044: Fix the 'clean' running of our CI locally (_Thierry Draper_)

## Build 904 - b5659c5 - 2019-08-31 - Composer: UTF-8 Parsing Library
* b5659c5: Add to a composer a library for manipulating UTF-8 strings / chars (_Thierry Draper_)

## Build 903 - 4070783 - 2019-08-30 - Feature: SubNav Widget
* 4070783: New array-based SubNav widget (which is a bit less integrated than the main nav) (_Thierry Draper_)

## Build 902 - 66dee51 - 2019-08-29 - Improvement: CSS Reset Tweaks
* 66dee51: Minor CSS empty responsive definition removal (_Thierry Draper_)
* df8382a: Reset heading CSS styles available by class as well as automatically by tag (_Thierry Draper_)

## Build 900 - ad59449 - 2019-08-28 - Hotfix: Desktop-only CSS Helpers
* ad59449: Desktop-only CSS helpers (rather than desktop-and-disable-per-size) (_Thierry Draper_)

## Build 899 - bac2ce0 - 2019-08-27 - Composer: August 2019
* bac2ce0: Fall-out from recent PHPMD to standards (_Thierry Draper_)

## Build 898 - ef246b2 - 2019-08-26 - Feature: CDN URL Builder
* ef246b2: CDN URL builder, in the similar manner to the Static URL builder (porting the legacy logic across) (_Thierry Draper_)

## Build 897 - 312d0ec - 2019-08-25 - Improvement: Word Pluraliser Options
* 312d0ec: Allow the word pluraliser to prepend or hide the number, rather than always appending (_Thierry Draper_)

## Build 896 - ee4618a - 2019-08-24 - Improvement: User's Name As Accessor
* ee4618a: Switch the user's name concatenator to an accessor (_Thierry Draper_)

## Build 895 - 55cb084 - 2019-08-23 - Hotfix: Test Analytics Urchin Over-writing
* 55cb084: Make the over-writing of the test Analytics urchin less heavy-handed (_Thierry Draper_)

## Build 894 - 2c9f405 - 2019-08-21 - Feature: Format Number Ranges
* 2c9f405: Number range grouping and formatting methods (_Thierry Draper_)

## Build 893 - 21bd75d - 2019-08-20 - Hotfix: Resource Policy Checks
* 21bd75d: Fix how resource policy checks are passed to the checking method (_Thierry Draper_)

## Build 892 - ad9acca - 2019-08-19 - Improvement: Load Font Awesome via Kits
* ad9acca: Move away from versioned FontAwesome resources to their new 'kits' (_Thierry Draper_)

## Build 891 - 912da7e - 2019-08-18 - Hotfix: Prevent CI Clobbering Live Databases
* 912da7e: Ensure we have a valid database prefix when attempting a CI database refresh (i.e., don't clobber live databases!) (_Thierry Draper_)

## Build 890 - 707b066 - 2019-08-17 - SysAdmin: Re-jig layout templates
* 707b066: Re-jig the Layout blade templates (_Thierry Draper_)

## Build 889 - 53a13b4 - 2019-08-13 - CI: Standardise GeoIP Unit Tests
* 53a13b4: Move GeoIP test parameters to config for standarisation with the SysMon unit tests (_Thierry Draper_)

## Build 888 - 81e66e1 - 2019-08-12 - Hotfix: Handle Missing Merging CSS/JS
* 81e66e1: Better handle missing resources when merging CSS/JS (_Thierry Draper_)

## Build 887 - f79d133 - 2019-08-11 - Improvement: Componentise Navigation URL
* f79d133: Componentise the navigation URL into various components, to be built up at run-time (_Thierry Draper_)

## Build 886 - a7ba841 - 2019-08-09 - CI: Allow Disabling Assumed HTTPS
* a7ba841: Allow the ability to assume an HTTPS request was made during unit tests (_Thierry Draper_)

## Build 885 - 817595c - 2019-08-09 - CD: Move Server Details to Env Vars
* 817595c: Minor GeoIP unit test tweak given latest database update (_Thierry Draper_)
* 9cbb4f1: Move the deploy server access details to environment variables (_Thierry Draper_)

## Build 883 - 1e5b323 - 2019-08-06 - Sysadmin: Duration Logging Improvements
* 1e5b323: Change the way the Logging duration calcs are made (as it was wrong before), and render DB queries as %age of total time (_Thierry Draper_)
* 064e93f: Parse argument list in the database logger to also analyse the unique ways in which a query was used (_Thierry Draper_)

## Build 881 - 379d386 - 2019-08-02 - CI: Disable Database Logging
* 379d386: Disable database logging during CI runs as it's not needed (_Thierry Draper_)

## Build 880 - 43ccc6e - 2019-08-01 - Hotfix: Missing Grid Prep Classes
* 43ccc6e: Include general grid setup classes within the grid CSS (_Thierry Draper_)

## Build 879 - 17d662b - 2019-07-30 - SysAdmin: Database Query logging
* 17d662b: Fix Debug panel overflow scrollbars on non-Mac browsers (_Thierry Draper_)
* b73fa79: Rename the Database logging class to reflect the fact it's actually a Logging class... (_Thierry Draper_)
* 3bb248f: Include an option (enabled by default for dev) for a database queries debug panel (_Thierry Draper_)
* f2b0532: Database logger to have the option for per (unique) query reporting (_Thierry Draper_)
* 5f6e479: Database query logging report (_Thierry Draper_)

## Build 874 - 7e2dfbd - 2019-07-29 - Hotfix: Access Cookie Preferences over HTTP
* 7e2dfbd: Fix how the cookie preferences are loaded over HTTP requests, when the cookie isn't accessible (because it's not secure) (_Thierry Draper_)

## Build 873 - 676599f - 2019-07-27 - Hotfix: Decouple the .box CSS class from <div
* 676599f: Decouple the .box CSS class from applying specifically to <div (_Thierry Draper_)

## Build 872 - b288a88 - 2019-07-26 - Feature: Respondable CSS Helpers
* b288a88: Make some of the CSS helpers controlable by responsive size (_Thierry Draper_)

## Build 871 - 31c4528 - 2019-07-25 - Hotfix: Disable client-side warnings on dev
* 31c4528: Disable client-side error reports on dev servers (_Thierry Draper_)

## Build 870 - dcc30d4 - 2019-07-21 - Composer: July 2019
* (Auto-commits only)

## Build 870 - f80cef4 - 2019-07-18 - Security: UA Session Salt In Config
* f80cef4: Move glaring session user agent salt security hole from config to env (_Thierry Draper_)

## Build 869 - 16d1f96 - 2019-07-17 - Feature: Monitor PHP Code Coverage
* 16d1f96: Missing a CI job dependency (_Thierry Draper_)
* 656b376: Enable PHP code coverage report during CI (_Thierry Draper_)

## Build 867 - 9649a80 - 2019-07-11 - Hotfix: Cached Config PHP Artisan Errors
* 9649a80: Cached config builder script needs to check the `php artisan` return code for errors too (_Thierry Draper_)

## Build 866 - c86ab78 - 2019-07-10 - Hotfix: Skip Deploy in LocalCI
* c86ab78: The new deploy stage should not be included in our localci run script (which also requires the script: rule to be last) (_Thierry Draper_)

## Build 865 - b3d4f58 - 2019-07-07 - Hotfix: CMS Exception Handling
* b3d4f58: BugReporting debug trace parsing fix from Homebrew-to-Laravel migration (_Thierry Draper_)
* 7926bcd: Silently fail missing CMS content on production, only throwing Exceptions in development (_Thierry Draper_)

## Build 863 - 74d38ac - 2019-07-06 - Feature: No Pagination Link
* 74d38ac: Allow the pagination widget from processing 'no links' (so all dealt with in external JS handler) (_Thierry Draper_)

## Build 862 - cd33df1 - 2019-07-05 - Feature: Enable Continuous Delivery
* cd33df1: Envoy script for implementing our Continuous Delivery (_Thierry Draper_)

## Build 861 - 39ed2e7 - 2019-07-03 - Feature: Optimise DNS Load Times
* 39ed2e7: Add some DNS-related HTML meta tags to try and optimise load times (_Thierry Draper_)

## Build 860 - c763f07 - 2019-07-02 - Feature: Scroller Widget
* c763f07: A scroller widget to automatically (and pleasently) switch between sub-instances (_Thierry Draper_)

## Build 859 - 1ff1c22 - 2019-07-01 - Feature: News models and migrations
* 1ff1c22: Models and Migrations for the RSS feed news tables (_Thierry Draper_)

## Build 858 - 256a8eb - 2019-06-25 - Sysadmin: Tidy Laravel deploy
* 256a8eb: Remove default Laravel deploy Blade template we no longer use (_Thierry Draper_)

## Build 857 - f6f8a94 - 2019-06-23 - Hotfix: ConvertTimezone class
* f6f8a94: ConvertTimezoe class fixes, around namespaces and missing PascalCase'ing (_Thierry Draper_)

## Build 856 - 7497f66 - 2019-06-22 - Feature: Benchmark module
* 7497f66: Wrapper to a composer Benchmarking module for us to analyse performance (_Thierry Draper_)

## Build 855 - bca4b07 - 2019-06-21 - Hotfix: Disable Whoops CSP
* bca4b07: Disable CSPs when error triggers the Whoops page (which does not meet our requirements) (_Thierry Draper_)

## Build 854 - 1ff8ab2 - 2019-06-20 - Feature: Singularise String
* 1ff8ab2: Basic helper for singularising a string (_Thierry Draper_)

## Build 853 - 05d994b - 2019-06-18 - CI: PHPUnit vendor simplification
* 05d994b: May need to create the base CPAN CI dir (_Thierry Draper_)
* 62ba8c2: Fix, and where appropriate merge, vendor folder creation within the CI (_Thierry Draper_)
* a138536: Include some visual notifications when fixing skeleton log dirs for CI (_Thierry Draper_)
* a2232c1: Ensure composer has a vendor folder to be installed into (_Thierry Draper_)
* 34e691c: Propagate the CI_FAUX flag when running CI tests locally, but from clean (_Thierry Draper_)

## Build 848 - 3b5d736 - 2019-06-16 - Feature: Enforce CSPs
* 3b5d736: Switch from passive "report only" CSP rules to full enforced CSP (_Thierry Draper_)

## Build 847 - c1235b7 - 2019-06-10 - Feature: Handle sub-sites
* c1235b7: Ability to create Fantasy/Sports site specific helper classes within the DeBear\Helpers namespace (_Thierry Draper_)
* 5a2186d: New Exception class to handled deprecated functionality within sites (_Thierry Draper_)
* 327edb3: Flag CSS fix used in the Sports and My sites, where their vertical-align is different from the icon list, but have been grouped together (_Thierry Draper_)
* 9a22844: Allow the "is admin" rules to be determined per sub-site, rather than just at the site level (_Thierry Draper_)
* f9999f0: Allow a customisable Blade template be loaded per (sub)site between the logo and nav bar (_Thierry Draper_)
* 29e3302: Process sub-site config as part of the middleware processing to ensure it is loaded (and customising) automatically (_Thierry Draper_)
* 0ca6b1a: Fix dodgy PHP in the Blade templates, where inline if's and loops did not have the curly braces (_Thierry Draper_)

## Build 840 - 896cb8c - 2019-06-09 - SysAdmin: Sprites to CDN
* 896cb8c: Move the sprites to the CDN (_Thierry Draper_)

## Build 839 - 93ae72d - 2019-06-08 - CI: Handle Perl Upgrades
* 93ae72d: Handle changing versions of perl in the CI (_Thierry Draper_)

## Build 838 - 87aae47 - 2019-06-07 - CI: Data Download Errors
* 87aae47: When downloading test CI data return an error if the download fails, rather than assume it was a 204/skip (_Thierry Draper_)

## Build 837 - be162a6 - 2019-06-06 - Hotfix: Missing REQUEST_SCHEME server variable
* be162a6: Ensure REQUEST_SCHEME is part of $_SERVER, as it appears it is not always sent (mostly in dodgy, edge cases) (_Thierry Draper_)

## Build 836 - a995d68 - 2019-06-05 - Hotfix: Domain fallback
* a995d68: Treat sites accessed via an "unknown" URL (such as mail.) as www (_Thierry Draper_)

## Build 835 - bab119e - 2019-06-04 - Hotfix: Policies setup before User object setup
* bab119e: Fixed a policies setup error when an error occurred before the User object was fully setup (_Thierry Draper_)

## Build 834 - 5885f40 - 2019-05-28 - CI: Security Header unit tests
* 5885f40: Unit tests to ensure security headers are sent, given earlier issue (_Thierry Draper_)

## Build 833 - 1b28664 - 2019-05-22 - Hotfix: Highcharts CSP false-positives
* 1b28664: Include a custom Highcharts JS file for tweaks to the library (rather than being diff'd in as we did before) (_Thierry Draper_)

## Build 832 - aa27641 - 2019-05-22 - Feature: Implement Feature-Policy header
* aa27641: Implement a new security header, Feature-Policy (_Thierry Draper_)

## Build 831 - e458c74 - 2019-05-22 - Hotfix: CSP headers never sending
* e458c74: CSP headers weren't being sent, even though enabled in config, due to handling a possible exception (_Thierry Draper_)

## Build 830 - 2e1a6b0 - 2019-05-19 - Composer: May 2019 updates
* 2e1a6b0: Fix a line wrapping error in FontAwesome's <link tag (_Thierry Draper_)

## Build 829 - 12224b1 - 2019-05-17 - Hotfix: Security Headers not sent
* 12224b1: Security Headers weren't being sent because of silly absolute namespace check (_Thierry Draper_)

## Build 828 - db2a801 - 2019-05-15 - Hotfix: Social Media setup
* db2a801: Social Media setup refinements now we're using the mechanism on a site (_Thierry Draper_)

## Build 827 - 468e548 - 2019-05-02 - Migrations: Merge w/Stored Procedures
* 468e548: Re-organise the database migrations layouts to keep all sub-grouped migrations/stored procs together (_Thierry Draper_)

## Build 826 - 796a5d2 - 2019-04-24 - Hotfix: Flexible User Agent checks
* 796a5d2: Target specific components of the user agent, rather than the whole string, when determining if a session's user agent has changed (i.e., suspicious activity check) (_Thierry Draper_)

## Build 825 - 2c081f1 - 2019-04-24 - Migrations: Connection-appropriate DROP TABLES
* 2c081f1: Migration "drop tables" needs appropriate Laravel connection applied (_Thierry Draper_)

## Build 824 - 3085b5e - 2019-04-22 - CI: Testing Migrations
* 3085b5e: Include running the database migrations properly as part of the MySQL Linting CI job (plus testing their rollbacks) (_Thierry Draper_)
* 006c02f: Minor "I'm artisan" check clarification (_Thierry Draper_)

## Build 822 - a5fd8b4 - 2019-04-16 - Composer: April 2019 updates
* a5fd8b4: Some local 'clean' CI test fixes and code optimisations (_Thierry Draper_)
* eb0b1c4: Fix how the fix-logs script is called as part of the PHPUnit tests (_Thierry Draper_)
* d9ca13c: Minor local-testing fix to fix the logs even in our 'clean' local run (_Thierry Draper_)
* 7bed78e: Include a third-party library we need to handle ENUMs within Doctrine (_Thierry Draper_)

## Build 818 - c8f2661 - 2019-04-09 - CI: Correct Logging Path
* c8f2661: Fixed the git clone's path-to-skeleton (_Thierry Draper_)

## Build 817 - a0555eb - 2019-04-09 - CI: Fix Logging
* a0555eb: Automatically fix the logs when cloning the skeleton within CI (_Thierry Draper_)
* 234d8ef: Script (for re-use by other repos) the way we fix logs within CI (_Thierry Draper_)

## Build 815 - 4c79311 - 2019-04-08 - CI: Check Migrations
* 4c79311: Add the migrations to our CI, including a PSR-12 workaround for Laravel's migrations (who do not follow PSR-4's fully qualified class name) (_Thierry Draper_)
* 75b2142: Re-jig how we run Laravel's migrations script should we sub-filter our migrations (_Thierry Draper_)

## Build 813 - 2c98ec9 - 2019-03-29 - CI: Exclude Tags
* 2c98ec9: YAML needed a less-global definition (_Thierry Draper_)
* 99b404b: Prevent CI when pushing tags (_Thierry Draper_)

## Build 811 - 2eb1a17 - 2019-03-26 - CI: Re-grouped Git remote
* 2eb1a17: Reflect re-grouping of the git remotes in our CI scripts (_Thierry Draper_)

## Build 810 - 7fa78a4 - 2019-03-26 - CI: GeoIP Standardisation
* 7fa78a4: Fix some (potentially plausible) variable GeoIP test locations (_Thierry Draper_)
* 3c764b4: Updated PHPUnit GeoIP tests to use (hopefully) more stable localised IP addresses (_Thierry Draper_)

## Build 808 - 24fd029 - 2019-03-22 - CI: MySQL Linter
* 24fd029: Minor changes as part of importing a MySQL linter (_Thierry Draper_)
* 6cce14b: Start using a singular composer cache to try and cut down on CI pipeline build time (_Thierry Draper_)

## Build 806 - 8eb021c - 2019-03-06 - Feature: Basic CMS
* 8eb021c: Revise the way test databases are created, and include some post-test cleanup of them (_Thierry Draper_)
* ca25f80: Basic CMS module, dealing with content for sections rather than whole pages for now (_Thierry Draper_)

## Build 804 - 24eba5d - 2019-03-05 - Hotfix: Post Launch
* 24eba5d: Widen the password hash we store from 12 to 30, with an upgrade pattern for current users (_Thierry Draper_)
* d5681ac: CI refinements to the Composer vendor caching rules and include a new Perl-linting process (_Thierry Draper_)
* 08fb500: Make the CI compatible with the symlinked and centralised (on production) storage directories (_Thierry Draper_)
* 51ec0de: Move the relevant Laravel storage dirs to outside the repo, into the area we rotate and prune using our existing scripts (_Thierry Draper_)
* 177ea80: Test script tweaks to attempt repo-name parity between repo and skeleton (_Thierry Draper_)
* 3f77a32: Ensure $_SERVER['SERVER_NAME'] exists - domain without the port (_Thierry Draper_)
* 404cac0: Enable the (appropriate) login modal to be triggered from outside that block (e.g., an in-page link) (_Thierry Draper_)
* 16ca993: Revert previous commit, a failed experiment (_Thierry Draper_)
* 88113f7: GeoIP unit test tweaks for our SysMon report_uri pre-process script (_Thierry Draper_)
* 5475ba8: Fix our newly restored logging script... so it doesn't try to write to disk during unit tests (as structure probably won't exist) (_Thierry Draper_)
* a8a37e3: Ensure /-compatible branch names can be pulled out for our test scripts (_Thierry Draper_)
* 2e148c5: Artisan-compatible sub-domain determination fix for the 'migrate' command (e.g., unit tests) (_Thierry Draper_)
* b6318ca: Analytics JS name clash fix (_Thierry Draper_)
* 8500af3: Revert previous decision to not link to the homepage from the Terms/Privacy page logo (_Thierry Draper_)
* 594096f: Fix logging the User ID of the user logging in in the appropriate column (_Thierry Draper_)
* 0cf6f7f: Fix the logging module (_Thierry Draper_)
* c194790: Fix incorrect $_SERVER variable name used on production (_Thierry Draper_)
* 25e7502: Framework changes required to get running on production (_Thierry Draper_)
* 3f5f3d6: Minor server config from running on production server (_Thierry Draper_)
* 43b720f: Cope with cache-based config that we'd want to run with on production (_Thierry Draper_)

## Build 784 - debe4b2 - 2019-02-17 - Launching Laravel
* debe4b2: Git tidy pre merge of Laravel branch (_Thierry Draper_)
* ad5510e: Use a repo-compatible mechanism for the version/revision info (including date) (_Thierry Draper_)
* 509823e: Sitemap XML file fixes (_Thierry Draper_)
* 51816fa: Database Refreshing script needed a slighty more generic vendor dir identification for GitLab (_Thierry Draper_)
* d24c28d: Fixed the 301/302 -> 307/308 conversion, as 307/308 definitions are the inverse of 301/302 (_Thierry Draper_)
* 285752e: Integrity test on the MySQL test seeding script - must have a prefix, otherwise we'll be nuking the prod/dev databases! (_Thierry Draper_)
* 94c507e: Manual RefreshDatabase mechanism, because out-of-the-box Laravel version doesn't work with our setup (_Thierry Draper_)
* 2ebc41f: Standardise the way Font Awesome fonts are loaded, and remove the old local copy of the fonts (_Thierry Draper_)
* 4c678ea: Hide the registration link in the 'sc1' error message when reg is disabled (_Thierry Draper_)
* c1add43: Unit tests for the new Format::bytes method (_Thierry Draper_)
* cdb6b0f: Add data attribute to pagination link to help JS handling (_Thierry Draper_)
* 3d1f08c: Highcharts functionality (plus per-site exceptions, that was missing in the original build) (_Thierry Draper_)
* e7808dc: Missing onclick target class on pagination (_Thierry Draper_)
* 6c47775: Slightly split out some Strings:: helper methods into a more suitable Format:: helper class (_Thierry Draper_)
* 887dca3: Pagination fixes to ensure every link includes a catchable 'onclick_target' class (_Thierry Draper_)
* 6a2f213: Fix how policy/test based resources are defined to allow site config to overlay properly (_Thierry Draper_)
* 3917acc: Database migration fixes from legacy copy (_Thierry Draper_)
* ddaffb9: Migration column naming fix (_Thierry Draper_)
* d3b5ab6: Switch test database creation to use the migrations, rather than synced test data (_Thierry Draper_)
* 505724f: Add a pruning phase to the test data checks (_Thierry Draper_)
* bc8156f: Migrations for the Skeleton (_Thierry Draper_)
* f36d905: Regular Composer update (_Thierry Draper_)
* 7c299b7: Various (mostly Forms) fixes from the My repo unit tests (_Thierry Draper_)
* 39e236d: Need to track the HTTP Request Method in our Unit Tests (_Thierry Draper_)
* e0b2795: Git CI test script calculator fix to allow https:// repos used in CI (_Thierry Draper_)
* 01af7eb: Include all database env vars in our MySQL unit tests (_Thierry Draper_)
* b9002d6: Fix how the super-globals are parsed for our unit tests (_Thierry Draper_)
* ab0b90d: Tweak the Clean CI script to also copy the skeleton, if we're testing a sub-site (_Thierry Draper_)
* 334061e: A local script for emulating the CI from a clean repo (_Thierry Draper_)
* 4bf09cc: Fixed the test data downloader requiring the scripts dir (_Thierry Draper_)
* d21f5f9: CI PHPUnit test fixes (_Thierry Draper_)
* b601392: Various repo tightenings (_Thierry Draper_)
* 6268c53: Tweak the database downloader to be based on the repo, so we can import multiple repos (if needs be) (_Thierry Draper_)
* cacb6db: Authentication-based unit tests (_Thierry Draper_)
* 7d94e04: "Feature" unit tests for our standard pages (_Thierry Draper_)
* 3aa5e22: Refine the MySQL database seeder to take files from the CI data server, rather than committed in the repo (_Thierry Draper_)
* c563062: CI version of the downloader not pulling across the git branch properly (_Thierry Draper_)
* 175c9d3: Remove the CI test data do-not-check-for-7-days test, as the MD5 cache better takes care of that for us (_Thierry Draper_)
* 73e2be3: Revise the test file downloader to be a more future-proof script (_Thierry Draper_)
* 3516f18: Fix the way we load routes across all the sub-sites (_Thierry Draper_)
* 35ecac2: CI composer fix to only install missing/out-of-date packages rather than update outside of composer.lock context (_Thierry Draper_)
* 650800c: New Form Validation unit tests, including some implementation fixes (_Thierry Draper_)
* fe7f6a3: New Strings unit tests, including some implementation tweaks for how we should be using the methods (_Thierry Draper_)
* 8dd2d9a: Fix the way we're generating Singletons within the Middleware so they are. actually. singletons! (_Thierry Draper_)
* 33c5586: New Arrays unit tests, including some implementation tweaks to the hasIndex method for how we should be using it (_Thierry Draper_)
* 0ef4b8f: Minor localci/run script fixes based on the PHPUnit test (_Thierry Draper_)
* e3d3de5: New ObjectCache unit tests (_Thierry Draper_)
* 185ca8a: New Browscap unit tests (_Thierry Draper_)
* 6281838: Required test seed data didn't copy across properly (_Thierry Draper_)
* 7951a33: Move the PHPUnit config XML out of the root folder (_Thierry Draper_)
* 7e7a1fa: Fix the GepIP tests to match the actual mmdb file! (_Thierry Draper_)
* c5be2cc: Implement a MySQL service for the GeoIP unit tests to work (_Thierry Draper_)
* b1ff21e: Fix path determination for difference when running the CI tests (_Thierry Draper_)
* 4516be6: Fix path determination when running CI tests (_Thierry Draper_)
* aeb405c: Try and fix the test script cache file downloader (_Thierry Draper_)
* 4b1180f: Enable more error info during the test setup script (_Thierry Draper_)
* 426ae13: Differentiate source of GeoIP/Browscap cache files when running CI (_Thierry Draper_)
* fff7a89: Experimental script for downloading to CI the required GeoIP and Browscap cache files (_Thierry Draper_)
* f50d7ac: GeoIP unit test (as we used it for testing, so might as well!) (_Thierry Draper_)
* b99ef89: A semi-filesystem revert to remove the PHPUnit helpers from the skeleton, so they can be used in other repos (_Thierry Draper_)
* 88a0d39: Disable Feature-based PHP Unit tests (that would have been a nice-to-have) as they aren't yet compatible with the CI (_Thierry Draper_)
* f61dc08: Add our PHP Unit Tests CI tests (_Thierry Draper_)
* 2e0cf3d: Re-jig the PHPUnit test structure to match where we are going, and get the examplar tests working (_Thierry Draper_)
* 21854e6: Fixes to facilitate PHPUnit testing (_Thierry Draper_)
* 08a38f3: Fixes from the new JS standards test (_Thierry Draper_)
* afb4817: Include extended JS standards checking in the CI (_Thierry Draper_)
* 788b8d8: Fix missing replacements of User::Get() with User::object() (_Thierry Draper_)
* 6a1efc8: Apply TitleCase notation to JavaScript classes and camelCase notation to JavaScript methods (_Thierry Draper_)
* 83838f2: Script to run the CI tests locally (and accessing the composer libs directly, rather than through a git-runner) (_Thierry Draper_)
* 8c25fb4: Add PSR and Pipeline Status flags to the README (_Thierry Draper_)
* f1013d6: Password-entered stamping method needs to be called statically (_Thierry Draper_)
* 7381917: Remove the legacy "header flags" template for account header issues, that we have moved to a different model (_Thierry Draper_)
* dae986a: Fallout from applying PSR-12 to the skeleton (_Thierry Draper_)
* 4ac86dc: Stage IV of PSR-12 compliance: the models (_Thierry Draper_)
* 17b9ac6: Break down some of our larger methods, that were flagged after tweaks from hitting line limits (_Thierry Draper_)
* f03dc9c: Stage III of PSR-12 compliance: the not-so-easy supporting bits! (_Thierry Draper_)
* 8014c6b: Stage II of PSR-12 compliance: the relatively easy bits! (_Thierry Draper_)
* 168799f: Stage I of PSR-12 compliance: the quick wins! (_Thierry Draper_)
* cde723c: Composer update (_Thierry Draper_)
* 4a8c2de: Break down the Forms module to satisfy the 1000 line class / 100 line method guidelines (_Thierry Draper_)
* ee70ef1: Fix some JS refactor inconsistencies from the phpcbf run due to subtleties of how we wrote our code (Take II) (_Thierry Draper_)
* 86f8ec2: Move the logic for stamping a visited email link from the controller to the model (_Thierry Draper_)
* c3698ce: Fix some JS refactor inconsistencies from the phpcbf run due to subtleties of how we wrote our code (_Thierry Draper_)
* a100101: Include @throws PHPDoc block rules for those we make use of (_Thierry Draper_)
* 902bef9: Tidy the polyfills we need (seeing as we're now well past the PHP 5.5 needed for array_column) (_Thierry Draper_)
* 03f8d32: Minor PSR tidying following a phpcs run (Part V) (_Thierry Draper_)
* c7df2d9: Minor PSR tidying following a phpcs run (Part IV) (_Thierry Draper_)
* 9a73733: Minor PSR tidying following a phpcs run (Part III) (_Thierry Draper_)
* 2ff0fd1: Minor PSR tidying following a phpcs run (Part II) (_Thierry Draper_)
* b6eb99d: Minor PSR tidying following a phpcs run (Part I) (_Thierry Draper_)
* 4569dc9: JS linting fixes using phpcbf (_Thierry Draper_)
* 1313567: Quick-win CI fixes (_Thierry Draper_)
* 88dfb19: Add some CSS and JS linting to our CI (plus minor tweak to only check skeleton PHP files) (_Thierry Draper_)
* ee8212f: PHPMD tweaks, including shortening the Pre_Process() middleware logic (_Thierry Draper_)
* 9af1e69: Apply the lazy-loading of subdomain config model to the routes too for our CI (_Thierry Draper_)
* 8ee9425: Our sub-routes are also loaded via 'require', which breaks our CI (_Thierry Draper_)
* b2f9616: Our CI tests cannot run because we try and load all sub-site config on boot (stored in separate repos), but they are not present during the testing (_Thierry Draper_)
* 6906403: First round of PHPMD fixes (_Thierry Draper_)
* 00d7cf6: PHPMD definition tweaks (_Thierry Draper_)
* 8f71cac: Switch PHP we are testing against from 7.3 to 7.2 (_Thierry Draper_)
* 149f59d: Initial project CI (_Thierry Draper_)
* 9a6a56e: Tweak the way recently entered passwords are flagged against a user to allow external flagging (_Thierry Draper_)
* 17d4315: Flag the label field in red when a form field fails validation (_Thierry Draper_)
* 0254c71: Form validation tweaks, better allowing optional fields and triggering 'external' failures (such as failing Field B when validating Field A) (_Thierry Draper_)
* 625b2c5: Persist form failures across the session (_Thierry Draper_)
* f6baf88: Load initial form values (_Thierry Draper_)
* 476bcba: Fix Forms module saving existing records correctly (_Thierry Draper_)
* 0783761: Fix the Forms module to handle the Post/Request/Get model and field updates correctly (_Thierry Draper_)
* 5b749be: Fix suspicious form checks for My Account pages (including secondary minor fixes) (_Thierry Draper_)
* 1c05c6a: User debug panel needs to hadle non-scalars now we've defined the relationships (_Thierry Draper_)
* ce48e0f: Fix login via cookies using the correct (numeric) User ID (_Thierry Draper_)
* c7abf86: Cull the session's Remember Me token from the database on logout (_Thierry Draper_)
* 4073aa2: Fix the double posting of the login form (_Thierry Draper_)
* ea1570a: Store User IDs as the numeric value rather than text ID (_Thierry Draper_)
* 5c34872: Implement the User_Permissions model check using the new hasMany relationship (_Thierry Draper_)
* e7137eb: Implement the correct compound key for the Email model (_Thierry Draper_)
* de16d46: Store the aggregated session_id in the Activity Log table now we have built the mechanism (_Thierry Draper_)
* d091a52: Define the 1:M relationships in our models (_Thierry Draper_)
* 553f50a: Put a fix in our models for those tables not using a single auto increment primary key (_Thierry Draper_)
* 2262139: Add definitions of the 1:1 relationships in our models (_Thierry Draper_)
* 7667fe7: Minor rename of the session model classes (_Thierry Draper_)
* 9d6fdea: Remove legacy password reset columns/questions that we no longer use (_Thierry Draper_)
* ee22cb8: User upsert fixes as part of a user update rather than creation (_Thierry Draper_)
* bd247e7: Re-worked GeoIP class to be more prevalent and include more retrievable info (_Thierry Draper_)
* d9fc255: Incorporate a policy check at the start of form, rather than the Controller that may break the flow (_Thierry Draper_)
* 65e8a2e: Fix the multi-part element of the form class, as well as handling via Post/Redirect/Get (_Thierry Draper_)
* 8847f59: Create specific Exception class for the user timelock generation method (_Thierry Draper_)
* e370cb9: Allow the email sender to build emails for sending (via either mechanism) by email address, rather than User ID or Object (_Thierry Draper_)
* 4259c9f: Tweaks from implementing the Login Details email trigger form in my. (_Thierry Draper_)
* 764ab7b: Email verification warning for tablet mode (tweaked from the desktop view) (_Thierry Draper_)
* b93d3dd: Mobile nav version of the email verification re-send (_Thierry Draper_)
* 457c825: Minor URL re-jig - from my./my-account/ to my./account/ (_Thierry Draper_)
* f313956: Allow users to re-send the email verification email from within the site (desktop only for now) (_Thierry Draper_)
* 17e86ec: Implement a CORS policy manager (with a corresponding composer update) (_Thierry Draper_)
* 71b0a87: Record logins from registration as their own login source, and rebrand "auto" as "system" for those triggered internally (_Thierry Draper_)
* ba98adf: Minor email and login syntactic fixes (_Thierry Draper_)
* 1cbe869: Email link processing (_Thierry Draper_)
* e51a8f8: Email link generation fixes (_Thierry Draper_)
* 5b8f2c1: Copy across the email viewer template (_Thierry Draper_)
* f6b25c6: Minor email template fixes, now it's in the wild (_Thierry Draper_)
* 30ba99e: Enforce the login controller to only apply to logged out users (_Thierry Draper_)
* 96a4756: Simplify how we can access the session handler (_Thierry Draper_)
* 8ec034a: Include the ability for ad hoc form setup pre-processing (_Thierry Draper_)
* 044de44: Remove "noopener noreferer" flags for internal pages to be opened in new tabs (_Thierry Draper_)
* 1d0f696: Improve the standardisation by which the fieldset legend appears like the previous fieldset h3 (_Thierry Draper_)
* fd5cd46: Fix the manifest link that was using an old version of the site section mechanism (_Thierry Draper_)
* cac2924: Correct the terms/privacy pages to use the site name from config, rather than hacked together from DeBear.tld (_Thierry Draper_)
* 1dd9fb1: Move the Registration and Login Details controller methods in to their own classes (in the my repo) as we expand the functionality (_Thierry Draper_)
* 1b15718: Fix the way mobile login errors are constantly displayed on Sign In toggle (_Thierry Draper_)
* 682e670: Minor registration process fixes (_Thierry Draper_)
* 1f09fd6: Import the LICENSE file, plus refer to the Laravel switch in the README file (_Thierry Draper_)
* f08c302: Remove no longer used naming conventin (_Thierry Draper_)
* 2884ef5: Modal building mechanism, starting with the Cookie Usage page (_Thierry Draper_)
* 72b4b4c: Cookie info and preference screen (_Thierry Draper_)
* 892844a: Create a popup-specific Blade layout as its own entity rather than a part of a single "main" layout, moving the Terms of Use and Privacy Policy route logic in to the SiteTools controller (_Thierry Draper_)
* 16ba8a4: Some usability improvements to the cookie preference saving actions (_Thierry Draper_)
* 6ded886: Tweak the standard list of icons (some of which can be replaced in CSS) (_Thierry Draper_)
* 12dbe11: Cookie preference manager to be displayed at the bottom of the page, including a re-jig of how the analytics are sent (_Thierry Draper_)
* 3efd501: Tweaked iconset CSS naming (_Thierry Draper_)
* 97c50a6: Move the email address HTML builder from the HTTP class (_Thierry Draper_)
* a7ee617: Visually tidy the User debug panel (_Thierry Draper_)
* c28ed09: Mechanism for each page to include a meta description, which could be linked to its sitemap descrip (_Thierry Draper_)
* ee4a64c: Incorporate header links in the nav menu when in mobile view (including the login form...) (_Thierry Draper_)
* 8f07c04: Move the config away from "GA" specifically to a more generic term (_Thierry Draper_)
* 708bd76: Standardise the Sitemap and Nav links to the new model (_Thierry Draper_)
* e750cda: Standardise the source of header/footer links into a single list (Part I of merging in certain header links to the nav in mobile view) (_Thierry Draper_)
* b57c99a: self:: -> static:: (_Thierry Draper_)
* 2ba6cc0: Fussy rename of the layout components to better tie the folder with the (initial) HTML template (_Thierry Draper_)
* 4099bb8: Fix the header links to display the "Welcome [...]" even if no links are available (_Thierry Draper_)
* 526807b: Fix logging users out of "Remember Me?" users, whose Cookie Jar wasn't being updated in inline pages (_Thierry Draper_)
* 1070d56: Minor tweak to session aggregation stats - exclude cookie logins, as they're internal processes from stateless HTTP (_Thierry Draper_)
* 4a926df: OG/Twitter meta tag fixes (_Thierry Draper_)
* b340a10: Ability to allow users "Remember Me?" for the session timeout (_Thierry Draper_)
* f6f4674: Add a new "session" debug panel to see what's in the session (which can be turned on/off accordingly) (_Thierry Draper_)
* a0aefe0: Allow certain policy rules from being re-calced per request (primary time-based?) (_Thierry Draper_)
* d7a64c8: Minor interpolation standardisation (_Thierry Draper_)
* 58b5fde: Re-jig the config building so it could be compatible with the Laravel cacher, but still compatible with the subdomains (_Thierry Draper_)
* ad7f198: Twitter social config no longer needs box width (_Thierry Draper_)
* 842a4f4: Store off-site referers (_Thierry Draper_)
* dc1da97: Minor tidy of some security config options we copied across from legacy but aren't going to use (_Thierry Draper_)
* 82be854: Implement the session hijacking mechanism (_Thierry Draper_)
* 93f8aba: Restore the login submit button label upon failure to something more meaningful (_Thierry Draper_)
* 93cb1e0: Flag "grouping" rules in the policies in a different colour (as their status is irrelevant) (_Thierry Draper_)
* a604dd5: Login source policy setting (_Thierry Draper_)
* bd8f052: Incorporate user levels into the policies model (_Thierry Draper_)
* e6b11ff: Finish off the login/logout process in the page header (_Thierry Draper_)
* 6246b2f: Implement custom session handler using the homebrew session model (_Thierry Draper_)
* 37b6883: Add an ORM wrapper around the model to match our processes (_Thierry Draper_)
* 97a42bf: Login processing from the in-page form (_Thierry Draper_)
* 7afb87d: Checkbox CSS fixes (_Thierry Draper_)
* 4cdd938: Complete the login box which triggers the logging in action (_Thierry Draper_)
* 404e143: Include a shorthand JS querySelector(All?) (_Thierry Draper_)
* fb5d818: Minor JS BugHandler fix when not passed all expected args (_Thierry Draper_)
* 00a92e0: Debug panel at the bottom to display info about the user/policies/phpInfo() on dev (_Thierry Draper_)
* 047f863: Policies calculator on-load, with some application in the header and footer links (_Thierry Draper_)
* 118fd18: Standardise the footer custom blocks mechanism (_Thierry Draper_)
* fe49ebc: Standardised how the header and footer links are generated and displayed (_Thierry Draper_)
* aeefbd6: Minor string codification method (_Thierry Draper_)
* 15875d3: Tweak the Ajax JS library to pass through the request config on failure as well as success (_Thierry Draper_)
* 58b7f69: Tweak how the bottom footer links are built so they can be ignored by config (_Thierry Draper_)
* 85968ae: Implement less-strict MySQL mode (as per current implementation config) (_Thierry Draper_)
* 6e12292: Basic grid CSS (_Thierry Draper_)
* 91f1f5b: Minor pruning of the "general" CSS (_Thierry Draper_)
* 6d4a2c5: Remove the nav CSS/JS from the default resources, and simplify the font-loading mechanism (_Thierry Draper_)
* 9298a29: Include identification of the current nav/subnav option, with some mild nav CSS fixes (_Thierry Draper_)
* 842900f: Responsive layout, including re-work of the navigation (_Thierry Draper_)
* b09b3dd: Allow dev/live specific JS/CSS files (_Thierry Draper_)
* f10f213: Split the old desktop header/footer layout into smaller component files, now there is no desktop/mobile split (_Thierry Draper_)
* d53998f: Minor PSR standardisation of class names that hadn't been adjusted correctly from the legacy classes (_Thierry Draper_)
* 5f818d9: Standardise the User model with the naming convention that we're starting to follow (_Thierry Draper_)
* 996aab5: Remove a random 'debear' folder in the skeleton that meant to represent (but wasn't being used as) global config that wasn't to be loaded on every page run (_Thierry Draper_)
* 5a6134c: Move towards responsive pages rather than m-dot sites (_Thierry Draper_)
* 58590ec: Email sending module based on the original pattern, and linking from the Forms module (_Thierry Draper_)
* 06d70e7: Fix the form string length validation, that was being performed on the encoded string (_Thierry Draper_)
* 0f2d66b: Fixing syntax errors from previous commits (_Thierry Draper_)
* 703da91: Switch away from $_SERVER to the Laravel Input:: facade (_Thierry Draper_)
* 6639d33: Turns out you can declare a void return type on methods (_Thierry Draper_)
* 535d3c4: Ability to check/record suspicious activity within forms (_Thierry Draper_)
* 2c2323f: Tweak how logins/user accessor works to have a guaranteed usable object, even if user is logged in (_Thierry Draper_)
* 3791303: Minor re-jig of Forms validation class, that is now performing more than just validation but becoming its own form-specific Helper class (_Thierry Draper_)
* 4db9434: Re-jig the way post-processing works to allow finer control (_Thierry Draper_)
* f0fd61c: Form module completion logic to render or redirect as per the configuration (_Thierry Draper_)
* 31ac663: Switch from 301/302s to 308/307s, given enforced persistent HTTP method (_Thierry Draper_)
* 153ca78: Extrenuous trailing ; in error blade templates was being (correctly!) inadvertently rendered (_Thierry Draper_)
* 17f51ad: Start implementing app-custom exception objects (_Thierry Draper_)
* 095d151: Form module post-processing, and clearing of the form in the session upon successful completion (_Thierry Draper_)
* b9baad9: Earlier commit for the User::PrepareUpsert was missing the encryption of the Secret Answer (_Thierry Draper_)
* b6ba2e8: Implement our legacy password hashing implementation, rather than the out-of-the-box Laravel version (_Thierry Draper_)
* 6da5363: Storing mechanism of the form module (_Thierry Draper_)
* 62ddbb7: Laravel 5.6 -> 5.7 upgrade (_Thierry Draper_)
* 4fc30de: Refine client-side form validation to allow asynchronous updates (_Thierry Draper_)
* 2e54c2b: Ajax object improvements (_Thierry Draper_)
* 6f03edb: Server-side form validation (_Thierry Draper_)
* db140b1: Date Field fixes when the month is December (_Thierry Draper_)
* fa71954: Some minor form tweaks, plus method for getting a single form field's value out (_Thierry Draper_)
* 0975e1d: Move the JS get element attribute method to the DOM class (_Thierry Draper_)
* cc41aba: Include the ability to generate faux form data (triggerable only on dev) using the Faker module (_Thierry Draper_)
* b2255f1: Move the form validtion data attributes to a methodised config-based model (_Thierry Draper_)
* 1f545de: Form validation fixes (_Thierry Draper_)
* 49e7508: Rename the Events JS class file, to conform to Class name == Filename standard and minor IE code removal (_Thierry Draper_)
* 72191f3: JavaScript form validation model (_Thierry Draper_)
* c0e3391: Allow dropdowns closed by keyboard to be re-opened by keyboard (_Thierry Draper_)
* a8a17ae: JavaScript form validation class (_Thierry Draper_)
* dfaf9ad: Splitting out the DOM class into smaller chunks as it was getting too cumbersome (_Thierry Draper_)
* 4975e1a: Re-factor of the JavaScript library so far.. (_Thierry Draper_)
* 20540b3: Standarise the custom DOM onLoad functions that were being called individually (_Thierry Draper_)
* 2fc4061: Finished JS class method name standardisation (_Thierry Draper_)
* 8798a15: Start of a generic forms module, using the User Account registration page as initial example (_Thierry Draper_)
* d871779: v1 of the Date Field form widget (_Thierry Draper_)
* e8b3900: v2 of the Dropdown form widget (_Thierry Draper_)
* 2946177: Modules for GeoIP and Timezone listing (_Thierry Draper_)
* 73c6837: Add standard classes to the body tag on each page for selector purposes (_Thierry Draper_)
* 728f53d: Some JavaScript standarisation - better method for global vars, objectification and start of using title case like in PHP objects (_Thierry Draper_)
* 76cd847: Minor return variable typo fix (_Thierry Draper_)
* 2846172: Standardise the string obfuscation and codification (_Thierry Draper_)
* a51305a: Make the Terms of Use page interactive, if required (_Thierry Draper_)
* 5c303ad: Minor GA urchin tweaks (_Thierry Draper_)
* da9cd64: HTML5ing of the offline warning (_Thierry Draper_)
* de7dd4a: Minor tweak to site naming variables, separating the common site name from the section specific name (e.g., DeBear.uk and My DeBear.uk, DeBear Sports, etc) (_Thierry Draper_)
* 03316a7: Implement the logic to state if a page is a chromeless pop (_Thierry Draper_)
* 8acb8d1: Highlight the current page in navigation (v1... needs to consider aliases) (_Thierry Draper_)
* b32cfad: Implementing the error page(s) using Laravel's model (_Thierry Draper_)
* 716e487: Desktop site navigation rendering (_Thierry Draper_)
* 87547db: Sitemaps (in-page and XML) in new framework (_Thierry Draper_)
* 8650458: Standardise the PWA service worker route logic into the SiteTools controller (_Thierry Draper_)
* 6fc2bde: Major commit, with the Blade version of the (desktop) header/footer templates, re-jigged CSS and re-worked class-based JS lib (_Thierry Draper_)
* 1987995: Internal bug reporting hook (_Thierry Draper_)
* b5afe16: Report URI processing, with initial version the DeBear middleware (_Thierry Draper_)
* 5e19bc7: Migrated the PWA server worker and (initial) manifest.json files from the old skeleton as part of a "site tools" controller (_Thierry Draper_)
* a94dc93: Changes required for merging of CSS/JS files, along with some config tweaks for the next part (_Thierry Draper_)
* 620b403: Added type hints to the methods and return values (_Thierry Draper_)
* 2eae4d7: Minor whitespace fixing (_Thierry Draper_)
* 1022b04: Expanding skeleton import, as part of www sites pages, with DOM actions and config generalisations (_Thierry Draper_)
* bd7c67e: Initial setup for building the www homepage / news, including general config setup (_Thierry Draper_)
* 31fae69: Enable URLs with trailing slashes within the htaccess (and not necessarily stripped from Laravel itself) (_Thierry Draper_)
* e1fb0b4: Offline template copied from legacy skeleton (_Thierry Draper_)
* f8e7b7b: Initial routes copied from the skeleton .htaccess, with some plumbed in (_Thierry Draper_)
* 4defbcc: First tweaks at the config file (_Thierry Draper_)
* cc2c3e3: Tidy up the public folder from the Laravel default (_Thierry Draper_)
* f23c73f: Start of process of switching from Homebrew skeleton to Laravel (_Thierry Draper_)

## Build 502 - d4e12d6 - 2018-06-10 - Homebrew
* _Changelog truncated for initial build_
