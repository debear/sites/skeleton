<?php

use Phan\Issue;

return [

    // Automatically determine the appropriate version from the executable.
    'target_php_version' => null,

    // This list should include third-party code that we wouldn't necessarily want analysed (which will be excluded below)
    'directory_list' => [
        'app',
        'vendor/awobaz/compoships/src',
        'vendor/browscap/browscap-php/src',
        'vendor/fakerphp/faker/src/Faker',
        'vendor/filp/whoops/src/Whoops',
        'vendor/guzzlehttp/guzzle/src',
        'vendor/icawebdesign/hibp-php/src',
        'vendor/laravel/envoy/src',
        'vendor/laravel/framework/src/Illuminate',
        'vendor/laravel/tinker/src',
        'vendor/league/flysystem/src',
        'vendor/lonniecoffman/faker-team-names/src',
        'vendor/matthiasmullie/scrapbook/src',
        'vendor/maxmind-db/reader/src/MaxMind/Db',
        'vendor/mockery/mockery/library',
        'vendor/monolog/monolog/src',
        'vendor/nesbot/carbon/src',
        'vendor/nubs/random-name-generator/src',
        'vendor/nunomaduro/collision/src',
        'vendor/phan/phan/src/Phan',
        'vendor/php-parallel-lint/php-parallel-lint',
        'vendor/phpmd/phpmd/src/main/php',
        'vendor/phpunit/phpunit/src',
        'vendor/symfony/finder',
        'vendor/symfony/string',
        'vendor/tedivm/jshrink/src',
    ],
    'exclude_analysis_directory_list' => [
        'vendor/', __DIRS_EXC__
    ],

    // File definition.
    'analyzed_file_extensions' => [
        'php',
    ],
    'exclude_file_regex' => '@^vendor/.*/(tests?|Tests?)/@',

    // Phan plugins to use.
    'plugins' => [
        'AlwaysReturnPlugin',
        'PregRegexCheckerPlugin',
        'UnreachableCodePlugin',
    ],

    // Misc properties.
    'allow_missing_properties' => true,
    'null_casts_as_any_type' => false,
    'null_casts_as_array' => true,
    'array_casts_as_null' => true,
    'scalar_implicit_cast' => false,
    'scalar_array_key_cast' => true,
    'strict_method_checking' => false,
    'strict_object_checking' => false,
    'strict_param_checking' => false,
    'strict_property_checking' => false,
    'strict_return_checking' => false,
    'ignore_undeclared_variables_in_global_scope' => false,
    'ignore_undeclared_functions_with_known_signatures' => true,
    'backward_compatibility_checks' => false,
    'check_docblock_signature_return_type_match' => true,
    'dead_code_detection' => false,
    'unused_variable_detection' => false,
    'redundant_condition_detection' => false,
    'assume_real_types_for_internal_functions' => false,
    'quick_mode' => false,
    'minimum_severity' => Issue::SEVERITY_LOW,
    'enable_include_path_checks' => true,
    'processes' => 1,
    'autoload_internal_extension_signatures' => [],

];
