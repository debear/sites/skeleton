#!/bin/bash
# Return the title case name for a site

code="$1"
if [ "x$code" = 'xskeleton' ]
then
    echo "Skeleton"

elif [ "x$code" = 'xfantasy' ]
then
    echo "Fantasy"

elif [ "x$code" = 'xhollyandmax' ]
then
    echo "HollyAndMax"

elif [ "x$code" = 'xmy' ]
then
    echo "My"

elif [ "x$code" = 'xsports' ]
then
    echo "Sports"

elif [ "x$code" = 'xsysmon' ]
then
    echo "SysMon"

elif [ "x$code" = 'xwww' ]
then
    echo "WWW"
fi
