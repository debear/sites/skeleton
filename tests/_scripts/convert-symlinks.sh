#!/bin/bash
# Our CI isn't compatible with the centralised storage symlinks
if [ ! -z $CI ] || [ ! -z $CI_FAUX ]
then
    dir_base=`dirname $0`
    dir_root=`realpath $dir_base/../../..`

    echo "* Converting symlinks-for-live to directories-for-test under $dir_root"
    for path in $(find $dir_root -xtype l)
    do
        # Skip symlinks to other files
        if [ $(echo "$path" | grep -Pc '/[^\/]+\.[^\/]+$') -gt 0 ]
        then
            continue
        fi

        # Convert
        echo -n "- $path: "
        rm $path
        mkdir $path
        echo "[ Converted ]"
    done
fi
