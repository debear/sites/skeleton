#!/bin/bash
# Lint our stored procedures by trying to create them in a test database
#  Ideally would like to use a CLI parser, but they don't seem to match our needs?

dir_base=$(realpath $(dirname $0))
dir_root=$(realpath $dir_base/../..)
dir_db="$dir_root/database"
dir_scripts="$dir_root/tests/_scripts"
mysql_args="-s --host=$DB_HOST --port=$DB_PORT --user=$DB_USERNAME"
total=0
failed=0

. $dir_base/helpers
. $dir_scripts/helpers.sh

# Validation: as this is for testing, we _MUST_ have a DB_PREFIX set!
if [ -z $DB_PREFIX ]
then
    echo "** Missing required environment variable: DB_PREFIX." >&1
    echo "**  Will not run without it, as it may affect production/development databases." >&1
    exit 9
fi

# Any (per-repo) setup process?
custom_setup 'lint'

# Loop through the sub-folders and then types within those sub-folders
cd $dir_db
for db in $(find * -maxdepth 0 -type d)
do
    dbname="$DB_PREFIX$db"
    display_title "$db (as '$dbname'):"
    cd $dir_db/$db
    for d in $(find * -maxdepth 0 -type d)
    do
        display_title "  $d:"
        cd $dir_db/$db/$d
        sub_total=0
        sub_sect=0
        for t in $(find * -maxdepth 0 -type d | grep -Pv '(schema|templates)')
        do
            sub_sect=$(($sub_sect + 1))
            sect_total=0
            sect_failed=0
            display_section "    $t:"
            cd $dir_db/$db/$d/$t
            for f in $(ls *.sql 2>/dev/null)
            do
                # Run the command (as we'll only display the file on failure)
                err=$((lint_file "$dbname" "$f") 2>&1)
                # Then record the output
                if [ $? -ne 0 ]
                then
                    echo "      - $f:"
                    echo "        >> $err"
                    failed=$(($failed + 1))
                    sect_failed=$(($sect_failed + 1))
                fi
                total=$(($total + 1))
                sub_total=$(($sub_total + 1))
                sect_total=$(($sect_total + 1))
            done

            # If no files in the section, flag
            if [ $sect_total -eq 0 ]
            then
                display_info "      - No tests found."
            # If no errors for the section, flag (breaks up the visuals)
            elif [ $sect_failed -eq 0 ]
            then
                display_info "      - All tests passed."
            fi
        done

        # If no files or folders in the section, flag (breaks up the visuals)
        if [ $sub_sect -eq 0 ] && [ $sub_total -eq 0 ]
        then
            display_info "    - No tests found."
        fi
        echo
    done
done

if [ $failed -gt 0 ]
then
    # Failures
    display_error "$failed of $total test(s) failed."
    exit 1
else
    # Success
    display_success "All $total tests passed!"
fi
