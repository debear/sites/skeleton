#!/bin/bash
# Lint our setup files by trying to import them in a test database

dir_base=$(realpath $(dirname $0))
dir_root=$(realpath $dir_base/../..)
dir_setup="$dir_root/setup"
dir_scripts="$dir_root/tests/_scripts"
mysql_args="-s --host=$DB_HOST --port=$DB_PORT --user=$DB_USERNAME"
total=0
failed=0

. $dir_base/helpers
. $dir_scripts/helpers.sh

# Validation: as this is for testing, we _MUST_ have a DB_PREFIX set!
if [ -z $DB_PREFIX ]
then
    echo "** Missing required environment variable: DB_PREFIX." >&1
    echo "**  Will not run without it, as it may affect production/development databases." >&1
    exit 9
fi

# Database name is a derivative of the repo we're in
db="debearco_$($dir_scripts/git.sh repo)"
dbname="$DB_PREFIX$db"
display_title "$db (as '$dbname'):"

# Worker logic
function proc_files() {
    for f in $(find $dir_proc/$d -mindepth 1 -maxdepth 1 -type f -name 20\*.sql -exec basename {} \; | sort -n | uniq)
    do
        echo -n "$2- $f: "
        err=$((lint_file "$dbname" "$dir_proc/$d/$f") 2>&1)
        if [ $? -eq 0 ]
        then
            display_green "Okay"
        else
            failed=$(($failed + 1))
            display_error "Failed"
            echo "$2  $err"
        fi
        total=$(($total + 1))
    done
}
function proc_dir() {
    local spacer="$3"
    local dir_proc="$1/$2"
    local d=''
    for d in $(find $dir_proc -mindepth 1 -maxdepth 1 -type d -exec basename {} \; | sort -n | uniq)
    do
        # Skip a directory that doesn't contain any "season" config files
        if [ $d = 'archive' ] || [ $(find $dir_proc/$d -name 20\*.sql | wc -l) -eq 0 ]
        then
            continue
        fi
        display_section "$spacer$d:"
        # Process any sub-dirs
        proc_dir $dir_proc $d "$spacer  "
        # Then any individual files
        proc_files "$dir_proc/$d" "$spacer  "
    done
}

# Loop through the setup files, and load each in to our dummy database
cd $dir_setup
proc_dir $dir_setup '' '  '

if [ $failed -gt 0 ]
then
    # Failures
    display_error "$failed of $total test(s) failed."
    exit 1
else
    # Success
    display_success "All $total tests passed!"
fi
