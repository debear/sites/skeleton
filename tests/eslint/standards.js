module.exports = [
    {
        "languageOptions": {
            "ecmaVersion": 13 // es2022
        },
        "rules": {
            // Functions and Variables should be in camelCase
            "camelcase": [
                "error",
                {
                    "properties": "never",
                    "ignoreDestructuring": true
                }
            ],
            // Constructors should be in PascalCase
            "new-cap": [
                "error",
                {
                    "newIsCap": true,
                    "capIsNew": true,
                    "properties": true
                }
            ],
            // Constructors should be called with brackets, even if arg list is empty
            "new-parens": ["error"],
            // Enforce placing object properties on separate lines
            "object-property-newline": [
                "error",
                {
                    "allowAllPropertiesOnSameLine": true
                }
            ],
            // Do not end elements on a comma
            "comma-dangle": ["error", "never"],
            // Commas must be used straight after the element
            "comma-style": ["error", "last"],
            // Dots should be used on the same line as the object, not the property, if split over multiple lines
            "dot-location": ["error", "object"],
            // Disallow constant expressions in conditions
            "no-constant-condition": [
                "error",
                {
                    "checkLoops": true
                }
            ],



            // Four space indentation
            "indent": ["error", 4],
            // Disallow trailing whitespace at the end of lines
            "no-trailing-spaces": ["error"],
            // Enforce spacing before and after semicolons
            "semi-spacing": [
                "error",
                {
                    "before": false,
                    "after": true
                }
            ],
            // Require space before blocks
            "space-before-blocks": ["error", "always"],
            // Disallow spaces inside of parentheses
            "space-in-parens": ["error", "never"],
            // Disallow spaces before/after unary operators (as names, not symbols)
            "space-unary-ops": [
                "error",
                {
                    "words": true,
                    "nonwords": false
                }
            ],
            // Require a whitespace at the start of a comment
            "spaced-comment": [
                "error",
                "always",
                {
                    "block": {
                        "balanced": true
                    }
                }
            ],
            // Enforce usage of spacing in template strings
            "template-curly-spacing": ["error"],
            // Disallow spaces between function name and its arguments
            "func-call-spacing": ["error", "never"],
            // Apply certain spacing around object keys
            "key-spacing": [
                "error",
                {
                    "beforeColon": false,
                    "afterColon": true,
                    "mode": "minimum"
                }
            ],
            // File requires a trailing newline
            "eol-last": ["error", "always"],



            // Require calls to isNaN() when checking for NaN
            "use-isnan": ["error"],
            // Enforce comparing typeof expressions against valid strings
            "valid-typeof": [
                "error",
                {
                    "requireStringLiterals": true
                }
            ],



            // Disallow Unused Variables
            "no-unused-vars": [
                "error",
                {
                    "vars": "all",
                    "args": "all",
                    "ignoreRestSiblings": false
                }
            ],
            // Disallow modifying variables of class declarations
            "no-class-assign": ["error"],
            // Disallow modifying variables that are declared using const
            "no-const-assign": ["error"],
            // Disallow duplicate arguments in function definitions
            "no-dupe-args": ["error"],
            // Disallow duplicate name in class members
            "no-dupe-class-members": ["error"],
            // Disallow duplicate keys in object literals
            "no-dupe-keys": ["error"],
            // Disable creating new arrays via an Object constructor
            "no-array-constructor": ["error"],
            // Disallow a duplicate case label in switches
            "no-duplicate-case": ["error"],
            // Explicitly flag (via a falls?\s?through comment) switch case statement fallthrough
            "no-fallthrough": ["error"],
            // Disallow Floating Decimals
            "no-floating-decimal": ["error"],
            // Disallow assignment to native objects or read-only global variables
            "no-global-assign": ["error"],
            // Disallow implied eval calls
            "no-implied-eval": ["error"],
            // Disallow invalid regular expression strings in RegExp constructors
            "no-invalid-regexp": ["error"],
            // Disallow Unnecessary Nested Blocks
            "no-lone-blocks": ["error"],
            // Disallow mixed spaces and tabs for indentation
            "no-mixed-spaces-and-tabs": ["error"],
            // Disallow Primitive Wrapper Instances
            "no-new-wrappers": ["error"],
            // Disallow variable redeclaration
            "no-redeclare": ["error"],
            // Disallow Assignment in return Statement
            "no-return-assign": ["error"],
            // Disallow Self Assignment
            "no-self-assign": ["error"],
            // Disallow Self Compare
            "no-self-compare": ["error"],
            // Disallow Shadowing of Restricted Names
            "no-shadow-restricted-names": ["error"],
            // Disallow unmodified conditions of loops
            "no-unmodified-loop-condition": ["error"],
            // Disallow ternary operators when simpler alternatives exist
            "no-unneeded-ternary": ["error"],
            // Disallow unreachable code after return, throw, continue, and break statements
            "no-unreachable": ["error"],
            // Disallow Yoda Conditions
            "yoda": ["error", "never"]
        }
    }
];
