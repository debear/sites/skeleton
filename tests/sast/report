#!/bin/bash
# Using the GitLab API, parse and report on the artifact(s) from the last SAST jobs that were run

# Configuration
dir_base=$(dirname $(realpath $0))
dir_scripts=$(realpath $dir_base/../_scripts)
dir_runs="$dir_base/runs"

# Load the common functions
. $dir_scripts/helpers.sh
. $dir_base/helpers.sh
. $dir_base/download.sh

# Process the command line arguments
parse_args $@

# By default we will be syncing pipelines from the source-of-truth
pipeline_sync=1

# If we're listing the pipelines we have, do so and exit
if [ $mode = 'list' ]
then
    display_title "SAST Reports for repo $(basename $(realpath $dir_scripts/../..))"
    runs=$(ls -td $dir_runs/20??-??-??_* 2>/dev/null | sort -nr | awk -F'/' '{print $NF}' | head -30)
    if [ ! -z "$runs" ]
    then
        for run in $runs
        do
            pipeline_date=$(echo "$run" | awk -F'_' '{ print $1 }')
            pipeline_id=$(echo "$run" | awk -F'_' '{ print $2 }')
            echo -n "- "
            display_section -n $pipeline_id
            display_info " ($pipeline_date)"
        done
    else
        display_warning "No SAST reports downloaded"
    fi
    exit

elif [ $mode = 'last' ]
then
    # Determine the last cached run and use that
    last_run=$(ls -td $dir_runs/20??-??-??_* 2>/dev/null | sort -nr | awk -F'/' '{print $NF}' | head -1)
    if [ ! -z "$last_run" ]
    then
        pipeline_date=$(echo "$last_run" | awk -F'_' '{ print $1 }')
        pipeline_id=$(echo "$last_run" | awk -F'_' '{ print $2 }')
        dir_run="$dir_runs/$last_run"
        pipeline_sync=0 # No upstream sync is required
    else
        # We do not have a last version, so warn and ensure we trigger an upstream sync
        display_warning "No local pipelines run have been stored, triggering an upstream sync"
    fi

elif [ $mode = 'emulated' ]
then
    # Report on the most recent locally emulated run
    dir_emulated="$dir_runs/emulated"
    if [ -e "$dir_emulated" ] && [ ! -z "$(ls $dir_emulated/*.json.gz 2>/dev/null)" ]
    then
        pipeline_id='(emulated)'
        pipeline_date=$(date -r $(ls -t $dir_emulated/*.json.gz | head -1) +'%F')
        dir_run="$dir_emulated"
        pipeline_sync=0 # No upstream sync is required
    else
        # We do not have a last version, so warn and ensure we trigger an upstream sync
        display_warning "No emulated run has been stored, triggering an upstream sync and reporting on the latest upstream run"
    fi
fi

[ $pipeline_sync -eq 1 ] && identify_pipeline_catchup # Download artifacts from any pipeline runs we have not synced with
[ -z "$pipeline_id" ] && identify_pipeline_latest # Determine the appropriate pipeline to search for SAST artifacts, if not provided a specific ID
[ $mode != 'emulated' ] && download_pipeline_artifacts

# Display our report per-job
display_title "SAST Report for Pipeline $pipeline_id ($pipeline_date)"
for job_file in $(ls $dir_run/*.json.gz | grep -Fv '/jobs.json.gz')
do
    job=$(basename $job_file | sed 's/\.json\.gz$//')
    [ $summary -eq 0 ] && echo && display_section "Job: $job"
    analyse_report "$job_file"
done
summarise_report
