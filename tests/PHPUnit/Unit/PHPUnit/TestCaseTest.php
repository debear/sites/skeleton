<?php

namespace Tests\PHPUnit\Unit\PHPUnit;

use Tests\PHPUnit\Base\APITestCase;
use Illuminate\Support\Facades\Config as FrameworkConfig;

class TestCaseTest extends APITestCase
{
    /**
     * The URL our test operates over
     * @var string
     */
    protected $url = '/api/unit-tests/v1.0/base-class';
    /**
     * The list of HTTP methods to loop over
     * @var array
     */
    protected $methods = [
        'get' => true,
        'post' => true,
        'patch' => true,
        'put' => true,
        'delete' => true,
        'options' => true,
        'head' => false,
    ];
    /**
     * The list of HTTP methods to test sending data
     * @var array
     */
    protected $data = ['post', 'patch', 'put', 'delete'];

    /**
     * A test of request methods in FeatureTestCase
     * @return void
     */
    public function testFeatureTestCase(): void
    {
        $rate_limit = FrameworkConfig::get('debear.api.thresholds.unit_tests');
        foreach ($this->methods as $method => $has_content) {
            $req = $this->$method($this->url);
            $req->assertStatus(200);
            if ($has_content) {
                $req->assertJSON(['success' => true]);
                $req->assertJSON(['method' => $method]);
            }
            // Also reduces the count for the rate limit.
            $req->assertHeader('x-ratelimit-remaining', --$rate_limit);
        }
    }

    /**
     * A test of request methods in FeatureTestCase with data
     * @return void
     */
    public function testFeatureTestCaseData(): void
    {
        foreach ($this->data as $method) {
            $req = $this->$method($this->url, ['data' => true], ['Content-Type' => 'application/json']);
            $req->assertStatus(200);
            $req->assertJSON(['success' => true]);
            $req->assertJSON(['method' => $method]);
            $req->assertJSONPath('input.data', true);
        }
    }

    /**
     * A test of request methods in APITestCase
     * @return void
     */
    public function testAPITestCase(): void
    {
        foreach ($this->methods as $method => $has_content) {
            $method_unthrottled = "{$method}Unthrottled";
            $req = $this->$method_unthrottled($this->url);
            $req->assertStatus(200);
            if ($has_content) {
                $req->assertJSON(['success' => true]);
                $req->assertJSON(['method' => $method]);
            }
            // No rate limit applicable.
            $req->assertHeaderMissing('x-ratelimit-remaining');
        }
    }
}
