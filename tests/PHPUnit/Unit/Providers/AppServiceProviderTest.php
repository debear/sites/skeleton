<?php

namespace Tests\PHPUnit\Unit\Providers;

use Tests\PHPUnit\Base\UnitTestCase;
use DeBear\Exceptions\Handler;

class AppServiceProviderTest extends UnitTestCase
{
    /**
     * Test our blade directives
     * @return void
     */
    public function testDirectives(): void
    {
        $response = response(view('skeleton.dev.ci.asp'));
        $this->assertEquals("Hello, world! // testing\n", $response->getContent());
    }
}
