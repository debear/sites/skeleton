<?php

namespace Tests\PHPUnit\Unit\Providers;

use Illuminate\Support\Facades\App;
use Tests\PHPUnit\Base\UnitTestCase;
use DeBear\Providers\HashServiceProvider;

class HashServiceProviderTest extends UnitTestCase
{
    /**
     * Test our providing response
     * @return void
     */
    public function testProvides(): void
    {
        $provider = new HashServiceProvider(App::getFacadeApplication());
        $this->assertEquals(['hash'], $provider->provides());
    }
}
