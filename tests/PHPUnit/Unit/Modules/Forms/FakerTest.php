<?php

namespace Tests\PHPUnit\Unit\Modules\Forms;

use Tests\PHPUnit\Base\UnitTestCase;
use DeBear\Http\Modules\Forms\Faker;

class FakerTest extends UnitTestCase
{
    /**
     * Process various options through the faux test data generator
     * @return void
     */
    public function testMain(): void
    {
        // Build or form fields.
        $fields = [
            'honeypot' => [
                'honeypot' => true,
            ],
            'user_id' => [
                'faker' => 'userName',
            ],
            'forename' => [
                'faker' => 'firstName',
            ],
            'surname' => [
                'faker' => 'lastName',
            ],
            'display_name' => [
                'faker' => [
                    '_copy', 'forename',
                ],
            ],
            'email' => [
                'faker' => 'safeEmail',
            ],
            'dob' => [
                'faker' => [
                    'date', [
                        'Y-m-d',
                        '-4 year',
                    ],
                ],
            ],
            'agreed' => [
                'faker' => [
                    '_value', true,
                ],
            ],
            'subscribe' => [
                'faker' => false,
            ],
        ];
        // Run through the data generator.
        $data = Faker::setup($fields);

        // Test the honeypot field.
        $this->assertFalse(isset($data['honeypot']));

        // Test the user_id field.
        $this->assertTrue(isset($data['user_id']));
        $this->assertNotEmpty($data['user_id']);
        $this->assertIsString($data['user_id']);
        $this->assertGreaterThan(0, strlen($data['user_id']));

        // Test the forename field.
        $this->assertTrue(isset($data['forename']));
        $this->assertNotEmpty($data['forename']);
        $this->assertIsString($data['forename']);
        $this->assertGreaterThan(0, strlen($data['forename']));

        // Test the surname field.
        $this->assertTrue(isset($data['surname']));
        $this->assertNotEmpty($data['surname']);
        $this->assertIsString($data['surname']);
        $this->assertGreaterThan(0, strlen($data['surname']));

        // Test the display_name field.
        $this->assertTrue(isset($data['display_name']));
        $this->assertNotEmpty($data['display_name']);
        $this->assertIsString($data['display_name']);
        $this->assertGreaterThan(0, strlen($data['display_name']));
        $this->assertEquals($data['display_name'], $data['forename']);

        // Test the email field.
        $this->assertTrue(isset($data['email']));
        $this->assertNotEmpty($data['email']);
        $this->assertIsString($data['email']);
        $this->assertGreaterThan(0, strlen($data['email']));
        $this->assertNotFalse(filter_var($data['email'], FILTER_VALIDATE_EMAIL));

        // Test the dob field.
        $this->assertTrue(isset($data['dob']));
        $this->assertIsString($data['dob']);
        $this->assertEquals(1, preg_match('/^\d{4}\-\d{2}\-\d{2}$/', $data['dob']));

        // Test the agreed field.
        $this->assertTrue(isset($data['agreed']));
        $this->assertIsBool($data['agreed']);
        $this->assertTrue($data['agreed']);

        // Test the subscribe field.
        $this->assertTrue(isset($data['subscribe']));
        $this->assertIsBool($data['subscribe']);
        $this->assertFalse($data['subscribe']);
    }
}
