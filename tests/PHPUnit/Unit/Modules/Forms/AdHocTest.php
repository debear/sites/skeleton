<?php

namespace Tests\PHPUnit\Unit\Modules\Forms;

use Tests\PHPUnit\Base\UnitTestCase;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Exceptions\FormsException;
use DeBear\Http\Modules\Forms\AdHoc;
use DeBear\Models\Skeleton\CommsEmail;
use DeBear\Models\Skeleton\User;
use DeBear\ORM\Skeleton\UserActivityLog;

class AdHocTest extends UnitTestCase
{
    /**
     * A boolean to indicate the custom callable was run
     * @var boolean
     */
    protected static $callableRun = false;

    /**
     * Process various ad hoc post-processing options
     * @return void
     */
    public function testMain(): void
    {
        // Initial email counter.
        $num_email_start = CommsEmail::count();

        // Our full post-processing options.
        $actions = [
            // Log a user in.
            'login',
            // Send an email, user from object.
            'email' => 'User: Registered',
            // A custom callable.
            [static::class, 'customCallable'],
            // Log them out again.
            'logout',
        ];
        $extra = [
            'obj' => [
                'DeBear\Models\Skeleton\User' => [
                    'obj' => User::find(10),
                ],
            ],
            'raw' => [
                'proc' => [],
            ],
        ];
        $this->assertFalse(static::isCallableRun());
        AdHoc::run($actions, $extra);
        $this->assertTrue(static::isCallableRun());
        $this->assertEquals($num_email_start + 1, CommsEmail::count());

        // Send an email, user from ID.
        $extra['obj'] = [];
        $actions = [
            'email' => [
                'name' => 'User: Registered',
                'user_id' => 'test_user',
            ],
        ];
        AdHoc::run($actions, $extra);
        $this->assertEquals($num_email_start + 2, CommsEmail::count());

        // Send an email, user currently logged in.
        $actions = [
            'email' => [
                'name' => 'User: Registered',
            ],
        ];
        User::doLogin(User::find(10));
        AdHoc::run($actions, $extra);
        User::doLogout();
        $this->assertEquals($num_email_start + 3, CommsEmail::count());

        // Send an email, no user (same email count).
        $actions = [
            'email' => [
                'name' => 'User: Registered',
            ],
        ];
        $this->expectException(FormsException::class);
        AdHoc::run($actions, $extra);
    }

    /**
     * Test the auditing capabilities separately, as it modifies data
     * @return void
     */
    public function testAudit(): void
    {
        // Make the effective change.
        $obj = User::find(35);
        $obj->update(['dob' => '1980-01-01'])->save();

        // Action of auditing the record.
        $actions = [
            'audit' => [
                'type' => 'account_update',
                'summary' => 'User updated',
                'fields' => ['display_name', 'dob'],
            ],
        ];
        $extra = [
            'obj' => [
                get_class($obj) => [
                    'obj' => $obj,
                ],
            ],
            'raw' => [
                'changes' => [
                    'fields' => [
                        'dob' => true,
                    ],
                ],
            ],
        ];
        AdHoc::run($actions, $extra, array_key_first($extra['obj']));

        // Confirm the change was audited.
        $audit = UserActivityLog::query()
            ->where('type', FrameworkConfig::get("debear.logging.user_activity.{$actions['audit']['type']}"))
            ->orderByDesc('activity_id')->limit(1)->get();
        $this->assertEquals('User updated', $audit->summary);
        $audit_detail = json_decode($audit->detail, true);
        $this->assertEquals('1980-01-01', $audit_detail['dob']);
        $this->assertFalse(isset($audit_detail['display_name']));
    }

    /**
     * Get the static property indicating the callable has been run
     * @return boolean That the customCallable() method has been run
     */
    public static function isCallableRun(): bool
    {
        return static::$callableRun;
    }

    /**
     * A custom callable method for testing bespoke ad hoc calls.
     * @param array $obj The form objects.
     * @return void
     */
    public static function customCallable(array $obj): void
    {
        static::$callableRun = count(array_keys(array_filter($obj)));
    }
}
