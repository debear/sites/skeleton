<?php

namespace Tests\PHPUnit\Unit\Modules\Forms;

use Tests\PHPUnit\Base\UnitTestCase;
use DeBear\Http\Modules\Forms\ModelData;
use DeBear\Models\Skeleton\User;
use DeBear\Repositories\Time;

class ModelDataTest extends UnitTestCase
{
    /**
     * Process various options through the model data form generator
     * @return void
     */
    public function testMain(): void
    {
        // We need a logged in user.
        User::doLogin(User::find(10));

        // Build our tests.
        $map = [
            'form_fields' => [
                'form_1',
                'form_2' => 'form_2a',
            ],
            'user_fields' => [
                'display_name',
            ],
            'fixed_fields' => [
                'f_str' => 'fixed_string',
                'f_now' => ['timestamp' => 'now'],
                'f_snow' => ['timestamp' => 'server_now'],
            ],
        ];
        $form_data = [
            'form_1' => 'string:f1',
            'form_2' => 'string:f2',
            'form_2a' => 'string:f2a',
        ];

        // Run and analyse.
        $table_data = ModelData::build($map, $form_data);
        // Field: form_1.
        $this->assertTrue(isset($table_data['form_1']));
        $this->assertEquals('string:f1', $table_data['form_1']);
        // Field: form_2 / 2a.
        $this->assertTrue(isset($table_data['form_2']));
        $this->assertEquals('string:f2a', $table_data['form_2']);
        $this->assertFalse(isset($table_data['form_2a']));
        // Field: display_name.
        $this->assertTrue(isset($table_data['display_name']));
        $this->assertEquals('Test User', $table_data['display_name']);
        // Field: f_str.
        $this->assertTrue(isset($table_data['f_str']));
        $this->assertEquals('fixed_string', $table_data['f_str']);
        // Field: f_now.
        $this->assertTrue(isset($table_data['f_now']));
        $this->assertMatchesRegularExpression(
            '/^20\d{2}\-[01]\d\-[0-3]\d [0-2]\d:[0-6]\d:[0-6]\d$/',
            $table_data['f_now']
        );
        // Field: f_snow.
        $this->assertTrue(isset($table_data['f_snow']));
        $this->assertEquals($table_data['f_now'], $table_data['f_snow']);
    }
}
