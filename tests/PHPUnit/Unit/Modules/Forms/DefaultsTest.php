<?php

namespace Tests\PHPUnit\Unit\Modules\Forms;

use Tests\PHPUnit\Base\UnitTestCase;
use DeBear\Http\Modules\Forms\Defaults;
use DeBear\Helpers\TransientStorage;
use DeBear\Models\Skeleton\User;
use DeBear\Models\Skeleton\Timezone;

class DefaultsTest extends UnitTestCase
{
    /**
     * Process various options through the default form data generator
     * @return void
     */
    public function testMain(): void
    {
        // We need a logged in user.
        $user = User::find(10);
        User::doLogin($user);

        // Build our input data.
        $definitions = [
            'user',
            'transient' => 'ci.tz',
        ];
        $fields = [
            'user_id' => 'def',
            'display_name' => 'def',
            'population' => 'def',
            'custom_key' => 'def',
        ];

        // Set the session value.
        $tz = Timezone::find($user->timezone);
        TransientStorage::set('ci.tz', $tz);

        // Run and analyse.
        $defaults = Defaults::load($definitions, $fields);
        $this->assertIsArray($defaults);
        $this->assertTrue(isset($defaults['user_id']));
        $this->assertTrue(isset($defaults['display_name']));
        $this->assertTrue(isset($defaults['population']));
        $this->assertFalse(isset($defaults['email']));
        $this->assertFalse(isset($defaults['custom_key']));
        $this->assertFalse(isset($defaults['code2']));

        // Unset the transient storage value.
        TransientStorage::unset('ci.tz');
    }
}
