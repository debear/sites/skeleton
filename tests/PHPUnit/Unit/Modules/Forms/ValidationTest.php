<?php

namespace Tests\PHPUnit\Unit\Modules\Forms;

use Tests\PHPUnit\Base\UnitTestCase;
use DeBear\Http\Modules\Forms\Validation;

class ValidationTest extends UnitTestCase
{
    /**
     * Process various versions that we would pass through the validator
     * @return void
     */
    public function testMain(): void
    {
        // Honeypot - Pass.
        $this->assertTrue(Validation::process(['honeypot' => true], null, [], []));

        // Honeypot - Fail.
        $ret = Validation::process(['honeypot' => true], 'value', [], []);
        $this->assertTrue(is_array($ret));
        $this->assertEquals('honeypot', $ret['code']);

        // Required - Pass.
        $this->assertTrue(Validation::process(['required' => true, 'type' => 'text'], 'value', [], []));

        // Required - Text Fail.
        $ret = Validation::process(['required' => true, 'type' => 'text'], null, [], []);
        $this->assertTrue(is_array($ret));
        $this->assertEquals('required', $ret['code']);
        $this->assertEquals('Please complete this required field', $ret['message']);

        // Required - Checkbox Fail.
        $ret = Validation::process(['required' => true, 'type' => 'checkbox'], null, [], []);
        $this->assertTrue(is_array($ret));
        $this->assertEquals('required', $ret['code']);
        $this->assertEquals('This option must be selected', $ret['message']);

        // Number - Pass.
        $this->assertTrue(Validation::process(['type' => 'num'], 1, [], []));
        $this->assertTrue(Validation::process(['type' => 'num'], 1.2, [], []));

        // Number - Fail.
        $ret = Validation::process(['type' => 'num'], 'value', [], []);
        $this->assertTrue(is_array($ret));
        $this->assertEquals('is_num', $ret['code']);

        // Email - Pass.
        $this->assertTrue(Validation::process(['type' => 'email'], 'test@example.com', [], []));

        // Email - Fail.
        $ret = Validation::process(['type' => 'email'], 'value', [], []);
        $this->assertTrue(is_array($ret));
        $this->assertEquals('is_email', $ret['code']);

        // From List - Pass.
        $this->assertTrue(Validation::process(['type' => 'text'], 'one', ['opt_list' => ['one', 'two', 'three']], []));

        // From List - Fail.
        $ret = Validation::process(['type' => 'text'], 'four', ['opt_list' => ['one', 'two', 'three']], []);
        $this->assertTrue(is_array($ret));
        $this->assertEquals('opt_list', $ret['code']);

        // Custom - Pass.
        $this->assertTrue(Validation::process(['type' => 'num', 'custom' => [
            'min' => [
                'method' => [static::class, 'customMin'],
                'args' => ['rand' => rand(2, 5)],
            ]
        ]], 6, [], []));

        // Custom - Fail.
        $ret = Validation::process(['type' => 'num', 'custom' => [
            'custom_min' => [
                'method' => [static::class, 'customMin'],
                'args' => ['rand' => rand(2, 5)],
            ]
        ]], 1, [], []);
        $this->assertTrue(is_array($ret));
        $this->assertEquals('custom_min', $ret['code']);
    }

    /**
     * Process various ranges that we would pass through the validator
     * @return void
     */
    public function testRanges(): void
    {
        // Min Length - Pass.
        $this->assertTrue(Validation::process(['type' => 'text', 'minlength' => 6], 'longtext', [], []));
        $this->assertTrue(Validation::process(['type' => 'text', 'minlength' => 8], 'boundary', [], []));

        // Min Length - Fail.
        $ret = Validation::process(['type' => 'text', 'minlength' => 6], 'short', [], []);
        $this->assertTrue(is_array($ret));
        $this->assertEquals('minlength', $ret['code']);

        // Max Length - Pass.
        $this->assertTrue(Validation::process(['type' => 'text', 'maxlength' => 6], 'short', [], []));
        $this->assertTrue(Validation::process(['type' => 'text', 'maxlength' => 8], 'boundary', [], []));

        // Max Length - Fail.
        $ret = Validation::process(['type' => 'text', 'maxlength' => 6], 'longtext', [], []);
        $this->assertTrue(is_array($ret));
        $this->assertEquals('maxlength', $ret['code']);

        // Min/Max Length - Pass.
        $this->assertTrue(Validation::process(['type' => 'text', 'minlength' => 3, 'maxlength' => 5], 'two', [], []));
        $this->assertTrue(Validation::process(['type' => 'text', 'minlength' => 3, 'maxlength' => 5], 'words', [], []));

        // Min/Max Length - Fail (Min).
        $ret = Validation::process(['type' => 'text', 'minlength' => 3, 'maxlength' => 5], 'an', [], []);
        $this->assertTrue(is_array($ret));
        $this->assertEquals('minlength', $ret['code']);

        // Min/Max Length - Fail (Max).
        $ret = Validation::process(['type' => 'text', 'minlength' => 3, 'maxlength' => 5], 'longtext', [], []);
        $this->assertTrue(is_array($ret));
        $this->assertEquals('maxlength', $ret['code']);

        // Min - Pass.
        $this->assertTrue(Validation::process(['type' => 'num', 'min' => 5], 6, [], []));
        $this->assertTrue(Validation::process(['type' => 'num', 'min' => 5], 5, [], []));

        // Min - Fail.
        $ret = Validation::process(['type' => 'num', 'min' => 5], 4, [], []);
        $this->assertTrue(is_array($ret));
        $this->assertEquals('min', $ret['code']);

        // Max - Pass.
        $this->assertTrue(Validation::process(['type' => 'num', 'max' => 5], 4, [], []));
        $this->assertTrue(Validation::process(['type' => 'num', 'max' => 5], 5, [], []));

        // Max - Fail.
        $ret = Validation::process(['type' => 'num', 'max' => 5], 6, [], []);
        $this->assertTrue(is_array($ret));
        $this->assertEquals('max', $ret['code']);

        // Min/Max - Pass.
        $this->assertTrue(Validation::process(['type' => 'num', 'min' => 4, 'max' => 5], 4, [], []));
        $this->assertTrue(Validation::process(['type' => 'num', 'min' => 4, 'max' => 5], 5, [], []));

        // Min/Max - Fail (Min).
        $ret = Validation::process(['type' => 'num', 'min' => 4, 'max' => 5], 3, [], []);
        $this->assertTrue(is_array($ret));
        $this->assertEquals('min', $ret['code']);

        // Min/Max - Fail (Max).
        $ret = Validation::process(['type' => 'num', 'min' => 4, 'max' => 5], 6, [], []);
        $this->assertTrue(is_array($ret));
        $this->assertEquals('max', $ret['code']);

        // Min/Max Date - Pass.
        $def = ['type' => 'date', 'min' => '2018-12-01', 'max' => '2018-12-31'];
        $this->assertTrue(Validation::process($def, '2018-12-10', [], []));

        // Min/Max Date - Fail (Min).
        $ret = Validation::process($def, '2018-11-30', [], []);
        $this->assertTrue(is_array($ret));
        $this->assertEquals('min', $ret['code']);

        // Min/Max Date - Fail (Max).
        $ret = Validation::process($def, '2019-01-01', [], []);
        $this->assertTrue(is_array($ret));
        $this->assertEquals('max', $ret['code']);
    }

    /**
     * Worker custom validation method
     * @param mixed $value    The specific value being validated.
     * @param array $all_data The full dataset we are processing.
     * @param array $opt      Custom validation properties.
     * @return string The error message for failed validation, or empty string otherwise
     */
    public static function customMin(mixed $value, array $all_data, array $opt): string
    {
        return $value < $opt['rand']
            // Value is below our (random) minimum.
            ? 'The value is below our minimum required value'
            // Value is okay, so pass empty string for success.
            : '';
    }
}
