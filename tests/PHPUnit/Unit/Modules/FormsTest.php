<?php

namespace Tests\PHPUnit\Unit\Modules;

use Tests\PHPUnit\Base\UnitTestCase;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Http\Modules\Forms;
use DeBear\ORM\Skeleton\UserActivityLog;

class FormsTest extends UnitTestCase
{
    /**
     * A form submitted via an API
     * @return void
     */
    public function testFromAPI(): void
    {
        $obj = new Forms('ci/api');
        $audit_type_id = FrameworkConfig::get('debear.logging.user_activity.account_update');

        // Invalid data - terms not selected.
        $invalid_terms = $obj->processFromAPI([
            'user_id' => 'api_form',
            'forename' => 'Test',
            'surname' => 'User',
            'terms' => false,
        ]);
        $this->assertIsArray($invalid_terms);
        $this->assertCount(1, $invalid_terms);
        $this->assertNotNull($invalid_terms['terms']);
        $this->assertEquals('required', $invalid_terms['terms']['code']);

        // Invalid data - range of anumber.
        $invalid_num = $obj->processFromAPI([
            'user_id' => 'api_form',
            'forename' => 'Test',
            'surname' => 'User',
            'terms' => true,
            'anumber' => 10,
        ]);
        $this->assertIsArray($invalid_num);
        $this->assertCount(1, $invalid_num);
        $this->assertNotNull($invalid_num['anumber']);
        $this->assertEquals('max', $invalid_num['anumber']['code']);

        // Pre-auditing count.
        $audit_count_before = UserActivityLog::query()->where('type', $audit_type_id)->count();
        // Valid data.
        $valid = $obj->processFromAPI([
            'user_id' => 'api_form',
            'forename' => 'Test',
            'surname' => 'User',
            'terms' => true,
        ]);
        $this->assertIsBool($valid);
        $this->assertTrue($valid);
        // That it was audited.
        $audit_count_after = UserActivityLog::query()->where('type', $audit_type_id)->count();
        $this->assertEquals(1, $audit_count_after - $audit_count_before);
        // And confirm those details.
        $audit = UserActivityLog::query()
            ->where('type', $audit_type_id)
            ->orderByDesc('activity_id')->limit(1)->get();
        $this->assertEquals('Record processed', $audit->summary);
        $audit_detail = json_decode($audit->detail, true);
        $this->assertEquals('Test', $audit_detail['forename']);
        $this->assertFalse(isset($audit_detail['surname']));
    }
}
