<?php

namespace Tests\PHPUnit\Unit\Repositories;

use Tests\PHPUnit\Base\UnitTestCase;
use Illuminate\Support\Facades\App;
use DeBear\Repositories\InternalCache;

class InternalCacheTest extends UnitTestCase
{
    /**
     * Instantiated object cache object
     * @var InternalCache
     */
    protected $cache;

    /**
     * Prepare the object by setting up the InternalCache instance
     * @return void
     */
    protected function setUp(): void
    {
        // Load the standard test setup methods.
        parent::setUp();
        // Instantiate our class.
        $this->cache = InternalCache::object();
    }

    /**
     * Test a boolean lookup
     * @return void
     */
    public function testBoolean(): void
    {
        $key = 'testBoolean';
        // Unset.
        $this->assertFalse($this->cache->cacheKeyExists($key));
        $this->assertFalse($this->cache->get($key));
        // Set.
        $value = false;
        $this->cache->set($key, $value);
        $this->assertTrue($this->cache->cacheKeyExists($key));
        $this->assertEquals($value, $this->cache->get($key));
    }

    /**
     * Test a scalar lookup
     * @return void
     */
    public function testScalar(): void
    {
        $key = 'testScalar';
        // Unset.
        $this->assertFalse($this->cache->cacheKeyExists($key));
        $this->assertFalse($this->cache->get($key));
        // Set.
        $value = 123;
        $this->cache->set($key, $value);
        $this->assertTrue($this->cache->cacheKeyExists($key));
        $this->assertEquals($value, $this->cache->get($key));
    }

    /**
     * Test a simple array lookup
     * @return void
     */
    public function testArraySimple(): void
    {
        $key = 'testArraySimple';
        // Unset.
        $this->assertFalse($this->cache->cacheKeyExists($key));
        $this->assertFalse($this->cache->get($key));
        // Set.
        $value_in = [
            123 => false,
            321 => true,
        ];
        $this->cache->set($key, $value_in);
        $this->assertTrue($this->cache->cacheKeyExists($key));
        // Test - getting whole array.
        $value_out = $this->cache->get($key);
        $this->assertTrue(is_array($value_out));
        $this->assertEquals(2, sizeof($value_out));
        $this->assertFalse($value_out[123]);
        // Test - via lookup.
        $ret_scalar = $this->cache->get($key, 321);
        $this->assertTrue($ret_scalar);
        $ret_array = $this->cache->get($key, [321]);
        $this->assertTrue(is_array($ret_array));
        $this->assertTrue($ret_array[321]);
    }

    /**
     * Test an array lookup by object IDs
     * @return void
     */
    public function testArrayById(): void
    {
        $key = 'testArrayById';
        // Unset.
        $this->assertFalse($this->cache->cacheKeyExists($key));
        $this->assertFalse($this->cache->get($key));
        // Set by specific ID.
        $value_in = (object)[
            'key' => 123,
        ];
        $this->cache->setById($key, 'key', $value_in);
        $this->assertTrue($this->cache->cacheKeyExists($key));
        // Test.
        $value_out = $this->cache->get($key);
        $this->assertTrue(is_array($value_out));
        $this->assertEquals(1, sizeof($value_out));
        $this->assertTrue(isset($value_out[123]));
    }

    /**
     * Test an array lookup by appending
     * @return void
     */
    public function testArrayAppend(): void
    {
        $key = 'testArrayAppend';
        // Unset.
        $this->assertFalse($this->cache->cacheKeyExists($key));
        $this->assertFalse($this->cache->get($key));
        // Set the base array.
        $value_in = [
            123 => false,
        ];
        $this->cache->set($key, $value_in);
        $this->assertTrue($this->cache->cacheKeyExists($key));
        // Add a secondary value.
        $value_append = true;
        $this->cache->append($key, $value_append);
        // Test.
        $value_out = $this->cache->get($key);
        $this->assertTrue(is_array($value_out));
        $this->assertEquals(2, sizeof($value_out));
        $this->assertFalse($value_out[123]);
        $this->assertTrue($value_out[0]);
    }

    /**
     * Test an array lookup by merging
     * @return void
     */
    public function testArrayMerge(): void
    {
        $key = 'testArrayMerge';
        // Unset.
        $this->assertFalse($this->cache->cacheKeyExists($key));
        $this->assertFalse($this->cache->get($key));
        // Set the base array.
        $value_in = [
            123 => false,
            321 => false,
        ];
        $this->cache->set($key, $value_in);
        $this->assertTrue($this->cache->cacheKeyExists($key));
        $value_merge = [
            321 => true,
        ];
        $this->cache->merge($key, $value_merge);
        // Test.
        $value_out = $this->cache->get($key);
        $this->assertTrue(is_array($value_out));
        $this->assertEquals(2, sizeof($value_out));
        $this->assertFalse($value_out[123]);
        $this->assertTrue($value_out[321]);
    }

    /**
     * Test for determining missing keys
     * @return void
     */
    public function testMissingKeys(): void
    {
        $key = 'testMissingKeys';
        // Unset.
        $this->assertFalse($this->cache->cacheKeyExists($key));
        $this->assertFalse($this->cache->get($key));
        // Set the base array.
        $value_in = [
            123 => false,
            321 => false,
        ];
        $this->cache->set($key, $value_in);
        $this->assertTrue($this->cache->cacheKeyExists($key));
        // Test - unknown object.
        $this->assertEquals(123, $this->cache->determineMissing('unknownCache', 123));
        // Test - as scalar.
        $value_out = $this->cache->determineMissing($key, 456);
        $this->assertIsArray($value_out);
        $this->assertEquals(1, sizeof($value_out));
        $this->assertTrue(isset($value_out[456]));
        // Test - as array.
        $value_out = $this->cache->determineMissing($key, [456]);
        $this->assertIsArray($value_out);
        $this->assertEquals(1, sizeof($value_out));
        $this->assertTrue(isset($value_out[456]));
        // Test - as array (includes known).
        $value_out = $this->cache->determineMissing($key, [123, 456]);
        $this->assertIsArray($value_out);
        $this->assertEquals(1, sizeof($value_out));
        $this->assertTrue(isset($value_out[456]));
    }
}
