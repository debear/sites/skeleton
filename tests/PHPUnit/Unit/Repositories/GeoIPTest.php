<?php

namespace Tests\PHPUnit\Unit\Repositories;

use Tests\PHPUnit\Base\UnitTestCase;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Repositories\GeoIP;

class GeoIPTest extends UnitTestCase
{
    /**
     * Instantiated GeoIP object
     * @var GeoIP
     */
    protected $geoip;

    /**
     * Prepare the object by setting up the GeoIP instance
     * @return void
     */
    protected function setUp(): void
    {
        // Load the standard test setup methods.
        parent::setUp();
        // Instantiate our class.
        $this->geoip = new GeoIP();
    }

    /**
     * Test a localhost lookup
     * @return void
     */
    public function testLocal(): void
    {
        $this->assertWorker('local');

        // Extra: Distance to Royal Observitory (51.4768418, 0)... to 1m resolution (1m == 0.001km).
        $this->assertEqualsWithDelta(2.575, $this->geoip->calculateDistance(51.4768418, 0), 0.001);
    }

    /**
     * Test a lookup in the UK
     * @return void
     */
    public function testUnitedKingdom(): void
    {
        $this->assertWorker('gb');
    }

    /**
     * Test a lookup in France
     * @return void
     */
    public function testFrance(): void
    {
        $this->assertWorker('fr');
    }

    /**
     * Test a lookup in Russia
     * @return void
     */
    public function testRussia(): void
    {
        $this->assertWorker('ru');
    }

    /**
     * Test a lookup in the UAE
     * @return void
     */
    public function testUnitedArabEmirates(): void
    {
        $this->assertWorker('ae');
    }

    /**
     * Test a lookup in Japan
     * @return void
     */
    public function testJapan(): void
    {
        $this->assertWorker('jp');
    }

    /**
     * Test lookups in the United States
     * @return void
     */
    public function testUnitedStates(): void
    {
        $this->assertWorker('us');
    }

    /**
     * Test an invalid IP lookup
     * @return void
     */
    public function testInvalid(): void
    {
        // Valid format.
        $this->geoip->updateIp('256.256.256.256');
        $this->assertEquals('EU', $this->geoip->getContinent());
        $this->assertEquals('GB', $this->geoip->getCountry());
        $this->assertEquals('Bath', $this->geoip->getCity());

        // Garbage string.
        $this->geoip->updateIp('invalid string');
        $this->assertEquals('EU', $this->geoip->getContinent());
        $this->assertEquals('GB', $this->geoip->getCountry());
        $this->assertEquals('Bath', $this->geoip->getCity());
    }

    /**
     * Worker method for processing an IP
     * @param string $testKey The config key with the test details to run.
     * @return void
     */
    protected function assertWorker(string $testKey): void
    {
        // Get the details out of the tests to run.
        $tests = FrameworkConfig::get("debear.tests.geoip.$testKey");
        if (isset($tests['ip'])) {
            // In some instances, we have multiple tests per key, so ensure all inputs match this mechanism.
            $tests = [$tests];
        }

        // Run the GeoIP calcs and tests.
        foreach ($tests as $test) {
            $this->geoip->updateIp($test['ip']);

            // Lat/Long.
            // We could be looking for something specific (value set)
            // or just returns something (unset).
            if (isset($test['lat']) || isset($test['lng'])) {
                $aLatLng = $this->geoip->getLatLong();
                if (isset($test['lat'])) {
                    $this->assertEquals($test['lat'], $aLatLng['lat']);
                }
                if (isset($test['lng'])) {
                    $this->assertEquals($test['lng'], $aLatLng['lng']);
                }
            }

            // Continent.
            $this->assertEquals($test['continent']['code'], $this->geoip->getContinent());
            $this->assertEquals($test['continent']['name'], $this->geoip->getContinentName());

            // Country.
            $this->assertEquals($test['country']['code'], $this->geoip->getCountry());
            $this->assertEquals($test['country']['name'], $this->geoip->getCountryName());

            // City (Name).
            if (is_string($test['city']['name'])) {
                $this->assertEquals($test['city']['name'], $this->geoip->getCity());
            } else {
                $this->assertContains($this->geoip->getCity(), $test['city']['name']);
            }
            // City (Full).
            if (is_string($test['city']['full'])) {
                $this->assertEquals($test['city']['full'], $this->geoip->getCityFull());
            } else {
                $this->assertContains($this->geoip->getCityFull(), $test['city']['full']);
            }
        }
    }
}
