<?php

namespace Tests\PHPUnit\Unit\Repositories;

use Tests\PHPUnit\Base\UnitTestCase;
use DeBear\Repositories\DataObject;

class DataObjectTest extends UnitTestCase
{
    /**
     * Test the functionality of the class
     * @return void
     */
    public function testFunctionality(): void
    {
        $raw = [
            'a' => 1,
            'b' => 2,
            'c' => 4,
            'd' => 7,
        ];
        $obj = new DataObject($raw);
        $this->assertEquals(1, $obj->a);
        $this->assertEquals(2, $obj->b);
        $this->assertEquals(4, $obj->c);
        $this->assertEquals(7, $obj->d);
        $obj->d = 8;
        $this->assertEquals(8, $obj->d);
        $this->assertNull($obj->e);
        $json = $obj->toJSON();
        $this->assertIsArray($json);
        $this->assertEquals(count($raw), count($json));
        $raw['d'] = $obj->d; // Account for our tweak above.
        $this->assertEquals($raw, $json);
    }
}
