<?php

namespace Tests\PHPUnit\Unit\Repositories;

use Tests\PHPUnit\Base\UnitTestCase;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Repositories\SocialMedia;

class SocialMediaTest extends UnitTestCase
{
    /**
     * Test the Twitter handling
     * @return void
     */
    public function testTwitterBasics(): void
    {
        $sm = new SocialMedia();
        // Initially, unset.
        $this->assertFalse($sm->getTwitterAccount('master'));
        // Load our test master creds.
        $sm->setup(FrameworkConfig::get('debear.social.twitter.creds.test'));
        $this->assertEquals('DeBearTestA', $sm->getTwitterAccount('master'));
        $this->assertEquals('DeBearTestB', $sm->getTwitterAccount('secondary'));
        $this->assertEquals('dev', $sm->getTwitterApp());

        // Display the footer widget.
        ob_start();
        $num_links = $sm->displayFooterWidget();
        $disp = ob_get_clean();
        $this->assertEquals(1, $num_links);
        $this->assertStringContainsString('@DeBearTestA', $disp);

        // Then switch to the secondary account.
        $sm->switchTwitterAccountType('master', 'invalid');
        $sm->switchTwitterAccountType('master', 'secondary');
        $this->assertFalse($sm->getTwitterAccount('master'));
    }
}
