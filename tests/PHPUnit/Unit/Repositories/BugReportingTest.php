<?php

namespace Tests\PHPUnit\Unit\Repositories;

use Illuminate\Support\Facades\App;
use Tests\PHPUnit\Base\UnitTestCase;
use DeBear\Repositories\BugReporting;
use DeBear\Exceptions\TestingException;

class BugReportingTest extends UnitTestCase
{
    /**
     * Test the handling of exceptions
     * @return void
     */
    public function testHandles(): void
    {
        $br = new BugReporting();

        // Generate an error manually.
        $br->handleError(E_USER_ERROR, 'Error String', 'error.txt', 123);
        // Generate an exception.
        $e = new TestingException('Test Exception', 12);
        $br->handleException($e);

        // Check we don't error when saving.
        $br->handleShutdown();
        $this->assertTrue(true);
    }
}
