<?php

namespace Tests\PHPUnit\Unit\Repositories;

use Tests\PHPUnit\Base\UnitTestCase;
use DeBear\Repositories\Dropdown;

class DropdownTest extends UnitTestCase
{
    /**
     * Test additional features
     * @return void
     */
    public function testAdditional(): void
    {
        $opt = [
            'Label 1',
            [
                'id' => 'label-2',
                'label' => 'Label 2',
                'classes' => 'css',
            ]
        ];
        $dropdown = new Dropdown('ci-id', $opt, ['searchable' => true]);
        $rendered = $dropdown->render();
        $this->assertStringContainsString(
            '<input type="text" class="textbox search" placeholder="Enter Search Term">',
            $rendered
        );
        $this->assertStringContainsString(
            '<li  data-id="Label 1" data-search="label 1">Label 1</li>',
            $rendered
        );
        $this->assertStringContainsString(
            '<li class="css" data-id="label-2" data-search="label 2">Label 2</li>',
            $rendered
        );

        // The parsed options.
        $parsed = [];
        foreach ($dropdown->getOptions() as $item) {
            $parsed[] = $item->id;
        }
        $this->assertEmpty($parsed[0]);
        $this->assertNull($parsed[1]);
        $this->assertEquals('Label 1', $parsed[2]);
        $this->assertEquals('label-2', $parsed[3]);
    }
}
