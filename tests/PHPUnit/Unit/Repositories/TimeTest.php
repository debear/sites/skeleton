<?php

namespace Tests\PHPUnit\Unit\Repositories;

use Tests\PHPUnit\Base\UnitTestCase;
use DeBear\Repositories\Time;
use DeBear\Models\Skeleton\User;
use Carbon\Carbon;

class TimeTest extends UnitTestCase
{
    /**
     * Test the formatting and processing of server times
     * @return void
     */
    public function testBasic(): void
    {
        // Prepare our test.
        $time = Time::object();
        $time->resetCache('2020-09-01 12:34:56');

        // Server - Europe/London.
        $this->assertEquals(1598960096, $time->getServerNow());
        $this->assertEquals('2020-09-01 12:34:56', $time->getServerNowFmt());
        $this->assertEquals('2020-09-01', $time->getServerCurDate());
        $this->assertEquals('12:34:56', $time->getServerCurTime());
        $this->assertEquals('Europe/London', $time->getServerTimezone());

        // Formatting.
        $this->assertEquals('20200901_123456', $time->formatServer('Ymd_His'));
        $this->assertEquals('20200901_123457', $time->adjustFormatServer('+1 second', 'Ymd_His'));
    }

    /**
     * Test the formatting and processing of application times
     * @return void
     */
    public function testApp(): void
    {
        // Prepare our test.
        $time = Time::object();
        $this->setTestConfig(['debear.datetime.timezone_app' => 'America/New_York']);

        // App - America/New_York.
        $time->resetCache('2020-09-01 12:34:56');
        $time->setup();
        $this->assertEquals(1598960096, $time->getNow());
        $this->assertEquals('2020-09-01 07:34:56', $time->getNowFmt());
        $this->assertEquals('2020-09-01 12:34:56', $time->getServerNowFmt());
        $this->assertEquals('2020-09-01', $time->getCurDate());
        $this->assertEquals('07:34:56', $time->getCurTime());
        $this->assertEquals('America/New_York', $time->getTimezone());

        // Formatting.
        $this->assertEquals('20200901_073456', $time->format('Ymd_His'));
        $this->assertEquals('20200901_073457', $time->adjustFormat('+1 second', 'Ymd_His'));
    }

    /**
     * Test the formatting and processing of database times
     * @return void
     */
    public function testDatabase(): void
    {
        // Prepare our test.
        $time = Time::object();
        $this->setTestConfig(['debear.datetime.timezone_db' => 'America/New_York']);

        // Run.
        $time->resetCache('2020-09-01 12:34:56');
        $this->assertEquals(1598960096, $time->getDatabaseNow());
        $this->assertEquals('2020-09-01 07:34:56', $time->getDatabaseNowFmt());
        $this->assertEquals('2020-09-01', $time->getDatabaseCurDate());
        $this->assertEquals('07:34:56', $time->getDatabaseCurTime());

        // Formatting.
        $this->assertEquals('20200901_073456', $time->formatDatabase('Ymd_His'));
        $this->assertEquals('20200901_073457', $time->adjustFormatDatabase('+1 second', 'Ymd_His'));

        // Test and update the timezone.
        $this->assertEquals('America/New_York', $time->getDatabaseTimezone());
        $time->setDatabaseTimezone('US/Eastern');
        $this->assertEquals('US/Eastern', $time->getDatabaseTimezone());
    }

    /**
     * Test the formatting and processing of user-localised times
     * @return void
     */
    public function testUser(): void
    {
        // Prepare our test.
        $time = Time::object();

        // Run.
        $time->resetCache('2020-09-01 12:34:56');
        $time->setUserTimezone('Europe/Paris');
        $this->assertEquals(1598960096, $time->getUserNow());
        $this->assertEquals('2020-09-01 13:34:56', $time->getUserNowFmt());
        $this->assertEquals('2020-09-01', $time->getUserCurDate());
        $this->assertEquals('13:34:56', $time->getUserCurTime());
        $this->assertEquals('Europe/Paris', $time->getUserTimezone());

        // Formatting.
        $this->assertEquals('20200901_133456', $time->formatUser('Ymd_His'));
        $this->assertEquals('20200901_133457', $time->adjustFormatUser('+1 second', 'Ymd_His'));
    }

    /**
     * Test the formatting and processing of times in alternative-timezones
     * @return void
     */
    public function testAlternative(): void
    {
        // Prepare our test.
        $time = Time::object();

        // Run.
        $time->resetCache('2020-09-01 12:34:56');
        $this->assertEquals(1598960096, $time->getAltTimezoneNow('Europe/Paris'));
        $this->assertEquals('2020-09-01 13:34:56', $time->getAltTimezoneNowFmt('Europe/Paris'));
        $this->assertEquals('2020-09-01', $time->getAltTimezoneCurDate('Europe/Paris'));
        $this->assertEquals('13:34:56', $time->getAltTimezoneCurTime('Europe/Paris'));

        // Formatting.
        $this->assertEquals('20200901_133456', $time->formatAlt('Europe/Paris', 'Ymd_His'));
        $this->assertEquals('20200901_133457', $time->adjustFormatAlt('Europe/Paris', '+1 second', 'Ymd_His'));
    }

    /**
     * Test the Carbon localisation process
     * @return void
     */
    public function testLocalise(): void
    {
        // Prepare our test.
        User::doLogin(User::find(10));
        $time = Time::object();

        // Localise and test - Europe/London.
        $time->resetCache('2020-09-01 12:34:56');
        $raw = new Carbon('2020-09-02 00:00:00');
        $localised = $time->localiseDatabase($raw);
        $this->assertEquals('2020-09-02 00:00:00', $localised->toDateTimeString());

        // Localise and test - America/New_York.
        $this->setTestConfig(['debear.datetime.timezone_db' => 'America/New_York']);
        $time->resetCache('2020-09-01 12:34:56');
        $raw = new Carbon('2020-09-02 00:00:00');
        $localised = $time->localiseDatabase($raw);
        $this->assertEquals('2020-09-02 05:00:00', $localised->toDateTimeString());

        // Restore the config changes.
        User::doLogout();
    }

    /**
     * Test the conversation of absolute (i.e., not current) times
     * @return void
     */
    public function testAbsolute(): void
    {
        $time = Time::object();

        // Timestamp.
        $this->assertEquals('2020-09-01 12:34:56', $time->formatTime(1598960096));
        // Date/Time.
        $this->assertEquals('2020-09-01 12:34:56', $time->formatTime('2020-09-01 12:34:56'));
    }
}
