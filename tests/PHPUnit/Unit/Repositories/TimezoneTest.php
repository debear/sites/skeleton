<?php

namespace Tests\PHPUnit\Unit\Repositories;

use Tests\PHPUnit\Base\UnitTestCase;
use DeBear\Repositories\Timezones;

class TimezoneTest extends UnitTestCase
{
    /**
     * Test the database timezones.
     * @return void
     */
    public function testTimezones(): void
    {
        $tz = new Timezones(['no_group' => true]);
        // Get the list.
        $tz_list = $tz->getTimezones();
        $this->assertIsArray($tz_list);
        $this->assertCount(393, $tz_list);
        // Various scans.
        $this->assertEquals('London', $tz_list[299]->city);
        $this->assertEquals('Europe/London', $tz_list[299]->timezone);
        $this->assertEquals('United Kingdom', $tz_list[299]->country);
        $this->assertEquals('gb', $tz_list[299]->country_code);
        $this->assertEquals('Paris', $tz_list[308]->city);
        $this->assertEquals('New York', $tz_list[129]->city);
        $this->assertEquals('Auckland', $tz_list[344]->city);
    }

    /**
     * Test the database timezones by country.
     * @return void
     */
    public function testTimezonesByCountry(): void
    {
        $tz = new Timezones();
        // Get the list.
        $tz_list = $tz->getTimezones();
        $this->assertIsArray($tz_list);
        $this->assertCount(242, $tz_list);
        // Index 229 = GB.
        $gb = $tz_list[229];
        $this->assertEquals('gb', $gb['code']);
        $this->assertCount(4, $gb['list']);
        $this->assertEquals([
            'London',
            'Guernsey',
            'Jersey',
            'Isle of Man',
        ], array_column($gb['list'], 'city'));
        // Re-get and test.
        $tz_list_alt = $tz->getTimezones();
        $this->assertCount(242, $tz_list_alt);
    }

    /**
     * Test the database timezones by GeoIP locality (raw).
     * @return void
     */
    public function testTimezonesByGeoIP(): void
    {
        $tz = new Timezones(['geoip' => true, 'no_group' => true]);
        // Get the list.
        $tz_list = $tz->getTimezones();
        $this->assertIsArray($tz_list);
        $this->assertCount(393, $tz_list);
        // First (closest) = London.
        $this->assertEquals('Europe/London', $tz_list[0]->timezone);
        $this->assertEquals('London', $tz_list[0]->city);
        $this->assertEquals('gb', $tz_list[0]->country_code);
        // Last (furthest) = Chatham (NZ).
        $this->assertEquals('Pacific/Chatham', $tz_list[392]->timezone);
        $this->assertEquals('Chatham', $tz_list[392]->city);
        $this->assertEquals('nz', $tz_list[392]->country_code);
    }

    /**
     * Test the database timezones by GeoIP locality (by country).
     * @return void
     */
    public function testTimezonesByGeoIPCountry(): void
    {
        $tz = new Timezones(['geoip' => true]);
        // Get the list.
        $tz_list = $tz->getTimezones();
        $this->assertIsArray($tz_list);
        $this->assertCount(242, $tz_list);
        $tz_keys = array_keys($tz_list);
        // First (closest) = GB.
        $this->assertEquals('gb', $tz_keys[0]);
        $closest = $tz_list['gb'];
        $this->assertCount(4, $closest);
        $this->assertEquals([
            'London',
            'Guernsey',
            'Jersey',
            'Isle of Man',
        ], array_column($closest, 'city'));
        // Last (furthest) = NZ.
        $this->assertEquals('nz', $tz_keys[241]);
        $furthest = $tz_list['nz'];
        $this->assertCount(2, $furthest);
        $this->assertEquals([
            'Auckland',
            'Chatham',
        ], array_column($furthest, 'city'));
    }
}
