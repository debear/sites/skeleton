<?php

namespace Tests\PHPUnit\Unit\Repositories;

use Illuminate\Support\Facades\App;
use Tests\PHPUnit\Base\UnitTestCase;
use DeBear\Repositories\File;
use DeBear\Repositories\Logging;
use DeBear\Repositories\DatabaseLogging;
use DeBear\Helpers\Benchmark;
use DeBear\ORM\Skeleton\User;

class FileAndLoggingTest extends UnitTestCase
{
    /**
     * Test file operations
     * @return void
     */
    public function testFileOps(): void
    {
        $filename = env('APP_DIR_CI_TMP') . '/' . getmypid() . '.txt';
        $file = new File($filename);
        // Write some lines to the file.
        $file->write("Line 1");
        $file->write("Line 2");
        $file->write("Line 3");
        $file->write("Line 4");
        $file->write("Line 5");
        $file->save();
        // Confirm it was saved succesfully.
        $this->assertTrue(file_exists($filename));
        // And get it back out again.
        $f_str = $file->read();
        $this->assertEquals("Line 1\nLine 2\nLine 3\nLine 4\nLine 5\n", $f_str);
        $f_arr = $file->readAsArray();
        $this->assertIsArray($f_arr);
        $this->assertCount(5, $f_arr);
        $this->assertEquals([
            "Line 1\n",
            "Line 2\n",
            "Line 3\n",
            "Line 4\n",
            "Line 5\n",
        ], $f_arr);

        // Now try to write a file that will generate an error.
        $filename = env('APP_DIR_CI_TMP') . '/' . getmypid() . '-empty.txt';
        $file_empty = new File($filename);
        $file_empty->save();
        $this->assertTrue(file_exists($filename));
        $this->assertEquals(0, filesize($filename));
    }

    /**
     * Test the logging mechanism
     * @return void
     */
    public function testLogging(): void
    {
        Logging::setup();
        Benchmark::scriptStart();
        Logging::add('CI', 'Test: Top');
        // Disable, and save.
        Logging::disable();
        Logging::add('CI', 'Test: Disabled');
        Benchmark::scriptEnd();
        Logging::close();
        // Ensure we got out what we put in.
        $logs = Logging::get();
        $this->assertCount(1, $logs); // Only the one message - as we're disabled the second is ignored.
        $this->assertIsArray($logs[0]);
        $this->assertEquals('CI', $logs[0]['type']);
        $this->assertEquals('Test: Top', $logs[0]['msg']);
    }

    /**
     * Test message reporting
     * @return void
     */
    public function testReporting(): void
    {
        $this->setTestConfig(['debear.version.breakdown.rev' => '123:456']);
        Logging::report('ci', [
            'key' => 'value',
        ]);
        // Ensure we didn't error.
        $this->assertTrue(true);
    }

    /**
     * Test the logging of database queries when disabled
     * @return void
     */
    public function testDatabaseLoggingDisabled(): void
    {
        $this->setTestConfig(['debear.logging.database.enabled' => false]);
        Logging::setup();
        $log_before = Logging::get();
        DatabaseLogging::setup();

        // Run a query.
        $user = User::query()
            ->where(['id' => 10])
            ->get();
        $this->assertEquals('test_user', $user->user_id);
        // Test the reporting produced.
        $this->assertEmpty(DatabaseLogging::getReport());
        // And detailed report (which didn't increase the standard internal log).
        DatabaseLogging::report();
        $log_after = Logging::get();
        $this->assertEquals(sizeof($log_before), sizeof($log_after));
    }

    /**
     * Test the logging of database queries
     * @return void
     */
    public function testDatabaseLoggingSimple(): void
    {
        $this->setTestConfig(['debear.logging.database.enabled' => true]);

        Logging::setup();
        $log_before = Logging::get();

        DatabaseLogging::setup();
        // Run a query.
        $user = User::query()
            ->where(['id' => 10])
            ->get();
        $this->assertEquals('test_user', $user->user_id);
        // Test the reporting produced.
        $report = DatabaseLogging::getReport();
        $this->assertNotEmpty($report);
        $this->assertCount(1, $report);
        $this->assertCount(1, $report['mysql']['queries']);
        $this->assertEquals(1, $report['mysql']['conn']['query_tot']);
        // Re-get and compare.
        $report_alt = DatabaseLogging::getReport();
        $this->assertEquals($report_alt, $report);

        // Get the detailed report.
        $this->assertFalse(DatabaseLogging::enabledQueryLog());
        DatabaseLogging::report();
        $log_after = Logging::get();
        $this->assertEquals(sizeof($log_before) + 1, sizeof($log_after));
        $base_key = sizeof($log_before);
        $this->assertEquals('Database: mysql', $log_after[$base_key]['type']);
        $this->assertEquals('Queries: 1', substr($log_after[$base_key]['msg'], 0, 10));
    }

    /**
     * Test the logging of database queries with additional info provided
     * @return void
     */
    public function testDatabaseLoggingExtended(): void
    {
        $this->setTestConfig([
            'debear.logging.database.enabled' => true,
            'debear.logging.database.extended.slow_log' => true,
            'debear.logging.database.extended.query_log' => true,
            'debear.logging.database.is_slow' => 0, // Captures everything.
        ]);

        Benchmark::scriptStart();
        Logging::setup();
        $log_before = Logging::get();

        DatabaseLogging::setup();
        // Run a query.
        $user = User::query()
            ->where(['id' => 10])
            ->get();
        $this->assertEquals('test_user', $user->user_id);
        // Get the detailed report.
        $this->assertTrue(DatabaseLogging::enabledQueryLog());
        DatabaseLogging::report();
        $log_after = Logging::get();
        $this->assertEquals(sizeof($log_before) + 3, sizeof($log_after));
        $base_key = sizeof($log_before);
        $this->assertEquals('Database: mysql', $log_after[$base_key]['type']);
        $this->assertEquals('Queries: 1', substr($log_after[$base_key]['msg'], 0, 10));
        $this->assertEquals('Database: mysql', $log_after[$base_key + 1]['type']);
        $this->assertEquals('Slow Query: ', substr($log_after[$base_key + 1]['msg'], 0, 12));
        $this->assertEquals('Database: mysql', $log_after[$base_key + 2]['type']);
        $this->assertEquals('Query: ', substr($log_after[$base_key + 2]['msg'], 0, 7));

        // The rendered report.
        $this->assertTrue(DatabaseLogging::enabledDisplay());
        $response = response("MySQL: <!-- TotalTime: mysql -->\nUnknown: <!-- TotalTime: mysql_unknown -->");
        Benchmark::scriptEnd();
        DatabaseLogging::render($response);
        $this->assertStringNotContainsString('MySQL: <!-- TotalTime: mysql -->', $response->getContent());
        $this->assertStringContainsString('Unknown: <!-- TotalTime: mysql_unknown -->', $response->getContent());
        // An empty report does not error.
        DatabaseLogging::render(null);
        $this->assertTrue(true);
        // Test the effect on enabling and disabling.
        $log_base = sizeof(Logging::get());
        Logging::add('test', 'Message');
        $this->assertEquals($log_base + 1, sizeof(Logging::get()));
        Logging::disable();
        Logging::add('test', 'Message');
        $this->assertEquals($log_base + 1, sizeof(Logging::get()));
        Logging::enable();
        Logging::add('test', 'Message');
        $this->assertEquals($log_base + 2, sizeof(Logging::get()));
    }

    /**
     * Tidy up internal object changes between tests
     * @return void
     */
    protected function tearDown(): void
    {
        Logging::reset();
        DatabaseLogging::reset();
        parent::tearDown();
    }
}
