<?php

namespace Tests\PHPUnit\Unit\Exceptions;

use Illuminate\Support\Facades\App;
use Illuminate\HTTP\Request;
use DivisionByZeroError;
use Tests\PHPUnit\Base\UnitTestCase;
use DeBear\Exceptions\Handler;

class HandlerTest extends UnitTestCase
{
    /**
     * Test our core handler
     * @return void
     */
    public function testHandler(): void
    {
        // Get our handler, then throw an Exception we want caught.
        $handler = App::make(Handler::class);
        try {
            (1 / 0);
        } catch (DivisionByZeroError $e) {
            // Generate a faux response and rendering the exception.
            $response = $handler->render(request(), $e);
            $content = $response->getContent();
        }

        // Now perform our tests.
        $this->assertNotNull($response);
        $this->assertNotNull($content);
        $this->assertStringContainsString('<!-- Division by zero (500 Internal Server Error) -->', $content);
        $this->assertStringContainsString('<title>Division by zero (500 Internal Server Error)</title>', $content);
        $this->assertStringContainsString('<div class="exception-metadata">
        <div class="container">
            <h2 class="exception-hierarchy">
                                <a href="#trace-box-1"><abbr title="DivisionByZeroError">DivisionByZeroError</abbr></a>
            </h2>
            <h2 class="exception-http">
                HTTP 500 <small>Internal Server Error</small>
            </h2>
        </div>
    </div>', $content);
    }

    /**
     * Test our core handler when an API / JSON request is made
     * @return void
     */
    public function testAPIHandler(): void
    {
        // Get our handler, then throw an Exception we want caught.
        $handler = App::make(Handler::class);
        try {
            (1 / 0);
        } catch (DivisionByZeroError $e) {
            // Generate a faux response and rendering the JSON exception.
            $request = new Request([], [], [], [], [], ['HTTP_ACCEPT' => 'application/json'], null);
            $this->assertTrue($request->expectsJson());
            $response = $handler->render($request, $e);
            $content = $response->getContent();
        }

        // Now perform our tests.
        $this->assertNotNull($response);
        $this->assertNotNull($content);
        $this->assertIsString($content);
        $json = json_decode($content, true);
        $this->assertIsArray($json);
        $this->assertEquals('The request was not completed. The server met an unexpected condition.', $json['msg']);
    }
}
