<?php

namespace Tests\PHPUnit\Unit\Implementations;

use Illuminate\Support\Facades\App;
use Tests\PHPUnit\Base\UnitTestCase;

class HasherTest extends UnitTestCase
{
    /**
     * Test the algorithm info parser
     * @return void
     */
    public function testInfo(): void
    {
        $bcrypt_hash = password_hash('12345abcde', PASSWORD_BCRYPT);
        $bcrypt_algo = App::make('hash')->info($bcrypt_hash);
        $this->assertIsArray($bcrypt_algo);
        $this->assertEquals('bcrypt', $bcrypt_algo['algoName']);
    }

    /**
     * Test making a hash
     * @return void
     */
    public function testMake(): void
    {
        $hasher = App::make('hash');
        $salt = 'AxXfApaD0efxrhlO';
        // Salt only.
        $this->assertEquals('c0373e9c212885dec823c2436d05a6', $hasher->make('12345abcde', ['salt' => $salt]));
        // This differs from the default salt.
        $this->assertNotEquals('c0373e9c212885dec823c2436d05a6', $hasher->make('12345abcde'));
        // Salt and length.
        $this->assertEquals('c0373e9c2128', $hasher->make('12345abcde', ['salt' => $salt, 'len' => 12]));
    }

    /**
     * Test the hash hecking
     * @return void
     */
    public function testCheck(): void
    {
        $hasher = App::make('hash');
        $salt = 'UyvJI1mBDTA6ISbl';
        // Standard.
        $this->assertTrue($hasher->check('12345abcde', '5204c8870d7b6783a4293365fc65a8', ['salt' => $salt]));
        // Legacy.
        $this->assertTrue($hasher->check('12345abcde', '5204c8870d7b', ['salt' => $salt, 'len' => 12]));
    }

    /**
     * Test a required, but unused, method
     * @return void
     */
    public function testNeedsRehash(): void
    {
        $this->assertFalse(App::make('hash')->needsRehash('cf60bf754c6dec83829d1cb41ce6a7'));
    }
}
