<?php

namespace Tests\PHPUnit\Unit\Implementations;

use Tests\PHPUnit\Base\UnitTestCase;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\SessionAggregation;
use DeBear\Implementations\SessionHandler;

class SessionAggregationTest extends UnitTestCase
{
    /**
     * Test the aggregation process, which is enabled, storing and processes the stats
     * The disabled test is performed as a Feature test
     * @return void
     */
    public function testEnabled(): void
    {
        $obj = SessionHandler::get();
        // Enable the "CI mode" in our session handler.
        $config = FrameworkConfig::get('debear.sessions.handler.enabled');
        FrameworkConfig::set(['debear.sessions.handler.enabled' => true]);
        // Standardise the data we'll use in our tests.
        $session_id = 'ci-session';
        $session_data = '{"json":true}';
        // Prepare the session.
        $this->assertTrue($obj->open('/tmp/session', $session_id));
        SessionAggregation::prepare();
        $agg_id = SessionAggregation::getID();
        // Do stuff with the session.
        $obj->write($session_id, $session_data);
        $this->assertEquals($session_data, $obj->read($session_id));
        // Now re-run the aggregation preparation, nuking the current version.
        session()->forget('session_stats');
        $this->assertFalse(session()->has('session_stats'));
        SessionAggregation::prepare();
        $this->assertTrue(session()->has('session_stats'));
        $this->assertEquals($agg_id, SessionAggregation::getID());
        // Then close the session.
        SessionAggregation::process();
        $this->assertTrue($obj->destroy($session_id));
        $this->assertTrue($obj->close());
        $obj->gc(FrameworkConfig::get('debear.sessions.lifetime') * 60);
        // Revert the "CI mode" in our session handler.
        FrameworkConfig::set(['debear.sessions.handler.enabled' => $config]);
    }
}
