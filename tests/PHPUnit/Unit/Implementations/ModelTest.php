<?php

namespace Tests\PHPUnit\Unit\Implementations;

use Tests\PHPUnit\Base\UnitTestCase;
use Carbon\Carbon;
use DeBear\Models\Skeleton\CMSContent;
use DeBear\Models\Skeleton\News;
use DeBear\Models\Skeleton\User;

class ModelTest extends UnitTestCase
{
    /**
     * Test the generic attribute getter
     * @return void
     */
    public function testGetters(): void
    {
        $model = User::find(10);
        $this->assertInstanceOf(User::class, $model);
        $this->assertEquals(10, $model->id);
        $this->assertEquals('test_user', $model->user_id);
        $this->assertEquals('Test', $model->forename);
        $this->assertEquals('User', $model->surname);
        $this->assertEquals('Test User', $model->name);
        $this->assertInstanceOf(Carbon::class, $model->account_verified);
    }

    /**
     * Test creating and saving a record with a single primary key
     * @return void
     */
    public function testPrimaryCreate(): void
    {
        $model = CMSContent::createOrUpdate(
            [
                'content_id' => null,
                'site' => 'ci-model',
                'page' => 'create',
                'section' => 'intro',
                'order' => 10,
                'content' => '<p>Hello, world!</p>',
            ],
            ['page', 'section', 'order', 'content']
        );
        $this->assertNotNull($model->content_id);
        $this->assertEquals('ci-model', $model->site);
        $this->assertEquals('create', $model->page);
    }

    /**
     * Test updating and saving a record with a single primary key
     * @return void
     */
    public function testPrimaryUpdate(): void
    {
        // Get the record we created above.
        $id = CMSContent::select('content_id')->where('site', 'ci-model')->first()->content_id;
        // And then call createOrUpdate again to update it.
        $model = CMSContent::createOrUpdate(
            [
                'content_id' => $id,
                'site' => 'ci-model',
                'page' => 'update',
                'section' => 'intro',
                'order' => 10,
                'content' => '<p>Hello, world!</p>',
            ],
            ['page', 'section', 'order', 'live', 'content']
        );
        $this->assertEquals($id, $model->content_id);
        $this->assertEquals('ci-model', $model->site);
        $this->assertEquals('update', $model->page);
        $this->assertEquals(0, $model->live);
    }

    /**
     * Test creating and saving a record with a compound primary key
     * @return void
     */
    public function testCompoundCreate(): void
    {
        $model = News::createOrUpdate(
            [
                'app' => 'ci-model',
                'news_id' => null,
                'source_id' => 1,
                'title' => 'Test Article',
                'description' => '<p>Hello, world</p>',
            ],
            ['title', 'description']
        );
        $this->assertEquals('ci-model', $model->app);
        $this->assertEquals(1, $model->source_id);
        $this->assertEquals('Test Article', $model->title);
    }

    /**
     * Test updating and saving a record with a compound primary key
     * @return void
     */
    public function testCompoundUpdate(): void
    {
        // Get the record we created above.
        $id = News::select('news_id')->where('app', 'ci-model')->first()->news_id;
        // And then call createOrUpdate again to update it.
        $model = News::createOrUpdate(
            [
                'app' => 'ci-model',
                'news_id' => $id,
                'source_id' => 1,
                'title' => 'Test Article',
                'description' => '<p>Hello, world!</p>',
            ],
            ['title', 'description', 'published']
        );
        $this->assertEquals('ci-model', $model->app);
        $this->assertEquals($id, $model->news_id);
        $this->assertEquals(1, $model->source_id);
        $this->assertEquals('Test Article', $model->title);
    }

    /**
     * Test the handling of dates when converting an object to array
     * @return void
     */
    public function testArrayDates(): void
    {
        $data = User::find(10)->toArray();
        $this->assertEquals('test_user', $data['user_id']);
        $this->assertEquals('1970-01-01', $data['dob']);
        $this->assertEquals('2019-01-01 12:34:56', $data['account_created']);
    }
}
