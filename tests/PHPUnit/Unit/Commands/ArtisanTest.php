<?php

namespace Tests\PHPUnit\Unit\Commands;

use Tests\PHPUnit\Base\UnitTestCase;

class ArtisanTest extends UnitTestCase
{
    /**
     * A test of the env config listing command.
     * @return void
     */
    public function testConfigEnv(): void
    {
        // Run the command, ensure we get an appropriate response.
        $this->artisan('debear:skeleton:config-env')
            ->assertSuccessful();
    }
}
