<?php

namespace Tests\PHPUnit\Unit\Models;

use Tests\PHPUnit\Base\UnitTestCase;
use DeBear\Helpers\Strings;
use DeBear\Models\Skeleton\CommsEmail;
use DeBear\Models\Skeleton\CommsEmailLink;

class EmailsTest extends UnitTestCase
{
    /**
     * Test creating and saving an email with its links.
     * @return void
     */
    public function testProcessing(): void
    {
        // Create the email.
        $email_code = 'ci:' . Strings::randomAlphaNum(13);
        $link_code = substr($email_code, 0, -2) . Strings::randomAlphaNum(2);
        $raw = CommsEmail::build([
            'app' => 'ci-model',
            'email_reason' => 'unknown',
            'email_from' => 'noreply@debear.test',
            'email_to' => 'support@debear.test',
            'email_subject' => 'This is a test message',
            'email_body' => 'Hello, world!',
            'email_queued' => '2020-01-01 00:00:00',
            'email_send' => '2020-01-01 00:00:00',
            'email_status' => 'queued',
            'email_code' => $email_code,
            'links' => [
                CommsEmailLink::build([
                    'link_full' => 'https://debear.test/',
                    'link_protocol' => 'https',
                    'link_domain' => 'debear.test',
                    'link_url' => '/',
                    'link_code' => $link_code,
                ]),
            ],
        ]);
        $raw->save();

        // Now extract it to test the save.
        $email = CommsEmail::with(['user', 'emailLinks'])->where([
            ['app', 'ci-model'],
            ['email_code', $email_code],
        ])->first();
        $this->assertEquals($email_code, $email->email_code);
        $this->assertNull($email->user);
        $this->assertEquals(1, $email->emailLinks->count());
        $this->assertEquals('https://debear.test/', $email->emailLinks->first()->link_full);

        // Get the raw link.
        $link = CommsEmailLink::with('email')->where([
            ['app', 'ci-model'],
            ['link_code', $link_code],
        ])->first();
        $this->assertEquals($link_code, $link->link_code);
        $this->assertEquals('https://debear.test/', $link->link_full);
        $this->assertEquals($email_code, $link->email->email_code);
        $this->assertEquals(0, $link->times_visited);
        $this->assertNull($link->first_visited);
        $this->assertNull($link->last_visited);

        // Now stamp and re-test the counts / visit dates.
        $link->stampVisited();
        $link = CommsEmailLink::with('email')->where([
            ['app', 'ci-model'],
            ['link_code', $link_code],
        ])->first();
        $this->assertEquals(1, $link->times_visited);
        $this->assertNotNull($link->first_visited);
        $this->assertNotNull($link->last_visited);
        $this->assertEquals($link->first_visited, $link->last_visited);

        // Now re-stamp and re-test the counts / visit dates.
        sleep(1);
        $link->stampVisited();
        $link = CommsEmailLink::with('email')->where([
            ['app', 'ci-model'],
            ['link_code', $link_code],
        ])->first();
        $this->assertEquals(2, $link->times_visited);
        $this->assertNotEquals($link->first_visited, $link->last_visited);
    }
}
