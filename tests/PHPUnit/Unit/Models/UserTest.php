<?php

namespace Tests\PHPUnit\Unit\Models;

use Tests\PHPUnit\Base\UnitTestCase;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use Carbon\Carbon;
use DeBear\Helpers\Cookie;
use DeBear\Helpers\Policies;
use DeBear\Models\Skeleton\User;
use DeBear\Models\Skeleton\UserActivityLog;
use DeBear\Models\Skeleton\UserPermissions;
use DeBear\Models\Skeleton\UserRememberMe;
use DeBear\Models\Skeleton\CookiePreferenceLog;
use DeBear\Models\Skeleton\Session;
use DeBear\Models\Skeleton\SessionStats;
use DeBear\Models\Skeleton\UserLevel;
use DeBear\Models\Skeleton\Timezone;

class UserTest extends UnitTestCase
{
    /**
     * Test the various relationships
     * @return void
     */
    public function testRelationships(): void
    {
        // From User.
        $user = User::with(['timezoneInfo', 'permissions', 'rememberMes'])->where('id', 10)->first();
        $this->assertEquals('user_id', $user->username());
        $this->assertEquals('test_user', $user->user_id);
        $this->assertInstanceOf(Timezone::class, $user->timezoneInfo);
        $this->assertEquals('gb', $user->timezoneInfo->code2);
        $this->assertInstanceOf(UserPermissions::class, $user->permissions->first());
        $this->assertEquals(1, $user->permissions->count());
        $this->assertEquals('www', $user->permissions->first()->site);
        $this->assertInstanceOf(UserRememberMe::class, $user->rememberMes->first());
        $this->assertEquals(1, $user->rememberMes->count());
        $this->assertEquals('2119-12-31 23:59:59', $user->rememberMes->first()->expires);

        // Permissions.
        $perms = UserPermissions::with(['user', 'levelInfo'])->where([
            ['user_id', 10],
            ['site', 'www'],
        ])->first();
        $this->assertNull($perms->section);
        $this->assertInstanceOf(User::class, $perms->user);
        $this->assertEquals('test_user', $perms->user->user_id);
        $this->assertEquals('user', $perms->level);
        $this->assertInstanceOf(UserLevel::class, $perms->levelInfo);
        $this->assertFalse($perms->levelInfo->isAdmin());

        // Remember Me.
        $rm = UserRememberMe::with('user')->where('rm_string', 'nWpLcFLTqZXctv4ItGgjyF9OptxU7FMd')->first();
        $this->assertEquals('2119-12-31 23:59:59', $rm->expires);
        $this->assertInstanceOf(User::class, $rm->user);
        $this->assertEquals('test_user', $rm->user->user_id);

        // Activities.
        $log = UserActivityLog::with(['user'])->where('session_id', 'a0a5d507067ab75b')->get();
        $this->assertEquals(2, $log->count());
        $login = $log->firstWhere('user_id', 10);
        $this->assertEquals(0, $login->type);
        $this->assertInstanceOf(User::class, $login->user);
        $this->assertEquals('test_user', $login->user->user_id);
        $this->assertIsString($login->session_id);
        $this->assertNull($login->session);

        // Cookie Preferences.
        $log = CookiePreferenceLog::with(['user'])->where('session_id', 'a0a5d507067ab75b')->get();
        $this->assertEquals(1, $log->count());
        $log = $log->first();
        $this->assertEquals('analytics', $log->setting);
        $this->assertInstanceOf(User::class, $log->user);
        $this->assertEquals('test_user', $log->user->user_id);
        $this->assertIsString($log->session_id);
        $this->assertNull($log->session);

        // Session.
        $session = Session::with('session')->where('session_id_uniq', 'a0a5d507067ab75b')->first();
        $this->assertEquals('2120-01-01 00:00:00', $session->session_started);
        $this->assertEquals('2120-01-01 12:34:56', $session->last_accessed);
        $this->assertNull($session->data);
        $this->assertInstanceOf(SessionStats::class, $session->session);
        $this->assertEquals('Linux', $session->session->os);
        $this->assertEquals('76.0', $session->session->browser_version);
        $this->assertEquals(1, $session->session->visits);
        $this->assertEquals(3, $session->session->hits);
        $this->assertEquals(1, $session->session->logins);
        $this->assertEquals(0, $session->session->logouts);

        // User Levels.
        $levels = UserLevel::whereIn('level', ['user', 'admin'])->get();
        $this->assertFalse($levels->firstWhere('level', 'user')->isAdmin());
        $this->assertTrue($levels->firstWhere('level', 'admin')->isAdmin());
    }

    /**
     * Test the various object data getters
     * @return void
     */
    public function testUserGetters(): void
    {
        $user = User::find(10);
        // Getters.
        $this->assertEquals('test_user', $user->user_id);
        $this->assertIsArray($user->links);
        $this->assertEmpty($user->links);
        // Logged In / Verified / Unverified.
        $this->assertFalse($user->isLoggedIn());
        $this->assertTrue($user->isVerified());
        $this->assertFalse($user->isUnverified());
    }

    /**
     * Test the logging in method via a standard password mechanism
     * @return void
     */
    public function testUserLoginPassword(): void
    {
        User::doLogout();
        // Failed Login via ID and password.
        $this->assertFalse(User::doLogin([
            'user_id' => 'test_user',
            'password' => 'awrongpassword',
        ]));

        // Login via ID and password.
        $this->assertTrue(User::doLogin([
            'user_id' => 'test_user',
            'password' => 'testlogindetails',
        ]));
        $this->assertTrue(User::checkLoginState('password'));
        // - Additional: Levels.
        $this->assertTrue(User::checkLevel('user'));
        $this->assertFalse(User::checkLevel('admin'));
        $this->assertFalse(User::checkLevel('admin', true));
        $this->assertTrue(User::checkLevel(['user', 'admin'], false));
        $this->assertFalse(User::checkLevel(['user', 'admin'], true));
        // - Additional: States.
        $this->assertFalse(User::checkLoginState('cookie'));
        $this->assertFalse(User::checkLoginState('cookie', true));
        $this->assertTrue(User::checkLoginState(['cookie', 'password']));
        $this->assertTrue(User::checkLoginState(['cookie', 'password'], false));
        $this->assertFalse(User::checkLoginState(['cookie', 'password'], true));
        $this->assertTrue(User::checkLoginState('password:recent', true));
        // - Additional: Debug info.
        $debug = User::getDebug();
        $this->assertEquals('test_user', $debug['user_id']);
        $this->assertEquals('Test User', $debug['display_name']);
        $this->assertNotInstanceOf(Carbon::class, $debug['dob']);
        $this->assertIsString(Carbon::class, $debug['dob']);
        $this->assertEquals('1st January 1970', $debug['dob']);
        $this->assertEquals('Active', $debug['status']);
        $this->assertIsArray($debug['permissions']);
        $this->assertEquals(1, count($debug['permissions']));
        $this->assertEquals('www', $debug['permissions'][0]['site']);
        $this->assertEquals('user', $debug['permissions'][0]['level']);
        User::doLogout();
    }

    /**
     * Test the other logging in method
     * @return void
     */
    public function testUserLoginRemaining(): void
    {
        User::doLogout();

        // Login via ID and legacy password.
        $user_orig = User::find(40);
        $this->assertTrue(User::doLogin([
            'user_id' => 'test_legacy',
            'password' => 'testlogindetails',
        ]));
        $this->assertTrue(User::checkLoginState('password'));
        $user_upd = User::find(40);
        $this->assertNotEquals($user_orig->password, $user_upd->password);
        $this->assertTrue(User::checkLoginState('password'));
        User::doLogout();

        // Login via Object.
        $this->assertTrue(User::doLogin(User::find(10)));
        $this->assertTrue(User::checkLoginState('system'));
        User::doLogout();

        // Login via registration form.
        $this->assertTrue(User::doLogin(User::find(10), ['from_registration' => true]));
        $this->assertTrue(User::checkLoginState('registration'));
        User::doLogout();

        // Login via cookie.
        $this->assertTrue(User::doLogin(User::find(10), ['from_cookie' => true]));
        $this->assertTrue(User::checkLoginState('cookie'));
        User::doLogout();

        // Login with Remember Me.
        $rm_orig = Cookie::getValue(FrameworkConfig::get('debear.sessions.cookies.rememberme'));
        $this->assertTrue(User::doLogin(User::find(10), ['rememberme' => true]));
        $this->assertTrue(User::checkLoginState('system'));
        $rm_upd = Cookie::getValue(FrameworkConfig::get('debear.sessions.cookies.rememberme'));
        $this->assertIsString($rm_upd);
        $this->assertNotEmpty($rm_upd);
        $this->assertNotEquals($rm_orig, $rm_upd);
        User::doLogout();
    }

    /**
     * Test
     * @return void
     */
    public function testUserObjectSetup(): void
    {
        User::doLogout();

        // Failed setup via invalid remember me.
        Cookie::setValue(FrameworkConfig::get('debear.sessions.cookies.rememberme'), 'invalid::code');
        User::reload('cookie');
        $this->assertFalse(User::object()->isLoggedIn());
        $this->assertFalse(Cookie::hasValue(FrameworkConfig::get('debear.sessions.cookies.rememberme')));

        // Login via valid remember me.
        Cookie::setValue(
            FrameworkConfig::get('debear.sessions.cookies.rememberme'),
            '10::nWpLcFLTqZXctv4ItGgjyF9OptxU7FMd'
        );
        User::reload('cookie');
        $user = User::object();
        $this->assertTrue($user->isLoggedIn());
        $this->assertEquals('test_user', $user->user_id);
        $this->assertTrue(User::checkLoginState('cookie'));
        $this->assertNotEquals('password', session('user.login.source'));

        // Login via password.
        $this->assertTrue(User::doLogin([
            'user_id' => $user->user_id,
            'password' => 'testlogindetails',
        ]));
        $this->assertTrue(User::checkLoginState('password'));
        $this->assertTrue(Policies::match('state:password:recent'));
        $this->assertEquals('password', session('user.login.source'));

        // Tidy up.
        Cookie::unsetValue(FrameworkConfig::get('debear.sessions.cookies.rememberme'));
        User::doLogout();
    }

    /**
     * Test the various name getters
     * @return void
     */
    public function testUserNameGetters(): void
    {
        $user = User::find(10);
        // Guest.
        User::doLogout();
        $this->assertEquals('Guest', User::object()->name);
        User::doLogout();
        // User.
        User::doLogin($user);
        $this->assertEquals('Test User', User::object()->name);
        User::doLogout();
        // Google Visitor.
        $sess_orig = session('user.referer.google');
        session(['user.referer.google' => true]);
        Policies::recalc($user);
        $this->assertEquals('Google Visitor', User::object()->name);
        session(['user.referer.google' => $sess_orig]);
        User::setup(); // Reset.
    }

    /**
     * Test the handling of Laravel's default remember token functionality
     * @return void
     */
    public function testUserRememberToken(): void
    {
        $user = User::find(10);
        // Token is originally null.
        $this->assertNull($user->getRememberToken());
        // Set it to a value, and it is still null.
        $user->setRememberToken('a-non-null-token');
        $this->assertNull($user->getRememberToken());
        // Whose name is also null.
        $this->assertNull($user->getRememberTokenName());
    }

    /**
     * Test preparing the object before being "upserted" to the database
     * @return void
     */
    public function testUserUpsert(): void
    {
        User::doLogout();
        $base = [
            'forename' => 'CI Test',
            'surname' => 'User',
            'password' => 'citestpassword',
            'email' => 'citest@debear.test',
            'display_name' => 'CI Test User',
            'dob' => '1970-01-01',
            'timezone' => 'Europe/London',
            'status' => 'Unverified',
        ];
        // With both an ID and Password.
        $data_with_user = array_merge($base, ['user_id' => 'ci_upsert']);
        User::prepareUpsert($data_with_user);
        $this->assertEquals('1970-01-01', $data_with_user['dob']);
        $this->assertEquals('461168a27284e347978f22523617d8', $data_with_user['password']);
        // With just a password.
        $data_no_user = $base;
        User::prepareUpsert($data_no_user);
        $this->assertEquals('1970-01-01', $data_no_user['dob']);
        $this->assertEquals('b2cc74582105fa4694758879a24abf', $data_no_user['password']);
    }
}
