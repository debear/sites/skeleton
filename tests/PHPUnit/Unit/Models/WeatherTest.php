<?php

namespace Tests\PHPUnit\Unit\Models;

use Tests\PHPUnit\Base\UnitTestCase;
use DeBear\ORM\Skeleton\WeatherForecast;

class WeatherTest extends UnitTestCase
{
    /**
     * Test converting from the database values
     * @return void
     */
    public function testConversion(): void
    {
        $model = new WeatherForecast([
            'temp' => 0,
        ]);
        $this->assertEquals(32, $model->tempF);

        $model->temp = 100;
        $this->assertEquals(212, $model->tempF);

        $model->temp = -160 / 9;
        $this->assertEquals(0, $model->tempF);
    }
}
