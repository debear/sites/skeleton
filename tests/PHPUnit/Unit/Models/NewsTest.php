<?php

namespace Tests\PHPUnit\Unit\Models;

use Tests\PHPUnit\Base\UnitTestCase;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Models\Skeleton\News;
use DeBear\Models\Skeleton\NewsSource;

class NewsTest extends UnitTestCase
{
    /**
     * Test the relationships between articles and sources
     * @return void
     */
    public function testRelationships(): void
    {
        $source = NewsSource::with('articles')->where('app', 'ci-app')->first();
        $this->assertEquals('DeBear CI', $source->name);
        $this->assertEquals(1, $source->articles->count());
        $this->assertInstanceOf(News::class, $source->articles->first());

        $article = News::with('source')->where('app', 'ci-app')->first();
        $this->assertEquals('ab7de06ee9cb', $article->uniqueReference());
        $this->assertEquals('Test Article', $article->title);
        $this->assertEquals('Test Article', $article->headline());
        $this->assertInstanceOf(NewsSource::class, $article->source);
        $this->assertEquals('DeBear CI', $article->source->name);
        $this->assertEquals('DeBear CI', $article->publisher());
    }

    /**
     * Test the CSP updates
     * @return void
     */
    public function testUpdateCSP(): void
    {
        // Config before.
        $orig = [
            'debear.headers.csp.policies.style-src' => FrameworkConfig::get('debear.headers.csp.policies.style-src'),
            'debear.headers.csp.policies.img-src' => FrameworkConfig::get('debear.headers.csp.policies.img-src'),
        ];
        // Manipulate and test.
        News::updateCSP('ci-app');
        $this->assertContains("'nonce-{{NONCE}}'", FrameworkConfig::get('debear.headers.csp.policies.style-src'));
        $this->assertContains(
            'https://static.debear.test',
            FrameworkConfig::get('debear.headers.csp.policies.img-src')
        );
        // Restore config.
        FrameworkConfig::set($orig);
    }

    /**
     * Test the published manipulation
     * @return void
     */
    public function testThumbnail(): void
    {
        $article = News::build([
            'app' => 'ci-app',
            'source_id' => 1,
            'thumbnail' => null,
        ]);
        $this->assertEquals('//static.debear.test/www/images/news/ci-app/1.png', $article->image());
        $article->thumbnail = 'https://static.debear.test/images/test-article.png';
        $this->assertEquals('https://static.debear.test/images/test-article.png', $article->image());
    }

    /**
     * Test the published manipulation
     * @return void
     */
    public function testPublished(): void
    {
        // Set the time to something deterministic for our tests.
        $this->setTestDateTime('2020-01-12 23:42:42');

        // New (no date).
        $article = News::build([]);
        $this->assertEquals('', $article->timeRelative());
        // Just Now.
        $article->published = '2020-01-12 23:42:12';
        $this->assertEquals('Just now', $article->timeRelative());
        // In Minutes.
        $article->published = '2020-01-12 23:32:42';
        $this->assertEquals('10 minutes ago', $article->timeRelative());
        // In Hours.
        $article->published = '2020-01-12 21:42:42';
        $this->assertEquals('2 hours ago', $article->timeRelative());
        // Today.
        $article->published = '2020-01-12 01:42:42';
        $this->assertEquals('1:42am', $article->timeRelative());
        // Yesterday.
        $article->published = '2020-01-11 23:42:42';
        $this->assertEquals('Yesterday, 11:42pm', $article->timeRelative());
        // Day.
        $article->published = '2020-01-09 23:42:12';
        $this->assertEquals('Thursday, 11:42pm', $article->timeRelative());
        // Previous Year.
        $article->published = '2019-12-31 23:42:12';
        $this->assertEquals('31st December 2019, 11:42pm', $article->timeRelative());
    }

    /**
     * Test the description summary
     * @return void
     */
    public function testDescription(): void
    {
        $base = 'A test article';
        $summary = ' <em>(Article continues on publisher&amp;#39;s site)</em>';
        $article = News::build();

        // No ellipsis.
        $article->description = $base;
        $this->assertEquals('A test article', $article->summary());
        // Three dots.
        $article->description = "$base...";
        $this->assertEquals("A test article$summary", $article->summary());
        // Ellipsis.
        $article->description = "{$base}…";
        $this->assertEquals("A test article$summary", $article->summary());
        // Octal ellipsis.
        $article->description = "$base&amp;#8230;";
        $this->assertEquals("A test article$summary", $article->summary());
        // Hex ellipsis.
        $article->description = "$base&amp;#x2026;";
        $this->assertEquals("A test article$summary", $article->summary());
        // Named ellipsis.
        $article->description = "$base&amp;hellip;";
        $this->assertEquals("A test article$summary", $article->summary());
    }
}
