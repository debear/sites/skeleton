<?php

namespace Tests\PHPUnit\Unit\Models;

use Tests\PHPUnit\Base\UnitTestCase;
use DeBear\Models\Skeleton\Country;
use DeBear\Models\Skeleton\Timezone;

class CountryTest extends UnitTestCase
{
    /**
     * Test accessing info about a country
     * @return void
     */
    public function testCountry(): void
    {
        $test = Country::with(['timezoneDefault', 'timezoneList'])->where('code2', 'us')->first();
        $this->assertEquals('usa', $test->code3);
        $this->assertEquals('America/New_York', $test->default_timezone);
        $this->assertInstanceOf(Timezone::class, $test->timezoneDefault);
        $this->assertEquals('America/New_York', $test->timezoneDefault->timezone);
        $this->assertEquals(17, $test->timezoneList->count());
        $tz_ordered = $test->timezoneList->sortByDesc('population');
        $this->assertEquals('America/New_York', $tz_ordered->first()->timezone);
        $this->assertEquals('America/Adak', $tz_ordered->last()->timezone);
    }

    /**
     * Test accessing info about a single timezone
     * @return void
     */
    public function testTimezone(): void
    {
        $test = Timezone::with('country')->orderByDesc('population')->first();
        $this->assertEquals('Asia/Ho_Chi_Minh', $test->timezone);
        $this->assertEquals('vn', $test->code2);
        $this->assertInstanceOf(Country::class, $test->country);
        $this->assertEquals('vnm', $test->country->code3);
    }
}
