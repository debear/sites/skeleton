<?php

namespace Tests\PHPUnit\Unit\Helpers;

use Tests\PHPUnit\Base\UnitTestCase;
use DeBear\Helpers\Cookie;

class CookieTest extends UnitTestCase
{
    /**
     * Test the basic functionality
     * @return void
     */
    public function testBasic(): void
    {
        // Pre-check.
        $this->assertFalse(Cookie::hasValue('Test'));
        Cookie::setValue('Test', 'Value');
        $this->assertTrue(Cookie::hasValue('Test'));
        $value = Cookie::getValue('Test');
        $this->assertEquals('Value', $value);
        Cookie::unsetValue('Test');
        $this->assertFalse(Cookie::hasValue('Test'));
    }

    /**
     * Test value manipulation
     * @return void
     */
    public function testValueManips(): void
    {
        // Empty / Invaluds.
        $this->assertEmpty(Cookie::encryptValue(''));
        $this->assertNull(Cookie::decryptValue('Invalid String'));

        // Encrypt a string.
        $raw = 'Test String';
        $enc = Cookie::encryptValue($raw);
        $this->assertIsString($enc);
        $this->assertNotEmpty($enc);

        // Decrypt a string.
        $dec = Cookie::decryptValue($enc);
        $this->assertIsString($dec);
        $this->assertNotEmpty($dec);
        $this->assertEquals($raw, $dec);
    }

    /**
     * Test the framework request
     * @return void
     */
    public function testFramework(): void
    {
        $this->assertFalse(Cookie::has('missing'));
    }
}
