<?php

namespace Tests\PHPUnit\Unit\Helpers;

use Tests\PHPUnit\Base\UnitTestCase;
use DeBear\Helpers\Blade;

class BladeTest extends UnitTestCase
{
    /**
     * Test the returned output
     * @return void
     */
    public function testOutput(): void
    {
        // No data.
        $offline = Blade::render('skeleton.offline');
        $this->assertNotEmpty($offline);
        $this->assertStringContainsString('<h1>Device Offline</h1>', $offline);

        // With data.
        $cookies = Blade::render('skeleton.pages.cookies', ['name' => 'DeBear Test']);
        $this->assertNotEmpty($cookies);
        $this->assertStringContainsString('<h1>Cookie usage by DeBear Test</h1>', $cookies);
    }
}
