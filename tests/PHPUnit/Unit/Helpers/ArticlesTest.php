<?php

namespace Tests\PHPUnit\Unit\Helpers;

use Tests\PHPUnit\Base\UnitTestCase;
use DeBear\Helpers\Articles;

class ArticlesTest extends UnitTestCase
{
    /**
     * Test rendering
     * @return void
     */
    public function testRender(): void
    {
        // Short Text.
        $cnv = Articles::render('Short Text', 30);
        $this->assertNotEmpty($cnv);
        $this->assertEquals('Short Text', $cnv);

        // Short Text, on the limit.
        $cnv = Articles::render('Short Text', 2);
        $this->assertNotEmpty($cnv);
        $this->assertEquals('Short Text', $cnv);

        // Short Text, just after the limit.
        $cnv = Articles::render('Short Text String', 2);
        $this->assertNotEmpty($cnv);
        $this->assertEquals('Short Text'
            . '<span class="contracted">&hellip; (<a class="article-toggle">More</a>)</span>'
            . '<span class="expanded hidden"> String (<a class="article-toggle">Less</a>)</span>', $cnv);

        // Long Text.
        $cnv = Articles::render($this->loremIpsum(), 30);
        $this->assertNotEmpty($cnv);
        $this->assertStringContainsString('vulputate vitae porta<span class="contracted">&hellip; '
            . '(<a class="article-toggle">More</a>)</span>', $cnv);
        $this->assertStringContainsString('<span class="expanded hidden"> ac, ultricies sed neque.', $cnv);
        $this->assertStringEndsWith('(<a class="article-toggle">Less</a>)</span>', $cnv);
    }

    /**
     * Test image parsing
     * @return void
     */
    public function testImages(): void
    {
        // Test image URL generation.
        $base = '//static.debear.test/www/images/news';
        $map = [
            'A: _{IMG src="test.png"}_' => "A: <img src=\"$base/test.png\">",
            'B: _{IMG src="test.png" float="left"}_' => "B: <img src=\"$base/test.png\" class=\"article_img_left "
                . "float_left\">",
            'C: _{IMG src="test.png" caption="Test"}_' => "C: <img src=\"$base/test.png\" alt=\"Test\">",
            'D: _{IMG src="test.png" unknown="skipped"}_' => "D: <img src=\"$base/test.png\">",
        ];

        $cnv = Articles::parseImages(join(', ', array_keys($map)));
        $this->assertNotEmpty($cnv);
        foreach ($map as $exp) {
            $this->assertStringContainsString($exp, $cnv);
        }

        // Test an image only paragraph.
        $cnv = Articles::render('_{IMG src="test.png"}_', 30);
        $this->assertNotEmpty($cnv);
        $this->assertEquals("<img src=\"$base/test.png\">", $cnv);
    }

    /**
     * Test link parsing
     * @return void
     */
    public function testLinks(): void
    {
        // Test link generation.
        $map = [
            'A: _{LINK href="/"}_' => 'A: <a href="/">/</a>',
            'B: _{LINK href="/" text="Home"}_' => 'B: <a href="/">Home</a>',
            'C: _{LINK href="/" popup="true"}_' => 'C: <a href="/" target="_blank">/</a>',
            'D: _{LINK href="/" popup="false"}_' => 'D: <a href="/">/</a>',
            'E: _{LINK href="/" noref="true"}_' => 'E: <a href="/" rel="noopener noreferrer">/</a>',
            'F: _{LINK href="/" noref="false"}_' => 'F: <a href="/">/</a>',
            'G: _{LINK href="/" unknown="skipped"}_' => 'G: <a href="/">/</a>',
        ];

        $cnv = Articles::parseLinks(join(', ', array_keys($map)));
        $this->assertNotEmpty($cnv);
        foreach ($map as $exp) {
            $this->assertStringContainsString($exp, $cnv);
        }
    }

    /**
     * Some test Lorem Ipsum to use in our test
     * @return string The lorem ipsum
     */
    protected function loremIpsum(): string
    {
        return '<p><strong>Lorem ipsum dolor sit amet</strong>, consectetur adipiscing elit. Aenean id sollicitudin '
            . 'risus. In nec tempus arcu. Integer suscipit nibh sed enim placerat, a aliquam ex dictum. Sed lorem '
            . 'magna, vulputate vitae porta ac, ultricies sed neque. Nam hendrerit convallis leo, a posuere elit '
            . 'scelerisque at. Cras fringilla lorem eget sollicitudin luctus. Quisque porttitor consequat faucibus. '
            . 'Sed pulvinar, libero eget rhoncus auctor, erat ante aliquet elit, sit amet sollicitudin nisl odio sed '
            . 'erat. Donec semper, enim ut aliquet ultricies, erat est rutrum leo, accumsan tincidunt sapien neque '
            . 'vitae tellus.</p>';
    }
}
