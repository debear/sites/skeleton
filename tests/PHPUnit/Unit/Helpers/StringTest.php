<?php

namespace Tests\PHPUnit\Unit\Helpers;

use Tests\PHPUnit\Base\UnitTestCase;
use Illuminate\Support\Facades\App;
use DeBear\Helpers\Strings;
use DeBear\Repositories\Time;
use DeBear\Exceptions\StringsException;

class StringTest extends UnitTestCase
{
    /**
     * Prepare the object by setting up the Time instance
     * @return void
     */
    protected function setUp(): void
    {
        // Load the standard test setup methods.
        parent::setUp();
        // Define our Time class as a singleton.
        App::singleton(Time::class);
    }

    /**
     * Test the various methods of encoding strings
     * @return void
     */
    public function testEncoding(): void
    {
        // Strings::encodeString - HTML mode.
        $this->assertEquals('&quot;', Strings::encodeString('"'));
        $this->assertEquals('&amp;', Strings::encodeString('&'));
        $this->assertEquals('&ccedil;', Strings::encodeString('ç'));
        $this->assertEquals('&pi;', Strings::encodeString('π'));
        $this->assertEquals('&euro;', Strings::encodeString('€'));
        $this->assertEquals('&quot;&amp;&ccedil;&pi;&euro;', Strings::encodeString('"&çπ€'));
        $this->assertEquals('Aim&eacute;', Strings::encodeString('Aimé'));
        // With a complex Unicode character.
        $unichar = '\u27a4'; // Black Rightwards Arrowhead.
        $this->assertEquals('Unicode: &#10148;', Strings::encodeString('Unicode: ' . json_decode("\"$unichar\"")));

        // Strings::encodeString - Add slashes.
        $this->assertEquals('\"', Strings::encodeString('"', false));

        // Strings::encodeInput - HTML mode.
        $this->assertEquals('&quot;', Strings::encodeInput('"'));
        $this->assertEquals(['&quot;'], Strings::encodeInput(['"']));
        $this->assertEquals(['"' => 1], Strings::encodeInput(['"' => 1]));
        $this->assertEquals(['"' => '&quot;'], Strings::encodeInput(['"' => '"']));
        $this->assertEquals('Aim&eacute;', Strings::encodeInput('Aimé'));
        $this->assertEquals(['Aim&eacute;'], Strings::encodeInput(['Aimé']));

        // Strings::encodeInput - Add slashes.
        $this->assertEquals('\"', Strings::encodeInput('"', false));
        $this->assertEquals(['\"'], Strings::encodeInput(['"'], false));
        $this->assertEquals(['"' => 1], Strings::encodeInput(['"' => 1]));
        $this->assertEquals(['"' => '\"'], Strings::encodeInput(['"' => '"'], false));

        // Strings::decodeString.
        $this->assertEquals('"', Strings::decodeString('&quot;'));
        $this->assertEquals('&', Strings::decodeString('&amp;'));
        $this->assertEquals('ç', Strings::decodeString('&ccedil;'));
        $this->assertEquals('π', Strings::decodeString('&pi;'));
        $this->assertEquals('€', Strings::decodeString('&euro;'));
        $this->assertEquals('"&çπ€', Strings::decodeString('&quot;&amp;&ccedil;&pi;&euro;'));
    }

    /**
     * Test the newline conversion
     * @return void
     */
    public function testConvertNewline(): void
    {
        $this->assertEquals('asddsa', Strings::convertNewlines("asddsa"));
        $this->assertEquals('<p>asd</p><p>dsa</p>', Strings::convertNewlines("asd\ndsa"));
        $this->assertEquals('<p>asd</p><p>dsa</p>', Strings::convertNewlines("asd\rdsa"));
        $this->assertEquals('<p>asd</p><p>dsa</p>', Strings::convertNewlines("asd\r\ndsa"));
        $this->assertEquals('<p>asd</p><p>dsa</p>', Strings::convertNewlines("asd\n\rdsa"));
        $this->assertEquals('<p>asd</p><p>dsa</p>', Strings::convertNewlines("asd\n\ndsa"));
    }

    /**
     * Test the various methods of codifying strings
     * @return void
     */
    public function testCodify(): void
    {
        // Codifying a string.
        $this->assertEquals('lower', Strings::codify('lower'));
        $this->assertEquals('upper', Strings::codify('UPPER'));
        $this->assertEquals('lower-upper', Strings::codify('lower-UPPER'));
        $this->assertEquals('title-title', Strings::codify('Title Title'));
        $this->assertEquals('123-numbered', Strings::codify('123 Numbered'));
        $this->assertEquals('quote-s', Strings::codify('Quote\'s'));
        $this->assertEquals('one-two-three', Strings::codify('One "Two" Three'));
        $this->assertEquals('one-two-three', Strings::codify('One -"Two"- Three'));
        $this->assertEquals('', Strings::codify('!?!?'));

        // The URL equivalents.
        $this->assertEquals('title-title', Strings::codifyURL('Title Title'));
        $this->assertEquals('123-numbered', Strings::codifyURL('123 Numbered'));
        $this->assertEquals('quote-s', Strings::codifyURL('Quote\'s'));
        $this->assertEquals('aun', Strings::codifyURL('&Aring;&uuml;&ntilde;&para;'));
        $this->assertEquals('aun', Strings::codifyURL('&para;&Aring;&uuml;&ntilde;'));
        $this->assertEquals('a-u-n', Strings::codifyURL('&Aring;&para;&uuml;&para;&ntilde;'));

        // Generating an MD5 code.
        $this->assertEquals('098f6bcd4621', Strings::md5Code('test'));
        $this->assertEquals('098f6bcd', Strings::md5Code('test', 8));
        $this->assertEquals('098f6bcd4621', Strings::md5Code('test', 12));
        $this->assertEquals('098f6bcd4621d373', Strings::md5Code('test', 16));
    }

    /**
     * Test naturally joining an array of strings
     * @return void
     */
    public function testNaturalJoin(): void
    {
        $this->assertEquals('', Strings::naturalJoin([]));
        $this->assertEquals('a', Strings::naturalJoin(['a']));
        $this->assertEquals('a &amp; b', Strings::naturalJoin(['a', 'b']));
        $this->assertEquals('a &amp; 1', Strings::naturalJoin(['a', 1]));
        $this->assertEquals('a, b &amp; c', Strings::naturalJoin(['a', 'b', 'c']));
        $this->assertEquals('a, b, c &amp; d', Strings::naturalJoin(['a', 'b', 'c', 'd']));
        $this->assertEquals('a,b,c &amp; d', Strings::naturalJoin(['a', 'b', 'c', 'd'], ','));
        $this->assertEquals('a,b,c,d', Strings::naturalJoin(['a', 'b', 'c', 'd'], ',', ','));
    }

    /**
     * Test the various methods of enc- and decrypting strings
     * @return void
     */
    public function testEncryption(): void
    {
        // One-way Encrypt.
        $this->assertEquals('fed2ee339e12c0acdb3310ede19962', Strings::oneWayHash('string1', 'salt1'));
        $this->assertEquals('e325ad8753a78810440bea73983494', Strings::oneWayHash('string1', 'salt2'));
        $this->assertEquals('cb312de4815cf3559aa09dc169d4ef', Strings::oneWayHash('string2', 'salt1'));
        $this->assertEquals('1fc2056d6f326e3da1e1c1da244b34', Strings::oneWayHash('string2', 'salt2'));
        $this->assertEquals('b488ad2f5a39', Strings::oneWayHash('legacy', 'salt1', ['len' => 12]));
        $this->assertEquals('86e30960eabd5abc73c70f93', Strings::oneWayHash('ycagel', 'salt1', ['len' => 24]));

        // Two-way Encrypt.
        $this->assertEquals('c3RyaW5n', Strings::twoWayEncrypt('string'));
        $this->assertEquals('c2FsdDFzdHJpbmc', Strings::twoWayEncrypt('string', 'salt1'));
        $this->assertEquals('c2FsdDJzdHJpbmc', Strings::twoWayEncrypt('string', 'salt2'));

        // Two-way Decrypt.
        $this->assertEquals('string', Strings::twoWayDecrypt('c3RyaW5n'));
        $this->assertEquals('string', Strings::twoWayDecrypt('c3RyaW5n===='));
        $this->assertEquals('string', Strings::twoWayDecrypt('c2FsdDFzdHJpbmc', 'salt1'));
        $this->assertEquals('string', Strings::twoWayDecrypt('c2FsdDFzdHJpbmc=', 'salt1'));
        $this->assertEquals('string', Strings::twoWayDecrypt('c2FsdDJzdHJpbmc', 'salt2'));
        $this->assertEquals('string', Strings::twoWayDecrypt('c2FsdDJzdHJpbmc=', 'salt2'));

        $this->expectException(StringsException::class);
        Strings::twoWayDecrypt('random', 'salt');
    }

    /**
     * Test the various methods around user timelocks
     * @return void
     */
    public function testUserLock(): void
    {
        // We'll need to define some time-based values and assumes a 12 hour validity.
        $user_id = 'test_user';
        $valid_for = 12; // Hours...
        $time_base = '2019-01-01 00:00:00';
        $time_valid = '2019-01-01 11:59:59';
        $time_boundary = '2019-01-01 12:00:00';
        $time_invalid = '2019-01-01 12:00:01';

        // Generate the code, using the base time (which we can't test because it's random).
        $this->setDate($time_base);
        $code_random = Strings::generateUserTimeLock($user_id);
        $code_fixed = '4QM5ATMwEDMwADMwEDN0V2c09VdzVmc';

        // Check when within the time bounds.
        $this->setDate($time_valid);
        $this->assertEquals($user_id, Strings::validateUserTimeLock($code_random, $valid_for));
        $this->assertEquals($user_id, Strings::validateUserTimeLock($code_fixed, $valid_for));

        // Check when on the time boundary.
        $this->setDate($time_boundary);
        $this->assertEquals($user_id, Strings::validateUserTimeLock($code_random, $valid_for));
        $this->assertEquals($user_id, Strings::validateUserTimeLock($code_fixed, $valid_for));

        // Check when after the time deadline.
        $this->setDate($time_invalid);
        $this->assertFalse(Strings::validateUserTimeLock($code_random, $valid_for));
        $this->assertFalse(Strings::validateUserTimeLock($code_fixed, $valid_for));

        // Check invalid details.
        $short_code = Strings::twoWayEncrypt('short', 'salt');
        $this->assertFalse(Strings::validateUserTimeLock($short_code, 2));
        $this->expectException(StringsException::class);
        Strings::generateUserTimeLock();
    }

    /**
     * Test the random string generator
     * @return void
     */
    public function testRand(): void
    {
        $rand = Strings::randomAlphaNum(12);
        $this->assertIsString($rand);
        $this->assertEquals(12, strlen($rand));
    }

    /**
     * Tweak the current time the app thinks it is
     * @param string $time The time to be set.
     * @return void
     */
    protected function setDate(string $time): void
    {
        App::make(Time::class)->resetCache($time);
    }
}
