<?php

namespace Tests\PHPUnit\Unit\Helpers;

use Tests\PHPUnit\Base\UnitTestCase;
use DeBear\Helpers\PHPUnit\RefreshDatabase;

class MiscTest extends UnitTestCase
{
    /**
     * Test the extremities of the RefreshDatabase class
     * Unlike other tests, due to the internal mechanics of the class,
     * we run the bulk of the tests within the class and expect a boolean
     * back to indicate the missing prefix was handled correctly or not.
     * @return void
     */
    public function testRefreshDatabase(): void
    {
        $this->assertTrue(RefreshDatabase::testNoDBPrefix());
    }
}
