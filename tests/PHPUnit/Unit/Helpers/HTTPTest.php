<?php

namespace Tests\PHPUnit\Unit\Helpers;

use Tests\PHPUnit\Base\UnitTestCase;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Support\Facades\Request;
use Illuminate\Http\Response;
use DeBear\Helpers\HTTP;

class HTTPTest extends UnitTestCase
{
    /**
     * Test the env info
     * @return void
     */
    public function testEnv(): void
    {
        $this->assertFalse(HTTP::isDev());
        $this->assertFalse(HTTP::isLive());
        $this->assertTrue(HTTP::isTest());
    }

    /**
     * Test the component builders
     * @return void
     */
    public function testBuilders(): void
    {
        // Domain.
        $this->assertEquals('//debear.test', HTTP::buildDomain());
        $this->assertEquals('//my.debear.test', HTTP::buildDomain('my'));

        // Section.
        $orig = [
            'debear.section.endpoint' => FrameworkConfig::get('debear.section.endpoint'),
            'debear.section.code' => FrameworkConfig::get('debear.section.code'),
        ];
        FrameworkConfig::set(['debear.section.endpoint' => 'endpoint', 'debear.section.code' => 'test']);
        $this->assertEquals('//debear.test/endpoint/', HTTP::buildSectionURL());
        FrameworkConfig::set($orig);

        // Static URLs (as scalar).
        $this->assertEquals('//static.debear.test/skel/css/test.css', HTTP::buildStaticURLs('css', 'skel/test.css'));
        $this->assertEquals('//static.debear.test/www/css/test.css', HTTP::buildStaticURLs('css', 'test.css'));
        // Static URLs (as array).
        $this->assertEquals([
            '//static.debear.test/skel/css/test.css',
            '//static.debear.test/www/css/test.css',
        ], HTTP::buildStaticURLs('css', [
            'skel/test.css',
            'test.css',
        ]));

        // CDN building.
        $str = HTTP::buildCDNURLs('test.png');
        $this->assertEquals('//cdn.debear.test/nZA82Ag0B_ySIvS0u0SSroNe', $str);

        $str_args = HTTP::buildCDNURLs('test.png', ['w' => 10, 'h' => 10]);
        $this->assertEquals('//cdn.debear.test/0XCkEKAihUeFoXJFnaJ2mpEAsMU0Uztrc1SLrNe', $str_args);

        $arr = HTTP::buildCDNURLs(['test.png', 'tset.png']);
        $this->assertEquals([
           '//cdn.debear.test/nZA82Ag0B_ySIvS0u0SSroNe',
           '//cdn.debear.test/nZAD3Ag0B_ySIvS0t4UKroNe',
        ], $arr);
    }

    /**
     * Test the resource links to a page
     * @return void
     */
    public function testResources(): void
    {
        // Resource Hints.
        $resources = HTTP::resourceHints();
        $this->assertIsArray($resources);
        $this->assertEquals('//static.debear.test', $resources[0]);
        $this->assertEquals('//cdn.debear.test', $resources[1]);

        // Status setters (scalar content).
        $status = HTTP::setStatus(204);
        $this->assertInstanceOf(Response::class, $status);
        $this->assertEmpty($status->content());
        $this->assertEquals(204, $status->status());

        // Status setters (array content).
        $status = HTTP::setStatus(201, ['test' => true]);
        $this->assertInstanceOf(Response::class, $status);
        $this->assertEquals('{"test":true}', $status->content());
        $this->assertEquals(201, $status->status());

        // Prepare $_SERVER.
        $orig = $raw = Request::server();
        $raw['REQUEST_METHOD'] = 'POST';
        Request::getFacadeRoot()->server->replace($raw);

        // Page redirect.
        $redirect = HTTP::redirectPage('/test', 308);
        $this->assertEquals(308, $redirect->status());
        $this->assertEquals('https://debear.test/test', $redirect->getTargetUrl());

        $redirect = HTTP::redirectPage('/test', 301);
        $this->assertEquals(308, $redirect->status());
        $this->assertEquals('https://debear.test/test', $redirect->getTargetUrl());

        // Reset.
        Request::getFacadeRoot()->server->replace($orig);
    }

    /**
     * Test the HTTP status code wrappers
     * @return void
     */
    public function testStatusWrappers(): void
    {
        // 201: Created.
        $resp = HTTP::sendCreated(['success' => true]);
        $this->assertInstanceOf(Response::class, $resp);
        $this->assertEquals(201, $resp->status());
        $this->assertEquals('{"success":true}', $resp->content());

        // 204: No Content.
        $resp = HTTP::sendNoContent();
        $this->assertInstanceOf(Response::class, $resp);
        $this->assertEmpty($resp->content());
        $this->assertEquals(204, $resp->status());

        // 400: Bad Request.
        $resp = HTTP::sendBadRequest();
        $this->assertInstanceOf(Response::class, $resp);
        $this->assertEquals(400, $resp->status());
        $this->assertStringContainsString('<strong>400: Bad Request</strong>', $resp->content());

        // 401: Unauthorised.
        $resp = HTTP::sendUnauthorised();
        $this->assertInstanceOf(Response::class, $resp);
        $this->assertEquals(401, $resp->status());
        $this->assertStringContainsString('<h1>User Not Signed In</h1>', $resp->content());

        // 403: Forbidden.
        $resp = HTTP::sendForbidden();
        $this->assertInstanceOf(Response::class, $resp);
        $this->assertEquals(403, $resp->status());
        $this->assertStringContainsString('<h1>Insufficient Privileges</h1>', $resp->content());

        // 404: Not Found.
        $resp = HTTP::sendNotFound();
        $this->assertInstanceOf(Response::class, $resp);
        $this->assertEquals(404, $resp->status());
        $this->assertStringContainsString('<strong>404: Not Found</strong>', $resp->content());

        // 405: Method Not Allowed.
        $resp = HTTP::sendMethodNotAllowed();
        $this->assertInstanceOf(Response::class, $resp);
        $this->assertEquals(405, $resp->status());
        $this->assertStringContainsString('<strong>405: Method Not Allowed</strong>', $resp->content());
    }

    /**
     * Test the addition of security headers to a request
     * @return void
     */
    public function testSecurityHeaders(): void
    {
        $this->setTestConfig([
            'debear.headers.csp.report_only' => true,
            'debear.headers.csp.report_uri' => '/report-uri/csp',
            'debear.headers.expect_ct.enabled' => true,
        ]);

        // Standard, enabling CSP Report Only.
        $response = response('', 204);
        HTTP::securityHeaders($response);
        $headers = $response->headers->all();
        $this->assertIsArray($headers);
        $this->assertNotEmpty($headers);
        $this->assertArrayHasKey('content-security-policy-report-only', $headers);
        $this->assertArrayNotHasKey('x-custom', $headers);

        // With custom headers (overlayed).
        $response = response('', 204);
        HTTP::securityHeaders($response, ['X-Custom' => 'test']);
        $headers = $response->headers->all();
        $this->assertIsArray($headers);
        $this->assertNotEmpty($headers);
        $this->assertArrayHasKey('content-security-policy-report-only', $headers);
        $this->assertArrayHasKey('x-custom', $headers);

        // With custom headers (replaced).
        $response = response('', 204);
        HTTP::securityHeaders($response, ['X-Custom' => 'test'], ['replace_default' => true]);
        $headers = $response->headers->all();
        $this->assertIsArray($headers);
        $this->assertNotEmpty($headers);
        $this->assertArrayNotHasKey('content-security-policy-report-only', $headers);
        $this->assertArrayHasKey('x-custom', $headers);
    }

    /**
     * Test CSP components
     * @return void
     */
    public function testCSP(): void
    {
        // Get when enabled, raw.
        $this->assertNotEmpty(HTTP::getCSPNonce());
        // Get when enabled, as a tag.
        $as_tag = HTTP::buildCSPNonceTag();
        $this->assertNotEmpty($as_tag);
        $this->assertStringStartsWith('nonce=', $as_tag);

        // Disable, re-test.
        HTTP::disableCSP();
        $this->assertEmpty(HTTP::getCSPNonce());
        // Get when enabled, as a tag.
        $as_tag = HTTP::buildCSPNonceTag();
        $this->assertEmpty($as_tag);
    }

    /**
     * Test referer info
     * @return void
     */
    public function testReferers(): void
    {
        // First call to store referer without one.
        HTTP::storeReferer();

        // Prepare $_SERVER with faux Google referer.
        $orig = $raw = Request::server();
        $raw['HTTP_REFERER'] = 'https://www.google.com/';
        Request::getFacadeRoot()->server->replace($raw);

        // First call, to process.
        $this->assertTrue(HTTP::refererIsGoogle());
        // Then second call to use the cached version.
        $this->assertTrue(HTTP::refererIsGoogle());

        // Then try another store (with a non-full referer).
        $raw['HTTP_REFERER'] = 'www.google.com';
        Request::getFacadeRoot()->server->replace($raw);
        HTTP::storeReferer();

        // Reset.
        Request::getFacadeRoot()->server->replace($orig);
    }
}
