<?php

namespace Tests\PHPUnit\Unit\Helpers;

use Tests\PHPUnit\Base\UnitTestCase;
use DeBear\Helpers\Format;
use DeBear\Exceptions\FormatException;

class FormatTest extends UnitTestCase
{
    /**
     * Test ordinalisation
     * @return void
     */
    public function testOrdinal(): void
    {
        $this->assertEquals('-1st', Format::ordinal(-1));
        $this->assertEquals('0th', Format::ordinal(-0));
        $this->assertEquals('0th', Format::ordinal(0));
        $this->assertEquals('1st', Format::ordinal(1));
        $this->assertEquals('2nd', Format::ordinal(2));
        $this->assertEquals('3rd', Format::ordinal(3));
        $this->assertEquals('4th', Format::ordinal(4));
        $this->assertEquals('5th', Format::ordinal(5));
        $this->assertEquals('9th', Format::ordinal(9));
        $this->assertEquals('10th', Format::ordinal(10));
        $this->assertEquals('11th', Format::ordinal(11));
        $this->assertEquals('12th', Format::ordinal(12));
        $this->assertEquals('13th', Format::ordinal(13));
        $this->assertEquals('14th', Format::ordinal(14));
        $this->assertEquals('15th', Format::ordinal(15));
        $this->assertEquals('19th', Format::ordinal(19));
        $this->assertEquals('20th', Format::ordinal(20));
        $this->assertEquals('21st', Format::ordinal(21));
        $this->assertEquals('22nd', Format::ordinal(22));
        $this->assertEquals('23rd', Format::ordinal(23));
        $this->assertEquals('24th', Format::ordinal(24));
        $this->assertEquals('25th', Format::ordinal(25));
        $this->assertEquals('422nd', Format::ordinal(422));
    }

    /**
     * Test pluralisation
     * @return void
     */
    public function testPluralise(): void
    {
        $this->assertEquals('1', Format::pluralise(1));
        $this->assertEquals('2s', Format::pluralise(2));
        $this->assertEquals('-2 tests', Format::pluralise(-2, ' test'));
        $this->assertEquals('-1 test', Format::pluralise(-1, ' test'));
        $this->assertEquals('0 tests', Format::pluralise(0, ' test'));
        $this->assertEquals('1 test', Format::pluralise(1, ' test'));
        $this->assertEquals('2 tests', Format::pluralise(2, ' test'));
        $this->assertEquals('1 red balloon', Format::pluralise(1, ' red balloon'));
        $this->assertEquals('99 red balloons', Format::pluralise(99, ' red balloon'));
        $this->assertEquals('0 trophies', Format::pluralise(0, ' trophy'));
        $this->assertEquals('1 trophy', Format::pluralise(1, ' trophy'));
        $this->assertEquals('2 trophies', Format::pluralise(2, ' trophy'));
        $this->assertEquals('0.5 pts', Format::pluralise(0.5, ' pt'));
        $this->assertEquals('1.5 pts', Format::pluralise(1.5, ' pt'));

        $this->assertEquals('0 test@', Format::pluralise(0, ' test', ['plural' => '@']));
        $this->assertEquals('1 test', Format::pluralise(1, ' test', ['plural' => '@']));
        $this->assertEquals('2 test@', Format::pluralise(2, ' test', ['plural' => '@']));

        // Then using the custom options:
        // - Prepending the string, rather than appending.
        $this->assertEquals('Test1', Format::pluralise(1, 'Test', ['prepend-str' => true]));
        $this->assertEquals('Tests2', Format::pluralise(2, 'Test', ['prepend-str' => true]));
        // - Then with a space, to be awkward.
        $this->assertEquals('Test 1', Format::pluralise(1, 'Test ', ['prepend-str' => true]));
        $this->assertEquals('Tests 2', Format::pluralise(2, 'Test ', ['prepend-str' => true]));
        // - Hiding the number completely.
        $this->assertEquals('Test', Format::pluralise(1, 'Test', ['hide-num' => true]));
        $this->assertEquals('Tests', Format::pluralise(2, 'Test', ['hide-num' => true]));
        // - And both options enabled (which is always the equivalent of just hiding).
        $this->assertEquals('Test', Format::pluralise(1, 'Test', ['prepend-str' => true, 'hide-num' => true]));
        $this->assertEquals('Tests', Format::pluralise(2, 'Test', ['prepend-str' => true, 'hide-num' => true]));
        // - Formatting the number being passed.
        $this->assertEquals('1 test', Format::pluralise(1, ' test', ['format-number' => true]));
        $this->assertEquals('999 tests', Format::pluralise(999, ' test', ['format-number' => true]));
        $this->assertEquals('1,000 tests', Format::pluralise(1000, ' test', ['format-number' => true]));
        $this->assertEquals('1,000.5 tests', Format::pluralise(1000.5, ' test', ['format-number' => true]));
        $this->assertEquals('1,000.5 tests', Format::pluralise(1000.5, ' test', ['format-number' => 1]));
        $this->assertEquals('1,000.50 tests', Format::pluralise(1000.5, ' test', ['format-number' => 2]));
    }

    /**
     * Test singularisation
     * @return void
     */
    public function testSingularise(): void
    {
        $this->assertEquals('test', Format::singularise('test'));
        $this->assertEquals('test', Format::singularise('tests'));
        $this->assertEquals('tests', Format::singularise('testss'));

        $this->assertEquals('countrie', Format::singularise('countries'));
        $this->assertEquals('country', Format::singularise('countries', 'ies', 'y'));
        $this->assertEquals('countriesy', Format::singularise('countriesies', 'ies', 'y'));
    }

    /**
     * Test converting decimal numbers to Roman Numerals
     * @return void
     */
    public function testRomanNumerals(): void
    {
        $this->assertEquals('I', Format::decimalToRoman(1));
        $this->assertEquals('II', Format::decimalToRoman(2));
        $this->assertEquals('III', Format::decimalToRoman(3));
        $this->assertEquals('IV', Format::decimalToRoman(4));
        $this->assertEquals('V', Format::decimalToRoman(5));
        $this->assertEquals('VI', Format::decimalToRoman(6));
        $this->assertEquals('VII', Format::decimalToRoman(7));
        $this->assertEquals('VIII', Format::decimalToRoman(8));
        $this->assertEquals('IX', Format::decimalToRoman(9));
        $this->assertEquals('X', Format::decimalToRoman(10));
        $this->assertEquals('XI', Format::decimalToRoman(11));
        $this->assertEquals('XII', Format::decimalToRoman(12));
        $this->assertEquals('XIII', Format::decimalToRoman(13));
        $this->assertEquals('XIV', Format::decimalToRoman(14));
        $this->assertEquals('XV', Format::decimalToRoman(15));
        $this->assertEquals('XX', Format::decimalToRoman(20));
        $this->assertEquals('XXX', Format::decimalToRoman(30));
        $this->assertEquals('XL', Format::decimalToRoman(40));
        $this->assertEquals('L', Format::decimalToRoman(50));
        $this->assertEquals('C', Format::decimalToRoman(100));
        $this->assertEquals('D', Format::decimalToRoman(500));
        $this->assertEquals('M', Format::decimalToRoman(1000));
        $this->assertEquals('MCMLXX', Format::decimalToRoman(1970));
        $this->assertEquals('MM', Format::decimalToRoman(2000));
        $this->assertEquals('MMX', Format::decimalToRoman(2010));
    }

    /**
     * Test converting byte totals
     * @return void
     */
    public function testBytes(): void
    {
        // Bytes.
        $this->assertEquals('0 B', Format::bytes(0));
        $this->assertEquals('1 B', Format::bytes(1));
        $this->assertEquals('100 B', Format::bytes(100));
        $this->assertEquals('1023 B', Format::bytes(1023));
        // Kilibytes.
        $this->assertEquals('1.000 KiB', Format::bytes(1024));
        $this->assertEquals('1.001 KiB', Format::bytes(1025));
        $this->assertEquals('976.562 KiB', Format::bytes(1000000));
        $this->assertEquals('1023.999 KiB', Format::bytes(1048575));
        // Mebibytes.
        $this->assertEquals('1.000 MiB', Format::bytes(1048576));
        $this->assertEquals('953.674 MiB', Format::bytes(1000000000));
        $this->assertEquals('1023.999 MiB', Format::bytes(1073740775));
        $this->assertEquals('1024.000 MiB', Format::bytes(1073741823));
        // Gitibytes.
        $this->assertEquals('1.000 GiB', Format::bytes(1073741824));
        $this->assertEquals('931.323 GiB', Format::bytes(1000000000000));
        $this->assertEquals('1023.999 GiB', Format::bytes(1099510553600));
        $this->assertEquals('1024.000 GiB', Format::bytes(1099511627775));
        // Tebibytes.
        $this->assertEquals('1.000 TiB', Format::bytes(1099511627776));
        $this->assertEquals('909.495 TiB', Format::bytes(1000000000000000));
        $this->assertEquals('1023.999 TiB', Format::bytes(1125898806886400));
        $this->assertEquals('1024.000 TiB', Format::bytes(1125899906842623));
        // Pebibytes (formatted as Tebibytes).
        $this->assertEquals('1024.000 TiB', Format::bytes(1125899906842624));
        $this->assertEquals('909494.702 TiB', Format::bytes(1000000000000000000));
        // Negative (generates Exception!).
        $this->expectException(FormatException::class);
        Format::bytes(-1);
    }

    /**
     * Test converting age values
     * @return void
     */
    public function testAge(): void
    {
        // Seconds.
        $this->assertEquals('0 secs', Format::age(0));
        $this->assertEquals('1 sec', Format::age(1));
        $this->assertEquals('2 secs', Format::age(2));
        $this->assertEquals('59 secs', Format::age(59));
        // Minutes.
        $this->assertEquals('1 min', Format::age(60));
        $this->assertEquals('1 min', Format::age(61));
        $this->assertEquals('1 min', Format::age(119));
        $this->assertEquals('2 mins', Format::age(120));
        $this->assertEquals('59 mins', Format::age(3599));
        // Hours.
        $this->assertEquals('1 hour', Format::age(3600));
        $this->assertEquals('1 hour', Format::age(3601));
        $this->assertEquals('1 hour', Format::age(7199));
        $this->assertEquals('2 hours', Format::age(7200));
        $this->assertEquals('23 hours', Format::age(86399));
        // Days.
        $this->assertEquals('1 day', Format::age(86400));
        $this->assertEquals('1 day', Format::age(86401));
        $this->assertEquals('1 day', Format::age(172799));
        $this->assertEquals('2 days', Format::age(172800));
        $this->assertEquals('364 days', Format::age(31535999));
        // Years.
        $this->assertEquals('1 year', Format::age(31536000));
        $this->assertEquals('1 year', Format::age(31536001));
        $this->assertEquals('1 year', Format::age(63071999));
        $this->assertEquals('2 years', Format::age(63072000));
        // Negative (generates Exception!).
        $this->expectException(FormatException::class);
        Format::age(-1);
    }

    /**
     * Test grouping array of numbers into range(s)
     * @return void
     */
    public function testGroupRanges(): void
    {
        // First test the groupings.
        $this->assertEquals('', Format::groupRanges([]));
        $this->assertEquals('0', Format::groupRanges([0]));
        $this->assertEquals('1', Format::groupRanges([1]));
        $this->assertEquals('2', Format::groupRanges([2]));
        $this->assertEquals('1 &amp; 2', Format::groupRanges([1, 2]));
        $this->assertEquals('1&ndash;3', Format::groupRanges([1, 2, 3]));
        $this->assertEquals('1&ndash;4', Format::groupRanges([1, 2, 3, 4]));
        $this->assertEquals('1, 3 &amp; 4', Format::groupRanges([1, 3, 4]));
        $this->assertEquals('1 &amp; 3&ndash;5', Format::groupRanges([1, 3, 4, 5]));
        $this->assertEquals('1&ndash;4 &amp; 6', Format::groupRanges([1, 2, 3, 4, 6]));
        $this->assertEquals('1&ndash;3, 5 &amp; 6', Format::groupRanges([1, 2, 3, 5, 6]));
        $this->assertEquals('1, 2 &amp; 4&ndash;6', Format::groupRanges([1, 2, 4, 5, 6]));
        $this->assertEquals('1 &amp; 3&ndash;6', Format::groupRanges([1, 3, 4, 5, 6]));
        $this->assertEquals('2&ndash;6', Format::groupRanges([2, 3, 4, 5, 6]));

        // Then the string joining.
        $str_join = [1, 3, 4, 5, 6, 7, 9];
        $this->assertEquals('1, 3&ndash;7 &amp; 9', Format::groupRanges($str_join));
        // Join.
        $this->assertEquals('1, 37 &amp; 9', Format::groupRanges($str_join, ''));
        $this->assertEquals('1, 3-7 &amp; 9', Format::groupRanges($str_join, '-'));
        $this->assertEquals('1, 3..7 &amp; 9', Format::groupRanges($str_join, '..'));
        // Join and separator.
        $this->assertEquals('137 &amp; 9', Format::groupRanges($str_join, '', ''));
        $this->assertEquals('1,3-7 &amp; 9', Format::groupRanges($str_join, '-', ','));
        $this->assertEquals('13-7 &amp; 9', Format::groupRanges($str_join, '-', ''));
        // Join and both separators.
        $this->assertEquals('1379', Format::groupRanges($str_join, '', '', ''));
        $this->assertEquals('1,3-7 and 9', Format::groupRanges($str_join, '-', ',', ' and '));
        $this->assertEquals('1,3-7,9', Format::groupRanges($str_join, '-', ',', ','));
    }
}
