<?php

namespace Tests\PHPUnit\Unit\Helpers;

use Tests\PHPUnit\Base\UnitTestCase;
use DeBear\Helpers\TransientStorage;
use DeBear\Models\Skeleton\User;

class TransientStorageTest extends UnitTestCase
{
    /**
     * Test the transient storage of session data
     * @return void
     */
    public function testTransientStorage(): void
    {
        User::doLogout(); // Ensure nobody logged in at start of the test.

        // Log a user in.
        User::doLogin([
            'user_id' => 'test_user',
            'password' => 'testlogindetails',
        ]);

        // Set a transient value, ensure you get it back.
        TransientStorage::set('ci.test.bool', true);
        $this->assertTrue(TransientStorage::get('ci.test.bool'));
        TransientStorage::set('ci.test.string', 'Hello, world!');
        $this->assertEquals('Hello, world!', TransientStorage::get('ci.test.string'));

        // Log the user out and ensure no value is now returned.
        User::doLogout();
        $this->assertNull(TransientStorage::get('ci.test.bool'));
        $this->assertNull(TransientStorage::get('ci.test.string'));
    }
}
