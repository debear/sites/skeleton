<?php

namespace Tests\PHPUnit\Unit\Helpers;

use Tests\PHPUnit\Base\UnitTestCase;
use DeBear\Helpers\Merge;
use DeBear\Exceptions\Resources\MissingException;

class MergeTest extends UnitTestCase
{
    /**
     * Test merging JavaScript
     * @return void
     */
    public function testJS(): void
    {
        // Perform the merge.
        $files = [
            'skel/vars.js',
            'skel/input.js',
        ];
        $merged = Merge::JS($files);
        $this->assertStringStartsWith('merged/', $merged);
        $this->assertStringEndsWith('.js', $merged);

        // Re-run, expecting the same output.
        $remerged = Merge::JS($files);
        $this->assertEquals($remerged, $merged);
    }

    /**
     * Test merging CSS
     * @return void
     */
    public function testCSS(): void
    {
        // Perform the merge.
        $files = [
            'skel/reset.css',
            'skel/general.css',
        ];
        $merged = Merge::CSS($files);
        $this->assertStringStartsWith('merged/', $merged);
        $this->assertStringEndsWith('.css', $merged);

        // Re-run, expecting the same output.
        $remerged = Merge::CSS($files);
        $this->assertEquals($remerged, $merged);
    }

    /**
     * Test merging with a missing file
     * @return void
     */
    public function testMissing(): void
    {
        // First, as a valid file with the content simply missing.
        $files = [
            'skel/reset.css',
            'skel/missing.css',
        ];
        $merged = Merge::CSS($files);
        $this->assertStringStartsWith('merged/', $merged);
        $this->assertStringEndsWith('.css', $merged);

        // Then as an exception.
        $files = [
            'skel/general.css',
            'skel/missing.css',
        ];
        $this->setTestConfig(['debear.resources.missing-exception' => true]);
        $caught = false;
        try {
            $merged = Merge::CSS($files);
        } catch (MissingException $e) {
            $caught = true;
        }
        $this->assertTrue($caught);
    }
}
