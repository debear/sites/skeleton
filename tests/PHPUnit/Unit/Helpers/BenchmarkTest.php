<?php

namespace Tests\PHPUnit\Unit\Helpers;

use Tests\PHPUnit\Base\UnitTestCase;
use DeBear\Helpers\Benchmark;

class BenchmarkTest extends UnitTestCase
{
    /**
     * Test the basic start/stopping
     * @return void
     */
    public function testBasic(): void
    {
        // Ensure enabled within the config.
        $this->setTestConfig(['debear.benchmark.enabled' => true]);
        Benchmark::unitTestRefresh();

        // Run.
        Benchmark::scriptStart();
        usleep(100); // 100 microseconds, 0.1 milliseconds.
        Benchmark::scriptEnd();

        // Test output.
        $start = Benchmark::getScriptStartTime();
        $stop = Benchmark::getScriptEndTime();
        $runtime = Benchmark::getScriptRunTime();
        $this->assertGreaterThan($start, $stop);
        $this->assertGreaterThan(0, $runtime);
    }

    /**
     * Test when disabled
     * @return void
     */
    public function testDisabled(): void
    {
        // Disable within the config.
        $this->setTestConfig(['debear.benchmark.enabled' => false]);
        Benchmark::unitTestRefresh();

        // Run.
        Benchmark::scriptStart();
        usleep(100); // 100 microseconds, 0.1 milliseconds.
        Benchmark::start('key');
        usleep(100); // 100 microseconds, 0.1 milliseconds.
        Benchmark::stop('key');
        usleep(100); // 100 microseconds, 0.1 milliseconds.
        Benchmark::scriptEnd();

        // Test output.
        $start = Benchmark::getScriptStartTime();
        $stop = Benchmark::getScriptEndTime();
        $runtime = Benchmark::getScriptRunTime();
        $this->assertGreaterThan($start, $stop);
        $this->assertGreaterThan(0, $runtime);
    }

    /**
     * Test the start corruption
     * @return void
     */
    public function testStartCorruption(): void
    {
        // Ensure enabled within the config.
        $this->setTestConfig(['debear.benchmark.enabled' => true]);
        Benchmark::unitTestRefresh();

        // Start script.
        Benchmark::scriptStart();

        // Double start.
        Benchmark::start('key1');
        Benchmark::start('key1');

        // End script.
        Benchmark::scriptEnd();

        // Get the rendered output to check for the error.
        $render = response('<!-- Benchmark: Render -->');
        Benchmark::render($render);
        $content = $render->getContent();
        $this->assertStringContainsString(
            '<dt class="error">Unable to Process Benchmark Stamps</dt>',
            $content
        );
        $this->assertStringContainsString(
            '<dd>Requested to start benchmark key \'key1\' when it has already been started. '
                . 'Potential duplicate name?</dd>',
            $content
        );
    }

    /**
     * Test the stop corruption
     * @return void
     */
    public function testStopCorruption(): void
    {
        // Ensure enabled within the config.
        $this->setTestConfig(['debear.benchmark.enabled' => true]);
        Benchmark::unitTestRefresh();

        // Start script.
        Benchmark::scriptStart();

        // Double stop.
        Benchmark::start('key2');
        Benchmark::stop('key3');
        Benchmark::stop('key2');

        // End script.
        Benchmark::scriptEnd();

        // Get the rendered output to check for the error.
        $render = response('<!-- Benchmark: Render -->');
        Benchmark::render($render);
        $content = $render->getContent();
        $this->assertStringContainsString(
            '<dt class="error">Unable to Process Benchmark Stamps</dt>',
            $content
        );
        $this->assertStringContainsString(
            '<dd>Requested to stop benchmark key \'key3\', when \'key2\' is the currently active benchmark key.</dd>',
            $content
        );
    }

    /**
     * Test rendering when enabled
     * @return void
     */
    public function testRenderEnabled(): void
    {
        // Ensure enabled within the config.
        $this->setTestConfig(['debear.benchmark.enabled' => true]);
        Benchmark::unitTestRefresh();

        // Run.
        Benchmark::scriptStart();
        usleep(100); // 100 microseconds, 0.1 milliseconds.
        Benchmark::start('key');
        usleep(100); // 100 microseconds, 0.1 milliseconds.
        Benchmark::stop('key');
        usleep(100); // 100 microseconds, 0.1 milliseconds.
        Benchmark::scriptEnd();

        // With the tag.
        $raw = 'A<!-- Benchmark: Render -->B';
        $response_with_tag = response($raw);
        Benchmark::render($response_with_tag);
        $this->assertNotEquals($raw, $response_with_tag->getContent());

        // Without the tag.
        $raw = 'AB';
        $response_without_tag = response($raw);
        Benchmark::render($response_without_tag);
        $this->assertEquals($raw, $response_without_tag->getContent());
    }

    /**
     * Test rendering when disabled
     * @return void
     */
    public function testRenderDisabled(): void
    {
        // Disable within the config.
        $this->setTestConfig(['debear.benchmark.enabled' => false]);
        Benchmark::unitTestRefresh();

        // Run.
        Benchmark::scriptStart();
        usleep(100); // 100 microseconds, 0.1 milliseconds.
        Benchmark::start('key');
        usleep(100); // 100 microseconds, 0.1 milliseconds.
        Benchmark::stop('key');
        usleep(100); // 100 microseconds, 0.1 milliseconds.
        Benchmark::scriptEnd();

        // With the tag.
        $raw = 'A<!-- Benchmark: Render -->B';
        $response_with_tag = response($raw);
        Benchmark::render($response_with_tag);
        $this->assertEquals($raw, $response_with_tag->getContent());
    }
}
