<?php

namespace Tests\PHPUnit\Unit\Helpers;

use Tests\PHPUnit\Base\UnitTestCase;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\Policies;
use DeBear\Models\Skeleton\User;
use DeBear\Exceptions\PoliciesException;

class PoliciesTest extends UnitTestCase
{
    /**
     * Cached copy of the default policy rules for re-building
     * @var array
     */
    protected static $default_rules;
    /**
     * List of test users
     * @var array
     */
    protected static $users;

    /**
     * Reset the policies between tests
     * @return void
     */
    protected function setUp(): void
    {
        // Load the standard test setup methods.
        parent::setUp();
        // Preserve / Restore default rules between tests.
        if (!isset(static::$default_rules)) {
            static::$default_rules = FrameworkConfig::get('debear.policies');
            static::$users = [
                10 => User::find(10),
                20 => User::find(20),
            ];
        } else {
            FrameworkConfig::set(['debear.policies' => static::$default_rules]);
        }
        // Force a reset.
        Policies::reset();
    }

    /**
     * Test the policies generated for a known active user
     * @return void
     */
    public function testActiveUser(): void
    {
        // Get and setup.
        Policies::setup(static::$users[10]);

        // Run the tests.
        $this->assertFalse(Policies::match('guest'));
        $this->assertTrue(Policies::match('logged_in'));
        $this->assertTrue(Policies::match('user'));
        $this->assertFalse(Policies::match('user:admin'));
        $this->assertTrue(Policies::match('user:verified'));
        $this->assertFalse(Policies::match('user:unverified'));
        $this->assertFalse(Policies::match('login:state')); // Not technically logged in.
        $this->assertFalse(Policies::match('state:password:recent'));
        $this->assertFalse(Policies::match('referer'));
        $this->assertFalse(Policies::match('referer:google'));
        // Matching multiple (all).
        $this->assertTrue(Policies::matchAll(['logged_in', 'user']));
        $this->assertFalse(Policies::matchAll(['guest', 'logged_in']));
        $this->assertFalse(Policies::matchAll(['guest', 'user:admin']));
        // Matching multiple (any).
        $this->assertTrue(Policies::matchAny(['logged_in', 'user']));
        $this->assertTrue(Policies::matchAny(['guest', 'logged_in']));
        $this->assertFalse(Policies::matchAny(['guest', 'user:admin']));

        // Force a recalc, and check some high-level policies.
        Policies::recalc(static::$users[10]);
        $this->assertFalse(Policies::match('guest'));
        $this->assertTrue(Policies::match('logged_in'));

        // Re-test with an empty rule.
        FrameworkConfig::set(['debear.policies.empty-rule' => false]);
        Policies::recalc(static::$users[10]);
        $this->assertFalse(Policies::match('guest'));
        $this->assertTrue(Policies::match('logged_in'));
    }

    /**
     * Test the policies generated for a known unverified user
     * @return void
     */
    public function testUnverifiedUser(): void
    {
        // Get and setup.
        Policies::setup(static::$users[20]);
        // Also test re-calling (with previous version cached).
        Policies::setup(static::$users[20]);

        // Run the tests.
        $this->assertFalse(Policies::match('guest'));
        $this->assertTrue(Policies::match('logged_in'));
        $this->assertTrue(Policies::match('user'));
        $this->assertFalse(Policies::match('user:admin'));
        $this->assertFalse(Policies::match('user:verified'));
        $this->assertTrue(Policies::match('user:unverified'));
        $this->assertFalse(Policies::match('login:state')); // Not technically logged in.
        $this->assertFalse(Policies::match('state:password:recent'));
        $this->assertFalse(Policies::match('referer'));
        $this->assertFalse(Policies::match('referer:google'));
    }

    /**
     * Test the policies generated for the fallback user
     * @return void
     */
    public function testFallback(): void
    {
        // Setup with no user passed.
        Policies::setup(null);

        // Run the tests.
        $this->assertTrue(Policies::match('guest')); // The only one that should match.
        $this->assertFalse(Policies::match('logged_in'));
        $this->assertFalse(Policies::match('user'));
        $this->assertFalse(Policies::match('user:admin'));
        $this->assertFalse(Policies::match('user:verified'));
        $this->assertFalse(Policies::match('user:unverified'));
        $this->assertFalse(Policies::match('login:state'));
        $this->assertFalse(Policies::match('state:password:recent'));
        $this->assertFalse(Policies::match('referer'));
        $this->assertFalse(Policies::match('referer:google'));

        // Re-test with an empty rule.
        FrameworkConfig::get(['debear.policies.empty-rule' => false]);
        Policies::recalc(null);
        $this->assertTrue(Policies::match('guest')); // The only one that should match.
        $this->assertFalse(Policies::match('logged_in'));
    }

    /**
     * Test the policies with some advanced child-based structures
     * @return void
     */
    public function testAdvancedChild(): void
    {
        FrameworkConfig::set([
            'debear.policies.advanced' => [
                'name' => 'Advanced Parent',
                'children' => [
                    'advanced:a' => [
                        'name' => 'Advanced A',
                        'rules' => [
                            ['id', '=', 10],
                        ],
                    ],
                    'advanced:b' => [
                        'name' => 'Advanced B',
                        'rules' => [
                            ['id', '=', 20],
                        ],
                    ],
                ],
            ],
        ]);

        // Matching at least one child component.
        Policies::recalc(static::$users[20]);
        $this->assertTrue(Policies::match('advanced'));

        // Matching all child components.
        FrameworkConfig::set(['debear.policies.advanced.all-children' => true]);
        Policies::recalc(static::$users[20]);
        $this->assertFalse(Policies::match('advanced'));
    }

    /**
     * Test the policies generated from scratch
     * @return void
     */
    public function testFromScratch(): void
    {
        // No user, so no user-based matches expected.
        $this->assertTrue(Policies::match('guest'));
        $this->assertFalse(Policies::match('logged_in'));
    }

    /**
     * Test the various operators within parseRules
     * @return void
     */
    public function testParseRuleOps(): void
    {
        $policies = array_merge(static::$default_rules, [
            'equality-gt' => [
                'name' => 'Equality: >',
                'rules' => [
                    ['id', '>', 20],
                ],
                'fallback' => true,
            ],
            'equality-lt' => [
                'name' => 'Equality: <',
                'rules' => [
                    ['id', '<', 20],
                ],
                'fallback' => true,
            ],
            'equality-gte' => [
                'name' => 'Equality: >=',
                'rules' => [
                    ['id', '>=', 20],
                ],
                'fallback' => true,
            ],
            'equality-lte' => [
                'name' => 'Equality: <=',
                'rules' => [
                    ['id', '<=', 20],
                ],
                'fallback' => true,
            ],
            'in-scalar' => [
                'name' => 'In: Scalar',
                'rules' => [
                    ['id', 'IN', '10,20'],
                ],
            ],
            'in-array' => [
                'name' => 'In: Array',
                'rules' => [
                    ['id', 'IN', [10, 20]],
                ],
            ],
        ]);
        FrameworkConfig::set(['debear.policies' => $policies]);
        Policies::setup(static::$users[20]);

        $this->assertFalse(Policies::match('equality-gt'));
        $this->assertFalse(Policies::match('equality-lt'));
        $this->assertTrue(Policies::match('equality-gte'));
        $this->assertTrue(Policies::match('equality-lte'));
        $this->assertTrue(Policies::match('in-scalar'));
        $this->assertTrue(Policies::match('in-array'));
    }

    /**
     * Test the various exceptions generated
     * @return void
     */
    public function testExceptions(): void
    {
        // Is-not-valid-rule exception rule within parseRules.
        FrameworkConfig::set([
            'debear.policies' => static::$default_rules,
            'debear.policies.exception' => [
                'name' => 'Exception',
                'rules' => [
                    ['user_id', 'UNKNOWN', true],
                ],
            ],
        ]);
        $caught = false;
        try {
            Policies::setup(static::$users[20]);
        } catch (PoliciesException $e) {
            $caught = true;
        }
        $this->assertTrue($caught);

        // Is-not-valid-method exception rule within parseRules.
        FrameworkConfig::set([
            'debear.policies' => static::$default_rules,
            'debear.policies.invalid-method' => [
                'name' => 'Invalid: Method Definition',
                'rules' => [
                    ['@\DeBear\Models\Skeleton\User::checkLevel'],
                ],
            ],
        ]);
        $caught = false;
        try {
            Policies::recalc(static::$users[20]);
        } catch (PoliciesException $e) {
            $caught = true;
        }
        $this->assertTrue($caught);

        // Is-not-valid-array exception rule within parseRules.
        FrameworkConfig::set([
            'debear.policies' => static::$default_rules,
            'debear.policies.invalid-arr' => [
                'name' => 'Invalid: Array Definition',
                'rules' => [
                    ['user_id', 'SET'],
                ],
            ],
        ]);
        $caught = false;
        try {
            Policies::recalc(static::$users[20]);
        } catch (PoliciesException $e) {
            $caught = true;
        }
        $this->assertTrue($caught);

        // The is-not-array exception rule within setupWorker.
        FrameworkConfig::set([
            'debear.policies' => static::$default_rules,
            'debear.policies.invalid-rule' => [
                'name' => 'Invalid: Not Array',
                'rules' => false,
            ],
        ]);
        $caught = false;
        try {
            Policies::recalc(static::$users[20]);
        } catch (PoliciesException $e) {
            $caught = true;
        }
        $this->assertTrue($caught);

        // Test the duplicate-code exception rule within setupWorker.
        FrameworkConfig::set([
            'debear.policies' => static::$default_rules,
            'debear.policies.test' => [
                'name' => 'Test Parent',
                'grouping' => true,
                'children' => [
                    'test' => [
                        'name' => 'Test Child',
                        'rules' => [
                            ['user_id', 'SET', true],
                        ],
                    ],
                ],
            ],
        ]);
        $caught = false;
        try {
            Policies::recalc(static::$users[20]);
        } catch (PoliciesException $e) {
            $caught = true;
        }
        $this->assertTrue($caught);
    }

    /**
     * Test the debug info
     * @return void
     */
    public function testDebug(): void
    {
        // Prepare a Guest user.
        Policies::setup(null);
        $debug = Policies::getDebug();
        $this->assertIsArray($debug);
        foreach ($debug['matches'] as $rule => $match) {
            $this->assertEquals($match, Policies::match($rule));
        }
    }
}
