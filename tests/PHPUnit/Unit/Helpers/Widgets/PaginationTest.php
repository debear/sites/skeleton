<?php

namespace Tests\PHPUnit\Unit\Helpers\Widgets;

use Tests\PHPUnit\Base\UnitTestCase;
use DeBear\Helpers\Widgets\Pagination;
use DeBear\Exceptions\ArraysException;
use Illuminate\View\View;

class PaginationTest extends UnitTestCase
{
    /**
     * Test the start / middle / end
     * @return void
     */
    public function testPageNumbers(): void
    {
        // Start.
        $start_obj = Pagination::render(100, 1, '/test/', 1); // First, as object.
        $start = $start_obj->render(); // But then also as a string.
        $this->assertInstanceOf(View::class, $start_obj);
        $this->assertStringContainsString('<strong>1</strong>', $start);
        $this->assertStringContainsString('<a href="/test/2" class="onclick_target" data-page="2">2</a>', $start);
        $this->assertStringContainsString('<a href="/test/10" class="onclick_target" data-page="10">10</a>', $start);
        $this->assertStringNotContainsString('<a href="/test/11" class="onclick_target" data-page="11">11</a>', $start);
        $this->assertStringContainsString(
            '<a href="/test/100" class="onclick_target" data-page="100"><em>100</em></a>',
            $start
        );
        $this->assertStringNotContainsString('&laquo; Prev<span class="hidden-m">ious Page</span>', $start);
        $this->assertStringContainsString('<a href="/test/2" class="onclick_target" data-page="2">
                Next<span class="hidden-m"> Page</span> &raquo;
            </a>', $start);
        $this->assertStringContainsString('&hellip;', $start);

        // Middle.
        $middle = Pagination::render(100, 1, '/test/', 50)->render(); // From now on, string only.
        $this->assertStringContainsString('<strong>50</strong>', $middle);
        $this->assertStringContainsString(
            '<a href="/test/1" class="onclick_target" data-page="1"><em>1</em></a>',
            $middle
        );
        $this->assertStringNotContainsString(
            '<a href="/test/45" class="onclick_target" data-page="45">45</a>',
            $middle
        );
        $this->assertStringContainsString('<a href="/test/46" class="onclick_target" data-page="46">46</a>', $middle);
        $this->assertStringContainsString('<a href="/test/55" class="onclick_target" data-page="55">55</a>', $middle);
        $this->assertStringNotContainsString(
            '<a href="/test/56" class="onclick_target" data-page="56">56</a>',
            $middle
        );
        $this->assertStringContainsString(
            '<a href="/test/100" class="onclick_target" data-page="100"><em>100</em></a>',
            $middle
        );
        $this->assertStringContainsString('<a href="/test/49" class="onclick_target" data-page="49">
                &laquo; Prev<span class="hidden-m">ious Page</span>
            </a>', $middle);
        $this->assertStringContainsString('<a href="/test/51" class="onclick_target" data-page="51">
                Next<span class="hidden-m"> Page</span> &raquo;
            </a>', $middle);
        $this->assertStringContainsString('&hellip;', $middle);

        // End.
        $end = Pagination::render(100, 1, '/test/', 100)->render();
        $this->assertStringContainsString('<strong>100</strong>', $end);
        $this->assertStringContainsString(
            '<a href="/test/1" class="onclick_target" data-page="1"><em>1</em></a>',
            $end
        );
        $this->assertStringNotContainsString('<a href="/test/90" class="onclick_target" data-page="90">90</a>', $end);
        $this->assertStringContainsString('<a href="/test/91" class="onclick_target" data-page="91">91</a>', $end);
        $this->assertStringContainsString('<a href="/test/99" class="onclick_target" data-page="99">99</a>', $end);
        $this->assertStringNotContainsString(
            '<a href="/test/100" class="onclick_target" data-page="100">100</a>',
            $end
        );
        $this->assertStringContainsString('<a href="/test/99" class="onclick_target" data-page="99">
                &laquo; Prev<span class="hidden-m">ious Page</span>
            </a>', $end);
        $this->assertStringNotContainsString('Next<span class="hidden-m"> Page</span> &raquo;', $end);
        $this->assertStringContainsString('&hellip;', $end);

        // Short.
        $short = Pagination::render(5, 1, '/test/', 3)->render();
        $this->assertStringContainsString('<strong>3</strong>', $short);
        $this->assertStringContainsString('<a href="/test/1" class="onclick_target" data-page="1">1</a>', $short);
        $this->assertStringContainsString('<a href="/test/2" class="onclick_target" data-page="2">2</a>', $short);
        $this->assertStringContainsString('<a href="/test/5" class="onclick_target" data-page="5">5</a>', $short);
        $this->assertStringNotContainsString('<a href="/test/6" class="onclick_target" data-page="6">6</a>', $short);
        $this->assertStringContainsString('<a href="/test/2" class="onclick_target" data-page="2">
                &laquo; Prev<span class="hidden-m">ious Page</span>
            </a>', $short);
        $this->assertStringContainsString('<a href="/test/4" class="onclick_target" data-page="4">
                Next<span class="hidden-m"> Page</span> &raquo;
            </a>', $short);
        $this->assertStringNotContainsString('&hellip;', $short);
    }

    /**
     * Test the various link types
     * @return void
     */
    public function testLinkTypes(): void
    {
        $url = Pagination::render(100, 1, '/test/', 50)->render();
        $this->assertStringContainsString(
            '<a href="/test/1" class="onclick_target" data-page="1"><em>1</em></a>',
            $url
        );
        $no = Pagination::render(100, 1, '', 50)->render();
        $this->assertStringContainsString('<a  class="onclick_target" data-page="1"><em>1</em></a>', $no);
    }

    /**
     * Test input options
     * @return void
     */
    public function testOptions(): void
    {
        $css = Pagination::render(100, 1, '/test/', 50, ['extra_css' => 'test-css'])->render();
        $this->assertStringContainsString(
            '<ul class="inline_list pagination test-css onclick_target" id="pagination">',
            $css
        );
        $tmpl = Pagination::render(100, 1, '/test-${page_num}', 50, ['str_replace_page_num' => true])->render();
        $this->assertStringContainsString('/test-1', $tmpl);
        $this->assertStringContainsString('/test-100', $tmpl);
        $this->assertStringNotContainsString('/test-50', $tmpl);
        $this->assertStringNotContainsString('/test/50', $tmpl);
    }
}
