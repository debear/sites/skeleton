<?php

namespace Tests\PHPUnit\Unit\Helpers\Comms;

use Tests\PHPUnit\Base\UnitTestCase;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\Comms\Email;
use DeBear\Helpers\HTTP;
use DeBear\Helpers\HTML;
use DeBear\Exceptions\Comms\EmailException;
use DeBear\Models\Skeleton\CommsEmail;

class EmailTest extends UnitTestCase
{
    /**
     * Test building an email correctly
     * @return void
     */
    public function testBuild(): void
    {
        // First, via ID.
        $by_id = Email::send('User: Reset Request', 10, [
            'callback' => [get_class($this), 'customData'],
        ]);
        $this->assertInstanceOf(CommsEmail::class, $by_id);
        $this->assertStringContainsString('Test Callback', $by_id->email_body);
        $this->assertStringNotContainsString('Content-Type: text/html; charset="UTF-8"', $by_id->email_body);

        // Then via email address.
        $by_email = Email::send('User: Reset Request', 'test@debear.uk', [
            'type' => 'html',
        ]);
        $this->assertInstanceOf(CommsEmail::class, $by_email);
        $this->assertStringNotContainsString('Test Callback', $by_email->email_body);
        $this->assertStringContainsString('Content-Type: text/html; charset="UTF-8"', $by_email->email_body);

        // Missing / Unknown template?
        $caught = false;
        try {
            Email::send('Unknown', 10);
        } catch (EmailException $e) {
            $caught = true;
        }
        $this->assertTrue($caught);
    }

    /**
     * Test the sending status returns of the email
     * @return void
     */
    public function testSend(): void
    {
        $orig = FrameworkConfig::get('debear.email.enabled');
        FrameworkConfig::set(['debear.email.enabled' => true]);

        // Opted Out.
        $opt_out = Email::send('User: Reset Request', 10, [
            'opt_out' => true,
        ]);
        $this->assertEquals('opt_out', $opt_out->email_status);

        // Disabled (Blocked).
        FrameworkConfig::set(['debear.email.enabled' => false]);
        $opt_out = Email::send('User: Reset Request', 10);
        $this->assertEquals('blocked', $opt_out->email_status);
        FrameworkConfig::set(['debear.email.enabled' => true]);

        // Unverified (Blocked).
        $opt_out = Email::send('User: Reset Request', 20); // 20 is our Unverified user.
        $this->assertEquals('blocked', $opt_out->email_status);

        // Sent (Never sends).
        $opt_out = Email::send('User: Reset Request', 10, [
            'send_time' => 'now',
        ]);
        $this->assertEquals('failed', $opt_out->email_status);

        // Queued.
        $opt_out = Email::send('User: Reset Request', 10);
        $this->assertEquals('queued', $opt_out->email_status);

        FrameworkConfig::set(['debear.email.enabled' => $orig]);
    }

    /**
     * Test the codes and checksums
     * @return void
     */
    public function testChecksums(): void
    {
        $tests = [
            'sJbwdoduuWwjOG5D' => ['c1' => 'faf48675', 'c2' => '243b3040'],
            'Jjb6dMVVO0wQHjQE' => ['c1' => 'd8c8dd1c', 'c2' => 'aaeb2fa0'],
            '1V3sIO3aFNXaUkor' => ['c1' => '68768d74', 'c2' => '1f1b5136'],
            'DV0GsxyRAuu46dDY' => ['c1' => 'edd8afb0', 'c2' => '2411325a'],
            'xOxCLrCHi5t6voBV' => ['c1' => 'a9997188', 'c2' => 'a90ae5bf'],
        ];
        foreach ($tests as $code => $checksums) {
            // Does the checksum match?
            $checks = Email::calculateChecksum($code);
            $this->assertEquals($checksums['c1'], $checks[0]);
            $this->assertEquals($checksums['c2'], $checks[1]);
            // Then validated correctly.
            $this->assertTrue(Email::validateCode($code, $checksums['c1'], $checksums['c2']));
            // And to be sure, incorrectly.
            $this->assertFalse(Email::validateCode($code, 'x' . $checksums['c1'], $checksums['c2']));
            $this->assertFalse(Email::validateCode($code, $checksums['c1'], 'x' . $checksums['c2']));
        }
    }

    /**
     * Test the email links
     * @return void
     */
    public function testLinks(): void
    {
        /* Emails */
        $addr = HTML::buildEmailRaw('noreply');

        // As Plain-Text.
        $email_plain = Email::send('User: Reset Request', 10, [
            'data' => [
                'body' => 'Raw: <a href="mailto:' . $addr . '">' . $addr . '</a>, '
                    . 'HTML: <a href="mailto:' . $addr . '">Email</a>',
            ],
        ]);
        $this->assertStringContainsString('Raw: noreply@debear.test, HTML: Email', $email_plain->email_body);

        // As HTML.
        $email_html = Email::send('User: Reset Request', 10, [
            'type' => 'html',
            'data' => [
                'body' => 'Raw: <a href="mailto:' . $addr . '">' . $addr . '</a>, '
                    . 'HTML: <a href="mailto:' . $addr . '">Email</a>',
            ],
        ]);
        $this->assertStringContainsString(
            'Raw: <a href="mailto:noreply@debear.test">noreply@debear.test</a>, '
            . 'HTML: <a href="mailto:noreply@debear.test">Email</a>',
            $email_html->email_body
        );
        // It still includes our plain-text version...
        $this->assertStringContainsString('Raw: noreply@debear.test, HTML: Email', $email_html->email_body);

        /* Links */
        $dom_raw = 'https:' . HTTP::buildDomain();
        $dom_test = 'https://debear.test';
        // As Plain-Text.
        $link_plain = Email::send('User: Reset Request', 10, [
            'data' => [
                'body' => 'Raw: <a href="' . $dom_raw . '/test">' . $dom_raw . '/test</a>, '
                    . 'HTML: <a href="' . $dom_raw . '/test">Test Page</a>, '
                    . 'Dupe Raw: <a href="' . $dom_raw . '/test">' . $dom_raw . '/test</a>, '
                    . 'Dupe HTML: <a href="' . $dom_raw . '/test">Test Page</a>',
            ],
        ]);
        $this->assertStringContainsString(
            'Raw: ' . $dom_test . '/test, HTML: Test Page [1], '
                . 'Dupe Raw:' . "\n" . '' . $dom_test . '/test, Dupe HTML: Test Page [1]',
            $link_plain->email_body
        );
        $this->assertStringContainsString('[1] ' . $dom_test . '/test', $link_plain->email_body);

        // As HTML.
        $link_html = Email::send('User: Reset Request', 10, [
            'type' => 'html',
            'data' => [
                'body' => 'Raw: <a href="' . $dom_raw . '/test">' . $dom_raw . '/test</a>, '
                    . 'HTML: <a href="' . $dom_raw . '/test">Test Page</a>, '
                    . 'Port: <a href="' . $dom_raw . ':8080/test/?query=arg">Test Port</a>',
            ],
        ]);
        $this->assertMatchesRegularExpression(
            '#Raw: <a href="' . $dom_test . '/eml-[a-z0-9]{16}-[a-f0-9]{8}-[a-f0-9]{8}">' . $dom_test . '/test</a>#i',
            $link_html->email_body
        );
        $this->assertMatchesRegularExpression(
            '#HTML: <a href="' . $dom_test . '/eml-[a-z0-9]{16}-[a-f0-9]{8}-[a-f0-9]{8}">Test Page</a>#i',
            $link_html->email_body
        );
        $this->assertStringContainsString(
            'Port: <a href="' . $dom_test . ':8080/test/?query=arg">Test Port</a>',
            $link_html->email_body
        );
        // It still includes our plain-text version...
        $this->assertStringContainsString(
            'Raw: ' . $dom_test . '/test, HTML: Test Page [1], Port: Test Port [2]',
            $link_html->email_body
        );
        $this->assertStringContainsString(
            '[1] ' . $dom_test . '/test' . "\n" . '[2] ' . $dom_test . ':8080/test/?query=arg',
            $link_html->email_body
        );
    }

    /**
     * A test method for adding data to the email data block
     * @param array $args The current arguments in the email data block.
     * @return void
     */
    public static function customData(array &$args): void
    {
        $args['data']['body'] = 'Test Callback';
    }
}
