<?php

namespace Tests\PHPUnit\Unit\Helpers;

use Tests\PHPUnit\Base\UnitTestCase;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\Resources;
use DeBear\Helpers\HTTP;
use DeBear\Exceptions\ResourcesException;

class ResourcesTest extends UnitTestCase
{
    /**
     * Test building CSS/JS lists
     * @return void
     */
    public function testCSSJS(): void
    {
        // Add some custom libs to config.
        Resources::addCSS('test-prepend.css', ['prepend' => true]);
        Resources::addCSS('test-append.css');
        Resources::addJS('test-prepend.js', ['prepend' => true]);
        Resources::addJS('test-append.js');

        // Get the CSS list from config.
        $prefix = '//static.debear.test/www/css/';
        $css = Resources::getCSS();
        $this->assertIsArray($css);
        $this->assertContains("{$prefix}test-prepend.css", $css);
        $this->assertContains("{$prefix}test-append.css", $css);
        $ind_pre = array_search("{$prefix}test-prepend.css", $css);
        $ind_app = array_search("{$prefix}test-append.css", $css);
        $this->assertGreaterThan($ind_pre, $ind_app);

        // Get the JS list from config.
        $prefix = str_replace('css', 'js', $prefix);
        $js = Resources::getJS();
        $this->assertIsArray($js);
        $this->assertContains("{$prefix}test-prepend.js", $js);
        $this->assertContains("{$prefix}test-append.js", $js);
        $ind_pre = array_search("{$prefix}test-prepend.js", $js);
        $ind_app = array_search("{$prefix}test-append.js", $js);
        $this->assertGreaterThan($ind_pre, $ind_app);

        // Merging the appropriate file(s).
        $orig = [
            'debear.resources.js_merge' => FrameworkConfig::get('debear.resources.js_merge'),
            'debear.resources.js' => FrameworkConfig::get('debear.resources.js'),
        ];
        FrameworkConfig::set([
            'debear.resources.js_merge' => true,
            'debear.resources.js' => ['skel/vars.js', 'skel/dom.js'],
        ]);
        $js = Resources::getJS();
        $this->assertIsArray($js);
        $this->assertCount(1, $js);
        FrameworkConfig::set($orig);

        // Filename conversion.
        $in = ['skel/test.js', 'test.js'];
        $exp = ['skel/test.js', 'www/test.js'];
        $out = Resources::convert($in);
        $this->assertIsArray($out);
        $this->assertEquals($exp, $out);

        // Resource pre-processing.
        $orig = ['debear.resources.js' => FrameworkConfig::get('debear.resources.js')];
        FrameworkConfig::set([
            'debear.resources.js' => [
                [
                    'file' => 'test-env.js',
                    'env' => [HTTP::class, 'isTest'],
                ],
                [
                    'file' => 'test-pol-pass.js',
                    'policies' => 'guest',
                ],
                [
                    'file' => 'test-pol-fail.js',
                    'policies' => 'logged_in',
                ],
            ],
        ]);
        $custom = Resources::getJS();
        $this->assertIsArray($custom);
        $this->assertCount(2, $custom);
        FrameworkConfig::set($orig);

        // Invalid file.
        $orig = ['debear.resources.css' => FrameworkConfig::get('debear.resources.css')];
        FrameworkConfig::set(['debear.resources.css' => [['file' => 'test-fail.css']]]);
        $custom = Resources::getCSS();
        $this->assertIsArray($custom);
        $this->assertEmpty($custom);
        FrameworkConfig::set($orig);

        // Invalid file, throwing fatal.
        $orig = ['debear.resources.css' => FrameworkConfig::get('debear.resources.css')];
        FrameworkConfig::set(['debear.resources.css' => [
            [
                'file' => 'test-fatal.css',
                'throw-fatal' => true,
            ]
        ]]);
        $this->expectException(ResourcesException::class);
        $custom = Resources::getCSS();
    }

    /**
     * Test passing externally sourced resources
     * @return void
     */
    public function testCSSJSExternal(): void
    {
        // Now with bespoke minified resources.
        $orig = [
            'debear.resources.css_merge' => FrameworkConfig::get('debear.resources.css_merge'),
            'debear.reources.css' => FrameworkConfig::get('debear.resources.css'),
        ];
        FrameworkConfig::set(['debear.resources.css_merge' => 'min.css']);
        Resources::addCSS('skel/widgets/subnav.css', ['external' => true]);
        // Test the output.
        $css = Resources::getCSS('desktop');
        $this->assertIsArray($css);
        $this->assertEquals(2, count($css));
        $this->assertStringStartsWith('//static.debear.test/www/css/merged/', $css[0]);
        $this->assertStringEndsWith('.css', $css[0]);
        $this->assertEquals('//static.debear.test/www/css/min.css', $css[1]);
        // Revert original config.
        FrameworkConfig::set($orig);
    }
}
