<?php

namespace Tests\PHPUnit\Unit\Helpers;

use Tests\PHPUnit\Base\UnitTestCase;
use DeBear\Helpers\Arrays;
use DeBear\Exceptions\ArraysException;

class ArrayTest extends UnitTestCase
{
    /**
     * Test the various permutations of the Arrays::contains() method
     * @return void
     */
    public function testContains(): void
    {
        // Input not an array (null, scalar).
        $this->assertFalse(Arrays::contains('val', null));
        $this->assertFalse(Arrays::contains('val', 1));

        // Main tests.
        $test = ['a', 'd', 1, 2.5, 4];
        // As strings.
        $this->assertTrue(Arrays::contains('a', $test));
        $this->assertFalse(Arrays::contains('b', $test));
        $this->assertFalse(Arrays::contains('c', $test));
        $this->assertTrue(Arrays::contains('d', $test));
        // As numbers.
        $this->assertTrue(Arrays::contains(1, $test));
        $this->assertFalse(Arrays::contains(2, $test));
        $this->assertTrue(Arrays::contains(2.5, $test));
        $this->assertFalse(Arrays::contains(2.6, $test));
        $this->assertFalse(Arrays::contains(3, $test));
        $this->assertTrue(Arrays::contains(4, $test));
        // Strictness tests - explicitly false.
        $this->assertTrue(Arrays::contains(1, $test, false));
        $this->assertTrue(Arrays::contains('1', $test, false));
        $this->assertFalse(Arrays::contains(2, $test, false));
        $this->assertFalse(Arrays::contains('2', $test, false));
        // Strictness tests - explicitly true.
        $this->assertTrue(Arrays::contains(1, $test, true));
        $this->assertFalse(Arrays::contains('1', $test, true));
        $this->assertFalse(Arrays::contains(2, $test, true));
        $this->assertFalse(Arrays::contains('2', $test, true));
        // Confirming PHP's handling of "0 == 'b'" (PHP 7 == Oddly; PHP 8 == Sensibly).
        $test[] = 0;
        $this->assertTrue(Arrays::contains(0, $test, false));
        $this->assertTrue(Arrays::contains(0, $test, true));
        $this->assertFalse(Arrays::contains('b', $test, false)); // Added for explicitness, as expected PHP behaviour.
        $this->assertFalse(Arrays::contains('b', $test, true));

        // Non-scalar needle.
        $this->expectException(ArraysException::class);
        Arrays::contains([], []);
    }

    /**
     * Test the various permutations of the Arrays::merge() method
     * @return void
     */
    public function testMerging(): void
    {
        // Native sort.
        $a = [0];
        $b = [1];
        $c = Arrays::merge($a, $b);
        $this->assertTrue(is_array($c));
        $this->assertEquals([0, 1], $c);

        // Numeric, No common.
        $a = [0 => 0];
        $b = [1 => 1];
        $c = Arrays::merge($a, $b);
        $this->assertTrue(is_array($c));
        $this->assertEquals([0, 1], $c);

        // Assoc, No common.
        $a = ['a' => 0];
        $b = ['b' => 1];
        $c = Arrays::merge($a, $b);
        $this->assertTrue(is_array($c));
        $exp = ['a' => 0, 'b' => 1];
        $this->assertEquals($exp, $c);

        // Over-writing scalar.
        $a = ['a' => 0, 'b' => 1];
        $b = ['a' => 1];
        $c = Arrays::merge($a, $b);
        $this->assertTrue(is_array($c));
        $exp = ['a' => 1, 'b' => 1];
        $this->assertEquals($exp, $c);
        // And in reverse.
        $c = Arrays::merge($b, $a);
        $this->assertTrue(is_array($c));
        $exp = ['a' => 0, 'b' => 1];
        $this->assertEquals($exp, $c);

        // Converting Scalar.
        $a = ['a' => 0, 'b' => 1];
        $b = ['a' => 1];
        $c = Arrays::merge($a, $b, ['merge_scalar' => true]);
        $this->assertTrue(is_array($c));
        $exp = ['a' => [0, 1], 'b' => 1];
        $this->assertEquals($exp, $c);
        // And in reverse.
        $c = Arrays::merge($b, $a, ['merge_scalar' => true]);
        $this->assertTrue(is_array($c));
        $exp = ['a' => [1, 0], 'b' => 1];
        $this->assertEquals($exp, $c);

        // Merging Array w/Scalar.
        $a = ['a' => [0], 'b' => 1];
        $b = ['a' => 1];
        $c = Arrays::merge($a, $b);
        $this->assertTrue(is_array($c));
        $exp = ['a' => [0, 1], 'b' => 1];
        $this->assertEquals($exp, $c);
        // And then when the array is in $b.
        $a = ['a' => 0, 'b' => 1];
        $b = ['a' => [1]];
        $c = Arrays::merge($a, $b);
        $this->assertTrue(is_array($c));
        $this->assertEquals($exp, $c); // Expecting the same output as the first test.

        // Combine assoc.
        $a = [
            'key3' => true,
            'key2' => [
                'a' => 1,
                'b' => 3,
                'c' => 2,
            ],
            'key4' => [0, 1],
        ];
        $b = [
            'key2' => [
                'b' => 2,
                'c' => 3,
                'd' => 4,
            ],
            'key1' => false,
            'key4' => [2, 3],
        ];
        $c = Arrays::merge($a, $b);
        $this->assertTrue(is_array($c));
        $exp = [
            'key3' => true,
            'key2' => [
                'a' => 1,
                'b' => 2,
                'c' => 3,
                'd' => 4,
            ],
            'key4' => [0, 1, 2, 3],
            'key1' => false,
        ];
        $this->assertEquals($exp, $c);
    }

    /**
     * Test the various permutations of the Arrays::isAssociative() method
     * @return void
     */
    public function testAssociative(): void
    {
        // An associative array (should be true).
        $isAssoc = [
            'key' => 'value',
        ];
        $this->assertTrue(Arrays::isAssociative($isAssoc));

        // A numerically-indexed array (should be false).
        $isNumeric = [
            0,
            1,
            2,
        ];
        $this->assertFalse(Arrays::isAssociative($isNumeric));
    }
}
