<?php

namespace Tests\PHPUnit\Unit\Helpers;

use Tests\PHPUnit\Base\UnitTestCase;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\HTML;

class HTMLTest extends UnitTestCase
{
    /**
     * Test the basic Getters and Setters
     * @return void
     */
    public function testBasicGetterSetters(): void
    {
        // Popups.
        $this->assertFalse(HTML::isPopup());
        HTML::setIsPopup();
        $this->assertTrue(HTML::isPopup());

        // Nav.
        $nav = HTML::getNavCurrent();
        $this->assertIsString($nav);
        $this->assertEmpty($nav);
        HTML::setNavCurrent('test');
        $this->assertEquals('test', HTML::getNavCurrent());

        // Meta Descrips.
        // Initial, default.
        $this->assertEquals('DeBear.uk', HTML::getMetaDescription());
        // Explicitly set.
        $explicit = 'Test Meta';
        HTML::setMetaDescription($explicit);
        $this->assertEquals($explicit, HTML::getMetaDescription());
        // Via nav link.
        HTML::linkMetaDescription('privacy');
        $this->assertEquals('The Privacy Policy for DeBear.uk', HTML::getMetaDescription());

        // Analytics.
        FrameworkConfig::set([
            'debear.analytics.enabled.v3' => true,
            'debear.analytics.urchins.tracking' => ['UA-48789782-X'],
        ]);
        HTML::setupAnalytics();
        $this->assertEquals(['UA-48789782-9'], HTML::getAnalyticsUrchin());

        // Misc.
        $this->assertEquals('Welcome, Guest', HTML::getHeaderIntro());
        $this->assertTrue(HTML::firstCookieWarning());
    }

    /**
     * Test the formatting of breadcrumbs
     * @return void
     */
    public function testBreadcrumbs(): void
    {
        $out = HTML::formatBreadcrumbs(['a', 'b', 'cd', 'e-f-g', 'h_i_j', 'k-l_mN', 'o~p', '-']);
        $this->assertIsArray($out);
        $this->assertCount(8, $out);
        $this->assertEquals(['A', 'B', 'Cd', 'E F G', 'H I J', 'K L MN', 'O~p', ' '], $out);
    }

    /**
     * Test page title basic Getters and Setters
     * @return void
     */
    public function testPageTitle(): void
    {
        // Passing as a scalar, and then an array.
        HTML::setPageTitle('Title 1');
        HTML::setPageTitle(['Title 2', 'Title 3']);

        // Test the raw getter.
        $ret = 'DeBear.uk &raquo; Title 1 &raquo; Title 2 &raquo; Title 3';
        $this->assertEquals($ret, HTML::getPageTitleText());
        $this->assertEquals($ret, HTML::getPageTitleText()); // Cached, so redux...

        // Test the markup getter.
        $ret = '<li>DeBear.uk</li><li>Title 1</li><li>Title 2</li><li>Title 3</li>';
        $this->assertEquals($ret, HTML::getPageTitle());
        $this->assertEquals($ret, HTML::getPageTitle()); // Cached, so redux...
    }

    /**
     * Test the social / meta options
     * @return void
     */
    public function testSocialMeta(): void
    {
        // Social Meta.
        $this->assertTrue(HTML::showSocialMeta());

        // Twitter card.
        $card = 'test';
        HTML::setMetaTwitterCard($card);
        $this->assertEquals($card, HTML::getMetaTwitterCard());

        // Open Graph type.
        $type = 'test';
        HTML::setMetaOpenGraphType($type);
        $this->assertEquals($type, HTML::getMetaOpenGraphType());

        // Meta Image.
        $img = 'test.png';
        HTML::setMetaImage($img);
        $this->assertEquals($img, HTML::getMetaImage());

        // Twitter Image.
        $img_t = 'twitter.png';
        HTML::setMetaImageTwitter($img_t);
        $root = 'https://static.debear.test/www/images/';
        $this->assertEquals("$root$img_t", HTML::getMetaImageTwitter());

        // Open Graph Image.
        $img_o = 'og.png';
        HTML::setMetaImageOpenGraph($img_o);
        $this->assertEquals("$root$img_o", HTML::getMetaImageOpenGraph());

        // Open Graph Image sizes.
        $width = 1024;
        $height = 768;
        HTML::setMetaImageOpenGraphSize($width, $height);
        $this->assertEquals($width, HTML::getMetaImageOpenGraphWidth());
        $this->assertEquals($height, HTML::getMetaImageOpenGraphHeight());

        // Theme colour.
        $colour = '#000000';
        HTML::setMetaThemeColour($colour);
        $this->assertEquals($colour, HTML::getMetaThemeColour());
    }

    /**
     * Test the page link builders and getters
     * @return void
     */
    public function testPageLinks(): void
    {
        // Header bar links.
        $head = HTML::getHeaderLinks();
        $this->assertIsArray($head);
        $this->assertNotEmpty($head);
        $this->assertEquals([true], array_unique(array_column($head, 'enabled')));
        $this->assertEquals([true], array_unique(array_values(array_map(function ($l) {
            return $l['header']['enabled'];
        }, $head))));
        // Cached, so re-run (expecting the same output).
        $this->assertEquals($head, HTML::getHeaderLinks());

        // Footer bar links.
        $foot = HTML::getFooterLinks();
        $this->assertIsArray($foot);
        $this->assertNotEmpty($foot);
        $this->assertEquals([true], array_unique(array_column($foot, 'enabled')));
        $this->assertEquals([true], array_unique(array_values(array_map(function ($l) {
            return $l['footer']['enabled'];
        }, $foot))));
        // Cached, so re-run (expecting the same output).
        $this->assertEquals($foot, HTML::getFooterLinks());

        // Footer size.
        $this->assertEquals(0, HTML::getFooterSize());
        $blocks = HTML::getFooterBlocks();
        $this->assertIsArray($blocks);
        $this->assertEmpty($blocks);

        // Logo link.
        // First, no link.
        HTML::noHeaderLink();
        $this->assertFalse(HTML::getHeaderLink());
        // An explicit link.
        HTML::setHeaderLink('/page');
        $this->assertEquals('/page', HTML::getHeaderLink());
        // The main home.
        HTML::setHeaderLink('');
        $this->assertEquals('/', HTML::getHeaderLink());
        // A section home.
        $orig = [
            'debear.section.endpoint' => FrameworkConfig::get('debear.section.endpoint'),
            'debear.section.code' => FrameworkConfig::get('debear.section.code'),
        ];
        FrameworkConfig::set(['debear.section.endpoint' => 'testendpoint', 'debear.section.code' => 'test']);
        $this->assertEquals('/testendpoint', HTML::getHeaderLink());
        FrameworkConfig::set($orig);
        // Then, a section home that's in an excluded list.
        $orig = ['debear.content.header.no_logo_link' => FrameworkConfig::get('debear.content.header.no_logo_link')];
        FrameworkConfig::set(['debear.content.header.no_logo_link' => ['test']]);
        $this->assertEquals('/', HTML::getHeaderLink());
        FrameworkConfig::set($orig);
    }

    /**
     * Test the CSS manipulation
     * @return void
     */
    public function testCSS(): void
    {
        // Test initial, empty list.
        $out = HTML::getBodyClass();
        $this->assertIsString($out);
        $this->assertEmpty($out);

        // Adding as a scalar, then an array.
        HTML::addBodyClass('class-1 class-2');
        HTML::addBodyClass(['class-3', 'class-4']);

        // Getter, without section code.
        $classes = 'class-1 class-2 class-3 class-4';
        $this->assertEquals('class="' . $classes . '"', HTML::getBodyClass());
        // Getter, with section code (temporarily set and then reset).
        $orig = FrameworkConfig::get('debear.section.code');
        FrameworkConfig::set(['debear.section.code' => 'test']);
        $this->assertEquals('class="section-test ' . $classes . '"', HTML::getBodyClass());
        FrameworkConfig::set(['debear.section.code' => $orig]);
    }

    /**
     * Test the basic Getters and Setters
     * @return void
     */
    public function testEmails(): void
    {
        // Raw.
        $this->assertEquals('support@debear.test', HTML::buildEmailRaw('support'));
        $this->assertEquals('noreply@debear.test', HTML::buildEmailRaw('unknown'));

        // HTML - config address, with Text.
        $out = '<a id="pem-%x" data-emnom="support" data-emdom="debear.test">Email us!</a>';
        $this->assertStringMatchesFormat($out, HTML::buildEmailMarkup('support', 'Email us!'));

        // HTML - other address, with Text.
        $out = '<a id="pem-%x" data-emnom="explicit" data-emdom="debear.other">Email us!</a>';
        $this->assertStringMatchesFormat($out, HTML::buildEmailMarkup('explicit@debear.other', 'Email us!'));

        // HTML - default, with Text.
        $out = '<a id="pem-%x" data-emnom="noreply" data-emdom="debear.test">Email us!</a>';
        $this->assertStringMatchesFormat($out, HTML::buildEmailMarkup('unknown', 'Email us!'));

        // HTML - no Text.
        $out = '<a id="pem-%x" data-emnom="support" data-emdom="debear.test">'
            . '<emnom>support</emnom><emdom>debear.test</emdom>'
            . '</a>';
        $this->assertStringMatchesFormat($out, HTML::buildEmailMarkup('support'));
    }
}
