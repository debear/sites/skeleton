<?php

namespace Tests\PHPUnit\Unit\Helpers;

use Tests\PHPUnit\Base\UnitTestCase;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\Config;
use DeBear\Exceptions\ConfigException;

class ConfigTest extends UnitTestCase
{
    /**
     * Test the standard post-processing
     * @return void
     */
    public function testPostProc(): void
    {
        $orig = FrameworkConfig::get('debear');

        // Customise, to check it processed correctly.
        $raw = $orig;
        $raw['subdomains']['missing'] = 'missing';
        $raw['content']['header']['no_logo_link'] = ['merges'];
        $proc = Config::postProcess($raw);
        $this->assertStringEndsWith('merged', $proc['content']['header']['no_logo_link'][0]);

        // Reset.
        FrameworkConfig::set(['debear' => $orig]);
    }

    /**
     * Test the dynamic processing
     * @return void
     */
    public function testDynamic(): void
    {
        $orig = FrameworkConfig::get('debear');
        $_SERVER_ORIG = $_SERVER;
        $_GET_ORIG = $_GET;
        $_SERVER['PHP_SELF'] = 'artisan';

        // Run - as config:cache.
        $_SERVER['argv'] = ['artisan', 'config:cache'];
        $raw = ['debear' => $orig];
        $proc = Config::dynamicallyProcessCache($raw);
        $this->assertEquals($raw, $proc);

        // Run - as other artisan config.
        $_SERVER['argv'] = $_SERVER_ORIG['argv'];
        $proc = Config::dynamicallyProcessCache($raw);
        $this->assertNotEquals(
            $raw['debear']['headers']['csp']['nonce'],
            $proc['debear']['headers']['csp']['nonce']
        );

        // Test non-standard inputs.
        // Some "before"s...
        $this->assertFalse($proc['debear']['resources']['css_merge']);
        $this->assertFalse($proc['debear']['resources']['js_merge']);
        // Process.
        $_SERVER['SERVER_PORT'] = 123;
        $_SERVER['REQUEST_URI'] = '/test';
        $_GET['merge_jscss'] = true;
        $proc = Config::dynamicallyProcessCache($raw);
        $this->assertEquals($proc['debear']['url']['port'], ':123');
        $this->assertTrue($proc['debear']['resources']['css_merge']);
        $this->assertTrue($proc['debear']['resources']['js_merge']);
        $this->assertTrue($proc['debear']['links']['home']['footer']['enabled']);

        // Test some sub-domain determinations (as artisan).
        unset($_SERVER['HTTP_HOST']);
        $raw['force-subdomain'] = true;
        $proc = Config::dynamicallyProcessCache($raw);
        $this->assertEquals('www', $proc['debear']['url']['sub']);

        // Test some sub-domain determinations (unknown, raises exception).
        unset($_SERVER['PHP_SELF']);
        $_SERVER['HTTP_HOST'] = 'debear.test';
        $proc = Config::dynamicallyProcessCache($raw);
        $this->assertEquals('www', $proc['debear']['url']['sub']);

        // Reset (as the exception in the next test must be the final command).
        $_GET = $_GET_ORIG;
        $_SERVER = $_SERVER_ORIG;
        FrameworkConfig::set(['debear' => $orig]);

        // Test some sub-domain determinations (unknown, raises exception).
        unset($_SERVER['HTTP_HOST']);
        $this->expectException(ConfigException::class);
        $proc = Config::dynamicallyProcessCache($raw);
    }

    /**
     * Test loading sub-site config
     * @return void
     */
    public function testLoadSite(): void
    {
        $orig = FrameworkConfig::get('debear');
        $_SERVER_ORIG = $_SERVER;

        // Create a faux sub-site and ensure its config is loaded.
        $_SERVER['REQUEST_URI'] = '/sub-test/page';
        FrameworkConfig::set(['debear.subsites.sub-test' => ['enabled' => true, 'set' => true]]);
        Config::loadSite();
        $this->assertTrue(FrameworkConfig::get('debear.subsites.sub-test.set'));

        // Reset.
        $_SERVER = $_SERVER_ORIG;
        FrameworkConfig::set(['debear' => $orig]);
    }
}
