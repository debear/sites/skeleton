<?php

namespace Tests\PHPUnit\Unit\Helpers;

use Tests\PHPUnit\Base\UnitTestCase;
use DeBear\Helpers\Browser;

class BrowscapTest extends UnitTestCase
{
    /**
     * Chrome (Linux, macOS, Windows, Android, iOS)
     * @return void
     */
    public function testChrome(): void
    {
        // Browser: Linux.
        $this->assertWorker(
            'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36',
            [
                'browser' => 'Chrome',
                'version' => '71.0',
                'platform' => 'Linux',
            ]
        );
        // Browser: macOS.
        $this->assertWorker(
            'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) '
                . 'Chrome/71.0.3578.98 Safari/537.36',
            [
                'browser' => 'Chrome',
                'version' => '71.0',
                'platform' => 'macOS',
            ]
        );
        // Browser: Windows.
        $this->assertWorker(
            'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                . 'Chrome/64.0.3282.119 Safari/537.36',
            [
                'browser' => 'Chrome',
                'version' => '64.0',
                'platform' => 'Win10',
            ]
        );
        // Browser: Android.
        $this->assertWorker(
            'Mozilla/5.0 (Linux; Android 8.0.0; SM-G955F) AppleWebKit/537.36 (KHTML, like Gecko) '
                . 'Chrome/71.0.3578.99 Mobile Safari/537.36',
            [
                'browser' => 'Chrome',
                'version' => '71.0',
                'platform' => 'Android',
            ]
        );
        // Browser: iOS.
        $this->assertWorker(
            'Mozilla/5.0 (iPhone; CPU iPhone OS 10_3 like Mac OS X) AppleWebKit/602.1.50 (KHTML, like Gecko) '
                . 'CriOS/56.0.2924.75 Mobile/14E5239e Safari/602.1',
            [
                'browser' => 'Chrome',
                'version' => '56.0',
                'platform' => 'iOS',
            ]
        );
    }

    /**
     * IE (Windows)
     * @return void
     */
    public function testIE(): void
    {
        $this->assertWorker(
            'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/6.0)',
            [
                'browser' => 'IE',
                'version' => '10.0',
                'platform' => 'Win7',
            ]
        );
    }

    /**
     * Safari (macOS, iOS)
     * @return void
     */
    public function testSafari(): void
    {
        // Browser: macOS.
        $this->assertWorker(
            'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_3) AppleWebKit/604.5.6 (KHTML, like Gecko) '
                . 'Version/11.0.3 Safari/604.5.6',
            [
                'browser' => 'Safari',
                'version' => '11.0',
                'platform' => 'macOS',
            ]
        );
        // Browser: iOS.
        $this->assertWorker(
            'Mozilla/5.0 (iPhone; CPU iPhone OS 10_3 like Mac OS X) AppleWebKit/603.1.23 (KHTML, like Gecko) '
                . 'Version/10.0 Mobile/14E5239e Safari/602.1',
            [
                'browser' => 'Safari',
                'version' => '10.0',
                'platform' => 'iOS',
            ]
        );
    }

    /**
     * Firefox (Linux. macOS, Windows)
     * @return void
     */
    public function testFirefox(): void
    {
        // Linux.
        $this->assertWorker(
            'Mozilla/5.0 (X11; zbuntu; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0',
            [
                'browser' => 'Firefox',
                'version' => '58.0',
                'platform' => 'Linux',
            ]
        );
        // Browser: macOS.
        $this->assertWorker(
            'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:58.0) Gecko/20100101 Firefox/58.0',
            [
                'browser' => 'Firefox',
                'version' => '58.0',
                'platform' => 'macOS',
            ]
        );
        // Windows.
        $this->assertWorker(
            'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0',
            [
                'browser' => 'Firefox',
                'version' => '57.0',
                'platform' => 'Win10',
            ]
        );
    }

    /**
     * Googlebot
     * @return void
     */
    public function testGooglebot(): void
    {
        $this->assertWorker(
            'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)',
            [
                'browser' => 'Google Bot',
                'version' => '2.1',
                'platform' => 'unknown',
            ]
        );
        $this->assertWorker(
            'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; '
                . 'Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36',
            [
                'browser' => 'Google Bot',
                'version' => '2.1',
                'platform' => 'unknown',
            ]
        );
    }

    /**
     * Google Verification
     * @return void
     */
    public function testGoogleVerify(): void
    {
        $this->assertWorker(
            'Mozilla/5.0 (compatible; Google-Site-Verification/1.0)',
            [
                'browser' => 'Google-Site-Verification',
                'version' => '1.0',
                'platform' => 'unknown',
            ]
        );
    }

    /**
     * Bingbot
     * @return void
     */
    public function testBingbot(): void
    {
        $this->assertWorker(
            'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)',
            [
                'browser' => 'BingBot',
                'version' => '2.0',
                'platform' => 'unknown',
            ]
        );
        $this->assertWorker(
            'Mozilla/5.0 (iPhone; CPU iPhone OS 7_0 like Mac OS X) AppleWebKit/537.51.1 (KHTML, like Gecko) '
                . 'Version/7.0 Mobile/11A465 Safari/9537.53 '
                . '(compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)',
            [
                'browser' => 'BingBot',
                'version' => '2.0',
                'platform' => 'iOS',
            ]
        );
    }

    /**
     * Worker method for processing an IP
     * @param string $ua      Test User Agent string.
     * @param array  $browser Expected browser info.
     * @return void
     */
    protected function assertWorker(string $ua, array $browser): void
    {
        // Run through Browscap.
        $_SERVER['HTTP_USER_AGENT'] = $ua;
        Browser::reset();
        $test = Browser::get();

        foreach ($browser as $col => $expected) {
            $this->assertEquals($expected, $test->$col);
        }
    }

    /**
     * Internal Hash representation of the browser
     * @return void
     */
    public function testInternalHash(): void
    {
        // Setup (preserving the "main" UA).
        $ua_orig = $_SERVER['HTTP_USER_AGENT'];
        $_SERVER['HTTP_USER_AGENT'] = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko)'
            . ' Chrome/72.0.3578.98 Safari/537.36';

        // Run once.
        Browser::resetHash();
        $hashA = Browser::internalHash();
        $exp = 'f1d581a210be7c20244377ed64772ec0';
        $this->assertIsString($hashA);
        $this->assertEquals($exp, $hashA);

        // And then a second time.
        $hashB = Browser::internalHash();
        $this->assertEquals($exp, $hashB);

        // Restore.
        $_SERVER['HTTP_USER_AGENT'] = $ua_orig;
    }
}
