<?php

namespace Tests\PHPUnit\Unit\Helpers;

use Tests\PHPUnit\Base\UnitTestCase;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\Sitemap;
use DeBear\Helpers\HTML;

class SitemapTest extends UnitTestCase
{
    /**
     * Test the building Sitemaps
     * @return void
     */
    public function testSitemap(): void
    {
        $build = Sitemap::buildSitemap();
        $this->assertIsArray($build);
        $this->assertNotEmpty($build);
        // Re-request, as we cached it when building.
        $fetch = Sitemap::buildSitemap();
        $this->assertEquals($build, $fetch);
    }

    /**
     * Test the building Navigation
     * @return void
     */
    public function testNavigation(): void
    {
        // Set the nav and tweak config.
        HTML::setNavCurrent('/test/');
        $orig_endpoint = FrameworkConfig::get('debear.section.endpoint');
        $orig_code = FrameworkConfig::get('debear.section.code');
        $orig_links = FrameworkConfig::get('debear.links');
        FrameworkConfig::set([
            // Force a test section.
            'debear.section.endpoint' => 'testendpoint',
            'debear.section.code' => 'test',
            // Add/Modify some links.
            'debear.links.home.links' => [
                'sub' => [
                    'url' => '/',
                    'title' => 'Our Homepage',
                    'label' => 'Home',
                    'order' => 10,
                ],
            ],
            'debear.links.extras' => [
                'url-section' => true,
                'url' => '/sub',
                'title' => 'Test Sub Page',
                'descrip' => 'Sub Page w/Extras',
                'url-extra' => '-page',
                'query' => [
                    'query=true',
                ],
                'order' => 50,
                'navigation' => [
                    'enabled' => true,
                ],
            ],
            'debear.links.disabled' => [
                'url' => 'disabled',
                'title' => 'Disabled Link',
                'descrip' => 'Disabled',
                'enabled' => false,
            ],
            'debear.links.disabled_nav' => [
                'url' => 'disabled_nav',
                'title' => 'Disabled Nav Link',
                'descrip' => 'Disabled Nav',
                'navigation' => false,
            ],
        ]);

        // Build.
        $build = Sitemap::buildNavigation();
        $this->assertIsArray($build);
        $this->assertNotEmpty($build);
        // Re-request, as we cached it when building.
        $fetch = Sitemap::buildNavigation();
        $this->assertEquals($build, $fetch);

        // Reset.
        FrameworkConfig::set([
            'debear.section.endpoint' => $orig_endpoint,
            'debear.section.code' => $orig_code,
            'debear.links' => $orig_links,
        ]);
    }

    /**
     * Test URL conversion
     * @return void
     */
    public function testConvert(): void
    {
        $this->assertEquals('https://debear.test/sub', Sitemap::convertURL('../sub'));
        $this->assertEquals('https://debear.test/sub', Sitemap::convertURL('/sub'));
        $this->assertEquals('https://debear.test/sub', Sitemap::convertURL('https://debear.test/sub'));
    }

    /**
     * Test the finding of elements
     * @return void
     */
    public function testFinding(): void
    {
        // Find within the Sitemap.
        $ele = Sitemap::findSitemapElement('home');
        $this->assertIsArray($ele);
        $this->assertEquals('/', $ele['url']);

        // Find within the Navigation.
        $ele = Sitemap::findNavigationElement('home');
        $this->assertIsArray($ele);
        $this->assertEquals('/', $ele['url']);

        // Find within the Navigation children.
        $ele = Sitemap::findNavigationElement('sitemap');
        $this->assertIsArray($ele);
        $this->assertEquals('/sitemap', $ele['url']);

        // Attempt to find a non-existing option.
        $ele = Sitemap::findNavigationElement('missing');
        $this->assertIsArray($ele);
        $this->assertEmpty($ele);
    }

    /**
     * Test parsing the sitemap into subsites
     * @return void
     */
    public function testParseSubsites(): void
    {
        // Build our regular subsite object.
        $sitemap = Sitemap::buildSitemap();
        $this->assertIsArray($sitemap);
        $this->assertCount(2, $sitemap);
        $this->assertArrayHasKey('home', $sitemap);
        $this->assertArrayNotHasKey('subsites', $sitemap);

        // Now parse it.
        $parsed = Sitemap::parseSubsites($sitemap);
        $this->assertIsArray($parsed);
        $this->assertCount(2, $parsed);
        // Key 'subsites' is set, but empty.
        $this->assertArrayHasKey('subsites', $parsed);
        $this->assertEmpty($parsed['subsites']);
        // Key 'admin' is set, with three links (sitemap, terms, privacy).
        $this->assertArrayHasKey('admin', $parsed);
        $this->assertCount(3, $parsed['admin']);
        $this->assertArrayHasKey('sitemap', $parsed['admin']);
        $this->assertArrayHasKey('terms', $parsed['admin']);
        $this->assertArrayHasKey('privacy', $parsed['admin']);
        // Key 'home' not copied across.
        $this->assertArrayNotHasKey('home', $parsed);
    }
}
