<?php

namespace Tests\PHPUnit\Unit\Helpers;

use Tests\PHPUnit\Base\UnitTestCase;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\Interpolate;

class InterpolateTest extends UnitTestCase
{
    /**
     * Test the User interpolation of a string
     * @return void
     */
    public function testStringUser(): void
    {
        $this->assertEquals(', ', Interpolate::string('{user|first_name}, {user|first_name}'));
    }

    /**
     * Test the Config interpolation of a string
     * @return void
     */
    public function testStringConfig(): void
    {
        $key = 'debear.names.site';
        $value = FrameworkConfig::get($key);
        $this->assertEquals("$value, $value", Interpolate::string("{config|$key}, {config|$key}"));
    }

    /**
     * Test the Session data interpolation of a string
     * @return void
     */
    public function testStringSession(): void
    {
        $key = 'ci.num';
        $str = "{session|$key}";
        $out = '123';
        session([$key => $out]);
        $this->assertEquals("$out, $out", Interpolate::string("$str, $str"));
        session([$key => null]);
    }

    /**
     * Test the Link interpolation of a string
     * @return void
     */
    public function testStringLink(): void
    {
        // HTML - with Text.
        $str = '{link|/link|Link}';
        $out = '<a href="/link">Link</a>';
        $this->assertEquals("$out, $out", Interpolate::string("$str, $str"));

        // HTML - no Text.
        $str = '{link|/link}';
        $out = '<a href="/link">/link</a>';
        $this->assertEquals("$out, $out", Interpolate::string("$str, $str"));

        // JavaScript - with Text.
        $str = '{link-js|link-click|Link}';
        $out = '<a class="link-click">Link</a>';
        $this->assertEquals("$out, $out", Interpolate::string("$str, $str"));

        // JavaScript - no Text.
        $str = '{link-js|link-click}';
        $out = '<a class="link-click">link-click</a>';
        $this->assertEquals("$out, $out", Interpolate::string("$str, $str"));
    }

    /**
     * Test the Email interpolation of a string
     * @return void
     */
    public function testStringEmail(): void
    {
        // HTML - with Text.
        $str = '{email|support|Support Team}';
        $out = '<a id="pem-%x" data-emnom="support" data-emdom="debear.test">Support Team</a>';
        $this->assertStringMatchesFormat("$out, $out", Interpolate::string("$str, $str"));

        // HTML - no Text.
        $str = '{email|support}';
        $out = '<a id="pem-%x" data-emnom="support" data-emdom="debear.test">support</a>';
        $this->assertStringMatchesFormat("$out, $out", Interpolate::string("$str, $str"));

        // Raw - with Text (which is technically HTML!).
        $str = '{email-raw|support|Support Team}';
        $out = '<a id="pem-%x" data-emnom="support" data-emdom="debear.test">Support Team</a>';
        $this->assertStringMatchesFormat("$out, $out", Interpolate::string("$str, $str"));

        // Raw - no Text.
        $str = '{email-raw|support}';
        $out = 'support@debear.test';
        $this->assertEquals("$out, $out", Interpolate::string("$str, $str"));
    }

    /**
     * Test the Data interpolation of a string
     * @return void
     */
    public function testStringData(): void
    {
        $this->assertEquals('A, A', Interpolate::string('{data|a}, {data|a}', [
            'a' => 'A',
        ]));
        $this->assertEquals('** Missing Interpolation Data \'b\'**', Interpolate::string('{data|b}', [
            'a' => 'A',
        ]));
    }

    /**
     * Test the If interpolation of a string
     * @return void
     */
    public function testStringIf(): void
    {
        // Config-based.
        FrameworkConfig::set(['debear.tests.interp' => [
            'true' => true,
            'false' => false,
        ]]);
        $this->assertEquals('true', Interpolate::string('{if|config:debear.tests.interp.true|true|false}'));
        $this->assertEquals('false', Interpolate::string('{if|config:debear.tests.interp.false|true|false}'));

        // Data-based.
        $this->assertEquals('true', Interpolate::string('{if|data:a|true|false}', [
            'a' => true,
            'b' => false,
        ]));
        $this->assertEquals('false', Interpolate::string('{if|data:b|true|false}', [
            'a' => true,
            'b' => false,
        ]));
    }

    /**
     * Test the interpolation of a basic array
     * @return void
     */
    public function testArray(): void
    {
        $output = Interpolate::array([
            'a' => '{link|/link|Link}',
            'b' => [
                'c' => '{link|/link|Link}',
            ],
        ]);
        $this->assertEquals('<a href="/link">Link</a>', $output['a']);
        $this->assertEquals('<a href="/link">Link</a>', $output['b']['c']);
    }
}
