<?php

namespace Tests\PHPUnit\Unit\Helpers;

use Tests\PHPUnit\Base\UnitTestCase;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use DeBear\Helpers\API;

class APITest extends UnitTestCase
{
    /**
     * Test the HTTP status code wrappers
     * @return void
     */
    public function testStatusWrappers(): void
    {
        // Generic setter.
        $status = API::setStatus(200, 'Test message');
        $this->assertInstanceOf(JsonResponse::class, $status);
        $this->assertEquals('{"msg":"Test message"}', $status->content());
        $this->assertEquals(200, $status->status());

        // 200: OK / Processed.
        $resp = API::sendResponse();
        $this->assertInstanceOf(JsonResponse::class, $resp);
        $this->assertEquals(200, $resp->status());
        $this->assertEquals('{"msg":"Resource Processed"}', $resp->content());

        // 201: Created.
        $resp = API::sendCreated();
        $this->assertInstanceOf(JsonResponse::class, $resp);
        $this->assertEquals(201, $resp->status());
        $this->assertEquals('{"msg":"Resource Created"}', $resp->content());

        // 204: No Content.
        $resp = API::sendNoContent();
        $this->assertInstanceOf(Response::class, $resp);
        $this->assertEmpty($resp->content());
        $this->assertEquals(204, $resp->status());

        // 400: Bad Request.
        $resp = API::sendBadRequest();
        $this->assertInstanceOf(JsonResponse::class, $resp);
        $this->assertEquals(400, $resp->status());
        $this->assertEquals('{"msg":"Bad Request"}', $resp->content());

        // 401: Unauthorised.
        $resp = API::sendUnauthorised();
        $this->assertInstanceOf(JsonResponse::class, $resp);
        $this->assertEquals(401, $resp->status());
        $this->assertEquals('{"msg":"User Not Signed In"}', $resp->content());

        // 403: Forbidden.
        $resp = API::sendForbidden();
        $this->assertInstanceOf(JsonResponse::class, $resp);
        $this->assertEquals(403, $resp->status());
        $this->assertEquals('{"msg":"Insufficient Privileges"}', $resp->content());

        // 404: Not Found.
        $resp = API::sendNotFound();
        $this->assertInstanceOf(JsonResponse::class, $resp);
        $this->assertEquals(404, $resp->status());
        $this->assertEquals('{"msg":"Resource Not Found"}', $resp->content());

        // 405: Method Not Allowed.
        $resp = API::sendMethodNotAllowed();
        $this->assertInstanceOf(JsonResponse::class, $resp);
        $this->assertEquals(405, $resp->status());
        $this->assertEquals('{"msg":"Request Method Not Allowed"}', $resp->content());

        // 409: Conflict.
        $resp = API::sendConflict();
        $this->assertInstanceOf(JsonResponse::class, $resp);
        $this->assertEquals(409, $resp->status());
        $this->assertEquals('{"msg":"Conflict"}', $resp->content());
    }

    /**
     * Test the HTTP validation wrappers
     * @return void
     */
    public function testValidation(): void
    {
        $resp = API::sendValidationFailure(['a' => ['message' => 'b']]);
        $this->assertInstanceOf(JsonResponse::class, $resp);
        $this->assertEquals(400, $resp->status());
        $this->assertEquals('{"msg":"The input data did not pass the required validation rules",'
            . '"fields":{"a":"b"}}', $resp->content());
    }
}
