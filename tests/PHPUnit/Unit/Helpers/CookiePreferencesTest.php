<?php

namespace Tests\PHPUnit\Unit\Helpers;

use Tests\PHPUnit\Base\UnitTestCase;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\CookiePreferences;
use DeBear\Helpers\Cookie;
use DeBear\Exceptions\Cookies\PrefsException;

class CookiePreferencesTest extends UnitTestCase
{
    /**
     * Test interactions around cookie preferences
     * @return void
     */
    public function testGet(): void
    {
        // Get an unknown option, expect a failure so set it and test again.
        $this->expectException(PrefsException::class);
        $ci_key = null;
        // Xdebug and phpcs disagree over whether we should have whitespace around the finally.
        // phpcs:disable Squiz.ControlStructures.ControlSignature
        try {
            $ci_key = CookiePreferences::get('ci-key');
        }
        finally
        {
            $this->assertNull($ci_key);
            // Continue in a finally block so the tests continue.
            CookiePreferences::set('ci-key', true);
            // Test again, this time where the value has been set.
            $this->assertTrue(CookiePreferences::get('ci-key'));
        }
        // phpcs:enable Squiz.ControlStructures.ControlSignature
    }

    /**
     * Test loading invalid cookie preferences
     * @return void
     */
    public function testLoadInvalid(): void
    {
        // Generate our broken preference, and store to be loaded.
        $invalid_pref = substr(json_encode([
            'ci-invalid-key' => true,
        ]), 0, -3);
        Cookie::setValue(FrameworkConfig::get('debear.sessions.cookies.prefs'), $invalid_pref);
        CookiePreferences::load();
        // Load, and then check that our key doesn't exist (we'll get an exception).
        $this->expectException(PrefsException::class);
        $invalid_key = null;
        // Xdebug and phpcs disagree over whether we should have whitespace around the finally.
        // phpcs:disable Squiz.ControlStructures.ControlSignature
        try {
            // Our key shouldn't exist.
            $invalid_key = CookiePreferences::get('ci-invalid-key');
        }
        finally
        {
            $this->assertNull($invalid_key);
            // But a default key does.
            $this->assertTrue(CookiePreferences::get('allow-analytics'));
        }
        // phpcs:enable Squiz.ControlStructures.ControlSignature
    }

    /**
     * Test the default preferences
     * @return void
     */
    public function testDefaultPrefs(): void
    {
        $pref = CookiePreferences::defaults();
        $this->assertIsArray($pref);
        $this->assertNotEmpty($pref);
    }
}
