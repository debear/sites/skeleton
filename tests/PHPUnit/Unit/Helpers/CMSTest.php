<?php

namespace Tests\PHPUnit\Unit\Helpers;

use Tests\PHPUnit\Base\UnitTestCase;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\CMS;
use DeBear\Exceptions\CMSException;

class CMSTest extends UnitTestCase
{
    /**
     * Test pulling out content
     * @return void
     */
    public function testContent(): void
    {
        // Something with an inactive row.
        $title = CMS::getSection('/test', 'title');
        $this->assertEquals('<h1>Test Page Title</h1>', $title);
        $this->assertStringNotContainsString('Inactive Title', $title);

        // Multiple rows.
        $intro = CMS::getSection('/test', 'intro');
        $this->assertEquals(2, substr_count($intro, '<p>'));
        $this->assertStringStartsWith('<p>This is a test page', $intro);
        $this->assertStringEndsWith('any templates.</p>', $intro);

        // Content with interpolation.
        $name = 'CMS Unit Test #' . rand(10, 99);
        $interp = CMS::getSection('/cookies', 'intro', ['site' => '_skel', 'interpolate' => compact('name')]);
        $this->assertStringContainsString($name, $interp);

        // A section that doesn't exist, with an exception thrown.
        $caught = false;
        $orig = FrameworkConfig::get('debear.cms.missing-exceptions.section');
        FrameworkConfig::set(['debear.cms.missing-exceptions.section' => true]);
        try {
            $missing = CMS::getSection('/test', 'footer');
        } catch (CMSException $e) {
            $caught = true;
        }
        $this->assertTrue($caught);
        FrameworkConfig::set(['debear.cms.missing-exceptions.section' => $orig]);

        // A section that doesn't exist, with empty content returned.
        $orig = FrameworkConfig::get('debear.cms.missing-exceptions.section');
        FrameworkConfig::set(['debear.cms.missing-exceptions.section' => false]);
        $missing = CMS::getSection('/test', 'footer');
        $this->assertEmpty($missing);
        FrameworkConfig::set(['debear.cms.missing-exceptions.section' => $orig]);

        // A page that doesn't exist.
        $caught = false;
        try {
            $missing = CMS::getSection('/missing', 'section');
        } catch (CMSException $e) {
            $caught = true;
        }
        $this->assertTrue($caught);
    }
}
