<?php

namespace Tests\PHPUnit\Unit\Helpers;

use Tests\PHPUnit\Base\UnitTestCase;
use DeBear\Helpers\User as UserHelper;
use DeBear\Models\Skeleton\User as UserModel;

class UserTest extends UnitTestCase
{
    /**
     * Test the logic for enforcing the password policy
     * @return void
     */
    public function testPasswordPolicy(): void
    {
        // Log in as our policy user.
        UserModel::doLogin(UserModel::find(35));
        $data = ['user_id' => 'test_policy', 'email' => 'test@debear.test'];
        $err_compromised = 'This password is known to be compromised so is not deemed secure. Please enter another';

        // 1. Password length.
        $out = UserHelper::validatePasswordPolicy('short', $data);
        $this->assertIsString($out);
        $this->assertEquals('Your password must be 8 or more characters in length', $out);
        // 2. Existing user has not changed password (when checking against existing).
        $out = UserHelper::validatePasswordPolicy('basic123', $data, ['check-reuse' => true]);
        $this->assertIsString($out);
        $this->assertEquals('You cannot re-use your existing password', $out);
        // 3. Includes user's ID.
        $out = UserHelper::validatePasswordPolicy('atest_policyb', $data);
        $this->assertIsString($out);
        $this->assertEquals('The password cannot include your username', $out);
        // 4. Includes user's email.
        $out = UserHelper::validatePasswordPolicy('atest@debear.testb', $data);
        $this->assertIsString($out);
        $this->assertEquals('The password cannot include your email address', $out);
        // 5a. Includes 'password'.
        $out = UserHelper::validatePasswordPolicy('mypassword', $data);
        $this->assertIsString($out);
        $this->assertEquals($err_compromised, $out);
        // 5b. Known part of a breach.
        $out = UserHelper::validatePasswordPolicy('basic123', $data);
        $this->assertIsString($out);
        $this->assertEquals($err_compromised, $out);

        // Valid!
        $out = UserHelper::validatePasswordPolicy('testlogindetails', $data);
        $this->assertIsBool($out);
        $this->assertFalse($out);

        // Revert the logged in user.
        UserModel::doLogout();
    }
}
