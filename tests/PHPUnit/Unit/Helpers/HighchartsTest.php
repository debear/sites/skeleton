<?php

namespace Tests\PHPUnit\Unit\Helpers;

use Tests\PHPUnit\Base\UnitTestCase;
use DeBear\Helpers\Highcharts;
use DeBear\Helpers\Resources;

class HighchartsTest extends UnitTestCase
{
    /**
     * Test basic chart creation
     * @return void
     */
    public function testBasic(): void
    {
        // Create the object.
        $chart = Highcharts::new('ele', [
            'title' => 'Test Chart',
            'annotations' => true,
            'js-module' => 'test',
        ]);
        $this->assertEquals('ele', $chart['chart']['renderTo']);
        $this->assertEquals('Test Chart', $chart['title']['text']);
        $this->assertTrue($chart['chart']['styledMode']);
        $this->assertFalse($chart['credits']['enabled']);

        // Check the resources were loaded correctly.
        $this->assertContains('//static.debear.test/www/css/highcharts/highcharts.css', Resources::getCSS());
        $this->assertContains('//static.debear.test/www/js/highcharts/highcharts.src.js', Resources::getJS());
        $this->assertContains('//static.debear.test/www/js/highcharts/annotations.src.js', Resources::getJS());
        $this->assertContains('//static.debear.test/www/js/highcharts/test.src.js', Resources::getJS());
    }

    /**
     * Test chart creation without styling enabled
     * @return void
     */
    public function testUnstyled(): void
    {
        // Create the object without styling.
        $chart = Highcharts::new('ele', [
            'styled-mode' => false,
        ]);
        $this->assertFalse($chart['chart']['styledMode']);
        $this->assertNotContains('//static.debear.test/www/css/highcharts/highcharts.css', Resources::getCSS());
    }

    /**
     * Test decoding the chart object
     * @return void
     */
    public function testDecoding(): void
    {
        $chart = Highcharts::new('ele');
        $chart['tooltip'] = [
            'formatter' => 'function() { return \'<strong>\' + this.x + \'</strong>\'; }',
        ];
        $dec = Highcharts::decode($chart);
        $this->assertNotEmpty($dec);
        $this->assertEquals(
            '{"chart":{"styledMode":true,"renderTo":"ele"},'
            . '"title":{"text":""},'
            . '"credits":{"enabled":false},'
            . '"tooltip":{"formatter":function() { return \'<strong>\' + this.x + \'</strong>\'; }}}',
            $dec
        );
    }

    /**
     * Test standardising the axis maxes
     * @return void
     */
    public function testRangeCalcs(): void
    {
        $pri = 123;
        $sec = 129;
        $div = Highcharts::calculateAxisRange($pri, $sec);
        $this->assertEquals(124, $pri);
        $this->assertEquals(186, $sec);
        $this->assertEquals(62, $div);
    }
}
