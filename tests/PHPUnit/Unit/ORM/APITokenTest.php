<?php

namespace Tests\PHPUnit\Unit\ORM;

use Tests\PHPUnit\Base\UnitTestCase;
use DeBear\Models\Skeleton\User as UserModel;
use DeBear\ORM\Skeleton\APIToken;

class APITokenTest extends UnitTestCase
{
    /**
     * Test accessing info about the user from an API token
     * @return void
     */
    public function testUserManagement(): void
    {
        $token = APIToken::query()->where('token', 'test-valid')->get();
        $this->assertEquals('2', $token->token_id);
        $this->assertEquals('Valid Token', $token->name);

        $token->setupInternalObject();
        $ret_obj = APIToken::object();
        $this->assertEquals(spl_object_hash($ret_obj), spl_object_hash($token));
        $this->assertEquals('test_user', UserModel::object()->user_id);
    }
}
