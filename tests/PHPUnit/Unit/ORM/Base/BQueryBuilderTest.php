<?php

namespace Tests\PHPUnit\Unit\ORM\Base;

use Tests\PHPUnit\Base\UnitTestCase;
use DeBear\ORM\Base\Helpers\QueryBuilder;
use DeBear\ORM\Skeleton\Timezone;

class BQueryBuilderTest extends UnitTestCase
{
    /**
     * Test building a query and then running it
     * @return void
     */
    public function testBuild(): void
    {
        // Start our query.
        $query = Timezone::query();
        $this->assertInstanceOf(QueryBuilder::class, $query);
        $this->assertEquals(Timezone::getTable(), $query->from);
        $this->assertEquals('SETUP_COUNTRY_TZ', $query->from);

        // Add some clauses.
        $query = $query->where('code2', 'us')->orderByDesc('population');
        $this->assertEquals(17, $query->count());
        // Then run and compare return values.
        $ret = $query->get();
        $this->assertEquals(17, $ret->count());
        $this->assertEquals('America/New_York', $ret->timezone);
    }

    /**
     * Test getting all the rows in a model
     * @return void
     */
    public function testGetAll(): void
    {
        $all = Timezone::all();
        $this->assertEquals(393, $all->count());
    }

    /**
     * Test the debug info returned
     * @return void
     */
    public function testDebug(): void
    {
        $query = Timezone::query()->where('code2', 'us');
        $debug = $query->debug();
        $this->assertIsArray($debug);
        $this->assertEquals(2, count($debug));
        $this->assertEquals(['sql', 'args'], array_keys($debug));
        $this->assertEquals('select * from `SETUP_COUNTRY_TZ` where `code2` = ?', $debug['sql']);
        $this->assertEquals(['us'], $debug['args']);
    }
}
