<?php

namespace Tests\PHPUnit\Unit\ORM\Base;

use Tests\PHPUnit\Base\UnitTestCase;
use DeBear\ORM\Skeleton\Country;
use DeBear\ORM\Skeleton\Timezone;
use DeBear\ORM\Skeleton\User;
use DeBear\ORM\Skeleton\UserPermissions;

class DSecondaryTest extends UnitTestCase
{
    /**
     * Test the secondary data defined via a default method
     * @return void
     */
    public function testDefault(): void
    {
        // Run our base query for some select users.
        $users = User::query()
            ->whereIn('user_id', ['test_disabled', 'sysmon_admin', 'test_user'])
            ->get()
            ->loadSecondary(['permissions']);
        $this->assertEquals(3, $users->count());
        // Test each user independently.
        // -test_disabled.
        $user_test = $users->where('user_id', 'test_disabled');
        $this->assertInstanceOf(User::class, $user_test);
        $this->assertInstanceOf(UserPermissions::class, $user_test->permissions);
        $this->assertEquals(0, $user_test->permissions->count());
        // -sysmon_admin.
        $user_sysmon = $users->where('user_id', 'sysmon_admin');
        $this->assertInstanceOf(User::class, $user_sysmon);
        $this->assertInstanceOf(UserPermissions::class, $user_sysmon->permissions);
        $this->assertEquals(2, $user_sysmon->permissions->count());
        // -test_user.
        $user_user = $users->where('user_id', 'test_user');
        $this->assertInstanceOf(User::class, $user_user);
        $this->assertInstanceOf(UserPermissions::class, $user_user->permissions);
        $this->assertEquals(1, $user_user->permissions->count());
    }

    /**
     * Test the secondary data defined specifically
     * @return void
     */
    public function testSpecific(): void
    {
        // Run our base query.
        $countries = Country::query()
            ->whereIn('code2', ['gb', 'fr', 'us'])
            ->get()
            ->loadSecondary(['timezones']);
        $this->assertEquals(3, $countries->count());
        // Test each country independently.
        // - GB.
        $country_gb = $countries->where('code2', 'gb');
        $this->assertInstanceOf(Country::class, $country_gb);
        $this->assertInstanceOf(Timezone::class, $country_gb->timezones);
        $this->assertEquals(4, $country_gb->timezones->count());
        // - FR.
        $country_fr = $countries->where('code2', 'fr');
        $this->assertInstanceOf(Country::class, $country_fr);
        $this->assertInstanceOf(Timezone::class, $country_fr->timezones);
        $this->assertEquals(1, $country_fr->timezones->count());
        // - US.
        $country_us = $countries->where('code2', 'us');
        $this->assertInstanceOf(Country::class, $country_us);
        $this->assertInstanceOf(Timezone::class, $country_us->timezones);
        $this->assertEquals(17, $country_us->timezones->count());
    }

    /**
     * Test the secondary data relationship flags (1:1, 1:M, etc)
     * @return void
     */
    public function testRelationshipFlags(): void
    {
        $model = new Country();
        $this->assertFalse($model->isSecondary());
        $this->assertFalse($model->isMultipleSecondary());
        $this->assertFalse($model->isSingularSecondary());
        // Flag it as a multiple secondary.
        $updateA = $model->setMultipleSecondary();
        $this->assertInstanceOf(Country::class, $updateA);
        $this->assertEquals(spl_object_hash($updateA), spl_object_hash($model));
        $this->assertTrue($updateA->isSecondary());
        $this->assertTrue($updateA->isMultipleSecondary());
        $this->assertFalse($updateA->isSingularSecondary());
        // And now as a singular secondary.
        $updateB = $model->setSingularSecondary();
        $this->assertInstanceOf(Country::class, $updateB);
        $this->assertEquals(spl_object_hash($updateB), spl_object_hash($model));
        $this->assertTrue($updateB->isSecondary());
        $this->assertFalse($updateB->isMultipleSecondary());
        $this->assertTrue($updateB->isSingularSecondary());
    }
}
