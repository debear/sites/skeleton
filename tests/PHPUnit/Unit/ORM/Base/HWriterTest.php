<?php

namespace Tests\PHPUnit\Unit\ORM\Base;

use Tests\PHPUnit\Base\UnitTestCase;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\ORM\Skeleton\CMSContent;
use DeBear\ORM\Skeleton\UserActivityLog;
use DeBear\ORM\Skeleton\User;
use DeBear\Exceptions\ORMException;

class HWriterTest extends UnitTestCase
{
    /**
     * Test creating a new single row
     * @return void
     */
    public function testCreateSingle(): void
    {
        $raw = CMSContent::create([
            'site' => 'ci-ins1',
            'page' => '/',
            'section' => 'heading',
            'order' => 10,
            'live' => 1,
            'content' => '<h3>Hello, world!</h3>',
        ]);
        $this->assertTrue($raw->isset());
        $this->assertEquals(1, $raw->count());
        $this->assertNotNull($raw->content_id);
        $this->assertEquals('ci-ins1', $raw->site);

        // Now compare with what we have in the database.
        $db = CMSContent::query()->where('content_id', $raw->content_id)->get();
        $this->assertTrue($db->isset());
        $this->assertEquals(1, $db->count());
        $this->assertEquals('ci-ins1', $db->site);
    }

    /**
     * Test creating multiple new rows
     * @return void
     */
    public function testCreateMultiple(): void
    {
        // Run the creation.
        CMSContent::createMany([
            [
                'site' => 'ci-insM',
                'page' => '/',
                'section' => 'heading',
                'order' => 10,
                'live' => 1,
                'content' => '<h3>Hello, world!</h3>',
            ],
            [
                'site' => 'ci-insM',
                'page' => '/',
                'section' => 'intro',
                'order' => 20,
                'live' => 1,
                'content' => '<p>Welcome.</p>',
            ],
        ]);
        // And test what we get back.
        $db = CMSContent::query()->where('site', 'ci-insM')->orderBy('content_id')->get();
        $this->assertTrue($db->isset());
        $this->assertEquals(2, $db->count());
        $this->assertEquals(['heading', 'intro'], $db->pluck('section'));
        $this->assertEquals([10, 20], $db->pluck('order'));
        $this->assertEquals([1], $db->unique('live'));
    }

    /**
     * Test updating a row
     * @return void
     */
    public function testUpdate(): void
    {
        $row = CMSContent::create([
            'site' => 'ci-upd',
            'page' => '/',
            'section' => 'heading',
            'order' => 10,
            'live' => 0,
            'content' => '<h3>Hello, world!</h3>',
        ]);
        $this->assertTrue($row->isset());
        $this->assertEquals(1, $row->count());
        $this->assertEquals(0, $row->live);
        // An update - when no changes - should not cause problems.
        $orig_id = $row->content_id;
        $row->save();
        $this->assertEquals($orig_id, $row->content_id);

        // Now make some changes and apply.
        $row->update([
            'live' => 1,
        ]);
        $this->assertNotEquals([], $row->getColumnChanges());
        $row->save();
        $this->assertEquals([], $row->getColumnChanges());
        $this->assertEquals($orig_id, $row->content_id);
    }

    /**
     * Test deleting a single row
     * @return void
     */
    public function testDeleteSingle(): void
    {
        $raw = CMSContent::create([
            'site' => 'ci-del1',
            'page' => '/',
            'section' => 'heading',
            'order' => 10,
            'live' => 0,
            'content' => '<h3>Hello, world!</h3>',
        ]);
        $this->assertTrue($raw->isset());
        $this->assertEquals(1, $raw->count());
        // Perform the delete.
        $raw->deleteRow();
        $db = CMSContent::query()->where('site', 'ci-del1')->get();
        $this->assertFalse($db->isset());
        $this->assertEquals(0, $db->count());

        // Also try performing a delete on an empty model (silently fails).
        $db->deleteRow();
        $this->assertEquals(0, $db->count());
    }

    /**
     * Test deleting multiple rows
     * @return void
     */
    public function testDeleteMultiple(): void
    {
        CMSContent::createMany([
            [
                'site' => 'ci-delM',
                'page' => '/',
                'section' => 'heading',
                'order' => 10,
                'live' => 1,
                'content' => '<h3>Hello, world!</h3>',
            ],
            [
                'site' => 'ci-delM',
                'page' => '/',
                'section' => 'intro',
                'order' => 20,
                'live' => 1,
                'content' => '<p>Welcome.</p>',
            ],
        ]);
        // Get the rows out and test okay.
        $query = CMSContent::query()->where('site', 'ci-delM');
        $db_pre = $query->get();
        $this->assertTrue($db_pre->isset());
        $this->assertEquals(2, $db_pre->count());
        // Now do the delete.
        $db_pre->deleteAll();
        $db_post = $query->get();
        $this->assertFalse($db_post->isset());
        $this->assertEquals(0, $db_post->count());

        // Again, try performing on the empty model (also, silently fails).
        $db_post->deleteAll();
        $this->assertEquals(0, $db_post->count());
    }

    /**
     * Test a query that generates an exception
     * @return void
     */
    public function testQueryException(): void
    {
        $this->expectException(ORMException::class);
        CMSContent::create([
            'site' => 'ci-exc',
            'page' => '/',
            'section' => 'heading',
            'order' => 10,
            'live' => 0,
            'content' => '<h3>Hello, world!</h3>',
            'faux_column' => true,
        ]);
    }

    /**
     * Test the audit capabilities
     * @return void
     */
    public function testAudit(): void
    {
        // Confirm empty starting point.
        $type = 'security';
        $type_id = FrameworkConfig::get("debear.logging.user_activity.$type");
        $count_before = UserActivityLog::query()->where('type', $type_id)->count();
        $this->assertEquals(0, $count_before);

        // Create a record and audit it.
        $record = CMSContent::create([
            'site' => 'ci-audit',
            'page' => '/',
            'section' => 'heading',
            'order' => 10,
            'live' => 0,
            'content' => '<p>Audited</p>',
        ])->audit($type, 'Content created', ['site', 'page', 'live']);

        // Confirm the audit.
        $count_create = UserActivityLog::query()->where('type', $type_id)->count();
        $this->assertEquals(1, $count_create);
        $audit = UserActivityLog::query()->where('type', $type_id)->orderByDesc('activity_id')->limit(1)->get();
        $this->assertEquals('Content created', $audit->summary);
        $audit_detail = json_decode($audit->detail, true);
        $this->assertEquals(0, $audit_detail['live']);
        $this->assertFalse(isset($audit_detail['section']));

        // Now update the record and audit it.
        $record->update(['live' => 1])->audit($type, 'Content updated', ['live']);

        // Confirm the audit.
        $count_update = UserActivityLog::query()->where('type', $type_id)->count();
        $this->assertEquals(2, $count_update);
        $audit = UserActivityLog::query()->where('type', $type_id)->orderByDesc('activity_id')->limit(1)->get();
        $this->assertEquals('Content updated', $audit->summary);
        $audit_detail = json_decode($audit->detail, true);
        $this->assertEquals(1, $audit_detail['live']);
        $this->assertFalse(isset($audit_detail['section']));

        // Consider auditing could mask certain fields.
        User::create([
            'id' => 1,
            'user_id' => 'test_audit',
            'password' => 'testlogindetails',
            'display_name' => 'Auditer',
            'status' => 'Active',
        ])->audit('account_registration', 'User created', ['user_id', 'display_name', 'password']);
        $audit = UserActivityLog::query()
            ->where('type', FrameworkConfig::get('debear.logging.user_activity.account_registration'))
            ->orderByDesc('activity_id')->limit(1)->get();
        $audit_detail = json_decode($audit->detail, true);
        $this->assertNotEquals('testlogindetails', $audit_detail['password']);
        $this->assertEquals('**REDACTED**', $audit_detail['password']);
    }
}
