<?php

namespace Tests\PHPUnit\Unit\ORM\Base;

use Tests\PHPUnit\Base\UnitTestCase;
use DeBear\ORM\Skeleton\Timezone;

class IMiscActionTest extends UnitTestCase
{
    /**
     * Test merging in externally sourced rows
     * @return void
     */
    public function testMerge(): void
    {
        $data = Timezone::query()->where('code2', 'gb')->get();
        $this->assertEquals(4, $data->count());
        // Create our custom record and merge it in.
        $merged = $data->merge(Timezone::build([
            'timezone' => 'Europe/Cardiff',
            'code2' => 'gb',
            'geo_lat' => 51.45057,
            'geo_lng' => -3.18176,
            'population' => 364248,
        ]));
        // Test what we get back, ensuring the objects are the same.
        foreach ([$data, $merged] as $obj) {
            $this->assertEquals(5, $obj->count());
            $this->assertEquals(364248, $obj->where('timezone', 'Europe/Cardiff')->population);
        }

        // Merging with encoded data isn't double-encoded.
        $a = Timezone::build(['timezone' => '"', 'code2' => 'a'], ['trusted' => true]);
        $this->assertEquals('"', $a->timezone);

        $b = Timezone::build(['timezone' => '"', 'code2' => 'b'], ['trusted' => true]);
        $this->assertEquals('"', $b->timezone);

        $merged = $a->merge($b);
        $this->assertEquals('"', $merged->where('code2', 'a')->timezone);
        $this->assertEquals('"', $merged->where('code2', 'b')->timezone);
    }

    /**
     * Test the automatic total rows counter on the query
     * @return void
     */
    public function testLimitCalcRows(): void
    {
        $data = Timezone::query()->where('code2', 'gb')->limit(2)->get();
        $this->assertEquals(2, $data->count());
        $this->assertEquals(4, $data->totalRows());
    }

    /**
     * Test the query pagintion orocess
     * @return void
     */
    public function testPagination(): void
    {
        // Run our paged query and assert some high-level info.
        $data = Timezone::query()->orderBy('code2')->orderBy('timezone')->paginate(3, 5)->get();
        $this->assertEquals(5, $data->count());
        $this->assertEquals(393, $data->totalRows());
        $tz = $data->pluck('timezone');
        $this->assertEquals('America/Argentina/Cordoba', $tz[0]);
        $this->assertEquals('America/Argentina/Rio_Gallegos', $tz[4]);
        $countries = $data->unique('code2');
        $this->assertEquals(1, count($countries));
        $this->assertEquals('ar', $countries[0]);
        // Test API counts and info we get back.
        $pagination = $data->resultsToPaginatedAPI('tz');
        $this->assertEquals('America/Argentina/Cordoba', $pagination['tz'][0]['timezone']);
        $this->assertEquals('ar', $pagination['tz'][0]['code2']);
        $this->assertEquals(11, $pagination['rows']['from']);
        $this->assertEquals(15, $pagination['rows']['to']);
        $this->assertEquals(393, $pagination['rows']['total']);
        $this->assertEquals(3, $pagination['pages']['curr']);
        $this->assertEquals(79, $pagination['pages']['total']);
        $this->assertEquals(true, $pagination['pages']['prev']);
        $this->assertEquals(true, $pagination['pages']['next']);
        // Test what happens with empty responses.
        $empty = Timezone::query()->where('code2', '-')->paginate(0, 10)->get()->resultsToPaginatedAPI('tz');
        $this->assertEmpty($empty['tz']);
        $this->assertEquals(0, $empty['rows']['from']);
        $this->assertEquals(0, $empty['rows']['to']);
        $this->assertEquals(0, $empty['rows']['total']);
        $this->assertEquals(0, $empty['pages']['curr']);
        $this->assertEquals(0, $empty['pages']['total']);
        $this->assertEquals(false, $empty['pages']['prev']);
        $this->assertEquals(false, $empty['pages']['next']);
    }
}
