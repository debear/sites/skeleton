<?php

namespace Tests\PHPUnit\Unit\ORM\Base;

use Tests\PHPUnit\Base\UnitTestCase;
use DeBear\ORM\Base\Helpers\QueryExecutor;
use DeBear\ORM\Skeleton\CMSContent;
use DeBear\Exceptions\ORMException;

class JQueryExecutorTest extends UnitTestCase
{
    /**
     * Test the "standard", working, path
     * @return void
     */
    public function testWorking(): void
    {
        // Run the (working) statements file.
        QueryExecutor::runTemplate('mysql_common', __DIR__ . '/../../../Setup/files/orm-statements.php');
        // Check the additions.
        $addts = CMSContent::query()->where('site', 'ci-exec')->get();
        $this->assertEquals(1, $addts->count());
        $this->assertEquals(1, $addts->live);
        $this->assertEquals('intro', $addts->section);
    }

    /**
     * Test the exception being thrown due to the supplied path
     * @return void
     */
    public function testExceptionFile(): void
    {
        $this->expectException(ORMException::class);
        QueryExecutor::runTemplate('mysql', __DIR__ . '/../../../Setup/files/orm-missing.php');
    }

    /**
     * Test the exception being thrown due to a statement
     * @return void
     */
    public function testExceptionStatements(): void
    {
        $this->expectException(ORMException::class);
        QueryExecutor::runTemplate('mysql', __DIR__ . '/../../../Setup/files/orm-exception.php');
    }
}
