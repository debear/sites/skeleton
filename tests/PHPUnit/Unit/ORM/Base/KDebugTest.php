<?php

namespace Tests\PHPUnit\Unit\ORM\Base;

use Tests\PHPUnit\Base\UnitTestCase;
use DeBear\ORM\Skeleton\Timezone;

class KDebugTest extends UnitTestCase
{
    /**
     * Test the "to string" details of the model
     * @return void
     */
    public function testToString(): void
    {
        // Root model.
        $root = Timezone::all();
        $this->assertEquals('{{~~ DeBearORM: DeBear\ORM\Skeleton\Timezone :: 393 records ~~}}', (string)$root);
        // Subset model.
        $subset = $root->where('code2', 'us');
        $this->assertEquals('{{~~ DeBearORM: DeBear\ORM\Skeleton\Timezone :: 17 records ~~}}', (string)$subset);
    }

    /**
     * Test the "to object" version of the model
     * @return void
     */
    public function testToObject(): void
    {
        $base = Timezone::query()->where('code2', 'us')->get();
        $copy = $base->toObject();
        $this->assertEquals($base->count(), $copy->count());
        $this->assertEquals($base->pluck('timezone'), $copy->pluck('timezone'));
        $this->assertNotEquals(spl_object_hash($base), spl_object_hash($copy));
    }
}
