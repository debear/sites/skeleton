<?php

namespace Tests\PHPUnit\Unit\ORM\Base;

use Tests\PHPUnit\Base\UnitTestCase;
use DeBear\ORM\Skeleton\Timezone;
use DeBear\Exceptions\ORMException;

class FSetterTest extends UnitTestCase
{
    /**
     * Test setting info from matching rows
     * @return void
     */
    public function testMatch(): void
    {
        $row = Timezone::query()->where('timezone', 'Europe/London')->get();
        $this->assertEquals(1, $row->count());
        $this->assertTrue($row->isset());
        $this->assertEquals('gb', $row->code2);
        $this->assertEquals(7556900, $row->population);
        $this->assertEquals([], $row->getColumnChanges());
        // Update a field.
        $row->population = 12345;
        $this->assertEquals(12345, $row->population);
        // What changes were recorded?
        $changes = $row->getColumnChanges();
        $this->assertEquals(['population'], $changes);
        // Reset the changes.
        $row->resetColumnChanges();
        $this->assertEquals([], $row->getColumnChanges());
        // Unset a value.
        unset($row->population);
        $this->assertNull($row->population);
        // Interact with a property.
        $this->assertNull($row->customProperty);
        $row->setProperty('customProperty', true);
        $this->assertTrue($row->customProperty);
    }

    /**
     * Test setting info from a subset model object
     * @return void
     */
    public function testSubset(): void
    {
        $row = Timezone::query()->where('code2', 'gb')->get()->where('timezone', 'Europe/London');
        $this->assertEquals(1, $row->count());
        $this->assertEquals(7556900, $row->population);
        $this->assertEquals([], $row->getColumnChanges());
        // Update a field.
        $row->population = 12345;
        $this->assertEquals(12345, $row->population);
        // What changes were recorded?
        $changes = $row->getColumnChanges();
        $this->assertEquals(['population'], $changes);
        // Reset the changes.
        $row->resetColumnChanges();
        $this->assertEquals([], $row->getColumnChanges());
    }

    /**
     * Test what happens when an empty resultset is checked
     * @return void
     */
    public function testEmpty(): void
    {
        $empty = Timezone::query()->where('timezone', 'Region/Not_Found')->get();
        $this->assertEquals(0, $empty->count());
        $this->assertFalse($empty->isset());
        // Writing should return an exception.
        $this->expectException(ORMException::class);
        $empty->population = 12345;
    }
}
