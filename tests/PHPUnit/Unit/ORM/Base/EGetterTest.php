<?php

namespace Tests\PHPUnit\Unit\ORM\Base;

use Tests\PHPUnit\Base\UnitTestCase;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use Carbon\Carbon;
use DeBear\ORM\Skeleton\User;
use DeBear\ORM\Skeleton\CMSContent;
use DeBear\Repositories\Time;
use Tests\PHPUnit\Setup\ORM\TestSET;
use Tests\PHPUnit\Setup\ORM\TestPrePostProcess;

class EGetterTest extends UnitTestCase
{
    /**
     * Test getting info from matching rows
     * @return void
     */
    public function testMatch(): void
    {
        $user = User::query()->where('user_id', 'test_user')->get();
        $this->assertEquals(1, $user->count());
        $this->assertTrue($user->isset());
        // Accessing a row's value.
        $this->assertNotNull($user->user_id);
        $this->assertTrue(isset($user->user_id));
        $this->assertEquals('test_user', $user->user_id);
        $this->assertEquals('Test User', $user->name);
        $this->assertNull($user->random_column);
        // Test isset on mutations and missing attributes.
        $this->assertTrue(isset($user->name));
        $this->assertFalse(isset($user->not_exists));
        // Dates should be Carbon objects.
        $this->assertInstanceOf(Carbon::class, $user->account_created);
        // Full dataset test.
        $ret_arr = $user->toArray();
        $this->assertEquals(10, $ret_arr['id']);
        $this->assertEquals('test_user', $ret_arr['user_id']);
        $ret_data = $user->getData();
        $this->assertEquals(1, count($ret_data));
        $this->assertEquals($ret_arr, array_filter($ret_data[0], function ($key) {
            return substr($key, 0, 6) != '__orm_';
        }, ARRAY_FILTER_USE_KEY));
    }

    /**
     * Test getting info from a subset model object
     * @return void
     */
    public function testSubset(): void
    {
        $user = User::query()->where('user_id', 'test_user')->get()->first();
        $this->assertEquals(1, $user->count());
        $this->assertEquals('test_user', $user->user_id);
        $ret_data = $user->getData();
        $this->assertEquals(1, count($ret_data));
        $this->assertEquals('test_user', $ret_data[0]['user_id']);
    }

    /**
     * Test what happens when an empty resultset is checked
     * @return void
     */
    public function testEmpty(): void
    {
        $empty = User::query()->where('user_id', 'not-found')->get();
        $this->assertEquals(0, $empty->count());
        $this->assertFalse($empty->isset());
        // Accessing any (supposed) value should be null.
        $this->assertNull($empty->user_id);
        $this->assertFalse(isset($empty->user_id));
        // Full dataset is empty.
        $exp = [];
        $this->assertEquals($exp, $empty->toArray());
        $this->assertEquals($exp, $empty->getData());
    }

    /**
     * Test what happens with the handling of cast dates
     * @return void
     */
    public function testDates(): void
    {
        // Ensure datetimes cast to dates are processed correctly when converting to "earlier" timezones.
        $config = FrameworkConfig::get('debear.datetime');
        FrameworkConfig::set([
            'debear.datetime.timezone_db' => 'Europe/London',
            'debear.datetime.timezone_app' => 'US/Eastern',
        ]);
        Time::object()->setup();
        $user = new User(['dob' => '2021-01-01']);
        $this->assertEquals('2021-01-01', $user->dob);
        $this->assertEquals('US/Eastern', $user->dob->getTimezone());
        $this->assertEquals(7, $user->dob->hour);
        FrameworkConfig::set(['debear.datetime' => $config]);
        Time::object()->setup();
    }

    /**
     * Test what happens with the handling of SET columns
     * @return void
     */
    public function testSETs(): void
    {
        // Create an in-memory object (as the skeleton schema does not contain a SET column).
        $obj = new TestSET([
            'scalar' => 'scalar',
            'set' => 'A,B,D',
        ]);
        // SET configuration.
        $fields = $obj->getSETFields();
        $this->assertIsArray($fields);
        $this->assertEquals(1, count($fields));
        $this->assertEquals(['set' => ['a', 'b', 'c', 'd']], $fields);
        // Some properties.
        $this->assertEquals('scalar', $obj->scalar);
        $this->assertTrue(is_object($obj->set));
        $this->assertTrue($obj->set->a);
        $this->assertTrue($obj->set->b);
        $this->assertFalse($obj->set->c);
        $this->assertTrue($obj->set->d);
        $this->assertNull($obj->set->e);
    }

    /**
     * Test the API output rendering
     * @return void
     */
    public function testAPIs(): void
    {
        $user = User::query()->where('user_id', 'test_user')->get();
        // As an individual row.
        $user_row = $user->rowToAPI();
        $this->assertIsArray($user_row);
        $this->assertEquals(11, count($user_row));
        $this->assertEquals(10, $user_row['id']);
        $this->assertEquals('test_user', $user_row['user_id']);
        $this->assertEquals('1970-01-01', $user_row['dob']);
        $this->assertEquals('2019-01-01T12:34:56+00:00', $user_row['account_created']);
        // And then a resultset.
        $user_res = $user->resultsToAPI();
        $this->assertIsArray($user_res);
        $this->assertEquals(1, count($user_res));
        $this->assertEquals([$user_row], $user_res);

        // With a secondary resultset.
        $user = User::query()
            ->where('user_id', 'test_user')
            ->get()
            ->loadSecondary(['permissions'])
            ->hideFieldsFromAPI(['dob']) // In this one, we'll hide the DOB field.
            ->rowToAPI();
        $this->assertIsArray($user);
        $this->assertFalse(isset($user['password'])); // Hidden field by default.
        $this->assertFalse(isset($user['dob'])); // Custom hidden field.
        $this->assertIsArray($user['permissions']);
        $this->assertEquals(1, count($user['permissions']));
        $this->assertEquals([
            'site' => 'www',
            'section' => null,
            'user_id' => 10,
            'level' => 'user',
        ], $user['permissions'][0]);

        // Then a model with a SET field.
        $set_obj = new TestSET([
            'scalar' => 'scalar',
            'set' => 'A,B,D',
        ]);
        $set_row = $set_obj->rowToAPI();
        $this->assertIsArray($set_row);
        $this->assertEquals('scalar', $set_row['scalar']);
        $this->assertIsArray($set_row['set']);
        $this->assertEquals([
            'a' => true,
            'b' => true,
            'c' => false,
            'd' => true,
        ], $set_row['set']);

        // Bespoke pre-/post-processing.
        $pp_obj = new TestPrePostProcess([
            'a' => 1,
            'b' => 2,
            // Formatting.
            'fmt_as_string' => 'string',
            'fmt_int_as_string' => 1,
            'fmt_as_int' => 2,
            'fmt_as_float' => 3,
            'fmt_as_2dp' => 4,
            'fmt_as_4dp' => 1.234567, // Will be rounded!
            'fmt_as_bool' => 0,
        ]);
        $this->assertNull($pp_obj->c);
        $pp_row = $pp_obj->rowToAPI();
        $this->assertIsArray($pp_row);
        $this->assertEquals(1, $pp_row['a']);
        $this->assertEquals(2, $pp_row['b']);
        $this->assertEquals(3, $pp_row['c']);
        $this->assertEquals(0, $pp_row['i']);
        $this->assertEquals('string', $pp_row['fmt_as_string']);
        $this->assertEquals('1', $pp_row['fmt_int_as_string']);
        $this->assertEquals(2, $pp_row['fmt_as_int']);
        $this->assertEquals(3.0, $pp_row['fmt_as_float']);
        $this->assertEquals(4.00, $pp_row['fmt_as_2dp']);
        $this->assertEquals(1.2346, $pp_row['fmt_as_4dp']);
        $this->assertEquals(false, $pp_row['fmt_as_bool']);
    }

    /**
     * Test the way data is encoded into and back out from the database
     * @return void
     */
    public function testEncoding(): void
    {
        // First, when creating an object it should be encoded.
        $obj_encoded = new CMSContent([
            'site' => 'ci-encoding',
            'page' => '/',
            'section' => 'object',
            'order' => 10,
            'live' => 0,
            'content' => '<span class="hello">World</span>',
        ]);
        $this->assertEquals('&lt;span class=&quot;hello&quot;&gt;World&lt;/span&gt;', $obj_encoded->content);
        // Unless we state it can be trusted.
        $obj_trusted = new CMSContent([
            'site' => 'ci-encoding',
            'page' => '/',
            'section' => 'object',
            'order' => 10,
            'live' => 0,
            'content' => '<span class="hello">World</span>',
        ], ['trusted' => true]);
        $this->assertEquals('<span class="hello">World</span>', $obj_trusted->content);

        // Then, something from the database, which hasn't been encoded.
        $db_native = CMSContent::query()->where([
            'site' => 'www',
            'page' => '/test',
            'section' => 'title',
            'order' => 10,
            'live' => 0,
        ])->get();
        $this->assertEquals('<h1>Inactive Title</h1>', $db_native->content);
    }
}
