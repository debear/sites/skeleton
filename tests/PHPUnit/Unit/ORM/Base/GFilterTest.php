<?php

namespace Tests\PHPUnit\Unit\ORM\Base;

use Tests\PHPUnit\Base\UnitTestCase;
use DeBear\ORM\Skeleton\Timezone;
use DeBear\Exceptions\ORMException;

class GFilterTest extends UnitTestCase
{
    /**
     * Our test data
     * @var Timezone
     */
    protected $tz;
    /**
     * Our test empty object
     * @var Timezone
     */
    protected $empty;

    /**
     * Get the test data, where we are returning actual rows
     * @return Timezone Model filled with data
     */
    protected function getData(): Timezone
    {
        if (!isset($this->tz)) {
            $this->tz = Timezone::query()->where('code2', 'gb')->orderByDesc('population')->get();
        }
        return $this->tz;
    }

    /**
     * Get an empty test object
     * @return Timezone Model filled no data
     */
    protected function getEmpty(): Timezone
    {
        if (!isset($this->empty)) {
            $this->empty = Timezone::query()->where('code2', 'zz')->get();
        }
        return $this->empty;
    }

    /**
     * Test the basic filters
     * @return void
     */
    public function testBasic(): void
    {
        $raw = $this->getData();
        $this->assertEquals(4, $raw->count());
        $this->assertEquals([
            'Europe/London',
            'Europe/Guernsey',
            'Europe/Jersey',
            'Europe/Isle_of_Man',
        ], $raw->pluck('timezone'));
        $this->assertEquals(['gb'], $raw->unique('code2'));
        // Some random subsets.
        $this->assertEquals('gb', $raw->random()->code2);
        $rand2 = $raw->random(2);
        $this->assertEquals(2, $rand2->count());
        $this->assertEquals(['gb', 'gb'], array_values($rand2->pluck('code2'))); // Unkey the returned value.
        // First/Last/Slice.
        $this->assertEquals('Europe/London', $raw->first()->timezone);
        $this->assertEquals('Europe/Isle_of_Man', $raw->last()->timezone);
        $sliced = $raw->slice(1);
        $this->assertEquals(3, $sliced->count());
        $this->assertEquals('Europe/Guernsey', $sliced->timezone);
        // Paginate.
        $paginated = $raw->forPage(1, 2);
        $this->assertEquals(2, $paginated->count());
        $this->assertEquals([
            'Europe/London',
            'Europe/Guernsey',
        ], $paginated->pluck('timezone'));
        // Reversed.
        $reversed = $raw->reverse();
        $this->assertEquals(4, $reversed->count());
        $this->assertEquals('Europe/Isle_of_Man', $reversed->first()->timezone);
        $this->assertEquals('Europe/London', $reversed->last()->timezone);
        // An empty random() entry.
        $empty = $this->getEmpty();
        $this->assertEquals(0, $empty->count());
        $random = $empty->random();
        $this->assertIsObject($random);
        $this->assertEquals(0, $random->count());
    }

    /**
     * Test the where filters
     * @return void
     */
    public function testWheres(): void
    {
        $raw = $this->getData();
        // Name checks.
        $this->assertEquals(0, $raw->where('timezone', 'Europe/Unknown')->count());
        $this->assertEquals(0, $raw->where('timezone', '=', 'Europe/Unknown')->count());
        $this->assertEquals(1, $raw->where('timezone', 'Europe/London')->count());
        $this->assertEquals(1, $raw->where('timezone', '=', 'Europe/London')->count());
        // Population checks.
        $this->assertEquals(1, $raw->where('population', '>', 1000000)->count());
        $this->assertEquals(3, $raw->where('population', '<', 1000000)->count());
        $this->assertEquals(2, $raw->where('population', '>', 100000)->count());
        $this->assertEquals(2, $raw->where('population', '<', 100000)->count());
        // Inverse.
        $this->assertEquals(3, $raw->whereNot('timezone', 'Europe/London')->count());
        // Ranges - Numeric.
        $this->assertEquals(2, $raw->whereBetween('population', 80000, 180000)->count());
        $this->assertEquals(2, $raw->whereNotBetween('population', 80000, 180000)->count());
        // Ranges - String.
        $this->assertEquals(1, $raw->whereBetween('timezone', 'Europe/K', 'Europe/M')->count());
        // Given values.
        $this->assertEquals(1, $raw->whereIn('population', [90812])->count());
        $this->assertEquals(3, $raw->whereNotIn('population', [90812])->count());
        $this->assertEquals(1, $raw->whereIn('timezone', ['Europe/Jersey'])->count());
        $this->assertEquals(3, $raw->whereNotIn('timezone', ['Europe/Jersey'])->count());
        // NULLs.
        $this->assertEquals(0, $raw->whereNull('population')->count());
        $this->assertEquals(4, $raw->whereNotNull('population')->count());
    }

    /**
     * Test the object getters
     * @return void
     */
    public function testGetters(): void
    {
        $raw = $this->getData();
        // As an array.
        $arrayZero = $raw->getIndex(0);
        $this->assertEquals('Europe/London', $arrayZero['timezone']);
        $arrayOne = $raw->getIndex(1);
        $this->assertEquals('Europe/Guernsey', $arrayOne['timezone']);
        // As an object.
        $objectZero = $raw->getRow(0);
        $this->assertEquals('Europe/London', $objectZero->timezone);
        $objectOne = $raw->getRow(1);
        $this->assertEquals('Europe/Guernsey', $objectOne->timezone);
        // The current object (which will also be the first item).
        $this->assertEquals('Europe/London', $raw->getCurrent()->timezone);
        // By specific keys.
        $specific = $raw->getByKeys([1, 2]);
        $this->assertTrue($specific->isset());
        $this->assertEquals(2, $specific->count());
        $this->assertEquals(['Europe/Guernsey', 'Europe/Jersey'], $specific->unique('timezone'));
    }

    /**
     * Test filtering the values by a closure
     * @return void
     */
    public function testFiltered(): void
    {
        $filtered = $this->getData()->filter(function ($item) {
            return ($item['geo_lat'] > 52) || ($item['geo_lng'] < -2.5);
        });
        $this->assertTrue($filtered->isset());
        $this->assertEquals(2, $filtered->count());
        $this->assertEquals(['Europe/Guernsey', 'Europe/Isle_of_Man'], $filtered->unique('timezone'));
    }

    /**
     * Test some values sorted via a closure
     * @return void
     */
    public function testSortedClosure(): void
    {
        $raw = $this->getData();
        // Our closure: population asc.
        $closure = function ($a, $b) {
            return $a['population'] <=> $b['population'];
        };
        // Run - ascending.
        $sort_asc = $raw->sort($closure);
        $this->assertTrue($sort_asc->isset());
        $this->assertEquals($raw->count(), $sort_asc->count());
        $this->assertEquals([
            'Europe/Isle_of_Man',
            'Europe/Jersey',
            'Europe/Guernsey',
            'Europe/London',
        ], array_values($sort_asc->pluck('timezone')));
        // Run - descending.
        $sort_desc = $raw->sortDesc($closure);
        $this->assertTrue($sort_desc->isset());
        $this->assertEquals($raw->count(), $sort_desc->count());
        // Technically, it should return the same result.
        $this->assertEquals($raw->pluck('timezone'), $sort_desc->pluck('timezone'));

        // We also have aliases via sortBy and sortByDesc.
        $sortBy_asc = $raw->sortBy($closure);
        $this->assertTrue($sortBy_asc->isset());
        $this->assertEquals($raw->count(), $sortBy_asc->count());
        $this->assertEquals($raw->pluck('timezone'), $sortBy_asc->pluck('timezone'));
        $sortBy_desc = $raw->sortByDesc($closure);
        $this->assertTrue($sortBy_desc->isset());
        $this->assertEquals($raw->count(), $sortBy_desc->count());
        $this->assertEquals($raw->pluck('timezone'), $sortBy_desc->pluck('timezone'));
    }

    /**
     * Test some values sorted by a column
     * @return void
     */
    public function testSortedColumn(): void
    {
        $raw = $this->getData();

        // Single column, ascending.
        $sortBy_str = $raw->sortBy('timezone');
        $this->assertTrue($sortBy_str->isset());
        $this->assertEquals($raw->count(), $sortBy_str->count());
        $this->assertEquals([
            'Europe/Guernsey',
            'Europe/Isle_of_Man',
            'Europe/Jersey',
            'Europe/London',
        ], array_values($sortBy_str->pluck('timezone')));
        // A single array element should work the same.
        $sortBy_arrSingle = $raw->sortBy(['timezone']);
        $this->assertTrue($sortBy_arrSingle->isset());
        $this->assertEquals($raw->count(), $sortBy_arrSingle->count());
        $this->assertEquals($sortBy_str->pluck('timezone'), $sortBy_arrSingle->pluck('timezone'));
        // Multi-column, ascending.
        $sortBy_arrMulti = $raw->sortBy(['code2', 'population']);
        $this->assertTrue($sortBy_arrMulti->isset());
        $this->assertEquals($raw->count(), $sortBy_arrMulti->count());
        $this->assertEquals([
            'Europe/Isle_of_Man',
            'Europe/Jersey',
            'Europe/Guernsey',
            'Europe/London',
        ], array_values($sortBy_arrMulti->pluck('timezone')));
        // Multi-column, unbreakable, ascending.
        $sortBy_unbreakable = $raw->sortBy(['code2', 'code2']);
        $this->assertTrue($sortBy_unbreakable->isset());
        $this->assertEquals($raw->count(), $sortBy_unbreakable->count());

        // Single column, descending.
        $sortByDesc_str = $raw->sortByDesc('timezone');
        $this->assertTrue($sortByDesc_str->isset());
        $this->assertEquals($raw->count(), $sortByDesc_str->count());
        $this->assertEquals([
            'Europe/London',
            'Europe/Jersey',
            'Europe/Isle_of_Man',
            'Europe/Guernsey',
        ], array_values($sortByDesc_str->pluck('timezone')));
        // Multi-column, descending.
        $sortByDesc_arrMulti = $raw->sortByDesc(['code2', 'population']);
        $this->assertTrue($sortByDesc_arrMulti->isset());
        $this->assertEquals($raw->count(), $sortByDesc_arrMulti->count());
        $this->assertEquals([
            'Europe/London',
            'Europe/Guernsey',
            'Europe/Jersey',
            'Europe/Isle_of_Man',
        ], array_values($sortByDesc_arrMulti->pluck('timezone')));
    }

    /**
     * Test some values sorted by a callable method
     * @return void
     */
    public function testSortedCallable(): void
    {
        $raw = $this->getData();

        // Direct closure.
        $sortBy_closure = $raw->sortBy(function ($a, $b) {
            return $a['geo_lat'] <=> $b['geo_lat'];
        });
        $this->assertTrue($sortBy_closure->isset());
        $this->assertEquals($raw->count(), $sortBy_closure->count());
        $this->assertEquals([
            'Europe/Jersey',
            'Europe/Guernsey',
            'Europe/London',
            'Europe/Isle_of_Man',
        ], array_values($sortBy_closure->pluck('timezone')));

        // Callable array.
        $sortBy_callable = $raw->sortBy([static::class, 'sortByCallable']);
        $this->assertTrue($sortBy_callable->isset());
        $this->assertEquals($raw->count(), $sortBy_callable->count());
        $this->assertEquals([
            'Europe/Jersey',
            'Europe/Guernsey',
            'Europe/London',
            'Europe/Isle_of_Man',
        ], array_values($sortBy_callable->pluck('timezone')));

        // Non-callable array.
        $sortBy_fields = $raw->sortBy(['timezone', 'geo_lat']);
        $this->assertTrue($sortBy_fields->isset());
        $this->assertEquals($raw->count(), $sortBy_fields->count());
        $this->assertEquals([
            'Europe/Guernsey',
            'Europe/Isle_of_Man',
            'Europe/Jersey',
            'Europe/London',
        ], array_values($sortBy_fields->pluck('timezone')));
    }

    /**
     * A callable sortBy method to sort two Timezones by get_lat
     * @param array $a The first Timezone instance.
     * @param array $b The comparison Timezone instance.
     * @return integer The appropriate sort result for the two elements being checked
     */
    public static function sortByCallable(array $a, array $b): int
    {
        return $a['geo_lat'] <=> $b['geo_lat'];
    }

    /**
     * Test getting aggregated values
     * @return void
     */
    public function testAggregate(): void
    {
        $raw = $this->getData();
        $this->assertEquals(1970190.25, $raw->avg('population'));
        $this->assertEquals(124406, $raw->median('population'));
        $this->assertEquals(75049, $raw->min('population'));
        $this->assertEquals(7556900, $raw->max('population'));
        $this->assertEquals(7880761, $raw->sum('population'));
        // We also need a tweaked median...
        $this->assertEquals(90812, $raw->where('population', '<', 1000000)->median('population'));
    }

    /**
     * Test exceptions generated by the 'avg' aggregator
     * @return void
     */
    public function testAggregatedExcptnAvg(): void
    {
        $empty = $this->getEmpty();
        $this->assertEquals(0, $empty->count());
        $this->expectException(ORMException::class);
        $empty->avg('population');
    }

    /**
     * Test exceptions generated by the 'median' aggregator
     * @return void
     */
    public function testAggregatedExcptnMedian(): void
    {
        $empty = $this->getEmpty();
        $this->assertEquals(0, $empty->count());
        $this->expectException(ORMException::class);
        $empty->median('population');
    }

    /**
     * Test exceptions generated by the 'min' aggregator
     * @return void
     */
    public function testAggregatedExcptnMin(): void
    {
        $empty = $this->getEmpty();
        $this->assertEquals(0, $empty->count());
        $this->expectException(ORMException::class);
        $empty->min('population');
    }

    /**
     * Test exceptions generated by the 'max' aggregator
     * @return void
     */
    public function testAggregatedExcptnMax(): void
    {
        $empty = $this->getEmpty();
        $this->assertEquals(0, $empty->count());
        $this->expectException(ORMException::class);
        $empty->max('population');
    }
}
