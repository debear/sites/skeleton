<?php

namespace Tests\PHPUnit\Unit\ORM\Base;

use Tests\PHPUnit\Base\UnitTestCase;
use DeBear\ORM\Skeleton\Timezone;

class CIterateTest extends UnitTestCase
{
    /**
     * Test the iterator for an empty object.
     * @return void
     */
    public function testEmpty(): void
    {
        $data = Timezone::query()->where('code2', 'zz')->get();
        $this->assertEquals(0, $data->count());
        // Empty models should have key/index of -1.
        $this->assertEquals(-1, $data->index());
        $this->assertEquals(-1, $data->key());
        $this->assertFalse($data->valid(0));
    }

    /**
     * Test the iterator when we have data to iterate over
     * @return void
     */
    public function testIterable(): void
    {
        $data = Timezone::query()->where('code2', 'us')->get();
        $this->assertNotEquals(0, $data->count());
        // First position should have key/index of 0.
        $this->assertEquals(0, $data->index());
        $this->assertEquals(0, $data->key());
        $this->assertTrue($data->valid(0));

        // Iterate over the results, testing the counters.
        $data_ref = spl_object_hash($data);
        $i = 0;
        foreach ($data as $row) {
            $this->assertEquals($i, $data->index());
            $this->assertEquals($i, $data->key()); // Because in this instance key == index.
            $this->assertEquals($data_ref, spl_object_hash($row));
            // Manually increment the counter (as we're testing iterators!).
            $i++;
        }
        $this->assertEquals($data->count(), $data->index());
    }

    /**
     * Test the validity tests with the model
     * @return void
     */
    public function testSubsetValid(): void
    {
        $data = Timezone::query()->where('code2', 'us')->get()->first();
        $this->assertEquals(1, $data->count());
        $this->assertTrue($data->valid(0));
    }
}
