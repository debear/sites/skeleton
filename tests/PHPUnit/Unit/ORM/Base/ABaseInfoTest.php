<?php

namespace Tests\PHPUnit\Unit\ORM\Base;

use Tests\PHPUnit\Base\UnitTestCase;
use DeBear\ORM\Skeleton\CommsEmail;
use DeBear\ORM\Skeleton\News;
use DeBear\ORM\Skeleton\User;
use DeBear\ORM\Skeleton\UserRememberMe;

class ABaseInfoTest extends UnitTestCase
{
    /**
     * Test getting the name of the model's connection
     * @return void
     */
    public function testConnectionName(): void
    {
        $this->assertEquals('mysql', User::getConnectionName());
        $this->assertEquals('mysql', UserRememberMe::getConnectionName());
        $this->assertEquals('mysql_common', CommsEmail::getConnectionName());
        $this->assertEquals('mysql_common', News::getConnectionName());
    }

    /**
     * Test getting the name of the model's database
     * @return void
     */
    public function testDatabaseName(): void
    {
        $this->assertEquals(env('DB_PREFIX') . 'debearco_admin', User::getDatabase());
        $this->assertEquals(env('DB_PREFIX') . 'debearco_admin', UserRememberMe::getDatabase());
        $this->assertEquals(env('DB_PREFIX') . 'debearco_common', CommsEmail::getDatabase());
        $this->assertEquals(env('DB_PREFIX') . 'debearco_common', News::getDatabase());
    }

    /**
     * Test getting the name of the model's table
     * @return void
     */
    public function testTableName(): void
    {
        $this->assertEquals('USERS', User::getTable());
        $this->assertEquals('USER_REMEMBERME', UserRememberMe::getTable());
        $this->assertEquals('COMMS_EMAIL', CommsEmail::getTable());
        $this->assertEquals('NEWS_ARTICLES', News::getTable());
    }

    /**
     * Test getting the model's primary key
     * @return void
     */
    public function testPrimaryKey(): void
    {
        $this->assertEquals('id', User::getPrimaryKey());
        $this->assertEquals(['user_id', 'rm_string'], UserRememberMe::getPrimaryKey());
        $this->assertEquals(['app', 'email_id'], CommsEmail::getPrimaryKey());
        $this->assertEquals(['app', 'news_id'], News::getPrimaryKey());
    }
}
