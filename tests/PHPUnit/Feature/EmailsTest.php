<?php

namespace Tests\PHPUnit\Feature;

use Tests\PHPUnit\Base\FeatureTestCase;

class EmailsTest extends FeatureTestCase
{
    /**
     * A test for viewing emails
     * @return void
     */
    public function testView(): void
    {
        // Invalid code.
        $response = $this->get('/email-gobbledigookcode-invalidx-checksum');
        $response->assertStatus(404);
        $response->assertSee('<h1>Unknown Email</h1>');
        // Invalid checksums.
        $response = $this->get('/email-ZH3Oml1BIo8zLUoy-invalidx-checksum');
        $response->assertStatus(404);
        $response->assertSee('<h1>Unknown Email</h1>');

        // Plain text code and checksum.
        $response = $this->get('/email-ZH3Oml1BIo8zLUoy-dd9d4b8d-c76c212d');
        $response->assertStatus(200);
        $response->assertSee('<!DOCTYPE html>');
        $response->assertSee('<dt>Subject:</dt><dd>DeBear.uk Login Details</dd>');
        $response->assertSee('<dt>Send:</dt><dd>Mon 10th Dec 2018 at 20:01</dd>');
        $response->assertSee('<div class="message">
            <pre>Hello,

Someone triggered the Forgotten Details process for the DeBear.uk
login details attached to this email address. If this was you, please
see below for futher details.');

        // HTML code and checksum.
        $response = $this->get('/email-rVmrRT8h9DXdM53s-3d78dfe7-0d16e64e');
        $response->assertStatus(200);
        $response->assertSee('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" '
            . '"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">');
        $response->assertSee('<dt>Subject:</dt><dd>DeBear.uk Login Details</dd>');
        $response->assertSee('<dt>Send:</dt><dd>Mon 10th Dec 2018 at 20:01</dd>');
        $response->assertSeeInOrder(['<style nonce="', '" type="text/css">
      /* Client-specific Styles */
      #outlook a{padding:0;} /* Force Outlook to provide a "view in browser" button. */']);
        $response->assertSee('<a href="https://debear.test/email-rVmrRT8h9DXdM53s-3d78dfe7-0d16e64e" target="_blank">'
            . 'View it in your browser</a>');
        $response->assertSee('<!-- Begin Text Content -->
                                <p>Hello,</p>' . "\r" . '
' . "\r" . '
<p>Someone triggered the Forgotten Details process for the DeBear.uk login details attached to this email address. '
            . 'If this was you, please see below for futher details.</p>');
        $response->assertSee('<li><strong>Approximate Location:</strong> Bath <img alt="United Kingdom" '
            . 'src="https://static.debear.test/flag/gb.png" width="16" height="11"><br/><em style="font-size: 12px; '
            . 'color: #808080">(Based on the IP address of the computer making the change)</em></li>');
    }

    /**
     * A test for processing email links
     * @return void
     */
    public function testLinks(): void
    {
        // Invalid code.
        $response = $this->get('/eml-gobbledigookcode-invalidx-checksum');
        $response->assertStatus(404);
        $response->assertSee('<h1>Unknown Link</h1>');
        // Invalid checksums.
        $response = $this->get('/eml-rVmrRT8h9DXdMqrG-invalidx-checksum');
        $response->assertStatus(404);
        $response->assertSee('<h1>Unknown Link</h1>');

        // Valid code and checksum.
        $response = $this->get('/eml-rVmrRT8h9DXdMqrG-3d5dffc2-b69f7428');
        $response->assertStatus(302);
        $response->assertRedirect('https://debear.test/');
    }
}
