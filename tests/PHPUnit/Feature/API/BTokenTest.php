<?php

namespace Tests\PHPUnit\Feature\API;

use Illuminate\Support\Facades\App;
use Tests\PHPUnit\Base\FeatureTestCase;

class BTokenTest extends FeatureTestCase
{
    /**
     * The base endpoint to target for our tests.
     * @var string
     */
    protected $endpoint = '/api/unit-tests/v1.0/token';

    /**
     * A test for the various token processing scenarios.
     * @return void
     */
    public function testScenarios(): void
    {
        // Tests need to run during a fixed time for token validity tests.
        $this->setTestDateTime('2021-11-01 12:34:56');

        // Define our test scenarios.
        $msg = [
            200 => 'Test Passed',
            401 => 'No valid API key supplied',
            403 => 'API key does not have access to this resource',
        ];
        $scenarios = [
            // Missing authorisation.
            ['header' => 'X-NotAuthorization', 'method' => 'DoesNotMatter', 'token' => 'doesnotmatter'],
            // Non-Basic authorisation.
            ['method' => 'Invalid', 'token' => 'test-valid'],
            // Revoked token.
            ['token' => 'test-revoked'],
            // Token valid in future.
            ['token' => 'test-valid-active-future'],
            // Invalid password.
            ['token' => 'test-valid', 'password' => 'invalidpassword'],
            // Valid details.
            ['token' => 'test-valid', 'status' => 200],
            // Token valid, but revoked in future.
            ['token' => 'test-valid-revoked-future', 'status' => 200],
            // Token valid, no scopes available.
            ['token' => 'test-valid-scope-none', 'status' => 403],
            // Token valid, but no matching scope.
            ['token' => 'test-valid-scope-invalid', 'status' => 403],
            // Token valid, scope active in future.
            ['token' => 'test-valid-scope-future-active', 'status' => 403],
            // Token valid, scope active but revoked.
            ['token' => 'test-valid-scope-revoked', 'status' => 403],
            // Token valid, scope active but revoked in future.
            ['token' => 'test-valid-scope-future-revoked', 'status' => 200],
        ];
        foreach ($scenarios as $test) {
            // Set standard scenario fields.
            $test['header'] = ($test['header'] ?? 'Authorization');
            $test['method'] = ($test['method'] ?? 'Basic');
            $test['password'] = ($test['password'] ?? 'apitokenpassword');
            $test['status'] = ($test['status'] ?? 401);
            // Now run.
            $response = $this->withHeaders([
                $test['header'] => "{$test['method']} " . base64_encode("{$test['token']}:{$test['password']}"),
            ])->get("{$this->endpoint}/valid");
            $response->assertStatus($test['status']);
            $response->assertJSON(['msg' => $msg[$test['status']]]);
        }
    }

    /**
     * A test for the logical operators applied to a scope
     * @return void
     */
    public function testScopeLogic(): void
    {
        $auth_header = [
            'Accept' => 'application/json',
            'Authorization' => 'Basic ' . base64_encode("test-valid:apitokenpassword"),
        ];

        // Test of 'or', which returns a valid response.
        $response = $this->withHeaders($auth_header)->get("{$this->endpoint}/logic/or");
        $response->assertStatus(200);
        $response->assertJSON(['msg' => 'Test Passed']);

        // Test of 'and', which returns a forbidden response.
        $response = $this->withHeaders($auth_header)->get("{$this->endpoint}/logic/and");
        $response->assertStatus(403);
        $response->assertJSON(['msg' => 'API key does not have access to this resource']);

        // Test of 'both', which returns an exception.
        $response = $this->withHeaders($auth_header)->get("{$this->endpoint}/logic/both");
        $response->assertStatus(500);
        $response->assertJSON(['msg' => 'The request was not completed. The server met an unexpected condition.']);
    }
}
