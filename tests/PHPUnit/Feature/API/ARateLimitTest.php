<?php

namespace Tests\PHPUnit\Feature\API;

use Tests\PHPUnit\Base\FeatureTestCase;

class ARateLimitTest extends FeatureTestCase
{
    /**
     * A test for 'generic' rules
     * @return void
     */
    public function testGeneric(): void
    {
        // The 'generic_read' rate.
        $response = $this->get('/api/unit-tests/v1.0/throttle/generic_read');
        $response->assertStatus(200);
        $response->assertJSON(['msg' => 'Test Passed']);
        $response->assertHeader('x-ratelimit-limit', 12);
        $response->assertHeader('x-ratelimit-remaining', 11);

        // The 'generic_write' rate.
        $response = $this->get('/api/unit-tests/v1.0/throttle/generic_write');
        $response->assertStatus(200);
        $response->assertJSON(['msg' => 'Test Passed']);
        $response->assertHeader('x-ratelimit-limit', 4);
        $response->assertHeader('x-ratelimit-remaining', 3);
    }
}
