<?php

namespace Tests\PHPUnit\Feature;

use Illuminate\Support\Facades\App;
use Tests\PHPUnit\Base\FeatureTestCase;
use DeBear\Repositories\InternalCache;

class CookiesTest extends FeatureTestCase
{
    /**
     * A test for viewing the preference details
     * @return void
     */
    public function testView(): void
    {
        $response = $this->get('/cookies');
        $response->assertStatus(200);
        $response->assertSee('<h1>Cookie usage by DeBear Software</h1>');
    }

    /**
     * A test of setting individual preferences
     * @return void
     */
    public function testSetter(): void
    {
        $orig_input = InternalCache::object()->get('php://input');

        // An unknown option, which returns an error.
        $response = $this->patch('/cookies', 'type=unknown&allow=false');
        $response->assertStatus(400);
        $response->assertJsonFragment(['success' => false]);
        $response->assertHeader('Content-Type', 'application/json');
        // A valid option, which necessitates a change.
        $response = $this->patch('/cookies', 'type=analytics&allow=false');
        $response->assertStatus(200);
        $response->assertJsonFragment(['success' => true]);
        $response->assertHeader('Content-Type', 'application/json');

        // Restore default php://input.
        InternalCache::object()->set('php://input', $orig_input);
    }
}
