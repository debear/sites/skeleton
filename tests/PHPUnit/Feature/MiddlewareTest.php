<?php

namespace Tests\PHPUnit\Feature;

use Tests\PHPUnit\Base\FeatureTestCase;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Support\Facades\Request;
use DeBear\Repositories\InternalCache;

class MiddlewareTest extends FeatureTestCase
{
    /**
     * A test for the variation in object setup
     * @return void
     */
    public function testObjects(): void
    {
        $orig = FrameworkConfig::get('debear.social.twitter');

        // Render a page with the live Twitter details at the bottom.
        FrameworkConfig::set(['debear.social.twitter.test' => false]);
        $response = $this->get('/terms-of-use');
        $response->assertStatus(200);
        $response->assertSee('href="//x.com/DeBearSupport"');
        $response->assertSee('<span class="text reset_font">@DeBearSupport</span>');
        $response->assertDontSee('href="//x.com/DeBearTestA"');
        $response->assertDontSee('<span class="text reset_font">@DeBearTestA</span>');

        // Render a page with the test Twitter details at the bottom.
        FrameworkConfig::set(['debear.social.twitter.test' => true]);
        $response = $this->get('/terms-of-use');
        $response->assertStatus(200);
        $response->assertSee('href="//x.com/DeBearTestA"');
        $response->assertSee('<span class="text reset_font">@DeBearTestA</span>');
        $response->assertDontSee('href="//x.com/DeBearSupport"');
        $response->assertDontSee('<span class="text reset_font">@DeBearSupport</span>');

        FrameworkConfig::set(['debear.social.twitter' => $orig]);
    }

    /**
     * A test for the variation in variable setup
     * @return void
     */
    public function testVars(): void
    {
        // Ensure php://input is an actual array.
        InternalCache::object()->unset('php://input');
        $new_input = ['success' => true];
        InternalCache::object()->set('raw:php://input', json_encode($new_input));
        $response = $this->get('/terms-of-use');
        $response->assertStatus(200);
        $parsed_input = InternalCache::object()->get('php://input');
        $this->assertIsArray($parsed_input);
        $this->assertEquals($new_input, $parsed_input);

        // Restore our original input.
        InternalCache::object()->unset('raw:php://input');
        InternalCache::object()->unset('php://input');
    }

    /**
     * A test for the various $_SERVER tweaks
     * @return void
     */
    public function testSERVER(): void
    {
        // Missing SERVER_NAME argument.
        $this->toggleConfig('SERVER_NAME');
        $orig = $_SERVER;
        unset($_SERVER['SERVER_NAME']);
        $this->assertFalse(isset($_SERVER['SERVER_NAME']));
        $response = $this->get('/terms-of-use', [
            'X-DeBear-Host' => 'debear.test',
        ]);
        $response->assertStatus(200);
        $this->assertEquals('debear.test', $_SERVER['SERVER_NAME']);
        $_SERVER = $orig;
        $this->toggleConfig('SERVER_NAME');
    }

    /**
     * A test for the various automated redirections
     * @return void
     */
    public function testRedirects(): void
    {
        // Non-HTTPS.
        $this->toggleConfig('ssl');
        $response = $this->get('/terms-of-use');
        $response->assertStatus(308);
        $response->assertRedirect('https://debear.test/terms-of-use');
        $this->toggleConfig('ssl');

        // Session hash mis-match.
        session(['user.agent' => 'invalid-hash']);
        $response = $this->get('/terms-of-use');
        $response->assertStatus(403);
        $response->assertSee('<h1>Security Concern</h1>');
        session()->forget('user.agent');

        // From fallback.debear to (www).debear.
        // - Prepare config and $_SERVER.
        $orig_config = FrameworkConfig::get('debear.url.subraw');
        FrameworkConfig::set(['debear.url.subraw' => 'fallback']);
        $orig_server = $raw = Request::server();
        $raw['HTTP_HOST'] = FrameworkConfig::get('debear.url.subraw') . '.debear.test';
        Request::getFacadeRoot()->server->replace($raw);
        // - The request.
        $response = $this->get('/terms-of-use', [
            'X-DeBear-Host' => 'fallback.debear.test',
        ]);
        $response->assertStatus(308);
        $response->assertRedirect('https://debear.test/terms-of-use');
        // - Revert config and $_SERVER.
        FrameworkConfig::set(['debear.url.subraw' => $orig_config]);
        Request::getFacadeRoot()->server->replace($orig_server);

        // Disabled sub-site.
        $orig = [
            'debear.section.endpoint' => FrameworkConfig::get('debear.section.endpoint'),
            'debear.section.code' => FrameworkConfig::get('debear.section.code'),
            'debear.subsites' => FrameworkConfig::get('debear.subsites'),
        ];
        FrameworkConfig::set([
            'debear.section.endpoint' => 'subsitetestendpoint',
            'debear.section.code' => 'subsitetest',
            'debear.subsites' => [
                'subsitetest' => [
                    'enabled' => false,
                ],
            ],
        ]);
        // First as code (which should always 404).
        $response_code = $this->get('/subsitetest');
        $response_code->assertStatus(404);
        $response_code->assertSee('<h1>Sorry, but the server ran into a problem...</h1>');
        $response_code->assertSee('<strong>404: Not Found</strong>');
        // And again as the expected endpoint.
        $response_endpoint = $this->get('/subsitetestendpoint');
        $response_endpoint->assertStatus(404);
        $response_endpoint->assertSee('<h1>Sorry, but the server ran into a problem...</h1>');
        $response_endpoint->assertSee('<strong>404: Not Found</strong>');
        FrameworkConfig::set($orig);
    }

    /**
     * A test for forcing users to conform to the (new) password policy
     * @return void
     */
    public function testRedirectPasswordPolicy(): void
    {
        // Perform the login.
        $response = $this->post('/login', [
            'username' => 'test_policy',
            'password' => 'basic123',
        ]);
        $response->assertStatus(200);
        $response->assertJsonFragment(['success' => true]);
        $response->assertHeader('Content-Type', 'application/json');

        // Now attempt to view the homepage, but get bounced to the Account Reset page.
        $response = $this->get('/');
        $response->assertStatus(307);
        $response->assertRedirect('https://my.debear.test/account/reset');
    }

    /**
     * A test of the various policy rules
     * @return void
     */
    public function testAuthPolicies(): void
    {
        // A match.
        $guest_html = $this->get('/auth-policy/match');
        $guest_html->assertStatus(200);
        $guest_html->assertSee('OK');

        $guest_api = $this->get('/auth-policy/match', ['Accept' => 'application/json']);
        $guest_api->assertStatus(200);
        $guest_api->assertHeader('Content-Type', 'application/json');
        $guest_api->assertJsonFragment(['status' => 'OK']);

        // An unauthorised request.
        $unauth_html = $this->get('/auth-policy/unauthorised');
        $unauth_html->assertStatus(401);
        $unauth_html->assertSee('You need to be signed in to view this page.');

        $unauth_api = $this->get('/auth-policy/unauthorised', ['Accept' => 'application/json']);
        $unauth_api->assertStatus(401);
        $unauth_api->assertHeader('Content-Type', 'application/json');
        $unauth_api->assertJsonFragment(['success' => false]);

        // A forbidden request.
        $login = $this->post('/login', [
            'username' => 'test_user',
            'password' => 'testlogindetails',
        ]);
        $login->assertStatus(200);
        $login->assertJsonFragment(['success' => true]);

        $forbidden_html = $this->get('/auth-policy/forbidden');
        $forbidden_html->assertStatus(403);
        $forbidden_html->assertSee('Sorry, but you do not have sufficient privileges to view this page!');

        $forbidden_api = $this->get('/auth-policy/forbidden', ['Accept' => 'application/json']);
        $forbidden_api->assertStatus(403);
        $forbidden_api->assertHeader('Content-Type', 'application/json');
        $forbidden_api->assertJsonFragment(['success' => false]);

        // Invalid combination returns an exception.
        $invalid = $this->get('/auth-policy/invalid');
        $invalid->assertStatus(500);
        $invalid->assertSee('The policy list cannot cannot contain both &quot;or&quot; and &quot;and&quot; logic');
    }
}
