<?php

namespace Tests\PHPUnit\Feature;

use Tests\PHPUnit\Base\FeatureTestCase;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Models\Skeleton\User;
use DeBear\Models\Skeleton\UserActivityLog;
use DeBear\Repositories\SuspiciousActivity;

class FormsTest extends FeatureTestCase
{
    /**
     * A restricted form that we cannot access
     * @return void
     */
    public function testRestricted(): void
    {
        // Performs a redirect.
        $response = $this->get('/forms/restricted/redirect');
        $response->assertStatus(303);
        $response->assertRedirect('https://debear.test/elsewhere');

        // Returns a response.
        $response = $this->get('/forms/restricted/response');
        $response->assertStatus(401);
        $response->assertSee('User Not Signed In');
    }

    /**
     * A basic form covering most of the process
     * @return void
     */
    public function testBasic(): void
    {
        $endpoint = '/forms/basic';
        $form_ref = '220c74'; // Fixed, path-based, reference.
        // Log the user in and render the form.
        User::doLogin(User::find(10));
        $s1_get = $this->get($endpoint);
        $s1_get->assertStatus(200);
        $s1_get->assertSee('<h1>Basic Form</h1>');
        $s1_get->assertSee('<form method="post" action="/forms/basic" id="frm_ci_basic_render" '
            . 'class="validate ci-basic-render">');
        $s1_get->assertSee('<input class="textbox" type="text" id="user_id" name="user_id" value="test_user"');
        $s1_get->assertSee('<input type="checkbox" id="terms" name="terms" required >');

        // Then perform the post, with invalid date (expecting a validation render).
        $s2_post = $this->post($endpoint, [
            'form_ref' => $form_ref,
            'user_id' => 'test_user2',
            'forename' => 'Test',
            'surname' => 'User',
            'anumber' => 10,
            'terms__cb' => 1,
        ]);
        $s2_post->assertStatus(303);
        $s2_post->assertRedirect("https://debear.test$endpoint");
        // Then check the displayed version.
        $s2_get = $this->get($endpoint);
        $s2_get->assertStatus(200);
        $s2_get->assertSee('<h1>Basic Form</h1>');
        $s2_get->assertSee('<li class="label error" id="terms_label">');
        $s2_get->assertSee('<input class="textbox" type="text" id="user_id" name="user_id" value="test_user2"');
        $s2_get->assertSee('<li class="status icon_error" id="anumber_status"></li>');

        // Now a valid post.
        $s3_post = $this->post($endpoint, [
            'form_ref' => $form_ref,
            'user_id' => 'test_user',
            'forename' => 'Test',
            'surname' => 'User',
            'terms__cb' => 1,
            'terms' => 'on',
        ]);
        $s3_post->assertStatus(303);
        $s3_post->assertRedirect("https://debear.test$endpoint");
        // Then check the displayed version.
        $s3_get = $this->get($endpoint);
        $s3_get->assertStatus(200);
        $s3_get->assertSee('<h1>Basic Form &ndash; Confirm</h1>');
        $s3_get->assertSee('<form method="post" action="/forms/basic" id="frm_ci_basic_confirm" '
            . 'class="validate ci-basic-confirm">');
        $s3_get->assertSee('<debug>toString: Class: \'DeBear\Http\Modules\Forms; Config: \'ci/basic\'; '
            . 'Step: \'confirm\'</debug>');
        $s3_get->assertSee('<debug>dump: {"form":"ci\/basic","ref":"220c74","steps":{"first":false,"last":true,'
            . '"rendered":true,"parsed":false,"failed":false,"completed":false},'
            . '"mode":{"render":true,"validate":false}}</debug>');

        // Complete the confirmation.
        $s4_post = $this->post($endpoint, [
            'form_ref' => $form_ref,
        ]);
        $s4_post->assertStatus(303);
        $s4_post->assertRedirect("https://debear.test$endpoint");
        // Ensure we are redirected properly.
        $s4_get = $this->get($endpoint);
        $s4_get->assertStatus(303);
        $s4_get->assertRedirect('https://debear.test/forms/complete');
    }

    /**
     * A form completing to a template
     * @return void
     */
    public function testCompleteTemplate(): void
    {
        $endpoint = '/forms/complete/template';
        // Log the user in and render the form.
        User::doLogin(User::find(10));
        $s1_get = $this->get($endpoint);
        $s1_get->assertStatus(200);
        $s1_get->assertSee('<h1>Complete &racquo; Template Form</h1>');
        $s1_get->assertSee('<form method="post" action="/forms/complete/template" id="frm_ci_complete_template_render" '
            . 'class="validate ci-complete-template-render">');
        $s1_get->assertSee('<input class="textbox" type="text" id="user_id" name="user_id" value="test_user"');

        // Post the data to complete the form.
        $s2_post = $this->post($endpoint, [
            'form_ref' => '42c253', // Fixed, path-based, reference.
            'user_id' => 'test_user',
        ]);
        $s2_post->assertStatus(303);
        $s2_post->assertRedirect("https://debear.test$endpoint");
        // Ensure we are redirected properly.
        $s2_get = $this->get($endpoint);
        $s2_get->assertStatus(200);
        $s2_get->assertSee('<h1>Complete &racquo; Template &ndash; Completion</h1>');
    }

    /**
     * A form completing back to itself
     * @return void
     */
    public function testCompleteFallback(): void
    {
        $endpoint = '/forms/complete/fallback';
        // Log the user in and render the form.
        User::doLogin(User::find(10));
        $s1_get = $this->get($endpoint);
        $s1_get->assertStatus(200);
        $s1_get->assertSee('<h1>Complete &racquo; Fallback Form</h1>');
        $s1_get->assertSee('<form method="post" action="/forms/complete/fallback" id="frm_ci_complete_fallback_render" '
            . 'class="validate ci-complete-fallback-render">');
        $s1_get->assertSee('<input class="textbox" type="text" id="user_id" name="user_id" value="test_user"');

        // Post the data to complete the form.
        $s2_post = $this->post($endpoint, [
            'form_ref' => '20a37d', // Fixed, path-based, reference.
            'user_id' => 'test_user',
        ]);
        $s2_post->assertStatus(303);
        $s2_post->assertRedirect("https://debear.test$endpoint");
        // Ensure we are redirected back to ourselves, and a new instance.
        $s2_get = $this->get($endpoint);
        $s2_get->assertStatus(303);
        $s2_get->assertRedirect("https://debear.test$endpoint");
        // This is now back to the original form.
        $s3_get = $this->get($endpoint);
        $s3_get->assertStatus(200);
        $s3_get->assertSee('<h1>Complete &racquo; Fallback Form</h1>');
        $s3_get->assertSee('<form method="post" action="/forms/complete/fallback" id="frm_ci_complete_fallback_render" '
            . 'class="validate ci-complete-fallback-render">');
        $s3_get->assertSee('<input class="textbox" type="text" id="user_id" name="user_id" value="test_user"');
        User::doLogout();
    }

    /**
     * A form covering the suspicious acivity checks
     * @return void
     */
    public function testSuspiciousActivity(): void
    {
        $endpoint = '/forms/suspicious';
        $form_ref = '78717b';
        // Adjust to something testable.
        App::make(SuspiciousActivity::class)->record('security', 'ci-test', true, ['user_id' => 30]);

        // First run, the test should fail.
        $s1_get = $this->get($endpoint);
        $s1_get->assertStatus(200);
        $s1_get->assertSee('<h1>Suspicious Check Form</h1>');

        $s2_post = $this->post($endpoint, [
            'form_ref' => $form_ref,
            'anumber' => 1,
            'terms__cb' => 1,
        ]);
        $s2_post->assertStatus(303);
        $s2_get = $this->get($endpoint);
        $s2_get->assertStatus(200);
        $s2_get->assertSee('<h1>Suspicious Check Form</h1>');
        $s2_get->assertSee('<li class="label error" id="anumber_label">');
        $s2_get->assertSee('<li class="error details " id="anumber_error">Unable to verify details.</li>');

        // Restore the setting to a passable value and test again - should not fail this time.
        UserActivityLog::where([
            'type' => FrameworkConfig::get('debear.logging.user_activity.security'),
            'user_id' => 30,
        ])->delete();
        $s3_post = $this->post($endpoint, [
            'form_ref' => $form_ref,
            'anumber' => 1,
            'terms__cb' => 1,
        ]);
        $s3_post->assertStatus(303);
        $s3_get = $this->get($endpoint);
        $s3_get->assertStatus(200);
        $s3_get->assertSee('<h1>Suspicious Check Form</h1>');
        $s3_get->assertSee('<li class="label " id="anumber_label">');
        $s3_get->assertSee('<li class="error details hidden" id="anumber_error"></li>');
    }

    /**
     * A form using mappings whilst storing
     * @return void
     */
    public function testStoreMaps(): void
    {
        $endpoint = '/forms/store';
        $form_ref = '43cb97';

        // Run the first time, creating our new user.
        $s1_get = $this->get($endpoint);
        $s1_get->assertStatus(200);
        $s1_get->assertSee('<h1>User Creation Form</h1>');

        $s2_post = $this->post($endpoint, [
            'form_ref' => $form_ref,
            'user_id' => 'ci_form_test',
            'email' => 'ci-form-test@debear.test',
            'forename' => 'Tset',
            'surname' => 'CI Form',
            'display_name' => 'Tset CI Form',
            'dob' => '1970-01-01',
            'timezone' => 'Europe/London',
        ]);
        $s2_post->assertStatus(303);
        $s2_get = $this->get($endpoint);
        $s2_get->assertStatus(303);
        $s2_get->assertRedirect("https://debear.test/forms/complete");

        // Get the user, verify they were created.
        $user = User::where('user_id', 'ci_form_test')->first();
        $this->assertNotNull($user);
        $this->assertEquals('ci-form-test@debear.test', $user->email);
        $this->assertEquals('Tset CI Form', $user->display_name);

        // Then run again, updating them.
        $s1_get = $this->get($endpoint);
        $s1_get->assertStatus(200);
        $s1_get->assertSee('<h1>User Creation Form</h1>');

        $s2_post = $this->post($endpoint, [
            'form_ref' => $form_ref,
            'user_id' => 'ci_form_test',
            'email' => 'ci-form-test@debear.test',
            'forename' => 'Test',
            'surname' => 'CI Form',
            'display_name' => 'Test CI Form',
            'dob' => '1970-01-01',
            'timezone' => 'Europe/London',
        ]);
        $s2_post->assertStatus(303);
        $s2_get = $this->get($endpoint);
        $s2_get->assertStatus(303);
        $s2_get->assertRedirect("https://debear.test/forms/complete");

        // Get the user, verify they were updated.
        $user = User::where('user_id', 'ci_form_test')->first();
        $this->assertNotNull($user);
        $this->assertEquals('Test CI Form', $user->display_name);
    }

    /**
     * A form requiring faker setup (though not testing the Faker helper class fully)
     * @return void
     */
    public function testFakerSetup(): void
    {
        $get = $this->get('/forms/faker?faker=on');
        $get->assertStatus(200);
        $get->assertSee('<h1>Faker Form</h1>');
        $get->assertSee('<form method="post" action="/forms/faker?faker=on" id="frm_ci_faker_render" '
            . 'class="validate ci-faker-render">');
        $get->assertSee('<input class="textbox" type="text" id="forename" name="forename" value="');
        $get->assertSee('" autocomplete="given-name" required maxlength="15" tabindex="1" data-tabindex="1">');
        $get->assertDontSee('<input class="textbox" type="text" id="forename" name="forename" value="" '
            . 'autocomplete="given-name" required maxlength="15" tabindex="1" data-tabindex="1">');
    }

    /**
     * A form triggering events
     * @return void
     */
    public function testEvents(): void
    {
        $endpoint = '/forms/events';
        $form_ref = '8fb720';

        // Initial form load.
        $s1_get = $this->get($endpoint);
        $s1_get->assertStatus(200);
        $s1_get->assertSee('<h1>Events Form</h1>');

        // Submit some data.
        $s2_post = $this->post($endpoint, [
            'form_ref' => $form_ref,
            'anumber' => '1',
        ]);
        $s2_post->assertStatus(303);
        $s2_get = $this->get($endpoint);
        $s2_get->assertStatus(200);
        $s2_get->assertSee('<h1>Events Form</h1>');
        $s2_get->assertSee('<li class="error details " id="anumber_error">Please enter a value between 5 and 9.</li>');
        $s2_get->assertSee('<h3>Validation Info</h3>
    <dl>
        <dt>Object:</dt>
            <dd>null</dd>
        <dt>Original:</dt>
            <dd>[]</dd>
        <dt>Raw:</dt>
            <dd>{"anumber":"1"}</dd>
        <dt>Changes:</dt>
            <dd>{"new":{"anumber":true},"updated":[],"fields":{"anumber":"new"}}</dd>
    </dl>');
    }
}
