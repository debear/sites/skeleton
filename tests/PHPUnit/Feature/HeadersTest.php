<?php

namespace Tests\PHPUnit\Feature;

use Tests\PHPUnit\Base\FeatureTestCase;
use Illuminate\Support\Facades\Config as FrameworkConfig;

class HeadersTest extends FeatureTestCase
{
    /**
     * A test of the headers sent by a stamdard page
     * @return void
     */
    public function testHeadersSent(): void
    {
        $response = $this->get('/terms-of-use'); // A simple page.
        $response->assertStatus(200);
        // Any CSP header?
        $assert_check = (FrameworkConfig::get('debear.headers.csp.enabled') ? 'assertHeader' : 'assertHeaderMissing');
        $csp_header = 'Content-Security-Policy'
            . (FrameworkConfig::get('debear.headers.csp.report_only') ? '-Report-Only' : '');
        $response->$assert_check($csp_header);
        // Switched from Feature-Policy to Permissions-Policy.
        $response->assertHeaderMissing('Feature-Policy');
        $assert_check = FrameworkConfig::get('debear.headers.permissions_policy.enabled')
            ? 'assertHeader' : 'assertHeaderMissing';
        $response->$assert_check('Permissions-Policy');
        // Some specific values we can check.
        $response->assertHeader('Referrer-Policy', 'strict-origin-when-cross-origin');
        $response->assertHeader('Strict-Transport-Security', 'max-age=31536000; includeSubDomains');
        $response->assertHeader('X-Content-Type-Options', 'nosniff');
        $response->assertHeader('X-Frame-Options', 'SAMEORIGIN');
        // X-XSS-Protection is redundant in modern browsers when CSPs are used.
        $response->assertHeaderMissing('X-XSS-Protection', '1; mode=block');
    }
}
