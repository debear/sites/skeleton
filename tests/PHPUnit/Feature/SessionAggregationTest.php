<?php

namespace Tests\PHPUnit\Feature;

use Tests\PHPUnit\Base\FeatureTestCase;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Models\Skeleton\Session;
use DeBear\Models\Skeleton\SessionStats;

class SessionAggregationTest extends FeatureTestCase
{
    /**
     * Test the aggregation process, when enabled, stores and processes the stats
     * The enabled test is performed as a Unit test
     * @return void
     */
    public function testDisabled(): void
    {
        // Counts from before our test.
        $before_sessions = Session::count();
        $before_stats = SessionStats::count();

        // Ensure the feature is disabled.
        $config = [
            'debear.sessions.handler.enabled' => FrameworkConfig::get('debear.sessions.handler.enabled'),
            'debear.sessions.aggregate' => FrameworkConfig::get('debear.sessions.aggregate'),
        ];
        FrameworkConfig::set([
            'debear.sessions.handler.enabled' => true,
            'debear.sessions.aggregate' => false,
        ]);

        // Make a series of requests.
        $requests = ['/forms/basic', '/auth-policy/match'];
        foreach ($requests as $url) {
            $response = $this->get($url);
            $response->assertStatus(200);
        }

        // Restore the feature flag.
        FrameworkConfig::set($config);

        // Then run our assertions to ensure the database row counts match our expectations.
        $this->assertEquals($before_sessions + count($requests), Session::count());
        $this->assertEquals($before_stats, SessionStats::count());
    }
}
