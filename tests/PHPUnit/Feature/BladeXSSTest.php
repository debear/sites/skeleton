<?php

namespace Tests\PHPUnit\Feature;

use Tests\PHPUnit\Base\FeatureTestCase;
use DeBear\Repositories\InternalCache;

class BladeXSSTest extends FeatureTestCase
{
    /**
     * The data to pass in to the tests.
     * @var array
     */
    protected $data = ['content' => '<script>alert(\'Hello, world!\');</script>'];

    /**
     * A test of data supplied to a view via a POST request
     * @return void
     */
    public function testViewPOST(): void
    {
        $response = $this->post('/xss/post', $this->data);
        $response->assertStatus(200);
        // The cleaned input.
        $response->assertSee('&lt;script&gt;alert(&#039;Hello, world!&#039;);&lt;/script&gt;');
        $response->assertSee('(Section (In): &quot;model&quot;)');
        $response->assertSee('(Section (Out): &quot;model&quot;)');
        $response->assertSee('(Section (In): &quot;orm&quot;)');
        $response->assertSee('(Section (Out): &quot;orm&quot;)');
        // Also check - explicitly - that we do not see the unencoded value.
        $response->assertDontSee('<script>alert(\'Hello, world!\');</script>');
        // But also no double-encoding database value.
        $response->assertDontSee('(Out): &amp;lt;script&amp;gt;alert(&amp;#039;Hello, '
            . 'world!&amp;#039;);&amp;lt;/script&amp;gt;');
    }

    /**
     * A test of data supplied to a view via php://input
     * @return void
     */
    public function testViewPHPInput(): void
    {
        InternalCache::object()->unset('php://input');
        InternalCache::object()->set('raw:php://input', json_encode($this->data));
        $response = $this->post('/xss/php-input');
        $response->assertStatus(200);
        // Ensure the data was passed successfully.
        $response->assertDontSee('** MISSING INPUT **');
        // The cleaned input.
        $response->assertSee('&lt;script&gt;alert(&#039;Hello, world!&#039;);&lt;/script&gt;');
        $response->assertSee('(Section (In): &quot;model&quot;)');
        $response->assertSee('(Section (Out): &quot;model&quot;)');
        $response->assertSee('(Section (In): &quot;orm&quot;)');
        $response->assertSee('(Section (Out): &quot;orm&quot;)');
        // Also check - explicitly - that we do not see the unencoded value.
        $response->assertDontSee('<script>alert(\'Hello, world!\');</script>');
        // But also no double-encoding database value.
        $response->assertDontSee('(Out): &amp;lt;script&amp;gt;alert(&amp;#039;Hello, '
            . 'world!&amp;#039;);&amp;lt;/script&amp;gt;');

        // Restore our original input.
        InternalCache::object()->unset('raw:php://input');
        InternalCache::object()->unset('php://input');
    }

    /**
     * A test of data supplied to a form via a POST request
     * @return void
     */
    public function testFormsPOST(): void
    {
        $endpoint = '/forms/xss';
        $form_ref = 'af6197';

        // Confirm empty start point.
        $s1_get = $this->get($endpoint);
        $s1_get->assertStatus(200);
        $s1_get->assertSee('ID: (unset)' . "\n");
        // Post our data to be created.
        $s2_post = $this->post($endpoint, array_merge($this->data, ['form_ref' => $form_ref]));
        $s2_post->assertStatus(303);
        $s2_get = $this->get($endpoint);
        $s2_get->assertStatus(303);
        $s2_get->assertRedirect("https://debear.test$endpoint");
        // Re-fetch the original form, to confirm the data is encoded correctly.
        $s3_get = $this->get($endpoint);
        $s3_get->assertStatus(200);
        $s3_get->assertDontSee('ID: (unset)' . "\n");
        $s3_get->assertSee('Order: 10' . "\n");
        $s3_get->assertSee('Content: &lt;script&gt;alert(&#039;Hello, world!&#039;);&lt;/script&gt;' . "\n");
        // Also check - explicitly - that we do not see the unencoded value.
        $s3_get->assertDontSee('<script>alert(\'Hello, world!\');</script>');
    }
}
