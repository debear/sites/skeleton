<?php

namespace Tests\PHPUnit\Feature;

use Tests\PHPUnit\Base\FeatureTestCase;

class AuthenticationTest extends FeatureTestCase
{
    /**
     * A test with invalid details
     * @return void
     */
    public function testLoginInvalid(): void
    {
        // Missing component.
        $response = $this->post('/login', [
            'username' => 'only',
        ]);
        $response->assertStatus(401);
        $response->assertJsonFragment(['success' => false]);
        $response->assertHeader('Content-Type', 'application/json');

        // Unkown user.
        $response = $this->post('/login', [
            'username' => 'unknown_user',
            'password' => 'testlogindetails',
        ]);
        $response->assertStatus(401);
        $response->assertJsonFragment(['success' => false]);
        $response->assertHeader('Content-Type', 'application/json');
    }

    /**
     * A test for logging in
     * @return void
     */
    public function testLoginValid(): void
    {
        $response = $this->post('/login', [
            'username' => 'test_user',
            'password' => 'testlogindetails',
        ]);
        $response->assertStatus(200);
        $response->assertJsonFragment(['success' => true]);
        $response->assertHeader('Content-Type', 'application/json');
    }

    /**
     * A test for a failed login in
     * @return void
     */
    public function testLoginFail(): void
    {
        $response = $this->post('/login', [
            'username' => 'test_user',
            'password' => 'testlogindetailsFAIL',
        ]);
        $response->assertStatus(401);
        $response->assertJsonFragment(['success' => false]);
        $response->assertHeader('Content-Type', 'application/json');
    }

    /**
     * A test for logging in as an unverified user
     * @return void
     */
    public function testLoginUnverified(): void
    {
        $response = $this->post('/login', [
            'username' => 'test_unverified',
            'password' => 'testlogindetails',
        ]);
        $response->assertStatus(200);
        $response->assertJsonFragment(['success' => true]);
        $response->assertHeader('Content-Type', 'application/json');
    }

    /**
     * A test for logging in as a disabled user
     * @return void
     */
    public function testLoginDisabled(): void
    {
        $response = $this->post('/login', [
            'username' => 'test_disabled',
            'password' => 'testlogindetails',
        ]);
        $response->assertStatus(401);
        $response->assertJsonFragment(['success' => false]);
        $response->assertHeader('Content-Type', 'application/json');
    }

    /**
     * A test for logging in... whilst already logged in
     * @return void
     */
    public function testLoginWhenLoggedIn(): void
    {
        // The first login.
        $post = [
            'username' => 'test_user',
            'password' => 'testlogindetails',
        ];
        $response = $this->post('/login', $post);

        // The second login.
        $response = $this->post('/login', $post);
        $response->assertStatus(409);
        $response->assertJsonFragment(['success' => false]);
        $response->assertHeader('Content-Type', 'application/json');
    }

    /**
     * A test for logging out... when not logged in
     * @return void
     */
    public function testLogoutNotLoggedIn(): void
    {
        $response = $this->post('/logout');
        $response->assertStatus(409);
        $response->assertJsonFragment(['success' => false]);
        $response->assertHeader('Content-Type', 'application/json');
    }

    /**
     * A test for logging out
     * @return void
     */
    public function testLogout(): void
    {
        // The login.
        $response = $this->post('/login', [
            'username' => 'test_user',
            'password' => 'testlogindetails',
        ]);

        // The logout.
        $response = $this->post('/logout');
        $response->assertStatus(200);
        $response->assertJsonFragment(['success' => true]);
        $response->assertHeader('Content-Type', 'application/json');
    }

    /**
     * A test for logging out... after having already logged out
     * @return void
     */
    public function testLogoutAfterLogout(): void
    {
        // The login.
        $response = $this->post('/login', [
            'username' => 'test_user',
            'password' => 'testlogindetails',
        ]);

        // The first logout.
        $response = $this->post('/logout');

        // The second logout.
        $response = $this->post('/logout');
        $response->assertStatus(409);
        $response->assertJsonFragment(['success' => false]);
        $response->assertHeader('Content-Type', 'application/json');
    }

    /**
     * A test for logging in hitting failure rate limits
     * @return void
     */
    public function testLoginFailedLimits(): void
    {
        // The first login.
        for ($i = 0; $i < 6; $i++) {
            $response = $this->post('/login', [
                'username' => 'test_user',
                'password' => 'testlogindetails' . ($i < 5 ? 'FAIL' : ''),
            ]);
            $response->assertStatus(401);
            $response->assertJsonFragment(['success' => false]);
            $response->assertHeader('Content-Type', 'application/json');
        }
    }
}
