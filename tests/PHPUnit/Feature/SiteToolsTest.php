<?php

namespace Tests\PHPUnit\Feature;

use Illuminate\Support\Facades\App;
use Tests\PHPUnit\Base\FeatureTestCase;
use DeBear\Repositories\InternalCache;

class SiteToolsTest extends FeatureTestCase
{
    /**
     * A test against the manifest.
     * @return void
     */
    public function testManifest(): void
    {
        $response = $this->get('/manifest.json');
        $response->assertStatus(200);
        $response->assertSee('/?utm_source=homescreen');
        $response->assertHeader('Content-Type', 'application/manifest+json');
    }

    /**
     * A test against the service worker.
     * @return void
     */
    public function testServiceWorker(): void
    {
        $response = $this->get('/pwa-sw-basic.js');
        $response->assertRedirect('//static.debear.' . env('APP_TLD') . "/www/js/pwa-sw/basic.js");
    }

    /**
     * A test against the "standard" Terms of Use page.
     * @return void
     */
    public function testTermsStandard(): void
    {
        $response = $this->get('/terms-of-use');
        $response->assertStatus(200);
        $response->assertSee('<h1>Terms of Use</h1>');
        $response->assertSee('<logo-img></logo-img>');
        $response->assertDontSee('<h2>Acceptance of these Terms</h2>');

        // Some extra tests to push the assertion engine.
        $response->assertSeeText('&quot;Client&quot;, &quot;You&quot; and &quot;Your&quot;');
        $response->assertSeeInOrder([
            '<li>Republish material',
            '<li>Sell, rent or sub-license material',
            '<li>Reproduce, duplicate or copy material',
            '<li>Redistribute content',
        ]);
        $response->assertSeeTextInOrder([
            '  Republish material',
            '  Sell, rent or sub-license material',
            '  Reproduce, duplicate or copy material',
            '  Redistribute content',
        ]);
        $response->assertDontSee('&lt;h1&gt;Terms of Use&lt;/h1&gt;');
        $response->assertDontSeeText('"Client", "You" and "Your"');
    }

    /**
     * A test against the interactive Terms of Use page.
     * @return void
     */
    public function testTermsInteractive(): void
    {
        $response = $this->get('/terms-of-use?interactive=on');
        $response->assertStatus(200);
        $response->assertSee('<h1>Terms of Use</h1>');
        $response->assertDontSee('<logo-img></logo-img>');
        $response->assertSee('<h2>Acceptance of these Terms</h2>');
    }

    /**
     * A test against the Privacy Policy page.
     * @return void
     */
    public function testPrivacy(): void
    {
        $response = $this->get('/privacy-policy');
        $response->assertStatus(200);
        $response->assertSee('<h1>Privacy Policy</h1>');
    }

    /**
     * A test against the Sitemap pages (HTML and XML).
     * @return void
     */
    public function testSitemap(): void
    {
        // HTML.
        $response = $this->get('/sitemap');
        $response->assertStatus(200);
        $response->assertSee('<h1>Find your way round DeBear.uk!</h1>');
        // XML.
        $response = $this->get('/sitemap.xml');
        $response->assertStatus(200);
        $response->assertSee('<?xml version="1.0" encoding="UTF-8"?>');
    }

    /**
     * A test against the Report URI notification.
     * @return void
     */
    public function testReportURI(): void
    {
        $orig_input = InternalCache::object()->get('php://input');

        // Invalid string input.
        InternalCache::object()->set('php://input', 'invalid_string');
        $response = $this->post('/report-uri/csp');
        $response->assertStatus(400);
        $response->assertJsonFragment(['success' => false]);
        $response->assertHeader('Content-Type', 'application/json');
        // Invalid array input.
        InternalCache::object()->set('php://input', []);
        $response = $this->post('/report-uri/csp');
        $response->assertStatus(400);
        $response->assertJsonFragment(['success' => false]);
        $response->assertHeader('Content-Type', 'application/json');
        // Valid input.
        InternalCache::object()->set('php://input', [
            'custom' => 'data',
        ]);
        $response = $this->post('/report-uri/csp');
        $response->assertStatus(204);

        // Restore default php://input.
        InternalCache::object()->set('php://input', $orig_input);
    }

    /**
     * A test against the Offline page.
     * @return void
     */
    public function testOffline(): void
    {
        $response = $this->get('/offline.htm');
        $response->assertStatus(200);
        $response->assertSee('<h1>Device Offline</h1>');
    }
}
