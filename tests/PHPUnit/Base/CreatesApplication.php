<?php

namespace Tests\PHPUnit\Base;

use Illuminate\Contracts\Console\Kernel;

trait CreatesApplication
{
    /**
     * The Laravel app instance
     * @var Illuminate\Foundation\Application
     */
    protected $app;

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        if (!isset($this->app)) {
            $this->app = require __DIR__ . '/../../../bootstrap/app.php';
            $this->app->make(Kernel::class)->bootstrap();
        }
        return $this->app;
    }
}
