<?php

namespace Tests\PHPUnit\Base;

use Illuminate\Routing\Middleware\ThrottleRequests;
use DeBear\Implementations\TestResponse;

class APITestCase extends FeatureTestCase
{
    /**
     * Test an API GET request without any throttling rules
     * @param string $uri     The URI we are accessing.
     * @param array  $headers Additional headers to pass to our test request.
     * @return TestResponse The returned information from the request
     */
    public function getUnthrottled(string $uri, array $headers = []): TestResponse
    {
        return parent::withoutMiddleware([
            ThrottleRequests::class,
        ])->get($uri, $headers);
    }

    /**
     * Test an API POST request without any throttling rules
     * @param string $uri     The URI we are accessing.
     * @param array  $data    The data to pass the request.
     * @param array  $headers Additional headers to pass to our test request.
     * @return TestResponse The returned information from the request
     */
    public function postUnthrottled(string $uri, array $data = [], array $headers = []): TestResponse
    {
        return parent::withoutMiddleware([
            ThrottleRequests::class,
        ])->post($uri, $data, $headers);
    }

    /**
     * Test an API PATCH request without any throttling rules
     * @param string $uri     The URI we are accessing.
     * @param array  $data    The data to pass the request.
     * @param array  $headers Additional headers to pass to our test request.
     * @return TestResponse The returned information from the request
     */
    public function patchUnthrottled(string $uri, array $data = [], array $headers = []): TestResponse
    {
        return parent::withoutMiddleware([
            ThrottleRequests::class,
        ])->patch($uri, $data, $headers);
    }

    /**
     * Test an API PUT request without any throttling rules
     * @param string $uri     The URI we are accessing.
     * @param array  $data    The data to pass the request.
     * @param array  $headers Additional headers to pass to our test request.
     * @return TestResponse The returned information from the request
     */
    public function putUnthrottled(string $uri, array $data = [], array $headers = []): TestResponse
    {
        return parent::withoutMiddleware([
            ThrottleRequests::class,
        ])->put($uri, $data, $headers);
    }

    /**
     * Test an API DELETE request without any throttling rules
     * @param string $uri     The URI we are accessing.
     * @param array  $data    The data to pass the request.
     * @param array  $headers Additional headers to pass to our test request.
     * @return TestResponse The returned information from the request
     */
    public function deleteUnthrottled(string $uri, array $data = [], array $headers = []): TestResponse
    {
        return parent::withoutMiddleware([
            ThrottleRequests::class,
        ])->delete($uri, $data, $headers);
    }

    /**
     * Test an API OPTIONS request without any throttling rules
     * @param string $uri     The URI we are accessing.
     * @param array  $data    The data to pass the request.
     * @param array  $headers Additional headers to pass to our test request.
     * @return TestResponse The returned information from the request
     */
    public function optionsUnthrottled(string $uri, array $data = [], array $headers = []): TestResponse
    {
        return parent::withoutMiddleware([
            ThrottleRequests::class,
        ])->options($uri, $data, $headers);
    }

    /**
     * Test an API HEAD request without any throttling rules
     * @param string $uri     The URI we are accessing.
     * @param array  $headers Additional headers to pass to our test request.
     * @return TestResponse The returned information from the request
     */
    public function headUnthrottled(string $uri, array $headers = []): TestResponse
    {
        return parent::withoutMiddleware([
            ThrottleRequests::class,
        ])->head($uri, $headers);
    }
}
