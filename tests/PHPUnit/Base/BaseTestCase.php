<?php

namespace Tests\PHPUnit\Base;

use Illuminate\Foundation\Testing\TestCase as IlluminateTestCase;
use DeBear\Helpers\PHPUnit\Traits\CustomiseTest as CustomiseTestTrait;

abstract class BaseTestCase extends IlluminateTestCase
{
    use CreatesApplication;
    use CustomiseTestTrait;
}
