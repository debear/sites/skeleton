<?php

// Due to an issue with compatibility with the base versions in the MakesHttpRequests testing concern, do
// not apply the Squiz.Commenting.FunctionComment rule to check function argument signatures.
// phpcs:disable Squiz.Commenting.FunctionComment

namespace Tests\PHPUnit\Base;

use Illuminate\Testing\TestResponse as BaseTestResponse;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\Benchmark;
use DeBear\Helpers\PHPUnit\RefreshDatabase;
use DeBear\Implementations\TestResponse;
use DeBear\Repositories\InternalCache;

class FeatureTestCase extends BaseTestCase
{
    /**
     * Whether or not we should refresh the database between runs
     * @var boolean
     */
    protected $refresh_database = true;
    /**
     * Configuration keys that are to be preserved and reset between calls
     * @var array
     */
    protected $config_keys = ['debear.content'];
    /**
     * A backup of the above configuration keys to be reset after the call
     * @var array
     */
    protected $config_orig;
    /**
     * Cached toggle statuses
     * @var array
     */
    protected $toggles = [
        'ssl' => true,
        'SERVER_NAME' => true,
    ];

    /**
     * Clean the database between runs
     * @return void
     */
    protected function setUp(): void
    {
        // Load the standard test setup methods.
        parent::setUp();
        // Database management.
        if ($this->refresh_database) {
            RefreshDatabase::run();
        }
        // Backup the original version of configuration that is reset between calls.
        $this->config_orig = FrameworkConfig::get($this->config_keys);
    }

    /**
     * Test a HTTP GET request
     * @param string $uri     The URI we are accessing.
     * @param array  $headers Additional headers to pass to our test request.
     * @return TestResponse The returned information from the request
     */
    public function get(/* string */ $uri, array $headers = []): TestResponse
    {
        $_SERVER['REQUEST_METHOD'] = 'GET';
        $this->prepare($uri, $headers);
        return $this->processResponse(parent::get($uri, $headers));
    }

    /**
     * Test a HTTP POST request
     * @param string       $uri     The URI we are accessing.
     * @param array|string $data    The data to pass the request.
     * @param array        $headers Additional headers to pass to our test request.
     * @return TestResponse The returned information from the request
     */
    public function post(/* string */ $uri, array|string $data = [], array $headers = []): TestResponse
    {
        // Update the PHP Input.
        $is_json = (isset($headers['Content-Type']) && $headers['Content-Type'] == 'application/json');
        if ($is_json && $data) {
            $original_input = InternalCache::object()->get('php://input');
            InternalCache::object()->set('php://input', $data);
            $data = [];
        }
        $_POST = $_REQUEST = $data;
        // Make the request.
        $_SERVER['REQUEST_METHOD'] = 'POST';
        $this->prepare($uri, $headers);
        $response = $this->processResponse(parent::post($uri, $data, $headers));
        // Restore the previous input.
        if (isset($original_input)) {
            InternalCache::object()->set('php://input', $original_input);
        }
        // Return the response.
        return $response;
    }

    /**
     * Test a HTTP PATCH request
     * @param string       $uri     The URI we are accessing.
     * @param array|string $data    The data to pass the request.
     * @param array        $headers Additional headers to pass to our test request.
     * @return TestResponse The returned information from the request
     */
    public function patch(/* string */ $uri, array|string $data = [], array $headers = []): TestResponse
    {
        // Update the PHP Input.
        $original_input = InternalCache::object()->get('php://input');
        InternalCache::object()->set('php://input', $data);
        // Make the request.
        $_SERVER['REQUEST_METHOD'] = 'PATCH';
        $this->prepare($uri, $headers);
        $response = $this->processResponse(parent::patch($uri, [], $headers));
        // Restore the previous input.
        InternalCache::object()->set('php://input', $original_input);
        // Return the response.
        return $response;
    }

    /**
     * Test a HTTP PUT request
     * @param string       $uri     The URI we are accessing.
     * @param array|string $data    The data to pass the request.
     * @param array        $headers Additional headers to pass to our test request.
     * @return TestResponse The returned information from the request
     */
    public function put(/* string */ $uri, array|string $data = [], array $headers = []): TestResponse
    {
        // Update the PHP Input.
        $original_input = InternalCache::object()->get('php://input');
        InternalCache::object()->set('php://input', $data);
        // Make the request.
        $_SERVER['REQUEST_METHOD'] = 'PUT';
        $this->prepare($uri, $headers);
        $response = $this->processResponse(parent::put($uri, [], $headers));
        // Restore the previous input.
        InternalCache::object()->set('php://input', $original_input);
        // Return the response.
        return $response;
    }

    /**
     * Test a HTTP DELETE request
     * @param string $uri     The URI we are accessing.
     * @param array  $data    The data to pass the request.
     * @param array  $headers Additional headers to pass to our test request.
     * @return TestResponse The returned information from the request
     */
    public function delete(/* string */ $uri, array $data = [], array $headers = []): TestResponse
    {
        // Update the PHP Input.
        $original_input = InternalCache::object()->get('php://input');
        InternalCache::object()->set('php://input', $data);
        // Make the request.
        $_SERVER['REQUEST_METHOD'] = 'DELETE';
        $this->prepare($uri, $headers);
        $response = $this->processResponse(parent::delete($uri, [], $headers));
        // Restore the previous input.
        InternalCache::object()->set('php://input', $original_input);
        // Return the response.
        return $response;
    }

    /**
     * Test a HTTP OPTIONS request
     * @param string $uri     The URI we are accessing.
     * @param array  $data    The data to pass the request.
     * @param array  $headers Additional headers to pass to our test request.
     * @return TestResponse The returned information from the request
     */
    public function options(/* string */ $uri, array $data = [], array $headers = []): TestResponse
    {
        $_SERVER['REQUEST_METHOD'] = 'OPTIONS';
        $this->prepare($uri, $headers);
        return $this->processResponse(parent::options($uri, $data, $headers));
    }

    /**
     * Test a HTTP HEAD request
     * @param string $uri     The URI we are accessing.
     * @param array  $headers Additional headers to pass to our test request.
     * @return TestResponse The returned information from the request
     */
    public function head(/* string */ $uri, array $headers = []): TestResponse
    {
        $_SERVER['REQUEST_METHOD'] = 'HEAD';
        $this->prepare($uri, $headers);
        return $this->processResponse(parent::head($uri, $headers));
    }

    /**
     * Some minor refinements to the request methods to better emulate the live environment
     * @param string $uri     The URI we are accessing.
     * @param array  $headers Additional headers passed in to our test request.
     * @return void
     */
    protected function prepare(string $uri, array &$headers): void
    {
        // Restore the per-request benchmark info.
        Benchmark::unitTestRefresh();
        // If we have a $_GET part, add it to the var too.
        $_GET = [];
        if (strpos($uri, '?') !== false) {
            $query = parse_url($uri, PHP_URL_QUERY);
            parse_str($query, $_GET);
        }
        $_REQUEST = array_merge($_REQUEST, $_GET);
        // Determine the appropriate domain for the test call.
        $domain = FrameworkConfig::get('debear.url.subdomains.' . env('APP_SUBDOMAIN'));
        if (isset($headers['X-DeBear-Host'])) {
            $domain = $headers['X-DeBear-Host'];
        }
        $_SERVER['HTTP_HOST'] = $domain;
        if ($this->toggles['SERVER_NAME']) {
            $_SERVER['SERVER_NAME'] = $_SERVER['HTTP_HOST'];
        }
        unset($_SERVER['REQUEST_ROUTE']);
        // Process the standard missing fields.
        $_SERVER['REQUEST_URI'] = $uri;
        // SSL considerations.
        FrameworkConfig::set(['debear.url.is_https' => $this->toggles['ssl']]);
        if ($this->toggles['ssl']) {
            $_SERVER['HTTPS'] = 'on';
        } else {
            unset($_SERVER['HTTPS']);
        }
    }

    /**
     * Set or toggle a per-test customisable setting
     * @param string  $key   Toggle key to update.
     * @param boolean $value Over-riding value. (Optional).
     * @return void
     */
    protected function toggleConfig(string $key, ?bool $value = null): void
    {
        $this->toggles[$key] = ($value ?? !$this->toggles[$key]);
    }

    /**
     * Return an amended test response object that includes our overrides
     * @param BaseTestResponse $response The raw, Laravel, test response.
     * @return TestResponse Our internal representation of the the response
     */
    protected function processResponse(BaseTestResponse $response): TestResponse
    {
        // Reset some internal objects between tests.
        InternalCache::object()->empty();
        $_REQUEST = $_GET = $_POST = [];
        FrameworkConfig::set($this->config_orig);
        return (new TestResponse($response));
    }
}
