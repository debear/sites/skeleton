<?php

namespace Tests\PHPUnit\Base;

use DeBear\Repositories\InternalCache;

class UnitTestCase extends BaseTestCase
{
    /**
     * Setup the Laravel app for us to use
     * @return void
     */
    protected function setUp(): void
    {
        // Load the Laravel app.
        $this->createApplication();
        // Load the standard test setup methods.
        parent::setUp();
        // Reset any routing.
        unset($_SERVER['REQUEST_ROUTE']);
        // Empty the internal cache between tests.
        InternalCache::object()->empty();
    }
}
