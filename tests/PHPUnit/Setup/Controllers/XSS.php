<?php

namespace DeBear\Http\Controllers\CI;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\Request;
use DeBear\Http\Controllers\Controller;
use DeBear\Repositories\InternalCache;
use DeBear\ORM\Skeleton\CMSContent as CMSContentORM;
use DeBear\Models\Skeleton\CMSContent as CMSContentModel;

class XSS extends Controller
{
    /**
     * Form for users to update their details
     * @param string $method The method by which the content is passed - POST or php://input.
     * @return Response The relevant (valid) response, depending on the Accept header
     */
    public function index(string $method): Response
    {
        // Get the content.
        if ($method == 'post') {
            $content = Request::post('content');
        } else {
            $php_input = InternalCache::object()->get('php://input');
            $content = $php_input['content'] ?? '** MISSING INPUT **';
        }

        // Put through our database models.
        $default = [
            'site' => 'ci',
            'page' => 'xss-view',
            'order' => 10,
            'content' => $content,
        ];
        $model_in = CMSContentModel::create(array_merge($default, ['section' => '"model"']));
        $model_out = CMSContentModel::where('content_id', $model_in->content_id)->first();
        $orm_in = CMSContentORM::create(array_merge($default, ['section' => '"orm"']));
        $orm_out = CMSContentORM::query()->where('content_id', $orm_in->content_id)->get();

        // Return a view.
        return response(view('skeleton.dev.ci.xss', compact([
            'content',
            'model_in',
            'model_out',
            'orm_in',
            'orm_out',
        ])));
    }
}
