<?php

namespace DeBear\Http\Controllers\CI;

use Illuminate\Http\Response;
use Illuminate\Http\RedirectResponse;
use DeBear\Http\Controllers\Controller;
use DeBear\Http\Modules\Forms as FormsModule;

class Forms extends Controller
{
    /**
     * Form for users to update their details
     * @param string $config The path to the configuration path containing the test specification.
     * @return Response|RedirectResponse The relevant response, which could be a redirect if not a valid request
     */
    public function index(string $config): Response|RedirectResponse
    {
        // Process and render.
        $obj = new FormsModule("ci/$config");
        return $obj->run();
    }
}
