<?php

namespace DeBear\Http\Controllers\CI;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use DeBear\Http\Controllers\Controller;

class AuthPolicies extends Controller
{
    /**
     * A basic route which returns on OK message
     * @param Request $req The original request made.
     * @return Response|JsonResponse The relevant (valid) response, depending on the Accept header
     */
    public function index(Request $req): Response|JsonResponse
    {
        if ($req->header('Accept', '*/*') == 'application/json') {
            return response()->json(['status' => 'OK']);
        }
        return response('OK');
    }
}
