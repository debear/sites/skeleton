<?php

return [
    // Default values to load in to the form.
    'defaults' => [
        'user' => [
            'id',
        ],
    ],

    // Visual steps.
    'steps' => [
        'render',
        'confirm',
    ],

    // How to handle the fields.
    'fields' => [
        'user_id' => [
            'required' => true,
            'minlength' => 5,
            'maxlength' => 20,
        ],
        'forename' => [
            'required' => true,
            'maxlength' => 15,
        ],
        'surname' => [
            'required' => true,
            'maxlength' => 20,
        ],
        'terms' => [
            'required' => true,
            'type' => 'checkbox',
        ],
        'anumber' => [
            'min' => 0,
            'max' => 9,
        ],
    ],

    // Where to go upon completion.
    'next' => [
        'redirect' => '/forms/complete',
        'message' => 'Test Complete',
    ],
];
