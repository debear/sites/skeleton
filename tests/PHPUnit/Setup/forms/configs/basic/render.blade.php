<h1>Basic Form</h1>

{{-- Username --}}
@php
    $form->validateField('user_id');
@endphp
<ul class="inline_list field user_id clearfix">
    <li class="label {!! !$form->isFieldValid('user_id') ? 'error' : '' !!}" id="user_id_label">
        <label for="user_id">Username:</label>&nbsp;
    </li>
    <li class="value">
        <input class="textbox" type="text" id="user_id" name="user_id" value="{!! $form->getValue('user_id') !!}" autocomplete="username" {!! $form->getElementAttributes('user_id') !!} {!! $form->addTabindex() !!}>
    </li>
    <li class="status {!! $form->formFieldStatus('user_id') !!}" id="user_id_status"></li>
    <li class="info">Must be between 5 and 20 characters</li>
    <li class="error details {!! $form->isFieldValid('user_id') ? 'hidden' : '' !!}" id="user_id_error">{!! !$form->isFieldValid('user_id') ? $form->getFieldError('user_id') : '' !!}</li>
</ul>

{{-- First Name --}}
@php
    $form->validateField('forename');
@endphp
<ul class="inline_list field forename clearfix">
    <li class="label {!! !$form->isFieldValid('forename') ? 'error' : '' !!}" id="forename_label">
        <label for="forename">Forename:</label>&nbsp;
    </li>
    <li class="value">
        <input class="textbox" type="text" id="forename" name="forename" value="{!! $form->getValue('forename') !!}" autocomplete="given-name" {!! $form->getElementAttributes('forename') !!} {!! $form->addTabindex() !!}>
    </li>
    <li class="status {!! $form->formFieldStatus('forename') !!}" id="forename_status"></li>
    <li class="info">Must be no more than 15 characters</li>
    <li class="error details {!! $form->isFieldValid('forename') ? 'hidden' : '' !!}" id="forename_error">{!! !$form->isFieldValid('forename') ? $form->getFieldError('forename') : '' !!}</li>
</ul>

{{-- Surname --}}
@php
    $form->validateField('surname');
@endphp
<ul class="inline_list field surname clearfix">
    <li class="label {!! !$form->isFieldValid('surname') ? 'error' : '' !!}" id="surname_label">
        <label for="surname">Surname:</label>&nbsp;
    </li>
    <li class="value">
        <input class="textbox" type="text" id="surname" name="surname" value="{!! $form->getValue('surname') !!}" autocomplete="family-name" {!! $form->getElementAttributes('surname') !!} {!! $form->addTabindex() !!}>
    </li>
    <li class="status {!! $form->formFieldStatus('surname') !!}" id="surname_status"></li>
    <li class="info">Must be no more than 20 characters</li>
    <li class="error details {!! $form->isFieldValid('surname') ? 'hidden' : '' !!}" id="surname_error">{!! !$form->isFieldValid('surname') ? $form->getFieldError('surname') : '' !!}</li>
</ul>

{{-- Terms --}}
@php
    $form->validateField('terms');
@endphp
<ul class="inline_list field terms clearfix">
    <li class="label {!! !$form->isFieldValid('terms') ? 'error' : '' !!}" id="terms_label">
        <input type="hidden" id="terms__cb" name="terms__cb" value="1">
        <input type="checkbox" id="terms" name="terms" {!! $form->getElementAttributes('terms') !!} {!! $form->getValue('terms') ? 'checked="checked"' : '' !!}>
        <label for="terms" data-id="terms" {!! $form->addTabindex() !!}>I agree to the {!! FrameworkConfig::get('debear.names.site') !!}</label> <a id="terms_show">Terms of Use</a>.
        <em>(Click the link to view and review the Terms of Use).</em>
    </li>
    <li class="error details {!! $form->isFieldValid('terms') ? 'hidden' : '' !!}" id="terms_error">{!! !$form->isFieldValid('terms') ? $form->getFieldError('terms') : '' !!}</li>
</ul>

{{-- A Number --}}
@php
    $form->validateField('anumber');
@endphp
<ul class="inline_list field anumber clearfix">
    <li class="label {!! !$form->isFieldValid('anumber') ? 'error' : '' !!}" id="anumber_label">
        <label for="anumber">A Number:</label>&nbsp;
    </li>
    <li class="value">
        <input class="textbox" type="number" id="anumber" name="anumber" value="{!! $form->getValue('anumber') !!}" {!! $form->getElementAttributes('anumber') !!} {!! $form->addTabindex() !!}>
    </li>
    <li class="status {!! $form->formFieldStatus('anumber') !!}" id="anumber_status"></li>
    <li class="info">Must be between 0 and 9</li>
    <li class="error details {!! $form->isFieldValid('anumber') ? 'hidden' : '' !!}" id="anumber_error">{!! !$form->isFieldValid('anumber') ? $form->getFieldError('anumber') : '' !!}</li>
</ul>
