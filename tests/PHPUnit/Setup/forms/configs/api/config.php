<?php

return [
    // How to handle the fields.
    'fields' => [
        'user_id' => [
            'required' => true,
            'minlength' => 5,
            'maxlength' => 20,
        ],
        'forename' => [
            'required' => true,
            'maxlength' => 15,
        ],
        'surname' => [
            'required' => true,
            'maxlength' => 20,
        ],
        'terms' => [
            'required' => true,
            'type' => 'checkbox',
        ],
        'anumber' => [
            'min' => 0,
            'max' => 9,
        ],
    ],

    // Where and how to save the data.
    'mapping' => [
        'DeBear\Models\Skeleton\User' => [
            'key' => [
                'user_id',
            ],
            'form_fields' => [
                'user_id',
                'forename',
                'surname',
            ],
            'actions' => [
                'success' => [
                    'audit' => [
                        'type' => 'account_update',
                        'summary' => 'Record processed',
                        'fields' => ['forename'],
                    ],
                ],
            ],
        ],
    ],
];
