@php
    $content = \DeBear\Models\Skeleton\CMSContent::where([
        'site' => 'ci',
        'page' => 'xss-form',
        'section' => '"forms"',
    ])->first();
@endphp
ID: {!! isset($content) ? ($content->content_id ?? '(null)') : '(unset)' !!}
Order: {!! isset($content) ? ($content->order ?? '(null)') : '(unset)' !!}
Content: {!! isset($content) ? ($content->content ?? '(null)') : '(unset)' !!}
