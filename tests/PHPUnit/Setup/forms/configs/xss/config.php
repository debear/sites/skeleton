<?php

return [
    // Visual steps.
    'steps' => [
        'render',
    ],

    // How to handle the fields.
    'fields' => [
        'content' => [
            'required' => true,
            'minlength' => 5,
        ],
    ],

    // Where and how to save the data.
    'mapping' => [
        '\DeBear\Models\Skeleton\CMSContent' => [
            'key' => [
                'site',
                'page',
                'section',
                'order',
            ],
            'form_fields' => [
                'content',
            ],
            'fixed_fields' => [
                'site' => [
                    'string' => 'ci',
                ],
                'page' => [
                    'string' => 'xss-form',
                ],
                'section' => [
                    'string' => '"forms"',
                ],
                'order' => [
                    'string' => 10,
                ],
            ],
        ],
    ],
];
