@php
    // Auto-fail the form if we find suspicious activity
    $form->validateSuspiciousActivity('security', 'anumber', 'Unable to verify details', [
        'user_id' => 'test_disabled',
    ]);
@endphp
<h1>Suspicious Check Form</h1>

{{-- A Number --}}
@php
    $form->validateField('anumber');
@endphp
<ul class="inline_list field anumber clearfix">
    <li class="label {!! !$form->isFieldValid('anumber') ? 'error' : '' !!}" id="anumber_label">
        <label for="anumber">A Number:</label>&nbsp;
    </li>
    <li class="value">
        <input class="textbox" type="number" id="anumber" name="anumber" value="{!! $form->getValue('anumber') !!}" {!! $form->getElementAttributes('anumber') !!} {!! $form->addTabindex() !!}>
    </li>
    <li class="status {!! $form->formFieldStatus('anumber') !!}" id="anumber_status"></li>
    <li class="info">Must be between 0 and 9</li>
    <li class="error details {!! $form->isFieldValid('anumber') ? 'hidden' : '' !!}" id="anumber_error">{!! !$form->isFieldValid('anumber') ? $form->getFieldError('anumber') : '' !!}</li>
</ul>

{{-- Terms --}}
@php
    $form->validateField('terms');
@endphp
<ul class="inline_list field terms clearfix">
    <li class="label {!! !$form->isFieldValid('terms') ? 'error' : '' !!}" id="terms_label">
        <input type="hidden" id="terms__cb" name="terms__cb" value="1">
        <input type="checkbox" id="terms" name="terms" {!! $form->getElementAttributes('terms') !!} {!! $form->getValue('terms') ? 'checked="checked"' : '' !!}>
        <label for="terms" data-id="terms" {!! $form->addTabindex() !!}>I agree to the {!! FrameworkConfig::get('debear.names.site') !!}</label> <a id="terms_show">Terms of Use</a>.
        <em>(Click the link to view and review the Terms of Use).</em>
    </li>
    <li class="error details {!! $form->isFieldValid('terms') ? 'hidden' : '' !!}" id="terms_error">{!! !$form->isFieldValid('terms') ? $form->getFieldError('terms') : '' !!}</li>
</ul>
