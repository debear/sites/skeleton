<?php

return [
    // Visual steps.
    'steps' => [
        'render',
    ],

    // How to handle the fields.
    'fields' => [
        'anumber' => [
            'min' => 0,
            'max' => 9,
        ],
        'terms' => [
            'required' => true,
            'type' => 'checkbox',
        ],
    ],

    // Where to go upon completion.
    'next' => [
        'redirect' => '/forms/complete',
        'message' => 'Test Complete',
    ],
];
