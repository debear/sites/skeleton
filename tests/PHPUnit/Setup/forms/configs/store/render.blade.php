<h1>User Creation Form</h1>

{{-- Username --}}
@php
    $form->validateField('user_id');
@endphp
<ul class="inline_list field user_id clearfix">
    <li class="label {!! !$form->isFieldValid('user_id') ? 'error' : '' !!}" id="user_id_label">
        <label for="user_id">Username:</label>&nbsp;
    </li>
    <li class="value">
        <input class="textbox" type="text" id="user_id" name="user_id" value="{!! $form->getValue('user_id') !!}" autocomplete="username" {!! $form->getElementAttributes('user_id') !!} {!! $form->addTabindex() !!}>
    </li>
    <li class="status {!! $form->formFieldStatus('user_id') !!}" id="user_id_status"></li>
    <li class="info">Must be between 5 and 20 characters</li>
    <li class="error details {!! $form->isFieldValid('user_id') ? 'hidden' : '' !!}" id="user_id_error"{!! !$form->isFieldValid('user_id') ? $form->getFieldError('user_id') : '' !!}</li>
</ul>

{{-- Email --}}
@php
    $form->validateField('email');
@endphp
<ul class="inline_list field email clearfix">
    <li class="label {!! !$form->isFieldValid('email') ? 'error' : '' !!}" id="email_label">
        <label for="email">Email Address:</label>&nbsp;
    </li>
    <li class="value">
        <input class="textbox" type="text" id="email" name="email" value="{!! $form->getValue('email') !!}" autocomplete="email" {!! $form->getElementAttributes('email') !!} {!! $form->addTabindex() !!} data-email="true">
    </li>
    <li class="status {!! $form->formFieldStatus('email') !!}" id="email_status"></li>
    <li class="info">Only used to contact you regarding use of the site</li>
    <li class="error details {!! $form->isFieldValid('email') ? 'hidden' : '' !!}" id="email_error"{!! !$form->isFieldValid('email') ? $form->getFieldError('email') : '' !!}</li>
</ul>

{{-- First Name --}}
@php
    $form->validateField('forename');
@endphp
<ul class="inline_list field forename clearfix">
    <li class="label {!! !$form->isFieldValid('forename') ? 'error' : '' !!}" id="forename_label">
        <label for="forename">Forename:</label>&nbsp;
    </li>
    <li class="value">
        <input class="textbox" type="text" id="forename" name="forename" value="{!! $form->getValue('forename') !!}" autocomplete="given-name" {!! $form->getElementAttributes('forename') !!} {!! $form->addTabindex() !!}>
    </li>
    <li class="status {!! $form->formFieldStatus('forename') !!}" id="forename_status"></li>
    <li class="info">Must be no more than 15 characters</li>
    <li class="error details {!! $form->isFieldValid('forename') ? 'hidden' : '' !!}" id="forename_error"{!! !$form->isFieldValid('forename') ? $form->getFieldError('forename') : '' !!}</li>
</ul>

{{-- Surname --}}
@php
    $form->validateField('surname');
@endphp
<ul class="inline_list field surname clearfix">
    <li class="label {!! !$form->isFieldValid('surname') ? 'error' : '' !!}" id="surname_label">
        <label for="surname">Surname:</label>&nbsp;
    </li>
    <li class="value">
        <input class="textbox" type="text" id="surname" name="surname" value="{!! $form->getValue('surname') !!}" autocomplete="family-name" {!! $form->getElementAttributes('surname') !!} {!! $form->addTabindex() !!}>
    </li>
    <li class="status {!! $form->formFieldStatus('surname') !!}" id="surname_status"></li>
    <li class="info">Must be no more than 20 characters</li>
    <li class="error details {!! $form->isFieldValid('surname') ? 'hidden' : '' !!}" id="surname_error"{!! !$form->isFieldValid('surname') ? $form->getFieldError('surname') : '' !!}</li>
</ul>

{{-- Display Name --}}
@php
    $form->validateField('display_name');
@endphp
<ul class="inline_list field display_name clearfix">
    <li class="label {!! !$form->isFieldValid('display_name') ? 'error' : '' !!}" id="display_name_label">
        <label for="display_name">Display Name:</label>&nbsp;
    </li>
    <li class="value">
        <input class="textbox" type="text" id="display_name" name="display_name" value="{!! $form->getValue('display_name') !!}" autocomplete="nickname" {!! $form->getElementAttributes('display_name') !!} {!! $form->addTabindex() !!}>
    </li>
    <li class="status {!! $form->formFieldStatus('display_name') !!}" id="display_name_status"></li>
    <li class="info">Must be no more than 20 characters</li>
    <li class="error details {!! $form->isFieldValid('display_name') ? 'hidden' : '' !!}" id="display_name_error"{!! !$form->isFieldValid('display_name') ? $form->getFieldError('display_name') : '' !!}</li>
</ul>

{{-- Date of Birth --}}
@php
    $form->validateField('dob');
    $date_field = new DateField('dob', Arrays::merge(
        [
            'form' => $form,
            'value' => $form->getValue('dob'),
        ],
        $form->getElementValidation('dob')
    ));
@endphp
<ul class="inline_list field dob clearfix">
    <li class="label {!! !$form->isFieldValid('dob') ? 'error' : '' !!}" id="dob_label">
        <label for="dob">Date of Birth:</label>&nbsp;
    </li>
    <li class="value">
        {!! $date_field->render() !!}
    </li>
    <li class="status {!! $form->formFieldStatus('dob') !!}" id="dob_status"></li>
    <li class="info">Please enter in Day-Month-Year format</li>
    <li class="error details {!! $form->isFieldValid('dob') ? 'hidden' : '' !!}" id="dob_error"{!! !$form->isFieldValid('dob') ? $form->getFieldError('dob') : '' !!}</li>
</ul>

{{-- Timezone --}}
@php
    $tz = new Timezones(['geoip' => true]);
    $opt = [];
    foreach ($tz->getTimezones() as $code => $list) {
        // Form a row for the country
        $opt[] = ['label' => "<span class=\"flag flag16_{$list[0]->country_code}\">{$list[0]->country}</span>"];
        // Then each timezone
        foreach ($list as $tz)
            $opt[] = ['id' => $tz->timezone, 'label' => $tz->city];
    }
    $form->validateField('timezone', [ 'opt_list' => array_column($opt, 'id') ]);
    $dropdown = new Dropdown('timezone', $opt, Arrays::merge(
        [
            'form' => $form,
            'select' => '-- Please select your Nearest City --',
            'value' => $form->getValue('timezone'),
        ],
        $form->getElementValidation('timezone')
    ));
@endphp
<ul class="inline_list field timezone clearfix">
    <li class="label {!! !$form->isFieldValid('timezone') ? 'error' : '' !!}" id="timezone_label">
        <label for="timezone">Nearest City:</label>&nbsp;
    </li>
    <li class="value">
        {!! $dropdown->render() !!}
    </li>
    <li class="status {!! $form->formFieldStatus('timezone') !!}" id="timezone_status"></li>
    <li class="info">Used to establish the time where you are</li>
    <li class="error details {!! $form->isFieldValid('timezone') ? 'hidden' : '' !!}" id="timezone_error"{!! !$form->isFieldValid('timezone') ? $form->getFieldError('timezone') : '' !!}</li>
</ul>
