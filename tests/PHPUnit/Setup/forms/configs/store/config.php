<?php

return [
    // Visual steps.
    'steps' => [
        'render',
    ],

    // How to handle the fields.
    'fields' => [
        'user_id' => [
            'required' => true,
            'minlength' => 5,
            'maxlength' => 20,
        ],
        'email' => [
            'required' => true,
            'type' => 'email',
        ],
        'forename' => [
            'required' => true,
            'maxlength' => 15,
        ],
        'surname' => [
            'required' => true,
            'maxlength' => 20,
        ],
        'display_name' => [
            'required' => true,
            'maxlength' => 20,
        ],
        'dob' => [
            'required' => true,
            'type' => 'date',
            'min' => '1920-01-01',
            'max' => date('Y-m-d', strtotime('-4 year')),
        ],
        'timezone' => [
            'required' => true,
        ],
    ],

    // Where and how to save the data.
    'mapping' => [
        '\DeBear\Models\Skeleton\User' => [
            'key' => 'user_id',
            'form_fields' => [
                'user_id',
                'email',
                'forename',
                'surname',
                'display_name',
                'dob',
                'timezone',
            ],
            'fixed_fields' => [
                'account_created' => [
                    'timestamp' => 'server_now',
                ],
                'status' => [
                    'string' => 'Unverified',
                ],
            ],
            // Post-processing actions.
            'actions' => [
                'create' => [],
                'update' => [],
                'success' => [],
            ],
        ],
    ],

    // Where to go upon completion.
    'next' => [
        'redirect' => '/forms/complete',
        'message' => 'Test Complete',
    ],
];
