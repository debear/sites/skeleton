<?php

return [
    // Set a fake policy that will fail, so the redirection takes effect.
    'policies' => [
        'list' => 'faux-policy',
        'redirect' => '/elsewhere',
    ],

    // Visual steps.
    'steps' => [
        'render',
    ],

    // How to handle the fields.
    'fields' => [
        'email' => [
            'required' => true,
            'type' => 'email',
        ],
    ],

    // Where to go upon completion.
    'next' => [
        'redirect' => '/complete',
    ],
];
