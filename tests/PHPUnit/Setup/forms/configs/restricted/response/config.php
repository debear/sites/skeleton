<?php

return [
    // Set a fake policy that will fail, so a response will be returned.
    'policies' => [
        'list' => 'faux-policy',
        'response' => 'sendUnauthorised',
    ],

    // Visual steps.
    'steps' => [
        'render',
    ],

    // How to handle the fields.
    'fields' => [
        'email' => [
            'required' => true,
            'type' => 'email',
        ],
    ],

    // Where to go upon completion.
    'next' => [
        'redirect' => '/complete',
    ],
];
