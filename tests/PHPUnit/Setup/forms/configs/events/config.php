<?php

return [
    // Visual steps.
    'steps' => [
        'render',
    ],

    // How to handle the fields.
    'fields' => [
        'anumber' => [
            'min' => 5,
            'max' => 9,
            'required' => true,
        ],
    ],

    // Post-processing actions.
    'actions' => [
        'failure' => [
            'events' => [
                Tests\PHPUnit\Setup\Events\TestFail::class,
            ],
        ],
    ],

    // Where to go upon completion.
    'next' => [
        'redirect' => '/forms/complete',
        'message' => 'Test Complete',
    ],
];
