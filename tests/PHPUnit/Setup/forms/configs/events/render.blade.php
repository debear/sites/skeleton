<h1>Events Form</h1>

{{-- A Number --}}
@php
    $form->validateField('anumber');
@endphp
<ul class="inline_list field anumber clearfix">
    <li class="label {!! !$form->isFieldValid('anumber') ? 'error' : '' !!}" id="anumber_label">
        <label for="anumber">A Number:</label>&nbsp;
    </li>
    <li class="value">
        <input class="textbox" type="number" id="anumber" name="anumber" value="{!! $form->getValue('anumber') !!}" {!! $form->getElementAttributes('anumber') !!} {!! $form->addTabindex() !!}>
    </li>
    <li class="status {!! $form->formFieldStatus('anumber') !!}" id="anumber_status"></li>
    <li class="info">Must be between 5 and 9</li>
    <li class="error details {!! $form->isFieldValid('anumber') ? 'hidden' : '' !!}" id="anumber_error">{!! !$form->isFieldValid('anumber') ? $form->getFieldError('anumber') : '' !!}</li>
</ul>

@php
    $event = session('ci.test-event');
@endphp
@isset($event)
    @php
        $event = session('ci.test-event');
        $event_obj = $event->getFormObject('Test\Class');
        $event_orig = $event->getOriginalData();
        $event_raw = $event->getRawData();
        $event_changed = $event->getChangedData();
    @endphp
    <h3>Validation Info</h3>
    <dl>
        <dt>Object:</dt>
            <dd>{!! json_encode($event_obj) !!}</dd>
        <dt>Original:</dt>
            <dd>{!! json_encode($event_orig) !!}</dd>
        <dt>Raw:</dt>
            <dd>{!! json_encode($event_raw) !!}</dd>
        <dt>Changes:</dt>
            <dd>{!! json_encode($event_changed) !!}</dd>
    </dl>
@endif
