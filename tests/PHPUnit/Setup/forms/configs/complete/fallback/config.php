<?php

return [
    // Default values to load in to the form.
    'defaults' => [
        'user' => [
            'id',
        ],
    ],

    // Visual steps.
    'steps' => [
        'render',
    ],

    // How to handle the fields.
    'fields' => [
        'user_id' => [
            'required' => true,
            'minlength' => 5,
            'maxlength' => 20,
        ],
    ],

    // Where to go upon completion.
    'next' => [
        'message' => 'Test Complete',
    ],
];
