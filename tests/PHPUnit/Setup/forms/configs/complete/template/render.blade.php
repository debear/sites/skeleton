<h1>Complete &racquo; Template Form</h1>

{{-- Username --}}
@php
    $form->validateField('user_id');
@endphp
<ul class="inline_list field user_id clearfix">
    <li class="label {!! !$form->isFieldValid('user_id') ? 'error' : '' !!}" id="user_id_label">
        <label for="user_id">Username:</label>&nbsp;
    </li>
    <li class="value">
        <input class="textbox" type="text" id="user_id" name="user_id" value="{!! $form->getValue('user_id') !!}" autocomplete="username" {!! $form->getElementAttributes('user_id') !!} {!! $form->addTabindex() !!}>
    </li>
    <li class="status {!! $form->formFieldStatus('user_id') !!}" id="user_id_status"></li>
    <li class="info">Must be between 5 and 20 characters</li>
    <li class="error details {!! $form->isFieldValid('user_id') ? 'hidden' : '' !!}" id="user_id_error">{!! !$form->isFieldValid('user_id') ? $form->getFieldError('user_id') : '' !!}</li>
</ul>
