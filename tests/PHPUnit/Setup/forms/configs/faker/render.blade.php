<h1>Faker Form</h1>

{{-- First Name --}}
@php
    $form->validateField('forename');
@endphp
<ul class="inline_list field forename clearfix">
    <li class="label {!! !$form->isFieldValid('forename') ? 'error' : '' !!}" id="forename_label">
        <label for="forename">Forename:</label>&nbsp;
    </li>
    <li class="value">
        <input class="textbox" type="text" id="forename" name="forename" value="{!! $form->getValue('forename') !!}" autocomplete="given-name" {!! $form->getElementAttributes('forename') !!} {!! $form->addTabindex() !!}>
    </li>
    <li class="status {!! $form->formFieldStatus('forename') !!}" id="forename_status"></li>
    <li class="info">Must be no more than 15 characters</li>
    <li class="error details {!! $form->isFieldValid('forename') ? 'hidden' : '' !!}" id="forename_error">{!! !$form->isFieldValid('forename') ? $form->getFieldError('forename') : '' !!}</li>
</ul>
