<?php

return [
    // Visual steps.
    'steps' => [
        'render',
    ],

    // How to handle the fields.
    'fields' => [
        'forename' => [
            'required' => true,
            'maxlength' => 15,
            'faker' => 'firstName',
        ],
    ],

    // Where to go upon completion.
    'next' => [
        'redirect' => '/forms/complete',
        'message' => 'Test Complete',
    ],
];
