#!/bin/bash
# Refine the environment for the PHPUnit tests
dir_tests=$(realpath $(dirname $0)/../..)
dir_root=$(realpath $dir_tests/..)

# Our CI isn't compatible with the centralised storage symlinks
$dir_tests/_scripts/convert-symlinks.sh

# But also some other paths we use within our Unit Tests
mkdir -p $dir_root/../../logs/www/apache
mkdir -p $dir_root/../www/resources/{css,js}/merged
mkdir -p $dir_root/../www/config
echo -e "<?php\n    return ['name' => 'WWW', 'version' => ['breakdown' => ['major' => 0, 'minor' => 0]]];" >$dir_root/../www/config/debear.php
echo -e "<?php\n    return [];" >$dir_root/../www/config/sitemap-_core.php
echo -n "123.456:"`date +'%s'` >$dir_root/../www/VERSION_REV
