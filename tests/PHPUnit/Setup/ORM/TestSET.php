<?php

namespace Tests\PHPUnit\Setup\ORM;

use DeBear\ORM\Base\Instance;

class TestSET extends Instance
{
    /**
     * SET columns and their values
     * @var array
     */
    protected $sets = [
        'set' => ['a', 'b', 'c', 'd'],
    ];
}
