<?php

namespace Tests\PHPUnit\Setup\ORM;

use DeBear\ORM\Base\Instance;

class TestPrePostProcess extends Instance
{
    /**
     * Attributes not to be formatted when representing a row in an API response
     * @var array
     */
    protected $apiFormats = [
        'str' => ['fmt_as_string', 'fmt_int_as_string'],
        'int' => ['fmt_as_int'],
        'float' => ['fmt_as_float'],
        '2dp' => ['fmt_as_2dp'],
        '4dp' => ['fmt_as_4dp'],
        'bool' => ['fmt_as_bool'],
    ];

    /**
     * Perform some preperatory steps before rendering API data
     * @return void
     */
    protected function preProcessAPIData(): void
    {
        foreach (array_keys($this->data) as $i) {
            $this->data[$i]['i'] = $i;
        }
    }

    /**
     * Perform post-processing on the API data for the test model
     * @param array $row The test object data to be post-processed.
     * @return void
     */
    protected function postProcessAPIData(array &$row): void
    {
        $row['c'] = $row['a'] + $row['b'];
    }
}
