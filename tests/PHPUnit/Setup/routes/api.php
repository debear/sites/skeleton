<?php

/*
 * API Unit Test routes (not for live consumption).
 */

Route::prefix('unit-tests/v1.0')->group(function () {
    /*
     * Unit tests.
     */
    Route::any('/base-class', function (\Illuminate\Http\Request $req) {
        return response()->json([
            'success' => true,
            'method' => strtolower($req->method()),
            'input' => DeBear\Repositories\InternalCache::object()->get('php://input'),
            'post' => $req->post(),
        ], 200);
    })->middleware('throttle:unit_tests');

    /*
     * Throttles.
     */
    foreach (array_keys(\Illuminate\Support\Facades\Config::array('debear.api.thresholds')) as $key) {
        Route::get("/throttle/$key", function () {
            return \DeBear\Helpers\API::setStatus(200, 'Test Passed');
        })->middleware("throttle:$key");
    }

    /*
     * Tokens.
     */
    Route::get('/token/valid', function () {
        return \DeBear\Helpers\API::setStatus(200, 'Test Passed');
    })->middleware('auth.token:valid');

    // Scope logical operators.
    Route::get('/token/logic/and', function () {
        return \DeBear\Helpers\API::setStatus(200, 'Test Passed');
    })->middleware('auth.token:valid&invalid');
    Route::get('/token/logic/or', function () {
        return \DeBear\Helpers\API::setStatus(200, 'Test Passed');
    })->middleware('auth.token:valid|invalid');
    Route::get('/token/logic/both', function () {
        return \DeBear\Helpers\API::setStatus(200, 'Test Passed');
    })->middleware('auth.token:valid|invalid&other');
});
