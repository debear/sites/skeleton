<?php

/*
 * Routes for the CI forms tests
 */

Route::match(['get', 'post'], '/forms/{config}', 'CI\Forms@index')
    ->where('config', '(.*)');

/*
 * XSS verification tests
 */

Route::post('/xss/{method}', 'CI\XSS@index')
    ->where('method', '(post|php-input)');

/*
 * Auth policies middleware tests
 */

Route::get('/auth-policy/match', 'CI\AuthPolicies@index')
    ->middleware('auth:guest');
Route::get('/auth-policy/unauthorised', 'CI\AuthPolicies@index')
    ->middleware('auth:user');
Route::get('/auth-policy/forbidden', 'CI\AuthPolicies@index')
    ->middleware('auth:user:admin');
Route::get('/auth-policy/invalid', 'CI\AuthPolicies@index')
    ->middleware('auth:guest|user&user:admin');
