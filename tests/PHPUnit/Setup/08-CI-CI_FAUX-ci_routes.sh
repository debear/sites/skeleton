#!/bin/bash
# Add the symlinks required for CI-only routes
dir_root=$(realpath $(dirname $0)/../../..)

# Controllers
cd $dir_root/app/Http/Controllers
ln -s ../../../tests/PHPUnit/Setup/Controllers CI

# Forms
cd $dir_root/forms
ln -s ../tests/PHPUnit/Setup/forms/configs ci
