<?php

namespace Tests\PHPUnit\Setup\Listeners;

use Tests\PHPUnit\Setup\Events\TestFail;

class TestListener
{
    /**
     * Handle a test form fail
     * @param TestFail $event The object containing information about the dispatched event.
     * @return void
     */
    public function handle(TestFail $event): void
    {
        // Store the object (in a circuituous way) for processing later.
        session()->flash('ci.test-event', $event);
    }
}
