<?php

return [
    // Ensure we're starting from the correct point.
    'DELETE FROM `CMS_CONTENT` WHERE `site` = "ci-exec"',
    // And now add our new data we'll check later.
    'INSERT INTO `CMS_CONTENT` (`site`, `page`, `section`, `order`, `live`, `content`) VALUES '
        . '("ci-exec", "/", "intro", 10, 0, "<h3>Hello, world!</h3>");',
    'UPDATE `CMS_CONTENT` SET `live` = 1 WHERE `site` = "ci-exec";',
];
