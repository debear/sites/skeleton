# DeBear Skeleton

The central code running the core functionality in the suite of DeBear.uk sites. Up until late 2018 it was a homebrew skeleton, with the appropriate components built - by hand - as and when required. However, it was deemed this was not the most beneficial long-term plan and so it was decided to switch to a common framework and after some initial R&D, Laravel was deemed the framework of choice.

## Status

[![Build: 1439](https://img.shields.io/badge/Build-1439-yellow.svg)](https://gitlab.com/debear/skeleton/blob/master/CHANGELOG.md)
[![Framework: Laravel](https://img.shields.io/badge/Framework-Laravel-lightgrey.svg)](https://laravel.com/docs/master)
[![Coding Style: PSR-12](https://img.shields.io/badge/Coding_Style-PSR--12-lightgrey.svg)](https://github.com/php-fig/fig-standards/blob/master/proposed/extended-coding-style-guide.md)
[![Pipeline Status](https://gitlab.com/debear/skeleton/badges/master/pipeline.svg)](https://gitlab.com/debear/skeleton/commits/master)
[![Coverage Report](https://gitlab.com/debear/sites/skeleton/badges/master/coverage.svg)](https://gitlab.com/debear/sites/skeleton/commits/master)
[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)

## Motivation

The initial purpose was to centralise the common components of the first, as well as all future, DeBear.uk sites. Having moved to Laravel, a lot of the same ideas have been preseved but converted to its code structure.

## Authors

* **Thierry Draper** - [@ThierryDraper](https://gitlab.com/thierrydraper)

## Contribute

Please read [CONTRIBUTING.md](https://gitlab.com/debear/skeleton/blob/master/CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## License

This project is made available under the terms of the GNU Affero General Public License v3.0. Please see the [LICENSE](https://gitlab.com/debear/skeleton/blob/master/LICENSE) file for details.

GNU AGPLv3 © [Thierry Draper](https://gitlab.com/thierrydraper)
