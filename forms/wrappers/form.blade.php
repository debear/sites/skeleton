@extends('skeleton.layouts.html')

@section('content')

<form method="post" action="{!! Request::server('REQUEST_URI') !!}" id="frm_{!! str_replace('.', '_', $form_step) !!}" class="validate {!! str_replace('.', '-', $form_step) !!}">
    @csrf
    <input type="hidden" name="form_ref" value="{!! $form->getUniqueReference() !!}">
    @include($form_step)
</form>

@endsection
