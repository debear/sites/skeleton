<?php

namespace DeBear\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Exceptions\RouteException;
use DeBear\Helpers\API;
use DeBear\Helpers\Strings;
use DeBear\ORM\Skeleton\APIToken;
use DeBear\ORM\Skeleton\APITokenScope;
use DeBear\Repositories\Time;

class AuthToken
{
    /**
     * Handle an incoming request.
     * @param Request $request The request from the user being processed.
     * @param Closure $next    How to handle the request.
     * @param string  $scope   The scope of the API token to be validated.
     * @return mixed
     * @throws RouteException When the scope has not been defined correctly.
     */
    public function handle(Request $request, Closure $next, string $scope)
    {
        // Ensure the scope is defined correctly - cannot contain both 'or' and 'and' logic.
        if (strpos($scope, '|') !== false && strpos($scope, '&') !== false) {
            throw new RouteException('The scope cannot cannot contain both "or" and "and" logic');
        }

        // Validate the auth token passed in permits access to the subdomain and scope required.
        $valid = $authorised = false; // Assume invalid until proven otherwise.
        $auth = $request->header('authorization');
        if (substr($auth, 0, 6) == 'Basic ') { // Must be using 'Basic' authorisation.
            $token = base64_decode(substr($auth, 6));
            $token_colon = strpos($token, ':');
            if ($token_colon !== false && $token_colon > 9) { // Implied minimum length.
                $token_user = substr($token, 0, $token_colon);
                $token_pass = Strings::oneWayHash(substr($token, $token_colon + 1), $token_user);
                // Check this token against the database.
                $tbl_token = APIToken::getTable();
                $tbl_scope = APITokenScope::getTable();
                $now = Time::object()->getServerNowFmt();
                $token_details = APIToken::query()
                    ->select(["$tbl_token.user_id", "$tbl_token.name"])
                    ->selectRaw("$tbl_token.token_id IS NOT NULL AS valid_token")
                    ->selectRaw("GROUP_CONCAT(DISTINCT $tbl_scope.scope ORDER BY $tbl_scope.scope) AS valid_scopes")
                    ->leftJoin($tbl_scope, function ($join) use ($tbl_scope, $tbl_token, $now) {
                        return $join->on("$tbl_scope.token_id", '=', "$tbl_token.token_id")
                            ->where("$tbl_scope.domain", '=', FrameworkConfig::get('debear.url.sub'))
                            ->where("$tbl_scope.date_active", '<=', $now)
                            ->whereRaw("IFNULL($tbl_scope.date_revoked, ?) >= ?", [$now, $now]);
                    })->where([
                        ["$tbl_token.token", '=', $token_user],
                        ["$tbl_token.password", '=', $token_pass],
                        ["$tbl_token.date_active", '<=', $now],
                    ])->whereRaw("IFNULL($tbl_token.date_revoked, ?) >= ?", [$now, $now])
                    ->get()
                    ->loadSecondary(['user']);
                // Use this to set our status flags.
                $valid = (bool)$token_details->valid_token;
                $authorised = $valid && $token_details->isScopeValid($scope);
            }
        }
        // Return error states, if appropriate.
        if (!$valid) {
            return API::sendUnauthorised('No valid API key supplied');
        } elseif (!$authorised) {
            return API::sendForbidden('API key does not have access to this resource');
        }

        // Proceed with the rest of the processing.
        if (isset($token_details)) {
            // This should exist, but wrapped in an isset() to be on the safe side, as it's defined inside a few if's.
            $token_details->setupInternalObject();
        }
        return $next($request);
    }
}
