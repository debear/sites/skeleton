<?php

namespace DeBear\Http\Middleware;

class ProcessRequestWeb extends ProcessRequest
{
    /**
     * The route type we are covering
     * @var string
     */
    protected $mode = 'web';
}
