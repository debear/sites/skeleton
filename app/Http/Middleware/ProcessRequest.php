<?php

namespace DeBear\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Support\Facades\Request;
use Illuminate\Http\Request as HttpRequest;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Http\RedirectResponse as HttpRedirectResponse;
use DeBear\Helpers\Browser;
use DeBear\Helpers\Benchmark;
use DeBear\Helpers\CookiePreferences;
use DeBear\Helpers\HTML;
use DeBear\Helpers\HTTP;
use DeBear\Helpers\Policies;
use DeBear\Helpers\SessionAggregation;
use DeBear\Helpers\Strings;
use DeBear\Models\Skeleton\User;
use DeBear\Repositories\BugReporting;
use DeBear\Repositories\GeoIP;
use DeBear\Repositories\Logging;
use DeBear\Repositories\InternalCache;
use DeBear\Repositories\SocialMedia;
use DeBear\Repositories\SuspiciousActivity;
use DeBear\Repositories\Time;

class ProcessRequest
{
    /**
     * Handle an incoming request.
     * @param HttpRequest $request The request from the user being processed.
     * @param Closure     $next    How to handle the request.
     * @return mixed
     */
    public function handle(HttpRequest $request, Closure $next)
    {
        /*
         * Pre-processing
         */
        $response = null;
        if (!$this->isCoreOnly()) {
            Benchmark::start('pre-process');
            $response = $this->preProcess($request);
            Benchmark::stop('pre-process');
        }

        /*
         * Processing, unless something (probably an error) was returned as part of the pre-processing?
         */
        if (!isset($response)) {
            Benchmark::start('process');
            $response = $next($request);
            Benchmark::stop('process');
        }

        /*
         * Post-processing
         */
        if (!$this->isCoreOnly() && is_object($response) && ($response instanceof HttpResponse)) {
            Benchmark::start('post-process');
            $this->postProcess($request, $response);
            Benchmark::stop('post-process');
        }

        // Tidy up our objects just before we end.
        if ($this->isWeb()) {
            Benchmark::start('session:aggregate');
            SessionAggregation::process();
            Benchmark::stop('session:aggregate');
        }

        /*
         * Return
         */
        return $response;
    }

    /**
     * Perform the relevant pre-processing steps
     * @param HttpRequest $request The request from the user being processed.
     * @return HttpResponse|HttpRedirectResponse|null On success, returns null, otherwise some form of Response object
     * to replace the requested response
     */
    protected function preProcess(HttpRequest $request): HttpResponse|HttpRedirectResponse|null
    {
        if ($this->isWeb()) {
            $this->processFixSERVER();
        }
        $this->processFixEnv();
        $this->processSetupVars();
        $this->processSetupObjects();
        if ($this->isWeb()) {
            // Consider if we need a redirect.
            $redirect = $this->processRedirectChecks();
            if (isset($redirect)) {
                return $redirect;
            }
            // Other web updates.
            $this->processMisc();
        }

        // All good, so return null to indicate success (that we haven't provided an alternative response).
        return null;
    }

    /**
     * Fix some (potentially missing) PHP env vars
     * @return void
     */
    protected function processFixSERVER(): void
    {
        $_SERVER['HTTP_USER_AGENT'] = ($_SERVER['HTTP_USER_AGENT'] ?? 'Unknown Browser');
        $_SERVER['REMOTE_ADDR'] = ($_SERVER['REMOTE_ADDR'] ?? '0.0.0.0');
        $_SERVER['SERVER_NAME'] = ($_SERVER['SERVER_NAME'] ?? preg_replace('/\:\d+$/', '', $_SERVER['HTTP_HOST']));
        $_SERVER['REQUEST_SCHEME'] = ($_SERVER['REQUEST_SCHEME'] ?? 'http');
    }

    /**
     * Turn error reporting on/off depending on development mode
     * @return void
     */
    protected function processFixEnv(): void
    {
        // By default (and failsafe), everything is off, however, on non-live we want to increase the detail.
        ini_set('display_errors', HTTP::isLive() ? 'Off' : 'On');
        error_reporting(HTTP::isLive() ? 0 : E_ALL); // 0 == NONE.
    }

    /**
     * Absorb the config for the appropriate site
     * @return void
     */
    protected function processSetupVars(): void
    {
        // Parse php://input into an object (if it is one).
        if (!InternalCache::object()->cacheKeyExists('php://input')) {
            $php_input = !HTTP::isTest() || !InternalCache::object()->cacheKeyExists('raw:php://input')
                ? trim(file_get_contents('php://input'))
                : InternalCache::object()->get('raw:php://input');
            if (in_array(substr($php_input, 0, 1), ['[','{'])) {
                $php_input_cnv = json_decode($php_input, true);
                if (is_array($php_input_cnv)) {
                    $php_input = $this->cleanRequestVar($php_input_cnv);
                }
            }
            InternalCache::object()->set('php://input', $php_input);
        }

        // Loop through special PHP arrays and variables making the contents DB safe.
        $cleaned = [
            'get' => $this->cleanRequestVar($_GET),
            'post' => $this->cleanRequestVar($_POST),
            'cookie' => $this->cleanRequestVar($_COOKIE),
            'server' => $this->cleanRequestVar($_SERVER),
        ];
        // Update within the Laravel variables as well as the standard PHP Super Globals.
        Request::getFacadeRoot()->request->replace($cleaned['get']);
        Request::getFacadeRoot()->request->add($cleaned['post']);
        Request::getFacadeRoot()->query->replace($cleaned['get']);
        Request::getFacadeRoot()->cookies->replace($cleaned['cookie']);
        Request::getFacadeRoot()->server->replace($cleaned['server']);
        Request::getFacadeRoot()->overrideGlobals();
    }

    /**
     * Clean an input array, making its content database (and display) safe
     * @param array $array The array to be cleaned.
     * @return array The cleaned array
     */
    protected function cleanRequestVar(array $array): array
    {
        return (isset($array) && is_array($array) && sizeof($array) ? Strings::encodeInput($array) : $array);
    }

    /**
     * Start our internal objects
     * @return void
     */
    protected function processSetupObjects(): void
    {
        // Date management.
        App::singleton(Time::class);
        App::make(Time::class)->setup();
        // Start our bug reporting handlers.
        App::singleton(BugReporting::class);
        // Build GeoIP info.
        App::singleton(GeoIP::class);

        // Session stuff.
        if ($this->isWeb()) {
            SessionAggregation::prepare();
            CookiePreferences::load();
        }

        // Setup our user object.
        if ($this->isWeb()) {
            User::setup();
        }

        // Social media info to display in the footer.
        if ($this->isWeb()) {
            App::singleton(SocialMedia::class);
            $social = App::make(SocialMedia::class);
            if (FrameworkConfig::get('debear.social.twitter.test')) {
                $social->setup(FrameworkConfig::get('debear.social.twitter.creds.test'));
            } elseif (FrameworkConfig::get('debear.social.twitter.enabled')) {
                $social->setup(FrameworkConfig::get('debear.social.twitter.creds.live'));
            }
        }
    }

    /**
     * Redirect non HTTPS requests
     * @return HttpResponse|HttpRedirectResponse|null On success, returns null, otherwise some form of Response object
     * to replace the requested response
     */
    protected function processRedirectChecks(): HttpResponse|HttpRedirectResponse|null
    {
        // Request is HTTP and not HTTPS.
        $isHTTP = (!FrameworkConfig::get('debear.url.is_https')
            && FrameworkConfig::get('debear.url.redirect_non_https'));
        if ($isHTTP) {
            $redirect = 'https://' . Request::server('HTTP_HOST') . Request::server('REQUEST_URI');
            Logging::add('Redirect', "Redirecting non-HTTPS request to '$redirect'");
            return HTTP::redirectPage($redirect, 308);
        }

        // Check the user's session matches what we're expecting, as if not it could be a session hijack.
        $userAgentChange = (session()->has('user.agent') && session('user.agent') != Browser::internalHash());
        if ($userAgentChange) {
            Logging::add('Security', 'Logging user out due to mis-matching User Agent hash');
            App::make(SuspiciousActivity::class)->record('security', 'User Agent mis-match', true, [
                'user_id' => User::object()->user_id ?? '**Unknown**',
            ]);
            User::doLogout();
            // Warn user and flag as a 403.
            return response()->view('skeleton.errors.sc0', [], 403);
        }

        // Falling back must use the (www.)debear rather than the entered fallback.debear.
        $isFallback = (FrameworkConfig::get('debear.url.sub') == 'www'
            && FrameworkConfig::get('debear.url.subraw') != FrameworkConfig::get('debear.url.sub'));
        if ($isFallback) {
            $redirect = 'https://'
                . substr(Request::server('HTTP_HOST'), strlen(FrameworkConfig::get('debear.url.subraw')) + 1)
                . Request::server('REQUEST_URI');
            Logging::add(
                'Redirect',
                "Redirecting fallback request from '" . Request::server('HTTP_HOST') . "' to '$redirect'"
            );
            return HTTP::redirectPage($redirect, 308);
        }

        // Disabled sub-site is the equivalent of a 404, unsetting the "invalid" section code.
        $subsite_code = FrameworkConfig::get('debear.section.code');
        $invalidSubsite = ($subsite_code && !FrameworkConfig::get("debear.subsites.$subsite_code.enabled"));
        if ($invalidSubsite) {
            FrameworkConfig::set(['debear.section.code' => '', 'debear.section.endpoint' => '']);
            return response()->view('skeleton.errors.404', [], 404);
        }

        // User needs to change their password to comply with the password policy.
        $passwordChange = (
            User::object()->isLoggedIn() && Policies::match('state:password:fails-policy')
            && (Request::server('REQUEST_URI') != '/logout')
            && ((FrameworkConfig::get('debear.url.sub') != 'my')
                || (Request::server('REQUEST_URI') != '/account/reset'))
        );
        if ($passwordChange) {
            return HTTP::redirectPage('https:' . HTTP::buildDomain('my') . '/account/reset', 307);
        }

        // If we're here, no redirects are needed.
        return null;
    }

    /**
     * Build up the breadcrumbs from the URL
     * @return void
     */
    protected function processMisc(): void
    {
        $regexp = '/\/(error|sitemap|terms|privacy_policy|manage|manifest.json)\.php$/';
        $raw = explode('/', Request::server(
            preg_match($regexp, Request::server('SCRIPT_NAME')) ? 'REQUEST_URI' : 'SCRIPT_NAME'
        ));
        FrameworkConfig::set([
            'debear.content.breadcrumbs.raw' => $raw,
            'debear.content.breadcrumbs.parsed' => HTML::formatBreadcrumbs(array_slice($raw, 1, sizeof($raw) - 2)),
        ]);

        // Customise Blade views to include checking our forms path.
        view()->addLocation(FrameworkConfig::get('debear.dirs.forms'));
    }

    /**
     * Perform the relevant post-processing steps
     * @param HttpRequest  $request  The request from the user being processed.
     * @param HttpResponse $response The response to send to the browser.
     * @return void
     */
    protected function postProcess(HttpRequest $request, HttpResponse $response): void
    {
        // Some default headers to be sent.
        HTTP::securityHeaders($response);
    }

    /**
     * Determine if we are operating on web routes (which includes 'core' requests)
     * @return boolean That the middleware is being applied to web routes, not API
     */
    protected function isWeb(): bool
    {
        return ($this->mode != 'api');
    }

    /**
     * Determine if we are operating in core (or mini) mode
     * @return boolean That the middleware is applying the core processing only
     */
    protected function isCoreOnly(): bool
    {
        return ($this->mode == 'core');
    }
}
