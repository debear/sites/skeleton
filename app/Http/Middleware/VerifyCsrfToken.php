<?php

namespace DeBear\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     * @var array
     */
    protected $except = [
        '/report-uri/csp',
        '/report-uri/ct',
        '/report-uri/php',
        '/report-uri/js',
    ];
}
