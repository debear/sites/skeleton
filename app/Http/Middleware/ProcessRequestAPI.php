<?php

namespace DeBear\Http\Middleware;

class ProcessRequestAPI extends ProcessRequest
{
    /**
     * The route type we are covering
     * @var string
     */
    protected $mode = 'api';
}
