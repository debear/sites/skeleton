<?php

namespace DeBear\Http\Middleware;

class ProcessRequestCore extends ProcessRequest
{
    /**
     * The route type we are covering
     * @var string
     */
    protected $mode = 'core';
}
