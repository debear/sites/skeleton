<?php

namespace DeBear\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use DeBear\Helpers\HTTP;
use DeBear\Helpers\Policies;
use DeBear\Exceptions\RouteException;

class AuthPolicies
{
    /**
     * Handle an incoming request.
     * @param Request $request The request from the user being processed.
     * @param Closure $next    How to handle the request.
     * @param string  $policy  Policy rule(s) for this request.
     * @return mixed
     * @throws RouteException When the policy has not been defined correctly.
     */
    public function handle(Request $request, Closure $next, string $policy)
    {
        // Ensure the scope is defined correctly - cannot contain both 'or' and 'and' logic.
        // This logic is an up-front "future plan" and may end up being refined.
        if (strpos($policy, '|') !== false && strpos($policy, '&') !== false) {
            throw new RouteException('The policy list cannot cannot contain both "or" and "and" logic');
        }

        // Break down our string into components.
        $split_char = (strpos($policy, '|') !== false ? '|' : '&');
        $list = explode($split_char, $policy);
        $match_all = ($split_char == '&');

        // If we match, we can proceed with the request.
        if (Policies::match($list, $match_all)) {
            return $next($request);
        }
        // We have an error, but may want non-standard response.
        $ret = ($request->header('Accept', '*/*') == 'application/json' ? ['success' => false] : null);
        // How we handle the error is dependent on what we were asked.
        if (Policies::match('guest')) {
            // User must authenticate first.
            return HTTP::sendUnauthorised($ret);
        }
        // If authenticated, they do not have access.
        return HTTP::sendForbidden($ret);
    }
}
