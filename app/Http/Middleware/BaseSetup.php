<?php

namespace DeBear\Http\Middleware;

use Closure;
use Illuminate\Http\Request as HttpRequest;
use Illuminate\Http\Response as HttpResponse;
use DeBear\Helpers\Benchmark;
use DeBear\Helpers\Config;
use DeBear\Repositories\DatabaseLogging;
use DeBear\Repositories\Logging;

class BaseSetup
{
    /**
     * Handle an incoming request.
     * @param HttpRequest $request The request from the user being processed.
     * @param Closure     $next    How to handle the request.
     * @return mixed
     */
    public function handle(HttpRequest $request, Closure $next)
    {
        // Load the config for this site / subsite.
        Config::loadSite();
        // Consider this the start point of our script.
        Benchmark::scriptStart();
        // Enable our database logging.
        DatabaseLogging::setup();
        // Logging.
        Logging::setup();

        // Core processing.
        Benchmark::start('handle');
        $response = $next($request);
        Benchmark::stop('handle');

        // And then end with all our performance reporting.
        Benchmark::start('report:database');
        DatabaseLogging::report();
        Benchmark::stop('report:database');
        Benchmark::scriptEnd();
        // Write back to the content (if applicable).
        if (is_object($response) && ($response instanceof HttpResponse)) {
            DatabaseLogging::render($response);
            Benchmark::render($response);
        }
        // Write back to the logs.
        Logging::close($response);

        /*
         * Return
         */
        return $response;
    }
}
