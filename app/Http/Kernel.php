<?php

namespace DeBear\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     * These middleware are run during every request to your application.
     * @var array
     */
    protected $middleware = [
        \DeBear\Http\Middleware\BaseSetup::class,
        /* -+- \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class, -+- */
        /* -+- \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class, -+- */
        \DeBear\Http\Middleware\TrimStrings::class,
        /* -+- \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class, -+- */
        \Illuminate\Http\Middleware\HandleCors::class,
    ];

    /**
     * The application's route middleware groups.
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \Illuminate\Cookie\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            /* \Illuminate\Session\Middleware\AuthenticateSession::class, */
            /* -+- \Illuminate\View\Middleware\ShareErrorsFromSession::class, -+- */
            \DeBear\Http\Middleware\VerifyCsrfToken::class,
            /* -+- \Illuminate\Routing\Middleware\SubstituteBindings::class, -+- */
            \DeBear\Http\Middleware\ProcessRequestWeb::class,
        ],

        'api' => [
            'bindings',
            \DeBear\Http\Middleware\ProcessRequestAPI::class,
        ],
    ];

    /**
     * The application's route middleware.
     * These middleware may be assigned to groups or used individually.
     * Note: "-+-" indicates a default line removed for optimisation.
     * @var array
     */
    protected $routeMiddleware = [
        'process.web' => \DeBear\Http\Middleware\ProcessRequestWeb::class,
        'process.api' => \DeBear\Http\Middleware\ProcessRequestAPI::class,
        'process.core' => \DeBear\Http\Middleware\ProcessRequestCore::class,
        'auth' => \DeBear\Http\Middleware\AuthPolicies::class,
        /* -+- 'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class, -+- */
        'auth.token' => \DeBear\Http\Middleware\AuthToken::class,
        'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        /* -+- 'cache.headers' => \Illuminate\Http\Middleware\SetCacheHeaders::class, -+- */
        /* -+- 'can' => \Illuminate\Auth\Middleware\Authorize::class, -+- */
        /* -+- 'signed' => \Illuminate\Routing\Middleware\ValidateSignature::class, -+- */
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
    ];
}
