<?php

namespace DeBear\Http\Modules;

use Illuminate\Http\Response;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Support\Facades\Request;
use DeBear\Helpers\Blade;
use DeBear\Helpers\HTTP;
use DeBear\Helpers\Interpolate;
use DeBear\Helpers\Policies;
use DeBear\Helpers\Resources;
use DeBear\Helpers\Strings;
use DeBear\Http\Modules\Forms\AdHoc;
use DeBear\Http\Modules\Forms\Defaults;
use DeBear\Http\Modules\Forms\Faker;
use DeBear\Http\Modules\Forms\ModelData;
use DeBear\Http\Modules\Forms\Validation;
use DeBear\Repositories\InternalCache;
use DeBear\Repositories\SuspiciousActivity;

class Forms
{
    /**
     * Path to the configuration file
     * @var string
     */
    protected $path;
    /**
     * Path to the route of the form views
     * @var string
     */
    protected $view_path;
    /**
     * The form configuration
     * @var array
     */
    protected $config;
    /**
     * Location in the session of the form data
     * @var string
     */
    protected $session_key;
    /**
     * Unique reference of the instantiated form
     * @var string
     */
    protected $form_ref;
    /**
     * The mode we are running in: 'render' or 'validate'
     * @var string
     */
    protected $mode;
    /**
     * Tabindex for the next element to render
     * @var integer
     */
    protected $tabindex;
    /**
     * Current form data
     * @var array
     */
    protected $data;

    /**
     * Constructor which breaks down and deals with the full processing of a form step
     * @param string $config Path to a configuration file we will use to render and process this form.
     */
    public function __construct(string $config)
    {
        // Add as an available singleton.
        InternalCache::object()->merge('forms', [$config => $this]);
        // Load the config so we know what we're doing.
        $this->path = $config;
        $this->view_path = str_replace('/', '.', $this->path);
        $this->loadConfig();
        $this->session_key = "forms.{$this->path}";
        $this->form_ref = Strings::md5Code($this->path, 6);
        $this->modeSetRender();
        $this->data = [
            'raw' => [], // Raw form data we received.
            'obj' => [], // Objects that were processed from the database.
            'err' => [], // Validation errors.
            'suspicious' => false, // Whether the processing returned a suspicious result.
        ];
        // Some other internal setup.
        $this->tabindex = 0;
    }

    /**
     * Gets the appropriate config for the form we want to display and process
     * @return void
     */
    protected function loadConfig(): void
    {
        $this->config = include realpath(FrameworkConfig::get('debear.dirs.forms') . "/{$this->path}/config.php");
    }

    /**
     * The main method that goes through the individual steps of processing / rendering
     * @return Response|RedirectResponse The response to send the browser, which could either be content or a redirect
     */
    public function run(): Response|RedirectResponse
    {
        // Setup internals, if this is the first time we have loaded the form.
        $restricted = $this->setup();
        if (isset($restricted)) {
            return is_string($restricted) ? redirect($restricted, 303) : $restricted;
        }

        // If we have a POST, process any of the incoming data.
        if ((Request::server('REQUEST_METHOD') === 'POST') && (Request::post('form_ref') === $this->form_ref)) {
            // Following the Post/Redirect/Get pattern, redirect to a GET request for the errored page or the next step.
            $this->process();
            return redirect(Request::server('REQUEST_URI'), 303);
        }

        if ($this->getStep()) {
            // If we have yet to complete the final step, render the form.
            return $this->render();
        }
        // Otherwise, render or redirect as required.
        return $this->completed();
    }

    /**
     * Perform the internal setup required to start using the form and perform all its processing
     * @return string|Response|null On success null, otherwise the a redirect URL or response to if no access allowed
     */
    protected function setup(): string|Response|null
    {
        // If we haven't already setup the session data, do so.
        if (!session($this->session_key)) {
            // Are we to be locked by a policy?
            if ($this->isRestrictedAccess()) {
                $test = $this->getRestriction();
                if (!Policies::match($test['list'])) {
                    if (isset($test['redirect'])) {
                        // Explicit redirect URL to be returned.
                        return $test['redirect'];
                    }
                    // A stock HTTP response.
                    $method = ($test['response'] ?? 'sendUnauthorised');
                    return HTTP::$method();
                }
            }

            // Base version.
            $form = [
                'ref' => $this->form_ref,
                'steps' => [
                    'all' => array_fill_keys($this->config['steps'], [
                        'is_first' => false,
                        'is_last' => false,
                        'prev' => false,
                        'next' => false,
                        'rendered' => false,
                        'parsed' => false,
                        'failed' => false,
                        'completed' => false,
                    ]),
                    'current' => $this->config['steps'][0],
                ],
                'data' => [
                    'orig' => [],
                    'proc' => [],
                    'changes' => [
                        'new' => [],
                        'updated' => [],
                        'fields' => [],
                    ],
                ],
                'err' => [],
            ];
            // Tweaks we cannot do inline.
            $num_steps = sizeof($this->config['steps']);
            $last_step = $num_steps - 1;
            for ($i = 0; $i < $num_steps; $i++) {
                // First/Last.
                if (!$i) {
                    $form['steps']['all'][$this->config['steps'][$i]]['is_first'] = true;
                }
                if ($i == $last_step) {
                    $form['steps']['all'][$this->config['steps'][$i]]['is_last'] = true;
                }
                // Prev/Next.
                if ($i) {
                    $form['steps']['all'][$this->config['steps'][$i - 1]]['next'] = $this->config['steps'][$i];
                } elseif ($i < $last_step) {
                    $form['steps']['all'][$this->config['steps'][$i + 1]]['prev'] = $this->config['steps'][$i];
                }
            }
            // Any default values to load?
            $form['data']['orig']
                = $form['data']['proc']
                    = Defaults::load($this->getDefaultDefinition(), $this->getFields());
            // Any special processing to run before we start?
            $this->preProcess($this->getProcessingRules('setup'), $form);
            // Write to the session.
            session([$this->session_key => $form]);
        }

        // Errors persist between loads.
        $this->data['err'] = session("{$this->session_key}.err");

        // Setting up fake data?
        if (
            !HTTP::isLive()
            && Request::input('faker')
            && $this->isStepFirst()
            && !$this->isStepCompleted()
            && !$this->getData()
        ) {
            $this->setupFaker();
        }
        // Return a null on success.
        return null;
    }

    /**
     * Setup some example data for use whilst testing
     * @return void
     */
    protected function setupFaker(): void
    {
        session(["{$this->session_key}.data.proc" => Faker::setup($this->getFields())]);
    }

    /**
     * Process the form with the data passed in from an API call
     * @param array $input The data passed in to be processed.
     * @return boolean|array Boolean true on success, otherwise an array of info as to why it failed the validation
     */
    public function processFromAPI(array $input): bool|array
    {
        // Build the input data to be parsed using only expected values from the form config.
        $fields = $this->getFields();
        $data = ['proc' => array_intersect_key($input, $fields)];
        // Validate the input fields.
        $err = [];
        foreach ($fields as $id => $config) {
            // Skip fields that are not required and not included.
            if (!isset($data['proc'][$id]) && (!isset($config['required']) || !$config['required'])) {
                continue;
            }
            $opt = array_intersect_key($config, ['opt_list' => true]);
            $validation = Validation::process($config, $data['proc'][$id] ?? null, $opt, $data['proc']);
            if (is_array($validation)) {
                $err[$id] = $validation;
            }
        }
        // If we found any issues in the data, return an appropriate error message.
        if (count($err)) {
            $this->postProcess($this->getProcessingRules('failure'));
            return $err;
        }
        // Process the data.
        session(["{$this->session_key}.data" => $data]);
        $this->data['raw'] = $data;
        $this->store();
        $this->postProcess($this->getProcessingRules('success'));
        // Inform of success.
        return true;
    }

    /**
     * Process any data posted to the form
     * @return void
     */
    protected function process(): void
    {
        // Validate the fields we know about to see if they were included in the POST.
        $data = $this->getDataObject();
        foreach ($this->getFields() as $id => $config) {
            $value = Request::post($id);

            // Checkbox handling? (Given unselected checkbox does not get sent via CGI).
            $is_cb = Request::post("{$id}__cb");
            if (isset($is_cb)) {
                $value = (int)isset($value);
            }

            // Update the form's contents.
            if (isset($value) && $value !== '') {
                $data['proc'][$id] = $value;
                // Any changes?
                if (!isset($data['orig'][$id]) || ($data['orig'][$id] !== $data['proc'][$id])) {
                    $is_new = !isset($data['orig'][$id]);
                    $data['changes'][$is_new ? 'new' : 'updated'][$id] = true;
                    $data['changes']['fields'][$id] = ($is_new ? 'new' : 'changed');
                }
            } else {
                // If the field is not required, set a null value, otherwise a value representing false.
                $data['proc'][$id] = (isset($config['required']) && $config['required'] ? '' : null);
            }
        }
        session(["{$this->session_key}.data" => $data]);
        $this->data['raw'] = $data;

        // Now (re-)process the form step to validate the info supplied.
        $this->modeSetValidate();
        $this->renderStep();
        $this->modeSetRender();
        // Preserve any errors.
        session(["{$this->session_key}.err" => $this->data['err']]);

        // Store that we've parsed the step.
        $this->setStepParsed();

        // If we failed the validation, re-render the form without moving on and flag that we failed this step.
        if (!$this->validationPassed()) {
            $this->setStepFailed();
            $this->postProcess($this->getProcessingRules('failure'));
            return;
        }

        /* Proceed with the processing */
        // Internal step object status updates.
        $this->setStepCompleted();
        $this->nextStep();

        // If this was the last step, we'll also store the data.
        if (!$this->getStep()) {
            // Perform the database action(s).
            $this->store();
            $this->postProcess($this->getProcessingRules('success'));
        }
    }

    /**
     * Worker method for performing the actual storing action
     * @return void
     */
    protected function store(): void
    {
        $form_data = $this->getData();
        foreach ($this->getFieldMapping() as $key => $map) {
            // The model name may be supplied in multiple ways.
            if (is_string($key) && !isset($map['model'])) {
                $map['model'] = $key;
            }
            // Loop through the two types of field to generate our final data array for this model.
            $table_data = ModelData::build($map, $form_data);

            // Any data manipulation defined within our model?
            if (method_exists($map['model'], 'prepareUpsert')) {
                call_user_func_array([$map['model'], 'prepareUpsert'], [&$table_data]);
            }

            // Are we creating or updating the row?
            $where = [];
            $key = $map['key'];
            if (!is_array($key)) {
                $key = [$key];
            }
            foreach ($key as $col) {
                $where[$col] = $table_data[$col];
            }
            $obj = call_user_func([$map['model'], 'where'], $where);
            $existing = (bool)$obj->get()->count();
            if (!$existing) {
                $obj = call_user_func([$map['model'], 'create'], $table_data);
            } else {
                $obj = $obj->first();
                $obj->update($table_data)->save();
            }
            $this->data['obj'][$map['model']] = ['obj' => $obj, 'data' => $table_data];

            // Any actions to run on this particular row?
            if (isset($map['actions']) && is_array($map['actions'])) {
                // On create.
                if (!$existing && isset($map['actions']['create']) && is_array($map['actions']['create'])) {
                    $this->postProcess($map['actions']['create'], $map['model']);
                }
                // On update.
                if ($existing && isset($map['actions']['update']) && is_array($map['actions']['update'])) {
                    $this->postProcess($map['actions']['update'], $map['model']);
                }
                // In all instances.
                if (isset($map['actions']['success']) && is_array($map['actions']['success'])) {
                    $this->postProcess($map['actions']['success'], $map['model']);
                }
            }
        }
    }

    /**
     * Perform the necessary pre-processing routines before we start any processing
     * @param array $actions The list of relevant actions to perform.
     * @param array $form    The form data for the form we are setting up. (By Reference).
     * @return void
     */
    protected function preProcess(array $actions, array &$form): void
    {
        AdHoc::run($actions, $form);
    }

    /**
     * Perform the necessary post-processing routines having completed the database action
     * @param array       $actions The list of relevant actions to perform.
     * @param string|null $model   An optional name of the current model instance being processed.
     * @return void
     */
    protected function postProcess(array $actions, ?string $model = null): void
    {
        AdHoc::run($actions, $this->data, $model);
    }

    /**
     * Render the form step we want the user to see
     * @return Response A Laravel Response object containing the appropriate content to send to the browser
     */
    protected function render(): Response
    {
        Resources::addCSS('skel/widgets/forms.css');
        // Flag that we've rendered the step and return.
        $this->setStepRendered();
        return response($this->renderStep());
    }

    /**
     * The step to render is marked as "current"
     * @return string The rendered step
     */
    protected function renderStep(): string
    {
        $step = session("{$this->session_key}.steps.current");
        if ($step) {
            $ret = Blade::render('wrappers.form', [
                'form' => $this, // Seeing as we can't use $this, pass in to Blade as a different variables.
                'form_step' => "{$this->view_path}.$step",
            ]);
        }
        return $ret ?? '';
    }

    /**
     * Establish what we display to the user after completing the form
     * @return Response|RedirectResponse The response to send the browser, which could either be content or a redirect
     */
    protected function completed(): Response|RedirectResponse
    {
        $next = $this->getNext();

        // Clear the form within the session but preserve the raw data.
        session()->forget($this->session_key);

        // Have we a message to render on the next page?
        if (isset($next['message'])) {
            session()->flash('form_complete', Interpolate::string($next['message'], $this->data['raw']));
        }

        // Are we rendering a blade template, redirecting or bouncing back to ourselves?
        if (isset($next['template'])) {
            // Blade template.
            return response(Blade::render('wrappers.complete', [
                'template' => "{$this->view_path}.{$next['template']}",
                'form_data' => $this->data['raw'],
            ]));
        } elseif (isset($next['redirect'])) {
            // Redirect to another URL?
            $redirect = Interpolate::string($next['redirect'], $this->data['raw']);
            if ($redirect) {
                return redirect($redirect, 303);
            }
        }
        // Fallback / Default: Re-run ourselves.
        return redirect(Request::server('REQUEST_URI'), 303);
    }

    /**
     * Get the unique reference for a form so we can distinguish two different forms being posted
     * @return string Unique reference to this form
     */
    public function getUniqueReference(): string
    {
        return session("{$this->session_key}.ref");
    }

    /**
     * Flag whether or not the form config requires user restrictions
     * @return boolean If we need to apply policy checks on this form
     */
    protected function isRestrictedAccess(): bool
    {
        return count($this->getRestriction()) > 0;
    }

    /**
     * Get the policy restriction we should apply to this form
     * @return array The logic for applying policy checks on this form
     */
    protected function getRestriction(): array
    {
        return isset($this->config['policies']) && is_array($this->config['policies']) ? $this->config['policies'] : [];
    }

    /**
     * Get the logic by which default values should be loaded
     * @return array The logic for building form defaults
     */
    protected function getDefaultDefinition(): array
    {
        return isset($this->config['defaults']) && is_array($this->config['defaults']) ? $this->config['defaults'] : [];
    }

    /**
     * Get the form field-to-database table mapping(s)
     * @return array The rules of how we'll store the form fields we have been supplied
     */
    protected function getFieldMapping(): array
    {
        return isset($this->config['mapping']) && is_array($this->config['mapping']) ? $this->config['mapping'] : [];
    }

    /**
     * Get the form fields from the config
     * @return array The configuration of the form's fields
     */
    protected function getFields(): array
    {
        return isset($this->config['fields']) && is_array($this->config['fields']) ? $this->config['fields'] : [];
    }

    /**
     * Get the form config for a specific field
     * @param string $id The field we want the configuration for.
     * @return array  The configuration of the requsted field
     */
    protected function getField(string $id): array
    {
        $config = $this->getFields();
        return $config[$id] ?? [];
    }

    /**
     * Get the list of processing rules (if any!) to be run
     * @param string $state The action state we want the processing rules for (e.g., setup, success, failure, etc).
     * @return array The array of rules to be run
     */
    protected function getProcessingRules(string $state): array
    {
        $config = $this->config['actions'] ?? [];
        if ((isset($config) && is_array($config)) && (isset($config[$state]) && is_array($config[$state]))) {
            return $config[$state];
        }
        // Fallback to an empty array.
        return [];
    }

    /**
     * Get the details on how to proceed once the form has been completed
     * @return array Definition of how to proceed
     */
    protected function getNext(): array
    {
        return isset($this->config['next']) && is_array($this->config['next']) ? $this->config['next'] : [];
    }

    /**
     * Get the config for the current form step
     * @return array|boolean The current step's configuration, or false if all steps have been completed
     */
    protected function getStep(): array|bool
    {
        $steps = session("{$this->session_key}.steps");
        if (!isset($steps['current']) || !isset($steps['all'][$steps['current']])) {
            return false;
        }
        return $steps['all'][$steps['current']];
    }

    /**
     * Check whether or not the current step is the first in the process
     * @return boolean That we are/are not the first step in the process
     */
    protected function isStepFirst(): bool
    {
        return $this->isStepFlag('is_first');
    }

    /**
     * Check whether or not the current step is the last in the process
     * @return boolean That we are/are not the last step in the process
     */
    protected function isStepLast(): bool
    {
        return $this->isStepFlag('is_last');
    }

    /**
     * Record whether or not the current step has been rendered
     * @param boolean $state The state we should record against the rendered flag. (Optional, Default: true).
     * @return void
     */
    protected function setStepRendered(bool $state = true): void
    {
        $this->updateStepFlag('rendered', $state);
    }

    /**
     * Check whether or not the current step has been rendered
     * @return boolean The state of the rendered flag
     */
    protected function isStepRendered(): bool
    {
        return $this->isStepFlag('rendered');
    }

    /**
     * Record whether or not the current step has been parsed
     * @param boolean $state The state we should record against the parsed flag. (Optional, Default: true).
     * @return void
     */
    protected function setStepParsed(bool $state = true): void
    {
        $this->updateStepFlag('parsed', $state);
    }

    /**
     * Check whether or not the current step has been parsed
     * @return boolean The state of the parsed flag
     */
    protected function isStepParsed(): bool
    {
        return $this->isStepFlag('parsed');
    }

    /**
     * Record whether or not the current step has been failed
     * @param boolean $state The state we should record against the failed flag. (Optional, Default: true).
     * @return void
     */
    protected function setStepFailed(bool $state = true): void
    {
        $this->updateStepFlag('failed', $state);
    }

    /**
     * Check whether or not the current step has been failed
     * @return boolean The state of the failed flag
     */
    protected function isStepFailed(): bool
    {
        return $this->isStepFlag('failed');
    }

    /**
     * Record whether or not the current step has been completed
     * @param boolean $state The state we should record against the completed flag. (Optional, Default: true).
     * @return void
     */
    protected function setStepCompleted(bool $state = true): void
    {
        $this->setStepFailed(false);  // Also remove the flag that says the step (may have) previously failed.
        $this->updateStepFlag('completed', $state);
    }

    /**
     * Check whether or not the current step has been completed
     * @return boolean The state of the completed flag
     */
    protected function isStepCompleted(): bool
    {
        return $this->isStepFlag('completed');
    }

    /**
     * Update the internal processing/progress flags we have stored against this step
     * @param string  $flag  The key of the flag to update.
     * @param boolean $state State of the flag to be stored.
     * @return void
     */
    protected function updateStepFlag(string $flag, bool $state): void
    {
        $current = session("{$this->session_key}.steps.current");
        if ($current) {
            session(["{$this->session_key}.steps.all.$current.$flag" => $state]);
        }
    }

    /**
     * Worker method to check whether the current step is/is not a specific flag
     * @param string $flag The flag to check in the current step.
     * @return boolean   The result of the check
     */
    protected function isStepFlag(string $flag): bool
    {
        $step = $this->getStep();
        return $step && $step[$flag];
    }

    /**
     * Move our internal step pointer to the next step
     * @return void
     */
    protected function nextStep(): void
    {
        $step = $this->getStep();
        session(["{$this->session_key}.steps.current" => $step['next']]);
    }

    /**
     * Get the full form's data object
     * @return array The object from the session
     */
    protected function getDataObject(): array
    {
        return session("{$this->session_key}.data");
    }

    /**
     * Get the whole data array out of the form
     * @return array The form's data as a key/value array
     */
    protected function getData(): array
    {
        $data = $this->getDataObject();
        return $data['proc'];
    }

    /**
     * Get the value for a specific field out of the form
     * @param string $id ID of the form element we want the value of.
     * @return mixed The form's value for the requested element
     */
    public function getValue(string $id): mixed
    {
        $data = $this->getData();
        return $data[$id] ?? null;
    }

    /**
     * Check the input matches our validation rules
     * @param string $id  The form field to check.
     * @param array  $opt Additional information for the validation process. (Optional).
     * @return void
     */
    public function validateField(string $id, array $opt = []): void
    {
        // Not checking whilst rendering.
        if (!$this->modeIsValidate()) {
            return;
        }

        // The config will detail what we need to do.
        $field = $this->getField($id);

        // If no value was passed for a non-required field, we have nothing to validate.
        $value = $this->getValue($id);
        if (!isset($value) && (!isset($field['required']) || !$field['required'])) {
            return;
        }

        // Then run our test and evaluate.
        $ret = Validation::process($field, $value, $opt, $this->getData());
        if (is_array($ret)) {
            $this->failField($id, $ret['code'], $ret['message']);
        }
    }

    /**
     * Fail a form automatically if we think the action is suspicious
     * @param string $type   Suspicious activity category.
     * @param string $id     Form field that we will store a failure against.
     * @param string $error  Error message to display if we fail the test.
     * @param array  $detail A blob of relevant information that will be JSON encoded on failure.
     * @return void
     */
    public function validateSuspiciousActivity(string $type, string $id, string $error, array $detail): void
    {
        // Not checking whilst rendering.
        if (!$this->modeIsValidate()) {
            return;
        }

        // Add the error to our list against the passed field.
        // We don't clear a previous instance, as this is done when clearing all errors before starting validation.
        $error_code = 'suspicious';
        $this->data['suspicious'] = !App::make(SuspiciousActivity::class)->allowed($type, $detail);
        if ($this->data['suspicious']) {
            $this->failField($id, $error_code, $error);
        }
    }

    /**
     * Flag that a form field has failed
     * @param string $id      Form field to record an error against.
     * @param string $code    Error code (internal).
     * @param string $message Error message (user facing).
     * @return void
     */
    protected function failField(string $id, string $code, string $message): void
    {
        // Build and add our error.
        if (!isset($this->data['err'][$id])) {
            $this->data['err'][$id] = [
                'error' => '',
                'message' => '',
                'all' => [],
            ];
        }
        if (!$this->data['err'][$id]['error']) {
            $this->data['err'][$id]['error'] = $code;
            $this->data['err'][$id]['message'] = $message;
        }
        $this->data['err'][$id]['all'][$code] = $message;
    }

    /**
     * Determine if the value stored in a form's field is valid or not
     * @param string $id The field we want to check.
     * @return boolean   Boolean indicating validity
     */
    public function isFieldValid(string $id): bool
    {
        return !isset($this->data['err'][$id]);
    }

    /**
     * Determine if the form field contains a value or not
     * @param string $id The field we want to check.
     * @return boolean   Boolean indicating a value has been entered
     */
    public function isFieldEntered(string $id): bool
    {
        return ($this->getValue($id) !== null) && ($this->getValue($id) !== '');
    }

    /**
     * Get the appropriate CSS icon class for a form field post server-side validation
     * @param string $id The field we are updating.
     * @return string The CSS class to apply to the status field
     */
    public function formFieldStatus(string $id): string
    {
        if (!$this->isFieldValid($id)) {
            // Invalid field => Invalid Icon.
            return 'icon_error';
        } elseif (!$this->isFieldEntered($id)) {
            // No value => No Icon.
            return '';
        }
        // Valid field => Valid Icon.
        return 'icon_valid';
    }

    /**
     * Get any validation error associated with a form field
     * @param string $id The field we want to check.
     * @return string The error message (if any) related to the passsed form field
     */
    public function getFieldError(string $id): string
    {
        if (
            isset($this->data['err'][$id]['message'])
            && is_string($this->data['err'][$id]['message'])
            && $this->data['err'][$id]['message']
        ) {
            $err = $this->data['err'][$id]['message'] . '.';
        }
        return $err ?? '';
    }

    /**
     * Find out if the form's validation passed successfuly or not
     * @return boolean Validation status
     */
    public function validationPassed(): bool
    {
        return !sizeof($this->data['err']);
    }

    /**
     * Set the tabindex attribute of a form element
     * @return string The HTML attribute
     */
    public function addTabindex(): string
    {
        $this->tabindex++;
        return "tabindex=\"{$this->tabindex}\" data-tabindex=\"{$this->tabindex}\"";
    }

    /**
     * Load the validation rules for an element
     * @param string $id The ID of the element.
     * @return array The validation array for this element
     */
    public function getElementValidation(string $id): array
    {
        $fields = $this->getFields();
        return isset($fields[$id]) ? $fields[$id] : [];
    }

    /**
     * Add the configured attributes for a form element
     * @param string $id   The ID of the element we want to process (as defined in the config array).
     * @param string $type Input type of the element we are rendering. (Optional).
     * @return string The list of attributes and appropriate values
     */
    public function getElementAttributes(string $id, string $type = ''): string
    {
        // If no config, there are no attributes.
        $config = $this->getElementValidation($id);
        $attr = [];

        // Required.
        if (isset($config['required']) && $config['required']) {
            $attr[] = 'required';
        }
        // MinLength.
        if (isset($config['minlength'])) {
            $attr[] = "minlength=\"{$config['minlength']}\"";
        }
        // MaxLength.
        if (isset($config['maxlength'])) {
            $attr[] = "maxlength=\"{$config['maxlength']}\"";
        }
        // Min.
        if (isset($config['min'])) {
            $attr[] = "min=\"{$config['min']}\"";
        }
        // Max.
        if (isset($config['max'])) {
            $attr[] = "max=\"{$config['max']}\"";
        }

        return join(' ', $attr);
    }

    /**
     * Flag that we are rendering the form step
     * @return void
     */
    protected function modeSetRender(): void
    {
        $this->mode = 'render';
    }

    /**
     * Check to see if we are rendering the form step
     * @return boolean Boolean if the mode is set to rendering
     */
    protected function modeIsRender(): bool
    {
        return ( $this->mode == 'render' );
    }

    /**
     * Flag that we are validating the form step
     * @return void
     */
    protected function modeSetValidate(): void
    {
        $this->mode = 'validate';
        $this->data['err'] = []; // Reset the validation results.
    }

    /**
     * Check to see if we are validating the form step
     * @return boolean Boolean if the mode is set to validating
     */
    protected function modeIsValidate(): bool
    {
        return ( $this->mode == 'validate' );
    }

    /**
     * An internal debug dump of the current form state
     * @return array The summarised form
     */
    public function debug(): array
    {
        if (!HTTP::isLive()) {
            $ret = [
                'form' => $this->path,
                'ref' => $this->form_ref,
                'steps' => [
                    'first' => $this->isStepFirst(),
                    'last' => $this->isStepLast(),
                    'rendered' => $this->isStepRendered(),
                    'parsed' => $this->isStepParsed(),
                    'failed' => $this->isStepFailed(),
                    'completed' => $this->isStepCompleted(),
                ],
                'mode' => [
                    'render' => $this->modeIsRender(),
                    'validate' => $this->modeIsValidate(),
                ],
            ];
        }
        return $ret ?? [];
    }

    /**
     * Internal PHP magic method we have customised to include details of the passed config and step
     * @return string Information about the class and the passed form
     */
    public function __toString(): string
    {
        $ret = ["Class: '" . get_class($this), "Config: '{$this->path}'"];
        $step = session("{$this->session_key}.steps.current");
        if ($step) {
            $ret[] = "Step: '$step'";
        }
        return join('; ', $ret);
    }
}
