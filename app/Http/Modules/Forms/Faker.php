<?php

namespace DeBear\Http\Modules\Forms;

use Faker as FakerModule;
use DeBear\Helpers\Strings;

class Faker
{
    /**
     * Setup the form data using fake (but plausible) data
     * @param array $fields The fields we are processing in the form.
     * @return array The array of faked data for each field
     */
    public static function setup(array $fields): array
    {
        $data = [];
        $faker = FakerModule\Factory::create('en_GB');
        foreach ($fields as $id => $field) {
            // Skip honeypots.
            if (isset($field['honeypot']) && $field['honeypot']) {
                continue;
            }
            // Now get a value out.
            switch (gettype($field['faker'])) {
                case 'array':
                    list($method, $args) = $field['faker'];
                    switch ($method) {
                        case '_value':
                            $data[$id] = $args;
                            break;
                        case '_copy':
                            if (!is_array($args)) {
                                $args = [$args];
                            }
                            $data[$id] = [];
                            foreach ($args as $arg) {
                                $data[$id][] = $data[$arg];
                            }
                            $data[$id] = join(' ', $data[$id]);
                            break;
                        default:
                            $data[$id] = call_user_func_array([$faker, $method], $args);
                            if (is_string($data[$id])) {
                                $data[$id] = Strings::encodeInput($data[$id]);
                            }
                            break;
                    }
                    break;
                case 'boolean':
                    $data[$id] = $field['faker'];
                    break;
                default:
                    $data[$id] = $faker->{$field['faker']};
                    if (is_string($data[$id])) {
                        $data[$id] = Strings::encodeInput($data[$id]);
                    }
                    break;
            }
        }
        return $data;
    }
}
