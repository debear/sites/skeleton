<?php

namespace DeBear\Http\Modules\Forms;

use DeBear\Helpers\Strings;

class Validation
{
    /**
     * Run a series of tests to validate a field
     * @param array $field    Definition of the field to validate.
     * @param mixed $value    The value the user entered in the form.
     * @param array $opt      Custom processing options.
     * @param array $all_data All the data entered by the user into the form.
     * @return boolean|array Boolean true on success, otherwise an array of info as to why it failed the validation
     */
    public static function process(array $field, mixed $value, array $opt, array $all_data): bool|array
    {
        $field_type = ($field['type'] ?? 'text');

        // An alternative version of the value that removes the potential for null in our tests.
        $value_alt = ($value ?? false);

        /* Process */
        // Honeypot.
        if (isset($field['honeypot']) && $field['honeypot'] && $value_alt) {
            return ['code' => 'honeypot', 'message' => 'The entered value was not correct'];
        }
        // Required.
        if (
            (!isset($field['honeypot']) || !$field['honeypot'])
            && (isset($field['required']) && $field['required'])
            && !$value_alt
        ) {
            return [
                'code' => 'required',
                'message' => $field_type == 'checkbox'
                    ? 'This option must be selected'
                    : 'Please complete this required field',
            ];
        }
        // Is Num(?).
        if (
            $value_alt
            && $field_type == 'num'
            && !filter_var($value, FILTER_VALIDATE_FLOAT)
            && !filter_var($value, FILTER_VALIDATE_INT)
        ) {
            return ['code' => 'is_num', 'message' => 'Please enter a number in this field'];
        }
        // Is Email.
        if ($value_alt && $field_type == 'email' && !filter_var($value, FILTER_VALIDATE_EMAIL)) {
            return ['code' => 'is_email', 'message' => 'Please enter a valid email address in this field'];
        }
        // Min Length.
        if (
            $value_alt
            && isset($field['minlength'])
            && strlen(Strings::decodeString((string)$value)) < $field['minlength']
        ) {
            return ['code' => 'minlength', 'message' => static::buildErrorLen($field)];
        }
        // Max Length.
        if (
            $value_alt
            && isset($field['maxlength'])
            && strlen(Strings::decodeString((string)$value)) > $field['maxlength']
        ) {
            return ['code' => 'maxlength', 'message' => static::buildErrorLen($field)];
        }
        // Min.
        if ($value_alt && isset($field['min']) && $value < $field['min']) {
            return ['code' => 'min', 'message' => static::buildErrorMinMax($field)];
        }
        // Max.
        if ($value_alt && isset($field['max']) && $value > $field['max']) {
            return ['code' => 'max', 'message' => static::buildErrorMinMax($field)];
        }
        // From a pre-defined list?
        if ($value_alt && isset($opt['opt_list']) && !in_array($value, $opt['opt_list'])) {
            return ['code' => 'opt_list', 'message' => 'Please select an option from the list'];
        }

        // Custom checks.
        // Note: These must handle optional values themselves - we will _always_ call them!
        if (isset($field['custom']) && is_array($field['custom'])) {
            foreach ($field['custom'] as $key => $custom) {
                $args = [$value, $all_data];
                if (isset($custom['args'])) {
                    $args[] = $custom['args'];
                }
                $ret = call_user_func_array($custom['method'], $args);
                if ($ret) {
                    return ['code' => $key, 'message' => $ret];
                }
            }
        }

        // All good, so return true.
        return true;
    }

    /**
     * Build the error message to render when there is a problem with the input length
     * @param array $field The field validation details.
     * @return string The appropriate error message
     */
    protected static function buildErrorLen(array $field): string
    {
        $ele_minlen = $field['minlength'] ?? 0;
        $ele_maxlen = $field['maxlength'] ?? 0;
        $error_len = 'Please enter ';
        if ($ele_minlen && $ele_maxlen) {
            $error_len .= ($ele_minlen == $ele_maxlen ? $ele_minlen : "between $ele_minlen and $ele_maxlen");
        } elseif ($ele_maxlen) {
            $error_len .= "no more than $ele_maxlen";
        } elseif ($ele_minlen) {
            $error_len .= "at least $ele_minlen";
        }
        $error_len .= ' characters';
        return $error_len;
    }

    /**
     * Build the error message to render when there is a problem with the input range
     * @param array $field The field validation details.
     * @return string The appropriate error message
     */
    protected static function buildErrorMinMax(array $field): string
    {
        $is_date = (isset($field['type']) && $field['type'] == 'date');
        $ele_min = $field['min'] ?? false;
        $ele_max = $field['max'] ?? false;
        if ($is_date) {
            $ele_min = ($ele_min ? date('jS F Y', strtotime($ele_min)) : false);
            $ele_max = ($ele_max ? date('jS F Y', strtotime($ele_max)) : false);
        }
        $error_minmax = 'Please enter a ' . ($is_date ? 'date' : 'value') . ' ';
        if ($ele_min && $ele_max) {
            $error_minmax .= ($ele_min == $ele_max ? "of $ele_min" : "between $ele_min and $ele_max");
        } elseif ($ele_max) {
            $error_minmax .= ($is_date ? "of $ele_max or earlier" : "of no more than $ele_max");
        } elseif ($ele_min) {
            $error_minmax .= ($is_date ? "of $ele_min or later" : "of at least $ele_min");
        }
        return $error_minmax;
    }
}
