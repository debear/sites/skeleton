<?php

namespace DeBear\Http\Modules\Forms;

use Illuminate\Support\Facades\App;
use DeBear\Models\Skeleton\User;
use DeBear\Repositories\Time;

class ModelData
{
    /**
     * Build the array of data to write to the database for the model
     * @param array $map       The mapping relevant to the model.
     * @param array $form_data The data entered by the user in to the form.
     * @return array The final array of data to write to the model
     */
    public static function build(array $map, array $form_data): array
    {
        $table_data = [];

        // Direct values from the form data.
        if (isset($map['form_fields']) && is_array($map['form_fields'])) {
            static::formFields($map['form_fields'], $table_data, $form_data);
        }
        // Fields from the User object.
        if (isset($map['user_fields']) && is_array($map['user_fields'])) {
            static::userFields($map['user_fields'], $table_data);
        }
        // Some processing required.
        if (isset($map['fixed_fields']) && is_array($map['fixed_fields'])) {
            static::fixedFields($map['fixed_fields'], $table_data);
        }

        return $table_data;
    }

    /**
     * Get data direct from the form
     * @param array $fields     The fields to copy from the form.
     * @param array $table_data The array we are populating. (Pass-by-reference).
     * @param array $form_data  The details entered in to the form by the user.
     * @return void
     */
    protected static function formFields(array $fields, array &$table_data, array $form_data): void
    {
        foreach ($fields as $fkey => $fval) {
            if (isset($form_data[$fval])) {
                // Numeric array key implies the map value is both the form field and database column.
                // Otherwise, the map value is still the form field, but the key is the database column.
                $array_key = (is_numeric($fkey) ? $fval : $fkey);
                $table_data[$array_key] = $form_data[$fval];
            }
        }
    }

    /**
     * Get data from the user object
     * @param array $fields     The fields to copy from the user object.
     * @param array $table_data The array we are populating. (Pass-by-reference).
     * @return void
     */
    protected static function userFields(array $fields, array &$table_data): void
    {
        foreach ($fields as $fkey => $fcol) {
            // Remove the need to define 'col' => 'col'.
            if (is_numeric($fkey)) {
                $fkey = $fcol;
            }
            $table_data[$fkey] = User::object()->$fcol;
        }
    }

    /**
     * Get data from a formulaic source
     * @param array $fields     The logic that we'll use to add the data.
     * @param array $table_data The array we are populating. (Pass-by-reference).
     * @return void
     */
    protected static function fixedFields(array $fields, array &$table_data): void
    {
        foreach ($fields as $fkey => $fmap) {
            // If the field map is a scalar, then we assume it's a literal to be used.
            if (!is_array($fmap)) {
                $fmap = ['string' => $fmap];
            }
            // Process, where key == action; value(s) == args.
            $faction = array_key_first($fmap);
            $fval = $fmap[$faction];
            switch ($faction) {
                // A timestamp.
                case 'timestamp':
                    switch ($fval) {
                        case 'now':
                            $val = App::make(Time::class)->format();
                            break;
                        case 'server_now':
                        default:
                            $val = App::make(Time::class)->formatServer();
                            break;
                    }
                    break;

                // The passed string literal.
                case 'string':
                default:
                    $val = $fval;
                    break;
            }
            // And store.
            $table_data[$fkey] = $val;
        }
    }
}
