<?php

namespace DeBear\Http\Modules\Forms;

use DeBear\Exceptions\FormsException;
use DeBear\Helpers\Arrays;
use DeBear\Helpers\Comms\Email;
use DeBear\Models\Skeleton\User;

class AdHoc
{
    /**
     * The (fully-qualified) name of the mode for the main User object
     * @var string
     */
    protected static $userModel = 'DeBear\Models\Skeleton\User';

    /**
     * Run any ad hoc processing rules
     * @param array       $actions The list of relevant actions to perform.
     * @param array       $extra   Extra data we pass to any callable processing. (Pass-by-Reference).
     * @param string|null $model   An optional name of the current model instance being processed.
     * @return void
     * @throws FormsException When the requested processing logic could not be executed.
     */
    public static function run(array $actions, array &$extra, ?string $model = null): void
    {
        foreach ($actions as $action => $args) {
            $run = false;

            // Allow actions to be defined with or without arguments.
            if (is_numeric($action)) {
                $action = $args;
                $args = false;
            }

            // Standard action.
            if (is_string($action)) {
                switch (preg_replace('/\-\w+$/', '', $action)) {
                    // Audit change(s) to the model.
                    case 'audit':
                        $run = static::runAudit($model, $args, $extra);
                        break;

                    // Send an email.
                    case 'email':
                        $run = static::runEmail($args, $extra);
                        break;

                    // Log the user in.
                    case 'login':
                        $run = static::runLogin($args, $extra);
                        break;

                    // Log the user out.
                    case 'logout':
                        $run = static::runLogout();
                        break;

                    // Fire events.
                    case 'events':
                        $run = static::runEvents($args, $extra);
                        break;
                }

            // Custom logic, where $action is a callback.
            } elseif (is_callable($action)) {
                $fn_args = (array)array_filter([$extra, $args]); // Phan linting quirk.
                call_user_func_array($action, $fn_args);
                $run = true;
            }

            // Flag if we failed, for whatever reason.
            if (!$run) {
                throw new FormsException(
                    'Unable to execute processing action: \'' . json_encode($action)
                        . '\' => \'' . json_encode($args) . '\''
                );
            }
        }
    }

    /**
     * The specific post-processing action of auditing the ORM change
     * @param string|null $model An optional name of the current model instance being processed.
     * @param array       $args  Arguments relevant to the action of auditing the ORM change.
     * @param array       $extra Extra data we pass to any callable processing. (Pass-by-Reference).
     * @return boolean The the post-processing action should be considered as having been run
     */
    protected static function runAudit(?string $model, array $args, array $extra): bool
    {
        // Only applies if we're processing a specific model.
        $run = false;
        if (isset($model)) {
            $obj = $extra['obj'][$model]['obj'];
            // What fields were changed?
            $changed_fields = $obj->wasRecentlyCreated ? $obj->getAttributes() : $obj->getChanges();
            $changes = array_keys(array_intersect_key($changed_fields, array_flip($args['fields'])));
            if (!empty($changes)) {
                $obj->audit($args['type'], $args['summary'], $changes);
            }
            // Considering the presence of changes is enough to qualify as this having run - no actual changes required.
            $run = true;
        }
        return $run;
    }

    /**
     * The specific post-processing action of sending the email
     * @param array|string $args  Arguments relevant to the action of sending the email.
     * @param array        $extra Extra data we pass to any callable processing. (Pass-by-Reference).
     * @return boolean The the post-processing action should be considered as having been run
     */
    protected static function runEmail(array|string $args, array $extra): bool
    {
        $run = false;
        if ($args) {
            // If only supplied a string, it's the email name.
            if (!is_array($args)) {
                $args = ['name' => $args];
            }
            if (isset($args['name'])) {
                // Who do we send it to?
                $user = false;
                if (isset($extra['obj'][static::$userModel])) {
                    $user = $extra['obj'][static::$userModel]['obj'];
                } elseif (isset($args['user_id'])) {
                    $user = $args['user_id'];
                } elseif (User::staticIsLoggedIn()) {
                    $user = User::object()->id;
                }
                // Perform.
                if ($user) {
                    if (!isset($args['data']) || !is_array($args['data'])) {
                        $args['data'] = [];
                    }
                    $args['data'] = Arrays::merge($args['data'], $extra['raw']['proc']);
                    Email::send($args['name'], $user, $args);
                    $run = true;
                }
            }
        }
        return $run;
    }

    /**
     * The specific post-processing action of logging the user in
     * @param mixed $args  Arguments relevant to the action of a user logging in.
     * @param array $extra Extra data we pass to any callable processing. (Pass-by-Reference).
     * @return boolean The the post-processing action should be considered as having been run
     */
    protected static function runLogin(mixed $args, array $extra): bool
    {
        // Can only proceed if we have a user object.
        $run = false;
        if (isset($extra['obj'][static::$userModel])) {
            $forms = session('forms'); // Because this will nuke the sessions in the form, preserve.
            User::doLogin($extra['obj'][static::$userModel]['obj'], is_array($args) ? $args : []);
            session(['forms' => $forms]); // Restore the preserved forms.
            $run = true;
        }
        return $run;
    }

    /**
     * The specific post-processing action of logging the user out
     * @return boolean The the post-processing action should be considered as having been run
     */
    protected static function runLogout(): bool
    {
        User::doLogout();
        return true;
    }

    /**
     * The specific post-processing action of firing internal events
     * @param array $events The list of events to be processed.
     * @param array $extra  Extra data we pass to any callable processing. (Pass-by-Reference).
     * @return boolean The the post-processing action should be considered as having been run
     */
    protected static function runEvents(array $events, array $extra): bool
    {
        foreach ($events as $event) {
            $event::dispatch($extra['obj'], $extra['raw']);
        }
        return true;
    }
}
