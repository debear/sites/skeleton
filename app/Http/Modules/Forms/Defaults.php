<?php

namespace DeBear\Http\Modules\Forms;

use DeBear\Helpers\TransientStorage;
use DeBear\Repositories\InternalCache as Cache;
use DeBear\Models\Skeleton\User;

class Defaults
{
    /**
     * Generate the default data we should have in our form
     * @param array $definitions The definitions for the form defaults.
     * @param array $fields      The fields we are processing in the form.
     * @return array The default data we should include in our form
     */
    public static function load(array $definitions, array $fields): array
    {
        $defaults = [];
        foreach ($definitions as $method => $args) {
            // Allow arg-less definitions.
            if (is_numeric($method)) {
                $method = $args;
                $args = [];
            }
            // Process.
            switch ($method) {
                case 'user':
                    // Only include what we need.
                    if (!sizeof($args)) {
                        $args = [];
                    }
                    $data = User::object()->toArray();
                    $args = array_merge($args, array_keys($fields));
                    $defaults = array_merge($defaults, array_intersect_key($data, array_flip($args)));
                    break;
                case 'internal-cache':
                case 'transient':
                    // Transient storage / Internal cache.
                    $data = $method == 'transient' ? TransientStorage::get($args) : Cache::object()->get($args);
                    if (is_object($data)) {
                        // Convert to an ORM object to an array of data.
                        $data = $data->toArray();
                    }
                    // Proceed if an array was returned.
                    if (is_array($data)) {
                        $defaults = array_merge($defaults, array_intersect_key($data, $fields));
                    }
                    break;
            }
        }

        return $defaults;
    }
}
