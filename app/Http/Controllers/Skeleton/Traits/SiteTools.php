<?php

namespace DeBear\Http\Controllers\Skeleton\Traits;

use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Http\Response;
use DeBear\Helpers\HTML;
use DeBear\Helpers\Resources;
use DeBear\Helpers\Sitemap;

trait SiteTools
{
    /**
     * Generate a sitemap for a particular part of the site
     * @return Response A response object with the appropriate sitemap details as markup
     */
    public function sitemapWorker(): Response
    {
        // Custom page config.
        Resources::addCSS('skel/pages/sitemap.css');
        Resources::addJS('skel/pages/sitemap.js');
        HTML::setNavCurrent('/sitemap');
        HTML::linkMetaDescription('sitemap');

        // Get the Sitemap.
        $sitemap = Sitemap::buildSitemap(FrameworkConfig::get('debear.url.sub'));

        // Then build the page.
        $base = 'debear.content.sitemap.view';
        $view = FrameworkConfig::get("$base.override") ?: FrameworkConfig::get("$base.default");
        return response()
            ->view($view, compact('sitemap'));
    }

    /**
     * Generate an XML version of the sitemap for a particular part of the site
     * @return Response A response object with the appropriate sitemap details as an XML sitemap
     */
    public function sitemapXmlWorker(): Response
    {
        // Get the Sitemap.
        $sitemap = Sitemap::buildSitemap(FrameworkConfig::get('debear.url.sub'));

        // Disable some of the security headers.
        FrameworkConfig::set([
            'debear.headers.csp.enabled' => false,
            'debear.headers.feature_policy.enabled' => false,
        ]);

        // Then build the page.
        return response()
            ->view('skeleton.pages.sitemap_xml', compact('sitemap'))
            ->header('Content-Type', 'text/xml');
    }
}
