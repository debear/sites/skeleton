<?php

namespace DeBear\Http\Controllers\Skeleton;

use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Support\Facades\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use DeBear\Http\Controllers\Controller;
use DeBear\Helpers\HTML;
use DeBear\Helpers\HTTP;
use DeBear\Helpers\Interpolate;
use DeBear\Helpers\Resources;
use DeBear\Repositories\Logging;
use DeBear\Repositories\InternalCache;
use DeBear\Http\Controllers\Skeleton\Traits\SiteTools as TraitSiteTools;

class SiteTools extends Controller
{
    use TraitSiteTools;

    /**
     * Generate a manifest JSON file for our app
     * @return Response A response object featuring our manifest
     */
    public function manifest(): Response
    {
        // Top-level or subsite?
        $is_subsite = (bool) FrameworkConfig::get('debear.section.code');
        // Generate.
        $name = join(' ', array_unique(array_filter([
            FrameworkConfig::get('debear.names.site'),
            $is_subsite ? FrameworkConfig::get('debear.names.section') : false,
        ])));
        $manifest = [
            /* $name, */
            'name' => $name,
            'short_name' => $name,
            'description' => Interpolate::string(FrameworkConfig::get('debear.links.home.descrip')),
            'start_url' => ($is_subsite ? '/' . FrameworkConfig::get('debear.section.endpoint') : '')
                . '/?utm_source=homescreen',
            'dir' => 'ltr',
            'lang' => 'en-GB',
            'display' => 'standalone',
            'orientation' => 'portrait',
            'background_color' => FrameworkConfig::get('debear.meta.theme_colour'),
            'theme_color' => FrameworkConfig::get('debear.meta.theme_colour'),
            'icons' => [],
        ];
        // Generate the icon list.
        foreach (array_keys(FrameworkConfig::get('debear.meta.sizes')) as $size) {
            $manifest['icons'][] = [
                'src' => HTML::getMetaImageOpenGraph(),
                'sizes' => $size . 'x' . $size,
                'type' => 'image/' . pathinfo(HTML::getMetaImageOpenGraph(), PATHINFO_EXTENSION),
            ];
        }

        // Return as JSON object to the browser.
        return response(json_encode($manifest, JSON_NUMERIC_CHECK))
            ->header('Content-Type', 'application/manifest+json');
    }


    /**
     * Redirect users to the selected progressive web app service worker
     * @param string $name The code for the site we're running.
     * @return RedirectResponse A redirect object pointing to the correct JavaScript file
     */
    public function pwaServiceWorker(string $name): RedirectResponse
    {
        return redirect(HTTP::buildDomain('static') . '/' . FrameworkConfig::get('debear.url.sub')
            . "/js/pwa-sw/$name.js");
    }

    /**
     * Display the site's Terms of Use policy
     * @return Response A response object featuring the rendered Terms
     */
    public function terms(): Response
    {
        // Custom page config.
        HTML::setPageTitle('Our Terms of Use');
        HTML::setNavCurrent('/terms-of-use');
        HTML::linkMetaDescription('terms');
        Resources::addCSS('skel/pages/terms_use.css');
        if (Request::input('interactive')) {
            Resources::addJS('skel/pages/terms_use.js');
            HTML::setIsPopup();
        }

        return response()
            ->view('skeleton.pages.terms_use', [
                'name' => FrameworkConfig::get('debear.names.site'),
                'email' => 'privacy',
                'domain' => FrameworkConfig::get('debear.url.domain'),
                'tld' => FrameworkConfig::get('debear.url.tld'),
            ]);
    }

    /**
     * Display the site's Privacy Policy
     * @return Response A response object featuring the rendered policy
     */
    public function privacy(): Response
    {
        // Custom page config.
        HTML::setPageTitle('Our Privacy Policy');
        HTML::setNavCurrent('/privacy-policy');
        HTML::linkMetaDescription('privacy');
        Resources::addCSS('skel/pages/privacy_policy.css');
        return response()
            ->view('skeleton.pages.privacy_policy', [
                'name' => FrameworkConfig::get('debear.names.site'),
                'email' => 'privacy',
                'domain' => FrameworkConfig::get('debear.url.domain'),
                'tld' => FrameworkConfig::get('debear.url.tld'),
            ]);
    }

    /**
     * Generate a sitemap for a particular part of the site
     * @return Response A response object with the appropriate sitemap details as markup
     */
    public function sitemap(): Response
    {
        // Our configuration.
        HTML::setPageTitle('Website Sitemap');
        // Then the standard processing from within the trait.
        return $this->sitemapWorker();
    }

    /**
     * Generate an XML version of the sitemap for a particular part of the site
     * @return Response A response object with the appropriate sitemap details as an XML sitemap
     */
    public function sitemapXml(): Response
    {
        // Pass to our trait logic in its entirety.
        return $this->sitemapXmlWorker();
    }

    /**
     * Write a Report URI incident to file
     * @param string $mode Type of Report to save.
     * @return Response A response object summarising the report
     */
    public function reportUri(string $mode): Response
    {
        // If php://input is empty, it's a meaningless report so return a 400 (Bad Request).
        $php_input = InternalCache::object()->get('php://input');
        if (is_string($php_input) || !sizeof($php_input)) {
            if (is_string($php_input) && strlen(trim($php_input))) {
                Logging::add('Report URI', 'Failed validation on string \'' . $php_input . '\'');
            }
            return HTTP::sendBadRequest(['success' => false, 'message' => 'Could not understand input']);
        }

        // Create as a JSON bundle to store to disk.
        Logging::report($mode);

        // We're not going to return any content, so send an HTTP 204 No Content.
        return HTTP::sendNoContent();
    }
}
