<?php

namespace DeBear\Http\Controllers\Skeleton;

use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use DeBear\Http\Controllers\Controller;
use DeBear\Models\Skeleton\User;
use DeBear\Repositories\SuspiciousActivity;

class Authentication extends Controller
{
    /**
     * Handle an authentication attempt.
     * @param Request $request Details of the request by the user.
     * @return JsonResponse
     */
    public function login(Request $request): JsonResponse
    {
        // If the user is already logged in, send an appropriate error.
        if (User::object()->isLoggedIn()) {
            return response()->json([
                'success' => false,
            ], 409);
        }

        // What was submitted?
        $user_id = trim($request->post('username'));
        $password = trim($request->post('password'));
        $rememberme = $request->post('rememberme') && ($request->post('rememberme') != 'false');

        // Pre-req: Can the user even attempt a login?
        $detail = [
            'user_id' => $user_id,
            'source' => 'form',
        ];
        if (!App::make(SuspiciousActivity::class)->allowed('failed_login', $detail)) {
            return $this->fail();
        }

        // No User ID or Password is an immediate fail.
        if (!$user_id || !$password) {
            return $this->fail('Incomplete Details', $user_id);
        }

        // Get the user object out of the database for the given ID.
        $user = User::where('user_id', $user_id)->first();
        if (!$user) {
            return $this->fail('Unknown User', $user_id);
        }

        // Is the user of an allowable status?
        if (!in_array($user->status, ['Active', 'Unverified'])) {
            return $this->fail('Inactive Member', $user_id);
        }

        // Attempt a framework login.
        if (User::doLogin(['user_id' => $user_id, 'password' => $password], ['rememberme' => $rememberme])) {
            /* Authentication passed, send a successful return message */
            // However, if we're in "remember me" mode then do action that too.
            // Return an appropriate message.
            return response()->json([
                'success' => true,
            ]);
        } else {
            // Something went wrong, so flag a failure.
            return $this->fail('Incorrect Password', $user_id);
        }
    }

    /**
     * Process a failed attempt
     * @param string $reason  An internal explanation as to cause of the failure.
     * @param string $user_id The entered ID being logged in.
     * @return JsonResponse The failure message to send the browser
     */
    protected function fail(string $reason = '', string $user_id = ''): JsonResponse
    {
        // Record a failed login in our activity log.
        if ($reason) {
            App::make(SuspiciousActivity::class)->record('failed_login', $reason, false, [
                'user_id' => $user_id,
                'source' => 'form',
            ]);
        }
        // Return an appropriate message.
        return response()->json([
            'success' => false,
        ], 401);
    }

    /**
     * Log the user out
     * @param Request $request The request being processed.
     * @return JsonResponse
     */
    public function logout(Request $request): JsonResponse
    {
        if (!User::object()->isLoggedIn()) {
            // Not logged in in the first place...
            $success = false;
            $code = 409;
        } else {
            // Perform the login.
            User::doLogout();
            $success = true;
            $code = 200;
        }
        // The message to return.
        return response()->json([
            'success' => $success,
        ], $code);
    }
}
