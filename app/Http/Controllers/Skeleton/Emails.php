<?php

namespace DeBear\Http\Controllers\Skeleton;

use Illuminate\Http\Response;
use Illuminate\Http\RedirectResponse;
use DeBear\Http\Controllers\Controller;
use DeBear\Helpers\HTTP;
use DeBear\Helpers\Strings;
use DeBear\Helpers\Comms\Email;
use DeBear\Models\Skeleton\CommsEmail;
use DeBear\Models\Skeleton\CommsEmailLink;

class Emails extends Controller
{
    /**
     * Display an email sent (or rather, attempted) by the system
     * @param string $i The first part of the email's unique reference code.
     * @param string $t The second part of the email's unique reference code.
     * @param string $r The third part of the email's unique reference code.
     * @return Response A response object featuring our rendered email
     */
    public function show(string $i, string $t, string $r): Response
    {
        // Validate the code, and if okay get the email out.
        if (Email::validateCode($i, $t, $r)) {
            $email = CommsEmail::where(['email_code' => $i])->first();
        }

        // If we didn't get a valid record for whatever reason, return a failure.
        if (!isset($email)) {
            return HTTP::sendNotFound(view('errors.em0'));
        }

        // What sort of email was sent - text only or HTML?
        $content = '';
        if (preg_match('/Content-Type: text\/html;/', $email->email_body)) {
            // HTML.
            $num_matches = preg_match(
                '/Content-Type: text\/html; charset=&quot;UTF-8&quot;(.+)$/s',
                $email->email_body,
                $tmp
            );
            $parsed = $num_matches ? Strings::decodeString($tmp[1], false) : 'none';

            // Get the main info ready for display.
            preg_match_all('/<head[^>]*>.*?(<style[^>]*>.*?<\/style>).*?<\/body>/s', $parsed, $head_content);
            if (is_array($head_content) && isset($head_content[1]) && is_array($head_content[1])) {
                $content .= join('', $head_content[1]);
            }

            preg_match('/<body[^>]*>(.+)<\/body>/s', $parsed, $body_content);
            $content .= $body_content[1];

            // Convert style tags to include the nonce for CSP compatibility.
            $content = str_replace('<style ', '<style ' . HTTP::buildCSPNonceTag() . ' ', $content);
        } else {
            // Text.
            $content = '<pre>' . $email->email_body . '</pre>';
        }
        $send_col = ($email->email_sent ? 'sent' : 'send');
        $send_col_obj = "email_$send_col";

        // Get the DOCTYPE.
        preg_match('/&lt;!DOCTYPE.+&gt;/', $email->email_body, $doctype);
        $doctype = !is_array($doctype) || !isset($doctype[0]) ? '<!DOCTYPE html>' : Strings::decodeString($doctype[0]);

        // Now render!
        return response()->view('skeleton.pages.email', compact([
            'email',
            'content',
            'doctype',
            'send_col',
            'send_col_obj',
        ]))->header('Content-Type', 'text/html; charset=UTF-8');
    }

    /**
     * Visit a link embeded in a sent email
     * @param string $i The first part of the email link's unique reference code.
     * @param string $t The second part of the email link's unique reference code.
     * @param string $r The third part of the email link's unique reference code.
     * @return RedirectResponse|Response A redirect to the link included in the email, or response on error
     */
    public function link(string $i, string $t, string $r): RedirectResponse|Response
    {
        // Validate the code, and if okay get the email out.
        if (Email::validateCode($i, $t, $r)) {
            $link = CommsEmailLink::where(['link_code' => $i])->first();
        }

        // If we didn't get a valid record for whatever reason, return a failure.
        if (!isset($link)) {
            return HTTP::sendNotFound(view('errors.em1'));
        }

        // Log that the link was visited.
        $link->stampVisited();

        // And preform the redirect!
        return redirect($link->link_full);
    }
}
