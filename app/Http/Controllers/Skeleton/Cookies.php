<?php

namespace DeBear\Http\Controllers\Skeleton;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Support\Facades\Request;
use Illuminate\Http\Request as HttpRequest;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Http\JsonResponse as HttpJsonResponse;
use DeBear\Exceptions\Cookies\PrefsException;
use DeBear\Helpers\CookiePreferences;
use DeBear\Helpers\HTML;
use DeBear\Helpers\Resources;
use DeBear\Helpers\SessionAggregation;
use DeBear\Http\Controllers\Controller;
use DeBear\Models\Skeleton\CookiePreferenceLog;
use DeBear\Models\Skeleton\User;
use DeBear\Repositories\InternalCache;
use DeBear\Repositories\Time;

class Cookies extends Controller
{
    /**
     * Display the main cookie preference list and notes
     * @param HttpRequest $request Details of the request by the user.
     * @return HttpResponse A response object with the rendered Blade template
     */
    public function view(HttpRequest $request): HttpResponse
    {
        // Custom page config.
        Resources::addCSS('skel/pages/cookies.css');
        HTML::setPageTitle('Cookie Preferences');
        HTML::setMetaDescription(
            'More information about the cookies used by ' . FrameworkConfig::get('debear.names.company')
                . ' and some preference control.'
        );

        // Then build the page.
        return response()->view('skeleton.pages.cookies', [
            'name' => FrameworkConfig::get('debear.names.company'),
        ]);
    }

    /**
     * Update a particular user's cookie preference
     * @param HttpRequest $request Details of the request by the user.
     * @return HttpJsonResponse
     */
    public function prefs(HttpRequest $request): HttpJsonResponse
    {
        // What was updated?
        $args = InternalCache::object()->get('php://input');
        parse_str($args, $args); // Arg 1 = input string, Arg 2 = output array.
        $type = $args['type'];
        $allow = $args['allow'] && ($args['allow'] != 'false');

        // Validate the 'type' exists?
        try {
            $key = "allow-$type";
            $existing = CookiePreferences::get($key);
        } catch (PrefsException $e) {
            // Nope, so flag back to the user.
            return response()->json([
                'success' => false,
            ], 400);
        }

        // Are we changing?
        if ($existing != $allow) {
            // Yes, so actually perform the update.
            CookiePreferences::set($key, $allow);
            CookiePreferences::store();
            // But also log the user requested the change.
            CookiePreferenceLog::create([
                'session_id' => SessionAggregation::getID(),
                'user_id' => User::object()->id ?: null,
                'remote_ip' => Request::server('REMOTE_ADDR'),
                'when_done' => App::make(Time::class)->formatServer(),
                'setting' => $type,
                'state' => (int)$allow,
            ]);
        }

        // Return success.
        return response()->json([
            'success' => true,
        ]);
    }
}
