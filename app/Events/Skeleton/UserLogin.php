<?php

namespace DeBear\Events\Skeleton;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class UserLogin
{
    use Dispatchable;
    use InteractsWithSockets;
    use SerializesModels;

    /**
     * ID of the user being logged in
     * @var integer
     */
    public int $user_id;

    /**
     * Create a new event instance.
     * @param integer $user_id ID of the user being logged in.
     * @return void
     */
    public function __construct(int $user_id)
    {
        $this->user_id = $user_id;
    }
}
