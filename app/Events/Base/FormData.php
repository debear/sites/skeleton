<?php

namespace DeBear\Events\Base;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

abstract class FormData
{
    use Dispatchable;
    use InteractsWithSockets;
    use SerializesModels;

    /**
     * The array of form object data
     * @var array
     */
    protected array $obj;
    /**
     * The array of form data
     * @var array
     */
    protected array $data;

    /**
     * Create a new event instance.
     * @param array $obj  The array of object(s) created/updated within the form.
     * @param array $data The array of processed form data.
     * @return void
     */
    public function __construct(array $obj, array $data)
    {
        $this->obj = $obj;
        $this->data = $data;
    }

    /**
     * Get an ORM object created/updated by the form
     * @param string $class A class name for form object retrieval.
     * @return mixed The ORM object created/updated by the form of the supplied class
     */
    public function getFormObject(string $class): mixed
    {
        return $this->obj[$class] ?? null;
    }

    /**
     * Get the original data from the form before it was processed
     * @return array The array of original form key/value pairs
     */
    public function getOriginalData(): array
    {
        return $this->data['orig'];
    }

    /**
     * Get the new form data after it was submitted, in an array format
     * @return array The array of updated form key/value pairs
     */
    public function getRawData(): array
    {
        return $this->data['proc'];
    }

    /**
     * Get the array storing the changes within the form data before and after submission
     * @return array The form data changelog
     */
    public function getChangedData(): array
    {
        return $this->data['changes'];
    }
}
