<?php

namespace DeBear\ORM\Skeleton;

use DeBear\ORM\Base\Instance;

class WeatherForecast extends Instance
{
    /**
     * Convert the database temperature (in Celsuis) to Fahrenheit
     * @return float The temperature in Fahrenheit
     */
    public function getTempFAttribute(): float
    {
        return 32 + ($this->temp * (9 / 5));
    }
}
