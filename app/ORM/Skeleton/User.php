<?php

namespace DeBear\ORM\Skeleton;

use DeBear\ORM\Base\Instance;

class User extends Instance
{
    /**
     * Attributes not to be included when representing a row in an API response
     * @var array
     */
    protected $hidden = ['password'];
    /**
     * Attributes which should be masked in external representation
     * @var array
     */
    protected $mask = ['password'];

    /**
     * The default handler for loading secondary user information.
     * @param string $name The requested secondary information.
     * @return void
     */
    protected function loadSecondaryDefault(string $name): void
    {
        $users = $this->unique('id');
        $maps = [
            'permissions' => UserPermissions::class,
            'rememberme' => UserRememberMe::class,
        ];
        $this->secondary[$name] = $maps[$name]::query()
            ->whereIn('user_id', $users)
            ->get()
            ->setMultipleSecondary();

        // Then tie to the users.
        foreach ($this->data as $i => $user) {
            $this->data[$i][$name] = $this->secondary[$name]->where('user_id', $user['id']);
        }
    }

    /**
     * The user's full contatenated name
     * @return string The contatenated name
     */
    protected function getNameAttribute(): string
    {
        return join(' ', array_filter([$this->forename, $this->surname]));
    }
}
