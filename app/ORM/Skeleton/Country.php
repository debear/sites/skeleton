<?php

namespace DeBear\ORM\Skeleton;

use DeBear\ORM\Base\Instance;

class Country extends Instance
{
    /**
     * Load the list of timezone(s) that apply to the loaded country/ies
     * @return void
     */
    protected function loadSecondaryTimezones(): void
    {
        $countries = $this->unique('code2');
        $this->secondary['timezones'] = Timezone::query()
            ->whereIn('code2', $countries)
            ->get()
            ->setMultipleSecondary();
        foreach ($this->data as $i => $country) {
            $this->data[$i]['timezones'] = $this->secondary['timezones']->where('code2', $country['code2']);
        }
    }
}
