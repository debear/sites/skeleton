<?php

namespace DeBear\ORM\Skeleton;

use DeBear\ORM\Base\Instance;
use DeBear\Models\Skeleton\User as UserModel;
use DeBear\Repositories\InternalCache;

class APIToken extends Instance
{
    /**
     * Attributes not to be included when representing a row in an API response
     * @var array
     */
    protected $hidden = ['password'];
    /**
     * Array of scopes that are valid for this token
     * @var array
     */
    protected $valid_scopes_arr;

    /**
     * Load the user details for the token(s)
     * @return void
     */
    public function loadSecondaryUser(): void
    {
        $user_ids = array_filter($this->unique('user_id'));
        if (!count($user_ids)) {
            // Skip when no valid tokens were returned.
            return;
        }
        $this->secondary['users'] = User::query()
            ->whereIn('id', $user_ids)
            ->get();
        foreach ($this->data as $i => $token) {
            $this->data[$i]['user'] = $this->secondary['users']->where('id', $token['user_id']);
        }
    }

    /**
     * Check whether a supplied scope is valid for this API token or not
     * @param string $scope The scope to be checked.
     * @return boolean That the requested scope is valid
     */
    public function isScopeValid(string $scope): bool
    {
        // On first pass, convert our comma-separated list to an array.
        if (!isset($this->valid_scopes_arr)) {
            $this->valid_scopes_arr = explode(',', $this->valid_scopes);
        }
        // Determine what, and how much, to match.
        if (strpos($scope, '&') !== false) {
            // Must match all scopes.
            $scopes = explode('&', $scope);
            $min_matches = count($scopes);
        } else {
            // Must match at least one scope.
            $scopes = explode('|', $scope);
            $min_matches = 1;
        }
        // Perform and count the matches.
        $matches = array_intersect($this->valid_scopes_arr, $scopes);
        return count($matches) >= $min_matches;
    }

    /**
     * Store the current object within the InternalCache for later access
     * @return void
     */
    public function setupInternalObject(): void
    {
        InternalCache::object()->set('APIToken', $this);
        UserModel::setupFromAPI($this->user_id);
    }

    /**
     * Retrive the current instance in the InternalCache
     * @return self Instantiated instance of the current object
     */
    public static function object(): self
    {
        return InternalCache::object()->get('APIToken');
    }
}
