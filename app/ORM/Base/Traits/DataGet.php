<?php

namespace DeBear\ORM\Base\Traits;

use Carbon\Carbon;
use DeBear\Helpers\Arrays;
use DeBear\Repositories\Time;
use DeBear\Repositories\DataObject;

trait DataGet
{
    /**
     * SET columns and their values
     * @var array
     */
    protected $sets;

    /**
     * Get the column value for the current record row
     * @param string $name The column name.
     * @return mixed The value of the requested column in the current row
     */
    public function __get(string $name): mixed
    {
        if (!$this->valid()) {
            return null;
        }

        $data = $this->toArray();
        if (array_key_exists($name, $data)) {
            // Direct lookup in the array.
            if (isset($this->dates[$name]) && is_string($data[$name])) {
                // A date of some form that we may need to cast it to timezone-localised a Carbon instance.
                $c = new Carbon($data[$name] . ($this->dates[$name] == 'Y-m-d' ? ' 12:00:00' : ''));
                $this->$name = $data[$name] = Time::object()->localiseDatabase($c)->settings([
                    'toStringFormat' => $this->dates[$name],
                ]);
            } elseif (isset($this->sets[$name]) && is_string($data[$name])) {
                // Convert it to an object of booleans.
                $this->$name = $data[$name] = new DataObject(array_merge(
                    array_fill_keys($this->sets[$name], false),
                    isset($data[$name]) ? array_fill_keys(explode(',', strtolower($data[$name])), true) : []
                ));
            }
            return $data[$name];
        } elseif ($this->hasAccessor($name)) {
            // Accessor?
            $method = $this->getAccessorName($name);
            return $this->$method();
        }
        // Fallback to a default value.
        return null;
    }

    /**
     * Get the data of all our results as a raw array
     * @return array The full data (of all row(s)) as an array
     */
    public function getData(): array
    {
        return $this->isRoot ? $this->data : array_values(Arrays::orderedIntersectKey(
            $this->root->getData(),
            array_fill_keys($this->keys, true)
        ));
    }

    /**
     * Get the data of the current record as a raw array
     * @return array The data of the current record as an array
     */
    public function toArray(): array
    {
        return array_filter($this->getIndex($this->index), function ($key) {
            return substr($key, 0, 6) != '__orm_';
        }, ARRAY_FILTER_USE_KEY);
    }

    /**
     * Get the data of the current record as a raw array, with internal fields masked, if appropriate
     * @return array The external-safe data of the current record as an array
     */
    public function toMaskedArray(): array
    {
        return array_merge(
            $this->toArray(),
            array_fill_keys($this->mask ?? [], '**REDACTED**')
        );
    }

    /**
     * Test the existence and non-NULL status of a property in the current row's model data
     * @param string $name The property name being checked.
     * @return boolean That the property is set within the current row
     */
    public function __isset(string $name): bool
    {
        if (!$this->valid()) {
            return false;
        }

        $data = $this->toArray();
        if (array_key_exists($name, $data)) {
            // Lookup in the data.
            return isset($data[$name]);
        } elseif ($this->hasAccessor($name)) {
            // Test the accessor.
            $method = $this->getAccessorName($name);
            return $this->$method() !== null;
        }
        // Otherwise, no it can't be set.
        return false;
    }

    /**
     * Test if the query returned any results.
     * @return boolean That the original query returned results to be processed.
     */
    public function isset(): bool
    {
        return ($this->count() > 0);
    }

    /**
     * Get the list of date fields in the model
     * @return array The columns containing dates
     */
    public function getDateFields(): array
    {
        return $this->dates ?? [];
    }

    /**
     * Get the list of SET fields in the model
     * @return array The columns containing SET values
     */
    public function getSETFields(): array
    {
        return $this->sets ?? [];
    }

    /**
     * Test whether the model contains a specific accessor
     * @param string $name The accessor being checked.
     * @return boolean That the accessor has been defined in the model
     */
    protected function hasAccessor(string $name): bool
    {
        return method_exists($this, $this->getAccessorName($name));
    }

    /**
     * Build the method name for a particular accessor
     * @param string $name The accessor being requested.
     * @return string The accessor's method name
     */
    protected function getAccessorName(string $name): string
    {
        return 'get' . str_replace(' ', '', ucwords(preg_replace('/[^a-z0-9]+/i', ' ', $name))) . 'Attribute';
    }
}
