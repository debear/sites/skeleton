<?php

namespace DeBear\ORM\Base\Traits;

use DeBear\Exceptions\ORMException;

trait DataSet
{
    /**
     * The list of modifications that have been made to the data since it was loaded
     * @var array
     */
    protected $columnChanges = [];

    /**
     * Update the column value for the current row
     * @param string $name  The name of the column being updated.
     * @param mixed  $value The new value to be applied.
     * @return void
     */
    public function __set(string $name, mixed $value): void
    {
        $this->root->setData($this->key, $name, $value);
    }

    /**
     * Update the column value of a row in the in-memory store
     * @param integer $key   The key in the data of the row being updated.
     * @param string  $name  The name of the column being updated.
     * @param mixed   $value The new value to be applied.
     * @throws ORMException When attempting to update an empty model object.
     * @return void
     */
    public function setData(int $key, string $name, mixed $value): void
    {
        if ($key == -1) {
            throw new ORMException('Unable to update the value of a model with no active record.');
        }
        $this->columnChanges["$key:$name"] = true;
        $this->data[$key][$name] = $value;
    }

    /**
     * Set/Update a property on the object model (rather than model data)
     * @param string $name  The property to be set/updated.
     * @param mixed  $value The value to be applied.
     * @return void
     */
    public function setProperty(string $name, mixed $value): void
    {
        $this->$name = $value;
    }

    /**
     * Remove the column value for the current row
     * @param string $name The column to be unset.
     * @return void
     */
    public function __unset(string $name): void
    {
        $this->root->unsetData($this->key, $name);
    }


    /**
     * Remove the column value of a row in the in-memory store
     * @param integer $key  The key in the data of the row being removed.
     * @param string  $name The name of the column being removed.
     * @return void
     */
    public function unsetData(int $key, string $name): void
    {
        unset($this->data[$key][$name]);
    }

    /**
     * Get the list of columns that have been updated in-memory for a given row
     * @param integer $key The key in the data of the row being requested (Default: The current row).
     * @return array The list of updated columns
     */
    public function getColumnChanges(?int $key = null): array
    {
        // This can only apply in the root object.
        if (!$this->isRoot) {
            return $this->root->getColumnChanges($this->key());
        }
        // As the root object, parse the changes array.
        $key = $key ?? $this->key();
        $key_cmp = "{$key}:";
        $key_cmp_len = strlen($key_cmp);
        return array_map(function ($col) use ($key_cmp_len) {
            return substr($col, $key_cmp_len);
        }, array_filter(array_keys($this->columnChanges), function ($key) use ($key_cmp, $key_cmp_len) {
            return (substr($key, 0, $key_cmp_len) == $key_cmp);
        }));
    }

    /**
     * Reset the list of columns that have been updated in-memory for a given row
     * @param integer $key The key in the data of the row being reset (Default: The current row).
     * @return void
     */
    public function resetColumnChanges(?int $key = null): void
    {
        // This can only apply in the root object.
        if (!$this->isRoot) {
            $this->root->resetColumnChanges($this->key());
            return;
        }
        // As the root object, parse the changes array.
        $key = $key ?? $this->key();
        $key_cmp = "{$key}:";
        $key_cmp_len = strlen($key_cmp);
        $this->columnChanges = array_filter($this->columnChanges, function ($key) use ($key_cmp, $key_cmp_len) {
            return (substr($key, 0, $key_cmp_len) != $key_cmp);
        }, ARRAY_FILTER_USE_KEY);
    }
}
