<?php

namespace DeBear\ORM\Base\Traits;

use DeBear\Helpers\Format;

trait Debug
{
    /**
     * Create a string representation of the object
     * @return string A text description of ourselves
     */
    public function __toString(): string
    {
        return '{{~~ DeBearORM: ' . static::class . ' :: ' . Format::pluralise($this->count(), ' record') . ' ~~}}';
    }

    /**
     * Create a new - unlinked - ORM object for this data.
     * @return self A new instance of the
     */
    public function toObject(): self
    {
        return new static($this->getData());
    }
}
