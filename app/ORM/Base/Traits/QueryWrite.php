<?php

namespace DeBear\ORM\Base\Traits;

use Illuminate\Support\Facades\Config as FrameworkConfig;

trait QueryWrite
{
    /**
     * Create an in-memory only instance of a model
     * @param array $data  The data applicable to the model.
     * @param array $flags Optional flags to be passed in the instantiation.
     * @return self The instantiated model object
     */
    public static function build(array $data, array $flags = []): self
    {
        return (new static($data, $flags));
    }

    /**
     * Create an in-memory instance of a model and commit it to the database
     * @param array $data  The data applicable to the model.
     * @param array $flags Optional flags to be passed in the instantiation.
     * @return self The instantiated model object
     */
    public static function create(array $data, array $flags = []): self
    {
        $model = static::build($data, $flags);
        $model->saveFull();
        return $model;
    }

    /**
     * Commit to the database only, one or more new models
     * @param array $data The data to be written to the database relating to this model.
     * @return void
     */
    public static function createMany(array $data): void
    {
        $faux_model = new static();
        // Get the columns to use in our query from our first object.
        $cols = array_keys($data[0]);
        $pk_cols = (array)$faux_model->getPrimaryKey();
        $odku_cols = array_diff($cols, $pk_cols);
        // Next flattern our arguments.
        $args = [];
        foreach ($data as $row) {
            foreach ($cols as $col) {
                $args[] = $row[$col];
            }
        }
        // Then run our query.
        $faux_model->doInsert($cols, $args, $odku_cols);
    }

    /**
     * Update the in-memory version of the current row in this model
     * @param array $changes The key/value pair of changes to the model data.
     * @return self The current model, allowing for chaining
     */
    public function update(array $changes): self
    {
        foreach ($changes as $key => $value) {
            $this->$key = $value;
        }
        // Return ourselves to allow chaining.
        return $this;
    }

    /**
     * Commmit any changes to the in-memory version of the current row to the database
     * @return void
     */
    public function save(): void
    {
        // Find out what was changed for this record.
        $cols = $args = [];
        $col_changes = $this->getColumnChanges();
        if (!count($col_changes)) {
            // If there aren't any changes, there's need to actually run anything, so skip.
            return;
        }
        $db_tz = FrameworkConfig::get('debear.datetime.timezone_db');
        foreach ($col_changes as $col) {
            $cols[] = $col;
            $args[] = (isset($this->dates[$col])) ? $this->$col->setTimezone($db_tz) : $this->$col;
        }
        // Determine the details of the keys in this table.
        $pk = [];
        $primary_key = (array)(new static())->getPrimaryKey();
        foreach ($primary_key as $col) {
            $pk[] = $col;
            $args[] = $this->$col;
        }
        // Finally build and run our query.
        $this->doUpdate($cols, $pk, $args);
        // Reset the changes, as we've committed them.
        $this->resetColumnChanges();
    }

    /**
     * Commit the current in-memory version of the current row to the database, even if nothing has changed
     * @return void
     */
    public function saveFull(): void
    {
        // Get the values to be stored.
        $data = $this->toArray();
        $cols = array_keys($data);
        $args = array_values($data);
        // Split the primary key column(s) from the data column(s).
        $pk_cols = (array)$this->getPrimaryKey();
        $odku_cols = array_diff($cols, $pk_cols);
        // Run our INSERT, getting back a (potential) auto-increment primary key.
        $insert_id = $this->doInsert($cols, $args, $odku_cols);
        if (isset($insert_id)) {
            // Determine which column it relates to (given the key could be compound).
            $reset_changes = false;
            foreach ($pk_cols as $col) {
                if (!isset($data[$col])) {
                    $this->$col = $insert_id;
                    $reset_changes = true;
                    break;
                }
            }
            // Reset the changes, as we've committed them.
            if ($reset_changes) {
                $this->resetColumnChanges();
            }
        }
    }

    /**
     * Delete the current model row from the database
     * @return void
     */
    public function deleteRow(): void
    {
        if (!$this->isset()) {
            return;
        }
        // Determine the details of the keys in this table and the relevant columns to the (single) row.
        $args = [];
        $primary_key = (array)(new static())->getPrimaryKey();
        foreach ($primary_key as $col) {
            $args[] = $this->$col;
        }
        // Finally build and run our query.
        $this->doDelete($primary_key, $args);
    }

    /**
     * Delete all rows in this model in the database
     * @return void
     */
    public function deleteAll(): void
    {
        if (!$this->count()) {
            return;
        }
        // Determine the details of the keys in this table and the relevant columns in the data.
        $args = [];
        $primary_key = (array)(new static())->getPrimaryKey();
        foreach ($this->getData() as $d) {
            foreach ($primary_key as $k) {
                $args[] = $d[$k];
            }
        }
        // Finally build and run our query.
        $this->doDelete($primary_key, $args);
    }
}
