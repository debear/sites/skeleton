<?php

namespace DeBear\ORM\Base\Traits;

use DeBear\ORM\Base\Instance as ORMBase;

trait Secondary
{
    /**
     * The list of secondary data and meta info we have stored for this model
     * @var array
     */
    protected $secondary = [];
    /**
     * Whether this is a secondary to another model (not null), and if so 1:m (true) or 1:1 (false)
     * @var boolean
     */
    protected $secondaryMultiple = null;

    /**
     * Lazy load secondary data about the main data being requested
     * @param array $list The list of internal references of secondary data.
     * @return self The current object, allowing for chaining
     */
    public function loadSecondary(array $list): self
    {
        // No point in running if no main data to process against.
        if ($this->count()) {
            foreach (array_unique($list) as $name) {
                // Look for a specific method for this additional data.
                $method = "loadSecondary$name";
                if (method_exists($this, $method)) {
                    $this->$method();
                    continue;
                }
                // Failing that, there'll be a standard method.
                $this->loadSecondaryDefault($name);
            }
        }
        // Return, allowing for chaining.
        return $this;
    }

    /**
     * If we have any required secondary details we should always run, run them now
     * @return self The current object, allowing for chaining
     */
    public function loadRequiredSecondaries(): self
    {
        // Return, allowing for chaining.
        return $this->loadSecondary($this->requiredSecondaries ?? []);
    }

    /**
     * State that the current model is secondary data in a 1:M relationship
     * @return self The current model, as this is a chaining operation
     */
    public function setMultipleSecondary(): self
    {
        $this->secondaryMultiple = true;
        return $this;
    }

    /**
     * State that the current model is secondary data in a 1:1 relationship
     * @return self The current model, as this is a chaining operation
     */
    public function setSingularSecondary(): self
    {
        $this->secondaryMultiple = false;
        return $this;
    }

    /**
     * Check if the current model is secondary data (in either a 1:1 or 1:M relationship)
     * @return boolean That the current model is secondary data to another model
     */
    public function isSecondary(): bool
    {
        return isset($this->secondaryMultiple);
    }

    /**
     * Check if the current model is secondary data in a 1:M relationship)
     * @return boolean That the current model is 1:M secondary data to another model
     */
    public function isMultipleSecondary(): bool
    {
        return $this->secondaryMultiple === true;
    }

    /**
     * Check if the current model is secondary data in a 1:1 relationship)
     * @return boolean That the current model is 1:1 secondary data to another model
     */
    public function isSingularSecondary(): bool
    {
        return $this->secondaryMultiple === false;
    }

    /**
     * Return information about the secondary data loaded against this model
     * @return array The info about all secondary fields loaded in this model
     */
    public function getSecondaryFields(): array
    {
        return array_map(function ($a) {
            $is_model = ($a instanceof ORMBase);
            return [
                'is_model' => $is_model,
                'is_resultset' => $is_model && $a->isMultipleSecondary(),
            ];
        }, $this->secondary);
    }
}
