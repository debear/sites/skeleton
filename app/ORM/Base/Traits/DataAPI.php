<?php

namespace DeBear\ORM\Base\Traits;

trait DataAPI
{
    /**
     * Attributes not to be formatted when representing a row in an API response
     * @var array
     */
    protected $apiFormats;
    /**
     * Attributes not to be included when representing a row in an API response
     * @var array
     */
    protected $hidden;
    /**
     * Attributes which should be masked in external representation
     * @var array
     */
    protected $mask;
    /**
     * Custom data used for processing the object into an API form
     * @var array
     */
    protected $apiCustomData;

    /**
     * Get the list of fields to be formatted in the model
     * @return array The columns containing values to be formatted, grouped by their format method
     */
    public function getFormattableFields(): array
    {
        return $this->apiFormats ?? [];
    }

    /**
     * Convert the full result set into an API response
     * @return array An array of none, one or more data rows for an API response
     */
    public function resultsToAPI(): array
    {
        $ret = [];
        foreach ($this as $row) {
            $ret[] = $row->rowToAPI();
        }
        return $ret;
    }

    /**
     * Convert the data into a paginated-ready payload
     * @param string $key Returned object key for the data element.
     * @return array The data in the object converted to an array
     */
    public function resultsToPaginatedAPI(string $key): array
    {
        return array_merge([$key => $this->resultsToAPI()], $this->resultsPagination());
    }

    /**
     * Convert the current row into an API response
     * @param array $data Custom data supplied from the caller to be supplied to any worker method calls.
     * @return array An array representing the current data row for an API response
     */
    public function rowToAPI(array $data = []): array
    {
        $this->apiCustomData = $data;

        // Before we start, any bespoke pre-processing?
        if (method_exists($this, 'preProcessAPIData')) {
            $this->preProcessAPIData();
        }
        // Get the row's data.
        $row = $this->toMaskedArray();
        // Ensure certain values are correctly typed.
        foreach ($this->getFormattableFields() as $fmt => $cols) {
            $method = (substr($fmt, -2) == 'dp' ? 'dp' : "{$fmt}val");
            $dp = (substr($fmt, -2) == 'dp' ? substr($fmt, 0, -2) : null);
            foreach ($cols as $col) {
                if (isset($row[$col])) {
                    $row[$col] = ($method != 'dp' ? $method($row[$col]) : (float)sprintf("%0.0{$dp}f", $row[$col]));
                }
            }
        }
        // Convert dates to an appropriate format - either date (Y-m-d) or ISO 8601.
        foreach ($this->getDateFields() as $col => $fmt) {
            if (isset($this->$col) && $fmt != 'Y-m-d') {
                $this->dates[$col] = 'c'; // Temporarily switch the format and convert.
                $row[$col] = (string)$this->$col;
                $this->dates[$col] = $fmt; // Restore.
            }
        }
        // Convert any SETs to an approriate format.
        foreach (array_keys($this->getSETFields()) as $col) {
            if (isset($this->$col)) {
                $row[$col] = $this->$col->toJSON();
            }
        }
        // Convert secondary data objects to their JSON representation.
        foreach ($this->getSecondaryFields() as $col => $info) {
            if ($info['is_model'] && isset($row[$col])) {
                $method = ($info['is_resultset'] ? 'resultsToAPI' : 'rowToAPI');
                $row[$col] = $row[$col]->$method();
            }
        }
        // Finally, any bespoke per-model processing?
        if (method_exists($this, 'postProcessAPIData')) {
            $this->postProcessAPIData($row);
        }
        // Return, removing fields we don't want to export.
        return array_diff_key($row, array_flip($this->hidden ?? []));
    }

    /**
     * Specify additional fields to be hidden when the model is rendered as an API
     * @param array $fields Additional field(s) to be excluded when converted to JSON.
     * @return self The current object, as this method is usually used in a chain
     */
    public function hideFieldsFromAPI(array $fields): self
    {
        $this->hidden = array_unique(array_merge($this->hidden ?? [], $fields));
        return $this;
    }

    /**
     * Convert our paged query into status info
     * @return array The data as paged counts
     */
    public function resultsPagination(): array
    {
        $pages_tot = ceil($this->totalRows() / $this->queryPerPage());
        $rows_from = ($this->queryPage() - 1) * $this->queryPerPage();
        return [
            'rows' => [
                'from' => $this->totalRows() ? $rows_from + 1 : 0,
                'to' => $this->totalRows() ? $rows_from + $this->count() : 0,
                'total' => $this->totalRows(),
            ],
            'pages' => [
                'curr' => $pages_tot ? (int)$this->queryPage() : 0,
                'total' => $pages_tot,
                'prev' => ($this->queryPage() > 1),
                'next' => ($this->queryPage() < $pages_tot),
            ],
        ];
    }
}
