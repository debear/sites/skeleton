<?php

namespace DeBear\ORM\Base\Traits;

trait Iterate
{
    /**
     * The current key in the iterator
     * @var integer
     */
    protected $key;
    /**
     * The current index in the iterator
     * @var integer
     */
    protected $index;

    /**
     * Return the pointer to the starting position
     * @return void
     */
    public function rewind(): void
    {
        $this->index = $this->count ? 0 : -1;
        $this->key = ($this->index >= 0 ? $this->keys[$this->index] : -1);
    }

    /**
     * The current object to be interrogated... which is really just ourself.
     * @return self The model object
     */
    public function current(): self
    {
        return $this;
    }

    /**
     * The array key for the current object
     * @return integer The key of the current pointer in our data array
     */
    public function key(): int
    {
        return $this->key;
    }

    /**
     * The current position of the iterator
     * @return integer The iterator position as an index (NOT array key)
     */
    public function index(): int
    {
        return $this->index;
    }

    /**
     * Move the internal pointer forward
     * @return void
     */
    public function next(): void
    {
        ++$this->index;
        $this->key = ($this->index < $this->count ? $this->keys[$this->index] : -1);
    }

    /**
     * Test if a key - not index - is valid in the context of the data we have
     * @param integer $key The key to be tested, or if not supplied then the current position.
     * @return boolean That the key being tested exists in our dataset
     */
    public function valid(?int $key = null): bool
    {
        if (!$this->isRoot) {
            return $this->root->valid($this->key());
        }
        return isset($this->data[$key ?? $this->key()]);
    }
}
