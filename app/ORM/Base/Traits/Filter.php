<?php

namespace DeBear\ORM\Base\Traits;

use Closure;
use DeBear\Exceptions\ORMException;

trait Filter
{
    /**
     * State the number of rows in this model
     * @return integer The number of data rows
     */
    public function count(): int
    {
        return $this->count;
    }

    /**
     * Return the paged query's page number, which is the theoretical page number not the query's OFFSET value
     * @return integer|null The page number of the results stated in the query, or null if not specified.
     */
    public function queryPage(): ?int
    {
        return $this->paging['page'] ?? null;
    }

    /**
     * Return the paged query's number of rows per page
     * @return integer|null The number of results "per page" stated in the query, or null if not specified.
     */
    public function queryPerPage(): ?int
    {
        return $this->paging['per_page'] ?? null;
    }

    /**
     * State the total number of rows in the non-LIMIT'd query
     * @return integer The total number of data rows available
     */
    public function totalRows(): int
    {
        return $this->totalCount;
    }

    /**
     * Get the contents of a particular column across all our models
     * @param string $data_key  The model's column we are requesting.
     * @param string $array_key The column we should use as the returned associative array's key (Optional).
     * @return array The associative array of values across all data in this model, keyed by the $array_key argument
     */
    public function pluck(string $data_key, string $array_key = '__orm_key'): array
    {
        $data = array_column($this->getData(), $data_key, $array_key);
        // If the data has been sorted (so keys are out of order), order the results as per the sort.
        if ($this->isSorted) {
            uksort($data, function ($a, $b) {
                return $this->keysSort[$a] <=> $this->keysSort[$b];
            });
        }
        return $data;
    }

    /**
     * The unique values in a given column across all rows in the model
     * @param string $column The model's column we are requesting.
     * @return array A numerically indexed array of unique values for the requested column
     */
    public function unique(string $column): array
    {
        return array_values(array_unique($this->pluck($column)));
    }

    /**
     * Get the raw data as an array at the given key
     * @param integer $key The key of the row in tge full dataset.
     * @return array The row's raw data
     */
    public function getIndex(int $key): array
    {
        return $key != -1 ? ($this->isRoot ? $this->data[$key] : $this->root->getIndex($this->keys[$key])) : [];
    }

    /**
     * Get a model row as its own (subset) object
     * @param integer $key The key of the row in tge full dataset.
     * @return self The subset model object
     */
    public function getRow(int $key): self
    {
        return $this->createSubset([$key]);
    }

    /**
     * Get the current row as its own (subset) object
     * @return self The subset model object
     */
    public function getCurrent(): self
    {
        return $this->getRow($this->key);
    }

    /**
     * Create a new subset model object for the specified keys
     * @param array $keys The keys for data in our new subset model object.
     * @return self The new subset model object
     */
    public function getByKeys(array $keys): self
    {
        return $this->createSubset($keys);
    }

    /**
     * Return a random row, or random number of rows if specified
     * @param integer $num The requested number of rows (Default: 1).
     * @return self The new subset model object with up to the specified number of rows
     */
    public function random(int $num = 1): self
    {
        // If we are dealing with an empty object, return ourselves (as an empty object).
        // Question: Could this be dangerous? Saves an object, but introduces potential risk.
        if (!$this->count()) {
            return $this;
        }
        $num = min($num, $this->count()); // Ensure we stay within array_rand's boundary.
        $rand = array_rand($this->keys, $num);
        // Ensure it's an array.
        $rand = ($num > 1 ? $rand : [$rand]);
        // Convert to the raw data keys.
        $keys = array_values(array_intersect_key($this->keys, array_fill_keys($rand, true)));
        return $this->createSubset($keys);
    }

    /**
     * Return a portion of the current model's data (linearly, by key)
     * @param integer $offset The starting point in the current model.
     * @param integer $length The number of rows to return. (Optional. Default: All remaining rows).
     * @return self The new subset model object with the requested rows
     */
    public function slice(int $offset, ?int $length = null): self
    {
        return $this->createSubset(array_slice($this->keys, $offset, $length));
    }

    /**
     * Return the first row as its own subset model object
     * @return self The new subset model object
     */
    public function first(): self
    {
        return $this->createSubset($this->count() ? [$this->keys[0]] : []);
    }

    /**
     * Return the last row as its own subset model object
     * @return self The new subset model object
     */
    public function last(): self
    {
        return $this->createSubset($this->count() ? [$this->keys[$this->count - 1]] : []);
    }

    /**
     * Paginate the results for the given page definition as its own subset model object
     * @param integer $page     The page number we are accessing data for.
     * @param integer $per_page The number of items we are accessing per page.
     * @return self The new subset model object
     */
    public function forPage(int $page, int $per_page): self
    {
        $offset = ($page - 1) * $per_page;
        return $this->slice($offset, $per_page);
    }

    /**
     * Return the object with the order directly reversed
     * @return self The new subset model object
     */
    public function reverse(): self
    {
        return $this->createSubset(array_reverse($this->keys));
    }

    /**
     * Sort the rows by a given Closure and return as a new subset model object
     * @param array|Closure $criteria How to sort the rows.
     * @return self The new subset model object
     */
    public function sort(array|Closure $criteria): self
    {
        $data = $this->getData();
        uasort($data, $criteria);
        return $this->createSubset(array_column($data, '__orm_key'), ['sorted' => true]);
    }

    /**
     * Sort the rows by a given Closure, semantically in "descending order", and return as a new subset model object
     * @param array|Closure $criteria How to sort the rows.
     * @return self The new subset model object
     */
    public function sortDesc(array|Closure $criteria): self
    {
        // All the directional logic is actually within the Closure, so this is just a synonym for the sort() method.
        return $this->sort($criteria);
    }

    /**
     * Sort the rows by a given column, columns or closure
     * @param string|array|Closure $criteria How to sort the rows.
     * @param boolean              $reverse  If we are reversing the rows after applying the passed sort order.
     * @return self The new subset model object
     */
    public function sortBy(string|array|Closure $criteria, bool $reverse = false): self
    {
        if ($criteria instanceof Closure || (is_array($criteria) && is_callable($criteria))) {
            // Closures and callables are dealt with by the sort() method.
            return $this->sort($criteria);
        }

        // A single array element should be treated as a string.
        if (is_array($criteria) && (count($criteria) == 1)) {
            $criteria = $criteria[0];
        }
        // Scalar lookup can be done quickly.
        if (is_string($criteria)) {
            // By a fixed column.
            $data = $this->pluck($criteria);
            natsort($data);
            return $this->createSubset(array_keys($data), ['sorted' => true, 'reverse' => $reverse]);
        }

        // An array lookup needs a more recusrive method.
        $max_depth = count($criteria);
        $data = $this->getData();
        uasort($data, function ($a, $b) use ($criteria, $max_depth) {
            for ($i = 0; $i < $max_depth; $i++) {
                if ($a[$criteria[$i]] != $b[$criteria[$i]]) {
                    // We can break by this criteria.
                    return $a[$criteria[$i]] <=> $b[$criteria[$i]];
                }
            }
            // No criteria are breakable, so the items are the same.
            return 0;
        });
        return $this->createSubset(array_column($data, '__orm_key'), ['sorted' => true, 'reverse' => $reverse]);
    }

    /**
     * Sort the rows by a given column, columns or closure, semantically in "descending order"
     * @param string|array|Closure $criteria How to sort the rows.
     * @return self The new subset model object
     */
    public function sortByDesc(string|array|Closure $criteria): self
    {
        // Re-use our ASC logic, but reversing the final results.
        return $this->sortBy($criteria, true);
    }

    /**
     * Return a subset of the model rows by a given closure
     * @param  Closure $filter The logic by which we decide whether to include a row or not.
     * @return self The new subset model object
     */
    public function filter(Closure $filter): self
    {
        return $this->createSubset(array_column(array_filter($this->getData(), $filter), '__orm_key'));
    }

    /**
     * Return the subset of the model rows based on the values in a given column
     * @param string $column      The column to perform the check against.
     * @param string $op_or_value The operator, or value if shorthand = check.
     * @param mixed  $value       The value to check, if an operator is specified.
     * @return self The new subset model object
     */
    public function where(string $column, string $op_or_value, mixed $value = null): self
    {
        $inverse = false;
        // Shorthand arguments, where $op_or_value is a value (defined as $value == null).
        if (!isset($value)) {
            $op = '=';
            $value = $op_or_value;
        } else {
            $op = $op_or_value;
        }
        // Define how we calculate our test. Boolean indicates inverse of test result.
        $is_eq = ['=' => false, '!=' => true, '<>' => true];
        $is_lt = ['<' => false, '>=' => true];
        $is_gt = ['>' => false, '<=' => true];
        // Run to get the appropriate indices and return the subset.
        $data = $this->pluck($column);
        if (isset($is_eq[$op])) {
            // - Equality based.
            $matches = array_keys($data, $value);
            $inverse = $is_eq[$op];
        } elseif (isset($is_lt[$op])) {
            // - Less than / Greater than or equals.
            $matches = array_keys(array_filter($data, function ($item) use ($value) {
                return $item < $value;
            }));
            $inverse = $is_lt[$op];
        } else {
            // - Greater than / Less than or equals.
            $matches = array_keys(array_filter($data, function ($item) use ($value) {
                return $item > $value;
            }));
            $inverse = $is_gt[$op];
        }
        return $this->createSubset($matches, ['inverse' => $inverse]);
    }

    /**
     * Return the subset of the model rows based on the values in a given column not matching a particular value
     * @param string $column The column to perform the check against.
     * @param mixed  $value  The value to check against.
     * @return self The new subset model object
     */
    public function whereNot(string $column, mixed $value): self
    {
        return $this->where($column, '!=', $value);
    }

    /**
     * Return the subset of the model rows where the values of a particular column are between two specified values
     * @param string  $column  The column to perform the check against.
     * @param mixed   $min     The lower boundary.
     * @param mixed   $max     The upper boundary.
     * @param boolean $inverse If we are returning the rows that actually do NOT match the range.
     * @return self The new subset model object
     */
    public function whereBetween(string $column, mixed $min, mixed $max, bool $inverse = false): self
    {
        $data = $this->pluck($column);
        $matches = array_keys(array_filter($data, function ($item) use ($min, $max) {
            return isset($item) && ($min <= $item) && ($item <= $max);
        }));
        return $this->createSubset($matches, ['inverse' => $inverse]);
    }

    /**
     * Return the subset of the model rows where the values of a particular column are not between two specified values
     * @param string $column The column to perform the check against.
     * @param mixed  $min    The lower boundary.
     * @param mixed  $max    The upper boundary.
     * @return self The new subset model object
     */
    public function whereNotBetween(string $column, mixed $min, mixed $max): self
    {
        return $this->whereBetween($column, $min, $max, true);
    }

    /**
     * Return the subset of the model rows where the values of a particular column match the supplied list
     * @param string  $column  The column to perform the check against.
     * @param array   $values  The value(s) to match.
     * @param boolean $inverse If we are returning the rows that actually do NOT match the list.
     * @return self The new subset model object
     */
    public function whereIn(string $column, array $values, bool $inverse = false): self
    {
        $values = array_flip(array_filter($values, function ($v) {
            return isset($v);
        })); // For an optimised identification.
        $data = $this->pluck($column);
        $matches = array_keys(array_filter($data, function ($item) use ($values) {
            return isset($values[$item]);
        }));
        return $this->createSubset($matches, ['inverse' => $inverse]);
    }

    /**
     * Return the subset of the model rows where the values of a particular column do not match the supplied list
     * @param string $column The column to perform the check against.
     * @param array  $values The value(s) we do not want to match.
     * @return self The new subset model object
     */
    public function whereNotIn(string $column, array $values): self
    {
        return $this->whereIn($column, $values, true);
    }

    /**
     * Return the subset of the model rows where the values of a particular column are NULL
     * @param string  $column  The column to perform the check against.
     * @param boolean $inverse If we are returning the rows that are actually NOT NULL.
     * @return self The new subset model object
     */
    public function whereNull(string $column, bool $inverse = false): self
    {
        $data = $this->pluck($column);
        $nulls = array_keys(array_filter($data, function ($item) {
            return !isset($item);
        }));
        return $this->createSubset($nulls, ['inverse' => $inverse]);
    }

    /**
     * Return the subset of the model rows where teh values of a particular column are NOT NULL
     * @param string $column The column to perform the check against.
     * @return self The new subset model object
     */
    public function whereNotNull(string $column): self
    {
        return $this->whereNull($column, true);
    }

    /**
     * Return the numerical mean of the values in a columm
     * @param string $column The column to perform the check against.
     * @throws ORMException When there is an empty model, we cannot have a mean value.
     * @return float The numerical mean
     */
    public function avg(string $column): float
    {
        if (!$this->count()) {
            throw new ORMException("Unable to retrieve the mean value of an empty list.");
        }
        return $this->sum($column) / $this->count();
    }

    /**
     * Return the numerical median of the values in a columm
     * @param string $column The column to perform the check against.
     * @throws ORMException When there is an empty model, we cannot have a median value.
     * @return float The numerical median
     */
    public function median(string $column): float
    {
        $num = $this->count();
        if (!$num) {
            throw new ORMException("Unable to retrieve the median value of an empty list.");
        }
        $values = $this->pluck($column);
        sort($values);
        if ($num % 2) {
            // Direct lookup.
            return $values[$num / 2];
        } else {
            // Mean between the middle two.
            $l = intval(floor($num / 2) - 1); // Zero-indexed values!
            $r = $l + 1;
            return ($values[$l] + $values[$r]) / 2;
        }
    }

    /**
     * Return the lowest value in a columm
     * @param string $column The column to perform the check against.
     * @throws ORMException When there is an empty model, we cannot have a lowest value.
     * @return float|string The lowest value in the column
     */
    public function min(string $column): float|string
    {
        if (!$this->count()) {
            throw new ORMException("Unable to retrieve the mean value of an empty list.");
        }
        return min($this->pluck($column));
    }

    /**
     * Return the highest value in a columm
     * @param string $column The column to perform the check against.
     * @throws ORMException When there is an empty model, we cannot have a highest value.
     * @return float|string The highest value in the column
     */
    public function max(string $column): float|string
    {
        if (!$this->count()) {
            throw new ORMException("Unable to retrieve the mean value of an empty list.");
        }
        return max($this->pluck($column));
    }

    /**
     * Return the sum of all values in a columm
     * @param string $column The column to perform the check against.
     * @return float The total of all values in the column
     */
    public function sum(string $column): float
    {
        return array_sum($this->pluck($column));
    }
}
