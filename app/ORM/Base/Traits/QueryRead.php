<?php

namespace DeBear\ORM\Base\Traits;

use DeBear\ORM\Base\Helpers\QueryBuilder;

trait QueryRead
{
    /**
     * Create a new (DeBearORM) QueryBuilder object for this
     * @param string $tbl_from An over-ride of the query's starting table name (Optional).
     * @return QueryBuilder The DeBearORM object based on this model
     */
    public static function query(?string $tbl_from = null): QueryBuilder
    {
        return new QueryBuilder(static::class, $tbl_from);
    }

    /**
     * Short-hand method to request all data in the model's table. Use with caution!
     * @return self An instance of the model object containing all the rows in the table
     */
    public static function all(): self
    {
        return static::query()->select('*')->get();
    }
}
