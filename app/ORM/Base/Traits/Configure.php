<?php

namespace DeBear\ORM\Base\Traits;

use DeBear\Helpers\Arrays;
use DeBear\Helpers\Strings;
use DeBear\ORM\Base\Instance;

trait Configure
{
    /**
     * The root object we are linked to
     * @var self
     */
    protected $root;
    /**
     * The raw data in this model
     * @var array
     */
    protected $data;
    /**
     * All keys as array values
     * @var array
     */
    protected $keys;
    /**
     * All keys with sort order as value
     * @var array
     */
    protected $keysSort;
    /**
     * The total number of rows in this model
     * @var integer
     */
    protected $count;
    /**
     * Query pagination info
     * @var array
     */
    protected $paging;
    /**
     * The total number of rows for the full (LIMIT'd) query
     * @var integer
     */
    protected $totalCount;
    /**
     * An array of date fields and formats within the data
     * @var array
     */
    protected $dates;
    /**
     * That we are the root node
     * @var boolean
     */
    protected $isRoot;
    /**
     * Data has been sorted since initial load.
     * @var boolean
     */
    protected $isSorted;

    /**
     * Prepare the root object for this model, expected to be the first time it is called
     * @param array $data  The full dataset for the model.
     * @param array $flags Optional information about the data being supplied.
     * @param array $extra Extra information relevant to the model(s).
     * @return void
     */
    protected function setupRoot(array $data = [], array $flags = [], array $extra = []): void
    {
        // If we're the passed data for a single model, convert it to a compatible structure.
        if (count($data) && Arrays::isAssociative($data)) {
            $data = [$data];
        }
        // Now process our internals.
        $this->root = $this;
        $this->isRoot = true;
        $this->setupData($data, $flags);
        $this->processExtra($extra);
        $this->isSorted = false;
        $this->setupDates();
        $this->rewind();
    }

    /**
     * Prepare the model's data within the object
     * @param array $data  The data being added to the model object.
     * @param array $flags Information about the data being supplied.
     * @return void
     */
    protected function setupData(array $data, array $flags): void
    {
        $this->data = $data;
        $this->keys = array_keys($data);
        // Determine which, if any, fields we considerd trusted and so do not need cleaning.
        $trusted = (isset($flags['trusted']) && $flags['trusted']);
        // Internal processing to include the ORM key within the data.
        array_walk($this->data, function (&$item, $index) use ($trusted) {
            if (!$trusted) {
                $item = Strings::encodeInput($item);
            }
            $item['__orm_key'] = $index;
        });
        $this->setupCommon();
    }

    /**
     * Prepare the object as a subset of another root model
     * @param Instance $root The root model object we are linked to.
     * @param array    $keys The keys within the root model we relate to.
     * @param array    $opt  Configuration options passed in relating to the subset data.
     * @return void
     */
    protected function setupSubset(Instance $root, array $keys, array $opt): void
    {
        $this->root = $root;
        $this->isRoot = false;
        $this->data = null;
        $this->keys = $keys;
        $this->dates = $root->getDateFields();
        $this->setupCommon();
        $this->rewind();
        foreach ($opt as $key => $val) {
            $this->$key = $val;
        }
    }

    /**
     * Some fine-tuning that occurs when setting up the original and subset objects
     * @return void
     */
    protected function setupCommon(): void
    {
        $this->count = count($this->keys);
        $this->keysSort = array_flip($this->keys);
    }

    /**
     * Prepare date-related configuration for the model.
     * @return void
     */
    protected function setupDates(): void
    {
        $eloquent_class = str_replace('ORM', 'Models', static::class);
        // Get the date fields from the Eloquent model, with a default (full datetime) format.
        $this->dates = array_fill_keys(call_user_func([$eloquent_class, 'staticGetDates']), 'Y-m-d H:i:s');
        // Then add any specific format customisations (also defined in the Eloquent model).
        $casts = array_map(function ($item) {
            return substr($item, strpos($item, ':') + 1);
        }, array_filter(call_user_func([$eloquent_class, 'staticGetCasts']), function ($cast) {
            return substr($cast, 0, 4) == 'date';
        }));
        $this->dates = array_merge($this->dates, $casts);
    }

    /**
     * Process the array of extra model information
     * @param array $extra Extra information relevant to the model(s).
     * @return void
     */
    protected function processExtra(array $extra): void
    {
        // Paged query?
        if (isset($extra['pagination'])) {
            $this->paging = $extra['pagination'];
        }
        // Total Row count?
        if (isset($extra['total-rows'])) {
            $this->totalCount = $extra['total-rows'];
        }
    }

    /**
     * Add additional data in to an existing model object
     * @param Instance $extra The other instance we are merging data from.
     * @return Instance The original object having data added (to allow for chanining).
     */
    public function merge(Instance $extra): Instance
    {
        $this->setupRoot(array_merge($this->getData(), $extra->getData()), ['trusted' => true]);
        return $this;
    }

    /**
     * Create a new model object that is a subset of a root model object
     * @param array $keys The keys comprising the subset data.
     * @param array $opt  Configuration options that apply to the subset data.
     * @return self The new subset model object
     */
    protected function createSubset(array $keys, array $opt = []): self
    {
        // Status values to copy across to the subset creator.
        $sub_opt = [
            'isSorted' => $this->isSorted,
            'hidden' => $this->hidden,
            'secondary' => $this->secondary,
            'secondaryMultiple' => $this->secondaryMultiple,
        ];
        // Any last minute adjustments?
        if (count($opt)) {
            // Flags?
            if (isset($opt['sorted']) && $opt['sorted'] && !$sub_opt['isSorted']) {
                $sub_opt['isSorted'] = true;
            }
            // Manipulations?
            if (isset($opt['inverse']) && $opt['inverse']) {
                // We actually want the logical inverse of the matching results.
                $keys = array_values(array_diff($this->keys, $keys));
            } elseif (isset($opt['reverse']) && $opt['reverse']) {
                $keys = array_reverse($keys);
            }
        }
        return new static($this->root, $keys, $sub_opt);
    }
}
