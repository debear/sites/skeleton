<?php

namespace DeBear\ORM\Base\Traits;

use Illuminate\Support\Facades\DB;
use Exception;
use DeBear\Exceptions\ORMException;
use DeBear\Helpers\HTTP;
use DeBear\Repositories\DatabaseLogging;
use DeBear\Repositories\Logging;

trait DataSave
{
    /**
     * Perform an INSERT statement
     * @param array $insert_col The list of columns being applied.
     * @param array $args       The data to be inserted.
     * @param array $odku_col   The (optional) list of columns to generate an ON DUPLICATE KEY UPDATE clause.
     * @return ?integer If a single statement, the auto_increment ID of the last row inserted
     */
    protected function doInsert(array $insert_col, array $args, array $odku_col = []): ?int
    {
        $tbl = static::getTable();
        // Split our values into appropriate (?, ..., ?) blocks.
        $num_rows = count($args) / count($insert_col);
        $rows = array_fill(0, $num_rows, '(' . join(', ', array_fill(0, count($insert_col), '?')) . ')');
        // Is there an ON DUPLICATE KEY UPDATE clause?
        $odku = '';
        if (count($odku_col)) {
            $odku_arg = [];
            foreach ($odku_col as $col) {
                $odku_arg[] = "`$col` = VALUES(`$col`)";
            }
            $odku = ' ON DUPLICATE KEY UPDATE ' . join(', ', $odku_arg);
        }
        // Now build our query and run.
        $this->doQuery(
            "INSERT INTO $tbl (`" . join('`, `', $insert_col) . '`) VALUES ' . join(', ', $rows) . "$odku;",
            $args
        );
        // Are we retrieving a LAST_INSERT_ID (presently only on single row INSERTs)?
        if ($num_rows > 1) {
            return null;
        }
        return static::queryLastInsertID();
    }

    /**
     * Perform an UPDATE statement
     * @param array $update_col The columns being updated.
     * @param array $pk_col     The columns that define the primary key components.
     * @param array $args       The values to be applied to the UPDATE.
     * @return void
     */
    protected function doUpdate(array $update_col, array $pk_col, array $args): void
    {
        $tbl = static::getTable();
        // Determine the columns we're changing and the columns in the primary key.
        $updates = $pk = [];
        foreach ($update_col as $col) {
            $updates[] = "`$col` = ?";
        }
        foreach ($pk_col as $col) {
            $pk[] = "`$col` = ?";
        }
        // Now build our query and run.
        $this->doQuery("UPDATE $tbl SET " . join(', ', $updates) . ' WHERE ' . join(' AND ', $pk) . ';', $args);
    }

    /**
     * Perform a DELETE statement
     * @param array $pk_col THe columns that define the primary key components.
     * @param array $args   The values to be applied to the DELETE.
     * @return void
     */
    protected function doDelete(array $pk_col, array $args): void
    {
        $tbl = static::getTable();
        // Build our base primary key clause, and then repeat as required.
        $pk = [];
        foreach ($pk_col as $col) {
            $pk[] = "`$col` = ?";
        }
        $pk = array_fill(0, count($args) / count($pk_col), join(' AND ', $pk));
        // Now our final query and run.
        $this->doQuery("DELETE FROM $tbl WHERE " . join(' OR ', $pk) . ';', $args);
    }

    /**
     * Query worker method
     * @param string $sql  The SQL to be run.
     * @param array  $args Any binding arguments to be passed (Optional).
     * @throws ORMException ORM specific exception mirroring the default Exception.
     * @return void
     */
    protected function doQuery(string $sql, array $args = []): void
    {
        $conn_name = static::getConnectionName();
        $pdo = DB::connection($conn_name)->getPdo();
        try {
            $bench_start = microtime(true);
            $sth = $pdo->prepare($sql);
            $sth->execute($args);
            $bench_stop = microtime(true);
            // Log the query.
            $bench = ($bench_stop - $bench_start) * 1000; // Converting the bench time from nanoseconds to milliseconds.
            DatabaseLogging::logORMQuery($conn_name, $sql, $args, $bench);
        } catch (Exception $e) {
            // Log the error.
            Logging::add('ORM', sprintf(
                "doQuery() :: %s :: Unable to run query: '%s' :: Error: '%s'.",
                $conn_name,
                $sql,
                $e->getMessage()
            ));
            // Throw our own exception.
            $err_msg = 'Unable to run ORM query' . (!HTTP::isLive() ? ': ' . $e->getMessage() : '');
            throw new ORMException($err_msg);
        }
    }

    /**
     * Get back the ID of the last row inserted
     * @return ?integer The auto_increment ID of the last row inserted
     */
    protected function queryLastInsertID(): ?int
    {
        $pdo = DB::connection(static::getConnectionName())->getPdo();
        $last_id = $pdo->lastInsertId();
        return is_numeric($last_id) && $last_id > 0
            ? (int)$last_id // We have a non-zero number, which must be the ID we are after.
            : null; // We could not determine an appropriate numeric value, so expect no insert ID.
    }
}
