<?php

namespace DeBear\ORM\Base\Traits;

use DeBear\Models\Skeleton\UserActivityLog;

trait CommonEloquent
{
    /**
     * Record an audit event for the database change
     * @param string $type    The category of audit log.
     * @param string $summary A summary explanation for the audit record.
     * @param array  $fields  A list of fields to which our audit detail is to be limited.
     * @return self The current model, allowing for chaining
     */
    public function audit(string $type, string $summary, array $fields): self
    {
        // Include appropriate data in the log entry.
        $data = (method_exists($this, 'toMaskedArray') ? $this->toMaskedArray() : $this->toArray());
        UserActivityLog::record($type, $summary, array_intersect_key($data, array_flip($fields)));
        // Return ourselves to allow chaining.
        return $this;
    }
}
