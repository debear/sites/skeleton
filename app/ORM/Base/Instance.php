<?php

namespace DeBear\ORM\Base;

use Countable; // Because this can't be part of the Filter trait.
use Iterator; // Because this can't be part of the Iterate trait.
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\ORM\Base\Traits\QueryRead;
use DeBear\ORM\Base\Traits\QueryWrite;
use DeBear\ORM\Base\Traits\Configure;
use DeBear\ORM\Base\Traits\Secondary;
use DeBear\ORM\Base\Traits\Iterate;
use DeBear\ORM\Base\Traits\DataGet;
use DeBear\ORM\Base\Traits\DataSet;
use DeBear\ORM\Base\Traits\DataSave;
use DeBear\ORM\Base\Traits\DataAPI;
use DeBear\ORM\Base\Traits\Filter;
use DeBear\ORM\Base\Traits\Debug;
use DeBear\ORM\Base\Traits\CommonEloquent;

class Instance implements Countable, Iterator
{
    use QueryRead;
    use Configure;
    use Secondary;
    use Iterate;
    use DataGet;
    use DataSet;
    use DataSave;
    use DataAPI;
    use Filter;
    use QueryWrite;
    use Debug;
    use CommonEloquent;

    /**
     * Prepare our object, depending on what input we received.
     */
    public function __construct()
    {
        // Triage the arguments passed in to determine what we have - the initial data, or a subset.
        $args = func_get_args();
        if (!sizeof($args) || gettype($args[0]) == 'array') {
            // Initial (root) data.
            $this->setupRoot(...$args);
        } else {
            // A subset of the root data (which Phan does not appear to like).
            /* @phan-suppress-next-line PhanParamTooFewUnpack */
            $this->setupSubset(...$args);
        }
    }

    /**
     * Determine the full class name of the equivalent Eloquent model
     * @return string The name of our equivalent Eloquent model
     */
    protected static function getEloquentClassName(): string
    {
        return str_replace('DeBear\ORM', 'DeBear\Models', static::class);
    }

    /**
     * Get the internal name of the internal name this model uses
     * @return string The internal name for the database connection this model uses
     */
    public static function getConnectionName(): string
    {
        $model = static::getEloquentClassName();
        return (new $model())->getConnectionName();
    }

    /**
     * Get the name of the database this model relates to
     * @return string The name of the database
     */
    public static function getDatabase(): string
    {
        return FrameworkConfig::get('database.connections.' . static::getConnectionName() . '.database');
    }

    /**
     * Get the name of the database table this model relates to
     * @return string The name of the database table
     */
    public static function getTable(): string
    {
        $model = static::getEloquentClassName();
        return (new $model())->getTable();
    }

    /**
     * Get the primary key column(s) for this model
     * @return string|array The primary key column(s) for this model
     */
    public static function getPrimaryKey(): string|array
    {
        $model_name = static::getEloquentClassName();
        $model = new $model_name();
        return $model->getKeysCompound() ?? $model->getKeysPrimary();
    }
}
