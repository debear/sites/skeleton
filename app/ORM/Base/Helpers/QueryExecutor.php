<?php

namespace DeBear\ORM\Base\Helpers;

use Illuminate\Support\Facades\DB;
use Exception;
use DeBear\Exceptions\ORMException;
use DeBear\Helpers\HTTP;
use DeBear\Repositories\DatabaseLogging;
use DeBear\Repositories\Logging;

class QueryExecutor
{
    /**
     * The external SQL query|ies to run
     * @var array
     */
    protected static $sql = [];

    /**
     * Run a given SQL statement on the appropriate database connection
     * @param string $conn_name The internal database connection name to run the query via.
     * @param string $sql       The SQL string to be executed.
     * @throws ORMException ORM specific exception mirroring the default Exception.
     * @return integer The number of rows affected by our query
     */
    public static function statement(string $conn_name, string $sql): int
    {
        $pdo = DB::connection($conn_name)->getPdo();
        // Run the query, benchmarking it.
        try {
            $bench_start = microtime(true);
            $affected = $pdo->exec($sql);
            $bench_stop = microtime(true);
            // Log the query.
            $bench = ($bench_stop - $bench_start) * 1000; // Converting the bench time from nanoseconds to milliseconds.
            DatabaseLogging::logORMQuery($conn_name, $sql, [], $bench);
        } catch (Exception $e) {
            // Log the error.
            Logging::add('ORM', sprintf(
                "QueryExecutor :: %s :: Unable to execute statement :: (Masking Query) :: Error: '%s'.",
                $conn_name,
                $e->getMessage()
            ));
            // Throw our own exception.
            $err_msg = 'Unable to execute ORM statement' . (!HTTP::isLive() ? ': ' . $e->getMessage() : '');
            throw new ORMException($err_msg);
        }

        // Now return the number of affected rows.
        return $affected;
    }

    /**
     * Run one or more queries defined in an external template file.
     * @param string $conn_name The internal database connection name to run the query via.
     * @param string $path      Path to the external file containing the SQL to run.
     * @param array  $interp    Interpolation variables (simple, for now) to apply to the SQL.
     * @throws ORMException When the file cannot be loaded / executed.
     * @return void
     */
    public static function runTemplate(string $conn_name, string $path, array $interp = []): void
    {
        // The use of MD5 is not for cryptographic purposes, so consider the SemGrep warning a false positive.
        // nosemgrep PHPCS_SecurityAudit.BadFunctions.CryptoFunctions.WarnCryptoFunc.
        $path_ref = substr(md5($path), 0, 12);
        // Have we loaded the SQL yet?
        if (!isset(static::$sql[$path_ref])) {
            if (file_exists($path)) {
                static::$sql[$path_ref] = require $path;
            } else {
                // Log the error.
                Logging::add('ORM', sprintf("QueryExecutor :: %s :: Cannot open file '%s'.", $conn_name, $path));
                // Throw our own exception.
                $err_msg = 'Unable to open template file' . (!HTTP::isLive() ? ": Cannot open '$path'." : '');
                throw new ORMException($err_msg);
            }
        }
        // Apply the interpolation (without tainting the raw version).
        $sql = str_replace(array_keys($interp), array_values($interp), static::$sql[$path_ref]);
        foreach ($sql as $q) {
            static::statement($conn_name, $q);
        }
    }
}
