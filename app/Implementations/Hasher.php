<?php

// Due to an issue with compatibility with the base versions in Hasher(Base), do
// not apply the Squiz.Commenting.FunctionComment rule to check function argument signatures.
// phpcs:disable Squiz.Commenting.FunctionComment

namespace DeBear\Implementations;

use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Contracts\Hashing\Hasher as HasherBase;
use DeBear\Helpers\Strings;

class Hasher implements HasherBase
{
    /**
     * Get information about the given hashed value
     * @param string $hashedValue The hashed password to analyse.
     * @return array  Some information about the password hash
     */
    public function info(/* string */ $hashedValue) /* : array */
    {
        return password_get_info($hashedValue);
    }

    /**
     * Hash a plain text password into a cipher text version
     * @param string $value   The plain text password to hash.
     * @param array  $options Custom hashing options.
     * @return string The hashed password
     */
    public function make(/* string */ $value, array $options = []) /* : string */
    {
        // Generate the string salt.
        if (!isset($options['salt'])) {
            $options['salt'] = FrameworkConfig::get('debear.salts.hasher');
        }
        // And return our generated one-way hash.
        return Strings::oneWayHash($value, $options['salt'], $options);
    }

    /**
     * Check the given plain text value against an existing hash
     * @param string $value       The plain text password to check.
     * @param string $hashedValue The comparison, already hashed, password.
     * @param array  $options     (Optional) Custom hashing options.
     * @return bool Return status of the check
     */
    public function check(/* string */ $value, $hashedValue, array $options = []) /* : bool */
    {
        // Legacy: 12 char hash?
        if ($this->make($value, array_merge($options, ['len' => 12])) === $hashedValue) {
            return true;
        }
        // Standard width check.
        return $this->make($value, $options) === $hashedValue;
    }

    /**
     * Determine if an existing hashed password needs to be re-hashed to another method
     * @param string $hashedValue The hashed password to check.
     * @param array  $options     (Optional) Custom processing options.
     * @return bool Whether or not the password should be re-hashed
     */
    public function needsRehash(/* string */ $hashedValue, array $options = []) /* : bool */
    {
        // In this implementation, no re-hashing is ever required.
        return false;
    }
}
