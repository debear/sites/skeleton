<?php

namespace DeBear\Implementations;

use SessionHandlerInterface;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Support\Facades\DB;
use DeBear\Helpers\Strings;
use DeBear\Helpers\SessionAggregation;
use DeBear\Models\Skeleton\Session;
use DeBear\Repositories\Time;

class SessionHandler implements SessionHandlerInterface
{
    /**
     * The data associated with the UID cookie
     * @var string
     */
    protected $session_data;
    /**
     * TTL for the session within the database
     * @var integer
     */
    protected $max_lifetime;

    /**
     * Constructor: Start the session, and register the main session variables
     */
    public function __construct()
    {
        $this->session_data = '';
        // Time management from config.
        $this->max_lifetime = (FrameworkConfig::get('debear.sessions.lifetime') * 60);
    }

    /**
     * Expected components for session management
     */

    // Due to an issue with compatibility with the base versions of the session handler, do
    // not apply the Squiz.Commenting.FunctionComment rule to check function argument signatures.
    // phpcs:disable Squiz.Commenting.FunctionComment

    /**
     * Open the session... by doing nothing (this method shouldn't apply here)
     * @param string $save_path    The filesystem location to save the session data to
     * @param string $session_name Name of the session being opened
     * @return boolean Success flag
     */
    public function open(/* string */ $save_path, /* string */ $session_name): bool
    {
        return true;
    }

    /**
     * Close the session
     * @return boolean Success flag
     */
    public function close(): bool
    {
        // If there's already data, consider garbage collecting.
        return !empty($this->session_data)
            ? $this->gc((int)ini_get('session.gc_maxlifetime'))
            : true;
    }

    /**
     * Read data for this session
     * @param string $session_id The session we are trying to load.
     * @return string|false The data for the given session
     */
    public function read(/* string */ $session_id): string|false
    {
        if (FrameworkConfig::get('debear.sessions.handler.enabled')) {
            $session = Session::where([
                ['session_id', '=', Strings::encodeString($session_id)],
                ['last_accessed', '>=', App::make(Time::class)->adjustFormatServer("-{$this->max_lifetime} seconds")],
            ])->first();
        } else {
            // On test, we are purely stateless...
            $session = null;
        }
        $this->session_data = isset($session) ? $this->decodeSessionData($session->data) : '';
        return $this->session_data;
    }

    /**
     * Write data for this session
     * @param string $session_id   The session we are trying to store.
     * @param string $session_data The data associated with this session to be stored that has already been
     * serialised by the framework.
     * @return boolean Success flag
     */
    public function write(/* string */ $session_id, /* array */ $session_data): bool
    {
        // Skip in our stateless testing setup.
        if (!FrameworkConfig::get('debear.sessions.handler.enabled')) {
            return true;
        }

        // Current info.
        $agg_id = SessionAggregation::getID();
        Session::createOrUpdate(
            [
                'session_id' => $session_id,
                'session_id_uniq' => $agg_id ? (string)$agg_id : null,
                'session_started' => App::make(Time::class)->formatServer(),
                'last_accessed' => App::make(Time::class)->formatServer(),
                'data' => $this->encodeSessionData($session_data),
            ],
            ['last_accessed', 'data']
        );
        // Session aggregation/stats?
        SessionAggregation::store();

        return true;
    }

    /**
     * Destroy this session
     * @param string $session_id The session being removed from history.
     * @return boolean Success flag
     */
    public function destroy(/* string */ $session_id): bool
    {
        // Skip in our stateless testing setup.
        if (FrameworkConfig::get('debear.sessions.handler.enabled')) {
            DB::delete('DELETE FROM USER_SESSIONS WHERE session_id = "' . Strings::encodeString($session_id) . '";');
        }
        return true;
    }

    /**
     * Perform garbage collection, remove any old sessions
     * @param integer $max_lifetime Longest length of time we preserve active session information in the database.
     * @return false|integer Success flag
     */
    public function gc(/* int */ $max_lifetime): false|int
    {
        // Skip in our stateless testing setup.
        if (FrameworkConfig::get('debear.sessions.handler.enabled')) {
            DB::delete('DELETE FROM USER_SESSIONS WHERE last_accessed < DATE_SUB(NOW(), INTERVAL '
                . Strings::encodeString((string)$max_lifetime) . ' SECOND);');
            DB::delete('DELETE FROM USER_REMEMBERME WHERE expires < NOW();');
        }
        return 1;
    }

    // Restore the Squiz.Commenting.FunctionComment test.
    // phpcs:enable Squiz.Commenting.FunctionComment

    /**
     * Session Object getter
     * @return SessionHandler The instantiated version of this class
     */
    public static function get(): SessionHandler
    {
        return session()->getHandler();
    }

    /**
     * Compress data to write to the database
     */

    /**
     * Flatten the array session data into a string
     * @param string $data The session data we are encoding.
     * @return string The encoded session data
     */
    protected function encodeSessionData(string $data): string
    {
        return gzencode($data, 9);
    }

    /**
     * Restore the flattened session data into a usable array
     * @param string $data The session data we are decoding.
     * @return string The decoded session data
     */
    protected function decodeSessionData(string $data): string
    {
        return trim($data) ? gzdecode($data) : '';
    }
}
