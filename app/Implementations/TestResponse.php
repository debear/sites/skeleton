<?php

namespace DeBear\Implementations;

use Illuminate\Testing\TestResponse as BaseTestResponse;

class TestResponse
{
    /**
     * Raw, framework, version of the TestResponse.
     * @var BaseTestResponse
     */
    protected $raw;

    /**
     * Constructor to store locally the raw TestResponse object.
     * @param BaseTestResponse $raw The raw TestResponse object we are interacting with.
     */
    public function __construct(BaseTestResponse $raw)
    {
        $this->raw = $raw;
    }

    /**
     * Pass off mot methods to the standard object.
     * @param string $name      The name of the requested method.
     * @param array  $arguments The arguments for the requested method.
     * @return mixed The relevant response from the requested method.
     */
    public function __call(string $name, array $arguments): mixed
    {
        return call_user_func_array([$this->raw, $name], $arguments);
    }

    // Due to an issue with compatibility with the base versions of the test response class, do
    // not apply the Squiz.Commenting.FunctionComment rule to check function argument signatures.
    // phpcs:disable Squiz.Commenting.FunctionComment

    /**
     * A method override to revert the default $escaped value from Laravel 7's assertSee.
     * @param  string  $value   The value to be searched for.
     * @param  boolean $escaped That the input is escaped.
     * @return BaseTestResponse
     */
    public function assertSee(/* string */ $value, bool $escaped = false) /*: BaseTestResponse */
    {
        return $this->raw->assertSee($value, $escaped);
    }

    /**
     * A method override to revert the default $escaped value from Laravel 7's assertSeeInOrder.
     * @param  array   $values  The list of values to scan.
     * @param  boolean $escaped That the input is escaped.
     * @return BaseTestResponse
     */
    public function assertSeeInOrder(array $values, bool $escaped = false) /*: BaseTestResponse */
    {
        return $this->raw->assertSeeInOrder($values, $escaped);
    }

    /**
     * A method override to revert the default $escaped value from Laravel 7's assertSeeText.
     * @param  string  $value   The value to be searched for.
     * @param  boolean $escaped That the input is escaped.
     * @return BaseTestResponse
     */
    public function assertSeeText(/* string */ $value, bool $escaped = false) /*: BaseTestResponse */
    {
        return $this->raw->assertSeeText($value, $escaped);
    }

    /**
     * A method override to revert the default $escaped value from Laravel 7's assertSeeTextInOrder.
     * @param  array   $values  The list of values to scan.
     * @param  boolean $escaped That the input is escaped.
     * @return BaseTestResponse
     */
    public function assertSeeTextInOrder(array $values, bool $escaped = false) /*: BaseTestResponse */
    {
        return $this->raw->assertSeeTextInOrder($values, $escaped);
    }

    /**
     * A method override to revert the default $escaped value from Laravel 7's assertDontSee.
     * @param  string  $value   The value to be searched for.
     * @param  boolean $escaped That the input is escaped.
     * @return BaseTestResponse
     */
    public function assertDontSee(/* string */ $value, bool $escaped = false) /*: BaseTestResponse */
    {
        return $this->raw->assertDontSee($value, $escaped);
    }

    /**
     * A method override to revert the default $escaped value from Laravel 7's assertDontSeeText.
     * @param  string  $value   The value to be searched for.
     * @param  boolean $escaped That the input is escaped.
     * @return BaseTestResponse
     */
    public function assertDontSeeText(/* string */ $value, bool $escaped = false) /*: BaseTestResponse */
    {
        return $this->raw->assertDontSeeText($value, $escaped);
    }

    // Restore the Squiz.Commenting.FunctionComment test.
    // phpcs:enable Squiz.Commenting.FunctionComment
}
