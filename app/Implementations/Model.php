<?php

namespace DeBear\Implementations;

use Illuminate\Database\Eloquent\Model as EloquentModel;
use Awobaz\Compoships\Compoships; // This trait allows composite key relationships.
use Illuminate\Database\Eloquent\Builder;
use Carbon\Carbon;
use DeBear\Helpers\Arrays;
use DeBear\Helpers\Strings;
use DeBear\ORM\Base\Traits\CommonEloquent; // Common traits from DeBearORM.
use DeBear\Repositories\Time;

class Model extends EloquentModel
{
    use Compoships;
    use CommonEloquent;

    /**
     * The definition of the compound primary key, instead of the single column $primaryKey definition
     * @var array
     */
    protected $compoundKey = null;
    /**
     * Content in this model is stored in an encoded manner
     * @var boolean
     */
    protected $rawContent = false;

    /**
     * Non-fillable constructor
     * @param array $attributes The data the model contains.
     */
    public function __construct(array $attributes = [])
    {
        // Call our parent without the attributes, as we'll deal with that ourself.
        parent::__construct();
        // And then emulate the fill().
        $this->update($attributes);
    }

    // Due to an issue with compatibility with the base versions in (Base)Model, do
    // not apply the Squiz.Commenting.FunctionComment rule to check function argument signatures.
    // phpcs:disable Squiz.Commenting.FunctionComment

    /**
     * Special wrapper to the model attribute getter for our local processing
     * @param string $key The attribute we are requesting for the model.
     * @return mixed The appropriate attribute for the model
     */
    public function __get(/* string */ $key): mixed // Phan does not like this type hint being explicitly set.
    {
        // Get the raw value.
        $raw = parent::__get($key);
        // If we have a date (a Carbon object), localise it.
        if ($raw instanceof Carbon) {
            return Time::object()->localiseDatabase($raw);
        }
        // Fallback to the original parent value.
        return $raw;
    }

    // Restore the Squiz.Commenting.FunctionComment test.
    // phpcs:disable Squiz.Commenting.FunctionComment

    /**
     * Handle dynamic static method calls into the model.
     * @param string $method     The requested method... using a static notation.
     * @param array  $parameters Any paramters to pass to the method.
     * @return mixed
     */
    public static function __callStatic($method, $parameters): mixed
    {
        // Clean the parameters in WHERE clauses.
        if (substr($method, 0, 6) == 'where') {
            $parameters = static::cleanWhereClause($parameters);
        }
        // Forward the request to an appropriate method.
        $class = (new static());
        if ((substr($method, 0, 6) == 'static') && method_exists($class, substr($method, 6))) {
            $method = substr($method, 6);
        }
        return $class->$method(...$parameters);
    }

    /**
     * Clean the parameters of a WHERE clause
     * @param array $input The raw paramters to be passed in the where clause.
     * @return array The cleaned clause
     */
    protected static function cleanWhereClause(array $input): array
    {
        $ele = count($input);
        if (Arrays::isAssociative($input)) {
            // Clean a key/value pair.
            array_walk($input, function (&$item) {
                $item = Strings::encodeInput($item);
            });
        } elseif (is_scalar($input[0]) && ($ele == 2 || $ele == 3)) {
            // Edit a ['col', '='?, 'value'] array, which may have an '='.
            $input[$ele - 1] = Strings::encodeInput($input[$ele - 1]);
        } else {
            // Recursive call required.
            array_walk($input, function (&$item) {
                $item = static::cleanWhereClause($item);
            });
        }
        return $input;
    }

    /**
     * Save a new model and return the instance
     * @param array $attributes The data the model contains.
     * @return $this A new instance of the model
     */
    public static function create(array $attributes = []): self
    {
        $instance = static::build($attributes);
        $instance->save();
        return $instance;
    }

    /**
     * Create a new instance without saving the data quite yet
     * @param array $attributes The data the model contains.
     * @return $this A new instance of the model
     */
    public static function build(array $attributes = []): self
    {
        $instance = new static();
        $instance->update($attributes);
        return $instance;
    }

    /**
     * Update an existing model with data that doesn't need the fillable
     * @param array $attributes The data the model contains.
     * @param array $options    Options to send the ORM when saving the record.
     * @return $this A chainable instance of the object
     * @phan-suppress PhanParamSignatureMismatch
     */
    public function update(array $attributes = [], array $options = []): self
    {
        foreach ($attributes as $key => $value) {
            $this->$key = (!$this->rawContent ? Strings::encodeInput($value) : $value);
        }
        return $this;
    }

    /**
     * Either create a new record, or if it already exists then get and update the existing object
     * @param array $attributes The data we want saved to the hdatabase.
     * @param array $updates    The column(s) to be updated if the record already exists.
     * @return $this The object we retrieved, either via creation or by identifying an existing record and updating it
     */
    public static function createOrUpdate(array $attributes, array $updates): self
    {
        // Determine the details of the keys in this table.
        $faux = new static();
        $prim = $faux->getKeysPrimary();
        $comp = $faux->getKeysCompound();

        if (!isset($comp)) {
            // Identifying via a single primary key, though if there is no value then skip the
            // lookup (as we know it'll fail).
            $record = isset($attributes[$prim]) ? static::find($attributes[$prim]) : null;
        } else {
            // Identifying via a composite key, though this time the NULL check doesn't apply as it
            // could be included in a (non-primary) UNIQUE KEY.
            $where = [];
            foreach ($comp as $keyCol) {
                $where[] = [$keyCol, '=', $attributes[$keyCol]];
            }
            $record = static::where($where)->first();
        }

        // If we find a match, update, commit and return it.
        if (isset($record)) {
            foreach ($updates as $col) {
                if (isset($attributes[$col])) {
                    $record->$col = $attributes[$col];
                } else {
                    $record->$col = null;
                }
            }
            $record->save();
            return $record;
        }

        // Create a new record and return it.
        return static::create($attributes);
    }

    /**
     * Set the keys for a save update query.
     * @param Builder $query Object representing the save query.
     * @return Builder
     */
    protected function setKeysForSaveQuery(/* Builder */ $query) /* : Builder */
    {
        if ($this->getKeysCompound() !== null) {
            foreach ($this->compoundKey as $key) {
                $query->where($key, '=', $this->$key);
            }
        } else {
            $query->where($this->getKeyName(), '=', $this->getKeyForSaveQuery());
        }

        return $query;
    }

    /**
     * Save the model to the database, encoding the appropriate columns
     * @param array $options Options to send the ORM when saving the record.
     * @return boolean
     */
    public function encodeSave(array $options = []) /* : bool */
    {
        // Encode, preserving the original value.
        $encodedTmp = [];
        foreach ($this->attributesToArray() as $key => $value) {
            if (is_string($value)) {
                if (!$this->rawContent) {
                    $this->$key = $encodedTmp[$key] = $value;
                } else {
                    $encodedTmp[$key] = $value;
                    $this->$key = Strings::encodeString($value);
                }
            }
        }

        // Save the record.
        $ret = $this->save($options);

        // Restore the encoded columns.
        foreach ($encodedTmp as $key => $val) {
            $this->$key = $val;
        }

        return $ret;
    }

    /**
     * Determine what the model's singular primary key
     * @return ?string The column name as a string, or null if not applicable
     */
    public function getKeysPrimary(): ?string
    {
        return $this->primaryKey;
    }

    /**
     * Determine what the model's composite primary key
     * @return ?array The column names as an array, or null if not applicable
     */
    public function getKeysCompound(): ?array
    {
        return $this->compoundKey;
    }

    /**
     * Overload the parent method to get the attributes that should be converted to dates.
     * @return array
     */
    public function getDates()
    {
        // Combine the standard Eloquent list with those defined as having a datetime cast.
        return array_merge(parent::getDates(), array_keys(array_filter($this->casts ?? [], function ($value) {
            return substr($value ?? '', 0, 8) == 'datetime';
        })));
    }

    /**
     * Fix the way dates are converted in the model to array conversion
     * @return array
     */
    public function toArray() /* : array */
    {
        // Get the standard conversion.
        $raw = parent::toArray();
        // And now fix the dates.
        foreach ($this->getDates() as $attr) {
            if ($this->$attr) {
                if (strpos($this->casts[$attr], ':') === false) {
                    // No specific formattng, so return in the full datetime format.
                    $raw[$attr] = $this->$attr->toDateTimeString();
                } else {
                    // Determine what the custom format is and use that.
                    $fmt = substr($this->casts[$attr], strpos($this->casts[$attr], ':') + 1);
                    $raw[$attr] = $this->$attr->format($fmt);
                }
            }
        }
        return $raw;
    }
}
