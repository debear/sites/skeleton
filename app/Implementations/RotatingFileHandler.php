<?php

namespace DeBear\Implementations;

use Illuminate\Log\Logger;
use Monolog\Handler\RotatingFileHandler as BaseRotatingFileHandler;

class RotatingFileHandler
{
    /**
     * Customise the given logger instance.
     * @param Logger $logger The principal logging object.
     * @return void
     */
    public function __invoke(Logger $logger): void
    {
        foreach ($logger->getHandlers() as $handler) {
            if ($handler instanceof BaseRotatingFileHandler) {
                // Resolves to dir/date.log (rather than default dir/-date.log).
                $handler->setFilenameFormat('{filename}{date}', 'Y-m-d');
            }
        }
    }
}
