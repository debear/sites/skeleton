<?php

namespace DeBear\Providers;

use DeBear\Implementations\Hasher;
use Illuminate\Support\ServiceProvider;

class HashServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider
     * @return void
     */
    public function register(): void
    {
        $this->app->singleton(
            'hash',
            function () {
                return new Hasher();
            }
        );
    }

    /**
     * Get the services provided by the provider
     * @return array List of services
     */
    public function provides(): array
    {
        return array('hash');
    }
}
