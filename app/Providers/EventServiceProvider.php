<?php

namespace DeBear\Providers;

use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use DeBear\Helpers\Arrays;
use DeBear\Helpers\HTTP;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The statically defined (global / skeleton) event listener mappings for the application.
     * @var array
     */
    protected $listen = [];

    /**
     * The statically defined (global / skeleton) event subscriber mappings for the application.
     * @var array
     */
    protected $subscribe = [];

    /**
     * Overload the base register() method to include our dynamic listener / subscriber logic
     * @return void
     */
    public function register()
    {
        if (HTTP::isTest()) {
            $base = FrameworkConfig::get('debear.events');
            FrameworkConfig::set(['debear.events' => Arrays::merge($base, $base['ci'])]);
        }
        // Set the event listeners / subscribers from the subsites.
        $this->listen = FrameworkConfig::get('debear.events.listeners');
        $this->subscribe = FrameworkConfig::get('debear.events.subscribers');
        // Continue with regular processing.
        parent::register();
    }
}
