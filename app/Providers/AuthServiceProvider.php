<?php

namespace DeBear\Providers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     * @var array
     */
    protected $policies = [
        'DeBear\Model' => 'DeBear\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     * @return void
     */
    public function boot()
    {
        // Use our custom UserProvider instance rather than the default EloquentUserProvider.
        Auth::provider(
            'userprovider',
            function ($app, array $config) {
                return new UserProvider($app['hash'], $config['model']);
            }
        );
    }
}
