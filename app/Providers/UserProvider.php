<?php

namespace DeBear\Providers;

use Illuminate\Auth\EloquentUserProvider;
use Illuminate\Contracts\Auth\Authenticatable as UserContract;

class UserProvider extends EloquentUserProvider
{
    /**
     * Customised credential validation, controlling what is passed to the hasher
     * @param UserContract $user        Instance of the user identified by the user_id.
     * @param array        $credentials Full array of details passed to the authentication module for checking.
     * @return boolean Validation status
     */
    public function validateCredentials(UserContract $user, array $credentials) /* : bool */
    {
        $opt = ['salt' => $credentials['user_id']];
        return $this->hasher->check($credentials['password'], $user->getAuthPassword(), $opt);
    }
}
