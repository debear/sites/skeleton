<?php

namespace DeBear\Providers;

use DateTimeZone;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use DeBear\Implementations\SessionHandler;
use DeBear\Repositories\InternalCache;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     * @return void
     */
    public function boot()
    {
        // Ensure our logging is performed in the base timezone.
        Log::setTimezone(new DateTimeZone(FrameworkConfig::get('app.timezone')));

        // Sessions should be handled via our custom handler.
        Session::extend('debear', function () {
            return new SessionHandler();
        });

        // Custom Blade directive to wrap around our Benchmark module.
        Blade::directive('benchmarkStart', function ($name) {
            return "<?php \DeBear\Helpers\Benchmark::start(\"blade:\" . $name); ?>";
        });
        Blade::directive('benchmarkStop', function ($name) {
            return "<?php \DeBear\Helpers\Benchmark::stop(\"blade:\" . $name); ?>";
        });
    }

    /**
     * Register any application services.
     * @return void
     */
    public function register()
    {
        // Our basic object cache.
        $this->app->singleton(InternalCache::class);
    }
}
