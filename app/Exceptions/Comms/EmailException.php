<?php

namespace DeBear\Exceptions\Comms;

use DeBear\Exceptions\CommsException;

class EmailException extends CommsException
{
}
