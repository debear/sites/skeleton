<?php

namespace DeBear\Exceptions\Testing;

use DeBear\Exceptions\TestingException;

class DatabaseException extends TestingException
{
}
