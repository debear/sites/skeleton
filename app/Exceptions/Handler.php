<?php

namespace DeBear\Exceptions;

use Throwable;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use DeBear\Helpers\HTTP;
use DeBear\Helpers\API;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     * @var array
     */
    protected $dontReport = [
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    // Due to an issue with compatibility with the base versions in (Exception)Handler, do
    // not apply the Squiz.Commenting.FunctionComment rule to check function argument signatures.
    // phpcs:disable Squiz.Commenting.FunctionComment
    /**
     * Render an exception into an HTTP response.
     * @param Request   $request   The request being processed.
     * @param Throwable $exception The exception being rendered.
     * @return Response|JsonResponse
     */
    public function render($request, Throwable $exception)
    {
        // If we have an API request, we need to render the error as JSON.
        if ($request->wantsJson()) {
            $code = (method_exists($exception, 'getStatusCode') ? $exception->getStatusCode() : null) ?? 500;
            if (!HTTP::isDev() || $code != 500) {
                $default = 'An error occurred whilst processing the request.';
                $message = (FrameworkConfig::get("debear.errors.$code.info") ?? $default);
                return API::setStatus($code, $message);
            }
        }
        // Customise our CSP to disable CSPs (as Whoops is not compliant to our rules).
        HTTP::disableCSP();
        // Now render.
        return parent::render($request, $exception);
    }
    // phpcs:enable Squiz.Commenting.FunctionComment
}
