<?php

namespace DeBear\Exceptions\Resources;

use DeBear\Exceptions\ResourcesException;

class MissingException extends ResourcesException
{
}
