<?php

namespace DeBear\Exceptions\Cookies;

use DeBear\Exceptions\CookiesException;

class PrefsException extends CookiesException
{
}
