<?php

namespace DeBear\Models\Skeleton;

use DeBear\Implementations\Model;

class SessionStats extends Model
{
    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql';
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'USER_SESSIONS_STATS';
    /**
     * Primary Key column for the database table
     * @var string
     */
    protected $primaryKey = 'session_id';
    /**
     * Date columns within the database table
     * @var array
     */
    protected $casts = [
        'session_start' => 'datetime',
        'last_accessed' => 'datetime',
        'hits_last_archived' => 'datetime',
    ];
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;
    /**
     * The primary key is not auto-incrementing
     * @var boolean
     */
    public $incrementing = false;
}
