<?php

namespace DeBear\Models\Skeleton;

use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Facades\App;
use DeBear\Implementations\Model;
use DeBear\Repositories\Time;

class CommsEmailLink extends Model
{
    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql_common';
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'COMMS_EMAIL_LINK';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'app',
        'email_id',
        'link_id',
    ];
    /**
     * Date columns within the database table
     * @var array
     */
    protected $casts = [
        'first_visited' => 'datetime',
        'last_visited' => 'datetime',
    ];
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;
    /**
     * The primary key is not auto-incrementing
     * @var boolean
     */
    public $incrementing = false;

    /**
     * First part of the link's checksum
     * @var string
     */
    public $code_checksum1 = null;
    /**
     * Second part of the link's checksum
     * @var string
     */
    public $code_checksum2 = null;
    /**
     * Built and usable URL for the link
     * @var string
     */
    public $url_cnv = null;

    /**
     * Relationship: 1:1 email instance
     * @return HasOne
     */
    public function email(): HasOne
    {
        return $this->hasOne(CommsEmail::class, ['app', 'email_id'], ['app', 'email_id']);
    }

    /**
     * Stamp a link as having been visited by the user
     * @return void
     */
    public function stampVisited(): void
    {
        if (!$this->times_visited) {
            $this->first_visited = App::make(Time::class)->formatServer();
        }
        $this->last_visited = App::make(Time::class)->formatServer();
        $this->times_visited++;
        $this->save();
    }
}
