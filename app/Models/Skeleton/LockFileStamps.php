<?php

namespace DeBear\Models\Skeleton;

use DeBear\Implementations\Model;

class LockFileStamps extends Model
{
    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql';
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'LOCKFILE_STAMPS';
    /**
     * Primary Key column for the database table
     * @var string
     */
    protected $primaryKey = 'app';
    /**
     * Date columns within the database table
     * @var array
     */
    protected $casts = [
        'last_started' => 'datetime',
        'last_finished' => 'datetime',
    ];
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;
    /**
     * The primary key is not auto-incrementing
     * @var boolean
     */
    public $incrementing = false;
}
