<?php

namespace DeBear\Models\Skeleton;

use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use DeBear\Implementations\Model;

class CommsEmail extends Model
{
    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql_common';
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'COMMS_EMAIL';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'app',
        'email_id',
    ];
    /**
     * Date columns within the database table
     * @var array
     */
    protected $casts = [
        'email_queued' => 'datetime',
        'email_send' => 'datetime',
        'email_sent' => 'datetime',
    ];
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;
    /**
     * The primary key is not auto-incrementing
     * @var boolean
     */
    public $incrementing = false;
    /**
     * Content in this model is not stored in an encoded manner
     * @var boolean
     */
    protected $rawContent = true;

    /**
     * If the email is to be sent in plain-text or HTML mode
     * @var string
     */
    public $type = null;
    /**
     * Summary string of the email body to be written near the top
     * @var string
     */
    public $summary = null;
    /**
     * Main message body
     * @var string
     */
    public $message = null;
    /**
     * Additional clause(s) to be used in the email footer
     * @var string
     */
    public $clause = null;
    /**
     * If the user has opted out of receiving emails or not
     * @var boolean
     */
    public $opt_out = null;
    /**
     * If the email should be blocked because the user has not completed the verification process
     * @var boolean
     */
    public $unverified = null;
    /**
     * Time (incl. POSIX string) When the email should be sent
     * @var string
     */
    public $send_time = null;
    /**
     * First part of the link's checksum
     * @var string
     */
    public $code_checksum1 = null;
    /**
     * Second part of the link's checksum
     * @var string
     */
    public $code_checksum2 = null;
    /**
     * Arbitrary string to be used as the content boundary within a mixed HTML/plain email
     * @var string
     */
    public $content_boundary = null;
    /**
     * List of link objects within the email
     * @var array
     */
    public $links = null;

    /**
     * Relationship: 1:1 user bind
     * @return HasOne
     */
    public function user(): HasOne
    {
        return $this->hasOne(User::class, 'user_id', 'user_id');
    }
    /**
     * Relationship: 1:M links within this email
     * @return HasMany
     */
    public function emailLinks(): HasMany
    {
        return $this->hasMany(CommsEmailLink::class, ['app', 'email_id'], ['app', 'email_id']);
    }

    /**
     * Save to the database, including any links we may have in the message
     * @param array $options Customisations to consider.
     * @return boolean Return status
     */
    public function save(array $options = []) /* : bool */
    {
        // Initial process.
        $ret = parent::save($options);
        // Get the (auto-increment) ID using our unique code to tie back.
        $this->email_id = static::where('email_code', '=', $this->email_code)->select('email_id')->first()->email_id;

        // Then save any links.
        if (isset($this->links) && is_array($this->links) && sizeof($this->links)) {
            foreach ($this->links as $i => $link) {
                $link->app = $this->app;
                $link->email_id = $this->email_id;
                $link->link_id = $i + 1;
                $link->times_visited = 0;
                $link->encodeSave();
            }
        }

        // And return our status.
        return $ret;
    }
}
