<?php

namespace DeBear\Models\Skeleton;

use Illuminate\Database\Eloquent\Relations\HasOne;
use DeBear\Implementations\Model;

class CookiePreferenceLog extends Model
{
    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql';
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'COOKIE_PREFERENCE_LOG';
    /**
     * Primary Key column for the database table
     * @var string
     */
    protected $primaryKey = 'log_id';
    /**
     * Date columns within the database table
     * @var array
     */
    protected $casts = [
        'when_done' => 'datetime',
    ];
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Relationship: 1:1 user bind
     * @return HasOne
     */
    public function user(): HasOne
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
