<?php

namespace DeBear\Models\Skeleton;

use Illuminate\Database\Eloquent\Relations\HasMany;
use DeBear\Implementations\Model;

class NewsSource extends Model
{
    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql_common';
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'NEWS_SOURCES';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'app',
        'source_id',
    ];
    /**
     * Date columns within the database table
     * @var array
     */
    protected $casts = [
        'last_updated' => 'datetime',
        'last_synced' => 'datetime',
    ];
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;
    /**
     * The primary key is not auto-incrementing
     * @var boolean
     */
    public $incrementing = false;

    /**
     * Relationship: 1:M current article list for this publisher
     * @return HasMany
     */
    public function articles(): HasMany
    {
        return $this->hasMany(News::class, ['app', 'source_id'], ['app', 'source_id']);
    }
}
