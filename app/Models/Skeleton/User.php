<?php

namespace DeBear\Models\Skeleton;

use Illuminate\Notifications\Notifiable;
use DeBear\Implementations\AuthenticatableUser;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use DeBear\Events\Skeleton\UserLogin;
use DeBear\Helpers\Browser;
use DeBear\Helpers\Cookie;
use DeBear\Helpers\HTML;
use DeBear\Helpers\Policies;
use DeBear\Helpers\SessionAggregation;
use DeBear\Helpers\Strings;
use DeBear\Helpers\User as UserHelper;
use DeBear\Models\Skeleton\UserActivityLog;
use DeBear\Repositories\InternalCache;
use DeBear\Repositories\SuspiciousActivity;
use DeBear\Repositories\Time;

class User extends AuthenticatableUser
{
    use Notifiable;

    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql';
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'USERS';
    /**
     * Date columns within the database table
     * @var array
     */
    protected $casts = [
        'dob' => 'datetime:Y-m-d',
        'account_created' => 'datetime',
        'account_verified' => 'datetime',
    ];
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;
    /**
     * Fields we should not include in JSON representations of the model
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * Instantiated User object
     * @var User
     */
    protected static $static_obj;
    /**
     * User levels for the user
     * @var array
     */
    protected static $static_levels;

    /**
     * Relationship: 1:1 timezone the user is predominantly in
     * @return HasOne
     */
    public function timezoneInfo(): HasOne
    {
        return $this->hasOne(Timezone::class, 'timezone', 'timezone');
    }
    /**
     * Relationship: 1:M permissions granted to this user
     * @return HasMany
     */
    public function permissions(): HasMany
    {
        return $this->hasMany(UserPermissions::class, 'user_id');
    }
    /**
     * Relationship: 1:M active "Remember me?" instances for this user
     * @return HasMany
     */
    public function rememberMes(): HasMany
    {
        return $this->hasMany(UserRememberMe::class, 'user_id');
    }

    /**
     * Define the "user ID" column for authentication
     * @return string The appropriate database column
     */
    public function username() /* : string */
    {
        return 'user_id';
    }

    // phpcs:disable Squiz.Commenting.FunctionComment
    /**
     * Magic __get method to mask some column aliases
     * @param string $key The column to access.
     * @return mixed The appropriate value for the given key/alias
     */
    public function __get(/* string */ $key): mixed // Phan does not like this type hint being explicitly set.
    {
        return parent::__get($key) ?? ($key != 'links' ? false : []);
    }
    // phpcs:enable Squiz.Commenting.FunctionComment

    /**
     * Name of the user
     * @return string Name of the user
     */
    public function getNameAttribute(): string
    {
        $name = join(' ', array_filter([$this->forename, $this->surname]));
        if ($name) {
            return $name;
        } elseif (Policies::match('referer:google')) {
            return 'Google Visitor';
        } else {
            return 'Guest';
        }
    }

    /**********************
     * Abstracted Getters *
     **********************/
    /**
     * User is logged in
     * @return boolean Whether the user is logged in
     */
    public function isLoggedIn(): bool
    {
        return Policies::match('logged_in');
    }

    /**
     * Is the user verified?
     * @return boolean Whether the user has completed verification
     */
    public function isVerified(): bool
    {
        return !$this->isUnverified();
    }

    /**
     * Has the user not completed verification?
     * @return boolean Whether the user has yet to complete verification
     */
    public function isUnverified(): bool
    {
        return ($this->status === 'Unverified');
    }

    /***********
     * Actions *
     ***********/

    /**
     * Log a user in via an ID / Password combo
     * @param array|User $creds Info we need to log the user in.
     * @param array      $opt   Configuration options.
     * @return boolean Success flag
     */
    public static function doLogin(array|User $creds, array $opt = []): bool
    {
        // Do the login attempt.
        if (is_array($creds)) {
            $ret = Auth::attempt($creds);
        } elseif (is_object($creds) && ($creds instanceof self)) {
            Auth::login($creds);
            $ret = true;
        }
        if (!isset($ret) || !$ret) {
            return false;
        }
        $user_id = Auth::user()->id;
        // Remembering the user for subsequent sessions?
        if (isset($opt['rememberme']) && $opt['rememberme']) {
            $rm_string = Strings::randomAlphaNum(32);
            $exp_min = FrameworkConfig::get('debear.sessions.cutoffs.cookie');
            UserRememberMe::create([
                'user_id' => $user_id,
                'rm_string' => $rm_string,
                'expires' => App::make(Time::class)->adjustFormatServer("+$exp_min minutes"),
            ])->save();
            Cookie::setValue(FrameworkConfig::get('debear.sessions.cookies.rememberme'), "$user_id::$rm_string");
        }
        // Determine how the login originated.
        $login_source = 'system';
        if (is_array($creds) && isset($creds['password'])) {
            $login_source = 'password';
            /* Whilst we have the plaintext password, perform some additional checks on the non-hashed value. */
            // 1. Conforms to the password policy?
            $policy_confirmed = UserActivityLog::query()
                ->where([
                    ['user_id', '=', Auth::user()->id],
                    ['type', '=', FrameworkConfig::get('debear.logging.user_activity.password_policy')],
                ])->whereJsonContains('detail->blocked', 'false')->first();
            if (is_string(UserHelper::validatePasswordPolicy($creds['password']))) {
                // A legacy password that does not meet our current policy.
                InternalCache::object()->set('user:password-fails-policy', true);
            } elseif ($policy_confirmed === null) { // Code coverage doesn't like "!isset()"!?
                // Stamp we have just verified conformity.
                $info = 'Policy conformity validated';
                App::make(SuspiciousActivity::class)->record('password_policy', $info, false, ['user_id' => $user_id]);
            }
            // 2. A legacy hash that should be upgraded?
            if (strlen(Auth::user()->password) == 12) {
                $upgraded_password = Strings::oneWayHash($creds['password'], Auth::user()->user_id);
                Auth::user()->update(['password' => $upgraded_password])->save();
            }
        } elseif (isset($opt['from_cookie']) && $opt['from_cookie']) {
            $login_source = 'cookie';
        } elseif (isset($opt['from_registration']) && $opt['from_registration']) {
            $login_source = 'registration';
        }
        // Log.
        if ($login_source != 'cookie') {
            App::make(SuspiciousActivity::class)->record('login', 'Successful Login', false, [
                'user_id' => $user_id,
                'source' => $login_source,
            ]);
        }
        // Internal management.
        static::reload($login_source);
        // Update the aggregated session counter.
        if ($login_source != 'cookie') {
            SessionAggregation::addLogin();
        }
        // Trigger our login event.
        UserLogin::dispatch(Auth::user()->id);
        return true;
    }

    /**
     * Log out the currently logged in user
     * @return boolean Success flag
     */
    public static function doLogout(): bool
    {
        // Kill a Remember Me?
        if (Cookie::hasValue(FrameworkConfig::get('debear.sessions.cookies.rememberme'))) {
            $rm = Cookie::getValue(FrameworkConfig::get('debear.sessions.cookies.rememberme')) ?? '';
            UserRememberMe::where([
                ['user_id', '=', User::object()->id],
                ['rm_string', '=', preg_replace('/^\d+::/', '', $rm)],
            ])->delete();
        }
        Cookie::unsetValue(FrameworkConfig::get('debear.sessions.cookies.rememberme'));
        // Remove from the session.
        Auth::logout();
        // Re-create a new session.
        static::reload(); // Re-run the internal management.
        SessionAggregation::addLogout();
        session()->regenerateToken();
        // If we reached this far successfully, we've logged out okay.
        return true;
    }

    /**
     * Prepare the user object so we have something valid to use in all situations
     * @return void
     */
    public static function setup(): void
    {
        static::setupWorker(true);
    }

    /**
     * Setup the appropriate internal elements when making a request through an API
     * @param integer $user_id The numeric ID of the user being loaded.
     * @return void
     */
    public static function setupFromAPI(int $user_id): void
    {
        static::$static_obj = static::find($user_id);
        Policies::recalc(static::$static_obj);
    }

    /**
     * Update our internal variables when re-loading the whole user object
     * @param string|false $login_source How the user was logging in (if they were logging in...). (Optional).
     * @return void
     */
    public static function reload(string|false $login_source = false): void
    {
        // Clear the old session info.
        session()->forget('user');
        session()->forget('forms');
        session()->forget('transient');
        // Any info to write back straight away?
        $info = [];
        if ($login_source) {
            $info['user.agent'] = Browser::internalHash();
            // If the login was by password, we will change this later (as we need to compare the old source with new).
            if ($login_source != 'password') {
                $info['user.login.source'] = $login_source;
            }
        }
        if (sizeof($info)) {
            session($info);
        }
        // Stamp if the login came from password entry.
        if ($login_source == 'password') {
            static::stampPasswordLogin();
        }
        static::setupWorker(false);
    }


    /**
     * Flag that the user just logged in with their password
     * @return void
     */
    public static function stampPasswordLogin(): void
    {
        $recalc = ((string)session('user.login.source') != 'password');
        session([
            'user.login.source' => 'password',
            'user.password.entered' => App::make(Time::class)->formatServer('U'),
        ]);
        if ($recalc) {
            Policies::recalc(static::$static_obj);
        }
    }

    /**
     * Worker method for updating the internal details of our user
     * @param boolean $initial Whether we're processing on the page for the first time or not.
     * @return void
     */
    protected static function setupWorker(bool $initial): void
    {
        $logged_in = Auth::check();

        // If we're not logged in and we have a (valid) remember me cookie, auth using that.
        if (!$logged_in && Cookie::hasValue(FrameworkConfig::get('debear.sessions.cookies.rememberme'))) {
            $rm = Cookie::getValue(FrameworkConfig::get('debear.sessions.cookies.rememberme'));
            if (isset($rm) && strpos($rm, '::') !== false) {
                list($user_id, $rm_string) = explode('::', $rm);
                $validate = UserRememberMe::where([
                    ['user_id', '=', $user_id],
                    ['rm_string', '=', $rm_string],
                    ['expires', '>=', App::make(Time::class)->formatServer()],
                ])->first();
                if (isset($validate)) {
                    // Have been passed a successful rm_string that is still valid.
                    static::doLogin(User::find($user_id), ['from_cookie' => true]);
                    $logged_in = true;
                } else {
                    Cookie::unsetValue(FrameworkConfig::get('debear.sessions.cookies.rememberme'));
                }
            }
        }

        // If we have a pukka object use that, otherise create an empty placeholder (which is still a valid object).
        static::$static_obj = ($logged_in ? Auth::user() : new User());

        // Set the time according to the user's timezone.
        Time::object()->setUserTimezone(
            static::$static_obj->timezone ?: FrameworkConfig::get('debear.datetime.timezone_app')
        );

        // Get the appropriate user level details.
        if (!session()->has('user.levels')) {
            $levels = [];
            if ($logged_in && isset(static::$static_obj->permissions)) {
                foreach (static::$static_obj->permissions as $level) {
                    if (!isset($levels[$level->site])) {
                        $levels[$level->site] = [];
                    }
                    $section = isset($level->section) && $level->section ? $level->section : '_global';
                    $levels[$level->site][$section] = $level->level;
                }
            }
            session(['user.levels' => $levels]);
        }
        static::$static_levels = (array)session('user.levels');

        // Process the policy list.
        $method = ($initial ? 'setup' : 'recalc');
        Policies::$method(static::$static_obj);
    }

    /************************
     * User Detail Checkers *
     ************************/
    /**
     * Determine if the user matches the required level for the site
     * @param string|array $levels The level(s) to compare.
     * @param boolean      $all    If the user matches every level in the list, not just one (Optional, Default: false).
     * @return boolean The status of the level check
     */
    protected static function checkLevel(string|array $levels, bool $all = false): bool
    {
        // Ensure the argument is an array.
        if (!is_array($levels)) {
            $levels = [$levels];
        }

        // What is the appropriate level?
        $site = FrameworkConfig::get('debear.url.sub');
        $section = (FrameworkConfig::get('debear.section.code') ?: '_global');
        $cmp = 'user';
        if (
            isset(static::$static_levels[$site])
            && isset(static::$static_levels[$site][$section])
        ) {
            $cmp = static::$static_levels[$site][$section];
        }

        // Run through the list (using proof-by-contradiction).
        foreach ($levels as $level) {
            $matched = ($level == $cmp);
            if (!$all && $matched) {
                // Any and one matched: Pass.
                return true;
            } elseif ($all && !$matched) {
                // All and on failed: Fail.
                return false;
            }
        }
        // No contraditions found to our $all flag.
        return $all;
    }

    /**
     * Determine if a user's login method matches a check
     * @param string|array $states The state we are checking for the user.
     * @param boolean      $all    If the user matches every level in the list, not just one (Optional, Default: false).
     * @return boolean Whether the user login method matches the required state
     */
    public static function checkLoginState(string|array $states, bool $all = false): bool
    {
        // Ensure the argument is an array.
        if (!is_array($states)) {
            $states = [$states];
        }

        // Run through the list (using proof-by-contradiction).
        foreach ($states as $state) {
            // Catch a subtle edge-case: password recency, which is a sub-check of just password.
            $check_recency = false;
            if ($state == 'password:recent') {
                $state = 'password';
                $check_recency = true;
            }
            // Run the check.
            $matched = (session('user.login.source') == $state);
            // Any post-checks?
            if ($matched && $check_recency && session()->has('user.password.entered')) {
                $matched = (App::make(Time::class)->formatServer('U') - intval(session('user.password.entered')))
                    <= FrameworkConfig::get('debear.security.password.recency');
            }

            if (!$all && $matched) {
                // Any and one matched: Pass.
                return true;
            } elseif ($all && !$matched) {
                // All and on failed: Fail.
                return false;
            }
        }
        // No contradictions found to our $all flag.
        return $all;
    }

    /**
     * Determine the status of a user cached flag
     * @param string $cache_key The cache key we are checking.
     * @return boolean Whether the cache key flag is set or not
     */
    public static function checkCacheFlag(string $cache_key): bool
    {
        return InternalCache::object()->get("user:$cache_key");
    }

    /******************
     * Static Getters *
     ******************/
    /**
     * Get an instantiated object for the user, even if one is logged out
     * @return ?User The appropriate User, or null if not found
     */
    public static function object(): ?User
    {
        return static::$static_obj;
    }

    /**
     * Get the user details for the debug panel
     * @return array A key/value pair of details for the user
     */
    public static function getDebug(): array
    {
        /* Get the base values */
        $user = static::object();
        $ret = $user->toArray();
        /* Tweak some values for display */
        // Email.
        if (isset($ret['email']) && is_string($ret['email']) && $ret['email']) {
            $ret['email'] = HTML::buildEmailMarkup($ret['email']);
        }
        // Dates (using the original Carbon object).
        foreach (['dob'] as $col) {
            if (isset($ret[$col])) {
                $ret[$col] = $user->$col->format('jS F Y');
            }
        }
        // Date/Times (again, using the Carbon object).
        foreach (['account_created', 'account_verified'] as $col) {
            if (isset($ret[$col])) {
                $ret[$col] = $user->$col->format('H:i:s \o\n jS F Y');
            }
        }
        // Booleans.
        /*foreach (['user_col'] as $col) {
            if (isset($ret[$col])) {
                $ret[$col] = '<span class="icon_' . ($ret[$col] ? 'valid' : 'delete') . '"></span>';
            }
        }*/
        // Joined columns.
        /*foreach (['user_col' => 'join_label'] as $col => $value) {
            if (isset($ret[$col])) {
                $ret[$col] = $user->$col()->first()->$value . " <strong>(ID: {$ret[$col]})</strong>";
            }
        }*/
        // Timezones (special instance of above).
        if (isset($ret['timezone'])) {
            $ret['timezone'] = '<span class="flag_right flag16_right_' . $user->timezoneInfo->code2 . '">'
                . $ret['timezone'] . '</span>';
        }
        /* Return */
        return $ret;
    }

    /****************
     * Form Methods *
     ****************/

    /**
     * Manipulate the data before being added to the database
     * @param array $data Data we will insert into the database. (Pass-by-Reference).
     * @return void
     */
    public static function prepareUpsert(array &$data): void
    {
        $user_id = ($data['user_id'] ?? static::object()->user_id);

        // Encode the password.
        if (isset($data['password'])) {
            $data['password'] = Strings::oneWayHash($data['password'], $user_id);
        }
    }

    /**************************
     * Disable remember_token *
     **************************/
    // Due to an issue with compatibility with the base versions in (Exception)Handler, do
    // not apply the Squiz.Commenting.FunctionComment rule to check function argument signatures.
    // phpcs:disable Squiz.Commenting.FunctionComment

    /**
     * Get the token value for the "remember me" session.
     * @return string|null
     */
    public function getRememberToken()
    {
        return null; // Not supported.
    }

    /**
     * Set the token value for the "remember me" session.
     * @param string $value "Remember Me" token value.
     * @return void
     */
    public function setRememberToken(/* string */ $value)
    {
        // Not supported.
    }

    /**
     * Get the column name for the "remember me" token.
     * @return string|null
     */
    public function getRememberTokenName()
    {
        return null; // Not supported.
    }

    /**
     * Overrides the method to ignore the remember token.
     * @param string $key   Column to store data against in the model.
     * @param mixed  $value Value to store in the column.
     * @return mixed
     */
    public function setAttribute(/* string */ $key, mixed $value): mixed
    {
        // Ignore updates to the Remember Token feature.
        if ($key != $this->getRememberTokenName()) {
            parent::setAttribute($key, $value);
        }
        return $this;
    }
    // phpcs:enable Squiz.Commenting.FunctionComment
}
