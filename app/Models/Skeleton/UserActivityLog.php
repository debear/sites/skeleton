<?php

namespace DeBear\Models\Skeleton;

use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Support\Facades\Request;
use DeBear\Helpers\HTTP;
use DeBear\Helpers\SessionAggregation;
use DeBear\Implementations\Model;
use DeBear\Repositories\Time;

class UserActivityLog extends Model
{
    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql';
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'USER_ACTIVITY_LOG';
    /**
     * Primary Key column for the database table
     * @var string
     */
    protected $primaryKey = 'activity_id';
    /**
     * Date columns within the database table
     * @var array
     */
    protected $casts = [
        'activity_time' => 'datetime',
    ];
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;
    /**
     * Content in this model is not stored in an encoded manner
     * @var boolean
     */
    protected $rawContent = true;

    /**
     * Relationship: 1:1 user bind
     * @return HasOne
     */
    public function user(): HasOne
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    /**
     * Save an audit log instance
     * @param string $type    The category of audit log.
     * @param string $summary A summary explanation for the audit record.
     * @param array  $detail  A blob of relevant information that will be JSON encoded.
     * @return void
     */
    public static function record(string $type, string $summary, array $detail): void
    {
        // Determine the appropriate user.
        $user_id = null;
        if ((User::object() !== null) && User::object()->id) {
            $user_id = User::object()->id;
        } elseif (isset($detail['user_id'])) {
            $user_id = $detail['user_id'];
        }
        // Convert text ID to numeric.
        if (is_string($user_id)) {
            $user = User::where('user_id', '=', $user_id)->first();
            if (isset($user)) {
                $user_id = $user->id;
            }
        }
        // Determine the appropriate session ID and store.
        $session_id = SessionAggregation::getID();
        // Create the database object.
        static::create([
            'activity_time' => App::make(Time::class)->formatServer(),
            'type' => FrameworkConfig::get("debear.logging.user_activity.$type"),
            'user_id' => $user_id,
            'session_id' => $session_id ?: null,
            'remote_ip' => Request::server('REMOTE_ADDR'),
            'browser' => Request::server('HTTP_USER_AGENT'),
            'url' => 'https:' . HTTP::buildDomain() . Request::server('REQUEST_URI'),
            'summary' => $summary,
            'detail' => json_encode($detail),
        ]);
    }
}
