<?php

namespace DeBear\Models\Skeleton;

use DeBear\Implementations\Model;

class APIToken extends Model
{
    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql';
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'API_TOKENS';
    /**
     * Primary Key column for the database table
     * @var string
     */
    protected $primaryKey = 'token_id';
    /**
     * Date columns within the database table
     * @var array
     */
    protected $casts = [
        'date_active' => 'datetime',
        'date_revoked' => 'datetime',
    ];
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;
}
