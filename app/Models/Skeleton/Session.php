<?php

namespace DeBear\Models\Skeleton;

use Illuminate\Database\Eloquent\Relations\HasOne;
use DeBear\Implementations\Model;

class Session extends Model
{
    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql';
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'USER_SESSIONS';
    /**
     * Primary Key column for the database table
     * @var string
     */
    protected $primaryKey = 'session_id';
    /**
     * Date columns within the database table
     * @var array
     */
    protected $casts = [
        'session_started' => 'datetime',
        'last_accessed' => 'datetime',
    ];
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;
    /**
     * The primary key is not auto-incrementing
     * @var boolean
     */
    public $incrementing = false;

    /**
     * Relationship: 1:1 session bind
     * @return HasOne
     */
    public function session(): HasOne
    {
        return $this->hasOne(SessionStats::class, 'session_id', 'session_id_uniq');
    }
}
