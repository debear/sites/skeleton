<?php

namespace DeBear\Models\Skeleton;

use DeBear\Implementations\Model;

class SiteReferer extends Model
{
    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql_common';
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SITE_REFERERS';
    /**
     * Primary Key column for the database table
     * @var string
     */
    protected $primaryKey = 'referer_id';
    /**
     * Date columns within the database table
     * @var array
     */
    protected $casts = [
        'when_done' => 'datetime',
    ];
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;
}
