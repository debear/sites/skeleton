<?php

namespace DeBear\Models\Skeleton;

use DeBear\Implementations\Model;

class APITokenScope extends Model
{
    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql';
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'API_TOKEN_SCOPE';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'token_id',
        'domain',
        'scope',
    ];
    /**
     * Date columns within the database table
     * @var array
     */
    protected $casts = [
        'date_active' => 'datetime',
        'date_revoked' => 'datetime',
    ];
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;
    /**
     * The primary key is not auto-incrementing
     * @var boolean
     */
    public $incrementing = false;
}
