<?php

namespace DeBear\Models\Skeleton;

use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use DeBear\Implementations\Model;

class Country extends Model
{
    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql';
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SETUP_COUNTRY';
    /**
     * Primary Key column for the database table
     * @var string
     */
    protected $primaryKey = 'code2';
    /**
     * Our non-integer primary key column data type
     * @var string
     */
    protected $keyType = 'string';
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;
    /**
     * The primary key is not auto-incrementing
     * @var boolean
     */
    public $incrementing = false;

    /**
     * Relationship: 1:1 default timezone
     * @return HasOne
     */
    public function timezoneDefault(): HasOne
    {
        return $this->hasOne(Timezone::class, 'timezone', 'default_timezone');
    }
    /**
     * Relationship: 1:M full list of timezones in this country
     * @return HasMany
     */
    public function timezoneList(): HasMany
    {
        return $this->hasMany(Timezone::class, 'code2', 'code2');
    }
}
