<?php

namespace DeBear\Models\Skeleton;

use Illuminate\Database\Eloquent\Relations\HasOne;
use DeBear\Implementations\Model;

class Timezone extends Model
{
    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql';
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SETUP_COUNTRY_TZ';
    /**
     * Primary Key column for the database table
     * @var string
     */
    protected $primaryKey = 'timezone';
    /**
     * Our non-integer primary key column data type
     * @var string
     */
    protected $keyType = 'string';
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;
    /**
     * The primary key is not auto-incrementing
     * @var boolean
     */
    public $incrementing = false;

    /**
     * Relationship: 1:1 country this timezone is located in
     * @return HasOne
     */
    public function country(): HasOne
    {
        return $this->hasOne(Country::class, 'code2', 'code2');
    }
}
