<?php

namespace DeBear\Models\Skeleton;

use DeBear\Implementations\Model;

class CommsEmailTemplate extends Model
{
    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql_common';
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'COMMS_EMAIL_TEMPLATE';
    /**
     * Primary Key column for the database table
     * @var string
     */
    protected $primaryKey = 'ref';
    /**
     * Our non-integer primary key column data type
     * @var string
     */
    protected $keyType = 'string';
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;
    /**
     * Content in this model is not stored in an encoded manner
     * @var boolean
     */
    protected $rawContent = true;
}
