<?php

namespace DeBear\Models\Skeleton;

use Illuminate\Database\Eloquent\Relations\HasOne;
use DeBear\Implementations\Model;

class UserPermissions extends Model
{
    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql';
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'USER_PERMISSIONS';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'site',
        'section',
        'user_id',
    ];
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;
    /**
     * The primary key is not auto-incrementing
     * @var boolean
     */
    public $incrementing = false;

    /**
     * Relationship: 1:1 user bind
     * @return HasOne
     */
    public function user(): HasOne
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
    /**
     * Relationship: 1:1 permission info
     * @return HasOne
     */
    public function levelInfo(): HasOne
    {
        return $this->hasOne(UserLevel::class, 'level', 'level');
    }
}
