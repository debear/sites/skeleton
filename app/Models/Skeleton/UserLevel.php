<?php

namespace DeBear\Models\Skeleton;

use DeBear\Implementations\Model;

class UserLevel extends Model
{
    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql';
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'USER_LEVELS';
    /**
     * Primary Key column for the database table
     * @var string
     */
    protected $primaryKey = 'level';
    /**
     * Our non-integer primary key column data type
     * @var string
     */
    protected $keyType = 'string';
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;
    /**
     * The primary key is not auto-incrementing
     * @var boolean
     */
    public $incrementing = false;

    /**
     * Whether the level grants the user Admin privileges
     * @return boolean
     */
    public function isAdmin(): bool
    {
        return (bool)$this->is_admin;
    }
}
