<?php

namespace DeBear\Models\Skeleton;

use DeBear\Implementations\Model;

class CMSContent extends Model
{
    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql_common';
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'CMS_CONTENT';
    /**
     * Primary Key column for the database table
     * @var string
     */
    protected $primaryKey = 'content_id';
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;
}
