<?php

namespace DeBear\Models\Skeleton;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Database\Eloquent\Relations\HasOne;
use DeBear\Implementations\Model;
use DeBear\Helpers\Format;
use DeBear\Helpers\HTTP;
use DeBear\Helpers\Strings;
use DeBear\Repositories\Time;

class News extends Model
{
    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql_common';
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'NEWS_ARTICLES';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'app',
        'news_id',
    ];
    /**
     * Date columns within the database table
     * @var array
     */
    protected $casts = [
        'published' => 'datetime',
    ];
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;
    /**
     * The primary key is not auto-incrementing
     * @var boolean
     */
    public $incrementing = false;

    /**
     * Relationship: 1:1 publisher bind
     * @return HasOne
     */
    public function source(): HasOne
    {
        return $this->hasOne(NewsSource::class, ['app', 'source_id'], ['app', 'source_id']);
    }

    /**
     * Return a unique reference from this article
     * @return string A unique reference based on unique attributes
     */
    public function uniqueReference(): string
    {
        return Strings::md5Code($this->app . '-' . $this->news_id);
    }

    /**
     * Format the article's headline
     * @return string Processed headline
     */
    public function headline(): string
    {
        // At this stage, appears no manipulation is requried.
        return $this->title;
    }

    /**
     * An image for the article
     * @return string URL of the article's image, or an appropriate fallback
     */
    public function image(): string
    {
        // An explicit image, if defined and on prod.
        if (isset($this->thumbnail) && !HTTP::isDev()) {
            return $this->thumbnail;
        }
        // A fallback (based on the publisher, and always during dev).
        return HTTP::buildStaticURLs('images', "news/{$this->app}/{$this->source_id}.png");
    }

    /**
     * State who the articler's publisher was
     * @return string Publisher name
     */
    public function publisher(): string
    {
        return $this->source->name;
    }

    /**
     * Display the relative time since the article was published
     * @return string Formatted relative time since the article was published
     */
    public function timeRelative(): string
    {
        // Skip if no valid time.
        if (!isset($this->published) || !$this->published) {
            return '';
        }

        // Determine the time difference in seconds.
        $pubDate = strtotime($this->published);
        $pubTime = date('g:ia', $pubDate);
        $now = Time::object();
        $diff = $now->getServerNow() - $pubDate;
        if ($diff < 300) {
            // Last five minutes = "recent".
            return 'Just now';
        } elseif ($diff < 3600) {
            // Last hour = value in minutes.
            return Format::pluralise(floor($diff / 60), ' minute') . ' ago';
        } elseif ($diff < 43200) { // = 12 * 3600.
            // Last twelve hours = value in hours.
            return Format::pluralise(floor($diff / 3600), ' hour') . ' ago';
        } elseif (date('Y-m-d', $pubDate) == $now->getServerCurDate()) {
            // Today, but outside the above limit.
            return $pubTime;
        } elseif (date('Y-m-d', $pubDate) == date('Y-m-d', strtotime('-1 day', $now->getServerNow()))) {
            // Yesterday.
            return "Yesterday, $pubTime";
        } elseif ($diff < 518400) { // = 6 * 86400.
            // Last six days = day of week.
            return date('l', $pubDate) . ", $pubTime";
        }
        // Date in short.
        $date = date('jS F', $pubDate);
        $date_y = date('Y', $pubDate);
        if ($date_y != substr(App::make(Time::class)->getServerCurDate(), 0, 4)) {
            $date .= " $date_y";
        }
        return "$date, $pubTime";
    }

    /**
     * Process the summarised article text
     * @return string Processed description field
     */
    public function summary(): string
    {
        $summary = $this->description;
        // Strip all links.
        $summary = preg_replace('/&lt;a .*?&lt;\/a&gt;/i', '', $summary);
        // Convert <br> tags to space.
        $summary = preg_replace('/&lt;br\s*\/?\s*&gt;/i', ' ', $summary);
        // Strip other tags.
        $summary = preg_replace('/&lt;.*?&gt;/i', '', $summary);
        // Trailing ellipsis in content?
        $ellipsis = [
            '...' => -3,
            '…' => -1,
            '&amp;#8230;' => -11,
            '&amp;#x2026;' => -12,
            '&amp;hellip;' => -12,
        ];
        foreach ($ellipsis as $test => $pos) {
            if (mb_substr($summary, $pos) == $test) {
                $summary = mb_substr($summary, 0, $pos) . ' <em>(Article continues on publisher&amp;#39;s site)</em>';
                break;
            }
        }
        // And return.
        return trim($summary);
    }

    /**
     * Add custom CSP rules based on the articles returned
     * @param string|array $app The name of the news 'app'(s) we are displaying.
     * @return void
     */
    public static function updateCSP(string|array $app): void
    {
        /* style-src: 'nonce=' option */
        $style_key = 'debear.headers.csp.policies.style-src';
        $style = FrameworkConfig::get($style_key);
        $style[] = '\'nonce-{{NONCE}}\'';
        /* img-src: domains for our image list */
        // Get the distinct mage domains.
        $thumbnail_domains = array_filter(
            static::select('thumbnail_domain')
                ->distinct()
                ->whereIn('app', is_string($app) ? [$app] : $app)
                ->get()
                ->pluck('thumbnail_domain')
                ->toArray()
        );
        // Now process.
        $img_key = 'debear.headers.csp.policies.img-src';
        $img = array_merge(
            FrameworkConfig::get($img_key),
            $thumbnail_domains
        );

        // Write changes back to the config.
        FrameworkConfig::set([
            $style_key => $style,
            $img_key => array_unique($img),
        ]);
    }
}
