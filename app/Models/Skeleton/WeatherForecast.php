<?php

namespace DeBear\Models\Skeleton;

use DeBear\Implementations\Model;

class WeatherForecast extends Model
{
    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql_common';
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'WEATHER_FORECAST';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'app',
        'instance_key',
        'forecast_date',
    ];
    /**
     * Date columns within the database table
     * @var array
     */
    protected $casts = [
        'forecast_date' => 'datetime',
        'last_updated' => 'datetime',
    ];
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;
    /**
     * The primary key is not auto-incrementing
     * @var boolean
     */
    public $incrementing = false;
}
