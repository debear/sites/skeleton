<?php

namespace DeBear\Console;

use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Console\Application as Artisan;
use Symfony\Component\Finder\Finder;

class Kernel extends ConsoleKernel
{
    /**
     * Register the commands for the application.
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }

    /**
     * A bespoke, symlink-sensitive, command loader for the given directory.
     * @param  array|string $paths One or more base paths to autoload commands.
     * @return void
     */
    protected function load($paths)
    {
        $namespace = $this->app->getNamespace();
        $console_base = strlen(realpath(__DIR__ . '/..') . '/');
        foreach ((new Finder())->in($paths)->followLinks()->files() as $command) {
            $command = $namespace . str_replace(
                ['/', '.php'],
                ['\\', ''],
                substr($command->getPathname(), $console_base)
            );
            Artisan::starting(function ($artisan) use ($command) {
                $artisan->resolve($command);
            });
        }
    }
}
