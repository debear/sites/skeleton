<?php

namespace DeBear\Console\Commands\Skeleton\Config;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\Arrays;
use DeBear\Helpers\Config;

class Env extends Command
{
    // Rather than generate phan errors, we will explicitly copy and use Symfony's Command constants.
    public const SUCCESS = 0;
    public const FAILURE = 1;
    public const INVALID = 2;

    /**
     * The name and signature of the console command.
     * @var string
     */
    protected $signature = 'debear:skeleton:config-env';

    /**
     * The console command description.
     * @var string
     */
    protected $description = 'List the local, uncommited, environmental configuration in place';

    /**
     * Execute the console command.
     * @return integer A return status
     */
    public function handle()
    {
        // Re-load config without env files.
        Config::disableCustomConfig();
        FrameworkConfig::set(['debear' => Config::postProcess(Config::getRawConfig())]);
        Config::enableCustomConfig();

        // Find any _env.php files in each repo.
        $base = realpath(base_path() . '/..');
        $base_len = strlen($base) + 1;
        $files = [];
        foreach (['_env', '_ci'] as $test) {
            foreach (['skeleton' => config_path() . '/debear', "$base/*/config"] as $key => $glob) {
                foreach (glob("$glob/$test.php") as $cfg) {
                    $repo = (is_string($key) ? $key : str_replace("/config/$test.php", '', substr($cfg, $base_len)));
                    if (!isset($files[$repo])) {
                        $files[$repo] = [];
                    }
                    $files[$repo][$test] = $cfg;
                }
            }
        }

        // Process each found file.
        $this->info("PRE-REPO ENVIRONMENTAL FILES AND CONFIGURATION");
        foreach ($files as $repo => $files) {
            $this->newLine();
            $this->info("$repo:");
            $table = [];
            foreach ($files as $source => $env) {
                $table = array_merge($table, $this->parseConfigRecursive($repo, $source, '', include $env));
            }
            $this->table(['Source', 'Key', 'Value', 'Original'], $table);
        }
        return static::SUCCESS;
    }

    /**
     * Recursively identify and format for output an environmental configuration file
     * @param string $repo   The repository where the configuration being parsed was found.
     * @param string $source The source file for the configuration being parsed.
     * @param string $prefix The configuration prefix that applies at the level of the configuration being parsed.
     * @param array  $config The configuration array to be parsed.
     * @return array The array of configuration, built ready to be passed to $this->table
     */
    protected function parseConfigRecursive(string $repo, string $source, string $prefix, array $config): array
    {
        $ret = [];
        foreach ($config as $key => $value) {
            $key = (!$prefix ? $key : "$prefix.$key"); // Append the prior config path.
            $direct_assoc = is_array($value) && Arrays::isAssociative($value);
            $indirect_assoc = is_array($value) && !Arrays::isAssociative($value)
                && is_array($value[0]) && Arrays::isAssociative($value[0]);
            if ($direct_assoc || $indirect_assoc) {
                $ret = array_merge($ret, $this->parseConfigRecursive($repo, $source, $key, $value));
            } else {
                // Format row for direct $this->table argument.
                $ret[] = [
                    $source,
                    $key,
                    json_encode($value),
                    json_encode(FrameworkConfig::get("debear.$repo.$key"))
                ];
            }
        }
        return $ret;
    }
}
