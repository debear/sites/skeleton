<?php

namespace DeBear\Helpers;

class Highcharts
{
    /**
     * A flag to say we've loaded the appropriate resources in to the page
     * @var boolean
     */
    protected static $loadedResources = false;
    /**
     * A list of functions used in decoding the JavaScript object
     * @var array
     */
    protected static $objFns = [];

    /**
     * Create a new base JS-compatible object
     * @param string $dom_id ID of the DOM element the chart is to be added to.
     * @param array  $opt    Custom config options. (Optional).
     * @return array Base object
     */
    public static function new(string $dom_id, array $opt = []): array
    {
        static::setupOptions($opt);
        static::setupResources($opt);
        $highchartsObject = [
            'chart' => [
                'styledMode' => $opt['styled-mode'],
                'renderTo' => $dom_id,
            ],
            'title' => [
                'text' => $opt['title'],
            ],
            'credits' => [
                'enabled' => false,
            ],
        ];
        return $highchartsObject;
    }

    /**
     * Set default values for options being passed in
     * @param array $opt Customised optinos, passed by reference.
     * @return void
     */
    protected static function setupOptions(array &$opt): void
    {
        $opt = Arrays::merge([
            'styled-mode' => true,
            'title' => '',
            'annotations' => false,
            'js-module' => '',
        ], $opt);
    }

    /**
     * Ensure the CSS and JS are loaded for the page once - and only once
     * @param array $opt Custom config options. (Optional).
     * @return void
     */
    public static function setupResources(array $opt = []): void
    {
        // Skip if we've already done so.
        if (static::$loadedResources) {
            return;
        }
        static::setupOptions($opt); // Ensure our defaults are in place if called externally.

        // We've not, so add the JS/CSS files.
        $cssjs_opt = ['external' => true];
        Resources::addJS('highcharts/highcharts.src.js', $cssjs_opt);
        Resources::addJS('highcharts/accessibility.src.js', $cssjs_opt);
        Resources::addJS('highcharts/highcharts.debear.js', $cssjs_opt); // Custom tweaks to the standard library.
        if ($opt['annotations']) {
            Resources::addJS('highcharts/annotations.src.js', $cssjs_opt);
        }
        if ($opt['js-module']) {
            Resources::addJS('highcharts/highcharts-more.src.js');
            Resources::addJS('highcharts/' . $opt['js-module'] . '.src.js');
        }
        if ($opt['styled-mode']) {
            Resources::addCSS('highcharts/highcharts.css', $cssjs_opt);
            Resources::addCSS('skel/modules/override/highcharts.css', $cssjs_opt);
        }

        // Record the resource processing has been completed.
        static::$loadedResources = true;
    }


    /**
     * Decode a DeBear Highcharts object into its JSON equivalent
     * @param array $obj Original Highcharts object, where closures are strings.
     * @return string The JSON-compatible Highcharts object
     */
    public static function decode(array $obj): string
    {
        // First we need to find anonymous functions, as json_encode doesn't handle them properly.
        $rand = Strings::randomAlphaNum(5);
        static::$objFns[$rand] = [];
        array_walk_recursive($obj, [static::class, 'decodeFunctionWalk'], $rand);

        // Then convert to text.
        $objEnc = json_encode($obj);

        // And finally revert the functions then return.
        foreach (static::$objFns[$rand] as $i => $fn) {
            $objEnc = str_replace('"{{' . $i . '}}"', $fn, $objEnc);
        }
        return $objEnc;
    }

    /**
     * Find closures within a Highcharts object and preserve for later processing
     * @param mixed  $item The array element being checked for JavaScript closure. (Pass-by-reference).
     * @param string $key  The array element's key.
     * @param string $rand The random string for our object function map.
     * @return void
     */
    protected static function decodeFunctionWalk(mixed &$item, string $key, string $rand): void
    {
        if (preg_match('/^function\s*\(\)\s*{/msi', $item)) {
            $fn = $item;
            $item = '{{' . sizeof(static::$objFns[$rand]) . '}}';
            static::$objFns[$rand][] = $fn;
        }
    }

    /**
     * Maxes based on primary y-axis and the number of divisions in the secondary axis
     *  -> start at max_pri / 2 because the highest divisor will be half its value
     * @param float|integer $maxPri Maximum value of the primary axis. (Pass-by-reference).
     * @param float|integer $maxSec Maximum value of the secondary axis (Pass-by-reference).
     * @return integer The appropriate divisor for the two axes
     */
    public static function calculateAxisRange(float|int &$maxPri, float|int &$maxSec): int
    {
        // Avoid problems with prime numbers by making them even.
        if ($maxPri % 2 == 1) {
            ++$maxPri;
        }

        $divisor = -1;
        for ($diff = 0; $divisor == -1 && $diff < 5; $diff++) {
            $max = $maxPri + $diff;
            for ($i = floor($max / 2); $i > 1 && $divisor == -1; $i--) {
                if ($max % $i == 0) {
                    $maxPri += $diff;
                    $divisor = $i;
                }
            }
        }

        // Now make the points max a multiple of this number.
        $maxSec = ceil($maxSec / $divisor) * $divisor;

        return $divisor;
    }
}
