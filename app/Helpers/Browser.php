<?php

namespace DeBear\Helpers;

use stdClass;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use BrowscapPHP\Browscap;
use League\Flysystem\Filesystem;
use League\Flysystem\Local\LocalFilesystemAdapter;
use MatthiasMullie\Scrapbook\Psr16\SimpleCache;
use MatthiasMullie\Scrapbook\Adapters\Flysystem;
use Monolog\Logger;

class Browser
{
    /**
     * Cached built Browser info
     * @var ?stdClass
     */
    protected static $browser;
    /**
     * Hashed version representing the user's browser
     * @var ?string
     */
    protected static $internalHash;

    /**
     * Get a parsed Browscap object
     * @return stdClass Object with parsed browser info
     */
    public static function get(): stdClass
    {
        if (!isset(static::$browser)) {
            /* @phan-suppress-next-line PhanUndeclaredClassMethod */
            $adapter = new LocalFilesystemAdapter(static::getFilePath());
            /* @phan-suppress-next-line PhanTypeMismatchArgument */
            $cache = new SimpleCache(new Flysystem(new Filesystem($adapter)));
            $logger = new Logger('name');
            /* @phan-suppress-next-line PhanTypeMismatchArgument */
            $browscap = new Browscap($cache, $logger);
            static::$browser = $browscap->getBrowser();
        }

        return static::$browser;
    }

    /**
     * Reset the cached browser info we have stored
     * @return void
     */
    public static function reset(): void
    {
        static::$browser = null;
    }

    /**
     * Determine where the GeoIP database file is
     * @return string Path to the GeoIP database on the filesystem
     */
    protected static function getFilePath(): string
    {
        $cacheDir = (!HTTP::isTest() ? 'browscap/browscap-php' : 'debear/setup/browscap');
        return FrameworkConfig::get('debear.dirs.vendor') . "/$cacheDir/resources";
    }

    /**
     * Return a hashed version of the User Agent string
     * @return string A special hashed version of the User Agent
     */
    public static function internalHash(): string
    {
        // Previously calced?
        if (isset(static::$internalHash)) {
            return static::$internalHash ?? ''; // Phan linting quirk.
        }

        // Build a hash from browser components.
        $b = static::get();
        $salt = FrameworkConfig::get('debear.salts.session_ua');
        // The use of MD5 is not for cryptographic purposes, so consider the SemGrep warning a false positive.
        // nosemgrep PHPCS_SecurityAudit.BadFunctions.CryptoFunctions.WarnCryptoFunc.
        static::$internalHash = md5($salt . '::' . $b->browser . '::' . $b->version . '::' . $b->platform);

        // Nope, generate and return.
        return static::$internalHash;
    }

    /**
     * Reset the cached hash info we have stored
     * @return void
     */
    public static function resetHash(): void
    {
        static::reset();
        static::$internalHash = null;
    }
}
