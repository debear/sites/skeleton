<?php

namespace DeBear\Helpers;

use DeBear\Exceptions\FormatException;

class Format
{
    /**
     * Given a number, get its ending, e.g.: 1 = 1st, 2 = 2nd, 13 = 13th, 23 = 23rd, 1250 = 1250th
     * @param integer $num The number to be ordinalised.
     * @return string The ordinalised string
     */
    public static function ordinal(int $num): string
    {
        $num_clean = preg_replace('/[^\d\.]/', '', (string)$num);
        $end_digit = substr($num_clean, strlen($num_clean) - 1);
        $end_digits = substr($num_clean, strlen($num_clean) - 2);
        if ($end_digit == 1 && $end_digits != 11) {
            return $num . 'st';
        } elseif ($end_digit == 2 && $end_digits != 12) {
            return $num . 'nd';
        } elseif ($end_digit == 3 && $end_digits != 13) {
            return $num . 'rd';
        }
        return $num . 'th';
    }

    /**
     * Pluralise text, depending on the preceding number
     * @param float  $num The number we are testing against.
     * @param string $str The string to be pluralised.
     * @param array  $opt Additional configuration options. (Optional).
     * @return string The pluralised string
     */
    public static function pluralise(float $num, string $str = '', array $opt = []): string
    {
        $append = !isset($opt['prepend-str']) || !$opt['prepend-str'];
        // Account for spaces within the string, as $plural needs to occur before the space.
        $space = '';
        if (!$append && substr($str, -1) == ' ') {
            $str = rtrim($str);
            $space = ' ';
        }
        // Do we need to change the grammar slightly?
        if ($num != 1 && preg_match('/[^aeiou]y$/', $str)) {
            $str = substr($str, 0, -1) . 'ie';
        }
        // Run the pluralisation.
        $plural = isset($opt['plural']) ? $opt['plural'] : 's';
        $pluralised = $str . (abs($num) == 1 ? '' : $plural) . $space;
        if (isset($opt['format-number'])) {
            $num_dp = is_numeric($opt['format-number'])
                ? $opt['format-number']
                : ($num != intval($num) ? 1 : 0);
            $num = number_format($num, $num_dp);
        }
        return (!$append ? $pluralised : '')
            . ((!isset($opt['hide-num']) || !$opt['hide-num']) ? $num : '')
            . (!$append ? '' : $pluralised);
    }

    /**
     * Singularise some text
     * @param  string $str      The string to be singularised.
     * @param  string $plural   The pluralised string to be removed.
     * @param  string $singular The singularisation to be used instead.
     * @return string The singularised string
     */
    public static function singularise(string $str, string $plural = 's', string $singular = ''): string
    {
        // Skip if no pluralisation needed.
        $plural_len = strlen($plural);
        if (substr($str, 0 - $plural_len) != $plural) {
            return $str;
        }
        return substr($str, 0, 0 - $plural_len) . $singular;
    }

    /**
     * Convert a number in decimal to roman numerals
     * @param integer|string $n The number to convert.
     * @return string The converted number in roman numerals
     */
    public static function decimalToRoman(int|string $n): string
    {
        $c = 'IVXLCDM';
        for ($a = 5, $b = 0, $s = ''; $n; $b++, $a ^= 7) {
            for ($o = $n % $a, $n = $n / $a ^ 0; $o--; $s = $c[$o > 2 ? $b + $n - ($n &= -2) + $o = 1 : $b] . $s) {
                // (Logic in the loop).
            }
        }
        return $s;
    }

    /**
     * Convert a raw byte total into more human-readable format
     * @param integer $size Raw number of bytes.
     * @return string The formatted number of bytes
     * @throws FormatException When an invalid input was passed in.
     */
    public static function bytes(int $size): string
    {
        // Negative == Exception!
        if ($size < 0) {
            throw new FormatException('Cannot format negative bytes value');
        // Bytes.
        } elseif ($size < 1024) { // Less than 1 kilibyte.
            return $size . ' B';
        // Kilibytes.
        } elseif ($size < 1048576) { // Less than 1 mebibyte.
            return sprintf('%0.03f KiB', $size / 1024);
        // Mebibytes.
        } elseif ($size < 1073741824) { // Less than 1 gibibyte.
            return sprintf('%0.03f MiB', $size / 1048576);
        // Gibibytes.
        } elseif ($size < 1099511627776) { // Less than 1 tebibyte.
            return sprintf('%0.03f GiB', $size / 1073741824);
        // Tebibytes.
        } else {
            return sprintf('%0.03f TiB', $size / 1099511627776);
        }
    }

    /**
     * Convert an age in seconds into more human-readable format
     * Note: This is constructed using crude, not timezone-safe, logic
     * @param integer $seconds Raw number of seconds.
     * @return string The formatted age
     * @throws FormatException When an invalid input was passed in.
     */
    public static function age(int $seconds): string
    {
        // Negative == Exception!
        if ($seconds < 0) {
            throw new FormatException('Cannot format negative age value');
        // Seconds.
        } elseif ($seconds < 60) { // Less than 1 minute.
            return Format::pluralise($seconds, ' sec');
        // Minutes.
        } elseif ($seconds < 3600) { // Less than 1 hour.
            return Format::pluralise(floor($seconds / 60), ' min');
        // Hours.
        } elseif ($seconds < 86400) { // Less than 1 day.
            return Format::pluralise(floor($seconds / 3600), ' hour');
        // Days.
        } elseif ($seconds < 31536000) { // Less than 1 year.
            return Format::pluralise(floor($seconds / 86400), ' day');
        // Years.
        } else {
            return Format::pluralise(floor($seconds / 31536000), ' year');
        }
    }

    /**
     * Concatenate a list of numbers, but grouping ranges together (e.g., "1, 3, 5-9")
     * @param array  $list     The list of numbers to process.
     * @param string $join     The string to separate the values in a range.
     * @param string $sep      The string to separate non-range values.
     * @param string $last_sep The string to separate the final two non-range values.
     * @return string The joined string of numbers
     */
    public static function groupRanges(
        array $list,
        string $join = '&ndash;',
        string $sep = ', ',
        string $last_sep = ' &amp; '
    ): string {
        // The simple cases...
        $len = count($list);
        $simple = null;
        switch ($len) {
            // Simple case A: empty array == empty string.
            case 0:
                $simple = '';
                break;
            // Simple case B: there is only one value, in which case we return just the one value.
            case 1:
                $simple = (string)array_shift($list);
                break;
        }
        if (isset($simple)) {
            return $simple;
        }

        // Those that require more processing.
        $list = array_values($list);
        sort($list);
        $ret = [];
        $last = null;
        foreach ($list as $ele) {
            if (!isset($last)) {
                // Starting a new block.
                $last = ['s' => $ele, 'e' => $ele];
            } elseif ($ele == ($last['e'] + 1)) {
                // Continuing the previous block.
                $last['e'] = $ele;
            } else {
                // Adding the last block to our list and starting a new one.
                static::groupRangesAdd($ret, $last);
                // Then start a new block.
                $last = ['s' => $ele, 'e' => $ele];
            }
        }
        // Add the final block.
        static::groupRangesAdd($ret, $last);
        // Convert from our 'start' / 'end' elements to a number or range.
        array_walk($ret, function (&$a, $key, $join) {
            $key .= ''; // PHPMD satisfaction.
            $a = ($a['s'] == $a['e'] ? (string)$a['s'] : "{$a['s']}{$join}{$a['e']}");
        }, $join);
        // And return as a joined string.
        return Strings::naturalJoin($ret, $sep, $last_sep);
    }

    /**
     * Add the last range block to our internal range grouping array
     * @param array $ret  The return array to be processed in to a string. (Pass-by-reference).
     * @param array $last The last block found.
     * @return void
     */
    protected static function groupRangesAdd(array &$ret, array $last): void
    {
        // Rather than "1-2", return "1, 2", despite being continuous.
        if (($last['e'] - $last['s']) == 1) {
            $ret[] = ['s' => $last['s'], 'e' => $last['s']];
            $ret[] = ['s' => $last['e'], 'e' => $last['e']];
        } else {
            $ret[] = $last;
        }
    }
}
