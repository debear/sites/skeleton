<?php

namespace DeBear\Helpers;

use Illuminate\Support\Facades\Cookie as FrameworkCookie;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Support\Facades\App;
use DeBear\Repositories\Time;

class Cookie
{
    /**
     * Changes to the cookie since the page was loaded.
     * @var array
     */
    protected static $changes = [];

    /**
     * Stores a cookie on the user's system
     * @param string          $var     The name for the cookie.
     * @param mixed           $value   The value we are storing in the cookie.
     * @param integer|boolean $expires When the cookie should expire (or false, for the default value).
     * @return void
     */
    public static function setValue(string $var, mixed $value, int|bool $expires = false): void
    {
        // Process.
        if (!$expires) {
            $expires = App::make(Time::class)->adjustFormatServer(
                '+' . FrameworkConfig::get('debear.sessions.cutoffs.cookie') . ' minutes',
                'U'
            );
        }
        // Preserve the change for future in-page requests (before we encrypt it).
        static::$changes[$var] = $value;
        // Encrypt, or set to empty string if no value passed.
        $value = static::encryptValue(is_string($value) ? $value : json_encode($value)) ?? '';
        // Then update the cookie (skipping on our (stateless) test environment).
        $domain = FrameworkConfig::get('debear.sessions.domain');
        !HTTP::isTest() && setcookie($var, $value, $expires, '/', $domain, true, true);
    }

    /**
     * Removes a cookie from the users system
     * @param string $var The name for the cookie we are unsetting.
     * @return void
     */
    public static function unsetValue(string $var): void
    {
        // Unset by removing its contents, and settings its expired time to an hour ago.
        if (static::hasValue($var)) {
            static::setValue($var, null, App::make(Time::class)->adjustFormatServer('-1 hour', 'U'));
        }
    }

    /**
     * Check to see if a cookie value exists
     * @param string $var The name for the cookie we are unsetting.
     * @return boolean Whether $var exists as a cookie value or not
     */
    public static function hasValue(string $var): bool
    {
        return static::getValue($var) !== null;
    }

    /**
     * Retrieve a cookie from the user's system, and decrypt if it exists
     * @param string $var The name for the cookie.
     * @return string|null The decrypted cookie value. or null of not defined
     */
    public static function getValue(string $var): string|null
    {
        if (isset(static::$changes[$var])) {
            // If we have an updated value since the page was loaded, use this.
            return static::$changes[$var];
        }

        // Otherwise, get it from the cookie we were given.
        $value = Cookie::get($var);
        return is_string($value) ? static::decryptValue($value) : $value;
    }

    /**
     * Encode a string for storing in the cookie
     * @param string $str The string to encrypt.
     * @return string The encrypted string
     */
    public static function encryptValue(string $str): string
    {
        // If no string, no need to encrypt, we intentioanlly want an empty value.
        if (!$str) {
            return '';
        }
        $rand = Strings::randomAlphaNum(3);
        return $rand . Strings::twoWayEncrypt($str) . Strings::md5Code($rand, 3);
    }

    /**
     * Decrypt the string we were given in the cookie
     * @param string $enc The encrypted value to decrypt and validate.
     * @return string|null The original value, or null if it is not valid
     */
    public static function decryptValue(string $enc): string|null
    {
        // Validate our security checks.
        if (Strings::md5Code(substr($enc, 0, 3), 3) == substr($enc, -3)) {
            return Strings::twoWayDecrypt(substr($enc, 3, -3));
        }
        // An error, implies no valid session.
        return null;
    }

    /**
     * Fallback static to call the Framework's Cookie:: class
     * @param string $method     Requested method we need to pass on to the FrameworkCookie class.
     * @param array  $parameters Arguments in the method call.
     * @return mixed  Whatever was returned by the FrameworkCookie class
     */
    public static function __callStatic(string $method, array $parameters): mixed
    {
        return call_user_func_array([FrameworkCookie::class, $method], $parameters);
    }
}
