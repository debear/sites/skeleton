<?php

namespace DeBear\Helpers\Widgets;

use Illuminate\Contracts\View\View;
use DeBear\Helpers\Resources;

class Pagination
{
    /**
     * The number groupings to use
     * @var integer
     */
    protected static $grouping = 10;
    /**
     * The max number displayed either side of the page in narrow view
     * @var integer
     */
    protected static $narrow_max = 2;

    /**
     * Show the Prev, List and Next links
     * @param integer $num_rows Total number of rows available.
     * @param integer $per_page The number of rows we dispay per page.
     * @param string  $url      The base of the URL we'll apply.
     * @param integer $page_num Current page number.
     * @param array   $opt      Custom options to pass to the processing.
     * @return View
     */
    public static function render(int $num_rows, int $per_page, string $url, int $page_num, array $opt = []): View
    {
        // Add the CSS to our list to display.
        Resources::addCSS('skel/widgets/pagination.css');

        // Custom options?
        $extra_css = ($opt['extra_css'] ?? '');

        // Processing.
        $next_rows = $num_rows - ($page_num * $per_page);
        $next_title = 'Last';
        if ($next_rows > $per_page) {
            $next_rows = $per_page;
            $next_title = 'Next';
        }

        // Get the list of individual pages.
        $ind_pages = static::getNumbers($num_rows, $per_page, $url, $page_num);
        return view(
            'skeleton.widgets.pagination',
            compact(
                [
                    'ind_pages',
                    'extra_css',
                    'page_num',
                    'url',
                    'next_rows',
                    'next_title',
                ]
            )
        );
    }

    /**
     * Format a page number for the navigation
     * @param string  $url      Base URL.
     * @param integer $page_num New page number.
     * @return string
     */
    public static function formatNumber(string $url, int $page_num): string
    {
        if (strpos($url, '${page_num}') !== false) {
            // Find and replace "page_num" (i.e., for JavaScript).
            return str_replace('${page_num}', (string)$page_num, $url);
        } else {
            // Just append.
            return $url . $page_num;
        }
    }

    /**
     * Show a list of pages
     * @param integer $num_rows Total number of rows available.
     * @param integer $per_page The number of rows we dispay per page.
     * @param string  $url      The base of the URL we'll apply.
     * @param integer $page_num Current page number.
     * @return array The page links relevant to the page
     */
    public static function getNumbers(int $num_rows, int $per_page, string $url, int $page_num): array
    {
        $num_pages = static::groupNumbers($num_rows, $per_page);

        // Small list?
        if ($num_pages <= (static::$grouping + 1)) {
            $the_list = static::getNumbersShort($num_pages, $url, $page_num);
        } else {
            $the_list = static::getNumbersExpanded($num_pages, $url, $page_num);
        }

        // Return the list of pages.
        return $the_list;
    }

    /**
     * Show a list of pages which fit in a single list
     * @param integer $num_pages Total number of pages available.
     * @param string  $url       The base of the URL we'll apply.
     * @param integer $page_num  Current page number.
     * @return array The page links relevant to the page
     */
    protected static function getNumbersShort(int $num_pages, string $url, int $page_num): array
    {
        $the_list = [];
        for ($i = 1; $i <= $num_pages; $i++) {
            if ($i == $page_num) {
                // Don't show a link, just the number.
                $the_list[] = [
                    'title' => "<strong>$i</strong>",
                    'shade' => false,
                    'narrow' => true,
                ];
            } else {
                // Print as a link.
                $the_list[] = [
                    'title' => '<a '
                        . static::buildLinkAttribute($url, $i, sizeof($the_list))
                        . ' class="onclick_target" data-page="' . $i . '">' . $i . '</a>',
                    'link'  => static::formatNumber($url, $i),
                    'shade' => true,
                    'narrow' => abs($i - $page_num) <= static::$narrow_max,
                ];
            }
        }
        return $the_list;
    }

    /**
     * Show a list of pages which fit in a single list
     * @param integer $num_pages Total number of pages available.
     * @param string  $url       The base of the URL we'll apply.
     * @param integer $page_num  Current page number.
     * @return array The page links relevant to the page
     */
    protected static function getNumbersExpanded(int $num_pages, string $url, int $page_num): array
    {
        $half_grouping = floor((static::$grouping - 1) / 2);
        $the_list = [];

        // Show the beginning and original &hellip; only if required.
        if ($page_num - $half_grouping > 1) {
            $the_list[] = [
                'title' => '<a '
                    . static::buildLinkAttribute($url, 1, sizeof($the_list))
                    . ' class="onclick_target" data-page="1"><em>1</em></a>',
                'link'  => static::formatNumber($url, 1),
                'shade' => true,
                'narrow' => true,
            ];
            if ($page_num != 6) {
                $the_list[] = [
                    'title' => '&hellip;',
                    'shade' => false,
                    'narrow' => true,
                ];
            }
        }

        // Get the start/end points.
        $start = $page_num - $half_grouping;
        $end = $page_num + $half_grouping + 1;

        // Now adjust.
        if ($start < 1) {
            $end = $end - $start + 1;
            $start = 1;
        }
        if ($end > $num_pages) {
            $start = $start - $end + $num_pages;
            $end = $num_pages;
        }

        // Show the list.
        for ($i = $start; $i <= $end; $i++) {
            if ($i == $page_num) {
                // Don't show a link, just the number.
                $the_list[] = [
                    'title' => "<strong>$i</strong>",
                    'shade' => false,
                    'narrow' => true,
                ];
            } else {
                // Print as a link.
                $the_list[] = [
                    'title' => '<a '
                        . static::buildLinkAttribute($url, $i, sizeof($the_list))
                        . ' class="onclick_target" data-page="' . $i . '">' . $i . '</a>',
                    'link'  => static::formatNumber($url, $i),
                    'shade' => true,
                    'narrow' => abs($i - $page_num) <= static::$narrow_max,
                ];
            }
        }

        // Show the end and final &hellp; only if required.
        if ($page_num + $half_grouping + 1 < $num_pages) {
            if ($page_num != ($num_pages - 6)) {
                $the_list[] = [
                    'title' => '&hellip;',
                    'shade' => false,
                    'narrow' => true,
                ];
            }
            $the_list[] = [
                'title' => '<a '
                    . static::buildLinkAttribute($url, $num_pages, sizeof($the_list))
                    . ' class="onclick_target" data-page="' . $num_pages . '"><em>' . $num_pages . '</em></a>',
                'link'  => static::formatNumber($url, $num_pages),
                'shade' => true,
                'narrow' => true,
            ];
        }
        return $the_list;
    }

    /**
     * Build the href="" or id="" attribute we use in our links
     * @param string         $url       The base of the URL we'll apply.
     * @param integer        $num_pages Total number of pages available.
     * @param string|integer $page_num  Current page number.
     * @return string The appropriate attribute for the passed args
     */
    public static function buildLinkAttribute(string $url, int $num_pages, string|int $page_num): string
    {
        return ($url ? 'href="' . static::formatNumber($url, $num_pages) . '"' : '');
    }

    /**
     * Break a number into a group of numbers
     * @param integer $total    Total number of rows.
     * @param integer $grouping Grouping size.
     * @return integer The number of pages to display
     */
    protected static function groupNumbers(int $total, int $grouping): int
    {
        return intval(floor($total / $grouping)) + ($total % $grouping > 0 ? 1 : 0);
    }
}
