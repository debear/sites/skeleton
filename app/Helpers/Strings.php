<?php

namespace DeBear\Helpers;

use Illuminate\Support\Facades\App;
use Symfony\Component\String\UnicodeString;
use DeBear\Models\Skeleton\User;
use DeBear\Repositories\Time;
use DeBear\Exceptions\StringsException;

class Strings
{
    /**
     * Make a string database safe
     * @param string  $string    Input string.
     * @param boolean $html_mode If we're converting in HTML mode or just escaping slashes.
     * @return string The escaped string
     */
    public static function encodeString(string $string, bool $html_mode = true): string
    {
        // If we're not HTML encoding, escape instead.
        if (!$html_mode) {
            return addslashes($string);
        }

        // Straight-up HTML conversion, without double encoding.
        $string = htmlentities($string, ENT_QUOTES, 'UTF-8', false);

        // Then check for Unicode characters (outside the range 0 - 127).
        if (!preg_match('/[^\x00-\x7F]/u', $string)) {
            return $string;
        }

        // Identify and convert those characters.
        $chars = [];
        $num_chars = 1;
        for ($i = 0; $i < strlen($string); $i++) {
            $ord = ord($string[$i]);
            // Skip this (standard) character.
            if ($ord < 128) {
                continue;
            }

            // Get the code for this character.
            if (count($chars) == 0) {
                $num_chars = ($ord < 224 ? 2 : 3);
            }
            $chars[] = $ord;

            if (count($chars) == $num_chars) {
                $code = ($num_chars == 3 ? ($chars[0] % 16) * 4096 : 0); // Special handling of 3-char.
                $code += (($chars[$num_chars - 2] % 32) * 64) + ($chars[$num_chars - 1] % 64);

                // Now we have the code, update inside a "&#...;".
                $string = substr_replace($string, '&#' . $code . ';', $i - $num_chars + 1, $num_chars);
                $i += 6 - $num_chars;

                // Reset internal vars.
                $chars = [];
                $num_chars = 1;
            }
        }

        return $string;
    }

    /**
     * Make the contents of an input database safe
     * @param mixed   $input     The input to encode.
     * @param boolean $html_mode If we're converting in HTML mode or just escaping slashes.
     * @return mixed The escaped input
     */
    public static function encodeInput(mixed $input, bool $html_mode = true): mixed
    {
        if (!is_array($input)) {
            return is_string($input) && mb_detect_encoding($input, null, true)
                ? static::encodeString($input, $html_mode)
                : $input;
        }

        foreach (array_keys($input) as $key) {
            $input[$key] = static::encodeInput($input[$key], $html_mode);
        }
        return $input;
    }

    /**
     * Decode a database safe string
     * @param string  $string The string to decode.
     * @param boolean $nl2br  Whether we should convert newlines to HTML equivalents. (Optional).
     * @return string The decoded string
     */
    public static function decodeString(string $string, bool $nl2br = true): string
    {
        $ret = html_entity_decode($string, ENT_QUOTES | ENT_HTML5, 'UTF-8');
        return $nl2br ? static::convertNewlines($ret) : $ret;
    }

    /**
     * Decode \n into HTML equivalents
     * @param string $str The string to convert.
     * @return string The converted string
     */
    public static function convertNewlines(string $str): string
    {
        return (preg_match('/[\r\n]/', $str) ? '<p>' : '')
            .  preg_replace('/[\r\n]+/', '</p><p>', $str)
            . (preg_match('/[\r\n]/', $str) ? '</p>' : '');
    }

    /**
     * Generate a random alphanumeric string up to $len chars long
     * @param integer $len The final length of the string.
     * @return string The built string
     */
    public static function randomAlphaNum(int $len): string
    {
        $lexicon = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        $rand_max = strlen($lexicon) - 1;
        $str = '';
        for ($i = 0; $i < $len; $i++) {
            $str .= $lexicon[mt_rand(0, $rand_max)];
        }
        return $str;
    }

    /**
     * Generate a codified version of a string
     * @param string $str The string to be codified.
     * @return string The codified string
     */
    public static function codify(string $str): string
    {
        return trim(preg_replace('/[^a-z0-9]+/i', '-', strtolower($str)), '-');
    }

    /**
     * Generate a codified version of the string suitable for a URL
     * @param string $str The string to be codified.
     * @return string The URL codified string
     */
    public static function codifyURL(string $str): string
    {
        if (strpos($str, '&') !== false) {
            // If we have HTML in the name, then normalise to Latin equivalents.
            $str = (new UnicodeString(static::decodeString($str, false)))->ascii();
        }
        return static::codify($str);
    }

    /**
     * Produce a "natural join" string from a list: e.g., "a, b, c & d"
     * @param  array  $list     The list of items to join.
     * @param  string $sep      The "main" separator to use.
     * @param  string $last_sep The separator to use beteen the final two items.
     * @return string The string of items joined according to the passed separators
     */
    public static function naturalJoin(array $list, string $sep = ', ', string $last_sep = ' &amp; '): string
    {
        $len = count($list);
        // If we don't need the non-final separator, do the easier join using the last separator.
        if ($len <= 2) {
            return join($last_sep, $list);
        }
        // Now use the main separator as the primary.
        $last = array_pop($list);
        return join($sep, $list) . "$last_sep$last";
    }

    /**
     * Return an MD5 codified version of a string, up to the given length
     * @param string  $str The string to be MD5'd.
     * @param integer $len String length to be returned. (Optional).
     * @return string The codified MD5 string of the given length
     */
    public static function md5Code(string $str, int $len = 12): string
    {
        // The use of MD5 is not for cryptographic purposes, so consider the SemGrep warning a false positive.
        // nosemgrep PHPCS_SecurityAudit.BadFunctions.CryptoFunctions.WarnCryptoFunc.
        return substr(md5($str), 0, $len);
    }

    /**
     * Generate a one-way hash of a string (for instance, in our password manager)
     * @param string $str  The string to be one-way hashed.
     * @param string $salt The salt to use within the hashing.
     * @param array  $opt  Custom processing options (Optional).
     * @return string The one-way hashed string / salt combination
     */
    public static function oneWayHash(string $str, string $salt, array $opt = []): string
    {
        // The use of MD5 is not for cryptographic purposes, so consider the SemGrep warning a false positive.
        // nosemgrep PHPCS_SecurityAudit.BadFunctions.CryptoFunctions.WarnCryptoFunc.
        $salt_base = strrev(md5(strrev(strtolower($salt)))); // All salts must be in lowercase.
        $salt_l = substr($salt_base, 0, 5);
        $salt_r = substr($salt_base, -5);

        // Update the string to include our salt.
        $hash_base = $salt_l . strrev($str) . $salt_r;

        // Return as first 30 (default) chars of the ensuing (reversed) MD5.
        $len = ($opt['len'] ?? 30);
        // The use of MD5 is not for cryptographic purposes, so consider the SemGrep warning a false positive.
        // nosemgrep PHPCS_SecurityAudit.BadFunctions.CryptoFunctions.WarnCryptoFunc.
        return substr(strrev(md5($hash_base)), 0, $len);
    }

    /**
     * Generate an encrypted two-way encrypted string. To be used when security is less of a concern
     * @param string $str  The string to encrypt.
     * @param string $salt The salt to use within the encryption.
     * @return string The encrypted string
     */
    public static function twoWayEncrypt(string $str, string $salt = ''): string
    {
        return rtrim(base64_encode("$salt$str"), '=');
    }

    /**
     * Decrypt a previously two-way encrypted string. To be used when security is less of a concern
     * @param string $str  The string to decrypt.
     * @param string $salt The salt to use within the decryption.
     * @return string The decrypted string
     * @throws StringsException When unable to decrypt a string.
     */
    public static function twoWayDecrypt(string $str, string $salt = ''): string
    {
        // Length check (as we may store the encoded version without the padding chars).
        $strlen = strlen($str);
        $pad = ($strlen % 4);
        if ($pad) {
            $str = str_pad($str, $strlen + 4 - $pad, '=');
        }
        $decoded = base64_decode($str);
        if (substr($decoded, 0, strlen($salt)) !== $salt) {
            throw new StringsException("Unable to decrypt a two-way string");
        }
        return substr($decoded, strlen($salt));
    }

    /**
     * Create a time locked code that refers to a particular user
     * @param string|boolean $user_id The user_id we want to create a reset code for. (Optional).
     * @return string The verification code
     * @throws StringsException When the timelock validation failed.
     */
    public static function generateUserTimeLock(string|bool $user_id = false): string
    {
        // Ensure we have a user to link to.
        if (!$user_id) {
            $user_id = (User::object() ? User::object()->user_id : false);
        }
        if (!$user_id) {
            throw new StringsException("Unable to identify a user for the timelock.");
        }
        // Are we reversing?
        $reverse_flag = rand(0, 9);
        $reverse = ($reverse_flag < 5);
        // Build the components.
        $date = App::make(Time::class)->formatServer('ymd');
        $time_as_int = (3600 * App::make(Time::class)->formatServer('G'))
            + (60 * App::make(Time::class)->formatServer('i'))
            + App::make(Time::class)->formatServer('s');
        $time = str_pad(
            (string)$time_as_int,
            5,
            '0',
            STR_PAD_LEFT
        );
        $datetime = "$date$time";
        // Glue together.
        $code = (!$reverse ? $user_id : strrev($user_id)) . rand(10, 99) . (!$reverse ? $datetime : strrev($datetime));
        return $reverse_flag . strrev(static::twoWayEncrypt($code));
    }

    /**
     * Create the code that could be used in the verification process
     * @param string  $code      The verification code.
     * @param integer $valid_for The number of hours the code is valid for.
     * @return string|boolean The username from the code, or false if not valid
     */
    public static function validateUserTimeLock(string $code, int $valid_for): string|bool
    {
        // Have we reversed components?.
        $reverse_flag = substr($code, 0, 1);
        $reverse = ($reverse_flag < 5);
        // Strip the flag and reverse.
        $code = strrev(substr($code, 1));
        // Decrypt and break down in to component parts.
        $code = static::twoWayDecrypt($code);
        $len = strlen($code);
        if ($len <= 13) {
            return false;
        }
        $username = substr($code, 0, $len - 13);
        $datetime = substr($code, -11);
        if ($reverse) {
            $username = strrev($username);
            $datetime = strrev($datetime);
        }
        // Convert the date back in to date/time components.
        $date = substr($datetime, 0, 6);
        $time = (int)substr($datetime, 6);
        $time = str_pad((string)floor($time / 3600), 2, '0', STR_PAD_LEFT)
              . str_pad((string)floor(floor($time % 3600) / 60), 2, '0', STR_PAD_LEFT)
              . str_pad((string)floor($time % 60), 2, '0', STR_PAD_LEFT);
        $datetime = "$date$time";
        // Verify the date.
        if (App::make(Time::class)->adjustFormatServer("-$valid_for hours", 'ymdHis') > $datetime) {
            return false;
        }
        // All good, user_id found so return.
        return $username;
    }
}
