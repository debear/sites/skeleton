<?php

namespace DeBear\Helpers;

use Illuminate\Support\Facades\Config as FrameworkConfig;

class Articles
{
    /**
     * Take the input article and render it for display
     * @param string  $article   The article to parse.
     * @param integer $max_words Maximum number of words in "summary" mode.
     * @return string  The article converted for display
     */
    public static function render(string $article, int $max_words = 100): string
    {
        // Parse for extra HTML items we encode in non-HTML.
        $article = static::parsePseudoMarkup($article);

        // Now shorten, if required.
        $split_var = '::{ARTICLE_SPLIT}::';

        // Loop through looking for line breaks, counting until we reach the limit.
        $word_delim = ['.', ',', ' ', "\n"];
        $num_words = $word_len = 0;
        $open_html = $close_html = false;
        for ($i = 0; $num_words < $max_words && $i < strlen($article); $i++) {
            $char = substr($article, $i, 1);

            // Are we in HTML? If so, break out of it?
            if (in_array($char, $word_delim)) {
                // Increment the word counter.
                if (!$open_html && !$close_html) {
                    if ($word_len) {
                        $num_words++;
                    }
                } elseif ($open_html && $close_html) {
                    $open_html = $close_html = false;
                }

                // Reset the word length.
                $word_len = 0;
            } elseif ($char == '<') {
                // Start a HTML tag.
                $open_html = true;
            } elseif ($char == '>') {
                // Closing a HTML tag.
                $close_html = true;
            } else {
                // Part of a normal word, we can ignore it.
                $word_len++;
            }
        }

        if ($num_words == $max_words) {
            // Now we have the splitting point, update text with SPANs and JavaScript links.
            $article = substr_replace($article, $split_var, $i - 1, 0);

            list($article_main, $article_extra) = explode($split_var, $article);
            $article = "$article_main<span class=\"contracted\">&hellip; (<a class=\"article-toggle\">More</a>)</span>"
                . "<span class=\"expanded hidden\">$article_extra (<a class=\"article-toggle\">Less</a>)</span>";
        }

        // Remove any image-only paragraphs and return.
        return preg_replace('/<p[^>]*>(<img[^>]+>)<\/p>/i', '$1', $article);
    }

    /**
     * Convert pseudo-markup stored in articles into HTML
     * @param string $article The article to parse.
     * @return string The article with pseudo-markup parsed
     */
    protected static function parsePseudoMarkup(string $article): string
    {
        return trim(static::parseLinks(static::parseImages($article)));
    }

    /**
     * Convert images stored in articles into HTML
     * @param string $article The article to parse.
     * @return string The article with images parsed
     */
    public static function parseImages(string $article): string
    {
        // Search for the _{IMG...}_ tags.
        preg_match_all('/_\{IMG[^\}]+\}_/', $article, $matches, PREG_SET_ORDER);
        foreach ($matches as $img) {
            // Get the component parts.
            preg_match_all('/([a-z][a-z\d]+)="([^"]*)"/', $img[0], $inner, PREG_SET_ORDER);
            $fields = array_intersect_key(
                array_combine(array_column($inner, 1), array_column($inner, 2)),
                ['src' => true, 'float' => true, 'caption' => true],
            );

            // Now process the component parts.
            $img_settings = [];
            foreach ($fields as $attrib => $value) {
                switch ($attrib) {
                    case 'caption':
                        // Convert the caption to the alt tag.
                        $attrib = 'alt';
                        break;

                    case 'float':
                        // Convert the float in to a CSS class.
                        $attrib = 'class';
                        $value = "article_img_{$value} float_{$value}";
                        break;

                    case 'src':
                        // Update the image path.
                        $value = HTTP::buildDomain('static') . '/' . FrameworkConfig::get('debear.url.sub')
                            . '/' . FrameworkConfig::get('debear.dirs.images') . "/news/{$value}";
                        break;
                }
                $img_settings[] = "{$attrib}=\"{$value}\"";
            }
            // Form the markup and update the article.
            $img_tag = '<img ' . join(' ', $img_settings) . '>';
            $article = str_replace($img[0], $img_tag, $article);
        }
        return $article;
    }

    /**
     * Convert links stored in articles into HTML
     * @param string $article The article to parse.
     * @return string The article with links parsed
     */
    public static function parseLinks(string $article): string
    {
        // Search for the _{LINK...}_ tags.
        preg_match_all('/_\{LINK[^\}]+\}_/', $article, $matches, PREG_SET_ORDER);
        foreach ($matches as $link) {
            // Get the component parts.
            preg_match_all('/([a-z][a-z\d]+)="([^"]*)"/', $link[0], $inner, PREG_SET_ORDER);
            $tags = array_intersect_key(
                array_combine(array_column($inner, 1), array_column($inner, 2)),
                ['href' => true, 'text' => true, 'popup' => true, 'noref' => true],
            );

            // Now process the component parts.
            $link_settings = [];
            $link_text = null;
            foreach ($tags as $attrib => $value) {
                $skip_attrib = false; // Some of these attributes may be flag-based so not need to be added.
                switch ($attrib) {
                    case 'text':
                        // The text element is not an attribute, but an inner component of the tag.
                        $link_text = $value;
                        $skip_attrib = true;
                        break;

                    case 'popup':
                        // Convert to a target flag.
                        $skip_attrib = ($value == 'false');
                        $attrib = 'target';
                        $value = '_blank';
                        break;

                    case 'noref':
                        // Convert to a rel flag.
                        $skip_attrib = ($value == 'false');
                        $attrib = 'rel';
                        $value = 'noopener noreferrer';
                        break;
                }
                if (!$skip_attrib) {
                    $link_settings[] = "{$attrib}=\"{$value}\"";
                }
            }
            // Form the markup and update the article.
            $link_tag = '<a ' . join(' ', $link_settings) . '>' . ($link_text ?? $tags['href']) . '</a>';
            $article = str_replace($link[0], $link_tag, $article);
        }
        return $article;
    }
}
