<?php

namespace DeBear\Helpers;

use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Models\Skeleton\SessionStats;
use DeBear\Repositories\Time;

class SessionAggregation
{
    /**
     * Get the session's unique identifier
     * @return string|boolean The session's unique identifier
     */
    public static function getID(): string|bool
    {
        $session_cookie = FrameworkConfig::get('debear.sessions.cookies.reference');
        return (session()->has('session_id') ? session('session_id') : (Cookie::getValue($session_cookie) ?? false));
    }

    /**
     * Store the unique identifier for this session
     * @param string $uid The session's unique identifier.
     * @return void
     */
    protected static function setID(string $uid): void
    {
        session(['session_id' => $uid]);
    }

    /**
     * Ensure the session variable is ready
     * @return void
     */
    public static function prepare(): void
    {
        // In all instances we need to ensure we have a unique ID, so if there isn't one, generate it.
        $uid = static::getID();
        if (!$uid) {
            $uid = uniqid(dechex(rand(256, 4095)));
            static::setID($uid);
        }

        // Now proceed with setting up the stats within the session, if enabled.
        if (static::enabled()) {
            $session_cookie = FrameworkConfig::get('debear.sessions.cookies.reference');
            // Load the stats from a reference in a cookie?
            if (!session()->has('session_stats') && Cookie::hasValue($session_cookie)) {
                $uid = Cookie::getValue($session_cookie);
                if (isset($uid)) {
                    $session = FrameworkConfig::get('debear.sessions.handler.enabled')
                        ? SessionStats::find($uid) : null;
                    if (isset($session)) {
                        session(['session_stats' => [
                            'browser' => $session->browser,
                            'browser_version' => $session->browser_version,
                            'os' => $session->os,
                            'last_visit' => $session->last_accessed->timestamp, // Convert the Carbon date.
                            'visits' => $session->visits,
                            'hits' => $session->hits,
                            'logins' => $session->logins,
                            'logouts' => $session->logouts,
                        ]]);
                    }
                }
            }

            // Fallback: A blank template for a first time visiter.
            if (!session()->has('session_stats')) {
                // Build...
                session(['session_stats' => [
                    'browser' => Browser::get()->browser,
                    'browser_version' => Browser::get()->version,
                    'os' => Browser::get()->platform,
                    'last_visit' => 0,
                    'visits' => 0,
                    'hits' => 0,
                    'logins' => 0,
                    'logouts' => 0,
                ]]);
                Cookie::setValue($session_cookie, $uid);
            }
        }
    }

    /**
     * Add to our stats
     * @return void
     */
    public static function process(): void
    {
        if (static::enabled()) {
            // Store the referer.
            HTTP::storeReferer();

            // Process (inside an if for code coverage purposes).
            if (session()->has('session_stats')) {
                // Visits/Hits.
                $stats = session('session_stats');
                $session_cutoff = (FrameworkConfig::get('debear.sessions.cutoffs.session') * 60);
                if ($stats['last_visit'] < (time() - $session_cutoff)) {
                    $stats['visits']++;
                }
                $stats['hits']++;
                $stats['last_visit'] = time();
                // Write-back.
                session(['session_stats' => $stats]);
            }
        }
    }

    /**
     * Persist to the database the updated stats for the session
     * @return void
     */
    public static function store(): void
    {
        if (static::enabled() && static::getID()) {
            // Main stats.
            SessionStats::createOrUpdate(
                [
                    'session_id' => static::getID(),
                    'browser' => session('session_stats.browser'),
                    'browser_version' => session('session_stats.browser_version'),
                    'os' => session('session_stats.os'),
                    'session_started' => Time::object()->formatServer(),
                    'last_accessed' => Time::object()->formatServer(),
                    'visits' => session('session_stats.visits'),
                    'hits' => session('session_stats.hits'),
                    'logins' => session('session_stats.logins'),
                    'logouts' => session('session_stats.logouts'),
                ],
                ['last_accessed', 'visits', 'hits', 'logins', 'logouts']
            );
        }
    }

    /**
     * Flag the session included a user login
     * @return void
     */
    public static function addLogin(): void
    {
        if (static::enabled() && session()->has('session_stats')) {
            $stats = session('session_stats');
            $stats['logins']++;
            session(['session_stats' => $stats]);
        }
    }

    /**
     * Flag the session included a user logout
     * @return void
     */
    public static function addLogout(): void
    {
        if (static::enabled() && session()->has('session_stats')) {
            $stats = session('session_stats');
            $stats['logouts']++;
            session(['session_stats' => $stats]);
        }
    }

    /**
     * Centrally return whether the stat aggregation feature is enabled
     * @return boolean That the session stats should be aggregated and stored
     */
    protected static function enabled(): bool
    {
        return FrameworkConfig::get('debear.sessions.aggregate');
    }
}
