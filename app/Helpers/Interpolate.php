<?php

namespace DeBear\Helpers;

use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Models\Skeleton\User;

class Interpolate
{
    /**
     * Perform standard interpolation patterns on an input string
     * @param string $str  The original string, which includes the tokens to interpolate.
     * @param array  $data Additional data to include in the check. (Optional).
     * @return string The resulting converted string
     */
    public static function string(string $str, array $data = []): string
    {
        // User info.
        $str = static::processUser($str);

        // Config option.
        $str = static::processConfig($str);

        // Session value.
        $str = static::processSession($str);

        // Links.
        $str = static::processLink($str);

        // Emails.
        $str = static::processEmail($str);

        // Custom data?
        $str = static::processData($str, $data);

        // If logic (bit use-case specific...?).
        // Last to ensure any other processing has already occurred...
        $str = static::processIf($str, $data);

        return $str;
    }

    /**
     * Perform standard interpolation patterns on an input array
     * @param array $arr  The original array, which includes the tokens to interpolate.
     * @param array $data Additional data to include in the check. (Optional).
     * @return array The resulting converted array
     */
    public static function array(array $arr, array $data = []): array
    {
        // Loop through each element and convert the strings, or re-process if not.
        foreach ($arr as &$value) {
            if (is_array($value)) {
                // Recusrive calls.
                $value = static::array($value, $data);
            } elseif (is_string($value)) {
                // Convert a string using our standard method.
                $value = static::string($value, $data);
                // Otherwise, leave alone anything else.
            }
        }
        return $arr;
    }

    /**
     * Worker components
     */

    /**
     * Perform user based interpolation steps
     * @param string $str The original string, which includes the tokens to interpolate.
     * @return string The resulting converted user details
     */
    protected static function processUser(string $str): string
    {
        preg_match_all('/{user\|(.*?)}/', $str, $matches, PREG_SET_ORDER);
        $proc = [];
        foreach ($matches as $match) {
            // Skip if we've already found and processed this token.
            $option = false; // PHPMD satisfaction.
            list($token, $option) = $match;
            if (isset($proc[$token])) {
                continue;
            }
            $proc[$token] = true;

            // Get and replace.
            $str = str_replace($token, User::object()->$option, $str);
        }
        return $str;
    }

    /**
     * Perform config based interpolation steps
     * @param string $str The original string, which includes the tokens to interpolate.
     * @return string The resulting converted configuration option
     */
    protected static function processConfig(string $str): string
    {
        preg_match_all('/{config\|(.*?)}/', $str, $matches, PREG_SET_ORDER);
        $proc = [];
        foreach ($matches as $match) {
            // Skip if we've already found and processed this token.
            list($token, $option) = $match;
            if (isset($proc[$token])) {
                continue;
            }
            $proc[$token] = true;

            // Get and replace.
            $str = str_replace($token, FrameworkConfig::get($option), $str);
        }
        return $str;
    }

    /**
     * Perform session value based interpolation steps
     * @param string $str The original string, which includes the tokens to interpolate.
     * @return string The resulting converted session value
     */
    protected static function processSession(string $str): string
    {
        preg_match_all('/{session\|(.*?)}/', $str, $matches, PREG_SET_ORDER);
        $proc = [];
        foreach ($matches as $match) {
            // Skip if we've already found and processed this token.
            list($token, $option) = $match;
            if (isset($proc[$token])) {
                continue;
            }
            $proc[$token] = true;

            // Get and replace.
            $str = str_replace($token, (string)session($option), $str);
        }
        return $str;
    }

    /**
     * Perform URL link based interpolation steps
     * @param string $str The original string, which includes the tokens to interpolate.
     * @return string The resulting converted link
     */
    protected static function processLink(string $str): string
    {
        preg_match_all('/{link(-js)?\|(.*?)}/', $str, $matches, PREG_SET_ORDER);
        $proc = [];
        foreach ($matches as $match) {
            // Skip if we've already found and processed this token.
            list($token, $js, $rule) = $match;
            if (isset($proc[$token])) {
                continue;
            }
            $proc[$token] = true;

            // Parse the details.
            if (strpos($rule, '|') !== false) {
                list($link, $text) = explode('|', $rule);
            } else {
                $link = $text = $rule;
            }

            // Render the link.
            $text = "<a " . (!$js ? "href" : "class") . "=\"$link\">$text</a>";

            // And process.
            $str = str_replace($token, $text, $str);
        }
        return $str;
    }

    /**
     * Perform email address based interpolation steps
     * @param string $str The original string, which includes the tokens to interpolate.
     * @return string The resulting converted email
     */
    protected static function processEmail(string $str): string
    {
        preg_match_all('/{email(-raw)?\|(.*?)}/', $str, $matches, PREG_SET_ORDER);
        $proc = [];
        foreach ($matches as $match) {
            // Skip if we've already found and processed this token.
            list($token, $raw, $rule) = $match;
            if (isset($proc[$token])) {
                continue;
            }
            $proc[$token] = true;

            // Get the replacement details.
            if (strpos($rule, '|') !== false) {
                list($address, $text) = explode('|', $rule);
                $raw = false; // Doesn't make sense to have a raw email with replacement text and no link!
            } else {
                $address = $text = $rule;
            }

            // Get the appropriate text to include.
            if ($raw) {
                $text = HTML::buildEmailRaw($address);
            } else {
                $text = HTML::buildEmailMarkup($address, $text);
            }

            // And process.
            $str = str_replace($token, $text, $str);
        }
        return $str;
    }

    /**
     * Perform custom data based interpolation steps
     * @param string $str  The original string, which includes the tokens to interpolate.
     * @param array  $data Additional data to include in the check. (Optional).
     * @return string The resulting converted bespoke data
     */
    protected static function processData(string $str, array $data = []): string
    {
        preg_match_all('/{data\|(.*?)}/', $str, $matches, PREG_SET_ORDER);
        $proc = [];
        foreach ($matches as $match) {
            // Skip if we've already found and processed this token.
            list($token, $index) = $match;
            if (isset($proc[$token])) {
                continue;
            }
            $proc[$token] = true;

            // Get and replace.
            $str = str_replace(
                $token,
                $data[$index] ?? "** Missing Interpolation Data '$index'**",
                $str
            );
        }
        return $str;
    }

    /**
     * Perform if-logic based interpolation steps
     * @param string $str  The original string, which includes the tokens to interpolate.
     * @param array  $data Additional data to include in the check. (Optional).
     * @return string The resulting converted generic if test
     */
    protected static function processIf(string $str, array $data = []): string
    {
        preg_match_all('/{if\|(.*?)}/', $str, $matches, PREG_SET_ORDER);
        foreach ($matches as $match) {
            // Break down the rule into our component parts, ensuring we add any missing components.
            list($token, $rule) = $match;
            list($check, $if_true, $if_false) = array_pad(explode('|', $rule), 3, ''); // 3 => check|if-true|if-false.

            // Process.
            $split = explode(':', $check);
            $res = false;
            switch ($split[0]) {
                // An option from our data array?
                case 'data':
                    if (sizeof($split) > 1) {
                        $res = isset($data[$split[1]]) && $data[$split[1]];
                    }
                    break;

                // Config option?
                case 'config':
                    $res = (bool) FrameworkConfig::get($split[1]);
                    break;
            }

            // What are we doing?
            $str = str_replace($token, $res ? $if_true : $if_false, $str);
        }
        return $str;
    }
}
