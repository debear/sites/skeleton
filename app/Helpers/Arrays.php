<?php

namespace DeBear\Helpers;

use DeBear\Exceptions\ArraysException;

class Arrays
{
    /**
     * A wrapper to in_array that validates the input arguments
     * @param mixed   $needle   The thing we are looking for in the array.
     * @param mixed   $haystack The array we are testing against.
     * @param boolean $strict   That the comparison should be type-sensitive. (Optional; Default: false).
     * @return boolean That the array contains the searched needle
     * @throws ArraysException If unknown $type passed in.
     */
    public static function contains(mixed $needle, mixed $haystack, bool $strict = false): bool
    {
        // Is the source array even an array?
        if (!isset($haystack) || !is_array($haystack)) {
            // No, so silently indicate we don't.
            return false;
        } elseif (!is_scalar($needle)) {
            // We can only deal with scalar needles.
            throw new ArraysException(
                "The needle for ::contains() must be a scalar. '" . gettype($needle) . "'' passed."
            );
        }
        // Run the test.
        return in_array($needle, $haystack, $strict);
    }

    /**
     * Improve PHPs array_merge for string keys by appending rather than replacing
     * @param array $a   Master array.
     * @param array $b   Slave array.
     * @param array $opt Merge rules.
     * @return array The merged array
     */
    public static function merge(array $a, array $b, array $opt = []): array
    {
        // If we have a pair of numerically keyed arrays, use the native array_merge.
        if (!static::isAssociative($a) && !static::isAssociative($b)) {
            return array_merge($a, $b);
        }

        // Otherwise, try and merge manually.
        $keys_a = array_keys($a);
        $keys_b = array_keys($b);

        // Determine those keys in $b that aren't in $a.
        foreach (array_diff($keys_b, $keys_a) as $k) {
            $a[$k] = $b[$k];
        }

        // Then those that are in both and need careful merging.
        foreach (array_intersect($keys_a, $keys_b) as $k) {
            $array_a = is_array($a[$k]);
            $array_b = is_array($b[$k]);
            if ($array_a && $array_b) {
                if (!static::isAssociative($a[$k]) && !static::isAssociative($b[$k])) {
                    // If both sequentially indexed arrays, append to each other.
                    $a[$k] = array_merge($a[$k], $b[$k]);
                } else {
                    // Otherwise, recurse.
                    $a[$k] = static::merge($a[$k], $b[$k], $opt);
                }
            } elseif ($array_a && !$array_b) {
                // Add the scalar element in $b on to the end of $a.
                $a[$k][] = $b[$k];
            } elseif (!$array_a && $array_b) {
                // Add the scalar element in $a to the start of $b.
                $a[$k] = array_merge([$a[$k]], $b[$k]);
            } elseif (isset($opt['merge_scalar']) && $opt['merge_scalar']) {
                // Convert the element to an array, with $a as the initial value.
                $a[$k] = [$a[$k], $b[$k]];
            } else {
                // Hard-overwrite the scalar in $a with the value in $b.
                $a[$k] = $b[$k];
            }
        }

        return $a;
    }

    /**
     * Is an array associative or sequentially indexed?
     * @param array $arr The array to be tested.
     * @return boolean Whether $arr is an associative array or not
     */
    public static function isAssociative(array $arr): bool
    {
        return ( array_keys($arr) !== range(0, count($arr) - 1) );
    }

    /**
     * Perform an array_intersect_key, but preserve the key order
     * @param array $arrA The first array.
     * @param array $arrB The second array.
     * @return array The ordered intersected array
     */
    public static function orderedIntersectKey(array $arrA, array $arrB): array
    {
        return array_intersect_key(array_replace($arrB, $arrA), $arrB);
    }
}
