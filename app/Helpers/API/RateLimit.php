<?php

namespace DeBear\Helpers\API;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Support\Facades\RateLimiter;

class RateLimit
{
    /**
     * Configure the global rate limiters across all applications.
     * @return void
     */
    public static function define()
    {
        foreach (FrameworkConfig::get('debear.api.thresholds') as $key => $limit_per_minute) {
            RateLimiter::for($key, function (Request $request) use ($limit_per_minute) {
                return Limit::perMinute($limit_per_minute);
            });
        }
    }
}
