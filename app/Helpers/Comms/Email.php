<?php

namespace DeBear\Helpers\Comms;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Exceptions\Comms\EmailException;
use DeBear\Helpers\Arrays;
use DeBear\Helpers\Blade;
use DeBear\Helpers\HTTP;
use DeBear\Helpers\HTML;
use DeBear\Helpers\Interpolate;
use DeBear\Helpers\Strings;
use DeBear\Helpers\Comms\Email\Links;
use DeBear\Helpers\Comms\Email\Send;
use DeBear\Models\Skeleton\CommsEmail;
use DeBear\Models\Skeleton\CommsEmailTemplate;
use DeBear\Models\Skeleton\User;
use DeBear\Repositories\Time;

class Email
{
    /**
     * Prepare an email to be sent to a user
     * @param string              $name The code of the database template to send.
     * @param User|string|integer $user The user to contact, which could be a db object, address string or a numeric ID.
     * @param array               $args Additional customisations. (Optional).
     * @return CommsEmail The email instance that was sent
     * @throws EmailException When the requested template could not be found.
     */
    public static function send(string $name, User|string|int $user, array $args = []): CommsEmail
    {
        // Get the email.
        $template = CommsEmailTemplate::find($name);
        // Error if one not found.
        if (!isset($template)) {
            throw new EmailException("Unable to find email template '$name'.");
        }
        $args['app'] = $template->app;

        // Ensure some defaults exist.
        if (!isset($args['data']) || !is_array($args['data'])) {
            $args['data'] = [];
        }
        if (!isset($args['reason'])) {
            $args['reason'] = 'unknown';
        }
        if (!isset($args['type'])) {
            $args['type'] = 'text';
        }
        if (!isset($args['send_time'])) {
            $args['send_time'] = '+30 minutes';
        }

        if (is_numeric($user)) {
            // Get the user, if only passed an ID.
            $user = User::find($user);
        } elseif (is_string($user)) {
            // If we don't have an object yet, we've been passed an email,
            // so need to build an in-memory only database object.
            $user = User::build(['email' => $user]);
        }

        // Merge user data into the data object, as well as some standard values.
        $args['data'] = Arrays::merge($args['data'], $user->toArray());
        static::addDefaultData($args['data']);

        // Any callback to run on the data?
        if (isset($args['callback']) && is_callable($args['callback'])) {
            call_user_func_array($args['callback'], [&$args]); // Where $args is passed-by-reference.
        }

        // Prepare the email.
        $email = static::build($template, $user, $args);

        // Send!
        static::process($email);

        // And return the object, should it need to be interrogated.
        return $email;
    }

    /**
     * Add standard values to the data array for interpolation
     * @param array $data The array of data to be passed to the interpolator. (Pass-by-Reference).
     * @return void
     */
    protected static function addDefaultData(array &$data): void
    {
        $data = Arrays::merge($data, [
            'date_full' => App::make(Time::class)->format('jS F Y'),
            'date_full_short' => App::make(Time::class)->format('jS M'),
            'date_dow' => App::make(Time::class)->format('l'),
            'date_dow_short' => App::make(Time::class)->format('D'),
            'date_day' => App::make(Time::class)->format('jS'),
            'date_day_num' => App::make(Time::class)->format('j'),
            'date_month' => App::make(Time::class)->format('F'),
            'date_month_short' => App::make(Time::class)->format('M'),
            'date_year' => App::make(Time::class)->format('Y'),
        ]);
    }

    /**
     * Constrct an email ready for storing in the database and sending
     * @param CommsEmailTemplate $template The database template we are sending.
     * @param User               $user     The user to receive the email.
     * @param array              $args     Custom processing arguments.
     * @return CommsEmail An in-memory (NOT COMMITTED!) object representing the email to send
     */
    protected static function build(CommsEmailTemplate $template, User $user, array $args): CommsEmail
    {
        // Create our base email instance, adding defaults where we are missing any.
        $email = [
            'app'           => $args['app'],
            'user_id'       => $user->id ?: null,
            'email_reason'  => $args['reason'],
            'email_from'    => $template->from
                ? Interpolate::string($template->from, $args['data'])
                : HTML::buildEmailRaw('noreply'),
            'email_to'      => $user->email
                ? Interpolate::string('"' . $user->name . '" <' . $user->email . '>')
                : HTML::buildEmailRaw('noreply'),
            'email_subject' => $template->subject
                ? Interpolate::string($template->subject, $args['data'])
                : 'A Message from ' . FrameworkConfig::get('debear.names.site'),
            // The final resulting message.
            'email_body'    => '',
            // Non-DB components.
            'type'          => $args['type'],
            'summary'       => $template->summary ? Interpolate::string($template->summary, $args['data']) : '',
            'message'       => $template->message ? Interpolate::string($template->message, $args['data']) : '',
            'clause'        => $template->clause ? Interpolate::string($template->clause, $args['data']) : '',
            'opt_out'       => isset($args['opt_out']) && $args['opt_out'],
            'unverified'    => (!isset($args['send_unverified']) || !$args['send_unverified']) && $user->isUnverified(),
            'send_time'     => $args['send_time'],
        ];
        $email = CommsEmail::build($email);

        // Create the final bits we'll need to send the email.
        $email->email_code = static::generateCode();
        list($email->code_checksum1, $email->code_checksum2) = static::calculateChecksum($email->email_code);
        $email->content_boundary = Strings::randomAlphaNum(16);

        // Render the email.
        $template_args = [ 'user' => $user, 'email' => $email, 'template' => $template, 'data' => $args['data'] ];
        $msg_html = Blade::render("email/{$template->wrapper}", $template_args);

        // Convert to text only.
        preg_match('/<!-- Begin Text Content -->(.*?)<!-- End Text Content -->/s', $msg_html, $content_match);
        $msg_text = $content_match[1];
        preg_match('/<!-- Begin Standard Footer -->(.*?)<!-- End Standard Footer -->/s', $msg_html, $content_match);
        preg_match_all('/<div.*?>(.*?)<\/div>/s', $content_match[1], $footer);
        $msg_text .= join($footer[1]);
        $msg_text = trim(preg_replace('/\n[ \t]+/', "\n", $msg_text));

        // Get/Update links.
        $links_raw = Links::processTextOnly($msg_text);
        if ($email->type == 'html') {
            $email->links = Links::processHTML($email, $links_raw, $msg_html);
        }

        // Perform some markup tidying.
        $msg_text = preg_replace('/\s*<style.*?<\/style>/is', '', $msg_text);
        $msg_text = preg_replace('/<br\s?\/?>\n?/is', "\n", $msg_text);
        $msg_text = preg_replace('/<p>(.*?)<\/p>/is', "\$1\n\n", $msg_text);
        $msg_text = preg_replace('/<[^>]+>/', '', $msg_text);
        $msg_text = preg_replace('/[\n\r]{3,}/m', "\n\n", $msg_text);
        $msg_text = html_entity_decode(trim($msg_text), ENT_QUOTES, 'UTF-8');

        // And now generate the email parts.
        $charset = 'charset="UTF-8"';
        if ($email->type == 'html') {
            $email->email_body .= "--{$email->content_boundary}\nContent-Type: text/plain; $charset\n\n";
        }
        $email->email_body .= $msg_text;
        if ($email->type == 'html') {
            $email->email_body .= "\n\n--{$email->content_boundary}\nContent-Type: text/html; $charset\n\n$msg_html";
        }

        // Return the object we've built.
        return $email;
    }

    /**
     * The sending component of the class
     * @param CommsEmail $email The built-up email object.
     * @return string Status of the attempted send
     */
    protected static function process(CommsEmail $email): string
    {
        // Ensure the subject does not contain any HTML codes.
        $email->email_subject = html_entity_decode($email->email_subject, ENT_QUOTES, 'UTF-8');

        // Word wrap plain text emails.
        if ($email->type == 'text') {
            $email->email_body = wordwrap($email->email_body, 70);
        }

        // If we're not in live mode, adjust the email to go to a test account.
        if (!HTTP::isLive()) {
            $email->email_to = HTML::buildEmailRaw('support');
        }

        // Determine what the status of the email is.
        if ($email->opt_out) {
            $email->email_status = 'opt_out';
        } elseif (!FrameworkConfig::get('debear.email.enabled') || $email->unverified) {
            $email->email_status = 'blocked';
        } elseif ($email->send_time == 'now') {
            $email->email_status = (Send::now($email) ? 'sent' : 'failed');
        } else {
            $email->email_status = 'queued';
        }

        // Some timings.
        $email->email_queued = App::make(Time::class)->getServerNowFmt();
        $email->email_send = App::make(Time::class)->adjustFormatServer($email->send_time);
        $email->email_sent = ($email->email_status == 'sent' ? $email->email_queued : null);

        // And now store in the database to be sent, returning the status.
        $email->encodeSave();
        return $email->email_status;
    }

    /**
     * Generate a unique identifier for an email
     * @param integer $len Length of code to be built. (Optional).
     * @return string A unique code for the email
     */
    protected static function generateCode(int $len = 16): string
    {
        $unique = false;
        while (!$unique) {
            $code = Strings::randomAlphaNum($len);
            $unique = CommsEmail::where('email_code', $code)->doesntExist();
        }
        return $code;
    }

    /**
     * Calculate the checksums for an email
     * @param string $code The string we want to build a checksum of.
     * @return array The components of our checksum
     */
    public static function calculateChecksum(string $code): array
    {
        return [
            hash('crc32', $code),
            substr(hash('md5', $code), 0, 8),
        ];
    }

    /**
     * Validate the email code we received is valid
     * @param string $i The main code.
     * @param string $t The first checksum.
     * @param string $r The second checksum.
     * @return boolean
     */
    public static function validateCode(string $i, string $t, string $r): bool
    {
        // Syntactic check.
        if (
            !preg_match('/^[a-z0-9]{16}$/i', $i)
            || !preg_match('/^[a-z0-9]{8}$/i', $t)
            || !preg_match('/^[a-z0-9]{8}$/i', $r)
        ) {
            return false;
        }

        // Now check the combinations.
        list($_t, $_r) = static::calculateChecksum($i);
        return ($t == $_t && $r == $_r);
    }
}
