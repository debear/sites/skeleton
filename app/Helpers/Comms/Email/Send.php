<?php

namespace DeBear\Helpers\Comms\Email;

use DeBear\Helpers\HTML;
use DeBear\Helpers\HTTP;
use DeBear\Models\Skeleton\CommsEmail;

class Send
{
    /**
     * Send the email immediatly via PHP, rather than the cron'd script
     * @param CommsEmail $email The email object we want to send.
     * @return boolean The outcome of the mail() function
     */
    public static function now(CommsEmail $email): bool
    {
        // What format are we sending the email in?
        $is_html = 'multipart/alternative; boundary="' . $email->content_boundary . '"';
        $is_plain = 'text/plain';
        $content_type = ($email->type == 'html' ? $is_html : $is_plain);

        // Build our headers.
        $headers = 'MIME-Version: 1.0' . "\r\n"
            . 'Content-type: ' . $content_type . '; charset=UTF-8' . "\r\n"
            . 'Content-Transfer-Encoding: 7bit' . "\r\n"
            . 'X-Mailer: DeBear Mailer/1.0' . "\r\n"
            . 'From: ' . $email->email_from;
        $from = '-f' . HTML::buildEmailRaw('noreply');

        // Send, return boolean indicating success.
        return !HTTP::isTest() && mail($email->email_to, $email->email_subject, $email->email_body, $headers, $from);
    }
}
