<?php

namespace DeBear\Helpers\Comms\Email;

use DeBear\Helpers\HTTP;
use DeBear\Helpers\Strings;
use DeBear\Helpers\Comms\Email;
use DeBear\Models\Skeleton\CommsEmail;
use DeBear\Models\Skeleton\CommsEmailLink;

class Links
{
    /**
     * Prepare the links in the text-only part of an email
     * @param string $msg_text The text-only component of the email. (Pass-by-Reference).
     * @return array The unique list of links in the email
     */
    public static function processTextOnly(string &$msg_text): array
    {
        preg_match_all('/<a.*?href="(.*?)".*?>(.*?)<\/a>/', $msg_text, $link_match, PREG_SET_ORDER);
        $i = 0;
        $links_raw = $link_matches = [];
        foreach ($link_match as $link) {
            // Skip email links.
            if (substr($link[1], 0, 7) == 'mailto:') {
                continue;
            }

            // Preserve all links for email linking.
            $links_raw[$link[1]] = 1;

            // Skip if the link is the text.
            if ($link[1] == $link[2]) {
                continue;
            }
            if (!$i) {
                $msg_text .= "\n";
            }

            // Have we encountered this link before?
            if (isset($link_matches[$link[1]])) {
                $link_num = $link_matches[$link[1]];
            } else {
                $link_num = ++$i;
                $msg_text .= "\n[$i] {$link[1]}";
                $link_matches[$link[1]] = $i;
            }
            $msg_text = str_replace($link[0], "{$link[2]} [$link_num]", $msg_text);
        }

        return array_keys($links_raw);
    }

    /**
     * Prepare the links in the HTML part of an email
     * @param CommsEmail $email     The email object we are processing links for.
     * @param array      $links_raw The raw list of links retrieved in the text-only processing.
     * @param string     $msg_html  The HTML component of the email. (Pass-by-Reference).
     * @return array The unique list of links, processed into components
     */
    public static function processHTML(CommsEmail $email, array $links_raw, string &$msg_html): array
    {
        $links = $existing = [];
        foreach ($links_raw as $link) {
            // Skip email links - not actually required, because they are skipped for us in processTextOnly().
            /* * /
            if (substr($link, 0, 7) == 'mailto:') {
                continue;
            }
            /* */

            // If there's no trailing slash after the domain, add one to satisfy the regex.
            $tmp = new CommsEmailLink();
            $link_orig = $link;
            if (preg_match('/^.*?:\/\/[^\/:]*?(?:\:\d+)?(?:\?.*)?$/', $link)) {
                $link = preg_replace('/^(.*?:\/\/[^\/:]*?(?:\:\d+)?)((?:\?.*)?)$/', '$1/$2', $link);
            }

            // Split in to component parts.
            preg_match('/^(.*?):\/\/([^\/:]*?)(:\d+)?(\/.*?)(\?.*?)?$/', $link, $parts);
            // Tweak port and query to strip leading char.
            if (isset($parts[3]) && is_string($parts[3]) && $parts[3]) {
                $parts[3] = substr($parts[3], 1);
            }
            if (isset($parts[5]) && is_string($parts[5]) && $parts[5]) {
                $parts[5] = substr($parts[5], 1);
            } else {
                $parts[5] = ''; // Satisfy the next line...
            }

            // Convert into named link args.
            foreach ($parts as &$part) {
                if ($part === '') {
                    $part = null;
                }
            }
            list(
                $tmp->link_full,
                $tmp->link_protocol,
                $tmp->link_domain,
                $tmp->link_port,
                $tmp->link_url,
                $tmp->link_query
            ) = $parts;

            // Determine the converted email link.
            $tmp->link_code = static::generateCode($email->email_code, $existing);
            list($tmp->code_checksum1, $tmp->code_checksum2) = Email::calculateChecksum($tmp->link_code);
            $tmp->url_cnv = 'https:' . HTTP::buildDomain() . '/eml-'
                . $tmp->link_code . '-' . $tmp->code_checksum1 . '-' . $tmp->code_checksum2;

            // Update the email body.
            $msg_html = preg_replace(
                '/(<a[^>]*href=")' . str_replace(
                    '/',
                    '\/',
                    $link_orig
                ) . '("[^>]*>)/',
                '$1' . $tmp->url_cnv . '$2',
                $msg_html
            );

            // Add to our list to be returned.
            $links[] = $tmp;
        }

        // Return our processed version for future processing.
        return $links;
    }

    /**
     * Generate a unique identifier for an email link
     * @param string  $email_code The base code for our link, which is formed from our email's code.
     * @param array   $existing   A list of existing codes we have built so we can check for uniqueness
     * among the codes we are creating and yet to commit to the database. (Pass-by-Reference).
     * @param integer $len        Length of code to be built. (Optional).
     * @return string A unique code for the email
     */
    protected static function generateCode(string $email_code, array &$existing, int $len = 3): string
    {
        $unique = false;
        $email_code = substr($email_code, 0, 0 - $len);
        while (!$unique) {
            $code = $email_code . Strings::randomAlphaNum($len);
            if (!isset($existing[$code])) {
                $unique = CommsEmailLink::where('link_code', $code)->doesntExist();
            }
        }
        $existing[$code] = true; // Add to our additional list (for non-DB lookup) and return.
        return $code;
    }
}
