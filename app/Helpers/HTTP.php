<?php

namespace DeBear\Helpers;

use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Http\Response;
use Illuminate\Http\RedirectResponse;
use DeBear\Models\Skeleton\SiteReferer;

class HTTP
{
    /**
     * Whether we are running in a dev environment or not
     * @return boolean
     */
    public static function isDev(): bool
    {
        return ((string)config('app.env')) == 'local'; // Must be pre-facade compatible.
    }

    /**
     * Whether we are running in the live environment or not
     * @return boolean
     */
    public static function isLive(): bool
    {
        return ((string)config('app.env')) == 'production'; // Must be pre-facade compatible.
    }

    /**
     * Whether we are running in the unit test environment or not
     * @return boolean
     */
    public static function isTest(): bool
    {
        return ((string)config('app.env')) == 'testing'; // Must be pre-facade compatible.
    }

    /**
     * Generate the domain name for links / resources
     * @param string|boolean $domain The domain to build, or false to use the current subdomain.
     * @return string The link, from protocol to port (excl. trailing slash)
     */
    public static function buildDomain(string|bool $domain = false): string
    {
        if (!$domain) {
            $domain = FrameworkConfig::get('debear.url.sub');
        }
        return '//' . FrameworkConfig::get('debear.url.subdomains.' . $domain)
            . FrameworkConfig::get('debear.url.port');
    }

    /**
     * Generate the current page's base URL for the section being visited
     * @return string The base URL for the current page's section
     */
    public static function buildSectionURL(): string
    {
        return static::buildDomain() . '/' . (FrameworkConfig::get('debear.section.endpoint')
            ? FrameworkConfig::get('debear.section.endpoint') . '/' : '');
    }

    /**
     * Generate URLs for the static sub-domain from on-disk names
     * @param string       $type  The type of file we're converting, which is the on-disk folder name.
     * @param string|array $files The file / list of file(s) to be converted.
     * @return string|array The input file(s) converted to a static domain URL
     */
    public static function buildStaticURLs(string $type, string|array $files): string|array
    {
        // As we accept multiple types, standardise.
        $was_string = is_string($files);
        if ($was_string) {
            $files = [$files];
        }

        // Then perform the conversion.
        $f = '';
        $domain_static = static::buildDomain('static');
        foreach ($files as &$f) {
            // Determine the appropriate structure.
            if (substr($f, 0, 5) == 'skel/') {
                $d = 'skel';
                $f = substr($f, 5);
            } else {
                $d = FrameworkConfig::get('debear.url.sub');
            }

            // Then build the link.
            $f = "$domain_static/$d/$type/$f";
        }

        // And return.
        return ($was_string ? $f : $files);
    }

    /**
     * Generate URLs for the CDN sub-domain from on-disk names
     * @param string|array $files The file / list of file(s) to be converted.
     * @param array        $args  URL query args to append. (Optional).
     * @return string|array The input file(s) converted to a CDN domain URL
     */
    public static function buildCDNURLs(string|array $files, array $args = []): string|array
    {
        // As we accept multiple types, standardise.
        $was_string = is_string($files);
        if ($was_string) {
            $files = [$files];
        }

        // Then perform the conversion.
        $domain_static = static::buildDomain('cdn');
        foreach (array_keys($files) as $k) {
            // Obfuscate and add the domain.
            $files[$k] = $domain_static . static::obfuscateCDNURL($files[$k], $args);
        }

        // And return.
        return ($was_string ? $files[0] : $files);
    }

    /**
     * Obfuscate a CDN URL
     * @param string $url_plain The URL to obfuscate.
     * @param array  $args      URL query args to append. (Optional).
     * @return string The obfuscated
     */
    protected static function obfuscateCDNURL(string $url_plain, array $args = []): string
    {
        // Add any arguments?
        if (count($args)) {
            $url_plain = 'i=' . $url_plain . '&' . http_build_query($args);
        }
        // Process.
        return '/' . strrev(str_replace('+', '_', str_replace('/', '-', rtrim(
            base64_encode(
                gzcompress(substr($url_plain, -12) . $url_plain, 9)
            ),
            '='
        ))));
    }

    /**
     * Return the list of off-site (sub-)domains we make use of to include in HTML resource hints
     * @return array The list of (sub-)domains
     */
    public static function resourceHints(): array
    {
        // Static and CDN.
        return [
            static::buildDomain('static'),
            static::buildDomain('cdn'),
        ];
    }

    /**
     * Return the response object for an appropriate status code
     * @param integer $status  The status code to return.
     * @param mixed   $content Content to send in the response.
     * @return Response Response object with the code and content set
     */
    public static function setStatus(int $status, mixed $content = ''): Response
    {
        $is_array = is_array($content);
        if ($is_array) {
            $content = json_encode($content, JSON_NUMERIC_CHECK);
        }
        $resp = response($content, $status);
        if ($is_array) {
            $resp = $resp->header('Content-Type', 'application/json');
        }
        return $resp;
    }

    /**
     * Wrapper to send the HTTP status code when content was created
     * @param mixed $ret Content to be returned in the response.
     * @return Response Response object with the appropriate HTTP status code
     */
    public static function sendCreated(mixed $ret): Response
    {
        return static::setStatus(201, $ret);
    }

    /**
     * Wrapper to send the HTTP status code when no content is to be returned
     * @return Response Response object with the appropriate HTTP status code
     */
    public static function sendNoContent(): Response
    {
        return static::setStatus(204);
    }

    /**
     * Wrapper to send the HTTP status code when a bad request was received
     * @param mixed $ret Content to be returned in the response (optional).
     * @return Response Response object with the appropriate HTTP status code
     */
    public static function sendBadRequest(mixed $ret = null): Response
    {
        return static::setStatus(400, $ret ?? view('errors.400'));
    }

    /**
     * Wrapper to send the HTTP status code when the user has not logged in yet
     * @param mixed $ret Content to be returned in the response (optional).
     * @return Response Response object with the appropriate HTTP status code
     */
    public static function sendUnauthorised(mixed $ret = null): Response
    {
        return static::setStatus(401, $ret ?? view('errors.sc1'));
    }

    /**
     * Wrapper to send the HTTP status code when the logged in user is not allowed to make the request
     * @param mixed $ret Content to be returned in the response (optional).
     * @return Response Response object with the appropriate HTTP status code
     */
    public static function sendForbidden(mixed $ret = null): Response
    {
        return static::setStatus(403, $ret ?? view('errors.sc2'));
    }

    /**
     * Wrapper to send the HTTP status code when the requested resource was not found
     * @param mixed $ret Content to be returned in the response (optional).
     * @return Response Response object with the appropriate HTTP status code
     */
    public static function sendNotFound(mixed $ret = null): Response
    {
        return static::setStatus(404, $ret ?? view('errors.404'));
    }

    /**
     * Wrapper to send the HTTP status code when the wrong HTTP method was used
     * @param mixed $ret Content to be returned in the response (optional).
     * @return Response Response object with the appropriate HTTP status code
     */
    public static function sendMethodNotAllowed(mixed $ret = null): Response
    {
        return static::setStatus(405, $ret ?? view('errors.405'));
    }

    /**
     * Redirect users to an alternative page
     * @param string  $url    The URL to be sent to.
     * @param integer $status The HTTP Status Code we'll send to explain the move.
     * @return RedirectResponse The redirect response to pass onwards
     */
    public static function redirectPage(string $url, int $status = 307): RedirectResponse
    {
        // Quick check - set the correct redirect status if we're not performing a HEAD or GET.
        $ignore_method = [
            'HEAD' => true,
            'GET' => true,
        ];
        $code_map = [
            // 301 as GET/HEAD should be a 308.
            301 => 308,
            // 302 as GET/HEAD should be a 307.
            302 => 307,
        ];
        if (isset($code_map[$status]) && !isset($ignore_method[Request::server('REQUEST_METHOD')])) {
            $status = $code_map[$status];
        }
        // Perform.
        return redirect($url, $status);
    }

    /**
     * Add security related headers to a response
     * @param Response $response Response object.
     * @param array    $custom   Custom headers to overlay on top of the defaults.
     * @param array    $opt      Config options.
     * @return void
     */
    public static function securityHeaders(Response $response, array $custom = [], array $opt = []): void
    {
        // Are we overlaying or replacing the header list?
        $overlay = !isset($opt['replace_default']) || !$opt['replace_default'];
        if (!$overlay && sizeof($custom)) {
            $headers = $custom;
        } else {
            // Build our default list.
            $headers = FrameworkConfig::get('debear.headers.standard');
            /* CSP */
            $csp = FrameworkConfig::get('debear.headers.csp');
            // If not enabled, don't do anything.
            // Conversely, if we're just reporting but have no report_uri configured, we can't continue.
            if (
                (isset($csp['enabled']) && $csp['enabled'])
                && ((!isset($csp['report_only']) || !$csp['report_only'])
                    || (isset($csp['report_uri']) && $csp['report_uri']))
                && !(bool)Request::input('debug_session')
            ) {
                // Get the header name.
                $csp_header = 'Content-Security-Policy';
                // Report only mode?
                if (isset($csp['report_only']) && $csp['report_only']) {
                    $csp_header .= '-Report-Only';
                    // We'll also need to remove a policy that doesn't apply.
                    if (isset($csp['policies']['upgrade-insecure-requests'])) {
                        unset($csp['policies']['upgrade-insecure-requests']);
                    }
                }
                // Build the components.
                $policies = [];
                foreach ($csp['policies'] as $k => $p) {
                    if ((bool)$p) {
                        $policies[] = $k . (is_array($p) ? ' ' . join(' ', $p) : '');
                    }
                }
                // Report URI?
                if (isset($csp['report_uri']) && is_string($csp['report_uri']) && $csp['report_uri']) {
                    $policies[] = 'report-uri ' . $csp['report_uri'];
                }
                // Output, replacing some placeholders.
                $csp_policy = str_replace(
                    '{{NONCE}}',
                    FrameworkConfig::get('debear.headers.csp.nonce'),
                    str_replace(
                        '{{DOMAIN_STATIC}}',
                        'https:' . static::buildDomain('static'),
                        str_replace(
                            '{{DOMAIN_CDN}}',
                            'https:' . static::buildDomain('cdn'),
                            join('; ', $policies)
                        )
                    )
                );
                $headers[$csp_header] = $csp_policy;
            }

            /* Permissions Policy */
            $pp = FrameworkConfig::get('debear.headers.permissions_policy');
            if (isset($pp['enabled']) && $pp['enabled']) {
                $policies = [];
                foreach ($pp['policies'] as $k => $p) {
                    $policies[] = "$k=(" . join(' ', $p) . ')';
                }
                $headers['Permissions-Policy'] = join(', ', $policies);
            }

            /* Expect CT */
            $ct = FrameworkConfig::get('debear.headers.expect_ct');
            if (isset($ct['enabled']) && $ct['enabled']) {
                $report_only = isset($ct['report_only']) && $ct['report_only'];
                $rule = (!$report_only ? 'enforce, ' : '')
                    . 'max-age=' . (!$report_only ? $ct['max_age'] : 0) . ', '
                    . 'report-url="' . $ct['report_uri'] . '"';
                $headers['Expect-CT'] = $rule;
            }
            // And then overlay?
            if ($overlay) {
                $headers = array_merge($headers, $custom);
            }
        }

        // Now add to the response.
        $response->withHeaders($headers);
    }

    /**
     * Return the raw CSP nonce, if it is enabled
     * @return string The Nonce as the raw string, or empty string if disabled
     */
    public static function getCSPNonce(): string
    {
        return (FrameworkConfig::get('debear.headers.csp.enabled')
            ? FrameworkConfig::get('debear.headers.csp.nonce') : '');
    }

    /**
     * Return the CSP nonce as a tag that can be embeded in the HTML
     * @return string The Nonce as the HTML attribute
     */
    public static function buildCSPNonceTag(): string
    {
        $nonce = static::getCSPNonce();
        if ($nonce) {
            return 'nonce="' . $nonce . '"';
        } else {
            return '';
        }
    }

    /**
     * Ensure the CSP rules are disabled
     * @return void
     */
    public static function disableCSP(): void
    {
        FrameworkConfig::set(['debear.headers.csp.enabled' => false]);
    }

    /**
     * Store referer information
     * @return void
     */
    public static function storeReferer(): void
    {
        // Skip if nothing to store.
        if (!Request::server('HTTP_REFERER')) {
            return;
        }

        // Split into parts.
        preg_match('/^\w+ttps?:\/\/([^\/]+)\/([^\?]*)\??(.*)$/', Request::server('HTTP_REFERER'), $split_referer);

        // If no actual refer parts available, self-generate with what we do have.
        if (!isset($split_referer[1])) {
            $split_referer = array('', Request::server('HTTP_REFERER'), '', '');
        }
        list($ref_domain, $ref_page, $ref_query) = array_slice($split_referer, 1);

        // However, skip internal redirects from one of our sites.
        $pattern = '/' . FrameworkConfig::get('debear.url.domain') . '(?:\.co)?\.([a-z]{2,4})(:\d{4})?$/';
        if (!preg_match($pattern, $ref_domain)) {
            SiteReferer::create([
                'referer_app' => FrameworkConfig::get('debear.names.site'),
                'when_done' => time(),
                'referer_domain' => $ref_domain,
                'referer_page' => ($ref_page !== '' ? $ref_page : null),
                'referer_args' => ($ref_query !== '' ? $ref_query : null),
                'referer_ip' => Request::server('REMOTE_ADDR'),
                'referer_browser' => Request::server('HTTP_USER_AGENT'),
                'referer_full_url' => Request::server('HTTP_REFERER'),
                'referered_to' => Request::server('REQUEST_SCHEME') . ':' . static::buildDomain()
                    . Request::server('REQUEST_URI'),
            ])->save();
        }
    }

    /**
     * Flag if the requests referer was Google-based
     * @return boolean If a Google domain was the referer
     */
    public static function refererIsGoogle(): bool
    {
        // Previously cached in the session?
        if (session()->has('user.referer.google')) {
            return session('user.referer.google');
        }
        // No, so calc and store.
        $google = (bool)preg_match('@^https?://[^/]*google(?:\.co)?\.[a-z]{2,3}/@i', Request::server('HTTP_REFERER'));
        session(['user.referer.google' => $google]);
        return $google;
    }
}
