<?php

namespace DeBear\Helpers;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Repositories\Logging;

class Benchmark
{
    /**
     * Whether the benchmarking is enabled or not (assume not, until explicitly enabled)
     * @var boolean
     */
    protected static $enabled = false;
    /**
     * The runtime info we build up over the course of a run
     * @var array
     */
    protected static $runtime = [
        'start' => 0,
        'stop' => 0,
        'runtime' => 0,
    ];
    /**
     * Individual runtime details
     * @var array
     */
    protected static $specific = [];
    /**
     * The current list of 'parent' nodes in our nesting process
     * @var array
     */
    protected static $nestingParents = [];
    /**
     * The current 'parent' node (ignoring the rest of its hierarchy)
     * @var ?string
     */
    protected static $nestingCurrent = null;
    /**
     * Top-level benchmark key
     * @var string
     */
    protected static $scriptKey = 'script';
    /**
     * The processed stamps into a relevant recursive list
     * @var array
     */
    protected static $stamps = [];
    /**
     * Whether we have detected a corruption within the start/stop naming
     * @var boolean
     */
    protected static $corrupt = false;
    /**
     * Details around the detected corruption
     * @var array
     */
    protected static $corruptionDetail = [
        'action' => false,
        'name_pri' => false,
        'name_sec' => false,
        'msg' => false,
    ];

    /**
     * Setup the benchmark processing
     * @return void
     */
    protected static function setup(): void
    {
        static::$enabled = FrameworkConfig::get('debear.benchmark.enabled');
    }

    /**
     * Start the timing flag for a section
     * @param string $name Benchmark name point to start.
     * @return void
     */
    public static function start(string $name): void
    {
        // Skip if a corruption was detected, or benchmarking is not enabled and this is not the base script option.
        if (static::$corrupt || (!static::$enabled && ($name != static::$scriptKey))) {
            return;
        }
        // Sanity check it doesn't exist (i.e., duplicate name or re-used).
        if (isset(static::$specific[$name])) {
            static::setCorrupted($name, 'start');
            return;
        }
        // Process.
        static::$specific[$name] = [
            'name' => $name,
            'start' => microtime(true),
            'end' => 0,
            'runtime' => 0,
            'percentage' => 0,
            'children' => [],
        ];
        // Nest?
        if (isset(static::$nestingCurrent)) {
            $curr = static::$nestingCurrent ?? ''; // Phan linting quirk.
            static::$specific[$curr]['children'][] = $name;
        }
        static::$nestingCurrent = static::$nestingParents[] = $name;
    }

    /**
     * End the timing flag for a section
     * @param string $name Benchmark name point to stop.
     * @return void
     */
    public static function stop(string $name): void
    {
        // Skip if a corruption was detected, or benchmarking is not enabled and this is not the base script option.
        if (static::$corrupt || (!static::$enabled && ($name != static::$scriptKey))) {
            return;
        }
        // Sanity check we are stopping the current stamp.
        if ($name != static::$nestingCurrent) {
            static::setCorrupted($name, 'stop');
            return;
        }
        // Process.
        static::$specific[$name]['end'] = microtime(true);
        array_pop(static::$nestingParents);
        static::$nestingCurrent = end(static::$nestingParents);
    }

    /**
     * Special wrapper to record the script starting
     * @return void
     */
    public static function scriptStart(): void
    {
        // Set ourselves up.
        static::setup();
        // Process.
        static::start(static::$scriptKey);
        static::$runtime['start'] = static::$specific[static::$scriptKey]['start'];
    }

    /**
     * Special wrapper to record the script finishing
     * @return void
     */
    public static function scriptEnd(): void
    {
        static::stop(static::$scriptKey);
        static::$runtime['end'] = static::$specific[static::$scriptKey]['end'];
        static::$runtime['runtime'] = static::$runtime['end'] - static::$runtime['start'];
    }

    /**
     * Get the time at which the script was started
     * @return float The raw start time that we stamped the script start
     */
    public static function getScriptRunTime(): float
    {
        return static::$runtime['runtime'];
    }

    /**
     * Get the time at which the script was started
     * @return float The raw start time that we stamped the script starting
     */
    public static function getScriptStartTime(): float
    {
        return static::$runtime['start'];
    }

    /**
     * Get the time at which the script finished
     * @return float The raw start time that we stamped the script finishing
     */
    public static function getScriptEndTime(): float
    {
        return static::$runtime['end'];
    }

    /**
     * State if we are outputting to logs the benchmarking info we logged
     * @return boolean Whether log rendering is enabled or not
     */
    public static function enabledLog(): bool
    {
        return static::enabledOption('log');
    }

    /**
     * State if we are outputting to screen the benchmarking info we logged
     * @return boolean Whether display rendering is enabled or not
     */
    public static function enabledDisplay(): bool
    {
        return static::enabledOption('display');
    }

    /**
     * State if we are outputting the benchmarking info we logged
     * @param string $type The type of output to render (which are independent of each other).
     * @return boolean Whether rendering is enabled or not
     */
    protected static function enabledOption(string $type): bool
    {
        return static::$enabled && FrameworkConfig::get("debear.benchmark.display.$type");
    }

    /**
     * Report on the benchmarking output
     * @param Response $response The response about to be rendered (with rendering placeholder).
     * @return void
     */
    public static function render(Response $response): void
    {
        // Skip outright if not enabled.
        if (!static::$enabled) {
            return;
        }

        // Process the raw stamps into something meaningful.
        if (!static::$corrupt) {
            static::$stamps = static::renderStamps(static::$scriptKey);
        }

        // First, if enabled, to log.
        if (static::enabledLog()) {
            static::renderLog();
        }
        // And then, if enabled and we have a response, to screen.
        if (isset($response) && static::enabledDisplay()) {
            static::renderDisplay($response);
        }
    }

    /**
     * Recursively process the stamps at a specific node
     * @param string $stamp The specific stamp to be processed.
     * @return array The processed stamp node
     */
    protected static function renderStamps(string $stamp): array
    {
        $info = static::$specific[$stamp];
        $info['runtime'] = $info['end'] - $info['start'];
        $overall_runtime = static::getScriptRunTime(); // Phan doesn't like using this inline.
        if ($overall_runtime) {
            $info['percentage'] = ($info['runtime'] / $overall_runtime);
        }
        // Any children to process?
        $children = [];
        foreach ($info['children'] as $child) {
            $children[] = static::renderStamps($child);
        }
        $info['children'] = $children;
        // Return our stamp component.
        return $info;
    }

    /**
     * Store the benchmark info in our logs
     * @return void
     */
    protected static function renderLog(): void
    {
        if (!static::$corrupt) {
            // Store as JSON, with some minor tweaks first.
            $log_msg = json_encode(static::$stamps);
            // Numbers to 3dp.
            $log_msg = preg_replace('/(\.\d{3})\d+/', '$1', $log_msg);
            // Remove the absolute timestamps.
            $log_msg = preg_replace('/"(start|end)":[\d\.]+,/', '', $log_msg);
            // And remove empty child nodes.
            $log_msg = str_replace(',"children":[]', '', $log_msg);
            // Shorten the keys.
            $log_msg = str_replace('"name":', '"n":', $log_msg);
            $log_msg = str_replace('"runtime":', '"t":', $log_msg);
            $log_msg = str_replace('"percentage":', '"p":', $log_msg);
            $log_msg = str_replace('"children":', '"c":', $log_msg);
        } else {
            $log_msg = "WARNING :: Benchmark corruption detected: " . json_encode(static::$corruptionDetail);
        }
        // Then write to the logs.
        Logging::add('Benchmark', $log_msg);
    }

    /**
     * Store the benchmark info to screen
     * @param Response $response The response about to be rendered (with rendering placeholder).
     * @return void
     */
    protected static function renderDisplay(Response $response): void
    {
        // Get the base content.
        $content = $response->getContent();

        // Skip if our tag is missing.
        $tag = '<!-- Benchmark: Render -->';
        if (strpos($content, $tag) === false) {
            return;
        }

        // Render our benchmark and add to the content.
        if (!static::$corrupt) {
            $rendered = Blade::render('skeleton.widgets.debug.benchmark.render', ['stamps' => [static::$stamps]]);
        } else {
            $rendered = Blade::render('skeleton.widgets.debug.benchmark.render-error', [
                'error' => static::$corruptionDetail,
            ]);
        }
        $response->setContent(str_replace($tag, $rendered, $content));
    }

    /**
     * Record the fact the benchmarking has been corrupted (without erroring)
     * @param string $name   The step where the corruption was detected.
     * @param string $action Whether we are attempting to start or stop a stamp.
     * @return void
     */
    protected static function setCorrupted(string $name, string $action): void
    {
        static::$corrupt = true;
        // Record additional info about the corruption.
        static::$corruptionDetail['action'] = $action;
        static::$corruptionDetail['name_pri'] = $name;
        if ($action == 'stop') {
            static::$corruptionDetail['name_sec'] = static::$nestingCurrent;
        }
        static::$corruptionDetail['msg'] = ($action == 'start'
            ? "Requested to start benchmark key '$name' when it has already been started. Potential duplicate name?"
            : "Requested to stop benchmark key '$name', when '" . static::$corruptionDetail['name_sec'] . "'"
                . ' is the currently active benchmark key.');
        // This will automatically set the write-to-log entry to record the warning.
        FrameworkConfig::set(["debear.benchmark.display.log" => true]);
    }

    /**
     * Reset the internals for our unit tests
     * @return void
     */
    public static function unitTestRefresh(): void
    {
        // Only applies in test mode.
        if (HTTP::isTest()) {
            static::$enabled = false;
            static::$corrupt = false;
            static::$runtime = [
                'start' => 0,
                'stop' => 0,
                'runtime' => 0,
            ];
            static::$specific = [];
            static::$nestingParents = [];
            static::$nestingCurrent = null;
            static::$stamps = [];
        }
    }
}
