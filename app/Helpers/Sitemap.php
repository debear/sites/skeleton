<?php

namespace DeBear\Helpers;

use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Repositories\InternalCache;

class Sitemap
{
    /**
     * Build the sitemap for the given section
     * @param string $site Sub-domain indicating the site we're building for.
     * @return array The generated sitemap
     */
    public static function buildSitemap(string $site = ''): array
    {
        if (!$site) {
            $site = FrameworkConfig::get('debear.url.sub');
        }
        $cache_key = "sitemap:$site";

        /* Have we already loaded it? */
        $sitemap = InternalCache::object()->get($cache_key);
        if ($sitemap !== false) {
            return $sitemap;
        }

        /* Nope, so generate, store it and return */
        $sitemap = static::build('sitemap', $site);

        // Store, return.
        InternalCache::object()->set($cache_key, $sitemap);
        return $sitemap;
    }

    /**
     * Get the navigation for a page
     * @param string $site Sub-domain indicating the site we're building for. (Optional).
     * @return array The appropriate navigation list
     */
    public static function buildNavigation(string $site = ''): array
    {
        if (!$site) {
            $site = FrameworkConfig::get('debear.url.sub');
        }
        $cache_key = "navigation:$site";

        /* Have we already loaded it? */
        $navigation = InternalCache::object()->get($cache_key);
        if ($navigation !== false) {
            return $navigation;
        }

        /* Nope, so generate, store it and return */
        // Get the current nav links.
        $curr = HTML::getNavCurrent() ?: '@@fail:not-set@@';
        // Starting with the sitemap, remove any items that are not for the nav.
        $sitemap = static::build('navigation', $site);
        $navigation = [];
        $section = FrameworkConfig::get('debear.section.code');
        foreach ($sitemap as $code => $link) {
            // Current navigation link?
            $url_section_set = (isset($link['url-section']) && $link['url-section']);
            $link['sel'] = ($link['url_raw'] == $curr) && (!$section || $url_section_set);

            // Any children to parse?
            if (is_array($link['links']) && count($link['links'])) {
                $children = [];
                foreach ($link['links'] as $subcode => $child) {
                    // Current sub-navigation link?
                    $is_sel = ($child['url_raw'] == $curr)
                        && (!$section || (isset($child['url-section']) && $child['url-section']));
                    $child['sel'] = $is_sel;
                    // If so, also mark as selected the parent node.
                    $link['sel'] = ($link['sel'] || $is_sel);
                    // Add to our list.
                    $children[$subcode] = $child;
                }
                $link['links'] = $children;
            }

            // Add to our object.
            $navigation[$code] = $link;
        }

        // Store, return.
        InternalCache::object()->set($cache_key, $navigation);
        return $navigation;
    }

    /**
     * Build the link list for the given section
     * @param string $type The link type we are loading.
     * @param string $site Sub-domain indicating the site we're building for.
     * @return array The generated list for a sitemap / nav
     */
    protected static function build(string $type, string $site): array
    {
        // Process the full-list to just the appropriate section.
        $links = static::processLinks($type, FrameworkConfig::get('debear.links'));
        // Move any children into appropriate sections.
        foreach ($links as $key => &$link) {
            if (isset($link['section']) && is_string($link['section']) && $link['section']) {
                $links[$link['section']]['links'][$key] = $link;
                unset($links[$key]);
            }
        }

        return $links;
    }

    /**
     * Process the list of links for a given type
     * @param string $type  The link type we are loading.
     * @param array  $links The raw links to be processed.
     * @return array The resulting links for the requested type
     */
    public static function processLinks(string $type, array $links): array
    {
        $site_section = FrameworkConfig::get('debear.section.endpoint');
        $ret = [];
        foreach ($links as $key => $link) {
            // Must be enabled and configured for this mode.
            if (isset($link['enabled']) && !$link['enabled']) {
                continue;
            }
            if (isset($link[$type]) && is_bool($link[$type])) {
                $link[$type] = [ 'enabled' => $link[$type] ];
            }
            if (!isset($link[$type]) || !is_array($link[$type])) {
                continue;
            }
            // Process.
            $link = Arrays::merge($link, $link[$type]);
            $enabled = !isset($link['enabled']) || $link['enabled'];
            $disabled = isset($link['disabled']) && $link['disabled']; // Alternate method to define...
            $failed_policy = isset($link['policy']) && !Policies::match($link['policy']);
            if ($enabled && !$disabled && !$failed_policy) {
                $ret[$key] = Arrays::merge(['title' => '', 'descrip' => '', 'url' => false, 'links' => []], $link);
                // Any post-processing on the URL?
                $ret[$key]['url_raw'] = $ret[$key]['url'];
                if ($ret[$key]['url']) {
                    if ($site_section && isset($ret[$key]['url-section']) && $ret[$key]['url-section']) {
                        $ret[$key]['url'] = "/{$site_section}{$ret[$key]['url']}";
                    }
                    if (
                        isset($ret[$key]['url-extra'])
                        && is_string($ret[$key]['url-extra'])
                        && $ret[$key]['url-extra']
                    ) {
                        $ret[$key]['url'] = rtrim($ret[$key]['url'], '/') . '/' . $ret[$key]['url-extra'];
                    }
                    if (isset($ret[$key]['query']) && is_array($ret[$key]['query']) && count($ret[$key]['query'])) {
                        $ret[$key]['url'] .= '?' . http_build_query($ret[$key]['query']);
                    }
                }

                // Sub-links to process?
                if (count($ret[$key]['links'])) {
                    $ret[$key]['links'] = static::processLinks($type, $ret[$key]['links']);
                }
            }
        }
        uasort(
            $ret,
            function ($a, $b) {
                return $a['order'] <=> $b['order'];
            }
        );

        // Then return.
        return $ret;
    }

    /**
     * An escaped, XML sitemap safe, URL conversion
     * @param string $url The URL to convert.
     * @return string The XML-safe URL
     */
    public static function convertURL(string $url): string
    {
        $domain = 'https:' . HTTP::buildDomain();
        $run_checks = true; // Gets us into the loop.
        while ($run_checks) {
            if (substr($url, 0, 3) == '../') {
                // Relative link?
                $url = $domain . str_replace('../', '/', $url);
            } elseif (substr($url, 0, 1) == '/') {
                // Absolute link missing domain?
                $url = $domain . $url;
            } else {
                // All OK then!
                $run_checks = false;
            }
        }

        return $url;
    }

    /**
     * Find a specific element (by code) in the sitemap
     * @param string $code The needle.
     * @param string $site Site to be looking at (Optional, Default: current site).
     * @return array The matching element
     */
    public static function findSitemapElement(string $code, string $site = ''): array
    {
        return static::findElement(static::buildSitemap($site), $code);
    }

    /**
     * Find a specific element (by code) in the navigation
     * @param string $code The needle.
     * @param string $site Site to be looking at (Optional, Default: current site).
     * @return array The matching element
     */
    public static function findNavigationElement(string $code, string $site = ''): array
    {
        return static::findElement(static::buildNavigation($site), $code);
    }

    /**
     * Worker method for finding an element in the model
     * @param array  $links The haystack of links (sitemap/nav) to parse.
     * @param string $code  The needle.
     * @return array The matching element
     */
    protected static function findElement(array $links, string $code): array
    {
        // A direct match?
        if (isset($links[$code])) {
            return $links[$code];
        }
        // Scan each of the children.
        foreach ($links as $link) {
            if (isset($link['links']) && is_array($link['links']) && count($link['links'])) {
                $ret = static::findElement($link['links'], $code);
                if (count($ret)) {
                    return $ret;
                }
            }
        }
        // No match, so return an empty array to indicate a failure.
        return [];
    }

    /**
     * Parse the sitemap to separate any subsites out from the remaining pages
     * @param array $sitemap The raw, original, ordered sitemap.
     * @return array The manipulated sitemap in semantic sections
     */
    public static function parseSubsites(array $sitemap): array
    {
        $subsite_keys = array_flip(array_filter(array_keys($sitemap), function ($key) {
            return substr($key, 0, 8) == 'subsite-';
        }));
        return [
            'subsites' => array_intersect_key($sitemap, $subsite_keys),
            'admin' => $sitemap['admin']['links'],
        ];
    }
}
