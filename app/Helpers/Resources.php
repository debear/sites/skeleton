<?php

namespace DeBear\Helpers;

use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Exceptions\ResourcesException;

class Resources
{
    /**
     * Append a CSS file to the list included on the page
     * @param string $file The CSS file to be added.
     * @param array  $opt  Bespoke processing options.
     * @return void
     */
    public static function addCSS(string $file, array $opt = []): void
    {
        static::addCSSJS('css', $file, $opt);
    }

    /**
     * Append a JavaScript file to the list included on the page
     * @param string $file The JavaScript file to be added.
     * @param array  $opt  Bespoke processing options.
     * @return void
     */
    public static function addJS(string $file, array $opt = []): void
    {
        static::addCSSJS('js', $file, $opt);
    }

    /**
     * Worker method to a custom CSS/JS file to the list to be included on the page
     * @param string $type Whether we're adding CSS or JavaScript.
     * @param string $file The file to add.
     * @param array  $opt  Bespoke processing options.
     * @return void
     */
    protected static function addCSSJS(string $type, string $file, array $opt = []): void
    {
        // A file we minify separately?
        if (!isset($opt['external']) || !$opt['external']) {
            $cssjs = $file;
        } else {
            $cssjs = ['file' => $file, 'external' => true];
        }
        // Now add the list, in the appropriate order.
        $config = "debear.resources.$type";
        if (isset($opt['prepend']) && $opt['prepend']) {
            $merged = array_merge([$cssjs], FrameworkConfig::get($config));
        } else {
            $merged = array_merge(FrameworkConfig::get($config), [$cssjs]);
        }
        FrameworkConfig::set([$config => $merged]);
    }

    /**
     * Determine the CSS files to include in the page
     * @return array The list of CSS files to embed in the page
     */
    public static function getCSS(): array
    {
        return static::getCSSJS('css');
    }

    /**
     * Determine the JavaScript files to include in the page
     * @return array The list of JavaScript files to embed in the page
     */
    public static function getJS(): array
    {
        return static::getCSSJS('js');
    }

    /**
     * Worker method to return the list of CSS/JS file(s) to include on the page
     * @param string $type Whether we're returning CSS or JavaScript.
     * @return array The URL of the CSS/JS file(s) to include in the page
     * @throws ResourcesException When we're passed in a test we haven't yet built processing logic for.
     */
    protected static function getCSSJS(string $type): array
    {
        // Any pre-processing logic on this list?
        $files = array_unique(FrameworkConfig::get("debear.resources.$type"), SORT_REGULAR);
        $ext_file = [];
        foreach ($files as $key => &$spec) {
            if (is_array($spec)) {
                $had_flag = false;
                /* An externally sourced file? */
                if (isset($spec['external']) && $spec['external']) {
                    $ext_file[] = $spec['file'];
                    $had_flag = true;
                }
                /* Determine what tests to run */
                $tests = [];
                // Environment test.
                if (isset($spec['env']) && is_callable($spec['env'])) {
                    $tests[] = [
                        'type' => 'env',
                        'test' => $spec['env'],
                        'args' => [],
                    ];
                }
                // Policy test.
                if (isset($spec['policies'])) {
                    $tests[] = [
                        'type' => 'policy',
                        'test' => ['DeBear\Helpers\Policies', 'match'],
                        'args' => [$spec['policies']],
                    ];
                }
                // Process the test.
                if (!sizeof($tests) && !$had_flag) {
                    if (HTTP::isDev() || (HTTP::isTest() && isset($spec['throw-fatal']) && $spec['throw-fatal'])) {
                        // Don't know how to proceed whilst in a dev environment, so flag.
                        throw new ResourcesException(
                            "Unable to process '$type' file '{$spec['file']}' - " . json_encode($spec)
                                . ' does not provide any tests?'
                        );
                    } else {
                        // Don't know how to proceed whilst in production, so remove the file (failsafe).
                        unset($files[$key]);
                    }
                    continue;
                }
                /* Run */
                foreach ($tests as $test) {
                    if (!call_user_func_array($test['test'], $test['args'])) {
                        unset($files[$key]);
                        continue 2;
                    }
                }
                // It matched so keep the file, setting it as the value for embedding later.
                $spec = $spec['file'];
            }
        }
        // Re-base the array.
        $files = array_values($files);

        // Are we going to merge and minify the files?
        $merge_config = FrameworkConfig::get("debear.resources.{$type}_merge");
        if ($merge_config && is_string($merge_config)) {
            // Pre-fabricated merge.
            $files = array_filter([$ext_file ? Merge::$type($ext_file) : [], $merge_config]);
        } elseif ($merge_config) {
            // Merging on the fly.
            $files = [Merge::$type($files)];
        }

        // Convert URLs to the static site naming.
        return HTTP::buildStaticURLs($type, $files);
    }

    /**
     * Convert input resource filenames to their filesystem equivalent
     * @param array $files The files to be processed.
     * @return array The input files converted to the filesystem names
     */
    public static function convert(array $files): array
    {
        foreach ($files as &$f) {
            if (substr($f, 0, 5) != 'skel/') {
                $f = FrameworkConfig::get('debear.url.sub') . '/' . $f;
            }
        }
        return $files;
    }
}
