<?php

namespace DeBear\Helpers\PHPUnit;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\HTTP;
use DeBear\Exceptions\Testing\DatabaseException;

class RefreshDatabase
{
    /**
     * Cached results
     * @var array
     */
    protected static $cache;
    /**
     * Whether or not we need the database prefix
     * @var boolean
     */
    protected static $noDBPrefix;

    /**
     * Run the database management
     * @return void
     */
    public static function run(): void
    {
        if (!isset(static::$cache)) {
            // On first run, build the cache info.
            static::buildCache();
        } else {
            // But then re-import on subsequent runs.
            static::reimportCache();
        }
    }

    /**
     * Build the cache of information to later run
     * @return void
     * @throws DatabaseException To alert user we are missing a pre-req environment variable.
     */
    protected static function buildCache(): void
    {
        // We can only run if we have been passed a prefix, otherwise we'll be affecting development databases.
        // (Written awkwardly for Code Coverage...).
        if ((env('DB_PREFIX') === null) || (HTTP::isTest() && static::$noDBPrefix)) {
            throw new DatabaseException('Missing required environment variable: DB_PREFIX');
        }

        // Get the _repo_ vendor dir, not the skeleton's dir.
        $vendor_db = 'debear/setup/mysql';
        $vendor_dir = realpath(FrameworkConfig::get('debear.dirs.vendor')) . "/$vendor_db";
        $repo = basename(getcwd());
        // Quick path fix for the CI, where the skeleton vendor dir may be a sub-folder within the repo's vendor dir.
        $vendor_dir = str_replace("$repo/vendor/debear/skeleton", $repo, $vendor_dir);
        // Set the correct vendor dir based on the repo we are testing (in an awkward code coverage way).
        ($repo == 'skeleton') || ($vendor_dir = str_replace('_debear', $repo, $vendor_dir));

        // Determine the appropriate databases and source files, prioritising files from our repo last.
        $src_dir = glob("$vendor_dir/*");
        // @codeCoverageIgnoreStart
        // The CI job that processes this works on a single repo and skips the sort so exclude from code coverage count.
        usort($src_dir, function ($a, $b) use ($repo) {
            $is_a = (basename($a) == $repo);
            $is_b = (basename($b) == $repo);
            return $is_a <=> $is_b;
        });
        // @codeCoverageIgnoreEnd
        $src = [];
        foreach ($src_dir as $repo_dir) {
            $src = array_merge($src, glob("$repo_dir/*/*.sql.gz"));
        }
        $tbl_map = [];
        static::$cache = array_flip(array_unique(array_map(
            function ($file) use ($vendor_db, &$tbl_map) {
                $db = env('DB_PREFIX') . preg_replace("@^.+$vendor_db/[^/]+/([^/]+)/.*?\.sql.gz$@", '$1', $file);
                if (!isset($tbl_map[$db])) {
                    $tbl_map[$db] = [];
                }
                // But also determine the database-to-table map.
                $tbl = preg_replace("@^.+$vendor_db/[^/]+/[^/]+/(.*?)\.sql.gz$@", '$1', $file);
                if (!isset($tbl_map[$db][$tbl])) {
                    $tbl_map[$db][$tbl] = [];
                }
                $tbl_map[$db][$tbl][] = $file;
                // Finally return our database name.
                return $db;
            },
            $src
        )));

        // Which connections do these map to?
        foreach (FrameworkConfig::get('database.connections') as $conn => $config) {
            if (
                substr($conn, 0, 5) == 'mysql' // We only want MySQL connections.
                && isset(static::$cache[$config['database']])
                && is_numeric(static::$cache[$config['database']])
            ) {
                $ti_col = 'Tables_in_' . $config['database'];
                static::$cache[$config['database']] = [
                    'connection' => $conn,
                    'truncate' => array_column(DB::connection($conn)->select('SHOW TABLES;'), $ti_col),
                    'insert' => $tbl_map[$config['database']],
                ];
            }
        }
    }

    /**
     * Reimport the data we cached
     * @return void
     */
    protected static function reimportCache(): void
    {
        foreach (static::$cache as $db) {
            // First, the truncations.
            foreach ($db['truncate'] as $tbl) {
                DB::connection($db['connection'])->delete("TRUNCATE TABLE $tbl;");
            }
            // Then import those files that have seeded data.
            foreach ($db['insert'] as $tbl_files) {
                foreach ($tbl_files as $file) {
                    $seed = gzdecode(file_get_contents($file));
                    DB::connection($db['connection'])->insert($seed);
                }
            }
        }
    }

    /**
     * Test the missing database prefix (internally, given the mechanics of the module)
     * @return boolean Whether the expected exception was caught or not
     */
    public static function testNoDBPrefix(): bool
    {
        $found = false;
        if (HTTP::isTest()) {
            $orig = static::$cache;
            static::$cache = null;
            static::$noDBPrefix = true;
            try {
                static::buildCache();
            } catch (DatabaseException $e) {
                static::$noDBPrefix = false;
                static::$cache = $orig;
                $found = true;
            }
        }
        return $found;
    }
}
