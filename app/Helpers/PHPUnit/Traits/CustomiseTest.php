<?php

namespace DeBear\Helpers\PHPUnit\Traits;

use Illuminate\Support\Facades\Config as FrameworkConfig;
use Carbon\Carbon;
use DeBear\Repositories\Time;

trait CustomiseTest
{
    /**
     * The cached core date/time config for the app.
     * @var array
     */
    protected $config_core;
    /**
     * The cached final date/time config for this sport.
     * @var array
     */
    protected $config_datetime;
    /**
     * The raw and original configuration values for the keys being tweaked to be used in resetting the time, at the
     * conclusion of a test.
     * @var array
     */
    protected $orig_config;
    /**
     * The raw and original date/time to be used in resetting the time, at the conclusion of a test.
     * @var ?string
     */
    protected $orig_datetime;

    /**
     * Tidy up internal object changes between tests
     * @return void
     */
    protected function tearDown(): void
    {
        // If we changed anything for the test, revert those changes.
        $this->resetTestConfig();
        $this->resetTestDateTime();
        // Run the standard tear down process.
        parent::tearDown();
    }

    /**
     * Adjust configuration properties to facilitate the test being run
     * @param array $test_config The new configuration (key as a fully-qualified path) to be applied for this test.
     * @return void
     */
    protected function setTestConfig(array $test_config): void
    {
        // Take a backup of the current value(s).
        $this->orig_config = [];
        foreach (array_keys($test_config) as $key) {
            $this->orig_config[$key] = FrameworkConfig::get($key);
        }
        // Then apply our per-test value(s).
        FrameworkConfig::set($test_config);
    }

    /**
     * Revert the configuration changes made for the test applied
     * @return void
     */
    protected function resetTestConfig(): void
    {
        if (isset($this->orig_config)) {
            FrameworkConfig::set($this->orig_config);
            $this->orig_config = null;
        }
    }

    /**
     * Adjust the time to facilitate the test being run, accounting for things like timezones
     * @param string $time The (app) time we will be changing to.
     * @return void
     */
    protected function setTestDateTime(string $time): void
    {
        // Update the timezone configuration to reflect the environment being run.
        if (!isset($this->config_core)) {
            $this->config_core = FrameworkConfig::get('debear.datetime');
            $this->config_datetime = array_merge($this->config_core, $this->config_datetime ?? []);
        }
        FrameworkConfig::set(['debear.datetime' => $this->config_datetime]);
        // Backup the current time for future restoration.
        $this->orig_datetime = Time::object()->getServerNowFmt();
        // Update the database timezone used, converting from desired-app time to server time.
        $time_server = (new Carbon($time, $this->config_datetime['timezone_app']))
            ->setTimezone($this->config_datetime['timezone_server'])
            ->toDateTimeString();
        Time::object()->resetCache($time_server);
        // Revert the datetime config tweaks for the time setting.
        FrameworkConfig::set(['debear.datetime' => $this->config_core]);
    }

    /**
     * Revert the time change made for the test applied
     * @return void
     */
    protected function resetTestDateTime(): void
    {
        if (isset($this->orig_datetime)) {
            // Reset the timezone info to the base values, not those updated by the test.
            FrameworkConfig::set(['debear.datetime' => $this->config_core]);
            // And now reset the time.
            Time::object()->resetCache($this->orig_datetime);
            $this->orig_datetime = null;
        }
    }
}
