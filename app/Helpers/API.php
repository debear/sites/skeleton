<?php

namespace DeBear\Helpers;

use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;

class API
{
    /**
     * Return a generic API response for an appropriate status code
     * @param integer $status The status code to return.
     * @param string  $msg    Message to send within the response.
     * @return JsonResponse API response with the appropriate HTTP status code and info set
     */
    public static function setStatus(int $status, string $msg): JsonResponse
    {
        return response()->json(['msg' => $msg], $status);
    }

    /**
     * Wrapper to send the HTTP status code when content was processed
     * @param string $msg Optional content to be returned in the response.
     * @return JsonResponse API response with the appropriate HTTP status code and info set
     */
    public static function sendResponse(string $msg = 'Resource Processed'): JsonResponse
    {
        return static::setStatus(200, $msg);
    }

    /**
     * Wrapper to send the HTTP status code when content was created
     * @param string $msg Optional content to be returned in the response.
     * @return JsonResponse API response with the appropriate HTTP status code and info set
     */
    public static function sendCreated(string $msg = 'Resource Created'): JsonResponse
    {
        return static::setStatus(201, $msg);
    }

    /**
     * Wrapper to send the HTTP status code when no content is to be returned
     * @return Response Response object with the appropriate HTTP status code
     */
    public static function sendNoContent(): Response
    {
        return response('', 204); // Does not need to use the worker because no content is actually sent.
    }

    /**
     * Wrapper to send the HTTP status code when a bad request was received
     * @param string $msg Optional content to be returned in the response.
     * @return JsonResponse Response object with the appropriate HTTP status code
     */
    public static function sendBadRequest(string $msg = 'Bad Request'): JsonResponse
    {
        return static::setStatus(400, $msg);
    }

    /**
     * Wrapper to send a bad request HTTP response when a request failed its validation
     * @param array $err Array of validation errors.
     * @return JsonResponse Response object with the appropriate HTTP status code and error details
     */
    public static function sendValidationFailure(array $err): JsonResponse
    {
        return response()->json([
            'msg' => 'The input data did not pass the required validation rules',
            'fields' => array_combine(array_keys($err), array_column($err, 'message')),
        ], 400);
    }

    /**
     * Wrapper to send the HTTP status code when the user has not logged in yet
     * @param string $msg Optional content to be returned in the response.
     * @return JsonResponse Response object with the appropriate HTTP status code
     */
    public static function sendUnauthorised(string $msg = 'User Not Signed In'): JsonResponse
    {
        return static::setStatus(401, $msg);
    }

    /**
     * Wrapper to send the HTTP status code when the logged in user is not allowed to make the request
     * @param string $msg Optional content to be returned in the response.
     * @return JsonResponse Response object with the appropriate HTTP status code
     */
    public static function sendForbidden(string $msg = 'Insufficient Privileges'): JsonResponse
    {
        return static::setStatus(403, $msg);
    }

    /**
     * Wrapper to send the HTTP status code when the requested resource was not found
     * @param string $msg Optional content to be returned in the response.
     * @return JsonResponse Response object with the appropriate HTTP status code
     */
    public static function sendNotFound(string $msg = 'Resource Not Found'): JsonResponse
    {
        return static::setStatus(404, $msg);
    }

    /**
     * Wrapper to send the HTTP status code when the wrong HTTP method was used
     * @param string $msg Optional content to be returned in the response.
     * @return JsonResponse Response object with the appropriate HTTP status code
     */
    public static function sendMethodNotAllowed(string $msg = 'Request Method Not Allowed'): JsonResponse
    {
        return static::setStatus(405, $msg);
    }

    /**
     * Wrapper to send the HTTP status code when there is a conflict between the request and our expectation of it
     * @param string $msg Optional content to be returned in the response.
     * @return JsonResponse Response object with the appropriate HTTP status code
     */
    public static function sendConflict(string $msg = 'Conflict'): JsonResponse
    {
        return static::setStatus(409, $msg);
    }
}
