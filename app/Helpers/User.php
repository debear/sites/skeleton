<?php

namespace DeBear\Helpers;

use GuzzleHttp\Exception\RequestException;
use Icawebdesign\Hibp\Password\PwnedPassword;
use Icawebdesign\Hibp\HibpHttp;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\HTTP;
use DeBear\Implementations\Hasher;

class User
{
    /**
     * Confirm the password complies with the password policy
     * @param mixed $password New password being validated.
     * @param array $data     Over-riding data (fields not taken from the current User object).
     * @param array $opt      Run-time options.
     * @return string|boolean A bool of false if valid, otherwise an approrpiate error message
     */
    public static function validatePasswordPolicy(mixed $password, array $data = [], array $opt = []): string|bool
    {
        $user_id = $data['user_id'] ?? Auth::user()->user_id ?? ''; // Phan linting quirk.
        $email = $data['email'] ?? Auth::user()->email ?? ''; // Phan linting quirk.

        // 1. Meets the minimum length.
        $min_length = FrameworkConfig::get('debear.security.password.min_length');
        if (mb_strlen($password) < $min_length) {
            return "Your password must be $min_length or more characters in length";
        }
        // 2. If an existing user, it has actually changed.
        if (
            isset($opt['check-reuse'])
            && $opt['check-reuse']
            && App::make(Hasher::class)->check($password, Auth::user()->password, ['salt' => $user_id])
        ) {
            return 'You cannot re-use your existing password';
        }
        // 3. Does not feature the user's ID.
        if (strpos($password, $user_id) !== false) {
            return 'The password cannot include your username';
        }
        // 4. Does not feature the user's email address.
        if (strpos($password, $email) !== false) {
            return 'The password cannot include your email address';
        }
        // 5a. (Not advertised in the policy) Does not feature the word 'password'.
        $pwned_error = 'This password is known to be compromised so is not deemed secure. Please enter another';
        if (strpos($password, 'password') !== false) {
            return $pwned_error;
        }
        // 5b. Was not part of a breach (allowing for a small threshold).
        $known_breach = static::knownBreachedPassword($password);
        if ($known_breach) {
            return (is_string($known_breach) ? $known_breach : $pwned_error);
        }

        // No issues, so return a status boolean.
        return false;
    }

    /**
     * Mask the logic for validating if password was breached
     * @param mixed $password New password being validated.
     * @return boolean|string That the password was known to be exposed in a previous breach
     */
    protected static function knownBreachedPassword(mixed $password): bool|string
    {
        // @codeCoverageIgnoreStart
        // Due to issues within this in live, exclude from code coverage and certain quality checks.
        if (HTTP::isTest()) {
            // In test mode react to a specific value.
            return in_array($password, FrameworkConfig::get('debear.security.password.ci_mock_breached'));
        } elseif (HTTP::isLive()) {
            // Put through the real service in live, using an upstream hashing function SemGrep does not approve of.
            // nosemgrep PHPCS_SecurityAudit.BadFunctions.CryptoFunctions.WarnCryptoFunc.
            $pwd_sha = strtoupper(sha1($password));
            try {
                $pwned_passwd = (new PwnedPassword(new HibpHttp()));
                $pwned_lookup = $pwned_passwd->paddedRangeDataFromHash($pwd_sha)->get($pwd_sha);
                /* As the catch/return error is outside our testable control, we cannot include it in our coverage. */
            } catch (RequestException $e) {
                // An error occurred with the lookup, fail safe and do not proceed.
                return 'An error occurred validating your password. Please try again later';
            }
            /* @phan-suppress-next-line PhanTypeArraySuspicious */
            $pwned_count = ($pwned_lookup !== null ? $pwned_lookup['count'] : 0);
            if ($pwned_count > FrameworkConfig::get('debear.security.password.breach_threshold')) {
                return true;
            }
        }
        // Do not process in development environment, so act as if no issues found.
        return false;
        // @codeCoverageIgnoreEnd
    }
}
