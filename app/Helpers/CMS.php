<?php

namespace DeBear\Helpers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Support\Collection;
use DeBear\Exceptions\CMSException;
use DeBear\Models\Skeleton\CMSContent;
use DeBear\Repositories\BugReporting;

class CMS
{
    /**
     * Pre-cached content we have already pulled out
     * @var array
     */
    protected static $content = [];

    /**
     * Get the CMS content for a particular section
     * @param string $page    The page to load content for.
     * @param string $section The specific section within the page we are after.
     * @param array  $opt     Custom options to define some processing rules.
     * @return string The CMS content for the requested section
     * @throws CMSException If no content could be found for this section.
     */
    public static function getSection(string $page, string $section, array $opt = []): string
    {
        $ret = join(static::loadSection($page, $section, $opt)->pluck('content')->toArray());

        // Any variable interpolation to run?
        if (isset($opt['interpolate']) && is_array($opt['interpolate'])) {
            foreach ($opt['interpolate'] as $key => $value) {
                $ret = str_replace("\$$key", $value, $ret);
            }
        }

        return $ret;
    }

    /**
     * Load from the database the CMS content for a particular page
     * @param string $page The page to load content for.
     * @param array  $opt  Custom options to define some processing rules.
     * @return array The relevant database objects for the requested page, broken down in to a more usable way
     * @throws CMSException If no content could be found for this section.
     */
    protected static function loadPage(string $page, array $opt = []): array
    {
        $site = $opt['site'] ?? FrameworkConfig::get('debear.url.sub');
        $key = "$site::$page";
        if (!isset(static::$content[$key])) {
            // Build, as it's not been reviously loaded.
            $raw = CMSContent::where([
                ['site', '=', $site],
                ['page', '=', $page],
                ['live', '=', 1],
            ])->orderBy('order')->get();

            // Anything found?
            if (!sizeof($raw)) {
                // No, so report the issue and give appropriate feedback.
                $err = "Could not find CMS content for $site page '$page'";
                App::make(BugReporting::class)->raise(E_USER_WARNING, $err);
                if (FrameworkConfig::get('debear.cms.missing-exceptions.page')) {
                    // Production: Handle silently.
                    // Development / Testing: Inform the user.
                    throw new CMSException($err);
                }
            }

            // Process.
            static::$content[$key] = ['_raw' => $raw];
            foreach ($raw->pluck('section')->unique() as $section) {
                static::$content[$key][$section] = $raw->where('section', '=', $section);
            }
        }
        return static::$content[$key];
    }

    /**
     * Load from the database the CMS content for a particular section
     * @param string $page    The page to load content for.
     * @param string $section The specific section within the page we are after.
     * @param array  $opt     Custom options to define some processing rules.
     * @return Collection The relevant database objects for the requested section
     * @throws CMSException If no content could be found for this section.
     */
    protected static function loadSection(string $page, string $section, array $opt = []): Collection
    {
        $pageContent = static::loadPage($page, $opt);

        // If nothing found for this section, complain.
        if (!isset($pageContent[$section])) {
            $site = $opt['site'] ?? FrameworkConfig::get('debear.url.sub');
            if (!FrameworkConfig::get('debear.cms.missing-exceptions.section')) {
                // Production: Handle silently.
                $pageContent[$section] = collect([
                    'content' => '',
                ]);
            } else {
                // Development / Testing: Inform the user.
                throw new CMSException("Could not find CMS content for section '$section' of $site page '$page'");
            }
        }

        // Return what we have then.
        return $pageContent[$section];
    }
}
