<?php

namespace DeBear\Helpers;

use JsonException;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Exceptions\Cookies\PrefsException;

class CookiePreferences
{
    /**
     * The data associated with the preferences cookie
     * @var array
     */
    protected static $pref_data;

    /**
     * Get the stored cookie preferences, creating with defaults if no existing cookie found
     * @return void
     */
    public static function load(): void
    {
        // Try and load from a cookie.
        try {
            if (Cookie::hasValue(FrameworkConfig::get('debear.sessions.cookies.prefs'))) {
                $pref_cookie = Cookie::getValue(FrameworkConfig::get('debear.sessions.cookies.prefs')) ?? '';
                static::$pref_data = json_decode($pref_cookie, true, 512, JSON_THROW_ON_ERROR);
                static::$pref_data['first-load'] = false;
            }
        } catch (JsonException $e) {
            // Silently fail, the next check will handler errors for us.
            static::$pref_data = null;
        }
        // But generate if not found (or an error in its value).
        if (!isset(static::$pref_data)) {
            static::$pref_data = static::defaults();
        }
        static::store();
    }

    /**
     * Actually perform the action of saving the cookie preferences as more than just the in-page array
     * @return void
     */
    public static function store(): void
    {
        // 4150785563 = 23:59:59 2099-12-31... i.e., indefinite.
        Cookie::setValue(
            FrameworkConfig::get('debear.sessions.cookies.prefs'),
            json_encode(static::$pref_data),
            4150785563
        );
    }

    /**
     * Get a specific cookie preference value from our config array
     * @param string $opt Name of the config option we are after.
     * @return mixed The value in the requested cookie preference config option
     * @throws PrefsException When requesting an option that doesn't exist in the config array.
     */
    public static function get(string $opt): mixed
    {
        // If the cookie hasn't setup or loaded properly (e.g., accessing over HTTP not HTTPS), we should
        // use the default value as a makeshift start point.
        $pref_data = (is_array(static::$pref_data ?? '') ? static::$pref_data : static::defaults());
        if (!isset($pref_data[$opt])) {
            // This should have been setup, isn't an expected edge-case, so return an error.
            throw new PrefsException("Attempting to load unknown option '$opt'");
        }
        return $pref_data[$opt];
    }

    /**
     * Update an individual cookie value
     * @param string $opt   Name of the config option we are trying to update.
     * @param mixed  $value The new value of the cookie preference config option.
     * @return void
     */
    public static function set(string $opt, mixed $value): void
    {
        static::$pref_data[$opt] = $value;
    }

    /**
     * State what the default / initial preferences should be regarding user's cookie preference
     * @return array The default / initial preferences to pass the user
     */
    public static function defaults(): array
    {
        return [
            'first-load' => true,
            'allow-analytics' => true,
        ];
    }
}
