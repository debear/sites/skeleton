<?php

namespace DeBear\Helpers;

use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Exceptions\ConfigException;

class Config
{
    /**
     * The raw configuration before it was pre-processed
     * @var array
     */
    protected static $rawConfig = [];
    /**
     * Flag to indicate whether the customisable configuration files should be loaded
     * @var boolean
     */
    protected static $loadCustomConfig = true;

    /**
     * Perform some multi-site post-processing on the config
     * @param array $config The config we will manipulate.
     * @return array The manipulated config
     */
    public static function postProcess(array $config): array
    {
        // Preserve the original configuration passed in.
        if (!static::$rawConfig) {
            static::$rawConfig = $config;
        }

        // Process the dirs to build absolute(ish) paths.
        $base_path = (!HTTP::isTest() ? base_path() : __DIR__ . '/../..');
        foreach ($config['dirs'] as &$d) {
            if (substr($d, 0, 1) != '/') {
                continue;
            }
            $d = "$base_path$d";
            if (!HTTP::isTest() || file_exists($d)) {
                $d = realpath($d);
            }
        }
        unset($d);

        // Any skeleton environment / CI-specific config to load (either-or, not both)?
        if (static::$loadCustomConfig) {
            $path = config_path() . "/debear/" . (!HTTP::isTest() ? '_env' : '_ci') . '.php';
            $config = Arrays::merge($config, file_exists($path) ? include $path : []);
        }

        // Which subdomains are we processing?
        $subdomains = $config['subdomains'];
        $config['subdomains'] = array_keys($subdomains);

        // Form the full URLs.
        $config['url']['base'] = $config['url']['domain'] . '.' . $config['url']['tld'];
        $config['url']['full'] = $config['url']['sub'] . '.' . $config['url']['base'];
        foreach ($config['url']['subdomains'] as &$s) {
            $s .= ($s ? '.' : '') . $config['url']['base'];
        }
        unset($s);
        foreach ($config['content']['header']['no_logo_link'] as &$d) {
            $d = $config['dirs'][$d];
        }
        unset($d);

        // Sub-domain specific processing.
        foreach (array_keys($subdomains) as $s) {
            $config['url']['subdomains'][$s] = ($subdomains[$s] ? $subdomains[$s] . '.' : '') . $config['url']['base'];
            // Load the config.
            $sub_file = $config['dirs']['base'] . "/$s/config/$s.php";
            if (!file_exists($sub_file)) {
                $sub_file = $config['dirs']['base'] . "/$s/config/debear.php";
                if (!file_exists($sub_file)) {
                    continue;
                }
            }
            $config[$s] = require $sub_file;
            // Update revision number and datestamp - if set.
            if (isset($config[$s]['version'])) {
                $v = Arrays::merge($config['version'], $config[$s]['version']);
                $rev_file = $config['dirs']['base'] . "/$s/VERSION_REV";
                if (file_exists($rev_file)) {
                    $rev = trim(file_get_contents($rev_file));
                    if ($rev) {
                        list($rev_num, $rev_date) = array_pad(explode('//', $rev), 2, false);
                        $v['breakdown']['rev'] = ($rev_num ? Strings::encodeInput($rev_num) : 0);
                        $v['updated'] = date('jS F Y', $rev_date ?: filemtime($rev_file));
                    }
                }
                $v['label']['short'] = $v['breakdown']['major'] . '.' . $v['breakdown']['minor'];
                $v['label']['full'] = $v['label']['short']
                    . ($v['breakdown']['rev'] ? '.' . $v['breakdown']['rev'] : '');
                $config[$s]['version'] = $v;
            }
            // Any local event config for this subsite?
            if (!HTTP::isTest() || $s == env('APP_SUBDOMAIN')) {
                $evt = $config['dirs']['base'] . "/$s/config/events.php";
                $config['events'] = Arrays::merge($config['events'], file_exists($evt) ? include $evt : []);
            }
            // Any environment / CI-specific config to load (either-or, not both)?
            if (static::$loadCustomConfig) {
                $path = $config['dirs']['base'] . "/$s/config/" . (!HTTP::isTest() ? '_env' : '_ci') . '.php';
                $config[$s] = Arrays::merge($config[$s], file_exists($path) ? include $path : []);
            }
        }

        // Finally, perform any dynamic processing.
        $config_dyn = static::dynamicallyProcessCache(['debear' => $config]);
        $config = $config_dyn['debear'];

        // Return.
        return $config;
    }

    /**
     * Due to config caching, some config options need to be dynamicised here
     * @param array $config The original, cached, configuration.
     * @return array The dynamically processed configuration array
     * @throws ConfigException When unable to determine where this code is running.
     */
    public static function dynamicallyProcessCache(array $config): array
    {
        $is_artisan = isset($_SERVER['PHP_SELF'])
            && is_string($_SERVER['PHP_SELF'])
            && substr($_SERVER['PHP_SELF'], -7) == 'artisan';

        // Skip if we're running via a console command.
        if (
            $is_artisan
            && isset($_SERVER['argv'])
            && is_array($_SERVER['argv'])
            && sizeof($_SERVER['argv']) > 1
            && $_SERVER['argv'][1] == 'config:cache'
        ) {
            return $config;
        }

        // Appropriate Subdomain (which is env() compatible, but not at the time the config is built).
        $from_env = env('APP_SUBDOMAIN');
        if (
            !isset($from_env)
            || !$from_env
            || (HTTP::isTest() && isset($config['force-subdomain']) && $config['force-subdomain'])
        ) {
            if (isset($_SERVER['HTTP_HOST'])) {
                $host_split = explode('.', $_SERVER['HTTP_HOST']);
                $config['debear']['url']['sub'] = $config['debear']['url']['subraw'] = $host_split[0];
                if (!isset($config['debear']['url']['subdomains'][$config['debear']['url']['sub']])) {
                    $config['debear']['url']['sub'] = 'www';
                    // Though consider debear.tld is a valid synonym for www.debear.tld.
                    if ($config['debear']['url']['subraw'] == 'debear') {
                        $config['debear']['url']['subraw'] = $config['debear']['url']['sub'];
                    }
                }
            } elseif ($is_artisan) {
                $config['debear']['url']['sub'] = $config['debear']['url']['subraw'] = 'www';
            } else {
                throw new ConfigException('Unable to determine sub-domain');
            }
        } else {
            $config['debear']['url']['sub'] = $config['debear']['url']['subraw'] = $from_env;
        }
        $config['debear']['dirs']['app'] .= '/' . $config['debear']['url']['sub'];
        // Server Port?
        if (isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] != 443) {
            $config['debear']['url']['port'] = ':' . $_SERVER['SERVER_PORT'];
        }
        // Accessed via HTTPS?
        if (!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] != 'on') {
            $config['debear']['url']['is_https'] = false;
        }
        // Minifying CSS/JS?
        if (isset($_GET['merge_jscss']) && $_GET['merge_jscss']) {
            $config['debear']['resources']['css_merge'] = $config['debear']['resources']['js_merge'] = true;
        }
        // Homepage to be linked on footer?
        if (isset($_SERVER['REQUEST_URI']) && (strtok($_SERVER['REQUEST_URI'], '?') != '/')) {
            $config['debear']['links']['home']['footer']['enabled'] = true;
        }
        // CSP nonce to be randomised.
        $config['debear']['headers']['csp']['nonce'] = str_shuffle(rtrim(base64_encode(str_shuffle(uniqid())), '='));

        // Flag that we've run, and return.
        return $config;
    }

    /**
     * Load the config for the site, and if applicable, subsite, on top of our other config
     * @param string|null $subsite An optional specific subsite whose config should be loaded.
     * @return void
     */
    public static function loadSite(?string $subsite = null): void
    {
        // If we've already processed, no need to continue (ensure we only make changes once).
        if (isset($_SERVER['REQUEST_ROUTE'])) {
            return;
        }
        // Main site config.
        $sub_config = FrameworkConfig::get('debear.' . ($subsite ?? FrameworkConfig::get('debear.url.sub')));
        if (is_array($sub_config)) {
            FrameworkConfig::set(['debear' => Arrays::merge(FrameworkConfig::get('debear'), $sub_config)]);
        }

        // Then check for subsite config.
        $_SERVER['REQUEST_ROUTE'] = strtok($_SERVER['REQUEST_URI'] ?? '/', '?');
        $route_parts = explode('/', $_SERVER['REQUEST_ROUTE']);
        $subsites = FrameworkConfig::get('debear.subsites');
        if (is_array($route_parts) && count($route_parts) && isset($subsites)) {
            $section = $route_parts[1] == 'api' && isset($route_parts[2]) ? $route_parts[2] : $route_parts[1];
            // As the subsite key is not necessarily the value used in the URL, build an appropriate mapping.
            array_walk($subsites, fn(&$v, $k) => $v = ($v['url'] ?? $k));
            $subsites = array_flip($subsites);
            if (isset($subsites[$section])) {
                $section_config = FrameworkConfig::get("debear.subsites.{$subsites[$section]}");
                // If the site is enabled, merge in the config.
                if (isset($section_config['enabled']) && $section_config['enabled']) {
                    unset($section_config['enabled']);
                    FrameworkConfig::set(['debear' => Arrays::merge(FrameworkConfig::get('debear'), $section_config)]);
                }
                // But always try and set the section info.
                FrameworkConfig::set([
                    'debear.section.endpoint' => $section,
                    'debear.section.code' => $subsites[$section],
                ]);
            }
        }
    }

    /**
     * Enable loading of per-subsite custom configuration files
     * @return void
     */
    public static function enableCustomConfig(): void
    {
        static::$loadCustomConfig = true;
    }

    /**
     * Disable loading of per-subsite custom configuration files
     * @return void
     */
    public static function disableCustomConfig(): void
    {
        static::$loadCustomConfig = false;
    }

    /**
     * Return the original, pre-processed, config from disk
     * @return array
     */
    public static function getRawConfig(): array
    {
        return static::$rawConfig;
    }
}
