<?php

namespace DeBear\Helpers;

class Blade
{
    /**
     * Render a Blade template to an internal variable, rather than to browser
     * @param string $view Name of the Blade template to render.
     * @param array  $data Data to pass the Blade template.
     * @return string The rendered Blade template
     */
    public static function render(string $view, array $data = []): string
    {
        return view($view, $data)->render();
    }
}
