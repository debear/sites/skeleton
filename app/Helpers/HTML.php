<?php

namespace DeBear\Helpers;

use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\CookiePreferences;
use DeBear\Models\Skeleton\User;

class HTML
{
    /**
     * Flag that a page is to be a pop-up window, with minimal chrome
     * @return void
     */
    public static function setIsPopup(): void
    {
        FrameworkConfig::set(['debear.content.is_popup' => true]);
    }

    /**
     * Get whether or not the page should be considered a pop-up window, with minimal chrome
     * @return boolean
     */
    public static function isPopup(): bool
    {
        return FrameworkConfig::get('debear.content.is_popup');
    }

    /**
     * Convert raw breadcrumbs from the URL into a display version
     * @param array $titles The raw breadcrumbs.
     * @return array The breadcrumbs parsed into display words
     */
    public static function formatBreadcrumbs(array $titles): array
    {
        $char = ' ';
        $fmt_titles = [];
        $num_titles = count($titles);
        for ($i = 0; $i < $num_titles; $i++) {
            $titles_split = explode($char, preg_replace('/[_\-]/', $char, $titles[$i]));
            $titles[$i] = '';
            foreach ($titles_split as $title_part) {
                $titles[$i] .= ucfirst($title_part) . $char;
            }
            $fmt_titles[] = substr($titles[$i], 0, -1);
        }

        return $fmt_titles;
    }

    /**
     * Add a new section title for the header
     * @param string|array $title The title to add.
     * @return void
     */
    public static function setPageTitle(string|array $title): void
    {
        // Convert a string argument into an array we can process.
        if (!is_array($title)) {
            $title = [$title];
        }
        // Write-back, including unsetting a previously set title string.
        FrameworkConfig::set([
            'debear.content.title.raw' => array_merge(FrameworkConfig::get('debear.content.title.raw'), $title),
            'debear.content.title.combined_markup' => '',
            'debear.content.title.combined_text' => '',
        ]);
    }

    /**
     * Get the page title as a string with potential display markup
     * @return string The "in-page" of the page title
     */
    public static function getPageTitle(): string
    {
        return static::getPageTitleWorker(true);
    }

    /**
     * Get the page title as a text string, without any display markup
     * @return string The "text-only" page title
     */
    public static function getPageTitleText(): string
    {
        return static::getPageTitleWorker(false);
    }

    /**
     * Get the page title as a string, either with or without any display markup
     * @param boolean $markup A flag indicating whether we can include display markup within the string.
     * @return string The page title, appropriately styled according to $markup
     */
    protected static function getPageTitleWorker(bool $markup): string
    {
        $key = 'combined_' . ($markup ? 'markup' : 'text');

        // Build the title string if we haven't already.
        if (!FrameworkConfig::get("debear.content.title.$key")) {
            $raw = array_filter(array_merge(
                array_unique([FrameworkConfig::get('debear.names.site'), FrameworkConfig::get('debear.names.section')]),
                FrameworkConfig::get('debear.content.title.raw')
            ));
            if ($markup) {
                $title = '<li>' . join('</li><li>', $raw) . '</li>';
            } else {
                $title = join(' &raquo; ', $raw);
            }
            FrameworkConfig::set(["debear.content.title.$key" => $title]);
        }

        return FrameworkConfig::get("debear.content.title.$key");
    }

    /**
     * State the page we should flag as the current page within the navigation
     * @param string $url The URL which matches our Sitemap option.
     * @return void
     */
    public static function setNavCurrent(string $url): void
    {
        FrameworkConfig::set(['debear.content.header.nav' => $url]);
    }

    /**
     * Get the URL we have flagged as being the current Navigation option
     * @return string The Sitemap URL option
     */
    public static function getNavCurrent(): string
    {
        return FrameworkConfig::get('debear.content.header.nav');
    }

    /**
     * Set the page's meta description via an absolute string
     * @param string $descrip The meta description for the page.
     * @param array  $data    Data to pass the Interpolator if we are loading via list. (Optional).
     * @return void
     */
    public static function setMetaDescription(string $descrip, array $data = []): void
    {
        FrameworkConfig::set(['debear.content.descrip' => Interpolate::string($descrip, $data)]);
    }

    /**
     * Set the page's meta description via the configured list of links
     * @param string $code Reference to a link in the site list.
     * @param array  $data Data to pass the Interpolator if we are loading via list. (Optional).
     * @return void
     */
    public static function linkMetaDescription(string $code, array $data = []): void
    {
        $sitemap = Sitemap::findSitemapElement($code);
        if (isset($sitemap['descrip']) && is_string($sitemap['descrip']) && $sitemap['descrip']) {
            static::setMetaDescription($sitemap['descrip'], $data);
        }
    }

    /**
     * Get the page's meta description tag value, which could come from a few sources
     * @return string The meta description for the page
     */
    public static function getMetaDescription(): string
    {
        return FrameworkConfig::get('debear.content.descrip') ?: static::getPageTitleText();
    }

    /**
     * Whether we show the meta info in the page or not
     * @return boolean That we should show the various meta tags or not
     */
    public static function showSocialMeta(): bool
    {
        return FrameworkConfig::get('debear.meta.social.show');
    }

    /**
     * Update the Twitter card type for the page
     * @param string $twitter_type The new card type for the page.
     * @return void
     */
    public static function setMetaTwitterCard(string $twitter_type): void
    {
        FrameworkConfig::set(['debear.meta.twitter.card' => $twitter_type]);
    }

    /**
     * Return the Twitter card type for the page
     * @return string The page's card type
     */
    public static function getMetaTwitterCard(): string
    {
        return FrameworkConfig::get('debear.meta.twitter.card');
    }

    /**
     * Update the Open Graph type for the page
     * @param string $og_type The new open graph type for the page.
     * @return void
     */
    public static function setMetaOpenGraphType(string $og_type): void
    {
        FrameworkConfig::set(['debear.meta.og.type' => $og_type]);
    }

    /**
     * Return the Open Graph type for the page
     * @return string The page's type
     */
    public static function getMetaOpenGraphType(): string
    {
        return FrameworkConfig::get('debear.meta.og.type');
    }

    /**
     * Update the generic meta image for the image
     * @param string $img The link to the image to use on the filesystem.
     * @return void
     */
    public static function setMetaImage(string $img): void
    {
        FrameworkConfig::set(['debear.meta.image.main' => $img]);
    }

    /**
     * Return the image as on filesystem for the generic meta image
     * @return string Filesystem path for the image
     */
    public static function getMetaImage(): string
    {
        return FrameworkConfig::get('debear.meta.image.main');
    }

    /**
     * Update the Twitter meta image for the image
     * @param string $img The link to the image to use on the filesystem.
     * @return void
     */
    public static function setMetaImageTwitter(string $img): void
    {
        FrameworkConfig::set(['debear.meta.image.twitter' => $img]);
    }

    /**
     * Return the image as on filesystem for the Twitter meta image
     * @return string Filesystem path for the image
     */
    public static function getMetaImageTwitter(): string
    {
        $img = FrameworkConfig::get('debear.meta.image.twitter') ?: static::getMetaImage();
        return 'https:' . (substr($img, 0, 2) == '//' ? $img : HTTP::buildStaticURLs('images', $img));
    }

    /**
     * Update the Open Graph meta image for the image
     * @param string $img The link to the image to use on the filesystem.
     * @return void
     */
    public static function setMetaImageOpenGraph(string $img): void
    {
        FrameworkConfig::set(['debear.meta.image.og' => $img]);
    }

    /**
     * Return the image as on filesystem for the Open Graph meta image
     * @return string Filesystem path for the image
     */
    public static function getMetaImageOpenGraph(): string
    {
        $img = FrameworkConfig::get('debear.meta.image.og') ?: static::getMetaImage();
        return 'https:' . (substr($img, 0, 2) == '//' ? $img : HTTP::buildStaticURLs('images', $img));
    }

    /**
     * Update the Open Graph meta image sizes
     * @param integer $width  The width, in pixels, of the meta image.
     * @param integer $height The height, in pixels, of the meta image.
     * @return void
     */
    public static function setMetaImageOpenGraphSize(int $width, int $height): void
    {
        FrameworkConfig::set([
            'debear.meta.image.width' => $width,
            'debear.meta.image.height' => $height,
        ]);
    }

    /**
     * Return the width of the Open Graph meta image
     * @return integer The width, in pixels, of the meta image
     */
    public static function getMetaImageOpenGraphWidth(): int
    {
        return FrameworkConfig::get('debear.meta.image.width');
    }

    /**
     * Return the height of the Open Graph meta image
     * @return integer The height, in pixels, of the meta image
     */
    public static function getMetaImageOpenGraphHeight(): int
    {
        return FrameworkConfig::get('debear.meta.image.height');
    }

    /**
     * Update the meta theme colour for the page
     * @param string $colour The colour, in hex, for the page theme.
     * @return void
     */
    public static function setMetaThemeColour(string $colour): void
    {
        FrameworkConfig::set(['debear.meta.theme_colour' => $colour]);
    }

    /**
     * Return the meta theme colour for the page
     * @return string The colour, in hex, for the page theme
     */
    public static function getMetaThemeColour(): string
    {
        return FrameworkConfig::get('debear.meta.theme_colour');
    }

    /**
     * Initial processing for GA determination
     * @return void
     */
    public static function setupAnalytics(): void
    {
        // Determine the urchins available to us.
        $urchins_raw = FrameworkConfig::get('debear.analytics.urchins.' . (HTTP::isLive() ? 'tracking' : 'testing'));
        $urchins = array_filter($urchins_raw, function ($u) {
            return (FrameworkConfig::get('debear.analytics.enabled.v3') && substr($u, 0, 3) == 'UA-')
                || (FrameworkConfig::get('debear.analytics.enabled.v4') && substr($u, 0, 2) == 'G-');
        });
        // Write back as our final urchin list.
        FrameworkConfig::set([
            'debear.analytics.urchins.raw' => array_unique($urchins_raw),
            'debear.analytics.urchins.tracking' => array_unique($urchins),
        ]);

        // Decide if we're going to run analytics.
        $key = 'debear.analytics.enabled';
        $enabled = FrameworkConfig::get("$key.v3") || FrameworkConfig::get("$key.v4");
        $configured = count($urchins);
        $track = $enabled && $configured;

        // Add analytics code if enabled (which needs to be at the top of the list...).
        if ($track) {
            Resources::addJS('skel/analytics.js', ['prepend' => true]);
        }
    }

    /**
     * Get the GA urchins to embed on the page
     * @return array The list of urchins
     */
    public static function getAnalyticsUrchin(): array
    {
        return array_unique(FrameworkConfig::get('debear.analytics.urchins.tracking'));
    }

    /**
     * Get the intro text to appear in the header bar by any links
     * @return string The intro text
     */
    public static function getHeaderIntro(): string
    {
        // In some instances, we may not have a user object yet (such as during an early exception).
        $user = User::object();
        return 'Welcome, ' . ($user?->name ?? 'Guest');
    }

    /**
     * Get the list of links that should appear in the user section of the header
     * @return array The list of links to display
     */
    public static function getHeaderLinks(): array
    {
        if (!FrameworkConfig::get('debear.content.header.links-proc')) {
            $links = Sitemap::processLinks('header', FrameworkConfig::get('debear.links'));
            FrameworkConfig::set([
                'debear.content.header.links-proc' => true,
                'debear.content.header.links' => $links,
            ]);
        } else {
            $links = FrameworkConfig::get('debear.content.header.links');
        }
        return $links;
    }

    /**
     * Get the list of links that should appear in the footer
     * @return array The list of links to display
     */
    public static function getFooterLinks(): array
    {
        if (!FrameworkConfig::get('debear.content.footer.links-proc')) {
            $links = FrameworkConfig::get('debear.links');
            // Add any automatic?
            if (!HTTP::isLive() || Policies::match('user:superadmin')) {
                $links['debug'] = [
                    'title' => 'View Debug Information',
                    'label' => 'Show Debug',
                    'order' => 99,
                    'footer' => true,
                ];
            }
            // Perform the processing.
            $links = Sitemap::processLinks('footer', $links);
            FrameworkConfig::set([
                'debear.content.footer.links-proc' => true,
                'debear.content.footer.links' => $links,
            ]);
        } else {
            $links = FrameworkConfig::get('debear.content.footer.links');
        }
        return $links;
    }

    /**
     * The list of additional blocks to display on the footer
     * @return array The list of additional blocks defined in the config
     */
    public static function getFooterBlocks(): array
    {
        return FrameworkConfig::get('debear.content.footer.blocks');
    }

    /**
     * Determine the "size" of the footer (in terms of custom blocks)
     * @return integer Number of custom blocks being included in the footer
     */
    public static function getFooterSize(): int
    {
        return count(static::getFooterBlocks()) + (int)!static::firstCookieWarning();
    }

    /**
     * Indicate whether we're displaying the cookie warning
     * @return boolean Cookie warning needs to be displayed
     */
    public static function firstCookieWarning(): bool
    {
        return CookiePreferences::get('first-load');
    }

    /**
     * Update the link used within the header image
     * @param string $url The URL to link to.
     * @return void
     */
    public static function setHeaderLink(string $url): void
    {
        FrameworkConfig::set(['debear.content.header.url' => $url]);
    }

    /**
     * Mark the fact we do not want a link in the header image
     * @return void
     */
    public static function noHeaderLink(): void
    {
        FrameworkConfig::set(['debear.content.header.url' => false]);
    }

    /**
     * Determine the URL we should link to in the header
     * @return string|boolean The URL to link to, or the boolean false to indicate no link required
     */
    public static function getHeaderLink(): string|bool
    {
        $url = FrameworkConfig::get('debear.content.header.url');
        if ($url === '') {
            // If we're not passed a specific URL, build an obvious one.
            // Are we within a site section?
            $section = FrameworkConfig::get('debear.section.endpoint');
            if ($section) {
                // Yes, so the section root is the link, unless we've disabled header links for this section.
                if (!in_array($section, FrameworkConfig::get('debear.content.header.no_logo_link'))) {
                    $url = "/$section";
                }
            } else {
                // No, it's the homepage.
                $url = '/';
            }
        }
        return $url;
    }

    /**
     * Add a custom class to the list of classes we add to the body
     * @param string|array $class The class(es) to add.
     * @return void
     */
    public static function addBodyClass(string|array $class): void
    {
        // Convert to a consistent format.
        if (!is_array($class)) {
            $class = [$class];
        }
        // Then add.
        FrameworkConfig::set(['debear.content.body_classes' => array_unique(array_merge(
            FrameworkConfig::get('debear.content.body_classes'),
            $class
        ))]);
    }

    /**
     * Return any custom CSS we want applied to the <body tag
     * @return string The class markup (including tag)
     */
    public static function getBodyClass(): string
    {
        $classes = [];

        // Automated classes.
        $section = FrameworkConfig::get('debear.section.code');
        if ($section) {
            $classes[] = "section-$section";
        }

        // Custom classes?
        $classes = array_merge($classes, FrameworkConfig::get('debear.content.body_classes'));

        // Now return.
        return count($classes) ? 'class="' . preg_replace('/ +/', ' ', join(' ', $classes)) . '"' : '';
    }

    /**
     * Generate a full email address for use in markup
     * @param string         $addr The config option for the address to send.
     * @param string|boolean $text Text to display in the link, or the boolean false to render the address.
     * @return string The rendered email in markup
     */
    public static function buildEmailMarkup(string $addr, string|bool $text = false): string
    {
        // Determine what address we're builing.
        if (strpos($addr, '@') === false) {
            // An internal address, from config.
            $address = FrameworkConfig::get("debear.email.addresses.$addr")
                ?: FrameworkConfig::get('debear.email.addresses.' . FrameworkConfig::get('debear.email.default'));
            $domain = FrameworkConfig::get('debear.url.base');
        } else {
            // An absolute address.
            list($address, $domain) = explode('@', $addr);
        }
        // Default a display text.
        if (!$text) {
            $text = "<emnom>$address</emnom><emdom>$domain</emdom>";
        }
        // Generate the markup.
        $id = 'pem-' . Strings::md5Code($address . '-' . rand(1000, 9999), 12);
        $text = "<a id=\"$id\" data-emnom=\"$address\" data-emdom=\"$domain\">$text</a>";
        return $text;
    }

    /**
     * Generate a full email address (not for use in markup due to scraping...)
     * @param string $addr The config option for the address to send.
     * @return string The constructred email address
     */
    public static function buildEmailRaw(string $addr): string
    {
        return (FrameworkConfig::get("debear.email.addresses.$addr")
            ?: FrameworkConfig::get('debear.email.addresses.' . FrameworkConfig::get('debear.email.default')))
            . '@' . FrameworkConfig::get('debear.url.base');
    }
}
