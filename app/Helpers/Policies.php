<?php

namespace DeBear\Helpers;

use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Support\Facades\Session as FrameworkSession;
use DeBear\Exceptions\PoliciesException;
use DeBear\Models\Skeleton\User as UserModel;
use DeBear\Repositories\Logging;

class Policies
{
    /**
     * Internal mapping check to ensure each policy code is defined and used only once
     * @var array
     */
    protected static $codes;
    /**
     * The policy mapping
     * @var array
     */
    protected static $policies;
    /**
     * Where in the session we will get / store the result
     * @var string
     */
    protected static $sessionKey = 'user.policies';

    /**
     * Prepare the policies
     * @param UserModel $user The user we are running policies against (which could be unset).
     * @return void
     */
    public static function setup(?UserModel $user): void
    {
        // Is there a previous version stored in the session?
        if (FrameworkSession::has(static::$sessionKey)) {
            static::$policies = FrameworkSession::get(static::$sessionKey);
        } else {
            static::$policies = [];
        }

        // Loop through the list and calculate for each level.
        if (isset($user)) {
            static::$codes = [];
            static::setupWorker($user, static::getConfig());
        } else {
            Logging::add(
                'Policies',
                'Setting up policies before the User object was setup. Setting up fallback policies instead.'
            );
            static::setupFallbackWorker(static::getConfig());
        }

        // Then cache in the session.
        session([static::$sessionKey => static::$policies]);
    }

    /**
     * Recalculate the policies should they be modified mid-flow
     * @param UserModel $user The user we are running policies against.
     * @return void
     */
    public static function recalc(?UserModel $user): void
    {
        static::reset();
        // And recalculcate.
        static::setup($user);
    }

    /**
     * Get the raw policy config
     * @return array The policy as a recursive array
     */
    protected static function getConfig(): array
    {
        return FrameworkConfig::get('debear.policies');
    }

    /**
     * Worker method to loop through a level of the policies config
     * @param UserModel $user   The user we are running policies against.
     * @param array     $config The policy config to process.
     * @param boolean   $parent The parent node's status that we inherit.
     * @return void
     * @throws PoliciesException When there is an invalid policy configuration.
     * @throws PoliciesException When a rule has insufficient definition logic.
     */
    protected static function setupWorker(UserModel $user, array $config, bool $parent = true): void
    {
        foreach ($config as $key => $policy) {
            // Test: All unsetting of a policy by removing the array of config, so skip if the value not an array.
            if (!is_array($policy)) {
                continue;
            }

            // Catch: Each key must be unique, so fail if we have already processed this array.
            if (isset(static::$codes[$key])) {
                throw new PoliciesException("Policy Setup Failed: Duplicate config key '$key' found.");
            }
            static::$codes[$key] = true;

            // What components do we have?
            $has_rules = isset($policy['rules']) && is_array($policy['rules']) && sizeof($policy['rules']);
            $has_children = isset($policy['children']) && is_array($policy['children']) && sizeof($policy['children']);

            // Must have one or the other...
            if (!$has_rules && !$has_children) {
                throw new PoliciesException("Policy Setup Failed: Policy '$key' has neither rules nor children.");
            }

            // Perform the logic? (Either because we haven't done so before, or we re-calc per request).
            $missing_policy = !isset(static::$policies[$key]);
            $always_check = isset($policy['check_per_request']) && $policy['check_per_request'];
            if ($has_rules && ($missing_policy || $always_check)) {
                static::$policies[$key] = $parent && static::parseRule($user, $key, $policy['rules']);
            }

            // Any children to run on too?
            if ($has_children) {
                // Process.
                static::setupWorker($user, $policy['children'], $has_rules ? static::$policies[$key] : $parent);

                // If we have no logic, then our state is dependent on the children.
                if (!$has_rules) {
                    $child_values = array_values(array_intersect_key(static::$policies, $policy['children']));
                    if (isset($policy['grouping']) && $policy['grouping']) {
                        // If we're just a grouping, then mark accordingly.
                        static::$policies[$key] = null;
                    } elseif (isset($policy['all-children']) && $policy['all-children']) {
                        // Must match all child rules?
                        static::$policies[$key] = (sizeof(array_unique($child_values)) == 1) && $child_values[0];
                    } else {
                        // Must match >= 1?
                        static::$policies[$key] = (bool)sizeof(array_filter($child_values));
                    }
                }
            }
        }
    }

    /**
     * Worker method to loop through a level of the fallback policies config
     * @param array $config The policy config to process.
     * @return void
     */
    protected static function setupFallbackWorker(array $config): void
    {
        foreach ($config as $key => $policy) {
            if (is_array($policy)) {
                // Set the fallback policy, as defined in config.
                static::$policies[$key] = isset($policy['fallback']) && $policy['fallback'];
                // Now check any children.
                if (isset($policy['children']) && is_array($policy['children']) && sizeof($policy['children'])) {
                    static::setupFallbackWorker($policy['children']);
                }
            }
        }
    }

    /**
     * Parse an individual policy rule
     * @param UserModel $user   The user we are running policies against.
     * @param string    $policy Key for the policy we are parsing.
     * @param array     $rules  The array of rule components to check.
     * @return boolean Whether the individual policy should be considered passed or not
     * @throws PoliciesException When a rule cannot be parsed.
     * @throws PoliciesException When a parsed method is not correctly defined.
     * @throws PoliciesException When a parsed user property is not correctly defined.
     * @throws PoliciesException When a parsed user property uses an unexpected operator.
     */
    protected static function parseRule(UserModel $user, string $policy, array $rules): bool
    {
        foreach ($rules as $i => $rule) {
            // What are we processing?
            $valid = true;
            if (substr($rule[0], 0, 1) == '@') {
                // Passing to a method to handle the processing.
                // Rule needs to have three components.
                if (sizeof($rule) != 3) {
                    throw new PoliciesException(
                        "Policy Parsing Failed: $policy\[$i\]: Method has an invalid definition"
                    );
                }
                list($params, $comparison) = array_slice($rule, 1);
                $callback = explode('::', ltrim($rule[0], '@'));
                // Now run, and compare.
                $valid = (call_user_func_array($callback, $params) === $comparison);
            } else {
                // A feature of the user.
                // Rule needs to have three components.
                if (sizeof($rule) != 3) {
                    throw new PoliciesException(
                        "Policy Parsing Failed: $policy\[$i\]: User property has an invalid definition"
                    );
                }
                list($col, $op, $comparison) = $rule;

                $value = $user->$col;
                switch ($op) {
                    case 'SET':
                        $isset = ($value !== false);
                        $valid = ($isset === $comparison);
                        break;

                    case '=':
                        $valid = ($value === $comparison);
                        break;

                    case '!=':
                    case '<>':
                        $valid = ($value !== $comparison);
                        break;

                    case '>':
                        $valid = ($value > $comparison);
                        break;

                    case '<':
                        $valid = ($value < $comparison);
                        break;

                    case '>=':
                        $valid = ($value >= $comparison);
                        break;

                    case '<=':
                        $valid = ($value <= $comparison);
                        break;

                    case 'IN':
                        if (!is_array($comparison)) {
                            $comparison = explode(',', $comparison);
                        }
                        $valid = in_array($value, $comparison);
                        break;

                    default:
                        throw new PoliciesException("Policy Parsing Failed: $policy\[$i\]: Unknown operator '$op'");
                }
            }

            // If we failed this component, no point in carrying on - the whole rule should be considered failed.
            if (!$valid) {
                return false;
            }
        }

        // If all components passed, the whole rule passed.
        return true;
    }

    /**
     * Matching method for the user against the passed policy list
     * @param array|string $list The policy/ies to check.
     * @param boolean      $all  Whether all policies should match, or at least one. (Optional, Default: false).
     * @return boolean Whether the user matches the appropriate policy/ies
     */
    public static function match(array|string $list, bool $all = false): bool
    {
        // Have we prepared the policies?
        if (!isset(static::$policies)) {
            static::setup(UserModel::object());
        }

        // Ensure the argument is an array.
        if (!is_array($list)) {
            $list = [$list];
        }

        // Run through the list (using proof-by-contradiction).
        foreach ($list as $policy) {
            $matched = isset(static::$policies[$policy]) && static::$policies[$policy];
            if (!$all && $matched) {
                // Any and one matched: Pass.
                return true;
            } elseif ($all && !$matched) {
                // All and on failed: Fail.
                return false;
            }
        }
        // No contraditions found to our $all flag.
        return $all;
    }

    /**
     * Matching wrapper to ensure the user has at least one policy in the list
     * @param array $list The list of policies to check.
     * @return boolean Whether the user has at least one of the policies in the given list
     */
    public static function matchAny(array $list): bool
    {
        return static::match($list, false);
    }

    /**
     * Matching wrapper to ensure the user has every policy in the list
     * @param array $list The list of policies to check.
     * @return boolean Whether the user has all the policies in the given list
     */
    public static function matchAll(array $list): bool
    {
        return static::match($list, true);
    }

    /**
     * Visually dump the hierarchic tree of policies and whether or not they apply to the user
     * @return array Debug information about the policies
     */
    public static function getDebug(): array
    {
        return [
            'policies' => static::getConfig(),
            'matches' => static::$policies,
        ];
    }

    /**
     * Reset the policies between unit tests
     * @return void
     */
    public static function reset(): void
    {
        // Clear what we have in the session.
        FrameworkSession::forget(static::$sessionKey);

        if (HTTP::isTest()) {
            static::$policies = null;
        }
    }
}
