<?php

namespace DeBear\Helpers;

class TransientStorage
{
    /**
     * Add a value to our transient session storage
     * @param string $key   The reference within our storage to store the value.
     * @param mixed  $value The value to be stored.
     * @return void
     */
    public static function set(string $key, mixed $value): void
    {
        session(["transient.$key" => $value]);
    }

    /**
     * Retrieve a value from our transient session storage
     * @param string $key The reference within our storage to retrieve a value.
     * @return mixed The value in transient storage for the given key
     */
    public static function get(string $key): mixed
    {
        return session("transient.$key");
    }

    /**
     * Remove a single value from our transient session storage
     * @param string $key The reference within our storage to be removed.
     * @return void
     */
    public static function unset(string $key): void
    {
        session()->forget("transient.$key");
    }
}
