<?php

namespace DeBear\Helpers;

use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Repositories\Logging;
use DeBear\Exceptions\Resources\MissingException;

class Merge
{
    /**
     * Merging a list of JS files into a single part-minified version
     * @param array $list List of files to merge.
     * @return string Relative URI of the merged JS files
     */
    public static function JS(array $list): string
    {
        return static::perform($list, 'js');
    }

    /**
     * Merging a list of CSS files into a single part-minified version
     * @param array $list List of files to merge.
     * @return string Relative URI of the merged CSS files
     */
    public static function CSS(array $list): string
    {
        return static::perform($list, 'css');
    }

    /**
     * Worker method for merging the CSS/JS
     * @param array  $list List of files to merge.
     * @param string $ext  Type of merge to be performed.
     * @return string Relative URI of the merged files
     */
    protected static function perform(array $list, string $ext): string
    {
        // Pre-process the list, adding the dir name for non-skel resources.
        $list = Resources::convert($list);

        // Determine the filesystem location.
        $dir_type = ($ext == 'js' ? 'javascript' : 'stylesheets');
        $fs_dir = resource_path(FrameworkConfig::get("debear.dirs.$dir_type")) . '/';

        // Get the collective name for these files.
        $merge_dir = FrameworkConfig::get('debear.dirs.app') . "/resources/$ext/"
            . FrameworkConfig::get('debear.dirs.merges') . '/';
        // During CI jobs, where these files aren't called by the browser, write files to a pruned location.
        if (HTTP::isTest()) {
            $merge_dir = env('APP_DIR_CI_MERGES') . "/$ext/";
        }
        $ret_file = static::getListMergeName($list, $fs_dir, $ext) . '.' . $ext;

        // If this merge file does not exist, create it.
        static::process($list, $fs_dir, $merge_dir . $ret_file, $ext);

        // Return the name.
        return FrameworkConfig::get('debear.dirs.merges') . '/' . $ret_file;
    }

    /**
     * Create the merge file if it does not exist
     * @param array  $list     List of files to merge.
     * @param string $fs_dir   Prefix of the files on the filesystem.
     * @param string $ret_file Filename to load/save of the merged files.
     * @param string $ext      Type of merge to be performed.
     * @return void
     */
    public static function process(array $list, string $fs_dir, string $ret_file, string $ext): void
    {
        // If we're compressing, add the appropriate suffix to the file we're generating / testing.
        $is_gzip = FrameworkConfig::get('debear.resources.gzip_merge');
        $ret_file .= ($is_gzip ? '.gz' : '');

        // If we've already produced the file, touch it to bump the modification file for pruning purposes.
        if (file_exists($ret_file)) {
            touch($ret_file);
            return;
        }

        // Establish the minify method.
        $minify = 'minify' . strtoupper($ext);

        // Loop through each of the files, adding to the merge file.
        $content = '';
        foreach ($list as $f) {
            // Get the contents of this file, minify and add to our content.
            $content .= static::$minify(file_get_contents("$fs_dir$f")) . "\n";
        }

        // Save to file, potentially compressed.
        file_put_contents($ret_file, $is_gzip ? gzencode($content) : $content);
    }

    /*
     * Internal methods
     */
    /**
     * Establish the file name of the merged documents
     * @param array  $list   List of files to merge. (Pass-by-reference).
     * @param string $fs_dir Prefix of the files on the filesystem.
     * @param string $ext    Type of merge to be performed.
     * @return string Unique name for the list of files based on content
     * @throws MissingException When passed an unknown input type.
     */
    protected static function getListMergeName(array &$list, string $fs_dir, string $ext): string
    {
        // Loop through the list, getting file mod time and add to the list to be md5'd.
        $list_md5 = array();
        foreach ($list as $i => $f) {
            // Check that the file actually exists.
            $file = "$fs_dir$f";
            if (!file_exists($file)) {
                // Log that it does not.
                $extDisp = strtoupper($ext);
                Logging::add("Minify $extDisp", "Unable to find $extDisp file '$file'");
                // Highlight to the user when in development (or unit testing...).
                if (!HTTP::isLive() && FrameworkConfig::get('debear.resources.missing-exception')) {
                    throw new MissingException("Unable to find $extDisp file '$file'");
                }
                // In all other instances, strip and move on.
                unset($list[$i]);
                $list = array_values($list);
                continue;
            }
            // Process as normal.
            $stat = stat($file);
            array_push($list_md5, $f . '@' . $stat['mtime']);
        }
        // The use of MD5 is not for cryptographic purposes, so consider the SemGrep warning a false positive.
        // nosemgrep PHPCS_SecurityAudit.BadFunctions.CryptoFunctions.WarnCryptoFunc.
        return md5(join('::', $list_md5));
    }

    /*
     * Minifying methods
     */
    /**
     * Perform the minification of JS using a third party library
     * @param string $js Full string of JavaScript.
     * @return string The minified JS
     */
    protected static function minifyJS(string $js): string
    {
        return \JShrink\Minifier::minify($js, array('flaggedComments' => false));
    }

    /**
     * Perform the minification of CSS using internal CSS parsing
     * @param string $css Full string of CSS.
     * @return string The minified CSS
     */
    protected static function minifyCSS(string $css): string
    {
        // Comments.
        $css = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $css);
        // Tabs, spaces, newlines, etc.
        $css = str_replace(array("\r\n","\r","\n","\t",'  ','    ','     '), '', $css);
        // Other spaces before/after semi-colon.
        $css = preg_replace(array('(( )+{)','({( )+)'), '{', $css);
        $css = preg_replace(array('(( )+})','(}( )+)','(;( )*})'), '}', $css);
        $css = preg_replace(array('(;( )+)','(( )+;)'), ';', $css);

        return $css;
    }
}
