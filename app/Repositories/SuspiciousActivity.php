<?php

namespace DeBear\Repositories;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Support\Facades\Request;
use DeBear\Helpers\SessionAggregation;
use DeBear\Models\Skeleton\User;
use DeBear\Models\Skeleton\UserActivityLog;
use DeBear\Repositories\Time;

class SuspiciousActivity
{
    /**
     * The list of violations
     * @var array
     */
    protected $violations = [];

    /**
     * Check if we've already reached violation threshold
     * @param string $type   The category of suspicious activity.
     * @param array  $detail A blob of relevant information that will be JSON encoded on failure.
     * @return boolean Boolean indicating whether the check failed and the action was not blocked
     */
    public function allowed(string $type, array $detail = []): bool
    {
        $errs = [
            'user_id' => 'Suspicious activity found by user_id',
            'session_id' => 'Suspicious activity found by session',
            'remote_ip' => 'Suspicious activity found by IP address',
        ];
        $fail_reason = $this->allowedWorker($type, $detail['user_id'] ?? false);
        if ($fail_reason && isset($errs[$fail_reason])) {
            // We came across an error.
            $this->record($type, $errs[$fail_reason], true, $detail);
            return false;
        }
        // All good then!
        return true;
    }

    /**
     * Worker method for checking suspicious activity
     * @param string         $type    The category of suspicious activity.
     * @param string|boolean $user_id User ID to check instead of the current user, or bool of false to indicate
     * the current user.
     * @return string|boolean The string explaining the reason a check failed, or a bool of false indicating no error
     */
    protected function allowedWorker(string $type, string|bool $user_id = false): string|bool
    {
        // Get the numbers.
        if (!isset($this->violations[$type])) {
            $this->violations[$type] = [];
            $limits = FrameworkConfig::get('debear.security.activity_limits');
            $check_time = App::make(Time::class)->adjustFormatServer("-{$limits[$type]['period']} minutes");
            // Convert our User ID from string to numeric ID.
            $user_id = $user_id ?: User::object()->id;
            if (is_string($user_id)) {
                $user = User::where('user_id', '=', $user_id)->first();
                $user_id = $user?->id;
            }
            // The checks to run.
            $checks = [
                'user_id' => $user_id,
                'session_id' => SessionAggregation::getID(),
                'remote_ip' => Request::server('REMOTE_ADDR'),
            ];
            foreach ($checks as $check => $value) {
                if ($value) {
                    $this->violations[$type][$check] = UserActivityLog::where([
                        ['type', '=', FrameworkConfig::get("debear.logging.user_activity.$type")],
                        [$check, '=', $value],
                        ['activity_time', '>=', $check_time],
                    ])->count();
                }
            }
        }
        // Process.
        foreach (array_reverse($this->violations[$type]) as $check => $value) {
            if ($value >= $limits[$type][$check]) {
                return $check;
            }
        }
        return false;
    }


    /**
     * Record an instance of potentially suspicious activity
     * @param string  $type    The category of suspicious activity.
     * @param string  $summary A summary explanation as to the status of the suspicious activity.
     * @param boolean $blocked Flag as to whether the request was blocked or not.
     * @param array   $detail  A blob of relevant information that will be JSON encoded.
     * @return void
     */
    public function record(string $type, string $summary, bool $blocked, array $detail): void
    {
        UserActivityLog::record($type, $summary, array_merge($detail ?? [], ['blocked' => $blocked]));
    }
}
