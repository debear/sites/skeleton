<?php

namespace DeBear\Repositories;

use Illuminate\Support\Facades\App;
use DeBear\Helpers\Arrays;

/**
 * Internal caching of data (to prevent multiple lookups)
 */
class InternalCache
{
    /**
     * Cache list
     * @var array
     */
    protected $cache = [];
    /**
     * Internal reference to the singleton
     * @var self
     */
    protected static $obj = null;
    /**
     * Cache entries to preserve when emptying the cache
     * @var array
     */
    protected array $preserve_on_empty = [
        'php://input' => true,
    ];

    /**
     * Get item(s) from the cache
     * @param string $object Object we want the data from.
     * @param mixed  $ids    ID(s) to be accessed from the cache; false to return whole object.
     * @return mixed  The cached object(s)
     */
    public function get(string $object, mixed $ids = false): mixed
    {
        // First, make sure we have it...
        if (!$this->cacheKeyExists($object)) {
            return false;
        }

        // If we're after the whole object, just return it.
        if ($ids === false) {
            return $this->cache[$object];
        }

        // Then get what we have keeping track of whether we received a scalar or array as this affects the return type.
        $is_scalar = is_scalar($ids);
        if ($is_scalar) {
            $ids = [$ids];
        }
        $ret = array_intersect_key($this->cache[$object], array_flip($ids));
        return $is_scalar ? $ret[$ids[0]] : $ret;
    }

    /**
     * Save (over-writing) items to the cache
     * @param string $object Object the data came from.
     * @param mixed  $data   Data being added to the cache.
     * @return void
     */
    public function set(string $object, mixed $data): void
    {
        $this->cache[$object] = $data;
    }

    /**
     * Remove an complete item from the cache
     * @param string $object Object to be removed.
     * @return void
     */
    public function unset(string $object): void
    {
        unset($this->cache[$object]);
    }

    /**
     * Add data to the cache when passing in an ID based array
     * @param string $object Object the data came from.
     * @param string $id_col Column to identify the unique key for each object.
     * @param mixed  $data   Data being added to the cache.
     * @return void
     */
    public function setByID(string $object, string $id_col, mixed $data): void
    {
        // Create the object array if it doesn't already exists...
        if (!$this->cacheKeyExists($object)) {
            $this->cache[$object] = [];
        }

        // Convert a single object into a parseable array.
        if (!is_array($data)) {
            $data = [$data];
        }

        // Add our items.
        foreach ($data as $d) {
            $this->cache[$object][$d->{$id_col}] = $d;
        }
    }

    /**
     * Append items to the cache
     * @param string $object Object the data came from.
     * @param mixed  $data   Data being added to the cache.
     * @return void
     */
    public function append(string $object, mixed $data): void
    {
        // Convert a single object into a parseable array.
        if (!is_array($data)) {
            $data = [$data];
        }

        $this->merge($object, $data);
    }

    /**
     * Merge items into the cache
     * @param string $object Object the data came from.
     * @param mixed  $data   Data being added to the cache.
     * @return void
     */
    public function merge(string $object, mixed $data): void
    {
        // Create the object array if it doesn't already exists...
        if (!$this->cacheKeyExists($object)) {
            $this->cache[$object] = $data;
            return;
        }

        $this->cache[$object] = Arrays::merge($this->cache[$object], $data);
    }

    /**
     * Determine which (if any) of a given list of IDs have yet to be stored in the cache
     * @param string $object Object to check.
     * @param mixed  $ids    ID(s) to check.
     * @return mixed ID(s) that are not stored in the cache
     */
    public function determineMissing(string $object, mixed $ids): mixed
    {
        // Easy check: if we have no key for this object, we have no data.
        if (!$this->cacheKeyExists($object)) {
            return $ids;
        }

        // Convert scalar input into an array for parsing.
        if (is_scalar($ids)) {
            $ids = [$ids];
        }

        // Process as an array lookup.
        return array_diff_key(array_flip($ids), $this->cache[$object]);
    }

    /**
     * Check if we have already created an object within the cache
     * @param string $object Key to check.
     * @return boolean Indicating if we have or not
     */
    public function cacheKeyExists(string $object): bool
    {
        return isset($this->cache[$object]);
    }

    /**
     * Empty the contents of the cache
     * @param array $preserve Optional additions to the cache keys to be preserved.
     * @return void
     */
    public function empty(array $preserve = []): void
    {
        $preserve = array_filter(array_merge($this->preserve_on_empty, $preserve));
        $this->cache = array_intersect_key($this->cache, $preserve);
    }

    /******************
     * Static Methods *
     ******************/
    /**
     * Get the instantiated singleton from within a static call.
     * @return self The instantiated singleton
     */
    public static function object(): self
    {
        if (!isset(static::$obj)) {
            static::$obj = App::make(self::class);
        }
        return static::$obj;
    }
}
