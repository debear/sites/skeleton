<?php

namespace DeBear\Repositories;

use DeBear\Helpers\Arrays;
use DeBear\Helpers\Blade;
use DeBear\Helpers\Resources;
use DeBear\Http\Modules\Forms;
use DeBear\Repositories\Dropdown\Item as DropdownItem;
use DeBear\Repositories\Dropdown\Search as DropdownSearch;

class Dropdown
{
    /**
     * Unique identifier for the dropdown
     * @var string
     */
    protected $id;
    /**
     * Configuration option for the dropdown
     * @var object
     */
    protected $opt;
    /**
     * Current / Default value
     * @var mixed
     */
    protected $value;
    /**
     * Relevant Forms instance object
     * @var Forms
     */
    protected $form;
    /**
     * Dropdown element fine tuning
     * @var array
     */
    protected $config = [
        'value' => '',
        'select' => '-- Please Select --',
        'form' => null,
        'label' => 'label',
        'required' => false,
        'disabled' => false,
    ];
    /**
     * ID to Option mapping
     * @var array
     */
    protected $mappings;

    /**
     * Setup the dropdown instance
     * @param string       $id     DOM ID of the element we are creating.
     * @param array|object $opt    List of options, accepted as either an array or object.
     * @param array        $config Display configuration (Optional).
     */
    public function __construct(string $id, array|object $opt, array $config = [])
    {
        /* Literal copies of args */
        $this->id = $id;
        $this->config = Arrays::merge($this->config, $config);
        $this->value = $this->config['value'];
        $this->form = $this->config['form'];
        $opt_obj = []; // An array version of our options we will later convert to an object.

        /* Process the items into a standard object */
        $this->mappings = ['__null' => 0];
        if ($this->config['select']) {
            $opt_obj[] = new DropdownItem((object)[
                'id' => '',
                $this->config['label'] => $this->config['select'],
                'classes' => 'fixed-search',
            ], $this);
        }

        // Searchable tem?
        if (isset($this->config['searchable']) && $this->config['searchable']) {
            $opt_obj[] = new DropdownSearch([], $this);
        }

        // Build the item list.
        foreach ($opt as $item) {
            // Allow simple passing of scalar options.
            if (is_scalar($item)) {
                $item = ['id' => $item, 'label' => $item];
            }
            // Enforce the item being an object.
            if (!is_object($item)) {
                $item = (object)$item;
            }
            // ID mapping?
            if (isset($item->id)) {
                $this->mappings[$item->id] = sizeof($opt_obj);
            }
            // Add.
            $opt_obj[] = new DropdownItem($item, $this);
        }
        // Convert the items into the iterable object.
        $this->opt = (object)$opt_obj;
    }

    /**
     * Render the dropdown as markup
     * @return string Markup of the resulting dropdown
     */
    public function render(): string
    {
        Resources::addCSS('skel/widgets/dropdown.css');
        Resources::addJS('skel/widgets/dropdown.js');
        return Blade::render('skeleton.widgets.dropdown', ['dropdown' => $this]);
    }

    /**
     * Markup to use as the "header" representing the current selection
     * @return string Label to use for the header
     */
    public function header(): string
    {
        $key = $this->mappings[$this->getValue() === '' ? '__null' : $this->getValue()];
        return $this->opt->$key->label;
    }

    /* Getters */

    /**
     * Get the ID of the widget
     * @return string The widget's ID
     */
    public function getID(): string
    {
        return $this->id;
    }

    /**
     * Get the value of the dropdown
     * @return string The current dropdown's value
     */
    public function getValue(): string
    {
        return $this->value ?? '';
    }

    /**
     * Access to the form processing object
     * @return ?Forms The form object
     */
    public function getForm(): ?Forms
    {
        return $this->form;
    }

    /**
     * Process the tab index for the element... if we have a form object to do it on
     * @return string The tab index to use on the element (if there is one)
     */
    public function getTabIndex(): string
    {
        return ($this->getForm() !== null ? $this->getForm()->addTabIndex() : '');
    }

    /**
     * Get the available options for the dropdown
     * @return object Iterable object of options for the dropdown
     */
    public function getOptions(): object
    {
        return $this->opt;
    }

    /**
     * Get the available configuration option for the dropdown
     * @param string $name The name of the requested config option.
     * @return mixed The appropriate value for the requested config option
     */
    public function getConfig(string $name): mixed
    {
        return $this->config[$name] ?? false;
    }

    /**
     * Get the string of CSS classes to use on the widget
     * @return string The widget's passed CSS classes as a space-separated string
     */
    public function getClasses(): string
    {
        return isset($this->config['classes']) ? join(' ', $this->config['classes']) : '';
    }

    /* Render Attributes */

    /**
     * Render the data attribute that indicates if the dropdown is a required field on the form
     * @return string The attribute to be include on the element
     */
    public function renderRequired(): string
    {
        return ($this->config['required'] ? 'data-required="true"' : '');
    }

    /**
     * Render the data attribute that indicates if the dropdown is a disabled field
     * @return string The attribute to be include on the element
     */
    public function renderDisabled(): string
    {
        return ($this->config['disabled'] ? 'data-disabled="true"' : '');
    }
}
