<?php

namespace DeBear\Repositories;

use stdClass;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;

class Timezones
{
    /**
     * Whether we group timezones by country
     * @var boolean
     */
    protected $group_by_country;
    /**
     * Whether we return the list sorted by distance from the user's IP
     * @var boolean
     */
    protected $sort_by_geoip;
    /**
     * Timezone list
     * @var array
     */
    protected $tz;

    /**
     * Constructor
     * @param array $opt Configuration options.
     */
    public function __construct(array $opt = [])
    {
        $this->group_by_country = !isset($opt['no_group']) || !$opt['no_group'];
        $this->sort_by_geoip = isset($opt['geoip']) && $opt['geoip'];
    }

    /**
     * Getter method for the list of timezones
     * @return array The appropriate list of timezones
     */
    public function getTimezones(): array
    {
        if (!isset($this->tz)) {
            $this->process();
        }
        return $this->tz;
    }

    /**
     * Internal processing to get the list of timezones
     * @return void
     */
    protected function process(): void
    {
        $tz_all = DB::table('SETUP_COUNTRY_TZ')
            ->join('SETUP_COUNTRY', 'SETUP_COUNTRY.code2', '=', 'SETUP_COUNTRY_TZ.code2')
            ->select(
                'SETUP_COUNTRY_TZ.timezone',
                DB::raw('REPLACE(SUBSTRING_INDEX(SETUP_COUNTRY_TZ.timezone, "/", -1), "_", " ") AS city'),
                'SETUP_COUNTRY.name AS country',
                'SETUP_COUNTRY.code2 AS country_code',
                'SETUP_COUNTRY_TZ.geo_lat',
                'SETUP_COUNTRY_TZ.geo_lng',
                'SETUP_COUNTRY_TZ.population'
            )
            ->get();

        $this->tz = [];
        if (!$this->sort_by_geoip) {
            $this->processList($tz_all);
        } else {
            $this->processGeoIP($tz_all);
        }
    }

    /**
     * Get data by GeoIP
     * @param Collection $tz_all The list of timezones to analyse.
     * @return void
     */
    protected function processGeoIP(Collection $tz_all): void
    {
        /* Get the info */
        $tz_list = [ 'all' => [], 'by_country' => [], 'by_distance' => [] ];
        foreach ($tz_all as $tz) {
            $tz->distance = App::make(GeoIP::class)->calculateDistance($tz->geo_lat, $tz->geo_lng);
            $tz_list['all'][] = $tz;

            if (!isset($tz_list['by_country'][$tz->country_code])) {
                $tz_list['by_country'][$tz->country_code] = [];
            }
            $tz_list['by_country'][$tz->country_code][] = $tz;

            if (
                !isset($tz_list['by_distance'][$tz->country_code])
                || $tz_list['by_distance'][$tz->country_code] > $tz->distance
            ) {
                $tz_list['by_distance'][$tz->country_code] = $tz->distance;
            }
        }

        /* Then process to be returned */
        if ($this->group_by_country) {
            // By country.
            natsort($tz_list['by_distance']); // Sort first.
            foreach (array_keys($tz_list['by_distance']) as $country) {
                if (sizeof($tz_list['by_country'][$country]) > 1) {
                    usort($tz_list['by_country'][$country], [ $this, 'processSortCity' ]);
                }
                $this->tz[$country] = $tz_list['by_country'][$country];
            }
        } else {
            // By list.
            usort($tz_list['all'], [ $this, 'processGeoIPSort' ]);
            $this->tz = $tz_list['all'];
        }
    }

    /**
     * Function for sorting by timezone distance
     * @param stdClass $a Timezone A.
     * @param stdClass $b Timezone B.
     * @return integer Comparison result
     */
    protected function processGeoIPSort(stdClass $a, stdClass $b): int
    {
        return $a->distance - $b->distance;
    }

    /**
     * Get full list
     * @param Collection $tz_all The list of timezones to analyse.
     * @return void
     */
    protected function processList(Collection $tz_all): void
    {
        // Country by Country, then Timezones.
        if ($this->group_by_country) {
            $by_country = [];
            foreach ($tz_all as $tz) {
                if (!isset($by_country[$tz->country_code]) || !is_array($by_country[$tz->country_code])) {
                    $by_country[$tz->country_code] = [
                        'country' => $tz->country,
                        'code' => $tz->country_code,
                        'list' => [],
                    ];
                }
                $by_country[$tz->country_code]['list'][] = $tz;
            }
            foreach (array_keys($by_country) as $country_code) {
                usort($by_country[$country_code]['list'], [ $this, 'processSortCity' ]);
            }
            usort($by_country, [ $this, 'processListSortCountry' ]);
            $this->tz = $by_country;
        } else {
            // Just the list.
            $tz_all->sort(function ($a, $b) {
                return $this->processSortCity($a, $b);
            });
            $this->tz = $tz_all->toArray();
        }
    }

    /**
     * Function for sorting countries alphabetically
     * @param array $a Timezone A.
     * @param array $b Timezone B.
     * @return integer Comparison result
     */
    protected function processListSortCountry(array $a, array $b): int
    {
        return strcmp($a['country'], $b['country']);
    }

    /**
     * Function for sorting cities alphabetically
     * @param stdClass $a Timezone A.
     * @param stdClass $b Timezone B.
     * @return integer Comparison result
     */
    protected function processSortCity(stdClass $a, stdClass $b): int
    {
        return $b->population - $a->population;
    }
}
