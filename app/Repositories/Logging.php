<?php

namespace DeBear\Repositories;

use DateTime;
use DateTimeZone;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Support\Facades\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use DeBear\Helpers\Benchmark;
use DeBear\Helpers\Browser;
use DeBear\Helpers\HTTP;

class Logging
{
    /**
     * Boolean as to whether or not we log
     * @var boolean
     */
    protected static $enabled = true;
    /**
     * Script start/end/run time
     * @var array
     */
    protected static $script;
    /**
     * Information about the remote client
     * @var array
     */
    protected static $remote;
    /**
     * Internal logging
     * @var array
     */
    protected static $log;

    /**
     * Constructor
     * @return void
     */
    public static function setup(): void
    {
        // Initial logging details.
        static::$script = [
            'pid' => getmypid(),
            'start' => microtime(true),
            'end' => false,
            'run_time' => false,
        ];
        static::$remote = [
            'ip' => Request::server('REMOTE_ADDR'),
            'method' => Request::server('REQUEST_METHOD'),
            'protocol' => 'http' . (FrameworkConfig::get('debear.url.is_https') ? 's' : ''),
            'uri' => Request::server('HTTP_HOST') . Request::server('REQUEST_URI'),
            'user_agent' => Request::server('HTTP_USER_AGENT'),
            'referer' => HTTP::isTest() ? 'Test Referer' : Request::server('HTTP_REFERER'),
        ];

        // Empty.
        static::$log = [];
    }

    /**
     * Close the log and write to disk
     * @param Response|JsonResponse|RedirectResponse|null $response An optinal response sent to the browser, containing
     * the last info to be logged.
     * @return void
     */
    public static function close(Response|JsonResponse|RedirectResponse|null $response = null): void
    {
        // Get script end time and calculate execution time.
        static::$script['end'] = microtime(true);
        static::$script['run_time'] = Benchmark::getScriptRunTime();
        static::$script['http_code'] = $response?->getStatusCode() ?? http_response_code();
        static::$script['content_length'] = strlen($response?->getContent() ?? '' ?: '');

        // Save, if we have something _to_ save.
        // Current logging could be disabled, but we may have logging notes from the first part to save.
        // Disabling only stops storing of messages, not saving of previous logs.
        if (static::isEnabled() || static::getSize()) {
            static::save();
        }
    }

    /**
     * Add a message to the log
     * @param string $type Message class.
     * @param string $msg  The message to log.
     * @return void
     */
    public static function add(string $type, string $msg): void
    {
        if (!static::isEnabled()) {
            return;
        }
        static::$log[] = ['time' => microtime(true), 'type' => $type, 'msg'  => $msg];
    }

    /***********
     * Logging *
     ***********/
    /**
     * Turn on logging
     * @return void
     */
    public static function enable(): void
    {
        static::$enabled = true;
    }

    /**
     * Turn off logging
     * @return void
     */
    public static function disable(): void
    {
        static::$enabled = false;
    }

    /**
     * Check if logging enabled
     * @return boolean Whether the logging module is enabled
     */
    protected static function isEnabled(): bool
    {
        return static::$enabled;
    }

    /***************
     * Log getters *
     ***************/
    /**
     * Return the log list
     * @return array The log list
     */
    public static function get(): array
    {
        return static::$log;
    }

    /**
     * Return the number of elements in the log
     * @return integer The number of elements in the log
     */
    protected static function getSize(): int
    {
        return sizeof(static::$log);
    }

    /**********************
     * Filesystem actions *
     **********************/
    /**
     * Save the logging info to file
     * @return void
     */
    protected static function save(): void
    {
        // Open file for logging.
        $date = (new DateTime('now', (new DateTimeZone(FrameworkConfig::get('app.timezone')))))->format('Y-m-d');
        $path = join('/', [
            FrameworkConfig::get('debear.dirs.logs'),
            FrameworkConfig::get('debear.url.sub'),
            FrameworkConfig::get('debear.dirs.applogs'),
            "$date.log.gz"
        ]);
        if (!HTTP::isLive() && env('APP_DIR_CI_LOGS') !== null) {
            // Use our separate location during CI jobs.
            $path = str_replace(
                '.log',
                "{$date}-" . FrameworkConfig::get('debear.dirs.applogs') . '.log.gz',
                env('APP_DIR_CI_LOGS')
            );
        }
        $file = new File($path, true);

        // Log basic script info.
        $file->write(static::write(
            static::$script['start'],
            static::$remote['method'] . ' ' . static::$remote['protocol'] . '://' . static::$remote['uri']
        ));
        $misc = [
            'IP: ' . (static::$remote['ip'] ?? '(Unknown)'),
            'User Agent: ' . (static::$remote['user_agent'] ?? '(Unknown)'),
            'Duration: ' . (static::$script['run_time'] >= 0
                ? sprintf('%.05f', static::$script['run_time'])
                : '(Unknown)'),
            'HTTP Code: ' . (static::$script['http_code'] ?: '(Unknown)'),
            'Content Length: ' . (static::$script['content_length'] ?: '(Unknown)'),
        ];
        $file->write(static::write(static::$script['start'], join('; ', $misc)));
        if (isset(static::$remote['referer']) && is_string(static::$remote['referer']) && static::$remote['referer']) {
            $file->write(static::write(static::$script['start'], 'Referer: ' . static::$remote['referer']));
        }

        // Log internal messages, if there are any.
        foreach (static::$log as $log) {
            $file->write(static::write($log['time'], $log['msg'], $log['type']));
        }

        // Close the file.
        $file->save('a');
    }

    /**
     * Format the logging line for writing
     * @param float  $time Current timestamp.
     * @param string $msg  The log message.
     * @param mixed  $type A category the log message may relate to.
     * @return string The line to be written to file
     */
    protected static function write(float $time, string $msg, mixed $type = null): string
    {
        // Due to an issue with DateTime and integer timestamps (w/no decimal part), determine if that applies here.
        $has_decimal = ($time != floor($time));
        $create_fmt = ($has_decimal ? 'U.u' : 'U');
        $create_time = sprintf($has_decimal ? '%f' : '%d', $time);
        // Render the timestamped log line.
        return sprintf(
            '{%d} [%s]%s %s',
            static::$script['pid'],
            DateTime::createFromFormat($create_fmt, $create_time)
                ->setTimezone(new DateTimeZone(FrameworkConfig::get('app.timezone')))
                ->format('Y-m-d H:i:s.v'),
            isset($type) ? ' (' . $type . ')' : '',
            $msg
        );
    }

    /*****************
     * Report Errors *
     *****************/
    /**
     * Store Report URI instances to the log file
     * @param string $mode   The type of report we are storing (CSP, PHP, etc).
     * @param array  $custom Custom arguments to store against the instance.
     * @return void
     */
    public static function report(string $mode, ?array $custom = null): void
    {
        $ret = [
            '_SERVER' => array_filter(
                Request::server(),
                function ($k) {
                    // Skip database settings or those that may refer to a password.
                    return (substr($k, 0, 3) != 'DB_') && (stripos($k, 'password') === false);
                },
                ARRAY_FILTER_USE_KEY
            ),
            '_BROWSER' => [
                'browser' => Browser::get()->browser,
                'version' => Browser::get()->version,
                'os' => Browser::get()->platform,
            ],
            '_COOKIE' => $_COOKIE,
            '_SESSION' => session()->all(),
            '_GET' => $_GET,
            '_POST' => $_POST,
            'php://input' => InternalCache::object()->get('php://input'),
            '_REV' => [
                'v' => FrameworkConfig::get('debear.version.label.short'),
                'rev' => FrameworkConfig::get('debear.version.breakdown.rev'),
                'rev_site' => false,
                'rev_skel' => false
            ]
        ];
        if (isset($custom) && is_array($custom)) {
            $ret['_REPORT'] = $custom;
        }

        // Split the revision.
        if ($ret['_REV']['rev']) {
            list($ret['_REV']['rev_site'], $ret['_REV']['rev_skel']) = explode(':', $ret['_REV']['rev']);
        }

        // Save to disk.
        $log_dir = FrameworkConfig::get('debear.dirs.logs') . '/' . FrameworkConfig::get('debear.url.sub')
            . '/' . FrameworkConfig::get('debear.dirs.report-uri') . strtolower($mode);
        file_exists($log_dir) || mkdir($log_dir, 0775, true);
        $file_name = sprintf(
            '%s/%s.%d.json.gz',
            $log_dir,
            date('Y-m-d.H-i-s'),
            getmypid()
        );
        $file = new File($file_name, true);
        $file->write(json_encode($ret));
        $file->save();
        static::add($mode, 'Writing ' . $mode . ' report to file \'' . $file_name . '\'');
    }

    /**
     * Reset the internal variables between CI tests
     * @return void
     */
    public static function reset(): void
    {
        if (HTTP::isTest()) {
            static::$enabled = true;
            static::$script = null;
            static::$remote = null;
            static::$log = null;
        }
    }
}
