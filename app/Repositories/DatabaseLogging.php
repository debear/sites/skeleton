<?php

namespace DeBear\Repositories;

use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use DeBear\Helpers\Benchmark;
use DeBear\Helpers\HTTP;
use DeBear\Helpers\Strings;

class DatabaseLogging
{
    /**
     * Whether logging has been enabled
     * @var boolean
     */
    protected static $enabled;
    /**
     * Extended logging configuration
     * @var array
     */
    protected static $extended;
    /**
     * List of database connections defined / used
     * @var array
     */
    protected static $conn_list;
    /**
     * The query log for requests through our ORM (rather than Eloquent)
     * @var array
     */
    protected static $orm_log;
    /**
     * The built logging report
     * @var array
     */
    protected static $report;

    /**
     * Enable logging within the ORM
     * @return void
     */
    public static function setup(): void
    {
        static::$enabled = FrameworkConfig::get('debear.logging.database.enabled');
        static::$extended = array_merge([
            'build' => count(array_filter(FrameworkConfig::get('debear.logging.database.extended'))),
        ], FrameworkConfig::get('debear.logging.database.extended'));
        static::$conn_list = array_keys(FrameworkConfig::get('database.connections'));
        static::$orm_log = array_fill_keys(static::$conn_list, []);

        if (!static::$enabled) {
            return;
        }

        foreach (static::$conn_list as $conn) {
            DB::connection($conn)->enableQueryLog();
        }
    }

    /**
     * Log the performance of an ORM query (which is not returned by the DB facade)
     * @param string $conn     The connection over which the query was run.
     * @param string $query    The query's SQL.
     * @param array  $bindings The bindings provided to the query.
     * @param float  $time     The benchmark time.
     * @return void
     */
    public static function logORMQuery(string $conn, string $query, array $bindings, float $time): void
    {
        $st = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 3);
        static::$orm_log[$conn][] = [
            'query' => $query,
            'bindings' => $bindings,
            'time' => $time,
            'caller' => "{$st[2]['class']}::{$st[2]['function']}(), Line " . ($st[1]['line'] ?? '-'),
        ];
    }

    /**
     * Build / Get the database reports
     * @return array Per-connection array of query report info
     */
    public static function getReport(): array
    {
        // No reports if not enabled.
        if (!static::$enabled) {
            return [];
        }

        // Previously calculated?
        if (isset(static::$report)) {
            return static::$report;
        }

        // No, so build and then return.
        static::$report = [];
        $is_slow = FrameworkConfig::get('debear.logging.database.is_slow');
        foreach (static::$conn_list as $conn) {
            // Load the log, from both Eloquent and our internal logger.
            $report = array_merge(
                DB::connection($conn)->getQueryLog(),
                static::$orm_log[$conn]
            );
            // Skip if no requests.
            if (!count($report)) {
                continue;
            }
            static::$report[$conn] = [];

            // Create some unique references for each query.
            array_walk($report, function (&$a) {
                $a['query_ref'] = Strings::md5Code($a['query']);
                $a['args_ref'] = Strings::md5Code(json_encode($a['bindings']));
                $a['uniq_ref'] = "{$a['query_ref']}::{$a['args_ref']}";
            });

            /* Connection level */
            $times = array_column($report, 'time');
            $slow = array_filter($times, function ($a) use ($is_slow) {
                return $a >= $is_slow;
            });
            static::$report[$conn]['conn'] = [
                'raw' => $report,
                'query_tot' => count($report),
                'query_uniq' => count(array_unique(array_column($report, 'uniq_ref'))),
                'query_slow' => count($slow),
                'time_tot' => array_sum($times) / 1000, // Reported in milliseconds.
                'time_max' => max($times) / 1000, // Reported in milliseconds.
                'slow' => $slow,
            ];

            /* Per-query level */
            static::$report[$conn]['queries'] = [];
            if (static::$extended['build']) {
                // Log on these unique entries individually.
                foreach (array_unique(array_column($report, 'query_ref')) as $ref) {
                    // Get, re-basing the index.
                    $queries = array_values(array_filter($report, function ($a) use ($ref) {
                        return ($a['query_ref'] == $ref);
                    }));
                    // Process.
                    $times = array_column($queries, 'time');
                    $slow = array_filter($times, function ($a) use ($is_slow) {
                        return $a >= $is_slow;
                    });
                    $args = array_unique(array_column($queries, 'args_ref'));
                    static::$report[$conn]['queries'][$ref] = [
                        'ref' => $ref,
                        'sql' => $queries[0]['query'],
                        'raw' => $queries,
                        'query_tot' => count($queries),
                        'query_args' => count($args),
                        'query_slow' => count($slow),
                        'time_tot' => array_sum($times) / 1000, // Reported in milliseconds.
                        'time_mean' => array_sum($times) / (count($queries) * 1000), // Reported in milliseconds.
                        'time_max' => max($times) / 1000, // Reported in milliseconds.
                        'slow' => $slow,
                    ];
                }
            }
        }

        return static::$report;
    }

    /**
     * Report on the logging output the ORM when winding down
     * @return void
     */
    public static function report(): void
    {
        // Only report if enabled.
        if (!static::$enabled) {
            return;
        }

        // Run.
        foreach (static::getReport() as $conn => $report) {
            static::reportConnection($conn, $report['conn']);
            static::reportQueries($conn, $report['queries']);
        }
    }

    /**
     * Report on our queries at the connection-level
     * @param string $conn  Internal name of the connection being reported on.
     * @param array  $stats The raw list of reported queries.
     * @return void
     */
    protected static function reportConnection(string $conn, array $stats): void
    {
        Logging::add("Database: $conn", sprintf(
            'Queries: %d (Unique: %d, %0.02f%%); Time: %0.03f (Max: %0.03f; Slow: %d)',
            $stats['query_tot'],
            $stats['query_uniq'],
            100 * ($stats['query_uniq'] / $stats['query_tot']),
            $stats['time_tot'],
            $stats['time_max'],
            $stats['query_slow']
        ));
        // Report on any slow queries.
        if (static::enabledSlowLog() && count($stats['slow'])) {
            foreach ($stats['slow'] as $i => $slow) {
                $q = $stats['raw'][$i];
                $caller = ($q['caller'] ?? '(unknown, from DB::conn->getQueryLog())');
                Logging::add("Database: $conn", sprintf('Slow Query: %0.03f - %s', $slow / 1000, $caller));
            }
        }
    }

    /**
     * Report on our queries at the per-query level
     * @param string $conn   Internal name of the connection being reported on.
     * @param array  $report The raw list of reported queries.
     * @return void
     */
    protected static function reportQueries(string $conn, array $report): void
    {
        // Skip if query logging is not enabled.
        if (!static::enabledQueryLog()) {
            return;
        }
        // Run per-query for this connection.
        foreach ($report as $stats) {
            Logging::add("Database: $conn", sprintf(
                'Query: %s; Called: x%d (Unique: %d, %0.02f%%); Time: %0.03f (Max: %0.03f; Slow: %d)',
                $stats['ref'],
                $stats['query_tot'],
                $stats['query_args'],
                100 * ($stats['query_args'] / $stats['query_tot']),
                $stats['time_tot'],
                $stats['time_max'],
                $stats['query_slow']
            ));
        }
    }

    /**
     * Render the runtime information of the request within the response
     * @param Response $response The response about to be rendered (with rendering placeholder).
     * @return void
     */
    public static function render(?Response $response): void
    {
        // Skip if we have no response to process.
        if (!isset($response)) {
            return;
        }

        // Get the content to modify, and process the runtime stats accordingly.
        $content = $response->getContent();
        $script_time = Benchmark::getScriptRunTime();
        foreach (static::getReport() as $conn => $report) {
            if (strpos($content, "<!-- TotalTime: $conn -->") !== false) {
                $time = (!$script_time ? '' : sprintf(
                    ' (%0.03f%% of %0.03fs)',
                    100 * ($report['conn']['time_tot']) / $script_time,
                    $script_time
                ));
                $content = str_replace("<!-- TotalTime: $conn -->", $time, $content);
            }
        }
        $response->setContent($content);
    }

    /**
     * State whether the query log logging mechanism is enabled
     * @return boolean That the query log logging type is enabled or not
     */
    public static function enabledQueryLog(): bool
    {
        return static::enabledOption('query_log');
    }

    /**
     * State whether the slow log logging mechanism is enabled
     * @return boolean That the slow log logging type is enabled or not
     */
    public static function enabledSlowLog(): bool
    {
        return static::enabledOption('slow_log');
    }

    /**
     * State whether the display logging mechanism is enabled
     * @return boolean That the display logging type is enabled or not
     */
    public static function enabledDisplay(): bool
    {
        return static::enabledOption('display');
    }

    /**
     * State whether a particular logging mechanism is enabled
     * @param string $type A type of logging.
     * @return boolean That the requested logging type is enabled or not
     */
    protected static function enabledOption(string $type): bool
    {
        return static::$extended['build'] && isset(static::$extended[$type]) && static::$extended[$type];
    }

    /**
     * Reset the internal variables between CI tests
     * @return void
     */
    public static function reset(): void
    {
        if (HTTP::isTest()) {
            static::$enabled = FrameworkConfig::get('debear.logging.database.enabled');
            static::$extended = null;
            static::$conn_list = null;
            static::$orm_log = null;
            static::$report = null;
        }
    }
}
