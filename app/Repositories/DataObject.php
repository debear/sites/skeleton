<?php

namespace DeBear\Repositories;

class DataObject
{
    /**
     * Our underlying data array
     * @var array
     */
    protected $data;

    /**
     * Constructor to store the object's initial set of data
     * @param array $data The original array of data to be processed as an object.
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    /**
     * Get the value stored in the object, passing null (rather than an Exception) if not found
     * @param string $attribute The name of the requested attribute.
     * @return mixed The value of the requested attribute
     */
    public function __get(string $attribute): mixed
    {
        return $this->data[$attribute] ?? null;
    }

    /**
     * Update the value stored in the object
     * @param string $attribute The name of the requested attribute.
     * @param mixed  $value     The new value to be applied.
     * @return void
     */
    public function __set(string $attribute, mixed $value): void
    {
        $this->data[$attribute] = $value;
    }

    /**
     * Return the object's data as JSON (e.g., for an API)
     * @return array The raw array for JSON rendering
     */
    public function toJSON(): array
    {
        return $this->data;
    }
}
