<?php

namespace DeBear\Repositories;

use DeBear\Helpers\HTTP;

class File
{
    /**
     * Name of the file to open
     * @var string
     */
    protected $file;
    /**
     * Array of lines to write to the file
     * @var array
     */
    protected $write;
    /**
     * Contents read from the file
     * @var string
     */
    protected $read;
    /**
     * Whether we will write compressed text
     * @var boolean
     */
    protected $compressed;

    /**
     * Constructor, setting up meta info about the file
     * @param string  $file     Name of the file (including path) to save.
     * @param boolean $compress Flag to indicate if the file should be ZLib compressed or not.
     */
    public function __construct(string $file, bool $compress = false)
    {
        $this->file = $file;
        $this->write = [];
        $this->read = '';

        // Functions we use depend whether the file will be compressed or not:
        // - true => Use the ZLib functions.
        // - false => Regular filesystem functions will suffice.
        $this->compressed = $compress;
    }

    /**
     * Add extra line to the file - in memory
     * @param string $line Line to be written to the file.
     * @return void
     */
    public function write(string $line): void
    {
        $this->write[] = $line;
    }

    /**
     * Perform the write action
     * @param string $mode Argument to file_put_contents explaining how we write the file to
     * disk (w = write, a = append).
     * @return void
     */
    public function save(string $mode = 'w'): void
    {
        $data = ($this->write ? join("\n", $this->write) . "\n" : '');
        $write = file_put_contents(
            $this->file,
            $this->compressed ? gzencode($data) : $data,
            $mode == 'a' ? FILE_APPEND : 0
        );
        if (!$write) {
            $type = ($this->compressed ? 'compressed file' : 'file');
            $err_all = error_get_last();
            $err_msg = (isset($err_all['message']) ? $err_all['message'] : '');
            $err = "Unable to write to $type {$this->file}: $err_msg";
            !HTTP::isTest() && error_log($err);
        } else {
            // Reset the write array.
            $this->write = [];
        }
    }

    /**
     * Read and return the contents of the file
     * @return string File contents output as a single string
     */
    public function read(): string
    {
        return file_get_contents($this->file);
    }

    /**
     * Read a file from disk as an array, not a string
     * @return array File contents output as an array of lines
     */
    public function readAsArray(): array
    {
        return file($this->file);
    }
}
