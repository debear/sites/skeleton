<?php

namespace DeBear\Repositories;

use Exception;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use MaxMind\Db\Reader;
use DeBear\Helpers\HTTP;
use DeBear\Models\Skeleton\Country;

class GeoIP
{
    /**
     * Instance of the MaxMind Reader
     * @var Reader
     */
    protected $geoip;
    /**
     * Base GeoIP test info
     * @var array
     */
    protected $base;

    /**
     * Constructor with initial setup based on the user's IP address
     */
    public function __construct()
    {
        // Instantiate our GeoIP file reader.
        $mmdb_file = $this->getFilePath();
        $this->geoip = new Reader($mmdb_file);
        // Configure against our current IP address.
        $this->updateIP(Request::server('REMOTE_ADDR'));
    }

    /**
     * Determine where the GeoIP database file is
     * @return string Path to the GeoIP database on the filesystem
     */
    protected function getFilePath(): string
    {
        $mmdb_path = FrameworkConfig::get('debear.dirs.common');
        if (HTTP::isTest()) {
            // Testing specific path.
            $mmdb_path = FrameworkConfig::get('debear.dirs.vendor') . '/debear/setup';
        }
        $mmdb_path .= '/geoip';
        return file_exists($mmdb_path . '/GeoLite2-City.mmdb')
            ? $mmdb_path . '/GeoLite2-City.mmdb'
            : $mmdb_path . '/GeoLite2-Country.mmdb';
    }

    /**
     * Switch the base IP we compare to
     * @param string $ip The IP address we want to analyse.
     * @return void
     */
    public function updateIP(string $ip): void
    {
        // Run the parsing.
        try {
            $info = $this->geoip->get($ip);
        } catch (Exception $e) {
            $info = null;
        }
        if (!is_array($info)) {
            $info = [];
        }

        // Then determine what sort of info we got out.
        $was_match = isset($info['country']);
        $has_location = isset($info['location']);
        $has_country = isset($info['country']);
        $has_city = isset($info['city']);

        // Some defaults.
        if (!$was_match) {
            // No match at all, so create some faux values.
            $defaults = [
                'location' => ['lat' => false, 'lng' => false],
                'continent' => ['code' => 'EU', 'name' => 'Europe'],
                'country' => ['code' => 'GB', 'name' => 'United Kingdom'],
                'city' => ['name' => 'Bath', 'full' => 'Bath'],
            ];
        } else {
            // Get from the country code in the database.
            $country = Country::find(strtolower($info['country']['iso_code']));
            $default_city = $country->timezoneDefault;

            $defaults = [
                'location' => [
                    'lat' => $default_city?->geo_lat ?? false,
                    'lng' => $default_city?->geo_lng ?? false,
                ],
            ];
            if (!$has_city) {
                $city = 'Unknown'; // Default value.
                if (isset($country)) {
                    $city = str_replace('_', ' ', preg_replace('@^.+/([^/]+)$@', '$1', $country->default_timezone));
                }
                $defaults['city'] = ['name' => $city, 'full' => $city];
            }
        }

        // Then build.
        $this->base = [
            'ip' => $ip,
            'lat' => $has_location ? $info['location']['latitude']  : $defaults['location']['lat'],
            'lng' => $has_location ? $info['location']['longitude'] : $defaults['location']['lng'],
            'continent' => [
                'code' => $has_country ? $info['continent']['code']        : $defaults['continent']['code'],
                'name' => $has_country ? $info['continent']['names']['en'] : $defaults['continent']['name'],
            ],
            'country' => [
                'code' => $has_country ? $info['country']['iso_code']    : $defaults['country']['code'],
                'name' => $has_country ? $info['country']['names']['en'] : $defaults['country']['name'],
            ],
            'city' => [
                'name' => $has_city ? $info['city']['names']['en'] : $defaults['city']['name'],
                'full' => $has_city ? $info['city']['names']['en'] : $defaults['city']['full'],
            ],
        ];
        // The full can possibly be broken down further using the 'subdivisions' field.
        if (isset($info['subdivisions']) && is_array($info['subdivisions']) && sizeof($info['subdivisions'])) {
            $full = [$this->base['city']['full']]; // Start with the city we built earlier.
            foreach (array_reverse($info['subdivisions']) as $subdiv) {
                $full[] = $subdiv['names']['en'];
            }
            if (sizeof($full)) {
                $this->base['city']['full'] = join(', ', array_unique($full));
            }
        }

        // Since MaxMind withdrew lat/long support, guess based on the country.
        if (!isset($this->base['lat']) || !$this->base['lat']) {
            // Slight localhost update.
            if ($ip == '127.0.0.1') {
                $this->base['lat'] = 51.5;
                $this->base['lng'] = 0.0;
                return;
            }
            $db_ret = DB::table('SETUP_COUNTRY')
                ->join(
                    'SETUP_COUNTRY_TZ',
                    function ($join) {
                        $join->on('SETUP_COUNTRY_TZ.code2', '=', 'SETUP_COUNTRY.code2')
                            ->on('SETUP_COUNTRY_TZ.timezone', '=', 'SETUP_COUNTRY.default_timezone');
                    }
                )
                ->where('SETUP_COUNTRY.code2', strtolower($this->base['country']['code']))
                ->select('geo_lat', 'geo_lng')
                ->first();
            $this->base['lat'] = $db_ret?->geo_lat ?? 51.5;
            $this->base['lng'] = $db_ret?->geo_lng ?? 0.0;
        }
    }

    /**
     * Calculate the distance from our base IP to the co-ords supplied
     * @param float $lat Latitude co-ordinate.
     * @param float $lng Longitude co-ordinate.
     * @return float Distance to the passed co-ordinate from the user's IP address location
     */
    public function calculateDistance(float $lat, float $lng): float
    {
        // Adapted from Haversine formula at http://www.movable-type.co.uk/scripts/latlong.html.
        $dist_lat = deg2rad($lat - $this->base['lat']);
        $dist_lng = deg2rad($lng - $this->base['lng']);
        $latA = deg2rad($this->base['lat']);
        $latB = deg2rad($lat);

        $a = sin($dist_lat / 2) * sin($dist_lat / 2)
            + sin($dist_lng / 2) * sin($dist_lng / 2) * cos($latA) * cos($latB);
        $c = 2 * atan2(sqrt($a), sqrt(1 - $a));

        return $c * 6371; // 6371 = Earth's radius, in km.
    }

    /**
     * Getters
     */

    /**
     * Get the (approximate!) lat/long associated with the IP address
     * @return array Array containing the lat/lng
     */
    public function getLatLong(): array
    {
        return [
            'lat' => $this->base['lat'],
            'lng' => $this->base['lng'],
        ];
    }

    /**
     * Get the code representing the continent of the IP addresses
     * @return string The IP's continent as a code
     */
    public function getContinent(): string
    {
        return $this->base['continent']['code'];
    }

    /**
     * Get the name representing the continent of the IP addresses
     * @return string The IP's continent
     */
    public function getContinentName(): string
    {
        return $this->base['continent']['name'];
    }

    /**
     * Get the code representing the country of the IP addresses
     * @return string The IP's country as a code
     */
    public function getCountry(): string
    {
        return $this->base['country']['code'];
    }

    /**
     * Get the name representing the country of the IP addresses
     * @return string The IP's country
     */
    public function getCountryName(): string
    {
        return $this->base['country']['name'];
    }

    /**
     * Get the (short) name representing the city of the IP addresses
     * @return string The IP's city
     */
    public function getCity(): string
    {
        return $this->base['city']['name'];
    }

    /**
     * Get the (full) name representing the city of the IP addresses
     * @return string The IP's city full name
     */
    public function getCityFull(): string
    {
        return $this->base['city']['full'];
    }
}
