<?php

namespace DeBear\Repositories;

use Illuminate\Support\Facades\Config as FrameworkConfig;

class SocialMedia
{
    /**
     * The applications to send from
     * @var array
     */
    protected $apps;
    /**
     * Name of the application posting the message
     * @var string
     */
    protected $user_agent;
    /**
     * List of accounts we could send from
     * @var array
     */
    protected $accounts;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->user_agent = FrameworkConfig::get('debear.names.site');
        $this->accounts = [];
        $this->apps = [];
        foreach (['twitter', 'facebook', 'instagram'] as $platform) {
            $this->accounts[$platform] = $this->apps[$platform] = [];
        }
    }

    /**
     * Twitter Setters
     */

    /**
     * Config setter
     * @param array $creds Appropriate credentials for the Twitter app.
     * @return void
     */
    public function setup(array $creds): void
    {
        $this->setTwitterApp($creds['app']);
        foreach (array_diff_key(array_keys($creds), ['app']) as $type) {
            $this->addTwitterAccount($creds[$type], $type);
        }
    }

    /**
     * Add a user account
     * @param string $screen_name The screen name / handle to store.
     * @param string $type        The type of account to register.
     * @return void
     */
    public function addTwitterAccount(string $screen_name, string $type = 'master'): void
    {
        $this->setUserAccount('twitter', $type, ['screen_name' => $screen_name]);
    }

    /**
     * Switch a user account between two types
     * @param string $from The original account.
     * @param string $to   The new account.
     * @return void
     */
    public function switchTwitterAccountType(string $from, string $to): void
    {
        $this->switchUserAccountType('twitter', $from, $to);
    }

    /**
     * Set the code for the twitter app
     * @param string $app The app code.
     * @return void
     */
    public function setTwitterApp(string $app): void
    {
        $this->setApp('twitter', $app);
    }

    /**
     * Twitter Getters
     */

    /**
     * Get info about the twitter app
     * @return string|boolean The app, or false if none set
     */
    public function getTwitterApp(): string|bool
    {
        return $this->getApp('twitter');
    }

    /**
     * Get details about the linked twitter account
     * @param string  $type The type of account to get.
     * @param boolean $full Whether we return full details or just the name.
     * @return string|boolean The account, either the name or full details, or false if none set
     */
    public function getTwitterAccount(string $type, bool $full = false): string|array|bool
    {
        $user = $this->getUserAccount('twitter', $type);
        if (!$user) {
            return false;
        }
        return !$full ? $user['screen_name'] : $user;
    }

    /**
     * Twitter Actions
     */

    /**
     * Send a tweet
     * @param array  $tweet     Tweet details.
     * @param string $tweet_app The app we should use.
     * @param string $send_time When to send the tweet.
     * @return void
     * /
    public function tweet(array &$tweet, string $tweet_app, string $send_time = '+30 minutes'): void
    {
    }
    /* */

    /**
     * Send an email to Support when something goes wrong
     * @param string $err       The error being sent.
     * @param string $tweet_app The app that generated the error.
     * @return void
     * /
    protected function tweetError(string $err, string $tweet_app): void
    {
    }
    /* */

    /**
     * Display a Tweet
     * @param array $tweet The tweet being displayed.
     * @return void
     * /
    public function displayTweet(array $tweet): void
    {
        // Parse some data.
        $tweet_sent = (bool)$tweet['tweet_sent'];
        $send_col = ($tweet_sent ? 'sent' : 'send');
        if ($tweet_sent) {
            $response = json_decode($tweet['twitter_return_response'], true);
            $is_rt = (bool)$response['retweeted'];
            $orig_tweet = ($is_rt ? $response['retweeted_status'] : $response);
            $orig_user = $orig_tweet['user'];
        } else {
            $is_rt = (bool)$tweet['retweet_id'];
        }

        view(
            'skeleton.widgets.social.tweet',
            compact(
                [
                    'tweet',
                    'tweet_sent',
                    'is_rt',
                    'response',
                    'orig_user',
                    'orig_tweet',
                    'send_col',
                ]
            )
        )->header('Content-Type', 'text/html; charset=UTF-8');
    }
    /* */

    /**
     * Process a tweet for display
     * @param array $tweet The tweet details.
     * @return string The visual rendition of the tweet
     * /
    public function displayTweetBody(array $tweet): string
    {
        $components = ['user' => [], 'hashtag' => [], 'link_orig' => [], 'link_full' => []];
        $text = $tweet['text'];

        /* Determine * /
        // Identify users.
        foreach ($tweet['entities']['user_mentions'] as $user) {
            $components['user'][] = substr($text, $user['indices'][0], $user['indices'][1] - $user['indices'][0]);
        }
        // Identify hashtags.
        foreach ($tweet['entities']['hashtags'] as $hashtag) {
            $components['hashtag'][] = substr(
                $text,
                $hashtag['indices'][0],
                $hashtag['indices'][1] - $hashtag['indices'][0]
            );
        }
        // Identify links.
        foreach ($tweet['entities']['urls'] as $link) {
            $components['link_orig'][] = substr($text, $link['indices'][0], $link['indices'][1] - $link['indices'][0]);
            $components['link_full'][] = $link['display_url'];
        }

        /* Update * /
        foreach ($components as &$value) {
            $value = array_values(array_unique($value));
        }
        // Users.
        foreach ($components['user'] as $user) {
            $text = str_replace($user, '<span class="user">' . $user . '</span>', $text);
        }
        // Hashtags.
        foreach ($components['hashtag'] as $hashtag) {
            $text = str_replace($hashtag, '<span class="hashtag">' . $hashtag . '</span>', $text);
        }
        // Links.
        foreach ($components['link_orig'] as $i => $link) {
            $text = str_replace($link, '<span class="link">' . $components['link_full'][$i] . '</span>', $text);
        }

        return $text;
    }
    /* */

    /**
     * Generic Functions
     */

    /**
     * Add a user account
     * @param string $platform The Social Media platform to update.
     * @param string $type     The type of account being set.
     * @param array  $user     Details of the user account to set.
     * @return void
     */
    protected function setUserAccount(string $platform, string $type, array $user): void
    {
        $this->accounts[$platform][$type] = $user;
    }

    /**
     * Switch a user account between two types
     * @param string $platform The Social Media platform to update.
     * @param string $from     The original account.
     * @param string $to       The new account.
     * @return void
     */
    public function switchUserAccountType(string $platform, string $from, string $to): void
    {
        if (!isset($this->accounts[$platform][$from])) {
            return;
        }
        $this->accounts[$platform][$to] = $this->accounts[$platform][$from];
        unset($this->accounts[$platform][$from]);
    }

    /**
     * Add an application
     * @param string $platform The Social Media platform to update.
     * @param string $app      The internal reference of the app to use.
     * @return void
     */
    protected function setApp(string $platform, string $app): void
    {
        $this->apps[$platform] = $app;
    }

    /**
     * Get a user account
     * @param string $platform The Social Media platform to request.
     * @param string $type     The type of account.
     * @return array|boolean The details, or false if none yet set
     */
    protected function getUserAccount(string $platform, string $type): array|bool
    {
        return ($this->accounts[$platform][$type] ?? false);
    }

    /**
     * Get an application
     * @param string $platform The Social Media platform to request.
     * @return string|boolean The app details, or false if none yet set
     */
    protected function getApp(string $platform): string|bool
    {
        return ($this->apps[$platform] ?? false);
    }

    /**
     * Establish the time (in database format) a tweet should be sent
     * Requires: use DeBear\Repositories\Time;
     * @param string $offset Time adjustment to make.
     * @return integer    The time in the server's timezone
     * /
    protected function calculateSendTime(string $offset): int
    {
        // Unlike all othere use-cases, do not use $_DATES['now'] as it could be over-ridden to something in the past,
        // so we use time() to get the current *actual* time.
        // However, this needs to be stored in the server's time, not the app's, so do we need to convert back?
        return Time::formatTime(strtotime($offset));
    }
    /* */

    /**
     * Display footer link
     * @return integer The number of links displayed
     */
    public function displayFooterWidget(): int
    {
        $links = [];
        /* Determine the links to display */
        // Twitter.
        $user = $this->getTwitterAccount('master');
        if ($user) {
            $links['x-twitter'] = ['url' => '//x.com/' . $user, 'text' => '@' . $user];
        }
        /* And display (if there are any...) */
        if (sizeof($links)) {
            print view('skeleton.widgets.social.footer', compact('links'))->render();
        }
        return sizeof($links);
    }
}
