<?php

namespace DeBear\Repositories\Dropdown;

use DeBear\Repositories\Dropdown;
use DeBear\Helpers\Blade;

class Search extends Item
{
    /**
     * Setup the dropdown item instance
     * @param array|object $data     Specifics for the dropdown item.
     * @param Dropdown     $dropdown Parent dropdown object.
     */
    public function __construct(array|object $data, Dropdown $dropdown)
    {
        // The "label" is our specially crafted template.
        parent::__construct([
            $dropdown->getConfig('label') => Blade::render('skeleton.widgets.dropdown.search'),
            'classes' => 'search',
        ], $dropdown);
    }
}
