<?php

namespace DeBear\Repositories\Dropdown;

use Symfony\Component\String\UnicodeString;
use DeBear\Helpers\Strings;
use DeBear\Repositories\Dropdown;

class Item
{
    /**
     * Parent Dropdown object
     * @var Dropdown
     */
    protected $dropdown;
    /**
     * Specifics for the dropdown item
     * @var object
     */
    protected $data;
    /**
     * Column we use for rendering the text value
     * @var string
     */
    protected $label;

    /**
     * Setup the dropdown item instance
     * @param array|object $data     Specifics for the dropdown item.
     * @param Dropdown     $dropdown Parent dropdown object.
     */
    public function __construct(array|object $data, Dropdown $dropdown)
    {
        $this->data = is_object($data) ? $data : (object)$data;
        $this->dropdown = $dropdown;
        $this->label = $this->dropdown->getConfig('label');
    }

    /**
     * Data Getter Magic Method
     * @param string $name Name of the attribute being accessed magically.
     * @return mixed The corresponding value from within our data
     */
    public function __get(string $name): mixed
    {
        // An exception: 'label' may be mapped.
        if ($name == 'label') {
            return $this->data->{$this->label};
        }
        return $this->data->$name ?? null;
    }

    /**
     * Determine the CSS clases we should apply to an item
     * @return string The class HTML attribute
     */
    public function renderClasses(): string
    {
        $classes = isset($this->data->classes) ? (array)$this->data->classes : [];
        if (!isset($this->data->id)) {
            $classes[] = 'optgroup';
        } elseif ($this->data->id == $this->dropdown->getValue()) {
            $classes[] = 'sel';
        }
        return sizeof($classes) ? 'class="' . join(' ', $classes) . '"' : '';
    }

    /**
     * Render the list of data attributes for this item
     * @return string The item's data attributes
     */
    public function renderDataAttributes(): string
    {
        // Prepare the list.
        $data = (array)($this->data->attrib ?? []);
        if (isset($this->data->id)) {
            $data['id'] = $this->data->id;
        }
        // Bespoke: search term (if searching is enabled).
        if (isset($this->data->id) && $this->dropdown->getConfig('searchable')) {
            $data['search'] = $this->data->{$this->label};
            $data['search'] = preg_replace('/<.*?>/', '', $data['search']); // Strip HTML off label.
            $data['search'] = Strings::decodeString($data['search'], false); // Generalise I.
            $data['search'] = (new UnicodeString($data['search']))->ascii(); // Generalise II.
            $data['search'] = strtolower($data['search']); // Standardise case of the label.
        }
        // Process.
        $ret = [];
        foreach ($data as $attrib => $value) {
            $ret[] = "data-$attrib=\"$value\"";
        }
        return join(' ', $ret);
    }
}
