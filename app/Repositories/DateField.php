<?php

namespace DeBear\Repositories;

use DeBear\Helpers\Blade;
use DeBear\Helpers\Resources;
use DeBear\Http\Modules\Forms;

class DateField
{
    /**
     * ID of the Date Field
     * @var string
     */
    protected $id;
    /**
     * Instance of the Forms module
     * @var Forms
     */
    protected $form;
    /**
     * Current form value
     * @var mixed
     */
    protected $value;
    /**
     * Earliest acceptable date
     * @var string
     */
    protected $min;
    /**
     * Latest acceptable date
     * @var string
     */
    protected $max;
    /**
     * That the field must be completed by the user
     * @var boolean
     */
    protected $required;
    /**
     * Dropdown object for our list of months
     * @var Dropdown
     */
    protected $mm_dropdown;

    /**
     * Setup the date field instance
     * @param string $id  DOM ID of the element we are creating.
     * @param array  $opt Control and display configuration.
     */
    public function __construct(string $id, array $opt)
    {
        $this->id = $id;
        $this->form = $opt['form'];
        $this->value = $opt['value'] ?? '';
        $this->min = $opt['min'] ?? date('Y-m-d', strtotime('-1 year'));
        $this->max = $opt['max'] ?? date('Y-m-d', strtotime('+1 year'));
        $this->required = $opt['required'] ?? false;

        $this->mm_dropdown = new Dropdown(
            "{$id}_mm",
            [
                ['id' =>  1, 'label' => 'January'],
                ['id' =>  2, 'label' => 'February'],
                ['id' =>  3, 'label' => 'March'],
                ['id' =>  4, 'label' => 'April'],
                ['id' =>  5, 'label' => 'May'],
                ['id' =>  6, 'label' => 'June'],
                ['id' =>  7, 'label' => 'July'],
                ['id' =>  8, 'label' => 'August'],
                ['id' =>  9, 'label' => 'September'],
                ['id' => 10, 'label' => 'October'],
                ['id' => 11, 'label' => 'November'],
                ['id' => 12, 'label' => 'December'],
            ],
            [
                'form' => $this->form,
                'select' => '-- Month --',
                'required' => $this->required,
                'value' => $this->getValueMM(),
                'classes' => [
                    'date_mm',
                ],
            ]
        );
    }

    /**
     * Render the dropdown as markup
     * @return string Markup of the resulting dropdown
     */
    public function render(): string
    {
        Resources::addCSS('skel/widgets/date_field.css');
        Resources::addJS('skel/widgets/date_field.js');
        return Blade::render('skeleton.widgets.date_field', ['date_field' => $this]);
    }


    /* Getters */

    /**
     * Get the ID of the widget
     * @return string The widget's ID
     */
    public function getID(): string
    {
        return $this->id;
    }

    /**
     * Get the value of the date field
     * @return string The current date field's value
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * Get the day component of the date field's value
     * @return string The current date field's day component
     */
    public function getValueDD(): string
    {
        return $this->value ? ltrim(substr($this->value, 8), '0') : $this->value;
    }

    /**
     * Get the month component of the date field
     * @return string The current date field's month component
     */
    public function getValueMM(): string
    {
        return $this->value ? ltrim(substr($this->value, 5, 2), '0') : $this->value;
    }

    /**
     * Get the year component of the date field
     * @return string The current date field's year component
     */
    public function getValueYY(): string
    {
        return $this->value ? substr($this->value, 0, 4) : $this->value;
    }

    /**
     * Get the month component's Dropdown instance
     * @return Dropdown The Dropdown object for the month component
     */
    public function getMMDropdown(): Dropdown
    {
        return $this->mm_dropdown;
    }

    /**
     * Access to the form processing object
     * @return Forms The form object
     */
    public function getForm(): Forms
    {
        return $this->form;
    }

    /* Validation */

    /**
     * The date field's minimum acceptable value
     * @return string The minimum value that can be entered
     */
    public function getMin(): string
    {
        return $this->min;
    }

    /**
     * The date field's minimum year component
     * @return string The minimum year component that can be entered
     */
    public function getMinYY(): string
    {
        return ltrim(substr($this->getMin(), 0, 4), '0');
    }

    /**
     * The date field's maximum acceptable value
     * @return string The maximum value that can be entered
     */
    public function getMax(): string
    {
        return $this->max;
    }

    /**
     * The date field's maximum year component
     * @return string The maximum year component that can be entered
     */
    public function getMaxYY(): string
    {
        return ltrim(substr($this->getMax(), 0, 4), '0');
    }

    /* Render Attributes */

    /**
     * Render the data attribute that indicates if the date field is a required field on the form
     * @return string The attribute to be include on the element
     */
    public function renderRequired(): string
    {
        return ($this->required ? 'data-required="true"' : '');
    }
}
