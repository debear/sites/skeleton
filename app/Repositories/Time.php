<?php

namespace DeBear\Repositories;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use Carbon\Carbon;
use DeBear\Helpers\HTTP;

class Time
{
    /**
     * Our current time components
     * @var array
     */
    protected $now = [];
    /**
     * Internal reference to the singleton
     * @var self
     */
    protected static $obj = null;

    /**
     * Constructor, setting default time
     */
    public function __construct()
    {
        // Initial setup: in the server's default timezone.
        $this->setDate();
    }

    /**
     * Re-run setup after config has been loaded.
     * @return void
     */
    public function setup(): void
    {
        // By this point, the config should be set to the correct final values.
        $this->setDate();
        // But we also need to localise PHP.
        if ($this->now['app']['timezone'] != $this->now['server']['timezone']) {
            date_default_timezone_set($this->now['app']['timezone']);
        }
    }

    /**
     * Set the time we'll store
     * @return void
     */
    public function setDate(): void
    {
        // Default timezone is the server's. (Only ever needs to run once).
        if (!isset($this->now['server'])) {
            $this->now['server'] = $this->buildDateComponents();
        }

        // But if the app is running in a different timezone, we need to switch to that instead.
        $this->setDateComponent('app', FrameworkConfig::get('debear.datetime.timezone_app'));

        // As we would if our database is in a different timezone.
        $this->setDateComponent('database', FrameworkConfig::get('debear.datetime.timezone_db'));

        // Set a default timezone for the user (which we may override later).
        $this->setDateComponent('user', FrameworkConfig::get('debear.datetime.timezone_app'));
    }

    /**
     * Set the date array for an individual component
     * @param string $component The array component to build.
     * @param mixed  $tz        An optional timezone to build against.
     * @return void
     */
    protected function setDateComponent(string $component, mixed $tz = false): void
    {
        if ($tz && ($tz != $this->getServerTimezone())) {
            $this->now[$component] = $this->buildDateComponents($tz);
        } else {
            $this->now[$component] = $this->now['server'];
        }
    }

    /**
     * Build the appropriate components we store for a given date
     * @param mixed $timezone The timezone to build the components against.
     * @return array The current date/time expressed as appropriate components to our class.
     */
    protected function buildDateComponents(mixed $timezone = false): array
    {
        // Current timestamp as both raw (int) and SQL formats.
        if (!$timezone) {
            $timezone = FrameworkConfig::get('debear.datetime.timezone_server');
            $override = FrameworkConfig::get('debear.datetime.override');
            $raw = (isset($override) ? strtotime($override) : time());
            $sql = date('Y-m-d H:i:s', $raw);
            $obj = new Carbon($sql, $timezone);
        } else {
            $obj = $this->now['server']['obj']->copy()->setTimezone($timezone);
            $raw = $obj->format('U');
            $sql = $obj->toDateTimeString();
        }
        // Date/Time parts.
        list($date, $time) = explode(' ', $sql);
        // Return as an array.
        return compact('obj', 'raw', 'sql', 'date', 'time', 'timezone');
    }

    /**
     * Format a/the time
     * @param string $fmt The format to be returned.
     * @return string The formatted time
     */
    public function format(string $fmt = 'Y-m-d H:i:s'): string
    {
        return $this->now['app']['obj']->format($fmt);
    }

    /**
     * Adjust and format a/the time in the given format
     * @param string $adj The adjustment to apply to the server's time.
     * @param string $fmt The format to be returned.
     * @return string The formatted time
     */
    public function adjustFormat(string $adj, string $fmt = 'Y-m-d H:i:s'): string
    {
        return $this->now['app']['obj']->copy()->modify($adj)->format($fmt);
    }

    /**
     * Format the server's time in the given format
     * @param string $fmt The format to be returned.
     * @return string The formatted time
     */
    public function formatServer(string $fmt = 'Y-m-d H:i:s'): string
    {
        return $this->now['server']['obj']->format($fmt);
    }

    /**
     * Adjust and format the server's time in the given format
     * @param string $adj The adjustment to apply to the server's time.
     * @param string $fmt The format to be returned.
     * @return string The formatted time
     */
    public function adjustFormatServer(string $adj, string $fmt = 'Y-m-d H:i:s'): string
    {
        return $this->now['server']['obj']->copy()->modify($adj)->format($fmt);
    }

    /**
     * Format the database's time in the given format
     * @param string $fmt The format to be returned.
     * @return string The formatted time
     */
    public function formatDatabase(string $fmt = 'Y-m-d H:i:s'): string
    {
        return $this->now['database']['obj']->format($fmt);
    }

    /**
     * Adjust and format the database's time in the given format
     * @param string $adj The adjustment to apply to the database's time.
     * @param string $fmt The format to be returned.
     * @return string The formatted time
     */
    public function adjustFormatDatabase(string $adj, string $fmt = 'Y-m-d H:i:s'): string
    {
        return $this->now['database']['obj']->copy()->modify($adj)->format($fmt);
    }

    /**
     * Format the user's time in the given format
     * @param string $fmt The format to be returned.
     * @return string The formatted time
     */
    public function formatUser(string $fmt = 'Y-m-d H:i:s'): string
    {
        return $this->now['user']['obj']->format($fmt);
    }

    /**
     * Adjust and format the user's time in the given format
     * @param string $adj The adjustment to apply to the user's time.
     * @param string $fmt The format to be returned.
     * @return string The formatted time
     */
    public function adjustFormatUser(string $adj, string $fmt = 'Y-m-d H:i:s'): string
    {
        return $this->now['user']['obj']->copy()->modify($adj)->format($fmt);
    }

    /**
     * Format the custom time in the given format
     * @param string $tz  The custom timezone.
     * @param string $fmt The format to be returned.
     * @return string The formatted time
     */
    public function formatAlt(string $tz, string $fmt = 'Y-m-d H:i:s'): string
    {
        $this->setAltTimezone($tz);
        return $this->now["alt-$tz"]['obj']->format($fmt);
    }

    /**
     * Adjust and format the custom timzeon in the given format
     * @param string $tz  The custom timezone.
     * @param string $adj The adjustment to apply to the custom timzeon.
     * @param string $fmt The format to be returned.
     * @return string The formatted time
     */
    public function adjustFormatAlt(string $tz, string $adj, string $fmt = 'Y-m-d H:i:s'): string
    {
        $this->setAltTimezone($tz);
        return $this->now["alt-$tz"]['obj']->copy()->modify($adj)->format($fmt);
    }

    /**
     * Format an absolute time with the given string
     * @param integer|string $time The time to render.
     * @return mixed The representation as either a formatted string or raw value
     */
    public function formatTime(int|string $time): mixed
    {
        return (is_numeric($time) ? date('Y-m-d H:i:s', $time) : $time);
    }

    /*************************
     * App Date/Time Getters *
     *************************/
    /**
     * The current timestamp, which may be localised
     * @return integer The current (localised) timestamp
     */
    public function getNow(): int
    {
        return $this->now['app']['raw'];
    }
    /**
     * The current datetime, which may be localised
     * @return string The current (localised) time in MySQL's Y-m-d H:i:s format
     */
    public function getNowFmt(): string
    {
        return $this->now['app']['sql'];
    }
    /**
     * The current date, which may be localised
     * @return string The current (localised) date in MySQL's Y-m-d format
     */
    public function getCurDate(): string
    {
        return $this->now['app']['date'];
    }
    /**
     * The current time, which may be localised
     * @return string The current (localised) time in MySQL's H:i:s format
     */
    public function getCurTime(): string
    {
        return $this->now['app']['time'];
    }
    /**
     * The app's timezone
     * @return string The timezone the app has been configured to run in
     */
    public function getTimezone(): string
    {
        return $this->now['app']['timezone'];
    }

    /****************************
     * Server Date/Time Getters *
     ****************************/
    /**
     * The current timestamp from the server's perspective
     * @return integer The current timestamp in the server's timezone
     */
    public function getServerNow(): int
    {
        return $this->now['server']['raw'];
    }
    /**
     * The current datetime from the server's perspective
     * @return string The current time in MySQL's Y-m-d H:i:s format in the server's timezone
     */
    public function getServerNowFmt(): string
    {
        return $this->now['server']['sql'];
    }
    /**
     * The current date from the server's perspective
     * @return string The current date in MySQL's Y-m-d format in the server's timezone
     */
    public function getServerCurDate(): string
    {
        return $this->now['server']['date'];
    }
    /**
     * The current time from the server's perspective
     * @return string The current time in MySQL's H:i:s format in the server's timezone
     */
    public function getServerCurTime(): string
    {
        return $this->now['server']['time'];
    }
    /**
     * The server's timezone
     * @return string The timezone the server has been configured to run in
     */
    public function getServerTimezone(): string
    {
        return $this->now['server']['timezone'];
    }

    /******************************
     * Database Date/Time Getters *
     ****************************/
    /**
     * The current timestamp from the database's perspective
     * @return integer The current timestamp in the database's timezone
     */
    public function getDatabaseNow(): int
    {
        return $this->now['database']['raw'];
    }
    /**
     * The current datetime from the database's perspective
     * @return string The current time in MySQL's Y-m-d H:i:s format in the database's timezone
     */
    public function getDatabaseNowFmt(): string
    {
        return $this->now['database']['sql'];
    }
    /**
     * The current date from the database's perspective
     * @return string The current date in MySQL's Y-m-d format in the database's timezone
     */
    public function getDatabaseCurDate(): string
    {
        return $this->now['database']['date'];
    }
    /**
     * The current time from the database's perspective
     * @return string The current time in MySQL's H:i:s format in the database's timezone
     */
    public function getDatabaseCurTime(): string
    {
        return $this->now['database']['time'];
    }
    /**
     * The timezone date/times are stored in the database as
     * @return string The timezone the database has value stored in
     */
    public function getDatabaseTimezone(): string
    {
        return $this->now['database']['timezone'];
    }
    /**
     * Update the timezone date/times are stored in the database as
     * @param string $tz The timezone the database has value stored in.
     * @return void
     */
    public function setDatabaseTimezone(string $tz): void
    {
        if ($tz != $this->getDatabaseTimezone()) {
            $this->setDateComponent('database', $tz);
        }
    }

    /**************************
     * User Date/Time Getters *
     **************************/
    /**
     * The current timestamp from the user's perspective
     * @return integer The current timestamp in the user's timezone
     */
    public function getUserNow(): int
    {
        return $this->now['user']['raw'];
    }
    /**
     * The current datetime from the user's perspective
     * @return string The current time in MySQL's Y-m-d H:i:s format in the user's timezone
     */
    public function getUserNowFmt(): string
    {
        return $this->now['user']['sql'];
    }
    /**
     * The current date from the user's perspective
     * @return string The current date in MySQL's Y-m-d format in the user's timezone
     */
    public function getUserCurDate(): string
    {
        return $this->now['user']['date'];
    }
    /**
     * The current time from the user's perspective
     * @return string The current time in MySQL's H:i:s format in the user's timezone
     */
    public function getUserCurTime(): string
    {
        return $this->now['user']['time'];
    }
    /**
     * The user's timezone
     * @return string The timezone the is located in
     */
    public function getUserTimezone(): string
    {
        return $this->now['user']['timezone'];
    }

    /**
     * Specifically set the timezone according to the user.
     * @param string $tz The user's timezone.
     * @return void
     */
    public function setUserTimezone(string $tz): void
    {
        if ($tz != $this->getUserTimezone()) {
            $this->setDateComponent('user', $tz);
        }
    }

    /***************************
     * Custom Timezone Getters *
     ***************************/
    /**
     * The current timestamp an alternative timezone's perspective
     * @param string $tz The custom timezone.
     * @return integer The current timestamp in the user's timezone
     */
    public function getAltTimezoneNow(string $tz): int
    {
        $this->setAltTimezone($tz);
        return $this->now["alt-$tz"]['raw'];
    }
    /**
     * The current datetime an alternative timezone's perspective
     * @param string $tz The custom timezone.
     * @return string The current time in MySQL's Y-m-d H:i:s format in the user's timezone
     */
    public function getAltTimezoneNowFmt(string $tz): string
    {
        $this->setAltTimezone($tz);
        return $this->now["alt-$tz"]['sql'];
    }
    /**
     * The current date an alternative timezone's perspective
     * @param string $tz The custom timezone.
     * @return string The current date in MySQL's Y-m-d format in the user's timezone
     */
    public function getAltTimezoneCurDate(string $tz): string
    {
        $this->setAltTimezone($tz);
        return $this->now["alt-$tz"]['date'];
    }
    /**
     * The current time an alternative timezone's perspective
     * @param string $tz The custom timezone.
     * @return string The current time in MySQL's H:i:s format in the user's timezone
     */
    public function getAltTimezoneCurTime(string $tz): string
    {
        $this->setAltTimezone($tz);
        return $this->now["alt-$tz"]['time'];
    }

    /**
     * Specifically set the timezone according to the user.
     * @param string $tz The user's timezone.
     * @return void
     */
    protected function setAltTimezone(string $tz): void
    {
        if (!array_key_exists("alt-$tz", $this->now)) {
            $this->setDateComponent("alt-$tz", $tz);
        }
    }

    /******************
     * Carbon Setters *
     ******************/
    /**
     * Localise a Date/Time field between the database's timezone and the user's
     * @param Carbon $dt The original Date/Time field.
     * @return Carbon The localised equivalent
     */
    public function localiseDatabase(Carbon $dt): Carbon
    {
        // If there's no need to localise, just return the original value.
        if ($this->getUserTimezone() == $this->getDatabaseTimezone()) {
            return $dt;
        }
        // Note: If the two timezones are synonyms to the same offset, we could skip the conversion, but to do so
        // we'd still have to do the conversion (as we can't rely on the _current_ time if we're checking an old one)
        // so we'll modify to the new locale and return in all instances.
        return Carbon::createFromFormat('Y-m-d H:i:s', $dt->toDateTimeString(), $this->getDatabaseTimezone())
            ->setTimezone($this->getUserTimezone());
    }

    /*******************
     * Testing Methods *
     *******************/
    /**
     * Reset the time in the object (e.g., after a unit test)
     * @param string $time The time to be set.
     * @return void
     */
    public function resetCache(string $time): void
    {
        if (HTTP::isTest()) {
            // Unset the cached values.
            $this->now = [];
            // Set the override time, and re-calc.
            FrameworkConfig::set(['debear.datetime.override' => $time]);
            $this->setDate();
        }
    }

    /******************
     * Static Methods *
     ******************/
    /**
     * Get the instantiated singleton from within a static call.
     * @return self The instantiated singleton
     */
    public static function object(): self
    {
        if (!isset(static::$obj)) {
            static::$obj = App::make(self::class);
        }
        return static::$obj;
    }
}
