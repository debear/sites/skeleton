<?php

namespace DeBear\Repositories;

use Exception;
use DeBear\Helpers\HTTP;

class BugReporting
{
    /**
     * Constant names
     * @var array
     */
    protected $const_names;
    /**
     * Log of incidents to report
     * @var array
     */
    protected $log;

    /**
     * Constructor
     */
    public function __construct()
    {
        // Setup.
        $this->log = [];
        $const = get_defined_constants();
        $this->const_names = array_flip(
            array_intersect_key(
                $const,
                array_flip(
                    array_filter(
                        array_keys($const),
                        function ($key) {
                            return substr($key, 0, 2) == 'E_';
                        }
                    )
                )
            )
        );
        // Enable on live only, but ignore these lines during our code coverage.
        if (HTTP::isLive()) {
            // @codeCoverageIgnoreStart
            set_error_handler([$this, 'handleError']);
            set_exception_handler([$this, 'handleException']);
            register_shutdown_function([$this, 'handleShutdown']);
            // @codeCoverageIgnoreEnd
        }
    }

    /**
     * External log catcher
     * @param integer $err_no   Error number.
     * @param string  $err_str  Error message.
     * @param string  $err_file File the error occurred in.
     * @param integer $err_line Line number in the file the error occurred.
     * @return void
     */
    public function handleError(int $err_no, string $err_str, string $err_file, int $err_line): void
    {
        $this->log($err_no, $err_str, $err_file, $err_line);
    }

    /**
     * Exception catcher
     * @param Exception $e The generated exception (as the top-level Object).
     * @return void
     */
    public function handleException(Exception $e): void
    {
        $this->log(
            E_ERROR,
            'Uncaught exception: \'' . $e->getMessage() . ' (Type: \'' . get_class($e) . '\'; Code: \''
                . $e->getCode() . '\')',
            $e->getFile(),
            $e->getLine()
        );
    }

    /**
     * Catch E_ERROR via alternate means
     * @return void
     */
    public function handleShutdown(): void
    {
        // Log in a code coverage way.
        $last = is_array(error_get_last()) ? error_get_last() : ['type' => 0];
        ($last['type'] === E_ERROR) && $this->log($last['type'], $last['message'], $last['file'], $last['line']);
        $this->save();
        !HTTP::isTest() && exit;
    }

    /**
     * Manual trigger
     * @param integer $err_no   Error number.
     * @param string  $err_str  Error message.
     * @param string  $err_file File the error occurred in.
     * @param integer $err_line Line number in the file the error occurred.
     * @return void
     */
    public function raise(int $err_no, string $err_str, ?string $err_file = null, ?int $err_line = null): void
    {
        if (!isset($err_file) || !isset($err_line)) {
            $caller = $this->methodCaller();
            if (!isset($err_file)) {
                $err_file = $caller['file'];
            }
            if (!isset($err_line)) {
                $err_line = $caller['line'];
            }
        }
        $this->log($err_no, $err_str, $err_file, $err_line);
    }

    /**
     * Internal workings
     * @param integer $err_no   Error number.
     * @param string  $err_str  Error message.
     * @param string  $err_file File the error occurred in.
     * @param integer $err_line Line number in the file the error occurred.
     * @return void
     */
    protected function log(int $err_no, string $err_str, ?string $err_file = null, ?int $err_line = null): void
    {
        $this->log[] = [
            'code' => substr($this->const_names[$err_no], 2),
            'error' => $err_str,
            'file' => $err_file,
            'line' => $err_line,
            // Strip the last trace options, as they relate to this logging method.
            'trace' => array_slice(debug_backtrace(), 2),
        ];
    }

    /**
     * Save reports
     * @return void
     */
    public function save(): void
    {
        if (sizeof($this->log)) {
            Logging::report('PHP', $this->log);
        }
    }

    /**
     * Determine the method that called our current caller (i.e., stack trace)
     * @param integer $offset Number of steps to be skipped when finding a method's caller. (Optional).
     * @return array   Relevant caller details from the stack trace
     */
    protected function methodCaller(int $offset = 1): array
    {
        // Get the traces.
        // +1 to remove method_caller() call itself, another +1 to get (from the previous call) the method info.
        $traces = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, $offset + 2);

        // Ensure we have the right option available.
        $ret = [];
        if (isset($traces[$offset])) {
            $ret = array_intersect_key($traces[$offset], ['file' => 1, 'line' => 1]);
        }

        // If available, get the method info from the previous call ($offset + 1).
        if (isset($traces[$offset + 1])) {
            $ret = array_merge($ret, array_intersect_key($traces[$offset + 1], ['class' => 1, 'function' => 1]));
        }

        return $ret;
    }
}
