##
## Twitter processing
##

DROP PROCEDURE IF EXISTS `twitter_get_template`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `twitter_get_template`(
  IN  v_app VARCHAR(30),
  IN  v_type VARCHAR(30),
  IN  v_lookback SMALLINT UNSIGNED,
  IN  v_error_handler ENUM('error-missing','ignore-missing'),
  OUT v_template_id TINYINT UNSIGNED,
  OUT v_template_body VARCHAR(350)
)
  COMMENT 'Twitter template loader'
BEGIN

  SELECT TWEET_TMPL.template_id, TWEET_TMPL.template_body
    INTO v_template_id, v_template_body
  FROM COMMS_TWITTER_TEMPLATE AS TWEET_TMPL
  LEFT JOIN COMMS_TWITTER AS TWEET
    ON (TWEET.app = TWEET_TMPL.app
    AND TWEET.tweet_type = TWEET_TMPL.tweet_type
    AND TWEET.template_id = TWEET_TMPL.template_id
    AND v_lookback IS NOT NULL
    AND TWEET.tweet_queued >= DATE_SUB(NOW(), INTERVAL v_lookback DAY))
  WHERE TWEET_TMPL.app = v_app
  AND   TWEET_TMPL.tweet_type = v_type
  AND   TWEET.tweet_id IS NULL
  ORDER BY RAND() DESC
  LIMIT 1;

  # If we failed to extract a template, this should be an error
  IF v_template_id IS NULL AND v_error_handler = 'error-missing' THEN
    SET @v_msg = CONCAT('Unable to load tweet template for "', v_app, '" of type "', v_type, '"', IF(v_lookback IS NOT NULL, CONCAT(' with a ', v_lookback, ' day lookback'), ''));
    SIGNAL SQLSTATE 'DB404' SET MESSAGE_TEXT = @v_msg;
  END IF;

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `twitter_queue`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `twitter_queue`(
  IN  v_app_internal VARCHAR(30),
  IN  v_app_api VARCHAR(30),
  IN  v_account VARCHAR(30),
  IN  v_account_retweet VARCHAR(30),
  IN  v_type VARCHAR(30),
  IN  v_template_id TINYINT UNSIGNED,
  IN  v_body VARCHAR(350),
  IN  v_send DATETIME,
  OUT v_tweet_id INT UNSIGNED
)
  COMMENT 'Schedule a new Tweet to be sent'
BEGIN

  # First, the main tweet
  INSERT INTO COMMS_TWITTER_SYNC (app, sync_id, tweet_type, template_id, retweet_id, tweet_body, tweet_queued, tweet_send, tweet_status, twitter_app, twitter_account)
    VALUES (v_app_internal, NULL, v_type, v_template_id, NULL, v_body, NOW(), v_send, 'queued', v_app_api, v_account);

  SELECT LAST_INSERT_ID() INTO v_tweet_id;

  # Then, if required, retweeted by an alternate account
  IF v_account_retweet IS NOT NULL THEN
    INSERT INTO COMMS_TWITTER_SYNC (app, sync_id, tweet_type, template_id, retweet_id, tweet_body, tweet_queued, tweet_send, tweet_status, twitter_app, twitter_account)
      VALUES (v_app_internal, NULL, v_type, NULL, v_tweet_id, NULL, NOW(), v_send, 'queued', v_app_api, v_account_retweet);
  END IF;

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `twitter_queue_media`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `twitter_queue_media`(
  IN  v_app_internal VARCHAR(30),
  IN  v_tweet_id INT UNSIGNED,
  IN  v_local VARCHAR(250),
  IN  v_media_ref VARCHAR(100),
  IN  v_order TINYINT UNSIGNED,
  OUT v_media_id TINYINT UNSIGNED
)
  COMMENT 'Attach media to a queued Tweet'
BEGIN

  INSERT INTO COMMS_TWITTER_SYNC_MEDIA (app, sync_id, sync_media_id, media_local, media_unique_ref, media_order, media_queued)
    VALUES (v_app_internal, v_tweet_id, NULL, v_local, v_media_ref, v_order, NOW());

  SELECT LAST_INSERT_ID() INTO v_media_id;

END $$

DELIMITER ;
