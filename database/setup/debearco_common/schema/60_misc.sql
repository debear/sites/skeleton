CREATE TABLE `SITE_REFERERS` (
  `referer_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `referer_app` VARCHAR(150) COLLATE latin1_general_ci DEFAULT NULL,
  `when_done` DATETIME DEFAULT NULL,
  `referer_domain` VARCHAR(150) COLLATE latin1_general_ci DEFAULT NULL,
  `referer_page` VARCHAR(255) COLLATE latin1_general_ci DEFAULT NULL,
  `referer_args` VARCHAR(150) COLLATE latin1_general_ci DEFAULT NULL,
  `referer_ip` VARCHAR(50) COLLATE latin1_general_ci DEFAULT NULL,
  `referer_browser` VARCHAR(255) COLLATE latin1_general_ci DEFAULT NULL,
  `referer_full_url` VARCHAR(255) COLLATE latin1_general_ci DEFAULT NULL,
  `referered_to` VARCHAR(255) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`referer_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
