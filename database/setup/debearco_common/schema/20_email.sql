CREATE TABLE `COMMS_EMAIL` (
  `app` VARCHAR(30) COLLATE latin1_general_ci NOT NULL,
  `email_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `email_reason` VARCHAR(50) COLLATE latin1_general_ci DEFAULT NULL,
  `email_from` VARCHAR(255) COLLATE latin1_general_ci DEFAULT NULL,
  `email_to` VARCHAR(255) COLLATE latin1_general_ci DEFAULT NULL,
  `email_subject` VARCHAR(255) COLLATE latin1_general_ci DEFAULT NULL,
  `email_body` TEXT COLLATE latin1_general_ci DEFAULT NULL,
  `email_queued` DATETIME DEFAULT NULL,
  `email_send` DATETIME NOT NULL,
  `email_sent` DATETIME DEFAULT NULL,
  `email_status` ENUM('queued','opt_out','blocked','sent','failed') COLLATE latin1_general_ci NOT NULL,
  `email_code` CHAR(16) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`app`,`email_id`),
  UNIQUE KEY `by_code` (`email_code`),
  KEY `by_send` (`email_send`,`email_status`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `COMMS_EMAIL_LINK` (
  `app` VARCHAR(30) COLLATE latin1_general_ci NOT NULL,
  `email_id` INT(11) UNSIGNED NOT NULL,
  `link_id` TINYINT(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `link_full` VARCHAR(255) COLLATE latin1_general_ci DEFAULT NULL,
  `link_protocol` VARCHAR(10) COLLATE latin1_general_ci DEFAULT NULL,
  `link_domain` VARCHAR(64) COLLATE latin1_general_ci DEFAULT NULL,
  `link_port` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `link_url` VARCHAR(100) COLLATE latin1_general_ci DEFAULT NULL,
  `link_query` VARCHAR(50) COLLATE latin1_general_ci DEFAULT NULL,
  `link_code` CHAR(16) COLLATE latin1_general_ci DEFAULT NULL,
  `times_visited` SMALLINT(5) UNSIGNED NOT NULL DEFAULT 0,
  `first_visited` DATETIME DEFAULT NULL,
  `last_visited` DATETIME DEFAULT NULL,
  PRIMARY KEY (`app`,`email_id`,`link_id`),
  UNIQUE KEY `by_code` (`link_code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `COMMS_EMAIL_TEMPLATE` (
  `app` VARCHAR(30) COLLATE latin1_general_ci NOT NULL,
  `template_id` TINYINT(2) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ref` VARCHAR(100) COLLATE latin1_general_ci NOT NULL,
  `wrapper` VARCHAR(100) COLLATE latin1_general_ci NOT NULL,
  `from` VARCHAR(255) COLLATE latin1_general_ci NOT NULL,
  `subject` VARCHAR(255) COLLATE latin1_general_ci NOT NULL,
  `summary` VARCHAR(100) COLLATE latin1_general_ci DEFAULT NULL,
  `message` TEXT COLLATE latin1_general_ci DEFAULT NULL,
  `clause` VARCHAR(255) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`app`,`template_id`),
  UNIQUE KEY `by_code` (`ref`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
