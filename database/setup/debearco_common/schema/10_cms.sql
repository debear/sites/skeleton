CREATE TABLE `CMS_CONTENT` (
  `content_id` SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `site` CHAR(8) COLLATE latin1_general_ci NOT NULL,
  `page` VARCHAR(50) COLLATE latin1_general_ci NOT NULL,
  `section` VARCHAR(30) COLLATE latin1_general_ci NOT NULL,
  `order` TINYINT(3) UNSIGNED NOT NULL,
  `live` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
  `content` TEXT COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`content_id`),
  KEY `by_page` (`site`,`page`,`live`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
