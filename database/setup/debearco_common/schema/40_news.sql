CREATE TABLE `NEWS_ARTICLES` (
  `app` VARCHAR(15) COLLATE latin1_general_ci NOT NULL,
  `news_id` SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `source_id` TINYINT(1) UNSIGNED DEFAULT NULL,
  `title` VARCHAR(255) COLLATE latin1_general_ci DEFAULT NULL,
  `description` TEXT COLLATE latin1_general_ci DEFAULT NULL,
  `link_domain` VARCHAR(50) COLLATE latin1_general_ci DEFAULT NULL,
  `link_location` VARCHAR(550) COLLATE latin1_general_ci DEFAULT NULL,
  `link` VARCHAR(600) COLLATE latin1_general_ci DEFAULT NULL,
  `published` DATETIME DEFAULT NULL,
  `thumbnail` VARCHAR(500) COLLATE latin1_general_ci DEFAULT NULL,
  `thumbnail_domain` VARCHAR(100) COLLATE latin1_general_ci DEFAULT NULL,
  `thumbnail_width` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `thumbnail_height` SMALLINT(5) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`app`,`news_id`),
  UNIQUE KEY `uniq_title` (`app`,`title`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `NEWS_SOURCES` (
  `app` VARCHAR(15) COLLATE latin1_general_ci NOT NULL,
  `source_id` TINYINT(1) UNSIGNED NOT NULL,
  `rss_feed` VARCHAR(100) COLLATE latin1_general_ci DEFAULT NULL,
  `name` VARCHAR(30) COLLATE latin1_general_ci DEFAULT NULL,
  `title` VARCHAR(100) COLLATE latin1_general_ci DEFAULT NULL,
  `homepage` VARCHAR(200) COLLATE latin1_general_ci DEFAULT NULL,
  `description` VARCHAR(255) COLLATE latin1_general_ci DEFAULT NULL,
  `rss_copyright` VARCHAR(200) COLLATE latin1_general_ci DEFAULT NULL,
  `disp_notice` VARCHAR(40) COLLATE latin1_general_ci DEFAULT NULL,
  `ttl` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `logo` VARCHAR(100) COLLATE latin1_general_ci DEFAULT NULL,
  `logo_title` VARCHAR(100) COLLATE latin1_general_ci DEFAULT NULL,
  `logo_link` VARCHAR(100) COLLATE latin1_general_ci DEFAULT NULL,
  `logo_width` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `logo_height` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `last_updated` DATETIME DEFAULT NULL,
  `last_synced` DATETIME NOT NULL DEFAULT '2001-01-01 00:00:00',
  `active` TINYINT(1) UNSIGNED DEFAULT 1,
  `max_articles` TINYINT(3) UNSIGNED DEFAULT NULL,
  `disp_order` TINYINT(1) UNSIGNED DEFAULT NULL,
  `disp_logo` TINYINT(1) UNSIGNED DEFAULT 1,
  PRIMARY KEY (`app`,`source_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
