CREATE TABLE `COMMS_TWITTER` (
  `app` VARCHAR(30) COLLATE latin1_general_ci NOT NULL,
  `tweet_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tweet_type` VARCHAR(30) COLLATE latin1_general_ci NOT NULL,
  `template_id` TINYINT(3) UNSIGNED DEFAULT NULL,
  `retweet_id` INT(11) UNSIGNED DEFAULT NULL,
  `tweet_body` VARCHAR(350) COLLATE latin1_general_ci NULL,
  `tweet_queued` DATETIME DEFAULT NULL,
  `tweet_send` DATETIME NOT NULL,
  `tweet_sent` DATETIME DEFAULT NULL,
  `tweet_status` ENUM('queued','blocked','sent','failed') COLLATE latin1_general_ci NOT NULL,
  `twitter_app` VARCHAR(30) COLLATE latin1_general_ci NOT NULL,
  `twitter_account` VARCHAR(30) COLLATE latin1_general_ci NOT NULL,
  `twitter_return_code` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `twitter_tweet_id` VARCHAR(25) COLLATE latin1_general_ci DEFAULT NULL,
  `twitter_return_response` BLOB DEFAULT NULL,
  `twitter_return_info` BLOB DEFAULT NULL,
  PRIMARY KEY (`app`,`tweet_id`),
  KEY `by_send` (`tweet_send`,`tweet_sent`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `COMMS_TWITTER_MEDIA` (
  `app` VARCHAR(30) COLLATE latin1_general_ci NOT NULL,
  `tweet_id` INT(11) UNSIGNED NOT NULL,
  `media_id` TINYINT(1) UNSIGNED NOT NULL AUTO_INCREMENT,
  `media_local` VARCHAR(250) COLLATE latin1_general_ci DEFAULT NULL,
  `media_unique_ref` VARCHAR(100) COLLATE latin1_general_ci DEFAULT NULL,
  `media_order` TINYINT(3) UNSIGNED NOT NULL,
  `media_uploaded` DATETIME NULL,
  `media_expires` DATETIME NULL,
  `twitter_return_code` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `twitter_media_id` VARCHAR(25) COLLATE latin1_general_ci DEFAULT NULL,
  `twitter_media_key` VARCHAR(25) COLLATE latin1_general_ci DEFAULT NULL,
  `twitter_return_response` BLOB DEFAULT NULL,
  `twitter_return_info` BLOB DEFAULT NULL,
  PRIMARY KEY (`app`,`tweet_id`,`media_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `COMMS_TWITTER_SYNC` (
  `app` VARCHAR(30) COLLATE latin1_general_ci NOT NULL,
  `sync_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tweet_id` INT(11) UNSIGNED NULL,
  `tweet_type` VARCHAR(30) COLLATE latin1_general_ci NOT NULL,
  `template_id` TINYINT(3) UNSIGNED DEFAULT NULL,
  `retweet_id` INT(11) UNSIGNED DEFAULT NULL,
  `tweet_body` VARCHAR(350) COLLATE latin1_general_ci NULL,
  `tweet_queued` DATETIME DEFAULT NULL,
  `tweet_send` DATETIME NOT NULL,
  `tweet_status` ENUM('queued','blocked','sent','failed') COLLATE latin1_general_ci NOT NULL,
  `twitter_app` VARCHAR(30) COLLATE latin1_general_ci NOT NULL,
  `twitter_account` VARCHAR(30) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`app`,`sync_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `COMMS_TWITTER_SYNC_MEDIA` (
  `app` VARCHAR(30) COLLATE latin1_general_ci NOT NULL,
  `sync_id` INT(11) UNSIGNED NOT NULL,
  `sync_media_id` TINYINT(1) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tweet_id` INT(11) UNSIGNED NULL,
  `media_id` TINYINT(1) UNSIGNED NULL,
  `media_local` VARCHAR(250) COLLATE latin1_general_ci DEFAULT NULL,
  `media_unique_ref` VARCHAR(100) COLLATE latin1_general_ci DEFAULT NULL,
  `media_order` TINYINT(3) UNSIGNED NOT NULL,
  `media_queued` DATETIME DEFAULT NULL,
  PRIMARY KEY (`app`,`sync_id`,`sync_media_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `COMMS_TWITTER_DM` (
  `app` VARCHAR(30) COLLATE latin1_general_ci NOT NULL,
  `dm_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `dm_type` VARCHAR(30) COLLATE latin1_general_ci NOT NULL,
  `recipient_id` INT(10) UNSIGNED NOT NULL,
  `dm_body` VARCHAR(5000) COLLATE latin1_general_ci NOT NULL,
  `dm_queued` DATETIME DEFAULT NULL,
  `dm_send` DATETIME NOT NULL,
  `dm_sent` DATETIME DEFAULT NULL,
  `dm_status` ENUM('queued','blocked','sent','failed') COLLATE latin1_general_ci NOT NULL,
  `twitter_app` VARCHAR(30) COLLATE latin1_general_ci NOT NULL,
  `twitter_account` VARCHAR(30) COLLATE latin1_general_ci NOT NULL,
  `twitter_return_code` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `twitter_return_response` BLOB DEFAULT NULL,
  `twitter_return_info` BLOB DEFAULT NULL,
  PRIMARY KEY (`app`,`dm_id`),
  KEY `by_send` (`dm_send`,`dm_status`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `COMMS_TWITTER_TEMPLATE` (
  `app` VARCHAR(30) COLLATE latin1_general_ci NOT NULL,
  `tweet_type` VARCHAR(30) COLLATE latin1_general_ci NOT NULL,
  `template_id` TINYINT(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `template_body` VARCHAR(350) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`app`,`tweet_type`,`template_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
