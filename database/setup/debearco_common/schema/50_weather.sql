CREATE TABLE `WEATHER_FORECAST` (
  `app` VARCHAR(30) COLLATE latin1_general_ci NOT NULL,
  `instance_key` VARCHAR(50) COLLATE latin1_general_ci NOT NULL,
  `forecast_date` DATETIME NOT NULL,
  `symbol` ENUM('sun','night','cloudy','clouds','rain','lightning','snow','mist') COLLATE latin1_general_ci NOT NULL,
  `symbol_raw` VARCHAR(20) COLLATE latin1_general_ci DEFAULT NULL,
  `summary` VARCHAR(50) COLLATE latin1_general_ci NOT NULL,
  `description` VARCHAR(255) COLLATE latin1_general_ci DEFAULT NULL,
  `temp` DECIMAL(4,2) NOT NULL,
  `pressure` DECIMAL(6,2) UNSIGNED DEFAULT NULL,
  `precipitation` DECIMAL(4,2) UNSIGNED DEFAULT NULL,
  `humidity` TINYINT(3) UNSIGNED DEFAULT NULL,
  `clouds` TINYINT(3) UNSIGNED DEFAULT NULL,
  `wind_speed` DECIMAL(4,2) UNSIGNED DEFAULT NULL,
  `wind_dir` ENUM('N','NE','E','SE','S','SW','W','NW') COLLATE latin1_general_ci DEFAULT NULL,
  `wind_dir_raw` DECIMAL(6,3) UNSIGNED DEFAULT NULL,
  `update_type` ENUM('current','hourly','daily') COLLATE latin1_general_ci DEFAULT NULL,
  `last_updated` DATETIME NOT NULL,
  PRIMARY KEY (`app`,`instance_key`,`forecast_date`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `WEATHER_SEARCHES` (
  `app` VARCHAR(30) COLLATE latin1_general_ci NOT NULL,
  `search_id` TINYINT(3) UNSIGNED NOT NULL,
  `display_name` VARCHAR(100) COLLATE latin1_general_ci NOT NULL,
  `database_name` ENUM('fantasy','sports') COLLATE latin1_general_ci NOT NULL,
  `search` TEXT COLLATE latin1_general_ci NOT NULL,
  `provider` ENUM('openweathermap') COLLATE latin1_general_ci NOT NULL DEFAULT 'openweathermap',
  `active` TINYINT(3) UNSIGNED NOT NULL DEFAULT 1,
  PRIMARY KEY (`app`,`search_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
