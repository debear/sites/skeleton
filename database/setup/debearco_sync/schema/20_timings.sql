CREATE TABLE `SERVER_SYNC_TIMINGS` (
  `sync_app` VARCHAR(50) NOT NULL,
  `sync_id` SMALLINT UNSIGNED NOT NULL,
  `sync_instance` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `sync_dir` ENUM('up','down') NOT NULL,
  `sync_master` ENUM('dev','data','live') NOT NULL,
  `sync_slave` ENUM('dev','data','live') NOT NULL,
  `sync_start` DATETIME,
  `sync_end` DATETIME,
  `sync_time` DECIMAL(7,3) UNSIGNED DEFAULT 0.000,
  PRIMARY KEY (`sync_app`,`sync_id`,`sync_instance`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SERVER_SYNC_TIMINGS_AGGR` (
  `sync_app` VARCHAR(50) DEFAULT '',
  `sync_id` SMALLINT UNSIGNED DEFAULT 0,
  `sync_master` ENUM('dev','data','live') NOT NULL,
  `sync_slave` ENUM('dev','data','live') NOT NULL,
  `aggr_date` DATE,
  `tot_syncs` SMALLINT UNSIGNED DEFAULT 0,
  `tot_mean` DECIMAL(7,3) UNSIGNED DEFAULT 0.000,
  `tot_min` DECIMAL(7,3) UNSIGNED DEFAULT 0.000,
  `tot_max` DECIMAL(7,3) UNSIGNED DEFAULT 0.000,
  `tot_stddev` DECIMAL(7,3) UNSIGNED DEFAULT 0.000,
  `l7d_syncs` TINYINT UNSIGNED DEFAULT 0,
  `l7d_mean` DECIMAL(7,3) UNSIGNED,
  `l7d_min` DECIMAL(7,3) UNSIGNED,
  `l7d_max` DECIMAL(7,3) UNSIGNED,
  `l7d_stddev` DECIMAL(7,3) UNSIGNED,
  `l30d_syncs` TINYINT UNSIGNED DEFAULT 0,
  `l30d_mean` DECIMAL(7,3) UNSIGNED,
  `l30d_min` DECIMAL(7,3) UNSIGNED,
  `l30d_max` DECIMAL(7,3) UNSIGNED,
  `l30d_stddev` DECIMAL(7,3) UNSIGNED
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
