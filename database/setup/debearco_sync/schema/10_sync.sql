CREATE TABLE `SERVER_SYNC` (
  `sync_app` VARCHAR(50) NOT NULL DEFAULT '',
  `name` VARCHAR(100),
  `sync_master` SET('dev','data','live'),
  `sync_slave` SET('dev','data','live'),
  `active` TINYINT(1) UNSIGNED DEFAULT 1,
  PRIMARY KEY (`sync_app`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SERVER_SYNC_STEPS` (
  `sync_app` VARCHAR(50) NOT NULL,
  `sync_id` SMALLINT UNSIGNED NOT NULL,
  `summary` VARCHAR(75) DEFAULT '',
  `sync_order` SMALLINT UNSIGNED DEFAULT 0,
  `sync_type` ENUM('schema','database','file','script') NOT NULL,
  `active` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
  PRIMARY KEY (`sync_app`,`sync_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SERVER_SYNC_SCHEMA` (
  `sync_app` VARCHAR(50) NOT NULL,
  `sync_id` SMALLINT UNSIGNED NOT NULL,
  `db_name` VARCHAR(30) DEFAULT '',
  `table_prefix` VARCHAR(50) DEFAULT '',
  PRIMARY KEY (`sync_app`,`sync_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SERVER_SYNC_DB` (
  `sync_app` VARCHAR(50) NOT NULL,
  `sync_id` SMALLINT UNSIGNED NOT NULL,
  `db_name` VARCHAR(30) DEFAULT '',
  `table_name` VARCHAR(50) DEFAULT '',
  `where_clause` VARCHAR(255) DEFAULT '',
  `clear_first` TINYINT UNSIGNED DEFAULT 0,
  PRIMARY KEY (`sync_app`,`sync_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SERVER_SYNC_FILE` (
  `sync_app` VARCHAR(50) NOT NULL,
  `sync_id` SMALLINT NOT NULL,
  `dev_location` VARCHAR(255),
  `live_location` VARCHAR(255),
  `rsync_opt` TEXT COLLATE latin1_general_ci,
  PRIMARY KEY (`sync_app`,`sync_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SERVER_SYNC_SCRIPT` (
  `sync_app` VARCHAR(50) NOT NULL,
  `sync_id` SMALLINT UNSIGNED NOT NULL,
  `script_name` VARCHAR(75) DEFAULT '',
  `script_args` VARCHAR(50) DEFAULT '',
  PRIMARY KEY (`sync_app`,`sync_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
