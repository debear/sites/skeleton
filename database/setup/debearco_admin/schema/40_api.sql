CREATE TABLE `API_TOKENS` (
  `token_id` SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` SMALLINT(5) UNSIGNED NOT NULL,
  `name` VARCHAR(100) COLLATE latin1_general_ci DEFAULT NULL,
  `token` CHAR(32) COLLATE latin1_general_ci NOT NULL,
  `password` CHAR(30) COLLATE latin1_general_ci NOT NULL,
  `date_active` DATETIME NOT NULL,
  `date_revoked` DATETIME DEFAULT NULL,
  PRIMARY KEY (`token_id`),
  UNIQUE KEY `by_ref` (`token`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `API_TOKEN_SCOPE` (
  `token_id` SMALLINT(5) UNSIGNED NOT NULL,
  `domain` ENUM('fantasy','my','sports','sysmon','www') COLLATE latin1_general_ci NOT NULL,
  `scope` VARCHAR(50) COLLATE latin1_general_ci NOT NULL,
  `date_active` DATETIME NOT NULL,
  `date_revoked` DATETIME DEFAULT NULL,
  PRIMARY KEY (`token_id`, `domain`, `scope`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
