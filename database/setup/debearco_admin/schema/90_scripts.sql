CREATE TABLE `ARCHIVE_RULES` (
  `name` VARCHAR(40) COLLATE latin1_general_ci NOT NULL,
  `db` ENUM('debearco_admin','debearco_common','debearco_fantasy','debearco_sports','debearco_sysmon','debearco_www') COLLATE latin1_general_ci DEFAULT NULL,
  `tbl` VARCHAR(40) COLLATE latin1_general_ci DEFAULT NULL,
  `ttl` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `col` VARCHAR(20) COLLATE latin1_general_ci DEFAULT NULL,
  `active` TINYINT(3) UNSIGNED DEFAULT 1,
  `order` SMALLINT(5) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `LOCKFILE_STAMPS` (
  `app` VARCHAR(50) COLLATE latin1_general_ci NOT NULL,
  `last_started` DATETIME DEFAULT NULL,
  `last_finished` DATETIME DEFAULT NULL,
  `active` TINYINT(1) UNSIGNED DEFAULT NULL,
  `daily` TINYINT(1) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`app`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
