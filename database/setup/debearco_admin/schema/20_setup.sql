CREATE TABLE `SETUP_COUNTRY` (
  `code2` CHAR(2) COLLATE latin1_general_ci NOT NULL,
  `code3` CHAR(3) COLLATE latin1_general_ci DEFAULT NULL,
  `name` VARCHAR(50) COLLATE latin1_general_ci DEFAULT NULL,
  `default_timezone` VARCHAR(40) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`code2`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SETUP_COUNTRY_TZ` (
  `timezone` VARCHAR(40) COLLATE latin1_general_ci NOT NULL,
  `code2` CHAR(2) COLLATE latin1_general_ci DEFAULT NULL,
  `geo_lat` DECIMAL(8,5) DEFAULT NULL,
  `geo_lng` DECIMAL(8,5) DEFAULT NULL,
  `population` INT(10) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`timezone`),
  KEY `code2` (`code2`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `USER_LEVELS` (
  `level` ENUM('restricted','user','admin','superadmin') COLLATE latin1_general_ci NOT NULL,
  `rank` TINYINT(1) UNSIGNED NOT NULL,
  `name` VARCHAR(75) COLLATE latin1_general_ci NOT NULL,
  `is_admin` TINYINT(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`level`,`rank`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `USER_PERMISSIONS` (
  `site` VARCHAR(50) COLLATE latin1_general_ci NOT NULL,
  `section` VARCHAR(20) COLLATE latin1_general_ci DEFAULT NULL,
  `user_id` SMALLINT(5) UNSIGNED NOT NULL,
  `level` ENUM('restricted','user','admin','superadmin') COLLATE latin1_general_ci NOT NULL DEFAULT 'user',
  UNIQUE KEY `faux_primary` (`site`,`section`,`user_id`),
  KEY `level` (`level`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
