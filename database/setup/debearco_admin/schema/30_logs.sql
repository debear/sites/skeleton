CREATE TABLE `COOKIE_PREFERENCE_LOG` (
  `log_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `session_id` CHAR(16) COLLATE latin1_general_ci NOT NULL,
  `user_id` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `remote_ip` CHAR(15) COLLATE latin1_general_ci NOT NULL,
  `when_done` DATETIME NOT NULL,
  `setting` CHAR(10) COLLATE latin1_general_ci NOT NULL,
  `state` TINYINT(1) UNSIGNED NOT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `USER_ACTIVITY_LOG` (
  `activity_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `activity_time` DATETIME,
  `type` TINYINT(3) UNSIGNED NOT NULL,
  `user_id` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `session_id` CHAR(16) COLLATE latin1_general_ci DEFAULT NULL,
  `remote_ip` CHAR(15) COLLATE latin1_general_ci DEFAULT NULL,
  `browser` VARCHAR(120) COLLATE latin1_general_ci DEFAULT NULL,
  `url` VARCHAR(100) COLLATE latin1_general_ci DEFAULT NULL,
  `summary` VARCHAR(255) COLLATE latin1_general_ci DEFAULT NULL,
  `detail` TEXT COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`activity_id`),
  KEY `by_user` (`type`,`user_id`,`activity_time`),
  KEY `by_session` (`type`,`session_id`,`activity_time`),
  KEY `by_ip` (`type`,`remote_ip`,`activity_time`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
