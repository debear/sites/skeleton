CREATE TABLE `USERS` (
  `id` SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` VARCHAR(20) COLLATE latin1_general_ci NOT NULL,
  `forename` VARCHAR(20) COLLATE latin1_general_ci DEFAULT NULL,
  `surname` VARCHAR(30) COLLATE latin1_general_ci DEFAULT NULL,
  `password` CHAR(30) COLLATE latin1_general_ci DEFAULT NULL,
  `email` VARCHAR(100) COLLATE latin1_general_ci DEFAULT NULL,
  `display_name` VARCHAR(30) COLLATE latin1_general_ci DEFAULT NULL,
  `dob` DATE DEFAULT NULL,
  `timezone` VARCHAR(40) COLLATE latin1_general_ci DEFAULT 'GMT',
  `account_created` DATETIME DEFAULT NULL,
  `account_verified` DATETIME DEFAULT NULL,
  `status` ENUM('Active','Unverified','Disabled') COLLATE latin1_general_ci NOT NULL DEFAULT 'Unverified',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `USER_REMEMBERME` (
  `user_id` SMALLINT(5) UNSIGNED NOT NULL,
  `rm_string` VARCHAR(32) COLLATE latin1_general_ci NOT NULL,
  `expires` DATETIME DEFAULT NULL,
  PRIMARY KEY (`user_id`,`rm_string`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `USER_SESSIONS` (
  `session_id` CHAR(40) COLLATE latin1_general_ci NOT NULL,
  `session_id_uniq` CHAR(16) COLLATE latin1_general_ci DEFAULT NULL,
  `session_started` DATETIME DEFAULT NULL,
  `last_accessed` DATETIME DEFAULT NULL,
  `data` MEDIUMBLOB DEFAULT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `USER_SESSIONS_STATS` (
  `session_id` CHAR(16) COLLATE latin1_general_ci NOT NULL,
  `browser` VARCHAR(30) COLLATE latin1_general_ci DEFAULT NULL,
  `browser_version` VARCHAR(20) COLLATE latin1_general_ci DEFAULT NULL,
  `os` VARCHAR(30) COLLATE latin1_general_ci DEFAULT NULL,
  `session_started` DATETIME DEFAULT NULL,
  `last_accessed` DATETIME DEFAULT NULL,
  `hits_last_archived` DATE DEFAULT NULL,
  `visits` SMALLINT(6) UNSIGNED DEFAULT NULL,
  `hits` MEDIUMINT(9) UNSIGNED DEFAULT NULL,
  `logins` SMALLINT(6) UNSIGNED DEFAULT NULL,
  `logouts` SMALLINT(6) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
