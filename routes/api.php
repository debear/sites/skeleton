<?php

/*
 * Load the main site's routes
 */

@include FrameworkConfig::string('debear.dirs.base') . '/'
    . FrameworkConfig::string('debear.url.sub') . '/routes/api.php';

// Unit Test routes.
if (\DeBear\Helpers\HTTP::isTest()) {
    @include FrameworkConfig::string('debear.dirs.skel') . '/tests/PHPUnit/Setup/routes/api.php';
}

// Handle generic 404s.
Route::fallback(
    function () {
        return DeBear\Helpers\API::sendNotFound();
    }
);
