<?php

/*
 * Load the main site's routes
 */

@include FrameworkConfig::string('debear.dirs.base') . '/'
    . FrameworkConfig::string('debear.url.sub') . '/routes/web.php';

/*
 * Common routes that apply to all sites
 */

// Footer Links.
Route::get('/terms-of-use', 'Skeleton\SiteTools@terms');
Route::redirect('/admin/terms', '/terms-of-use', 308);

Route::get('/privacy-policy', 'Skeleton\SiteTools@privacy');
Route::redirect('/admin/privacy-policy', '/privacy-policy', 308);

// Generated content.
Route::get(
    '/google1c960a509629a21f.html',
    function () {
        return response(view('skeleton.pages.google_verification'), 200)
                ->header('Content-Type', 'text/plain; charset=utf8');
    }
);
Route::get(
    '/robots.txt',
    function () {
        return response(view('skeleton.pages.robots'), 200)
                ->header('Content-Type', 'text/plain; charset=utf8');
    }
);

// User account related.
Route::post('/login', 'Skeleton\Authentication@login');
Route::post('/logout', 'Skeleton\Authentication@logout');

// Triggers for the registration/login details links.
Route::match(['get', 'post'], '/register', 'My\Register@index');
Route::match(['get', 'post'], '/login-details', 'My\LoginDetails@index');

// Cookie management.
Route::get('/cookies', 'Skeleton\Cookies@view');
Route::patch('/cookies', 'Skeleton\Cookies@prefs');

// Site tools.
Route::get('/manifest.json', 'Skeleton\SiteTools@manifest');
Route::get('/pwa-sw-{name}.js', 'Skeleton\SiteTools@pwaServiceWorker');
Route::get('/sitemap', 'Skeleton\SiteTools@sitemap');
Route::get('/sitemap-{section}', 'Skeleton\SiteTools@sitemap');
Route::redirect('/admin/sitemap', '/sitemap', 308);
// Slightly more verbose version...
Route::get(
    '/admin/sitemap-{section}',
    function ($section) {
        return redirect("/sitemap-$section", 308);
    }
);
Route::get('/sitemap.xml', 'Skeleton\SiteTools@sitemapXml');

// Emails.
Route::get('/email-{i}-{t}-{r}', 'Skeleton\Emails@show')
    ->where('i', '[^-]{16}')->where('t', '[^-]{8}')->where('r', '[^-]{8}');
Route::get(
    '/admin/view-email-{code}',
    function ($code) {
        return redirect("/email-$code", 308);
    }
);
Route::get('/eml-{i}-{t}-{r}', 'Skeleton\Emails@link')
    ->where('i', '[^-]{16}')->where('t', '[^-]{8}')->where('r', '[^-]{8}');

// Report URI.
Route::post('/report-uri/{mode}', 'Skeleton\SiteTools@reportUri')
    ->where(['mode' => 'csp|js']);

// Offline template.
Route::view('/offline.htm', 'skeleton.offline');

// Any CI-specific tests?
if (\DeBear\Helpers\HTTP::isTest()) {
    @include FrameworkConfig::string('debear.dirs.skel') . '/tests/PHPUnit/Setup/routes/web.php';
}

// Handle generic 404s.
Route::fallback(
    function () {
        return response(view('errors.404'), 404);
    }
);
