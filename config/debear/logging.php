<?php

use DeBear\Helpers\HTTP;
use DeBear\Helpers\Policies;

if (!isset($debear_subload)) {
    return config('debear.logging');
}

/*
 * Logging details
 */
return [
    'database' => [
        'enabled' => !HTTP::isTest(),
        'extended' => [
            'query_log' => false,
            'slow_log' => true,
            'display' => !HTTP::isLive(),
        ],
        'is_slow' => 1000, // In milliseconds.
    ],
    'user_activity' => [
        'login' => 0,
        'failed_login' => 1,
        'password_policy' => 2,
        'account_reset_request' => 3,
        'account_password_reset' => 4,
        'account_registration' => 5,
        'account_update' => 6,
        'security' => 10,
        'scraping_attempt' => 15,
    ],
];
