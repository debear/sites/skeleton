<?php

if (!isset($debear_subload)) {
    return config('debear.version');
}

/*
 * Site revision info
 */
return [
    'breakdown' => [
        'major' => 0,
        'minor' => 0,
        'rev' => 0,
    ],
    'label' => [
        'short' => '0.0',
        'full' => '0.0',
    ],
    'updated' => false,
];
