<?php

if (!isset($debear_subload)) {
    return config('debear.policies');
}

/*
 * User permissions
 */
return [
    // Guest user who has not logged in yet.
    'guest' => [
        'name' => 'Guest',
        'rules' => [
            ['user_id', 'SET', false],
        ],
        'fallback' => true,
    ],
    // A user who has logged in.
    'logged_in' => [
        'name' => 'Logged In',
        'rules' => [
            ['user_id', 'SET', true],
        ],
        'children' => [
            // Is an administrator of some sort.
            'user:admin' => [
                'name' => 'Admin',
                'check_per_request' => true,
                'rules' => [
                    [
                        '@\DeBear\Models\Skeleton\User::checkLevel',
                        [['admin', 'superadmin']],
                        true,
                    ],
                ],
                'children' => [
                    // Specifically a Super Admin.
                    'user:superadmin' => [
                        'name' => 'Super Admin',
                        'check_per_request' => true,
                        'rules' => [
                            [
                                '@\DeBear\Models\Skeleton\User::checkLevel',
                                ['superadmin'],
                                true,
                            ],
                        ],
                    ],
                ],
            ],
            // Standard user only.
            'user' => [
                'name' => 'User',
                'rules' => [
                    ['user_id', 'SET', true],
                ],
                'children' => [
                    'user:unrestricted' => [
                        'name' => 'Unrestricted',
                        'rules' => [
                            [
                                '@\DeBear\Models\Skeleton\User::checkLevel',
                                ['restricted'],
                                false,
                            ],
                        ],
                    ],
                    // Restricted user with limited capabilities.
                    'user:restricted' => [
                        'name' => 'Restricted',
                        'rules' => [
                            [
                                '@\DeBear\Models\Skeleton\User::checkLevel',
                                ['restricted'],
                                true,
                            ],
                        ],
                    ],
                ],
            ],
            // The user's verification status.
            'verification:state' => [
                'name' => 'Verification',
                'grouping' => true,
                'children' => [
                    'user:verified' => [
                        'name' => 'Verified',
                        'rules' => [
                            ['status', '!=', 'Unverified'],
                        ],
                    ],
                    'user:unverified' => [
                        'name' => 'Unverified',
                        'rules' => [
                            ['status', '=', 'Unverified'],
                        ],
                    ],
                ],
            ],
            // How much we can be certain the person at the keyboard is the user.
            'login:state' => [
                'name' => 'State',
                'grouping' => true,
                'children' => [
                    'state:system' => [
                        'name' => 'System',
                        'rules' => [
                            [
                                '@\DeBear\Models\Skeleton\User::checkLoginState',
                                [['system', 'registration']],
                                true,
                            ],
                        ],
                    ],
                    'state:cookie' => [
                        'name' => 'Cookie',
                        'rules' => [
                            [
                                '@\DeBear\Models\Skeleton\User::checkLoginState',
                                ['cookie'],
                                true,
                            ],
                        ],
                    ],
                    'state:password' => [
                        'name' => 'Password',
                        'rules' => [
                            [
                                '@\DeBear\Models\Skeleton\User::checkLoginState',
                                ['password'],
                                true,
                            ],
                        ],
                        'children' => [
                            'state:password:recent' => [
                                'name' => 'Recent Password',
                                'check_per_request' => true,
                                'rules' => [
                                    [
                                        '@\DeBear\Models\Skeleton\User::checkLoginState',
                                        ['password:recent'],
                                        true,
                                    ],
                                ],
                            ],
                            'state:password:fails-policy' => [
                                'name' => 'Fails Policy',
                                'check_per_request' => false,
                                'rules' => [
                                    [
                                        '@\DeBear\Models\Skeleton\User::checkCacheFlag',
                                        ['password-fails-policy'],
                                        true,
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    // A visitor from Google.
    'referer' => [
        'name' => 'Referer',
        'grouping' => true,
        'children' => [
            'referer:google' => [
                'name' => 'Google Visitor',
                'rules' => [
                    ['@\DeBear\Helpers\HTTP::refererIsGoogle', [], true],
                ],
            ],
        ],
    ],
];
