<?php

if (!isset($debear_subload)) {
    return config('debear.section');
}

/*
 * Info about the section being loaded
 */
return [
    'code' => '', // Dynamic: Built from root directory in URL.
];
