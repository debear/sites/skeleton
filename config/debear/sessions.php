<?php

use DeBear\Helpers\HTTP;

if (!isset($debear_subload)) {
    return config('debear.sessions');
}

/*
 * Sessions / Cookies
 */
return [
    'handler' => [
        'enabled' => !HTTP::isTest(),
    ],
    'aggregate' => env('SESSION_AGGREGATION', true),
    'domain' => env('SESSION_DOMAIN'),
    'lifetime' => env('SESSION_LIFETIME'),
    'secure_cookie' => env('SESSION_SECURE_COOKIE'),
    'cookies' => [
        'main' => env('SESSION_COOKIE'),
        'reference' => env('SESSION_REFERENCE'),
        'rememberme' => env('SESSION_REMEMBERME'),
        'prefs' => env('SESSION_PREFS'),
        'analytics' => env('SESSION_ANALYTICS'),
    ],
    'cutoffs' => [
        'session' => env('SESSION_CUTOFF'),
        'cookie' => env('COOKIE_CUTOFF'),
    ],
];
