<?php

use DeBear\Helpers\HTTP;

if (!isset($debear_subload)) {
    return config('debear.dirs');
}

/*
 * Central directories
 */
return [
    'root' => '/../..',
    'base' => '/..',
    'app' => '/..', // Dynamic: Assumed same as base, before being appended during dynamic processing.
    'skel' => "/../$skel_dir",
    'common' => '/../common',
    'logs' => '/../../logs',
    'applogs' => 'app',
    'vendor' => (!HTTP::isTest() ? '/../_debear' : '') . '/vendor',
    'forms' => "/../$skel_dir/forms",
    'stylesheets' => 'css',
    'javascript' => 'js',
    'images' => 'images',
    'merges' => 'merged',
    'report-uri' => 'reports_', // Base folder name.
];
