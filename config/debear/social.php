<?php

if (!isset($debear_subload)) {
    return config('debear.social');
}

/*
 * Social Media details
 */
return [
    'twitter' => [
        'test' => false,
        'enabled' => true,
        'creds' => [
            'test' => [
                'app' => 'dev',
                'master' => 'DeBearTestA',
                'secondary' => 'DeBearTestB',
            ],
            'live' => [
                'app' => 'support',
                'master' => 'DeBearSupport',
            ],
        ],
    ],
];
