<?php

use DeBear\Helpers\HTTP;
use DeBear\Helpers\Policies;

if (!isset($debear_subload)) {
    return config('debear.benchmark');
}

/*
 * Benchmarking
 */
return [
    'enabled' => !HTTP::isLive(),
    'display' => [
        'log' => HTTP::isTest(),
        'display' => !HTTP::isLive(),
    ],
];
