<?php

if (!isset($debear_subload)) {
    return config('debear.url');
}

/*
 * The URL info in this environment
 */
return [
    'domain' => 'debear',
    'tld' => env('APP_TLD'),
    'base' => '', // Built during post-processing.
    'full' => '', // Built during post-processing.
    'port' => '', // Dynamic: Assumed "standard" HTTPS.
    'sub' => '', // Dynamic: Assumed unset for now.
    'subdomains' => [
        'static' => 'static',
        'cdn' => 'cdn',
    ],
    'is_https' => true, // Dynamic: Assumed we are.
    'redirect_non_https' => true,
];
