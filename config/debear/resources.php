<?php

use DeBear\Helpers\HTTP;

if (!isset($debear_subload)) {
    return config('debear.resources');
}

/*
 * Global resources
 */
return [
    'css' => [
        'skel/fonts.css',
        'skel/fontawesome/fontawesome.css',
        'skel/fontawesome/brands.css',
        'skel/fontawesome/regular.css',
        'skel/fontawesome/solid.css',
        'skel/offline.css',
        'skel/boxes.css',
        'icons.css',
        'skel/flags/16.css',
        'skel/reset.css',
        'skel/layout.css',
        'skel/layouts/desktop.css',
        'skel/layouts/tablet.css',
        'skel/layouts/mobile.css',
        'skel/grids/base.css',
        'skel/grids/mobile.css',
        'skel/grids/tablet.css',
        'skel/grids/tablet-portrait.css',
        'skel/grids/tablet-landscape.css',
        'skel/general.css',
        'skel/helpers/base.css',
        'skel/helpers/desktop.css',
        'skel/helpers/mobile.css',
        'skel/helpers/tablet.css',
        'skel/helpers/tablet-portrait.css',
        'skel/helpers/tablet-landscape.css',
        [
            'file' => 'skel/widgets/login.css',
            'policies' => 'guest',
        ],
        [
            'file' => 'skel/widgets/account.css',
            'policies' => 'logged_in',
        ],
        'skel/widgets/nav.css',
        'skel/widgets/modals.css',
        [
            'file' => 'skel/widgets/debug.css',
            'policies' => HTTP::isDev() ? ['guest', 'logged_in'] : 'user:superadmin',
        ],
    ],
    'js' => [
        'skel/vars.js',
        'skel/ajax.js',
        'skel/' . (!HTTP::isLive() ? 'widgets/dev-' : '') . 'handlers.js',
        'skel/dom.js',
        'skel/events.js',
        'skel/date.js',
        'skel/input.js',
        'skel/number.js',
        'skel/string.js',
        [
            'file' => 'skel/widgets/login.js',
            'policies' => 'guest',
        ],
        [
            'file' => 'skel/widgets/account.js',
            'policies' => 'logged_in',
        ],
        'skel/widgets/nav.js',
        'skel/widgets/modals.js',
        'skel/widgets/cookies.js',
        [
            'file' => 'skel/widgets/debug.js',
            'policies' => HTTP::isDev() ? ['guest', 'logged_in'] : 'user:superadmin',
        ],
    ],
    // Minify on live site or when requested.
    'js_merge' => HTTP::isLive(), // Dynamic: Assumed so on live.
    'css_merge' => HTTP::isLive(), // Dynamic: Assumed so on live.
    'gzip_merge' => true,
    'missing-exception' => HTTP::isDev(),
];
