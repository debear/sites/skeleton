<?php

if (!isset($debear_subload)) {
    return config('debear.errors');
}

/*
 * Error details
 */
return [
    // Apache errors.
    400 => [
        'title' => 'Bad Request',
        'info'  => 'The server did not understand the request.',
    ],
    401 => [
        'title' => 'Authorisation Required',
        'info'  => 'The requested page needs a username and a password.',
    ],
    403 => [
        'title' => 'Forbidden',
        'info'  => 'Access is forbidden to the requested page.',
    ],
    404 => [
        'title' => 'Not Found',
        'info'  => 'The server can not find the requested page.',
    ],
    405 => [
        'title' => 'Method Not Allowed',
        'info'  => 'The method specified in the request is not allowed.',
    ],
    406 => [
        'title' => 'Not Acceptable',
        'info'  => 'The server can only generate a response that is not accepted by the client.',
    ],
    408 => [
        'title' => 'Request Timed Out',
        'info'  => 'The server can only generate a response that is not accepted by the client.',
    ],
    410 => [
        'title' => 'Gone',
        'info'  => 'The requested resource is no longer available at this location.',
    ],
    415 => [
        'title' => 'Unsupported Media Type',
        'info'  => 'The server received something in a format it does not support.',
    ],
    422 => [
        'title' => 'Unprocessable Entity',
        'info'  => 'The server was unable to process the request due to semantic, rather than syntactic, '
            . 'errors.',
    ],
    429 => [
        'title' => 'Too Many Requests',
        'info'  => 'The client has sent more requests than the server is prepared to serve.',
    ],
    500 => [
        'title' => 'Internal Server Error',
        'info'  => 'The request was not completed. The server met an unexpected condition.',
    ],
    501 => [
        'title' => 'Not Implemented',
        'info'  => 'The request was not completed. The server did not support the functionality required.',
    ],
    502 => [
        'title' => 'Bad Gateway',
        'info'  => 'The request was not completed. The server received an invalid response from the '
            . 'upstream server.',
    ],
    503 => [
        'title' => 'Service Unavailable',
        'info'  => 'The request was not completed. The server received an invalid response from the '
            . 'upstream server.',
    ],
    504 => [
        'title' => 'Gateway Timeout',
        'info'  => 'The gateway has timed out.',
    ],
    505 => [
        'title' => 'HTTP Version Not Supported',
        'info'  => 'The server is unable to serve the request using the requested version of the HTTP '
            . 'protocol.',
    ],

    // Internal - Security.
    'scR' => [
        'title' => 'Please Re-authenticate',
        'info' => 'You are trying to access a restricted page, but have not recently logged in. So we can '
            . 'confirm you have sufficient privileges, we ask you to log in again using the above form.',
    ],
    'sc0' => [
        'title' => 'Security Concern',
        'info'  => 'We apologise for the disturbance, but a potential breach in security has been detected '
            . 'so you have been automatically logged out as a precaution. To continue, please '
            . '{link-js|link-signin|log back in}.',
    ],
    'sc1' => [
        'title' => 'User Not Signed In',
        'info' => 'You need to be signed in to view this page. Please '
            . '{link-js|link-signin|sign in} '
            . 'above{if|config:debear.links.register.enabled|, or click {link|/register|Register} '
            . 'to create yourself a {config|debear.names.site} user account!|.}',
    ],
    'sc2' => [
        'title' => 'Insufficient Privileges',
        'info'  => 'Sorry, but you do not have sufficient privileges to view this page! Please contact '
            . 'the Administrator if you think this is an error. Click {link-js|link-back|here} '
            . 'to go back to previous page.',
    ],

    // Internal - Emails.
    'em0' => [
        'title' => 'Unknown Email',
        'info'  => 'The requested email could not be found in the system.  Please check the link and try '
            . 'again.  If the problem persists, please contact the {email|support|Support Team}.',
    ],
    'em1' => [
        'title' => 'Unknown Link',
        'info'  => 'The requested link could not be found in the system.  Please check the email and try '
            . 'again.  If the problem persists, please contact the {email|support|Support Team}.',
    ],

    // Internal - Calendar object.
    'cal0' => [
        'title' => 'Unable to create Calender',
        'info'  => 'No "name" argument passed.',
    ],
];
