<?php

use DeBear\Helpers\HTTP;

if (!isset($debear_subload)) {
    return config('debear.cms');
}

/*
 * CMS
 */
return [
    'missing-exceptions' => [
        'page' => !HTTP::isLive(),
        'section' => !HTTP::isLive(),
    ],
];
