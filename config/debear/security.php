<?php

if (!isset($debear_subload)) {
    return config('debear.security');
}

/*
 * Security / User settings
 */
return [
    // Suscpicious activity limits before we deem them violations.
    'activity_limits' => [
        'failed_login'           => ['period' => 5,  'user_id' => 3, 'session_id' => 5,  'remote_ip' => 8],
        'account_registration'   => ['period' => 2,  'user_id' => 5, 'session_id' => 5,  'remote_ip' => 8],
        'account_update'         => ['period' => 15, 'user_id' => 5, 'session_id' => 10, 'remote_ip' => 15],
        'account_reset_request'  => ['period' => 5,  'user_id' => 3, 'session_id' => 5,  'remote_ip' => 8],
        'account_password_reset' => ['period' => 5,  'user_id' => 3, 'session_id' => 5,  'remote_ip' => 8],
        'security'               => ['period' => 1,  'user_id' => 1, 'session_id' => 1,  'remote_ip' => 1],
        'scraping_attempt'       => ['period' => 1,  'user_id' => 1, 'session_id' => 1,  'remote_ip' => 1],
    ],
    // Password related details.
    'password' => [
        'min_length' => 8,
        'breach_threshold' => 10, // May be included in less than this number of breaches and be deemed acceptable.
        'recency' => 1200, // Considered "recent" for 20 minutes after entering a password.
        // Stub the callout to the live provider in CI, so define the list of breached passwords in CI.
        'ci_mock_breached' => [
            'basic123',
        ],
    ],
];
