<?php

use DeBear\Helpers\HTTP;

if (!isset($debear_subload)) {
    return config('debear.email');
}

/*
 * Relevant email details
 */
return [
    'enabled' => HTTP::isLive(),
    'default' => 'noreply',
    'addresses' => [
        'support' => 'support',
        'privacy' => 'privacy',
        'registration' => 'registration',
        'noreply' => 'noreply',
    ],
];
