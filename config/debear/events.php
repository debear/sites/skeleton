<?php

use DeBear\Helpers\HTTP;

if (!isset($debear_subload)) {
    return config('debear.events');
}

/*
 * Laravel Event logic
 */
return [
    'listeners' => [],
    'subscribers' => [],
    // Config to include within CI jobs.
    'ci' => [
        'listeners' => [
            \Tests\PHPUnit\Setup\Events\TestFail::class => [
                \Tests\PHPUnit\Setup\Listeners\TestListener::class,
            ],
        ],
    ],
];
