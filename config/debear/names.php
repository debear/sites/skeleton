<?php

if (!isset($debear_subload)) {
    return config('debear.names');
}

/*
 * Site name details
 */
return [
    'site' => 'DeBear.uk',
    'company' => 'DeBear Software',
    'section' => 'DeBear.uk',
];
