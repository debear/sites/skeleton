<?php

use DeBear\Helpers\HTTP;

if (!isset($debear_subload)) {
    return config('debear.analytics');
}

/*
 * Google Analytics
 */
return [
    'enabled' => [
        'v3' => false, // Universal Analytics ("V3").
        'v4' => HTTP::isLive(), // Google Analytics 4 ("V4").
    ],
    'urchins' => [
        'testing' => [
            'UA-48789782-9', // V3.
            'G-PX81X2TMQ2', // V4.
        ],
        'tracking' => [
            'G-ZEM9L4RNBY', // V4.
        ],
    ],
];
