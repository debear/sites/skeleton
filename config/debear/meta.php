<?php

if (!isset($debear_subload)) {
    return config('debear.meta');
}

/*
 * Meta tag and image settings
 */
return [
    'image' => [
        'main' => 'skel/meta/debear.png',
        'width' => 300,
        'height' => 300,
    ],
    'twitter' => [
        'card' => 'summary',
    ],
    'og' => [
        'type' => 'article',
    ],
    'social' => [
        'show' => true,
    ],
    'sizes' => [ 512 => [ 'w' => 512, 'h' => 512 ],
                 384 => [ 'w' => 384, 'h' => 384 ],
                 256 => [ 'w' => 256, 'h' => 256 ],
                 192 => [ 'w' => 192, 'h' => 192 ],
                 180 => [ 'w' => 180, 'h' => 180 ],
                 152 => [ 'w' => 152, 'h' => 152 ],
                 144 => [ 'w' => 144, 'h' => 144 ],
                 120 => [ 'w' => 120, 'h' => 120 ],
                 114 => [ 'w' => 114, 'h' => 114 ],
                  76 => [ 'w' =>  76, 'h' =>  76 ],
                  72 => [ 'w' =>  72, 'h' =>  72 ],
                  57 => [ 'w' =>  57, 'h' =>  57 ], ],
    'theme_colour' => '#880000',
];
