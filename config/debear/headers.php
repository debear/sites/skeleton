<?php

use DeBear\Helpers\HTTP;

if (!isset($debear_subload)) {
    return config('debear.headers');
}

/*
 * HTTP Header config
 */
return [
    // Content Security Policy.
    'csp' => [
        'enabled' => true,
        'report_only' => false,
        'report_uri' => false,
        'nonce' => 'NONCE',
        'policies' => [
            'default-src' => [
                '\'none\'',
            ],
            'script-src' => [
                '\'self\'',
                '\'nonce-{{NONCE}}\'',
                '{{DOMAIN_STATIC}}',
                'https://*.googletagmanager.com',
            ],
            'style-src' => [
                '\'self\'',
                '\'nonce-{{NONCE}}\'',
                '{{DOMAIN_STATIC}}',
            ],
            'connect-src' => [
                '\'self\'',
                'https://*.googletagmanager.com',
                'https://*.google-analytics.com',
                'https://*.analytics.google.com',
            ],
            'frame-src' => [
                '\'self\'',
            ],
            'img-src' => [
                '\'self\'',
                '{{DOMAIN_STATIC}}',
                '{{DOMAIN_CDN}}',
                'https://*.googletagmanager.com',
                'https://*.google-analytics.com',
            ],
            'font-src' => [
                '\'self\'',
                '{{DOMAIN_STATIC}}',
            ],
            'manifest-src' => [
                '\'self\'',
            ],
            'upgrade-insecure-requests' => true,
            'form-action' => [
                '\'self\'',
            ],
        ],
    ],

    // Permissions Policy.
    'permissions_policy' => [
        'enabled' => true,
        'policies' => [
            'sync-xhr' => [
                'self',
            ],
            'fullscreen' => [
                'self',
            ],
            'geolocation' => [],
            'autoplay' => [],
            'camera' => [],
            'microphone' => [],
            'midi' => [],
            'usb' => [],
            'accelerometer' => [],
            'gyroscope' => [],
            'magnetometer' => [],
            'picture-in-picture' => [],
            'payment' => [],
        ],
    ],

    // Expect-CT config.
    'expect_ct' => [
        'enabled' => false,
        'report_only' => true,
        'max_age' => 30, // Over-ridden with 0 if report_only set.
        'report_uri' => '/report-uri/ct',
    ],

    // Standard headers to issue.
    'standard' => [
        // Strict-Transport-Security (https://scotthelme.co.uk/hsts-the-missing-link-in-tls/).
        'Strict-Transport-Security' => 'max-age=31536000; includeSubDomains',
        // X-Frame-Options (https://scotthelme.co.uk/hardening-your-http-response-headers/#x-frame-options).
        'X-Frame-Options' => 'SAMEORIGIN',
        // X-Content-Type-Options.
        // (https://scotthelme.co.uk/hardening-your-http-response-headers/#x-content-type-options).
        'X-Content-Type-Options' => 'nosniff',
        // Referrer-Policy (https://scotthelme.co.uk/a-new-security-header-referrer-policy/).
        'Referrer-Policy' => 'strict-origin-when-cross-origin',
    ],
];
