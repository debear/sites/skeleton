<?php

if (!isset($debear_subload)) {
    return config('debear.links');
}

/*
 * Various Nav/Header/Footer/Sitemap links
 */
return [
    'login' => [
        'label' => 'Sign in',
        'order' => 10,
        'policy' => 'guest',
        'header' => true,
        'navigation' => [
            'enabled' => true,
            'class' => 'mobile-only onclick_target',
            'icon' => 'right_nav_expand subnav',
        ],
    ],
    'home' => [
        'url' => '/',
        'title' => 'Our Homepage',
        'label' => 'Home',
        'descrip' => 'A DeBear.uk site',
        'order' => 20,
        'footer' => [
            'enabled' => false,
        ],
        'navigation' => [
            'enabled' => true,
        ],
        'sitemap' => [
            'enabled' => true,
            'priority' => 0.8,
            'frequency' => 'monthly',
        ],
    ],
    'register' => [
        'url' => '/register',
        'domain' => 'my',
        'label' => 'Register',
        'order' => 80,
        'policy' => 'guest',
        'header' => true,
    ],
    'account' => [
        'url' => '/account',
        'domain' => 'my',
        'label' => 'My Account',
        'order' => 85,
        'policy' => 'user:unrestricted', // Do not include for Restricted users.
        'header' => true,
        'navigation' => [
            'enabled' => true,
            'class' => 'mobile-only',
        ],
    ],
    'resend-verify' => [
        'label' => 'Email Address Not Verified',
        'order' => 86,
        'policy' => 'user:unverified',
        'navigation' => [
            'enabled' => true,
            'class' => 'unverified mobile-only onclick_target',
            'icon' => 'user_unverified icon_right_nav_expand subnav',
        ],
    ],
    'logout' => [
        'label' => 'Sign Out',
        'policy' => 'user',
        'order' => 90,
        'header' => true,
        'navigation' => [
            'enabled' => true,
            'class' => 'mobile-only',
        ],
    ],
    'admin' => [
        'url' => false,
        'title' => 'Administrative',
        'order' => 92,
        'descrip' => 'Links to administrative tools and pages to help you around our site',
        'navigation' => [
            'enabled' => true,
            'title' => 'Useful Links',
            'class' => 'mobile-only',
        ],
        'sitemap' => [
            'enabled' => true,
        ],
    ],
    'sitemap' => [
        'url' => '/sitemap',
        'title' => 'A Sitemap of our website',
        'label' => 'Sitemap',
        'descrip' => 'Sitemap detailing the available content on {config|debear.names.site}',
        'order' => 94,
        'section' => 'admin',
        'footer' => true,
        'navigation' => [
            'enabled' => true,
        ],
        'sitemap' => [
            'enabled' => true,
            'priority' => 0.2,
            'frequency' => 'monthly',
        ],
    ],
    'terms' => [
        'url' => '/terms-of-use',
        'title' => 'Our Terms and Conditions',
        'label' => 'Terms of Use',
        'descrip' => 'The Terms of Use Policy for {config|debear.names.site}',
        'order' => 96,
        'section' => 'admin',
        'footer' => true,
        'navigation' => [
            'enabled' => true,
        ],
        'sitemap' => [
            'enabled' => true,
            'priority' => 0.3,
            'frequency' => 'yearly',
        ],
    ],
    'privacy' => [
        'url' => '/privacy-policy',
        'title' => 'Our Privacy Policy',
        'label' => 'Privacy Policy',
        'descrip' => 'The Privacy Policy for {config|debear.names.site}',
        'order' => 98,
        'section' => 'admin',
        'footer' => true,
        'navigation' => [
            'enabled' => true,
        ],
        'sitemap' => [
            'enabled' => true,
            'priority' => 0.3,
            'frequency' => 'yearly',
        ],
    ],
    'status' => [
        'domain' => 'status',
        'url' => '/',
        'label' => 'System Status',
        'new_page' => true,
        'order' => 99,
        'footer' => true,
    ],
];
