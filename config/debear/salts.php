<?php

if (!isset($debear_subload)) {
    return config('debear.salts');
}

/*
 * Various salting instances
 */
return [
    'session_ua' => env('SALT_SESSION_UA'),
    'hasher' => env('DB_DATABASE'),
];
