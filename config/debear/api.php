<?php

/*
 * API settings
 */
return [
    'rate_limits' => [
        DeBear\Helpers\API\RateLimit::class,
    ],
    // Individual threshold levels.
    'thresholds' => [
        'unit_tests' => env('API_RATELIMIT', 120), // On average, two per second. Intended for testing only.
        'generic_read' => env('API_RATELIMIT', 12), // On average, one every five (5) seconds.
        'generic_write' => env('API_RATELIMIT', 4), // On average, one every 15 seconds.
    ],
];
