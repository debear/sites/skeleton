<?php

use DeBear\Helpers\HTTP;

if (!isset($debear_subload)) {
    return config('debear.datetime');
}

/*
 * Date/Time configuration
 */
$default = config('app.timezone');
return [
    'timezone_server' => $default,
    'timezone_app' => $default,
    'timezone_db' => $default,
];
