<?php

if (!isset($debear_subload)) {
    return config('debear.subdomains');
}

/*
 * The sub-domains we have in our site
 */
return [
    'www' => '',
    'my' => 'my',
    'hollyandmax' => 'hollyandmax',
    'status' => 'status',
    'sysmon' => 'sysmon',
    'sports' => 'sports',
    'fantasy' => 'fantasy',
];
