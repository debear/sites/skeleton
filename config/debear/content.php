<?php

if (!isset($debear_subload)) {
    return config('debear.content');
}

/*
 * Page content values
 */
return [
    'is_popup' => false,
    'body_classes' => [],
    'breadcrumbs' => [
        'raw' => [],
        'parsed' => [],
    ],
    'title' => [
        'raw' => [],
    ],
    'descrip' => '',
    'header' => [
        'url' => '',
        'no_logo_link' => [], // Defined as keys to the 'dirs' array that we parse later.
        'prenav' => '',
        'notices' => '',
        'nav' => '',
        'subnav' => '',
    ],
    'footer' => [
        'content' => '', // The second line of the footer.
        'blocks' => [],
    ],
    'sitemap' => [
        'view' => [
            'default' => 'skeleton.pages.sitemap',
            'override' => false,
        ],
    ],
];
