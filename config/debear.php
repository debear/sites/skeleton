<?php

/**
 * DeBear configuration rules
 */

use DeBear\Helpers\Arrays;
use DeBear\Helpers\Config;
use DeBear\Helpers\HTTP;

// Skeleton folder differs on CI (helpfully).
$full_path = explode('/', __DIR__);
$skel_dir = $full_path[sizeof($full_path) - 2];

// Load the config from the sub-files.
$debear = [];
$debear_subload = true;
foreach (glob(__DIR__ . '/debear/*.php') as $sub) {
    $section = pathinfo($sub);
    if (substr($section['filename'], 0, 1) != '_') {
        $debear[$section['filename']] = require($sub);
    }
}
unset($debear_subload);

// Process for any dynamic changes before returning.
return Config::postProcess($debear);
