@if ($item['url'])
    <url>
        <loc>{!! Sitemap::convertURL($item['url']) !!}</loc>
        <changefreq>{!! $item['frequency'] !!}</changefreq>
        <priority>{!! $item['priority'] !!}</priority>
    </url>
@endif

{{-- Its children --}}
@if (sizeof($item['links']))
    @each('skeleton.pages.sitemap.item_xml', $item['links'], 'item')
@endif
