@php
    $has_url = isset($item['url']) && is_string($item['url']) && $item['url'];
    $inc_children = (bool)sizeof($item['links']);
    $toggle_closed = ($inc_children && isset($item['sitemap']['toggle_closed']) && $item['sitemap']['toggle_closed']);

    $toggle_class = '';
    if ($inc_children) {
        $toggle_class = 'icon_toggle_' . ($toggle_closed ? 'plus' : 'minus');
    }
@endphp
<li class="item">
    <div class="toggle {!! $toggle_class !!}"></div>
    @if ($has_url)
        <a href="{!! str_replace('"', '\'', $item['url']) !!}">
    @endif
    <strong>{!! $item['label'] ?? $item['title'] !!}</strong>
    @if ($has_url)
        </a>
    @endif

    @if ($item['descrip'])
        <em class="descrip">{!! Interpolate::string($item['descrip']) !!}</em>
    @endif

    @if (sizeof($item['links']))
        <ul class="inline_list children {!! $toggle_closed ? 'closed' : 'open' !!}">
            @each('skeleton.pages.sitemap.item', $item['links'], 'item')
        </ul>
    @endif
</li>
