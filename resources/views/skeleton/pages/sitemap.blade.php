@extends('skeleton.layouts.html')

@section('content')

@php
    $name = FrameworkConfig::get('debear.names.site');
    if (FrameworkConfig::get('debear.names.section') && (FrameworkConfig::get('debear.names.section') != FrameworkConfig::get('debear.names.site'))) {
        $name .= ' ' . FrameworkConfig::get('debear.names.section');
    }
@endphp
<h1>Find your way round {!! $name !!}!</h1>

<ul class="inline_list sitemap" id="sitemap">
    @each('skeleton.pages.sitemap.item', $sitemap, 'item')
</ul>

@endsection
