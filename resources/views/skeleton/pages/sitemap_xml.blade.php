@php
    print '<'.'?xml version="1.0" encoding="UTF-8"?'.'>'; // Quirky to satisfy linting
@endphp
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    @each('skeleton.pages.sitemap.item_xml', $sitemap, 'item')
</urlset>
