{!!$doctype!!}
<html>
    <head>
        <title>{!!$email->email_subject!!}</title>
        <link rel="stylesheet" type="text/css" href="{!! HTTP::buildStaticURLs('css', 'skel/pages/email.css') !!}" />
    </head>
    <body>
        <dl class="message_header">
            <dt>From:</dt><dd>{!!preg_replace('/&quot;(.+)&quot;/', '&quot;<em>$1</em>&quot;', $email->email_from)!!}</dd>
            <dt>To:</dt><dd>{!!preg_replace('/&quot;(.+)&quot;/', '&quot;<em>$1</em>&quot;', $email->email_to)!!}</dd>
            <dt>Subject:</dt><dd>{!!$email->email_subject!!}</dd>
            <dt>{!! ucfirst($send_col) !!}:</dt><dd>{!! date('D jS M Y \a\t H:i', strtotime($email->$send_col_obj)) !!}</dd>
        </dl>
        <div class="message">
            {!! $content !!}
        </div>
    </body>
</html>
