@extends('skeleton.layouts.popup')

@section('content')

<h1>Cookie usage by {!! $name !!}</h1>

{!! CMS::getSection('/cookies', 'intro', ['site' => '_skel', 'interpolate' => compact('name')]) !!}

<cookies>
    <fieldset>
        <legend>
            <input type="checkbox" id="for_required" class="checkbox" checked="checked" disabled>
            <label for="for_required">Required Functionality</label>
        </legend>
        {!! CMS::getSection('/cookies', 'cookies-required', ['site' => '_skel']) !!}
    </fieldset>

    <fieldset>
        <legend>
            <input type="checkbox" id="for_analytics" class="checkbox" {!! \DeBear\Helpers\CookiePreferences::get('allow-analytics') ? 'checked="checked"' : '' !!}>
            <label for="for_analytics">Analytics Tracking</label>
            <div class="hidden box"></div>
        </legend>
        {!! CMS::getSection('/cookies', 'cookies-analytics', ['site' => '_skel']) !!}
    </fieldset>
</cookies>

@endsection
