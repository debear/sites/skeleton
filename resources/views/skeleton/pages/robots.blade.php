#
# DeBear robots.txt
#

# With the exception of the Twitterbot...
User-agent: Twitterbot
Disallow:

# ...hide all images from image searches
User-agent: *
Disallow: /images/
