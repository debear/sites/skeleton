{{-- XSS Preventation confirmation --}}
Argument: {!! $content !!}
Model (In): {!! $model_in->content !!} (Section (In): {!! $model_in->section !!})
Model (Out): {!! $model_out->content !!} (Section (Out): {!! $model_out->section !!})
ORM (In): {!! $orm_in->content !!} (Section (In): {!! $orm_in->section !!})
ORM (Out): {!! $orm_out->content !!} (Section (Out): {!! $orm_out->section !!})
