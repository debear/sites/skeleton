@php
    // Add our resources
    Resources::addCSS('skel/widgets/subnav.css');
    Resources::addJS('skel/widgets/subnav.js');
    // CSS
    $css = (isset($subnav['tabs-below']) && $subnav['tabs-below'] ? 'below' : 'above');
    if (isset($subnav['css']) && is_string($subnav['css']) && $subnav['css']) {
        $css .= ' ' . $subnav['css'];
    }
    // What are the breakpoints between mobile and desktop versions?
    $mob_css = 'hidden-d hidden-tl';
    $desk_css = 'hidden-m hidden-tp';
    if (isset($subnav['breakpoint'])) {
        // Check for non-default options
        switch ($subnav['breakpoint']) {
            case 'mobile':
                $mob_css = 'hidden-d hidden-tl hidden-tp';
                $desk_css = 'hidden-m';
                break;
            case 'tablet-landscape':
                $mob_css = 'hidden-d';
                $desk_css = 'hidden-m hidden-tp hidden-tl';
                break;
        }
    }
@endphp
<subnav class="{!! $css !!}">
    {{-- Mobile Version --}}
    <mob class="{!! $mob_css !!}">
        <sel>
            {!! $subnav['list'][$subnav['default']] !!}
        </sel>
        <select id="subnav">
            @foreach ($subnav['list'] as $id => $name)
                <option value="{!! $id !!}" {!! $subnav['default'] == $id ? 'selected="selected"' : '' !!}>{!! strip_tags($name) !!}</option>
            @endforeach
        </select>
    </mob>
    {{-- Desktop Version --}}
    <desk class="{!! $desk_css !!}">
        @foreach ($subnav['list'] as $id => $name)
            <sep class="{!! $loop->index ? 'mid' : 'left' !!}"></sep>
            <item class="{!! $subnav['default'] == $id ? 'sel' : 'unsel' !!}" data-tab="{!! $id !!}">{!! $name !!}</item>
        @endforeach
        <sep class="right"></sep>
    </desk>
</subnav>
