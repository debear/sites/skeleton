@php
    // Some maths
    $uniq_pct = 100 * ($report['conn']['query_uniq'] / $report['conn']['query_tot']);
    // State checks
    $state_query_uniq = ($uniq_pct < 25 ? 'bad' : ($uniq_pct < 50 ? 'warn' : ''));
    $state_time_tot = ($report['conn']['time_tot'] > 2 ? 'bad' : ($report['conn']['time_tot'] > 1 ? 'warn' : ''));
    $state_slow = ($report['conn']['query_slow'] ? 'bad' : '');
@endphp
<dl class="individual">
    {{-- Headline figures --}}
    <dt class="headline">Queries:</dt>
        <dd class="headline">{!! $report['conn']['query_tot'] !!}</dd>
    <dt class="headline {!! $state_query_uniq !!}">Unique:</dt>
        <dd class="headline {!! $state_query_uniq !!}">{!! $report['conn']['query_uniq'] !!} ({!! sprintf('%0.02f%%', $uniq_pct) !!})</dd>
    <dt class="headline {!! $state_time_tot !!}">Total Time:</dt>
        <dd class="headline {!! $state_time_tot !!}">{!! sprintf('%0.03f', $report['conn']['time_tot']) !!}s<!-- TotalTime: {!! $conn !!} --></dd>
    <dt class="headline {!! $state_slow !!}">Max Time:</dt>
        <dd class="headline {!! $state_slow !!}">{!! sprintf('%0.03f', $report['conn']['time_max']) !!}s</dd>
    <dt class="headline {!! $state_slow !!}">Slow Queries:</dt>
        <dd class="headline {!! $state_slow !!}">{!! $report['conn']['query_slow'] !!}</dd>

    {{-- Individual queries --}}
    @foreach ($report['queries'] as $ref => $query)
        @php
            $state = 'okay';
            if ($query['query_slow']) {
                $state = 'bad';
            } elseif ($query['query_tot'] > 4) {
                $state = 'warn';
            } elseif ($query['time_tot'] > 0.5) {
                // Slow... but acceptable?
                $state = 'warn';
            }
        @endphp
        <dt class="query {!! $state !!}" data-query="{!! $ref !!}">{!! $ref !!}:</dt>
            <dd class="query {!! $state !!} ref-{!! $ref !!} summary" data-query="{!! $ref !!}">
                Called: x{!! $query['query_tot'] !!}
                (Unique: {!! $query['query_args'] !!}, {!! sprintf('%0.02f%%', 100 * ($query['query_args'] / $query['query_tot'])) !!});
                Time: {!! sprintf('%0.03f', $query['time_tot']) !!}s
                (Max: {!! sprintf('%0.03f', $query['time_max']) !!}s; Slow: {!! $query['query_slow'] !!})
            </dd>
            <dd class="query {!! $state !!} ref-{!! $ref !!} sql hidden">
                <code>{!! $query['sql'] !!}</code>
            </dd>
    @endforeach
</dl>
