@php
    $debug = Policies::getDebug();
    $matches = $debug['matches'];
@endphp
<ul class="policies">
    @foreach ($debug['policies'] as $code => $policy)
        @include('skeleton.widgets.debug.policies.item', ['key' => $code, 'item' => $policy])
    @endforeach
</ul>
