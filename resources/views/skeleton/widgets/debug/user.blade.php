@php
    $user = \DeBear\Models\Skeleton\User::getDebug();
@endphp
@if (sizeof($user))
    <dl class="user">
        @foreach ($user as $key => $value)
            <dt>{!! $key !!}:</dt>
                <dd>
                    @if (!isset($value))
                        <em>Unset</em>
                    @elseif (is_object($value))
                        {!! $value->toString() !!}
                    @elseif (is_array($value))
                        {!! json_encode($value) !!}
                    @else
                        {!! $value !!}
                    @endif
                </dd>
        @endforeach
    </dl>
@endif
