@php
    // CSS class for this policy?
    if (!isset($matches[$key])) {
        $class = 'grouping';
    } elseif ($matches[$key]) {
        $class = 'on';
    } else {
        $class = 'off';
    }
@endphp
<li>
    <span class="{!! $class !!}" title="{!! $key !!}">
        <strong>{!! $item['name'] !!}</strong>
    </span>
    @php
        if (!isset($item['children']) || !is_array($item['children'])) {
            $item['children'] = [];
        }
    @endphp
    @if (sizeof($item['children']))
        <ul>
            @foreach ($item['children'] as $code => $child)
                @include('skeleton.widgets.debug.policies.item', ['key' => $code, 'item' => $child])
            @endforeach
        </ul>
    @endif
</li>
