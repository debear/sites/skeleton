@php
    $reports = \DeBear\Repositories\DatabaseLogging::getReport();
@endphp
<dl class="subtable queries">
    @if(count($reports))
        {{-- Connection Heading(s) --}}
        @foreach (array_keys($reports) as $conn)
            <dt class="subtitle {!! $loop->first ? 'sel' : '' !!}" data-code="{!! $conn !!}">{!! $conn !!}</dt>
        @endforeach

        {{-- Report --}}
        @foreach ($reports as $conn => $report)
            <dd class="subcell {!! $conn !!} {!! $loop->first ? '' : 'hidden' !!}" data-code="{!! $conn !!}">
                @include('skeleton.widgets.debug.queries.item')
            </dd>
        @endforeach
    @else
        {{-- No queries made --}}
        <dd class="none">No queries made.</dd>
    @endif
</dl>
