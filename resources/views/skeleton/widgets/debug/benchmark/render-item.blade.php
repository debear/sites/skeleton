<li class="name" title="{!! $item['name'] !!}">
    {!! $item['name'] !!}
</li>
<li class="runtime">
    {!! sprintf('%0.03fs', $item['runtime']) !!}
</li>
<li class="percentage">
    {!! sprintf('%0.02f%%', 100 * $item['percentage']) !!}
</li>

@if (count($item['children']))
    <li class="children">
        @include('skeleton.widgets.debug.benchmark.render', ['stamps' => $item['children']])
    </li>
@endif
