@php
    // Add our resources
    Resources::addCSS('skel/widgets/calendar.css');
    Resources::addJS('skel/widgets/calendar.js');
@endphp
<calendar data-min="{!! $calendar['min'] !!}" data-current="{!! $calendar['current'] !!}" data-max="{!! $calendar['max'] !!}" {!! isset($calendar['json']) ? "data-json=\"{$calendar['json']}\"" : '' !!}><a class="icon_calendar no_underline month-toggle"></a><div class="cal-body hidden"></div></calendar>
