<scroller class="box section" data-min="1" data-max="{!! sizeof($scroller['list']) !!}" data-curr="{!! $scroller['default'] !!}" data-resp='@json($scroller['resp-widths'])' data-interval="{!! $scroller['interval'] !!}">
    <prev class="onclick_target prev-next {!! $scroller['prev-next'] !!}"><i class="fas fa-chevron-circle-left"></i></prev>
    <block>
        @foreach ($scroller['list'] as $i => ${$scroller['item-variable']})
            @include($scroller['item-view'])
        @endforeach
    </block>
    <next class="onclick_target prev-next {!! $scroller['prev-next'] !!}"><i class="fas fa-chevron-circle-right"></i></next>
    <bullets>
        @for ($i = 1; $i <= sizeof($scroller['list']); $i++)
            <bullet class="onclick_target" data-ref="{!! $i !!}">
                <i class="fas active fa-dot-circle"></i>
                <i class="far inactive fa-dot-circle"></i>
                <i class="fas hover fa-dot-circle"></i>
            </bullet>
        @endfor
    </bullets>
</scroller>
