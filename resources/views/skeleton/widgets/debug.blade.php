@php
    $sections = [
        [
            'label' => 'User',
            'template' => 'user',
            'enabled' => Policies::match('logged_in'),
        ],
        [
            'label' => 'Policies',
            'template' => 'policies',
        ],
        [
            'label' => 'Session',
            'template' => 'session',
            'enabled' => (bool)Request::input('debug_session'),
        ],
        [
            'label' => 'Queries',
            'template' => 'queries',
            'enabled' => \DeBear\Repositories\DatabaseLogging::enabledDisplay(),
        ],
        [
            'label' => 'Benchmark',
            'template' => 'benchmark',
            'enabled' => Benchmark::enabledDisplay(),
        ],
        [
            'label' => 'phpInfo()',
            'template' => 'phpinfo',
            'enabled' => HTTP::isDev() && (bool)Request::input('phpInfo'),
        ],
    ];
    // Prune those not to include
    foreach ($sections as $i => $sect)
        if (isset($sect['enabled']) && !$sect['enabled'])
            unset($sections[$i]);
@endphp
<dl class="debug hidden">
    {{-- Header Rows --}}
    @foreach ($sections as $i => $sect)
        <dd class="cell {!! $sect['template'] !!} {!! !$loop->first ? 'hidden' : '' !!}" data-code="{!! $sect['template'] !!}">
            @include("skeleton.widgets.debug.{$sect['template']}")
        </dd>
    @endforeach

    {{-- Title Rows --}}
    @foreach ($sections as $sect)
        <dt class="title {!! $sect['template'] !!} {!! $loop->first ? 'sel' : '' !!}" data-code="{!! $sect['template'] !!}">
            {!! $sect['label'] !!}
        </dt>
    @endforeach
</dl>
