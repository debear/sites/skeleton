<ul class="inline_list pagination {!! $extra_css !!} onclick_target" id="pagination">
    @if ($page_num > 1)
        <li class="previous_page shaded_row">
            <a {!! Pagination::buildLinkAttribute($url, $page_num - 1, 'prev') !!} class="onclick_target" data-page="{!! $page_num - 1 !!}">
                &laquo; {!! $page_num == 2 ? 'First' : 'Prev' !!}<span class="hidden-m">{!! $page_num > 2 ? 'ious' : '' !!} Page</span>
            </a>
        </li>
    @endif

    <li class="page_title unshaded_row">
        Pages:
    </li>

    @foreach ($ind_pages as $page)
        <li class="page_box {!! $page['shade'] ? '' : 'un' !!}shaded_row {!! $page['narrow'] ? '' : 'not-narrow' !!}">
            {!! $page['title'] !!}
        </li>
    @endforeach

    @if ($next_rows > 0)
        <li class="next_page shaded_row">
            <a {!! Pagination::buildLinkAttribute($url, $page_num + 1, 'next') !!} class="onclick_target" data-page="{!! $page_num + 1 !!}">
                {!! $next_title !!}<span class="hidden-m"> Page</span> &raquo;
            </a>
        </li>
    @endif
</ul>
