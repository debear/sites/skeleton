<!DOCTYPE html>
<html>
<head>
    <title>Tweet Details</title>
    <style>
        body {
            font-family: Arial, "Nimbus Sans L";
            font-size: 10pt;
            margin: 0px;
            min-width: 520px;
        }

        ul.tweet,
        div.details div {
            background-color: #ffffff;
        }

        ul.tweet {
            border: 1px solid #888888;
            margin: 5px auto;
            padding: 5px;
            width: 500px;
        }

        ul.tweet li {
            list-style: none;
        }

        ul.tweet li.avatar,
        ul.tweet li.account,
        ul.tweet li.handle {
            float: left;
        }

        ul.tweet li.avatar,
        ul.tweet li.avatar img {
            height: 48px;
            width: 48px;
        }

        ul.tweet li.rt_account,
        ul.tweet li.avatar {
            margin-bottom: 3px;
        }

        ul.tweet li.account,
        ul.tweet li.handle {
            margin-left: 5px;
            width: 447px;
        }

        ul.tweet li.account {
            font-size: 16px;
            font-weight: bold;
            height: 24px;
            line-height: 24px;
        }

        ul.tweet li.handle {
            height: 16px;
            line-height: 16px;
        }

        ul.tweet li.body {
            clear: left;
        }

        ul.tweet li.body span {
            color: #00C;
        }

        ul.tweet li.rt_account,
        ul.tweet li.handle,
        ul.tweet li.when {
            color: #666666;
        }

        div.details,
        div.details div {
            height: 298px;
        }

        div.details {
            margin: 0px auto;
            width: 98%;
        }

        div.details div {
            border: 1px solid #888888;
            float: left;
            width: 49%;
        }

        div.details div.info {
            margin-left: 1%;
        }

        div.details h3 {
            height: 15px;
            margin: 5px 3px;
            text-align: center;
        }

        div.details pre {
            height: 273px;
            margin: 0px;
            overflow: scroll;
            width: 100%;
        }
    </style>
</head>
<body>
    <ul class="tweet">
        @if ($is_rt)
            <li class="rt_account">{!! $tweet_sent ? $response['user']['name'] . ' (@' . $response['user']['screen_name'] . ')' : $tweet['retweet_account'] !!} Retweeted</li>
        @endif
        <li class="avatar">
            @if ($tweet_sent)
                <img alt="{!! $orig_user['name'] !!}" src="{!! $orig_user['profile_image_url_https'] !!}">
            @endif
        </li>
        <li class="account">{!! $tweet_sent ? $orig_user['name'] : '' !!}</li>
        <li class="handle">{!! '@' . ($tweet_sent ? $orig_user['screen_name'] : $tweet['twitter_account']) !!}</li>
        <li class="body">{!! $tweet_sent ? App::make(\DeBear\Repositories\SocialMedia::class)->displayTweetBody($orig_tweet) : $tweet['tweet_body'] !!}</li>
        <li class="when">{!! date('H:i:s D jS M Y', strtotime($tweet['tweet_' . $send_col])) !!}{!! !$tweet_sent ? ' (Queued)' : '' !!}</li>
    </ul>

    @if ($tweet_sent)
        <div class="details">
            <div class="response">
                <h3>Response</h3>
                <pre>{!! json_encode($response, JSON_PRETTY_PRINT) !!}</pre>
            </div>
            <div class="info">
                <h3>Request Info</h3>
                <pre>{!! str_replace('\n', "\n", str_replace('        ', '  ', $tweet['twitter_return_info'])) !!}</pre>
            </div>
        </div>
    @endif
</body>
</html>
