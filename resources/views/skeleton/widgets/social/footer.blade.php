<ul class="inline_list social">
    @foreach ($links as $platform => $info)
        <li class="{!! $platform !!}">
            <a class="no_underline" href="{!! $info['url'] !!}" rel="noopener noreferrer" target="_blank"><div class="fab fa-{!! $platform !!}{!! $info['extra'] ?? '' !!}">{!! isset($info['text']) && is_string($info['text']) && $info['text'] ? '<span class="text reset_font">' . $info['text'] . '</span>' : ''!!}</div></a>
        </li>
    @endforeach
</ul>
