<input type="hidden" id="{!! $dropdown->getID() !!}" name="{!! $dropdown->getID() !!}" value="{!! $dropdown->getValue() !!}" data-is-dropdown="true" {!! $dropdown->renderRequired() !!}>
<dl class="dropdown {!! $dropdown->getClasses() !!}" data-id="{!! $dropdown->getID() !!}" {!! $dropdown->renderDisabled() !!} {!! $dropdown->getTabIndex() !!}>
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>{!! $dropdown->header() !!}</span>
    </dt>
    <dd class="hidden">
        <ul class="inline_list">
            @foreach ($dropdown->getOptions() as $item)
                <li {!! $item->renderClasses() !!} {!! $item->renderDataAttributes() !!}>{!! $item->label !!}</li>
            @endforeach
        </ul>
    </dd>
</dl>
