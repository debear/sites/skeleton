<input type="hidden" id="{!! $date_field->getID() !!}" name="{!! $date_field->getID() !!}" value="{!! $date_field->getValue() !!}" data-min="{!! $date_field->getMin() !!}" data-max="{!! $date_field->getMax() !!}" data-is-date="true" {!! $date_field->renderRequired() !!}>

<input class="numbox date_dd" type="num" id="{!! $date_field->getID() !!}_dd" value="{!! $date_field->getValueDD() !!}" autocomplete="bday-day" min="1" max="31" maxlength="2" {!! $date_field->getForm()->addTabindex() !!}>

{!! $date_field->getMMDropdown()->render() !!}

<input class="numbox date_yy" type="num" id="{!! $date_field->getID() !!}_yy" value="{!! $date_field->getValueYY() !!}" autocomplete="bday-year" min="{!! $date_field->getMinYY() !!}" max="{!! $date_field->getMaxYY() !!}" maxlength="4" {!! $date_field->getForm()->addTabindex() !!}>
