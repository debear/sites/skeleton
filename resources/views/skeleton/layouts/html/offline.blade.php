{{-- Offline alert? --}}
<offline class="invisible" id="device_offline">
  <div class="box status">
    <h3 class="icon icon_warning">No internet connection?</h3>
    <p class="message">It appears your device is no longer connected to the internet, so we probably won&#39;t be able to save data back to the server or show you anything new until you are connected to the internet again.</p>
  </div>
</offline>
