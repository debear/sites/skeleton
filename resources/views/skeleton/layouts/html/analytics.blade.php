{{-- Load the Google Analytics code --}}
@if (FrameworkConfig::get('debear.analytics.enabled.v3') || FrameworkConfig::get('debear.analytics.enabled.v4'))
    @php
        $ga_opt = [
            'app_name' => FrameworkConfig::get('debear.names.section') ?: FrameworkConfig::get('debear.names.site'),
            'app_version' => FrameworkConfig::get('debear.version.label.short'),
            'cookie_domain' => FrameworkConfig::get('debear.url.base'),
            'cookie_name' => FrameworkConfig::get('debear.sessions.cookies.analytics'),
        ];
        $urchins = [];
        $ga4_tag = null;
        foreach (HTML::getAnalyticsUrchin() as $i => $id) {
            $urchins[] = $id;
            $ga4_tag = (substr($id, 0, 2) == 'G-') ? $id : $ga4_tag;
        }
    @endphp
    <script async src="https://www.googletagmanager.com/gtag/js?id={!! $ga4_tag ?? $urchins[0] !!}" {!! HTTP::buildCSPNonceTag() !!}></script>
    <script {!! HTTP::buildCSPNonceTag() !!}>
        @if (!\DeBear\Helpers\CookiePreferences::get('allow-analytics'))
            Analytics.disable();
        @endif
        Analytics.setup(@json($urchins), @json($ga_opt));
    </script>
@elseif (HTTP::isDev())
    <!-- Analytics Disabled. Urchins: @json(FrameworkConfig::get('debear.analytics.urchins.raw')) -->
@endif
