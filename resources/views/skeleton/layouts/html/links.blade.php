<ul class="horiz_list links">
    @if ($title)
        <li class="title">
            {!! $title !!}
        </li>
    @endif
    @foreach ($links as $code => $link)
        @php
            $has_link = (isset($link['url']) && is_string($link['url']) && $link['url']);
            if ($has_link && isset($link['domain']) && $link['domain'] != FrameworkConfig::get('debear.url.sub')) {
                $link['url'] = HTTP::buildDomain($link['domain']) . $link['url'];
                $link['new_page'] = true; // Auto-set the new page flag, implied by the new domain
            }

            // What components do we need?
            $components = [];
            if (!$has_link) {
                $components[] = "id=\"{$type}_{$code}_link\"";
            } else {
                $components[] = "href=\"{$link['url']}\"";
            }
            if (isset($link['title'])) {
                $components[] = "title=\"{$link['title']}\"";
            }
            if (isset($link['new_page']) && $link['new_page']) {
                $components[] = 'target="_blank"';
                // If we're linking off-site, then hide the referer and DOM link to us
                if (!preg_match('/^(https?:)?\/\/(\w+\.)?' . FrameworkConfig::get('debear.url.base') . '\//', $link['url'])) {
                    $components[] = 'rel="noopener noreferrer"';
                }
            }
        @endphp
        <li class="link {!! $type !!}_{!! $code !!}">
            @includeIf("skeleton.layouts.html.links.$type.$code")
            <a {!! join(' ', $components) !!}>{!! $link['label'] !!}</a>
        </li>
    @endforeach
</ul>
