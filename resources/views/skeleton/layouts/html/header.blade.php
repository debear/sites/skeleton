{{-- Show the header bar --}}
<header id="header_bar">
    @include('skeleton.layouts.html.links', [
        'type' => 'header',
        'title' => HTML::getHeaderIntro(),
        'links' => HTML::getHeaderLinks(),
    ])
    @include('skeleton.layouts.html.header.breadcrumbs')
</header>
