<footer id="footer_bar" class="custom_{!! HTML::getFooterSize() !!} {!! HTML::firstCookieWarning() ? 'fixed_cookie_warning' : '' !!}">
    {{-- Custom footer blocks? --}}
    @foreach (HTML::getFooterBlocks() as $css => $msg)
        <div class="custom {!! $css !!})">
            {!! $msg !!})
        </div>
    @endforeach

    <links>
        @include('skeleton.layouts.html.links', [
            'type' => 'footer',
            'title' => '<strong>Useful Links:</strong>',
            'links' => HTML::getFooterLinks(),
        ])

        <version>
            <strong>Version: </strong> {!! FrameworkConfig::get('debear.version.label.full') !!}
            @if ($updated = FrameworkConfig::get('debear.version.updated'))
                <strong>Last Updated: </strong>{!! $updated !!}
            @endif
        </version>
    </links>

    <info>
        {{-- Show the social media info? --}}
        @php
            $num_platforms = App::make(\DeBear\Repositories\SocialMedia::class)->displayFooterWidget();
            $copyright = FrameworkConfig::get('debear.names.site') . ' &copy; ' . FrameworkConfig::get('debear.names.company') . ' MM' . Format::decimalToRoman(substr(Time::object()->getServerCurDate(), 2, 2)) . '.';
        @endphp
        <div class="info info_w{!! $num_platforms !!}_social">
            <copyright>
                <span class="main">{!! $copyright !!}</span>
                <span class="secondary">All Rights Reserved. No part of this website may be copied or reproduced without prior written permission of the copyright owner.</span>
            </copyright>
            {!! FrameworkConfig::get('debear.content.footer.content') !!}
        </div>
    </info>

    {{-- Cookie warning --}}
    @include('skeleton.layouts.html.footer.cookies')
</footer>
