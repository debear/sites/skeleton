<head>
    {{-- Resource Hints --}}
    @foreach (HTTP::resourceHints() as $domain)
        <link rel="dns-prefetch" href="{!! $domain !!}">
        <link href="{!! $domain !!}" rel="preconnect" crossorigin>
    @endforeach

    <meta name="Description" content="{!! HTML::getMetaDescription() !!}" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="csrf-token" content="{!! csrf_token() !!}">
    <meta name="viewport" content="width=device-width" />

    {{-- Social Media meta data --}}
    @if (HTML::showSocialMeta())
        <meta name="twitter:card" content="{!! HTML::getMetaTwitterCard() !!}" />
        <meta name="twitter:site" content="{!! '@' . (App::make(\DeBear\Repositories\SocialMedia::class)->getTwitterAccount('master') ?: 'thierrydraper') !!}" />
        <meta name="twitter:title" content="{!! HTML::getPageTitleText() !!}" />
        <meta name="twitter:description" content="{!! HTML::getMetaDescription() !!}" />
        <meta name="twitter:creator" content="{!! '@' . (App::make(\DeBear\Repositories\SocialMedia::class)->getTwitterAccount('master') ?: 'thierrydraper') !!}" />
        <meta name="twitter:image" content="{!! HTML::getMetaImageTwitter() !!}" />
        <meta property="og:title" content="{!! HTML::getPageTitleText() !!}" />
        <meta property="og:type" content="{!! HTML::getMetaOpenGraphType() !!}" />
        <meta property="og:url" content="https:{!! HTTP::buildDomain() !!}{!! Request::server('REQUEST_URI') !!}" />
        <meta property="og:image" content="{!! HTML::getMetaImageOpenGraph() !!}" />
        <meta property="og:image:width" content="{!! HTML::getMetaImageOpenGraphWidth() !!}" />
        <meta property="og:image:height" content="{!! HTML::getMetaImageOpenGraphHeight() !!}" />
        <meta property="og:description" content="{!! HTML::getMetaDescription() !!}" />
        <meta property="og:site_name" content="{!! FrameworkConfig::get('debear.names.site') !!}" />
    @endif
    <meta name="theme-color" content="{!! HTML::getMetaThemeColour() !!}" />
    <title>{!! HTML::getPageTitleText() !!}</title>

    {{-- Setup GA --}}
    @php
        HTML::setupAnalytics();
    @endphp

    {{-- Include the JavaScript and CSS files --}}
    @foreach (Resources::getJS() as $file)
        <script crossorigin="anonymous" src="{!! $file !!}" {!! HTTP::buildCSPNonceTag() !!}></script>
    @endforeach
    @foreach (Resources::getCSS() as $file)
        <link rel="stylesheet" type="text/css" href="{!! $file !!}" />
    @endforeach

    <link rel="icon" type="image/x-icon" href="/favicon.ico" />
    <link rel="manifest" href="{!! HTTP::buildSectionURL() !!}manifest.json" />
</head>
