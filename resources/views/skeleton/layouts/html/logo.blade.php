@php
    $link_href = HTML::getHeaderLink();
@endphp
<logo>
    @if ($link_href)
        <a href="{!! $link_href !!}">
    @endif
    <logo-img></logo-img>
    @if ($link_href)
        </a>
    @endif
</logo>
