{{-- Flag if the user has not yet verified their email address --}}
@if (\DeBear\Models\Skeleton\User::object()->isUnverified())
    <verify>
        <div class="box status head icon_user_unverified">Email Address Not Verified</div>
        <div class="box status body icon_warn">
            <p>Please click the link in the email you received to confirm ownership of the email address you entered in the registration process, or click Re-Send to have a new link sent to you. We ask this of you so we do not send emails to people who have not signed up, so please complete this process to ensure you receive emails from us!</p>
            <div>[ <span class="icon icon_refresh resend"><a>Re-Send Verification</a></span> ]</div>
        </div>
    </verify>
@endif
