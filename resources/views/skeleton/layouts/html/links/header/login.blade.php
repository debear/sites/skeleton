<login class="hidden">
    <form>
        <dl>
            <dt>
                Username:
            </dt>
            <dd>
                <input type="text" name="username" autocomplete="section-login username" required class="text_field username" maxlength="20" data-form-validate="on">
            </dd>

            <dt>
                Password:
            </dt>
            <dd>
                <input type="password" name="password" autocomplete="section-login current-password" required class="text_field password" maxlength="24" data-form-validate="on">
            </dd>

            <dt class="checkbox">
                <input type="checkbox" id="loginhead_rememberme" name="rememberme" class="checkbox rememberme">
                <label for="loginhead_rememberme" class="icon_right">Remember Me?</label>
            </dt>

            <dd class="submit">
                <button type="submit" class="btn_small submit" data-submit-label="Signing in...">Sign me in!</button>
            </dd>

            <dd class="links">
                <a class="forgotten">Forgotten details?</a>
                <a class="cancel">Cancel</a>
            </dd>
        </dl>
    </form>
</login>
<span class="login error hidden"></span>
