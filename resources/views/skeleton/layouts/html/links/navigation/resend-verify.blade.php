<verify class="hidden">
    <p>Please click the link in the email you received to confirm ownership of the email address you entered in the registration process, or click Re-Send to have a new link sent to you. We ask this of you so we do not send emails to people who have not signed up, so please complete this process to ensure you receive emails from us!</p>
    <a class="resend"><span class="invert icon_refresh">Re-Send Verification</span></a>
</verify>
