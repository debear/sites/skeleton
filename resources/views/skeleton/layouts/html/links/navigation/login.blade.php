<login class="hidden">
    <form>
        <dl class="clearfix">
            <dd class="user">
                <input type="text" name="username" placeholder="Username" autocomplete="section-login username" required class="text_field username" maxlength="20" data-form-validate="on">
            </dd>
            <dd class="pass">
                <input type="password" name="password" placeholder="Password" autocomplete="section-login current-password" required class="text_field password" maxlength="24" data-form-validate="on">
                <span class="password-toggle icon_password_show"></span>
            </dd>

            <dt class="checkbox">
                <input type="checkbox" id="loginnav_rememberme" name="rememberme" class="checkbox rememberme">
                <label for="loginnav_rememberme" class="icon_right">Remember Me?</label>
            </dt>

            <dd class="links">
                <a class="forgotten">Forgotten details?</a>
            </dd>

            <dd class="submit">
                <a class="submit" data-submit-label="Signing in...">Sign me in!</a>
            </dd>
        </dl>
    </form>
</login>
<div class="login error hidden">Invalid username/password combination</div>
