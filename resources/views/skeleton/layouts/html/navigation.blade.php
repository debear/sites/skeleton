@php
    $nav = Sitemap::buildNavigation();
    $nav_items = sizeof($nav);
    $resp_width = ($nav_items < 5 ? 'na' : 'wi'); // Deal with the 768px to 1002px range based on the number of items

    // Quick pre-check: are all the links mobile only?
    $mobile_only = $nav_items;
    foreach ($nav as $item) {
        $mobile_only = ($mobile_only && isset($item['class']) && is_string($item['class']) && strpos($item['class'], 'mobile-only') !== false);
    }
@endphp
@if ($nav_items)
    <nav class="{!! $resp_width !!} {!! $mobile_only ? 'mobile-only' : '' !!}">
        <menu>Menu</menu>
        <ul class="main nav">
            @foreach($nav as $code => $item)
                @php
                    $classes = [];
                    if (isset($item['sel']) && $item['sel']) {
                        $classes[] = 'sel';
                    }
                    if (isset($item['class']) && is_string($item['class']) && $item['class']) {
                        $classes[] = $item['class'];
                    }
                    foreach ($item['links'] as $subitem) {
                        if (isset($subitem['sel']) && $subitem['sel']) {
                            $classes[] = 'open';
                        }
                    }
                    $classes = join(' ', $classes);
                @endphp
                <li class="item {!! $classes !!}">
                    {{-- Main item --}}
                    @include('skeleton.layouts.html.navigation.item', ['nav' => $item, 'mode' => 'item'])

                    {{-- Submenu? --}}
                    @if (sizeof($item['links']))
                        <ul class="sub nav">
                            {{-- Include an alternative for the header item that links to the correct page --}}
                            @if (isset($item['url']) && is_string($item['url']) && $item['url'])
                                <li class="sub {!! $classes !!} is-alt">
                                    @include('skeleton.layouts.html.navigation.item', ['nav' => $item, 'mode' => 'sub', 'is_alt' => true])
                                </li>
                            @endif
                            {{-- List all the subnav items --}}
                            @foreach($item['links'] as $code => $subitem)
                                @php
                                    $classes = [];
                                    if (isset($subitem['sel']) && $subitem['sel']) {
                                        $classes[] = 'sel';
                                    }
                                    if (isset($subitem['class']) && is_string($subitem['class']) && $subitem['class']) {
                                        $classes[] = $subitem['class'];
                                    }
                                    $classes = join(' ', $classes);
                                @endphp
                                <li class="sub {!! $classes !!}">
                                    @include('skeleton.layouts.html.navigation.item', ['nav' => $subitem, 'mode' => 'sub'])
                                </li>
                            @endforeach
                        </ul>
                    @endif

                    {{-- Specific template? --}}
                    @includeIf("skeleton.layouts.html.links.navigation.$code")
                </li>
            @endforeach
        </ul>
    </nav>
@endif
