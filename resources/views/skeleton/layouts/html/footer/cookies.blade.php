<cookies {!! HTML::firstCookieWarning() ? 'class="first_load"' : '' !!}>
    <input type="checkbox" id="cookie_analytics" class="checkbox" {!! !\DeBear\Helpers\CookiePreferences::get('allow-analytics') ? 'checked="checked"' : '' !!} data-mode="inverse">
    <label for="cookie_analytics">Disable Analytics Tracking</label>
    <span>We use third-party cookies to help us deliver a better service.</span>
    <a href="/cookies" data-modal="cookie">Learn more about our cookies.</a>
    <div class="hidden box"></div>
</cookies>
