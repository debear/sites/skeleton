@php
    // Arg prepping
    if (!isset($is_alt)) {
        $is_alt = false;
    }
    $is_sel = (isset($nav['sel']) && $nav['sel']);

    // If this is a top-level item and it has a sub-menu, we process it slightly differently
    $has_subnav = ($mode == 'item' && sizeof($nav['links']));
    if ($has_subnav) {
        // We include an alternate, intended for mobile version, with no link
        $nav_alt = $nav;
        if (isset($nav_alt['alt']) && is_array($nav_alt['alt'])) {
            $nav_alt = Arrays::merge($nav_alt, $nav_alt['alt']);
        }
        $nav_alt['url'] = false;
    }

    // Parse JavaScript links differently
    $ele_id = false;
    if (isset($nav['url']) && is_string($nav['url']) && $nav['url']) {
        if (isset($nav['domain']) && $nav['domain'] != FrameworkConfig::get('debear.url.sub')) {
            $nav['url'] = HTTP::buildDomain($nav['domain']) . $nav['url'];
            $nav['new_page'] = true; // Auto-set the new page flag, implied by the new domain
        }
    } else {
        $ele_id = "nav_{$code}_link";
    }

    // Some extra classes
    $classes = [$mode];
    if ($mode == 'item') {
        // Alternate version of main item?
        if (!$is_alt) {
            $classes[] = ($has_subnav ? 'has' : 'no') . '-alt';
        // Is the alternate version, in either the header or footer?
        } else {
            $classes[] = 'is-alt';
        }
    }
    if (isset($nav['class']) && is_string($nav['class']) && $nav['class']) {
        $classes[] = $nav['class'];
    }
    $classes = join(' ', $classes);

    // What icons do we need?
    $icons = [];
    // A selected icon?
    if (isset($nav['icon']) && is_string($nav['icon']) && $nav['icon']) {
        $icons[] = "icon_{$nav['icon']}";
    }
    // Sub-nav in mobile view?
    if ($has_subnav && $is_alt) {
        $icons[] = 'subnav icon_right_nav_' . (!$is_sel ? 'expand' : 'contract');
    }
    // Prepare for display
    $icons = join(' ', $icons);

    /* Combine */
    $components = ["class=\"$classes\""];
    if ($nav['url']) {
        $components[] = "href=\"{$nav['url']}\"";
    }
    if ($ele_id) {
        $components[] = "id=\"$ele_id\"";
    }
    if (isset($nav['new_page']) && $nav['new_page']) {
        $components[] = 'target="_blank"';
        // If we're linking off-site, then hide the referer and DOM link to us
        if (!preg_match('/^(https?:)?\/\/(\w+\.)?' . FrameworkConfig::get('debear.url.base') . '\//', $nav['url'])) {
            $components[] = 'rel="noopener noreferrer"';
        }
    }
@endphp
<a {!! join(' ', $components) !!}>
    @if ($icons)
        <span class="{!! $icons !!}">
    @endif
    {!! $nav['label'] ?? $nav['title'] !!}
    @if ($icons)
        </span>
    @endif
</a>

@if ($has_subnav && !$is_alt)
    @include('skeleton.layouts.html.navigation.item', ['nav' => $nav_alt, 'mode' => 'item', 'is_alt' => true])
@endif
