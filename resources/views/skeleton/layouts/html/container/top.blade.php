<container class="footer_{!! HTML::getFooterSize() !!}">

    @include('skeleton.layouts.html.logo')

    @if ($pre_nav = FrameworkConfig::get('debear.content.header.prenav'))
        @includeIf($pre_nav)
    @endif

    @include('skeleton.layouts.html.navigation')

    <content>
        @if ($notices = FrameworkConfig::get('debear.content.header.notices'))
            <div class="notices">{!! $notices !!}</div>
        @endif
