@php
    HTML::addBodyClass('is_popup')
@endphp
<!DOCTYPE html>
<html>
@include('skeleton.layouts.html.meta')

<body {!! HTML::getBodyClass() !!}>

    @yield('content')

    @include('skeleton.layouts.html.offline')

    @include('skeleton.layouts.html.analytics')

</body>
</html>
