<!DOCTYPE html>
<html>
@include('skeleton.layouts.html.meta')

<body {!! HTML::getBodyClass() !!}>

    <page>

        @include('skeleton.layouts.html.header')

        @include('skeleton.layouts.html.container.top')

        @yield('content')

        @include('skeleton.layouts.html.container.bottom')

        @include('skeleton.layouts.html.footer')

    </page>

    @include('skeleton.layouts.html.offline')

    @include('skeleton.layouts.html.analytics')

</body>
</html>
