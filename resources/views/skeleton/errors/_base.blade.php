@php
    // Ensure the config for the relevant site we are viewing has been loaded
    Config::loadSite();
    // Details of the error
    $errors = FrameworkConfig::get("debear.errors");
    if (!isset($code) || !isset($errors[$code]) || !is_array($errors[$code]))
        $code = 404;
    $error = $errors[$code];
    // Some standard text to be used around numeric codes
    if (is_numeric($code)) {
        $error['info'] = "When attempting to get the page you requested, the server returned the error code <strong>$code: {$error['title']}</strong>. This usually means &quot;<em>{$error['info']}</em>&quot;</p><p>If you are not expecting to see this page, please try again in a few minutes. Should the error continue to be displayed, please email {email|support|our Support Team}.";
        $error['title'] = 'Sorry, but the server ran into a problem...';
    }

    // Perform some processing on the info text (e.g., links, emails, etc)
    $error['info'] = Interpolate::string($error['info']);

    // Some page setup
    Resources::addCSS('skel/pages/error.css');
    Resources::addJS('skel/pages/error.js');
@endphp

@extends('skeleton.layouts.html')

@section('content')

<h1>{!! $error['title'] !!}</h1>
<p>{!! $error['info'] !!}</p>
<em>The {!! FrameworkConfig::get('debear.names.company') !!} Team</em>

@endsection
