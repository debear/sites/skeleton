/**
 * Methods for date management
 */
class DateManip { // eslint-disable-line no-unused-vars
    // Check whether a passed date is valid
    static validate(inDate) {
        var cmp = inDate.split('-');
        var obj = new Date(inDate);
        return    (parseInt(cmp[0]) == obj.getFullYear())    // Year matches
               && ((parseInt(cmp[1]) - 1) == obj.getMonth()) // Month matches (where what we get back is zero-indexed)
               && (parseInt(cmp[2]) == obj.getDate());       // Date matches
    }

    // Format a date into a display string
    static format(inDate) {
        var obj = new Date(inDate);
        return Numbers.ordinal(obj.getDate()) + ' ' + obj.toLocaleDateString('en-GB', { year: 'numeric', month: 'long' });
    }
}
