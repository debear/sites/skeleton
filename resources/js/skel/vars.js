// Store of DeBear-specific global variables
window._debear = {};
get = (varName) => { return window._debear[varName]; }
set = (varName, varVal) => { window._debear[varName] = varVal; }
