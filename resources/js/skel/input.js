/**
 * JavaScript input test methods
 */
class Input { // eslint-disable-line no-unused-vars
    // Returns true if input is a defined value, false otherwise
    static isDefined(input) {
        return input != null && (typeof input != 'undefined');
    }

    // Returns true if the supplied argument is a boolean
    static isBoolean(input) {
        return typeof input == 'boolean';
    }

    // Returns true if input is a number, false otherwise
    static isNumber(input) {
        return !isNaN(parseFloat(input)) && isFinite(input);
    }

    // Returns true if the supplied argument is a string
    static isString(input) {
        return typeof input == 'string';
    }

    // Returns true if the supplied argument is an object
    static isObject(input) {
        return typeof input == 'object';
    }

    // Returns true if the supplied argument is a function
    static isFunction(input) {
        return typeof input == 'function';
    }

    // Returns true if the supplied argument is an array
    static isArray(input) {
        return Array.isArray(input);
    }
    // Returns true if the needle is included in the haystack array
    static inArray(needle, haystack) {
        return Input.isArray(haystack) && (haystack.indexOf(needle) != -1);
    }

    // State if a value resolves to a 'true' or 'false' value
    //  (which also considers the string 'false' to be false - in case it is added as a DOM attribute, for instance)
    static isTrue(input) {
        return (input ?? false) && (input !== 'false');
    }

    // Check to see if an input string is a valid e-mail address
    static isEmail(input) {
        return /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/.test(input);
    }

    // Convert a key/value object into a serialised URL encoded string
    static serialise(input) {
        // Ensure we have an object
        if (!Input.isObject(input)) {
            return '';
        }
        var ret = [];
        for (var key in input) {
            ret.push(encodeURIComponent(key) + '=' + encodeURIComponent(input[key]));
        }
        return ret.join('&').replace(/%20/, '+');
    }
}
