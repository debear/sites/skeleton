// Pluralise text, depending on the preceding number
Number.prototype.plural = function (str, plural) {
    return this + (str ?? '') + (this == 1 ? '' : (plural ?? 's'));
}

/**
 * JavaScript number methods
 */
class Numbers { // eslint-disable-line no-unused-vars
    // Convert a number to a fixed number of decimal places (default 2)
    static toXdp(num, dp) {
        return parseFloat(Number(num).toFixed(dp ?? 2));
    }

    // Function to convert a base-10 number into base-16
    static decToHex(num) {
        return num.toString(16);
    }

    // Given a number, get its ending, e.g.: 1 = 1st, 2 = 2nd, 23 = 23rd, 1250 = 1250th
    static ordinal(num) {
        num = num.toString();
        var endDigit = parseInt(num.replace(/[^\d\.]/, '').substr(num.length - 1), 10);
        var endDigits = parseInt(num.replace(/[^\d\.]/, '').substr(num.length - 2), 10);
        if (endDigit == 1 && endDigits != 11) {
            return `${num}st`;
        } else if (endDigit == 2 && endDigits != 12) {
            return `${num}nd`;
        } else if (endDigit == 3 && endDigits != 13) {
            return `${num}rd`;
        }
        return `${num}th`;
    }

    // Convert a raw byte total into more human-readable format
    static toBytes(size) {
        // Negative == Exception!
        if (size < 0) {
            return 'Cannot format negative bytes value';
        } else if (size < 1024) { // Less than 1 kilibyte.
            // Bytes.
            return `${size} B`;
        } else if (size < 1048576) { // Less than 1 mebibyte.
            // Kilibytes.
            return (size / 1024).toFixed(3) + ' KiB';
        } else if (size < 1073741824) { // Less than 1 gibibyte.
            // Mebibytes.
            return (size / 1048576).toFixed(3) + ' MiB';
        } else if (size < 1099511627776) { // Less than 1 tebibyte.
            // Gibibytes.
            return (size / 1073741824).toFixed(3) + ' GiB';
        }
        // Fallback: Tebibytes.
        return (size / 1099511627776).toFixed(3) + ' TiB';
    }

    // Convert a raw age into more human-readable format
    static toAge(seconds) {
        // Negative == Exception!
        if (seconds < 0) {
            return 'Cannot format negative bytes value';
        } else if (seconds < 60) { // Less than 1 minute.
            // Seconds.
            return Number(seconds).plural(' sec');
        } else if (seconds < 3600) { // Less than 1 hour.
            // Minutes.
            return Number(Math.floor(seconds / 60)).plural(' min');
        } else if (seconds < 86400) { // Less than 1 day.
            // Hours.
            return Number(Math.floor(seconds / 3600)).plural(' hour');
        } else if (seconds < 31536000) { // Less than 1 year.
            // Days.
            return Number(Math.floor(seconds / 86400)).plural(' day');
        }
        // Fallback: Years.
        return Number(Math.floor(seconds / 31536000)).plural(' year');
    }
}
