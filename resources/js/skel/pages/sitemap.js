// Install the click handlers for toggling entries
get('dom:load').push(() => {
    Events.attach(DOM.child('#sitemap'), 'click', (e) => {
        var ele = e.target;
        // Only want toggles
        if (!ele.classList.contains('toggle')) {
            return;
        }
        // Togglging which way?
        var children = ele.parentNode.querySelector('.children');
        var closing = ele.classList.contains('icon_toggle_minus');
        ele.classList.remove(closing ? 'icon_toggle_minus' : 'icon_toggle_plus');
        ele.classList.add(closing ? 'icon_toggle_plus' : 'icon_toggle_minus');
        children.classList.remove(closing ? 'open' : 'closed');
        children.classList.add(closing ? 'closed' : 'open');
    });
});
