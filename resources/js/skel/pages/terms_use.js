get('dom:load').push(() => {
    if (Input.isDefined((new URLSearchParams(window.location.search))?.get('interactive'))) {
        Events.attach(DOM.child('#terms_yes'), 'click', () => { updateTerms(true); });
        Events.attach(DOM.child('#terms_no'), 'click', () => { updateTerms(false); });
        Events.attach(DOM.child('#terms_cancel'), 'click', () => { window.close(); });
    }
});

// Update the checkbox on the registration page
updateTerms = (value) => {
    if (opener && value != opener.document.getElementById('terms').checked) {
        opener.document.getElementById('terms').checked = value;
    }
    window.close();
}
