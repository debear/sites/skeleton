get('dom:load').push(() => {
    Events.attach(DOM.child('content'), 'click', (e) => {
        var ele = e.target;
        if (ele.classList.contains('link-signin')) {
            Login.toggle(ele);
        } else if (ele.classList.contains('link-back')) {
            window.history.go(-1);
        }
    });
});
