/**
 * Error catching and reporting
 */
function BugReporting() {
    var ajax; // Object for sending error(s)
    var list = [ ]; // Error(s) to be sent
    var timeout; // Debounce handler

    /* Capture */
    // Catch an error via window.onerror
    this.catch = function (err) {
        var msg = err?.error?.message ?? err.message;
        var stack = err?.error?.stack ?? 'unknown:??';
        this.log(msg, stack);
    }
    // Catch an error via exception
    this.exception = function (err) {
        this.log(err.message, err.stack);
    }
    this.wrap = function (inFunc) {
        try {
            inFunc();
        } catch(err) {
            this.exception(err);
        }
    }
    // Raise a manual exception
    this.raise = function (err) {
        if (typeof err == 'string') {
            err = new Error(err);
        }
        this.log(err.message, err.stack);
    }

    /* Process */
    this.log = function (err, trace) {
        // Add to our list
        list.push({ 'err': err, 'trace': trace.split(/\n\s+/) });

        // Then send on a debounce
        if (Input.isDefined(timeout)) {
            window.clearTimeout(timeout);
        }
        timeout = window.setTimeout(() => {
            ajax.request({
                'method': 'POST',
                'url': '/report-uri/js',
                'content_type': 'application/json',
                'args': JSON.stringify(list),
                'success': false, // Silent handling
                'failure': false  // Silent handling
            });
            list = [ ];
        }, 500); // Wait half a second, just in case
    }

    /* Constructor */
    ajax = new Ajax();
}

// Instantiate and attach
set('BugReporting', new BugReporting());
if (window.addEventListener) {
    window.addEventListener('error', get('BugReporting').catch.bind(get('BugReporting')), false);
} else if (window.attachEvent) {
    window.attachEvent('onerror', get('BugReporting').catch.bind(get('BugReporting')));
}
