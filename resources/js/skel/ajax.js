/**
 * JavaScript to handle AJAX requests, returning:
 *  - XML
 *  - JSON
 *  - Text
 */
// Create the object
function Ajax() { // eslint-disable-line no-unused-vars
    // Create instance variables
    var xmlHTTP; // Object to get the XML/JSON/Text
    var config; // Definition of how to run

    // Define functions
    this.request = function (inConfig) {
        config = inConfig;

        // Validation
        if (!config.url) {
            this.defaultFeedback('The request is missing a URL?');
            return;
        }

        // Cancel a previous request?
        this.cancelRequest();

        // Which object we use depends on the browser
        if (window.XMLHttpRequest) { // Non-IE browsers
            xmlHTTP = new XMLHttpRequest();
        } else if (window.ActiveXObject) { // IE browsers
            xmlHTTP = new ActiveXObject('Microsoft.XMLHTTP');
        }

        // Check we have an object
        if (!Input.isDefined(xmlHTTP)) {
            // Tell the user their browser is too old.
            //  WE SHOULD NEVER NEED THIS LINE!!
            this.defaultFeedback('Your browser does not support XMLHTTP.');
            return;
        }

        // Define how to handle the response
        if (!Input.isDefined(config.response)) {
            xmlHTTP.handler = this;
            xmlHTTP.onreadystatechange = this.defaultResponse;
        } else {
            xmlHTTP.onreadystatechange = () => config.response(xmlHTTP);
        }
        // Bind some helpers
        this.bindResponseHelpers(xmlHTTP);

        // Make the request accordingly
        config.method = (config?.method?.toUpperCase() ?? 'GET');
        xmlHTTP.open(config.method, config.url, true);
        // Content Type
        if (Input.isDefined(config.content_type)) {
            xmlHTTP.setRequestHeader('Content-type', config.content_type);
        } else if (['POST','PATCH','PUT'].indexOf(config.method) != -1) {
            xmlHTTP.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        }
        // Misc headers
        Object.keys(config.headers ?? {}).forEach((header) => xmlHTTP.setRequestHeader(header, config.headers[header]));
        xmlHTTP.setRequestHeader('X-CSRF-TOKEN', DOM.getAttribute(DOM.child('meta[name="csrf-token"]'), 'content'));
        var target = config.url.match(/^(?:[^:]+:)?\/\/([^\/]+)\//);
        if (Input.isDefined(target)) {
            var currDomain = location.host.replace(/^([^\.]+\.)?([^\.]+\.[^\.]+)$/, '$2');
            xmlHTTP.withCredentials = (target[1].slice(0 - currDomain.length) == currDomain);
        }
        // Now run
        xmlHTTP.send(Input.isObject(config.args) ? Input.serialise(config.args) : config?.args);
    }

    // What to do once the request has been completed
    this.defaultResponse = function () {
        if (this.readyState != 4) { // 4 means the request has been completed
            return;
        }

        config.responseHeaders = this.parseResponseHeaders(); // Preserve the response headers
        if (this.status.toString().substr(0, 1) == '2') { // HTTP 200 codes mean "OK"
            // Gather the appropriate return object
            var retValue = this.parseResponseText();
            // Then process
            if (Input.isObject(config.success)) {
                config.success[0][config.success[1]](retValue, config);
            } else if (Input.isFunction(config.success)) {
                config.success(retValue, config);
            } else if (!Input.isTrue(config.success)) {
                // Skip processing
            }

            // Errors (skipping aborts, which do not have a status code)
        } else if (parseInt(this.status)) {
            // Report
            if (this.status.toString().substring(0, 1) != '4') {
                get('BugReporting').raise(`${this.status} error during AJAX request to '${this.responseURL}' returned: '${this.statusText}'`);
            }
            // Gather the appropriate return object
            this.responseParsed = this.parseResponseText();
            // Handle
            if (Input.isObject(config.failure)) {
                config.failure[0][config.failure[1]](this, config);
            } else if (Input.isFunction(config.failure)) {
                config.failure(this, config);
            } else if (!Input.isTrue(config.failure)) {
                // Silently fail
            } else {
                // Generically handle an error
                this.handler.defaultFeedback(`There was a problem processing the request. If it happens again, please contact us and quote reference 'AJAX${this.status}'.`);
            }
        }

        // Unset the variable
        delete this;
    }

    // Cancel a request
    this.cancelRequest = function () {
        xmlHTTP?.abort();
    }

    // Provide feedback to the user (usually after an unrecoverable error)
    this.defaultFeedback = function (msg) {
        console.error(`We are unable to process an inline request: ${msg}`);
    }

    // Bind some helpers
    this.bindResponseHelpers = function (xmlHTTP) {
        // Convert the string of response headers returned by XMLHttpRequest into a usable array
        xmlHTTP.parseResponseHeaders = function () {
            headers = {};
            this.getAllResponseHeaders().trim().split(/[\r\n]+/).forEach((rawText) => {
                rawArr = rawText.split(": ");
                header = rawArr.shift();
                headers[header] = rawArr.join(": ");
            });
            return headers;
        };

        // Interpret the response body in an appropriate format
        xmlHTTP.parseResponseText = function () {
            if (this.getResponseHeader('Content-Type') == 'text/xml') {
                // XML
                return this.responseXML;

            } else if (this.getResponseHeader('Content-Type') == 'application/json') {
                // JSON
                return JSON.parse(this.responseText);
            }
            // Generic text
            return this.responseText;
        }

        // Convert the response code into a generic error message
        xmlHTTP.statusErrorMessage = function () {
            if (this.status.toString().match(/^40[13]$/)) {
                // Authorisation / Authentication error
                return 'Please log in and try again';
            } else if (this.status == 400) {
                // Badly formed request
                return 'Request could not be understood';
            } else if (this.status == 409) {
                // Conflicted request
                return 'The requested action does not match what we expected &ndash; please reload the page and try again';
            } else if (this.status == 419) {
                // Token expired
                return 'An internal token has expired &ndash; please reload the page and try again';
            } else if (this.status.toString().match(/^42[23]$/)) {
                // Action unavailable
                return 'This action is not available';
            }
            // We won't provide a generalised message
            return undefined;
        };
    }
}
