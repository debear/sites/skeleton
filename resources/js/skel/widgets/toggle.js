/* Setup the toggle */
get('dom:load').push(() => {
    Events.attach(document, 'click', (e) => {
        // Relevant to a toggle on the page?
        var ele = e.target;
        if (!ele.classList.contains('toggle-link')) {
            return;
        }
        // Toggle the classes
        ele.closest('.toggle-parent').querySelectorAll('.toggle-target').forEach((eleTarget) => {
            eleTarget.classList.toggle(DOM.getAttribute(eleTarget, 'toggle'));
        });
        // Update the label
        var newAltLabel = ele.innerHTML;
        ele.innerHTML = DOM.getAttribute(ele, 'alt');
        DOM.setAttribute(ele, 'alt', newAltLabel);
    });
});
