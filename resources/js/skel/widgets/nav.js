get('dom:load').push(() => {
    // Click handler for toggling menus
    Events.attach(DOM.child('nav'), 'click', (e) => {
        var toggle;
        if (e.target.tagName.toLowerCase() == 'menu') {
            // Menu toggle?
            toggle = e.target.closest('nav');
        } else if (e.target?.closest('a.item')?.classList.contains('is-alt')) {
            // Item toggle?
            toggle = e.target.closest('li.item');
        }

        if (!Input.isDefined(toggle)) {
            return;
        }
        // Update a submenu toggle icon?
        if (toggle.classList.contains('item')) {
            var span = toggle.querySelector('.subnav');
            var closing = toggle.classList.contains('open');
            span.classList.remove(closing ? 'icon_right_nav_contract' : 'icon_right_nav_expand');
            span.classList.add(closing ? 'icon_right_nav_expand' : 'icon_right_nav_contract');
        }
        // Then update the main list
        toggle.classList.toggle('open');
    });
});
