/**
 * Methods relating to the scroller widget
 */
function Scroller()
{
    // Instance vars
    var widget;
    var widgetBlock;
    var widgetBullets;
    var range;
    var respWidth;
    var interval;
    var intervalID;

    // Prepare the object on first load
    this.setup = function () {
        // DOM elements
        widget = DOM.child('scroller');
        widgetBlock = widget.querySelector('block');
        widgetBullets = widget.querySelector('bullets');
        // Details about where we are in the list
        range = {
            'min': parseInt(DOM.getAttribute(widget, 'min')),
            'max': parseInt(DOM.getAttribute(widget, 'max')),
            'curr': parseInt(DOM.getAttribute(widget, 'curr'))
        };
        // Responded widths
        var rawWidths = JSON.parse(DOM.getAttribute(widget, 'resp'));
        var breakpoints = Object.keys(rawWidths);
        respWidth = {
            'val': null,
            'opt': []
        };
        for (var i = 0; i < breakpoints.length; i++) {
            respWidth.opt.push({
                'minWidth': parseInt(breakpoints[i]),
                'maxWidth': 99999,
                'numItems': parseInt(rawWidths[breakpoints[i]])
            });
            if (i) {
                respWidth.opt[i - 1].maxWidth = breakpoints[i] - 1;
            }
        }
        // Determine the current viewport, setting up a resize event listener should the viewport change
        this.setupViewport();
        Events.attach(window, 'resize', this.setupViewport);
        // Duplicate the first item (to create the wrap-around)
        widgetBlock.appendChild(widgetBlock.firstElementChild.cloneNode(true));

        // Define the click handler
        Events.attach(widget, 'click', this.clickHandler.bind(this));
        // And start the auto-timer
        interval = parseInt(DOM.getAttribute(widget, 'interval'));
        this.setupInterval();
    }

    // Determine the number of items being displayed in this viewport
    this.setupViewport = function () {
        respWidth.val = null; // Explicitly reset
        respWidth.opt.forEach((o) => {
            if (!Input.isDefined(respWidth.val) && (o.minWidth <= window.innerWidth) && (window.innerWidth <= o.maxWidth)) {
                respWidth.val = o.numItems;
            }
        });
        DOM.setAttribute(widget, 'view', respWidth.val);
    }

    // Prepare the interval used to automatically scroll the content
    this.setupInterval = function () {
        intervalID = setInterval(this.scrollForward.bind(this), interval);
        this.triggerProgress();
    }

    // Stop the automatic scrolling
    this.cancelInterval = function () {
        clearInterval(intervalID);
    }

    // Automatically scroll to the next item
    this.scrollForward = function () {
        this.triggerProgress();
        this.moveForward();
    }

    // Trigger the progress bar transition
    this.triggerProgress = function () {
        DOM.triggerTransition(widgetBullets, 'progress');
    }

    // Move backward a single step
    this.moveBackward = function () { this.move(-1); }

    // Move forward a single step
    this.moveForward = function () { this.move(1); }

    // Move worker
    this.move = function (change) {
        var newPos = range.curr + change;
        if (newPos < 1) {
            // Moving to the end
            newPos = range.max;
        } else if (newPos > range.max) {
            // Moving to the start
            newPos = 1;
        } else {
            // Moving incrementally
        }

        // Update internal counters / refs
        range.curr = newPos;
        DOM.setAttribute(widget, 'curr', range.curr);
    }

    // Handle clicks within the widget, by-passing the timed changes
    this.clickHandler = function (e) {
        // Is the click of an appropriate widget?
        if (!Input.isDefined(e.target)
            || !Input.isDefined(e.target.closest('scroller'))
            || !Input.isDefined(e.target.closest('.onclick_target'))
        ) {
            // Not a scroller click
            return;
        }

        // Then reset the automatic scroller (so we don't step on each other's toes)
        this.cancelInterval();

        // What was clicked?
        e.target = e.target.closest('.onclick_target');
        switch (e.target.tagName.toLowerCase()) {
        case 'prev':
            // Go back an item
            this.moveBackward();
            break;
        case 'next':
            // Go forward an item
            this.moveForward();
            break;
        case 'bullet':
            // Go to the specific item
            var ref = DOM.getAttribute(e.target, 'ref');
            if (ref != range.curr) {
                this.move(ref - range.curr);
            }
            break;
        }

        // Restart the auto scrolling
        this.setupInterval();
    }

    // Faux-constructor
    this.setup();
}

/* Prepare the Scroller */
get('dom:load').push(() => { set('scroller', new Scroller()); });
