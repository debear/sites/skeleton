/**
 * Form handling methods
 */
class Forms {
    // What to do when a form loads
    static loaded() {
        set('forms:validation', { });
        Forms.setupCheckboxes();
        Forms.setupValidation();
        Forms.setupSubmission();
        Forms.setInitialFocus();
    }

    // Handle selecting (via keyboard) our faux checkboxes
    static setupCheckboxes() {
        DOM.children('input[type="checkbox"] + label').forEach((ele) => {
            Events.attach(ele, 'keydown', (e) => {
                // Only looking for space/enter keys
                if ([13,32].indexOf(DOM.getKeyPress(e)) == -1) {
                    return;
                }
                // Toggle the status
                e.target.previousElementSibling.checked = !e.target.previousElementSibling.checked;
                // Trigger the onChange event (which doesn't fire after the above update)
                var evtChange = new Event('change');
                e.target.previousElementSibling.dispatchEvent(evtChange);
            });
            var eleFor = DOM.getAttribute(ele, 'for');
            if (Input.isDefined(eleFor)) {
                var eleCheckbox = DOM.child(`#${eleFor}__cb`);
                if (Input.isDefined(eleCheckbox)) {
                    Events.attach(DOM.child(`#${eleFor}`), 'change', (e) => { DOM.child(`#${e.target.name}__cb`).value = e.target.checked ? 1 : 0 });
                }
            }
        });
    }

    // Give focus to the first form element
    static setInitialFocus() {
        var firstEle = DOM.child('[data-tabindex="1"]');
        if (Input.isDefined(firstEle)) {
            if (firstEle.classList.contains('dropdown')) {
                get('dropdowns')[firstEle.dataset.id].focus();
            } else {
                firstEle.focus();
            }
        }
    }

    /**
     * Validation
     */
    // Setup the handlers
    static setupValidation() {
        // Elements could come from a pair of sources
        ['[data-tabindex]','[data-form-validate="on"]'].forEach((sel) => {
            DOM.children(sel).forEach((ele) => {
                var eleId = DOM.getAttribute(ele, 'id');
                // Skip elements with no ID (as we assume they aren't valid elements)
                if (!eleId) {
                    return;
                }
                // Create the validation status object
                if (!Input.isDefined(get('forms:validation')[ele.closest('form').id])) {
                    get('forms:validation')[ele.closest('form').id] = { };
                }
                get('forms:validation')[ele.closest('form').id][eleId] = Forms.validationDefault();

                // What is the element, as this affects what we validate
                if (ele.classList.contains('dropdown')) {
                    // A dropdown, so listen to the custom event
                    Events.attach(document, 'debear:dropdown-set', function (e) {
                        if (e.detail.id == this.id) {
                            Forms.validate(this);
                        }
                    }.bind(DOM.child(`#${ele.dataset.id}`)));
                } else if (ele.hasAttribute('for')) {
                    // A checkbox
                    Events.attach(DOM.child(`#${ele.getAttribute('for')}`), 'change', function () {
                        Forms.validate(this);
                    }.bind(DOM.child(`#${ele.getAttribute('for')}`)));
                } else {
                    // Fallback: "standard" input widget
                    Events.attach(ele, 'keyup', function () {
                        Forms.validate(this);
                    }.bind(ele));
                }

                // If part of a date element, we also want to validate the date as a whole, so listen to the custom event
                //  (but we'll only decide to do so when we reach the final, year, component)
                if (eleId.substr(-3) == '_yy') {
                    eleId = eleId.substr(0, eleId.length - 3);
                    get('forms:validation')[ele.closest('form').id][eleId] = Forms.validationDefault();
                    Events.attach(document, 'debear:date_field-set', function (e) {
                        if (e.detail.id == this.id) {
                            Forms.validate(this);
                        }
                    }.bind(DOM.child(`#${eleId}`)));
                }
            });
        });
    }

    static validationDefault() {
        return {
            'valid': true, // Assume everything is okay until proven otherwise
            'entered': false, // Assume nothing was entered until told otherwise
            'skip': false,
            'message': '',
            'errors': [ ]
        };
    }

    // Run on a given element, on a debounce...
    static validate(ele) {
        // Check the debounce
        var debounces = get('forms:debounces');
        var debounceKey = `validae:${ele.id}`;
        if (Input.isDefined(debounces[debounceKey])) {
            window.clearTimeout(debounces[debounceKey]);
        }

        // Run
        debounces[debounceKey] = window.setTimeout(function () {
            Forms.validateElement(this);
        }.bind(ele), 500);
    }

    // Validation worker method
    static validateElement(ele) {
        var id = ele.id;
        var value = ele.value.toString().trim();
        var newValidation = Forms.validationDefault();

        // "What am I?" checkers...
        var isDate = DOM.getAttribute(ele, 'is-date');
        // var isDropdown = DOM.getAttribute(ele, 'is-dropdown');
        var isCheckbox = (DOM.getAttribute(ele, 'type') == 'checkbox');
        // var isTextbox = !isDate && !isDropdown && !isCheckbox;
        var isEntered = (value !== '');
        newValidation.entered = isEntered;

        // Pre-req: Only validate a date if all components have been entered. If one is missing, assume all missing.
        if (isDate && (value.substring(0, 4).replace(/^0+/, '').match(/^\d{1,3}$/) || (value.substring(0, 4) == '0000') || (value.substring(5, 7) == '00') || (value.substring(8, 10) == '00'))) {
            value = '0000-00-00';
        }

        // Look for standard attributes to check
        // - required
        if (DOM.getAttribute(ele, 'required', { 'has-no-value': true })) {
            // Checkbox: checked, Others: defined and non-empty
            newValidation.valid = (DOM.getAttribute(ele, 'type') == 'checkbox') ? ele.checked : isEntered;
            if (!newValidation.valid) {
                newValidation.errors.push('required');
                if (!newValidation.message) {
                    newValidation.message = (isCheckbox ? 'This option must be selected' : 'Please complete this required field.');
                }
            }
        }
        // - [type=num] (Is numeric)
        if (isEntered && (DOM.getAttribute(ele, 'type') == 'num') && !Input.isNumber(value)) {
            if (newValidation.valid) {
                newValidation.errors.push('num');
                if (!newValidation.message) {
                    newValidation.message = 'Please enter a number in this field';
                }
            }
            newValidation.valid = false;
        }
        // - data-email (Is email address)
        if (isEntered && DOM.getAttribute(ele, 'email') && !Input.isEmail(value)) {
            if (newValidation.valid) {
                newValidation.errors.push('email');
                if (!newValidation.message) {
                    newValidation.message = 'Please enter a valid email address in this field';
                }
            }
            newValidation.valid = false;
        }
        // (Length error getter)
        var eleMinLen = DOM.getAttribute(ele, 'minlength', { 'default': 0 });
        var eleMaxLen = DOM.getAttribute(ele, 'maxlength', { 'default': 0 });
        var eleStrLen = 'Please enter ';
        if (eleMinLen && eleMaxLen) {
            eleStrLen += (eleMinLen == eleMaxLen ? eleMinLen.toString() : `between ${eleMinLen} and ${eleMaxLen}`);
        } else if (eleMinLen) {
            eleStrLen += `at least ${eleMinLen}`;
        } else if (eleMaxLen) {
            eleStrLen += `no more than ${eleMaxLen}`;
        }
        eleStrLen += ' characters';
        // - minlength
        if (isEntered && eleMinLen && (value.length < eleMinLen)) {
            if (newValidation.valid) {
                newValidation.errors.push('minlength');
                if (!newValidation.message) {
                    newValidation.message = eleStrLen;
                }
            }
            newValidation.valid = false;
        }
        // - maxlength
        if (isEntered && eleMaxLen && (value.length > eleMaxLen)) {
            if (newValidation.valid) {
                newValidation.errors.push('maxlength');
                if (!newValidation.message) {
                    newValidation.message = eleStrLen;
                }
            }
            newValidation.valid = false;
        }
        // (Min/Max error getter)
        var eleMin = DOM.getAttribute(ele, 'min', { 'default': null });
        var eleMax = DOM.getAttribute(ele, 'max', { 'default': null });
        if (isDate) {
            eleMin = (eleMin ? DateManip.format(eleMin) : 0);
            eleMax = (eleMax ? DateManip.format(eleMax) : 0);
        }
        var eleStrMinMax = `Please enter a ${isDate ? 'date' : 'value'} `;
        if (eleMin && eleMax) {
            eleStrMinMax += (eleMin == eleMax ? `of ${eleMin}` : `between ${eleMin} and ${eleMax}`);
        } else if (eleMax) {
            eleStrMinMax += (isDate ? `of ${eleMax} or earlier` : `of no more than ${eleMax}`);
        } else if (eleMin) {
            eleStrMinMax += (isDate ? `of ${eleMin} or later` : `of at least ${eleMin}`);
        }
        // - min
        if (isEntered && Input.isDefined(eleMin) && (parseInt(value) < eleMin)) {
            if (newValidation.valid) {
                newValidation.errors.push('min');
                if (!newValidation.message) {
                    newValidation.message = eleStrMinMax;
                }
            }
            newValidation.valid = false;
        }
        // - max
        if (isEntered && Input.isDefined(eleMax) && (parseInt(value) > eleMax)) {
            if (newValidation.valid) {
                newValidation.errors.push('max');
                if (!newValidation.message) {
                    newValidation.message = eleStrMinMax;
                }
            }
            newValidation.valid = false;
        }

        // Custom validation for this element?
        var method = 'validateCustom' + id.toTitleCase();
        if (Input.isFunction(Forms[method])) {
            var ret = Forms[method](newValidation.valid);
            if (!ret.valid) {
                newValidation.valid = ret.valid;
                newValidation.errors.push(`custom:${id}`);
                if (!newValidation.message) {
                    newValidation.message = ret.message;
                }
            }
            if (Input.isTrue(ret.force)) {
                newValidation.force = true;
            }
            newValidation.skip = Input.isTrue(ret.skip);
        }

        // Store the validation result and reflect on-screen
        Forms.validationDisplay(ele, value, newValidation);
    }

    // Process the validation outcome
    static validationDisplay(ele, value, validation) {
        var eleForm = ele.closest('form');
        get('forms:validation')[eleForm.id][ele.id] = validation;
        var eleInput = DOM.getAttribute(ele, 'is-dropdown') ? ele.nextElementSibling : ele;
        var statusId = DOM.getAttribute(ele, 'status-field') ?? ele.id;
        var eleLabel = DOM.child(`#${statusId}_label`);
        var eleStatus = DOM.child(`#${statusId}_status`);
        var eleError = DOM.child(`#${statusId}_error`);

        // Do we need to consider the entered nature of another field in a group?
        var entered;
        if (statusId != ele.id) {
            var eleGroup = eleForm.querySelectorAll(`[data-status-field="${statusId}"]`);
            eleGroup.forEach((e) => {
                // Check all fields were entered
                var eleEntered = get('forms:validation')[eleForm.id][e.id].entered;
                entered = !Input.isDefined(entered) ? eleEntered : entered || eleEntered; // At least one field needs to have been entered
            });
            // No, we can just take what we were given
        } else {
            entered = validation.entered;
        }

        // Process
        var updates = {
            'input': { 'add': [], 'remove': [] },
            'status': { 'add': [], 'remove': [] },
            'error': { 'add': [], 'remove': [], 'message': '' }
        };
        // Determine the actions to perform
        if ((!entered || (DOM.getAttribute(ele, 'is-date') && value == '0000-00-00') || validation.skip) && !Input.isTrue(validation.force)) {
            updates.input.remove.push('error');
            updates.status.remove.push('icon_valid');
            updates.status.remove.push('icon_error');
            updates.error.add.push('hidden');

        } else if (validation.valid) {
            updates.input.remove.push('error');
            updates.status.add.push('icon_valid');
            updates.status.remove.push('icon_error');
            updates.error.add.push('hidden');
        } else {
            updates.input.add.push('error');
            updates.status.remove.push('icon_valid');
            updates.status.add.push('icon_error');
            updates.error.remove.push('hidden');
            updates.error.message = `${validation.message}.`;
        }
        // And now run
        Forms.validateElementClasses(eleInput, updates.input);
        if (Input.isDefined(eleLabel)) {
            Forms.validateElementClasses(eleLabel, updates.input);
        }
        if (Input.isDefined(eleStatus)) {
            Forms.validateElementClasses(eleStatus, updates.status);
        }
        if (Input.isDefined(eleError)) {
            Forms.validateElementClasses(eleError, updates.error);
            eleError.innerHTML = updates.error.message;
        }
        // Finally, generate an event should anyone be interested
        Events.fire(`form:validation:${ele.id}`, validation);
    }

    static validateElementClasses(ele, classes) {
        if (classes.add.length) {
            ele.classList.add(...classes.add);
        }
        if (classes.remove.length) {
            ele.classList.remove(...classes.remove);
        }
    }

    /**
     * External field failing
     */
    static failField(ele, code, message) {
        // ID -> DOM Element?
        ele = Input.isString(ele) ? DOM.child(`#${ele}`) : ele;
        // Get the validation results, tweak
        var validation = get('forms:validation')[ele.closest('form').id][ele.id];
        validation.valid = false;
        validation.force = true;
        validation.errors.push(code);
        if (!validation.message) {
            validation.message = message
            // And write back, both to the array and visually
            Forms.validationDisplay(ele, ele.value.toString().trim(), validation);
        }
    }

    static clearFailedField(ele) {
        // ID -> DOM Element?
        ele = Input.isString(ele) ? DOM.child(`#${ele}`) : ele;
        // Get the validation results, tweak
        var validation = get('forms:validation')[ele.closest('form').id][ele.id];
        validation.valid = true;
        delete validation.force;
        validation.message = '';
        validation.errors = [];
        // And write back, both to the array and visually
        Forms.validationDisplay(ele, ele.value.toString().trim(), validation);
    }

    /**
     * Form submission
     */
    static setupSubmission() {
        DOM.children('form').forEach((ele) => {
            if (!ele.classList.contains('validate')) {
                return;
            }
            Events.attach(ele, 'submit', (e) => {
                // Disable the button to try and prevent double clicking
                var eleSubmit = e.target.querySelector('[type="submit"]');
                Forms.disableSubmit(eleSubmit);

                // We can't rely on the current validation status objects to be set (given autocomplete won't trigger our validation)
                //  so we have to re-validate (and consider the fact there could be multiple forms on the page)
                var valid = true; // Assume true until told otherwise
                var formId = eleSubmit.closest('form').id;
                Object.keys(get('forms:validation')[formId]).forEach((eleId) => {
                    var fele = DOM.child(`#${eleId}`);
                    // Only those elements we have presented to the user
                    if (!Input.isDefined(fele.offsetParent) && (['hidden','checkbox'].indexOf(DOM.getAttribute(fele, 'type')) == -1)) {
                        return;
                    }
                    Forms.validateElement(fele);
                    // Update our counter
                    valid = (valid && get('forms:validation')[formId][eleId].valid);
                });

                // Stop the event on failure
                var eleSubmitErr = eleSubmit.parentNode.querySelector('.error');
                if (!valid) {
                    Events.cancel(e);
                    Forms.enableSubmit(eleSubmit);
                    eleSubmitErr?.classList.remove('hidden');
                    return false;
                } else {
                    eleSubmitErr?.classList.add('hidden');
                }
            });
        });
    }

    static disableSubmit(ele) {
        // Disable
        ele.disabled = true;
        // Preserve the current label for if/when we reset it
        if (!Input.isDefined(DOM.getAttribute(ele, 'label'))) {
            ele.setAttribute('data-label', ele.innerHTML);
        }
        var label = DOM.getAttribute(ele, 'submit-label');
        ele.innerHTML = (label ?? 'Please wait...');
        // Set a long timeout in case the page hasn't submitted to allow the user to try again
        window.setTimeout(function () {
            Forms.enableSubmit(this);
        }.bind(ele), 10000);
    }

    static enableSubmit(ele) {
        // Restore the label and then re-enable
        ele.innerHTML = DOM.getAttribute(ele, 'label');
        ele.disabled = false;
    }
}

set('forms:debounces', {});
get('dom:load').push(() => Forms.loaded());
