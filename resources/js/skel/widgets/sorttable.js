class SortTable { // eslint-disable-line no-unused-vars
    // Setup the various event handlers
    static setup(ele) {
        // Click handler
        Events.attach(ele, 'mouseup', SortTable.clickHandler);
        // Select handler
        DOM.children('mobile-select select').forEach((sel) => { Events.attach(sel, 'change', SortTable.selectHandler); });
    }

    // Click handler
    static clickHandler(e) {
        var ele = e.target;
        // Skip if not an element we can process
        if (!ele.classList.contains('unsel')) {
            return;
        }
        // Sorting what, by what ID and in what direction?
        var sortEle = ele.closest('scrolltable');
        var sortID = SortTable.getIndex(ele);
        var sortDir = SortTable.getDirection(ele);
        // Run
        SortTable.sort(sortEle, sortID, sortDir);
        // Update the icons
        SortTable.updateIcons(ele, sortEle);
    }

    // Select handler
    static selectHandler(e) {
        // Do a quick sort on the new field (always ascending)
        var sel = e.target;
        var tbl = sel.closest('scrolltable');
        SortTable.sort(tbl, sel.value, 'asc');
    }

    // Standard sorting method (when two values are defined)
    static defaultSort(a, b, dir) {
        // Test, assuming an ASC sort
        var ret = (parseInt(a) > parseInt(b) ? 1 : -1);
        // But reverse in a DESC sort
        return (dir == 'asc' ? ret : ret * -1);
    }

    // Standard position display method
    static defaultDisplayPos(rank) {
        return rank;
    }

    // Run the actual sort
    static sort(ele, id, dir) {
        // Decide our sorting and display method
        var sortMethod = (Input.isFunction(SortTable.customSort) ? SortTable.customSort : SortTable.defaultSort);
        var dispMethod = (Input.isFunction(SortTable.customDisplayPos) ? SortTable.customDisplayPos : SortTable.defaultDisplayPos);
        // Run
        [ele.querySelector('main'), ele.querySelector('datalist', ele)].forEach((tbl) => {
            // Order the rows
            [...tbl.children].sort((a, b) => {
                if (a.tagName.toLowerCase() == 'top') {
                    // a is the top row
                    return -1;
                } else if (b.tagName.toLowerCase() == 'top') {
                    // b is the top row
                    return 1;
                }
                // Sorting by defined order
                var aSort = DOM.getAttribute(a, `sort-${id}`);
                var bSort = DOM.getAttribute(b, `sort-${id}`);
                if (!Input.isDefined(aSort) && !Input.isDefined(bSort)) {
                    // Neither applicable, so return equal
                    return 0;
                } else if (!Input.isDefined(bSort)) {
                    // a applicable, b did not
                    return -1;
                } else if (!Input.isDefined(aSort)) {
                    // b applicable, a did not
                    return 1;
                }
                // Pass through our sorting logic when both elements are defined
                return sortMethod(aSort, bSort, dir);
            }).map(node => tbl.appendChild(node));
            // Display sort and row colouring?
            var cache = []; var i = 0;
            [...tbl.children].forEach((row) => {
                // Skip top rows
                if (row.tagName.toLowerCase() == 'top') {
                    return;
                }
                // Display order?
                var disp = row.querySelector('.sort_order');
                if (Input.isDefined(disp)) {
                    var rank = dispMethod(DOM.getAttribute(row, `sort-${id}`));
                    disp.innerHTML = (!Input.isDefined(cache[rank]) ? rank : '=');
                    cache[rank] = true;
                }
                // Row colour?
                i++;
                var rowCSS = 'row_' + (i % 2);
                var oldRowCSS = 'row_' + ((i + 1) % 2);
                if (!Input.isDefined(row.querySelector(`.${oldRowCSS}`))) {
                    // Already correct / not to be updated
                    return;
                }
                row.querySelectorAll(`.${oldRowCSS}`).forEach((cell) => {
                    cell.classList.remove(oldRowCSS);
                    cell.classList.add(rowCSS);
                });
            });
        });
    }

    // Visually update the column sorts
    static updateIcons(ele, sortEle) {
        var eleInv = sortEle.querySelector('.sel');
        // Update ourselves
        ele.classList.add('sel');
        ele.classList.remove('unsel');
        var isUp = ele.classList.contains('icon_sort_up');
        ele.classList.remove(isUp ? 'icon_sort_up' : 'icon_sort_down');
        ele.classList.add(isUp ? 'icon_sort_up_disabled' : 'icon_sort_down_disabled');
        // Update the previous sort
        eleInv.classList.remove('sel');
        eleInv.classList.add('unsel');
        var invIsIup = eleInv.classList.contains('icon_sort_up_disabled');
        eleInv.classList.remove(invIsIup ? 'icon_sort_up_disabled' : 'icon_sort_down_disabled');
        eleInv.classList.add(invIsIup ? 'icon_sort_up' : 'icon_sort_down');
    }
}
