/**
 * Methods for modal interaction
 */
class Modals {
    // Create a modal window
    static open(size, src, root) {
        var modal = document.createElement('modal');
        modal.classList.add(size);
        modal.innerHTML = `\
            <cover></cover>\
            <div class="close"><span class="icon_delete"></span><a>Close</a></div>\
            <iframe src="${src}"></iframe>`;
        root.appendChild(modal);
        Modals.lockBodyScroll();
    }

    // Close the modal
    static close(root) {
        root.remove();
        Modals.lockBodyScroll();
    }

    // Prevent the background window from scrolling
    static lockBodyScroll() {
        DOM.child('body').classList.toggle('modal');
    }
}

/* Click handlers */
get('dom:load').push(() => {
    // Does this click event relate to a modal?
    Events.attach(document, 'click', (e) => {
        // Closing the modal?
        var modal = e.target.closest('modal div.close');
        if (Input.isDefined(modal)) {
            Modals.close(modal.parentNode);
            return;
        }
        // Opening one?
        if (!Input.isDefined(e.target.href)) {
            return;
        }
        var size = DOM.getAttribute(e.target, 'modal');
        if (!Input.isDefined(size)) {
            return;
        }

        // Yes, so action
        Events.cancel(e);
        Modals.open(size, e.target.href, e.target.closest('page'));
    });
});
