// Dropdown processing class
function Dropdown(inWidget) {
    // Instance vars
    var dl = inWidget;
    var dt = dl.querySelector('dt');
    var dd = dl.querySelector('dd');
    var opt = dd.querySelectorAll('li[data-id]');
    var input = dl.previousElementSibling;
    var select = null;
    var searchField = dl.querySelector('input.search');
    var enabled = !Input.isTrue(DOM.getAttribute(dl, 'disabled'));

    // Triage the click
    this.processClick = function (target) {
        // Ignore if disabled
        if (this.isDisabled()) {
            return;
        } else if (Input.isDefined(target.closest('dt'))) {
            // A title row clicked, so toggle the visibility of the selections
            this.optionsToggle();
        } else if (target?.closest('li')?.dataset?.id ?? false) {
            // A value item clicked
            this.optionsHide();
            this.setValue(target.closest('li'));
        }
    }
    this.fauxClick = function () {
        // Ignore if disabled
        if (this.isDisabled()) {
            return;
        }
        var sel = this.getOptionList().querySelector('.faux_hover');
        if (Input.isDefined(sel) && !sel.classList.contains('sel')) {
            this.processClick(sel);
        }
    }

    // Value details
    this.getValue = function () {
        return this.getInput().value;
    }
    this.setValue = function (sel) {
        // Ignore if disabled
        if (this.isDisabled()) {
            return;
        }
        // If the requested update is a scalar, get the appropriate option
        if (!Input.isObject(sel)) {
            sel = this.getOptionList().querySelector(`li[data-id="${sel}"]`);
            // However, we must get an option for this to work...
            if (!Input.isDefined(sel)) {
                return;
            }
        }
        // Then perform the update
        var valueNow = sel.dataset.id;
        var valueWas = this.getInput().value;
        // In some instances we may be called when no value actually occurred, in which case no need to continue
        if (valueNow === valueWas) {
            return;
        }
        // Store
        this.getInput().value = valueNow;
        // Update header row
        this.getHead().querySelector('span').innerHTML = sel.innerHTML;
        // Update row selection colours
        sel.classList.add('sel');
        this.getOptionList().querySelector(`li[data-id="${valueWas}"]`)?.classList.remove('sel');
        // Update the corresponding select for mobile view
        if (Input.isDefined(this.getMobileSelect())) {
            this.getMobileSelect().value = valueNow;
        }
        // Fire a bespoke event to let people know we have made a change (should they want to know)
        Events.fire('dropdown-set', {'id': this.getID(), 'value': valueNow, 'was': valueWas});
        DOM.performTabindex(this.getWidget());
    }

    // Enable/Disable the entire dropdown
    this.enable = function () {
        this.setEnabled(true);
    }
    this.disable = function () {
        this.setEnabled(false);
    }
    this.setEnabled = function (status) {
        enabled = status;
        DOM.setAttribute(dl, 'disabled', !enabled ? 'true' : 'false');
        if (Input.isDefined(select)) {
            select.disabled = !enabled;
        }
    }

    // Enabled/Disabled getters
    this.isEnabled = function () {
        return enabled;
    }
    this.isDisabled = function () {
        return !this.isEnabled();
    }

    // Show/Hide/Toggle the options dropdown
    this.optionsVisible = function () {
        return !this.getOptionList().classList.contains('hidden');
    }
    this.optionsToggle = function () {
        // Ignore if disabled
        if (this.isDisabled()) {
            return;
        } else if (DOM.child('body').classList.contains('viewport-mobile')) {
            // Skip mobile view clicks, which are handled via traditional selects
            return;
        } else if (!this.optionsVisible()) {
            this.optionsShow();
        } else {
            this.optionsHide();
        }
    }
    this.optionsShow = function () {
        // Ignore if disabled
        if (this.isDisabled()) {
            return;
        }
        // Display the list
        this.getOptionList().classList.remove('hidden');
        // If we have a search option, give it focus
        if (this.isSearchable()) {
            searchField.focus();
        }
    }
    this.optionsHide = function (setFauxHover) {
        // Ignore if disabled
        if (this.isDisabled()) {
            return;
        }
        var fauxHover = this.getOptionList().querySelector('.faux_hover');
        if (Input.isDefined(fauxHover)) {
            // Remove the visual effect
            fauxHover.classList.remove('faux_hover');
            // Store the value?
            if (Input.isDefined(setFauxHover) && setFauxHover) {
                this.setValue(fauxHover);
            }
        }
        this.getOptionList().classList.add('hidden');
    }

    // If searchable, reset the view
    this.resetSearch = function () {
        if (!this.isSearchable()) {
            return;
        }
        opt.forEach((ele) => { ele.classList.remove('hidden'); });
        searchField.value = '';
    }

    // Misc Getters
    this.getID = function () {
        return this.getInput().id;
    }
    this.getInput = function () {
        return input;
    }
    this.getWidget = function () {
        return dl;
    }
    this.getHead = function () {
        return dt;
    }
    this.getOptionList = function () {
        return dd;
    }
    this.getOptions = function () {
        return opt;
    }
    this.getMobileSelect = function () {
        return select;
    }
    this.isSearchable = function () {
        return Input.isDefined(searchField);
    }

    // What happens if the widget is given focus
    this.focus = function () {
        // Ignore if disabled
        if (this.isDisabled()) {
            return;
        }
        this.getWidget().focus();
        this.optionsShow();
    }
    // Or if we lose it
    this.blur = function () {
        // Ignore if disabled
        if (this.isDisabled()) {
            return;
        }
        this.optionsHide();
    }

    /* Scrolling */
    this.fauxScrollUp = function () {
        this.fauxScroll(true);
    }
    this.fauxScrollDown = function () {
        this.fauxScroll(false);
    }
    this.fauxScroll = function (scrollUp) {
        // Ignore if disabled
        if (this.isDisabled()) {
            return;
        }
        // Get the current "selection"
        var curSel = this.getOptionList().querySelector('.faux_hover');
        var allSel = this.getOptions(); var allSelMax = allSel.length - 1;
        var index = -1;
        var testClass = !Input.isDefined(curSel) ? 'sel' : 'faux_hover';
        for (var i = 0; (index == -1) && (i < allSel.length); i++) {
            if (allSel[i].classList.contains(testClass)) {
                index = i;
            }
        }
        // Get the next item
        if (!index && scrollUp) {
            // Moving up from the first item
            index = allSelMax;
        } else if ((index == allSelMax) && !scrollUp) {
            // Moving down from the last item
            index = 0;
        } else if (scrollUp) {
            // Moving up
            index--;
        } else {
            // Moving down
            index++;
        }
        // Deselect the current option
        curSel?.classList.remove('faux_hover');
        allSel[index].classList.add('faux_hover');
        // Scroll to keep the element centred?
        if (this.getOptionList().scrollHeight > this.getOptionList().clientHeight) {
            var eleHeight = this.getOptionList().querySelector('li').offsetHeight;
            var newTop = allSel[index].offsetTop - parseInt((this.getOptionList().clientHeight - eleHeight) / 2);
            if (newTop < eleHeight) {
                newTop = 0;
            } else if ((newTop + this.getOptionList().clientHeight) > this.getOptionList().scrollHeight) {
                newTop = this.getOptionList().scrollHeight - this.getOptionList().clientHeight;
            }
            this.getOptionList().scrollTop = newTop;
        }
    }

    // Mobile option setup
    this.setupMobile = function () {
        // Already set up?
        if (Input.isDefined(this.getMobileSelect())) {
            return;
        }
        // Add to the DOM, storing the internal DOM reference
        select = document.createElement('select');
        select.id = `${this.getID()}-select`;
        DOM.setAttribute(select, 'dropdown', this.getID());
        select.disabled = this.isDisabled();
        // Add the individual elements
        var optgroup = '';
        var options = {};
        options[optgroup] = [];
        this.getOptionList().querySelectorAll('li').forEach((opt) => {
            // Skip certain options
            if (opt.classList.contains('search')) {
                return;
            }
            // Continue processing
            var label = DOM.getAttribute(opt, 'label-text') ?? opt.textContent;
            if (opt.classList.contains('optgroup')) {
                // Option Group
                optgroup = label;
                options[optgroup] = [];
            } else {
                // Selectable option
                var selectOption = document.createElement('option');
                selectOption.value = DOM.getAttribute(opt, 'id');
                selectOption.text = label;
                options[optgroup].push(selectOption);
            }
        });
        Object.keys(options).forEach((groupLabel) => {
            if (groupLabel === '') {
                // Standalone
                options[groupLabel].forEach((item) => { select.appendChild(item); });
            } else {
                // Within an option group
                var optgroup = document.createElement('optgroup');
                optgroup.label = groupLabel;
                options[groupLabel].forEach((item) => { optgroup.appendChild(item); });
                select.appendChild(optgroup);
            }
        });
        select.value = this.getValue();
        this.getHead().appendChild(select);
        // Attach the selecting events
        Events.attach(select, 'change', (e) => {
            var sel = e.target;
            if (Input.isDefined(sel)) {
                get('dropdowns')[DOM.getAttribute(sel, 'dropdown')].setValue(sel.value);
            }
        });
    }

    /* Setup */
    this.setupKeyboard = function () {
        // Catch certain key presses
        Events.attach(this.getWidget(), 'keydown', function (e) {
            // Skip anything that isn't a tab (9), enter (13), space (32), up (38), down (40) or escape (27) key
            var charCode = DOM.getKeyPress(e);
            if ([9,13,32,38,40,27].indexOf(charCode) == -1) {
                return;
            }

            e.preventDefault();
            switch (charCode) {
            case  9 : // Tab
                this.optionsHide(true);
                break;
            case 13 : // Enter
            case 32 : // Space
                // If opened, act as a click
                if (this.optionsVisible()) {
                    this.fauxClick();
                }
                // Otherwise, open
                else {
                    this.optionsShow();
                }
                break;
            case 38 : // Up
                this.fauxScrollUp();
                break;
            case 40 : // Down
                this.fauxScrollDown();
                break;
            case 27 : // Escape
                this.optionsHide();
                break;
            }
        }.bind(this));
        // Clear any faux hovers
        Events.attach(this.getOptionList(), 'mouseenter', function () {
            this.getOptionList().querySelector('.faux_hover')?.classList.remove('faux_hover');
        }.bind(this));
    }
    this.setupSearch = function () {
        if (!this.isSearchable()) {
            return;
        }

        // Add the event listener to process
        var debounce;
        Events.attach(searchField, 'keyup', (e) => {
            var term = e.target.value.trim().toLowerCase();
            var reset = (term === '');
            // Debounce, so clear if we are waiting from a previous change
            if (Input.isDefined(debounce)) {
                window.clearTimeout(debounce);
            }
            // Perform the search after a short pause (to catch potential later changes)
            debounce = window.setTimeout(() => {
                // Loop through all the options, finding those that match
                opt.forEach((ele) => {
                    // Skip option groups
                    if (ele.classList.contains('fixed-search') || ele.classList.contains('optgroup')) {
                        return;
                    } else if (reset) {
                        // Resetting to always on?
                        ele.classList.remove('hidden');
                    } else if (DOM.getAttribute(ele, 'search').indexOf(term) == -1) {
                        // No match, hide
                        ele.classList.add('hidden');
                    } else {
                        // Matches, ensure visible
                        ele.classList.remove('hidden');
                    }
                });
            }, 500);
        });
    }

    /* Constructor */
    this.setupKeyboard();
    this.setupSearch();
}

setupDropdowns = () => {
    set('dropdowns', []);
    var all = DOM.children('dl.dropdown');
    for (var i = 0; i < all.length; i++) {
        get('dropdowns')[all[i].dataset.id] = new Dropdown(all[i]);
    }
}

// Some onload processing
get('dom:load').push(() => {
    // Build an object will all the objects
    setupDropdowns();

    // Click handler
    Events.attach(document, 'click', (e) => {
        // Does this click event relate to a dropdown?
        var dropdown;
        if (!Input.isDefined(e.target)) {
            return;
        }
        if (Input.isFunction(e.target.closest)) {
            dropdown = e.target.closest('dl.dropdown');
        }
        if (!Input.isDefined(dropdown)) {
            return;
        }

        // Deemed as a click from a dropdown widget, so process it
        get('dropdowns')[dropdown.dataset.id].processClick(e.target);
    });

    // Tabindex management
    DOM.children('[data-tabindex]').forEach((ele) => {
        // Losing focus on a dropdown
        if (ele.classList.contains('dropdown')) {
            Events.attach(ele, 'blur', (e) => { get('dropdowns')[e.target.dataset.id].blur(); });
        }

        // Tabbing away
        Events.attach(ele, 'keydown', (e) => {
            // Skip non-tab presses
            if (DOM.getKeyPress(e) != 9) {
                return;
            }
            // Do not do the browser's default action
            e.preventDefault();
            // Determine the relevant target and jump to it
            DOM.performTabindex(e.target, e.shiftKey);
        });
    });

    // Setup the mobile views
    Events.attach(document, 'debear:viewport:mobile', () => {
        Object.keys(get('dropdowns')).forEach((key) => { get('dropdowns')[key].setupMobile(); });
    });
});
