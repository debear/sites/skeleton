/* Setup Processing */
class ScrollTable {
    // Initial setup on first render
    static initialSetup(force) {
        DOM.children('scrolltable').forEach((ele) => { ScrollTable.processSetup(ele, force); });
    }

    // Re-run the setup after a resize
    static resizeSetup() {
        ScrollTable.initialSetup(true);
    }

    // Perform the first render actions
    static processSetup(ele, force) {
        if (!Input.isDefined(ele) // Unknown element (at this time)
            || !DOM.isDisplayed(ele) // Element not visible? (This will affect our calcs, so can only run when visible)
            || (Input.isDefined(DOM.getAttribute(ele, 'setup')) && !Input.isTrue(force)) // Previously run?
        ) {
            return;
        }
        DOM.setAttribute(ele, 'setup', true);

        // Reset an earlier run
        ele.classList.remove('scrollable');

        var eleMain = ele.querySelector('main');
        var eleData = ele.querySelector('data');
        var eleDatalist = ele.querySelector('datalist');

        // Process scrolling events
        Events.attach(eleData, 'scroll', ScrollTable.scrollShadow);

        // Scrollable?
        var mainWidth = eleMain.clientWidth;
        var datalistWidth = eleDatalist.clientWidth;
        var width = mainWidth + datalistWidth;
        if (ele.parentNode.clientWidth < width) {
            ele.classList.add('scrollable');
        } else {
            ele.classList.remove('scrollable');
        }
        // Setup the scrollbar height
        if (!Input.isDefined(DOM.getAttribute(ele, 'scroll-height'))) {
            var mainHeight = eleMain.offsetHeight;
            var dataHeight = eleData.offsetHeight;
            DOM.setAttribute(ele, 'scroll-height', dataHeight - mainHeight);
        }
        // Initial scroll shadow setup (run in all instances)
        ScrollTable.scrollShadow(ele);

        // Mobile select handler
        DOM.children('mobile-select select').forEach((sel) => { Events.attach(sel, 'change', ScrollTable.selectHandler); });

        // Scroll position?
        var colDefault = DOM.getAttribute(ele, 'default');
        var colTotal = DOM.getAttribute(ele, 'total');
        if (!Input.isDefined(colDefault) || !Input.isDefined(colTotal) || !colTotal) {
            // Skip if not configured
            return;
        }
        // Only scroll if the column does not appear in the left half of the data view
        var colWidth = datalistWidth / colTotal;
        var offsetCols = (eleData.clientWidth / colWidth) / 2;
        if (colDefault < offsetCols) {
            return;
        }
        eleData.scrollLeft = colWidth * (colDefault - offsetCols);
        // Initial scroll shadow setup (re-run if manually adjusted scroll position)
        ScrollTable.scrollShadow(ele);
    }

    // Select handler
    static selectHandler(e) {
        var sel = e.target;
        // Update the label
        var tbl = sel.closest('scrolltable');
        tbl.querySelector('current').innerHTML = sel.querySelector(`option[value="${sel.value}"]`).innerHTML;
        // Display the new value
        var list = tbl.querySelector('datalist');
        list.querySelectorAll(`cell[data-field="${sel.value}"]`).forEach((cell) => { cell.classList.remove('hidden-m'); });
        // Hide the old
        var prev = DOM.getAttribute(tbl, 'current');
        list.querySelectorAll(`cell[data-field="${prev}"]`).forEach((cell) => { cell.classList.add('hidden-m'); });
        // Update our internal reference
        DOM.setAttribute(tbl, 'current', sel.value);
    }

    // Handle the scroll shadows
    static scrollShadow(e) {
        var sel = e?.target?.closest('scrolltable') ?? e;
        var data = sel.querySelector('data');
        var tbl = sel.querySelector('datalist');
        // Leading shadow
        if (data.scrollLeft > 0) {
            sel.classList.add('shadow-l');
        } else {
            sel.classList.remove('shadow-l');
        }
        // Trailing shadow
        if (tbl.clientWidth > (data.clientWidth + data.scrollLeft)) {
            sel.classList.add('shadow-r');
        } else {
            sel.classList.remove('shadow-r');
        }
    }
}

/* Page Loaders */
get('dom:load').push(() => {
    ScrollTable.initialSetup();

    // Tab display - initial setup (if not previous run)
    Events.attach(document, 'debear:subnav:display', (e) => {
        e.detail.ele.querySelectorAll('scrolltable').forEach((ele) => { ScrollTable.processSetup(ele); });
    });

    // Re-run on resize
    Events.attach(window, 'resize', ScrollTable.resizeSetup);
});
