/**
 * Methods relating to user accounts
 */
class Account {
    // Log the user out
    static doLogout(target) {
        get('account:ajax').request({
            'url': '/logout',
            'method': 'POST',
            'success': () => {
                // Reload the page on success, unless we've been passed a distinct URL
                var to = DOM.getAttribute(target, 'to');
                if (Input.isDefined(to)) {
                    DOM.redirectPage(to);
                } else {
                    location.reload();
                }
            },
            'failure': () => alert('Oops, something went wrong. Please try again.')
        });
    }

    // Re-send the email address verification email
    static resendVerify(target) {
        // What version are we processing?
        var isNav = Input.isDefined(target.closest('nav'));

        // Flag that we're sending
        target.classList.add('sent');
        var icon; var text;
        if (!isNav) {
            icon = target;
            text = target.querySelector('a');
            text.classList.add('no_underline');
        } else {
            icon = target.querySelector('span');
            text = icon;
        }
        icon.classList.remove('icon_refresh');
        icon.classList.add('icon_loading_status');
        if (!Input.isDefined(DOM.getAttribute(text, 'original'))) {
            DOM.setAttribute(text, 'original', text.innerHTML);
        }
        text.innerHTML = 'Processing...';

        // Perform the Ajax request
        get('account:ajax').request({
            'url': `//${location.host.replace(/^([^\.]+\.)?([^\.]+\.[^\.]+)$/, 'my.$2')}/account/resend-verify`,
            'method': 'PATCH',
            'success': () => {
                icon.classList.remove('icon_loading_status');
                icon.classList.add('icon_valid');
                text.innerHTML = 'Verification email re-sent!';
                window.setTimeout(() => {
                    var ele;
                    if (!isNav) {
                        ele = icon.closest('verify');
                    } else {
                        ele = icon.closest('li.item');
                        Account.toggleUnverified(ele.querySelector('a.item'));
                    }
                    ele.remove();
                }, 5000);
            },
            'failure': () => {
                icon.classList.remove('icon_loading_status');
                icon.classList.add('icon_error');
                text.innerHTML = 'Oops, something went wrong. Please try again later...';
                window.setTimeout(() => {
                    target.classList.remove('sent');
                    icon.classList.remove('icon_error');
                    icon.classList.add('icon_refresh');
                    text.innerHTML = DOM.getAttribute(text, 'original');
                }, 5000);
            }
        });
    }

    // Show/Hide the verification details in the mobile nav
    static toggleUnverified(ele) {
        var li = ele.closest('li.item');
        var span = ele.querySelector('a.item span');
        var verify = li.querySelector('verify');
        var show = span.classList.contains('icon_right_nav_expand');
        span.classList.remove(show ? 'icon_right_nav_expand' : 'icon_right_nav_contract');
        span.classList.add(show ? 'icon_right_nav_contract' : 'icon_right_nav_expand');
        if (show) {
            // Show
            li.classList.add('open');
            verify.classList.remove('hidden');
        } else {
            // Hide
            li.classList.remove('open');
            verify.classList.add('hidden');
        }
    }
}

// Event handlers
get('dom:load').push(() => {
    set('account:ajax', new Ajax());

    // Click handler for the logout modal
    Events.attach(DOM.child('#header_logout_link'), 'click', (e) => Account.doLogout(e.target));

    // Click handler for the email verification modal
    Events.attach(DOM.child('a.item.unverified'), 'click', (e) => Account.toggleUnverified(e.target.closest('.onclick_target')));

    // Click handlers
    DOM.children('verify .resend').forEach((e) => {
        Events.attach(e, 'click', (e) => {
            var target = e.target.closest('.resend');
            if (!target.classList.contains('sent')) {
                Account.resendVerify(target);
            }
        });
    });
});
