/**
 * Methods relating to logging in
 */
class CookieManager {
    // User has requested we disable the analytics cookie
    static disableAnalytics() {
        Analytics.disable();
        CookieManager.setPrefs('analytics', false);
    }

    // User has allowed use of the analytics cookie
    static enableAnalytics() {
        Analytics.enable();
        CookieManager.setPrefs('analytics', true);
    }

    // Worker method for updating cookie preferences
    // @todo: Works when only a single preference...
    static setPrefs(type, allow) {
        // If we're about to hide the message, skip as we're going to dispay a new message instead
        if (get('cookie:debounce-msg')) {
            window.clearTimeout(get('cookie:debounce-msg'));
        }
        // If we have a previous request debounce, cancel it and don't proceed as we'd be seting the value back to itself
        if (get('cookie:debounce-action')) {
            window.clearTimeout(get('cookie:debounce-action'));
            set('cookie:debounce-action', false);
            return;
        }
        // Okay, let's now (kick-off) storing the change
        CookieManager.showMessage('section loading', 'Please wait, saving...', false);
        set('cookie:debounce-action', window.setTimeout(() => {
            get('cookie:ajax').request({
                'method': 'PATCH',
                'url': '/cookies',
                'args': {
                    'type': type,
                    'allow': allow
                },
                'success': () => { CookieManager.showMessage('success icon_valid', 'Your cookie preferences have been updated!', true); },
                'failure': () => { CookieManager.showMessage('error icon_delete', 'Sorry, an error occurred, please try again later.', true); }
            });
        }), 500);
    }

    static showMessage(status, message, hide) {
        // Update
        set('cookie:debounce-action', false);
        var widget = DOM.child('cookies .box');
        widget.innerHTML = message;
        ['section','loading','success','icon_valid','error','icon_delete'].forEach((s) => { widget.classList.remove(s); });
        status.split(' ').forEach((s) => { widget.classList.add(s); });
        widget.classList.remove('hidden');
        // Remove?
        if (Input.isTrue(hide)) {
            set('cookie:debounce-msg', window.setTimeout(() => { widget.classList.add('hidden'); }, 4000));
        }
    }
}

/* Click handlers for Cookie warnings */
get('dom:load').push(() => {
    if (!Input.isDefined(DOM.child('cookies'))) {
        return;
    }

    set('cookie:ajax', new Ajax());
    set('cookie:debounce-action', false);
    set('cookie:debounce-msg', false);

    Events.attach(DOM.child('cookies'), 'click', (e) => {
        // Was the checkbox clicked?
        if (e.target.tagName.toLowerCase() == 'input') {
            var enable = e.target.checked;
            if (DOM.getAttribute(e.target, 'mode', { 'default': 'normal' }) == 'inverse') {
                enable = !enable;
            }
            if (enable) {
                CookieManager.enableAnalytics();
            } else {
                CookieManager.disableAnalytics();
            }
        }
    });
});
