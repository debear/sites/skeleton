// Date Field processing class
function DateField(inId) {
    // Instance vars
    var id = inId;
    var input = DOM.child(`#${id}`);
    var inputDD = DOM.child(`#${id}_dd`);
    var inputMM = DOM.child(`#${id}_mm`);
    var inputYY = DOM.child(`#${id}_yy`);

    // Set the input value from an external update
    this.storeValue = function () {
        // Update the hidden field
        var value = this.getInputYY().value.toString().padStart(4, '0') + '-' + this.getInputMM().value.toString().padStart(2, '0') + '-' + this.getInputDD().value.toString().padStart(2, '0');
        this.getHidden().value = value;
        // Validate
        var isValid = DateManip.validate(value);
        // Fire an event to let others know of the update
        Events.fire('date_field-set', {'id': this.getID(), 'value': value, 'valid': isValid});
    }

    // Getters
    this.getID = function () {
        return id;
    }
    this.getHidden = function () {
        return input;
    }
    this.getInputDD = function () {
        return inputDD;
    }
    this.getInputMM = function () {
        return inputMM;
    }
    this.getInputYY = function () {
        return inputYY;
    }
}

// Helpers methods
class DateFieldHelpers {
    // Static methods
    static getDateFieldID(id) {
        if (!Input.isDefined(id.substr(-3).match(/_(dd|mm|yy)$/)) || !Input.isDefined(DOM.child(`#${id.replace(/_(dd|mm|yy)$/, '')}`))) {
            return null;
        }
        return id.replace(/_(dd|mm|yy)$/, '');
    }
}

set('date_fields', {});
get('dom:load').push(() => {
    // Build an object will all the objects
    var all = DOM.children('dl.dropdown.date_mm');
    for (var i = 0; i < all.length; i++) {
        get('date_fields')[all[i].dataset.id.replace(/_mm$/, '')] = new DateField(all[i].dataset.id.replace(/_mm$/, ''));
    }
    // Handle a change in an input
    Events.attach(document, 'keyup', (e) => {
        // Only continue if this applies to a month field
        var id = DateFieldHelpers.getDateFieldID(e.target.id);
        if (Input.isDefined(id)) {
            get('date_fields')[id].storeValue();
        }
    });
    // Handle a change in the month dropdown
    Events.attach(document, 'debear:dropdown-set', (e) => {
        // Only continue if this applies to a month field
        var id = DateFieldHelpers.getDateFieldID(e.detail.id);
        if (Input.isDefined(id)) {
            get('date_fields')[id].storeValue();
        }
    });
});
