/* Action class */
class SubNav {
    static onLoad(force) {
        SubNav.setup();
        SubNav.setDefaultToggle();
        if (window.location.hash || Input.isTrue(force)) {
            var opt = window.location.hash.substring(1);
            var tab = DOM.child(`item[data-tab="${opt}"`);
            if (Input.isDefined(tab)) {
                SubNav.toggle(tab.closest('subnav'), opt, true);
            }
        }
    }

    static setup() {
        /* Click handler */
        DOM.children('subnav desk').forEach((subnav) => {
            Events.attach(subnav, 'click', (e) => {
                var target = e.target.closest('item');
                if (Input.isDefined(target) && !target.classList.contains('sel')) {
                    SubNav.toggle(target.closest('subnav'), DOM.getAttribute(target, 'tab'), false);
                }
            });
        });

        /* Mobile Dropdown handler */
        DOM.children('subnav select').forEach((subnav) => {
            Events.attach(subnav, 'change', (e) => {
                var opt = e.target.value;
                if (!e.target.closest('subnav').querySelector(`item[data-tab="${opt}"`).classList.contains('sel')) {
                    SubNav.toggle(e.target.closest('subnav'), opt);
                }
            });
        });
    }

    static setDefaultToggle() {
        set('subnav:default', DOM.getAttribute(DOM.child('item.sel'), 'tab'));
    }

    static toggle(root, opt, initial) {
        // Get and hide the current display
        var currTab = root.querySelector('item.sel');
        var curr = DOM.getAttribute(currTab, 'tab');
        currTab.classList.remove('sel');
        currTab.classList.add('unsel');
        DOM.children(`.subnav-${curr}`).forEach((c) => {
            c.classList.add('hidden');
            SubNav.event('hide', opt, c);
        });
        // Display the selection
        var optTab = root.querySelector(`item[data-tab="${opt}"`);
        optTab.classList.remove('unsel');
        optTab.classList.add('sel');
        root.querySelector('mob sel').innerHTML = optTab.innerHTML;
        DOM.children(`.subnav-${opt}`).forEach((c) => {
            c.classList.remove('hidden');
            SubNav.event('display', opt, c);
        });
        // Finally, set the fragment identifier
        if (!initial) {
            if (get('subnav:default') != opt) {
                history.pushState("", document.title, `${window.location.pathname}#${opt}`);
            } else {
                history.pushState("", document.title, window.location.pathname);
            }
        } else {
            root.querySelector('mob select').value = opt;
        }
    }

    static event(mode, opt, c) {
        Events.fire(`subnav:${mode}`, { 'tab': opt, 'ele': c });
    }
}

/* Handlers */
get('dom:load').push(() => { SubNav.onLoad(); });
