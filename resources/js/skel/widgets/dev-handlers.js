/**
 * Empty error catching and reporting
 */
function BugReporting() {
    this.catch = function (err) {
        console.error("BugReporting::catch() ", err);
    }
    this.exception = function (err) {
        console.error("BugReporting::exception() ", err);
    }
    this.wrap = function (inFunc) {
        console.error("BugReporting::wrap() ", inFunc);
    }
    this.raise = function (err) {
        console.error("BugReporting::raise() ", err);
    }
    this.log = function (err, trace) {
        console.error("BugReporting::log() ", err, trace);
    }
}
set('BugReporting', new BugReporting());
