// Install the click handlers for our debug panel
get('dom:load').push(() => {
    // Click handler for toggling menus
    Events.attach(DOM.child('.footer_debug'), 'click', (e) => {
        var dl;

        // Toggle the display?
        if (e.target.id == 'footer_debug_link') {
            // Toggle visuals
            dl = e.target.closest('li').querySelector('dl');
            dl.classList.toggle('hidden');
            // Update the label
            e.target.dataset.defaultLabel = e.target.dataset.defaultLabel ?? e.target.innerHTML.replace(/(Show|Hide)/, '@@@');
            var label = e.target.dataset.defaultLabel;
            var rplc = (dl.classList.contains('hidden') ? 'Show' : 'Hide');
            e.target.innerHTML = label.replace(/@@@/, rplc);
            return;
        }

        // Toggle a principle component?
        if (Input.isDefined(e.target.closest('dt.title')) && !e.target.closest('dt.title').classList.contains('sel')) {
            dl = e.target.closest('dl');
            // Remove the current selection and enable the new one
            var curTitleSel = dl.querySelector('.title.sel');
            [curTitleSel, e.target].forEach((o) => {
                o.classList.toggle('sel');
                dl.querySelector('dd.' + o.dataset.code).classList.toggle('hidden');
            });
            return;
        }

        // Toggle a secondary component?
        if (Input.isDefined(e.target.closest('dt.subtitle')) && !e.target.closest('dt.subtitle').classList.contains('sel')) {
            dl = e.target.closest('dl');
            // Remove the current selection and enable the new one
            var curSubtitleSel = dl.querySelector('.subtitle.sel');
            [curSubtitleSel, e.target].forEach((o) => {
                o.classList.toggle('sel');
                dl.querySelector(`dd.${o.dataset.code}`).classList.toggle('hidden');
            });
            return;
        }

        // Toggle a query summary?
        var queryRef = DOM.getAttribute(e.target, 'query');
        if (Input.isDefined(queryRef)) {
            e.target.closest('dl.individual').querySelector(`dd.sql.ref-${queryRef}`).classList.toggle('hidden');
        }
    });
});
