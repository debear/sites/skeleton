/**
 * Methods relating to logging in
 */
class Login {
    // Show or hide the login fields
    static toggle(ele) {
        var toggled = false;

        // Toggle triggered with one of the login widgets
        var li = ele.closest('li');
        if (Input.isDefined(li)) {
            toggled = true;
            Login.toggleLoginForm(li.querySelector('login'));
        }

        // Toggle the mobile nav?
        var toggle = ele.closest('li.item');
        if (toggle?.classList.contains('mobile-only')) {
            toggled = true;
            Login.toggleMobileItem(toggle);
        }

        // Externally triggered toggle (e.g., in page link)?
        if (!toggled) {
            // Which menu are we displaying?
            var isDesktop = DOM.isDisplayed('.header_login');
            var baseEle = DOM.child(isDesktop ? '.header_login' : 'nav.mobile-only');
            if (!isDesktop && !baseEle.classList.contains('open')) {
                // Toggle the main menu?
                baseEle.classList.add('open');
                // No need to continue if the login form is already open...
                var signinEle = baseEle.querySelector('li.item.mobile-only.onclick_target');
                if (signinEle.classList.contains('open')) {
                    return;
                }
                Login.toggleMobileItem(signinEle);
            }
            Login.toggleLoginForm(baseEle.querySelector('login'));
        }
    }

    // Perform the logging of a specific form
    static toggleLoginForm(loginForm) {
        loginForm.classList.toggle('hidden');
        if (!loginForm.classList.contains('hidden')) {
            loginForm.querySelector('.username').focus();
        } else {
            loginForm.closest('login').parentNode.querySelector('.login.error')?.classList.add('hidden');
        }
    }

    // Toggle the mobile "Sign In" list item
    static toggleMobileItem(toggle) {
        var span = toggle.querySelector('.subnav');
        var closing = toggle.classList.contains('open');
        span.classList.remove(closing ? 'icon_right_nav_contract' : 'icon_right_nav_expand');
        span.classList.add(closing ? 'icon_right_nav_expand' : 'icon_right_nav_contract');
        toggle.closest('li.item').classList.toggle('open');
    }

    // Toggle whether the password
    static toggleVisiblePassword(toggle) {
        var show = toggle.classList.contains('icon_password_show');
        var elePass = toggle.closest('dd').querySelector('input');
        elePass.type = (show ? 'text' : 'password');
        toggle.classList.remove(show ? 'icon_password_show' : 'icon_password_hide');
        toggle.classList.add(show ? 'icon_password_hide' : 'icon_password_show');
    }

    // Load the forgotten details page, linking to an entered user_id
    static resetDetails(loginForm) {
        var widget = DOM.child('.header_register').querySelector('a');
        var redirect = widget.href.replace(/\/register$/, '/login-details');
        if (loginForm.querySelector('.username').value.trim()) {
            redirect += '?username=' + encodeURIComponent(loginForm.querySelector('.username').value.trim());
        }

        // Opening in a popup?
        if (Input.isDefined(widget.href.match(/^http/i))) {
            // The random number is not for cryptographic purposes, so consider the SemGrep warning a false positive.
            // nosemgrep javascript-crypto-rule-node_insecure_random_generator.
            var popup = window.open(redirect, 'popup_' + Math.floor(Math.random() * Math.floor(9999999)));
            if (window.focus) {
                popup.focus();
            }
        } else {
            DOM.redirectPage(redirect);
        }
    }

    // Process form submission - mini validation and processing
    static submit(loginForm) {
        // Validation
        var errs = [];
        if (loginForm.querySelector('.username').value.trim() === '') {
            errs.push('Please enter a username.');
        }
        if (loginForm.querySelector('.password').value.trim() === '') {
            errs.push('Please enter a password.');
        }

        if (errs.length) {
            // Give the error
            alert('There were errors in your details:\n\n- ' + errs.join('\n- '));
            return false;
        }

        // Now submit via an AJAX request
        Login.setSubmitLabel(loginForm);
        var errWidget = loginForm.closest('login').parentNode.querySelector('.login.error');
        get('login:ajax').request({
            'method': 'POST',
            'url': '/login',
            'args': {
                'username': loginForm.querySelector('.username').value,
                'password': loginForm.querySelector('.password').value,
                'rememberme': loginForm.querySelector('.rememberme').checked
            },
            'success': () => {
                errWidget?.classList.add('hidden');
                // Reload the page on success, unless we've been passed a distinct URL
                var to = DOM.getAttribute(loginForm.closest('login'), 'to');
                if (Input.isDefined(to)) {
                    DOM.redirectPage(to)
                } else {
                    location.reload();
                }
            },
            'failure': (retValue) => {
                Login.resetSubmitLabel(loginForm);
                loginForm.querySelector('.username').focus();
                if (Input.isDefined(errWidget)) {
                    var errStr = 'Oops, that didn&#39;t quite work &ndash; please try again!'; // A fallback message, likely returned due to server errors
                    if (retValue.status.toString().match(/^40[13]$/)) {
                        errStr = 'Invalid username/password combination';
                    } else if (retValue.status.toString().match(/^4[01]9$/)) {
                        errStr = (retValue.status == 409
                            ? 'Reloading the page&hellip;' // Already logged in
                            : 'Request expired &ndash; reloading the page for you to try again'); // Token from original request expired
                        window.setTimeout(() => {
                            location.reload(); // Reload the page to get a new token
                        }, 3000);
                    }
                    errWidget.innerHTML = errStr;
                    errWidget.classList.remove('hidden');
                }
            }
        });
    }

    // Update the Submit button to reflect the fact we're logging in
    static setSubmitLabel(loginForm) {
        var btn = loginForm.querySelector('dd.submit .submit');
        if (!DOM.hasAttribute(btn, 'data-submit-original')) {
            DOM.setAttribute(btn, 'submit-original', btn.innerHTML);
        }
        btn.innerHTML = DOM.getAttribute(btn, 'submit-label');
    }

    // Restore the label of the Submit button to is original alue
    static resetSubmitLabel(loginForm) {
        var btn = loginForm.querySelector('dd.submit .submit');
        btn.innerHTML = DOM.getAttribute(btn, 'submit-original');
    }
}

// Event handlers
get('dom:load').push(() => {
    set('login:ajax', new Ajax());

    // Click handler for the login modal
    ['header', 'nav'].forEach((p) => {
        Events.attach(DOM.child(`#${p}_login_link`), 'click', (e) => { Login.toggle(e.target); });
    });

    // Click handler for toggling menus/links
    DOM.children('login').forEach((lf) => {
        Events.attach(lf, 'click', (e) => {
            var loginForm = e.target.closest('form');
            if (e.target.classList.contains('cancel')) {
                // Close the login box?
                Login.toggle(loginForm);
            } else if (e.target.classList.contains('forgotten')) {
                // Access forgotten details?
                Login.resetDetails(loginForm);
            } else if (e.target.classList.contains('submit')) {
                // Submit handler
                Events.cancel(e);
                Login.submit(loginForm);
            } else if (e.target.classList.contains('password-toggle')) {
                // Show/Hide password
                Login.toggleVisiblePassword(e.target);
            }
        });
    });

    // Automated form submission
    Events.attach(DOM.child('login form'), 'submit', (e) => {
        Events.cancel(e);
        Login.submit(e.target);
    });
});
