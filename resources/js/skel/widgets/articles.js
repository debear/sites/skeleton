get('dom:load').push(() => {
    Events.attach(DOM.child('body'), 'click', (e) => {
        var ele = e.target;
        if (ele.classList.contains('article-toggle')) {
            var eleArticle = ele.closest('p');
            eleArticle.querySelector('.contracted').classList.toggle('hidden');
            eleArticle.querySelector('.expanded').classList.toggle('hidden');
        }
    });
});
