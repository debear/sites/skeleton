class Calendar {
    // Setup each calendar widget
    static setup() {
        set('calendar:ajax', new Ajax());
        set('calendar:json', {});
        DOM.children('calendar').forEach((cal) => {
            // Get the URL, skipping if there is no JSON
            var jsonURL = DOM.getAttribute(cal, 'json');
            if (!Input.isDefined(jsonURL)) {
                Calendar.renderInitial(cal);
                return;
            }

            // Load, marking the URL as being loaded
            var curr = get('calendar:json')[jsonURL];
            if (Input.isDefined(curr)) {
                Calendar.renderInitial(cal);
                return;
            }
            Calendar.displayLoading(cal);
            get('calendar:json')[jsonURL] = {'loading': true};
            get('calendar:ajax').request({
                'url': jsonURL,
                'success': (retValue) => {
                    get('calendar:json')[jsonURL] = retValue;
                    Calendar.renderInitial(cal);
                },
                'failure': () => { Calendar.displayError(cal); }
            });
        });
    }

    // Add the click handler
    static setupClickHandler() {
        Events.attach(document, 'click', (e) => {
            // Only applies to a calendar widget
            var ele = e.target;
            var cal = ele.closest('calendar');
            if (!Input.isDefined(cal)) {
                return;
            }
            // What was clicked?
            if (ele.classList.contains('month-toggle')) {
                // Open/Close the calendar
                Calendar.displayToggle(cal);
            } else if (ele.classList.contains('month-click')) {
                // New date selected
                Events.fire('calendar:click', {target: ele, widget: cal, date: DOM.getAttribute(ele, 'date')});
                Calendar.displayToggle(cal);
            } else if (ele.classList.contains('switch-month')) {
                // Switch to previous/next month
                var month = DOM.getAttribute(ele, 'month');
                Calendar.renderMonth(cal, month);
                cal.querySelector('select').value = month;
            }
        });
    }

    // Widget-specific setup
    static setupWidget(cal) {
        // Setup the event listener when the calendar's select is changed
        Events.attach(cal.querySelector('select'), 'change', (e) => {
            var sel = e.target;
            var cal = sel.closest('calendar');
            Calendar.renderMonth(cal, sel.value);
        });

        // Calender close toggle
        Events.attach(cal.querySelector('.head a'), 'click', (e) => { Calendar.displayToggle(e.target.closest('calendar')); });
    }

    // Show a loading screen within a calendar widget
    static displayLoading(cal) {
        cal.querySelector('.cal-body').innerHTML = '<fieldset class="section loading">Please wait, loading...</fieldset>';
    }

    // Render an error within a calendar widget
    static displayError(cal, err) {
        cal.querySelector('.cal-body').innerHTML = `<fieldset class="error icon icon_error">${err ?? 'Oops. Please try again later...'}</fieldset>`;
    }

    // Once ready, render the (default) calendar
    static renderInitial(cal) {
        // The monthly options
        var selOpt = [];
        var mNames = [
            '', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'
        ];
        var m = DOM.getAttribute(cal, 'min').substr(0, 7);
        var mEnd = DOM.getAttribute(cal, 'max').substr(0, 7);
        var dCurr = DOM.getAttribute(cal, 'current');
        var mCurr = dCurr.substr(0, 7);
        do {
            var mNum = parseInt(m.substr(5));
            var mYear = parseInt(m.substr(0, 4));
            selOpt.push(`<option value="${m}"` + (m == mCurr ? ' selected="selected"' : '') + '>' + `${mNames[mNum]} ${mYear}</option>`);
            // Move on to the next month
            mNum++;
            if (mNum == 13) {
                mNum = 1;
                mYear++;
            }
            m = `${mYear}-` + (mNum < 10 ? '0' : '') + mNum;
        } while (m <= mEnd);
        // The markup
        cal.querySelector('.cal-body').innerHTML = '<div class="head">\
            <span>[ <a>Close</a> ]</span>\
            <h3>Select a Date</h3>\
        </div>\
        <div class="month">\
            <a class="no_underline icon icon_scroll_left switch-month switch-prev"></a>\
            <a class="no_underline icon icon_scroll_right switch-month switch-next"></a>\
            <select>\
                ' + selOpt.join('') + '\
            </select>\
        </div>\
        <ul class="inline_list days clearfix">\
            <li>Mon</li><li>Tue</li><li>Wed</li><li>Thu</li><li>Fri</li><li>Sat</li><li>Sun</li>\
        </ul>\
        <ul class="inline_list dates clearfix"></ul>';
        Calendar.setupWidget(cal);
        Calendar.renderMonth(cal, mCurr);
    }

    // Render the dates in a given month
    static renderMonth(cal, dispMonthStr) {
        // Calendar range
        var dateMin = DOM.getAttribute(cal, 'min');
        var dateMax = DOM.getAttribute(cal, 'max');
        var dateCurr = DOM.getAttribute(cal, 'current');
        // Any date exceptions?
        var jsonURL = DOM.getAttribute(cal, 'json');
        var dateExc = Input.isDefined(jsonURL) && Input.isDefined(get('calendar:json')[jsonURL].exceptions) ? get('calendar:json')[jsonURL].exceptions : [];

        // Some month-based calcs
        var [dispRawYear, dispRawMonth] = dispMonthStr.split('-');
        var dispMonth = new Date(dispRawYear, parseInt(dispRawMonth) - 1, 1); // 1st of month
        // Previous month
        var prevRollover = (dispMonth.getMonth() == 0 ? 1 : 0);
        var prevMonth = new Date(dispMonth.getFullYear() - prevRollover, prevRollover ? 11 : dispMonth.getMonth() - 1, 1);
        var prevMonthLength = (new Date(dispMonth.getTime() - 86400000)).getDate(); // Remove 24 hours
        var prevMonthStr = '' + prevMonth.getFullYear() + '-' + (prevMonth.getMonth() < 9 ? '0' : '') + (prevMonth.getMonth() + 1);
        // Next month
        var nextRollover = (dispMonth.getMonth() == 11 ? 1 : 0);
        var nextMonth = new Date(dispMonth.getFullYear() + nextRollover, nextRollover ? 0 : dispMonth.getMonth() + 1, 1);
        var nextMonthStr = '' + nextMonth.getFullYear() + '-' + (nextMonth.getMonth() < 9 ? '0' : '') + (nextMonth.getMonth() + 1);
        var dispMonthLength = (new Date(nextMonth.getTime() - 86400000)).getDate(); // Remove 24 hours
        // Today
        var now = new Date();
        var dateToday = '' + now.getFullYear() + '-' + (now.getMonth() < 9 ? '0' : '') + (now.getMonth() + 1) + '-' + (now.getDate() < 10 ? '0' : '') + now.getDate();

        var dates = [];
        var i;
        // Any days in the previous month?
        var firstDay = dispMonth.getDay() ? dispMonth.getDay() : 7; // Correct if Sunday - so ranges from 0->6 becomes 1->7
        for (i = firstDay; i > 1; i--) {
            var d = prevMonthLength - i + 2;
            dates.push({'full': `${prevMonthStr}-${d}`, 'disp': d, 'css': 'month-prev'});
        }

        // Current month
        for (i = 1; i <= dispMonthLength; i++) {
            dates.push({'full': `${dispMonthStr}-` + (i < 10 ? '0' : '') + i, 'disp': i, 'css': 'month-curr'});
        }

        // Any days in the next month?
        var remDays = 8 - firstDay - (dispMonthLength % 7);
        if (remDays < 1) {
            remDays += 7;
        }
        if (remDays < 7) {
            for (i = 1; i <= remDays; i++) {
                dates.push({'full': `${nextMonthStr}-0` + i, 'disp': i, 'css': 'month-next'});
            }
        }

        // Render
        var ret = [];
        dates.forEach((d) => {
            // What CSS classes do we need?
            var css = [d.css];
            if (d.full == dateToday) {
                css.push('today');
            }
            if (d.full == dateCurr) {
                css.push('current');
            }
            if ((d.full < dateMin) || (d.full > dateMax) || Input.isDefined(dateExc[d.full])) {
                css.push('no-action');
            } else {
                css.push('month-click');
            }
            // Build the cell markup
            ret.push('<li class="' + css.join(' ') + `" data-date="${d.full}">${d.disp}</li>`);
        });
        cal.querySelector('.dates').innerHTML = ret.join('');

        // Show/Hide Prev/Next tabs
        var elePrev = cal.querySelector('.switch-prev');
        DOM.setAttribute(elePrev, 'month', prevMonthStr);
        if (dispMonthStr == dateMin.substr(0, 7)) {
            elePrev.classList.add('hidden');
        } else {
            elePrev.classList.remove('hidden');
        }
        var eleNext = cal.querySelector('.switch-next');
        DOM.setAttribute(eleNext, 'month', nextMonthStr);
        if (dispMonthStr == dateMax.substr(0, 7)) {
            eleNext.classList.add('hidden');
        } else {
            eleNext.classList.remove('hidden');
        }
    }

    // Show/Hide the picker
    static displayToggle(cal) {
        cal.querySelector('.cal-body').classList.toggle('hidden');
    }
}

// Install the click handlers for our widgets
get('dom:load').push(() => {
    Calendar.setupClickHandler();
    Calendar.setup();
});
