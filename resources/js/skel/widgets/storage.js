class Storage {
    static #transient = { }; // Transient, in-memory, cache automatically emptied on page close/reload

    // Add an object to local storage
    static setItem(key, value, expires) {
        // Set a transient value if the expiry is not appropriate for local storage
        if (expires <= 0) {
            this.#transient[key] = value;
            return;
        }
        // This request is for local storage, so attempt to add to the browser object
        try {
            localStorage.setItem(key, JSON.stringify({
                'c': !Input.isString(value),
                'e': expires ?? (Date.now() + 300000), // Ensure the expiry has a set value (arbitrarily five minutes)
                'v': JSON.stringify(value)
            }));
        } catch {
            // Silently ignore if the user has disabled the localStorage feature
        }
    }

    // Get a single object from local storage
    static getItem(key) {
        // Prioritise lookups in our transient storage
        if (Input.isDefined(this.#transient[key])) {
            return this.#transient[key];
        }
        // Not found in transient storage, so attempt to load from local storage
        var cacheValue;
        try {
            cacheValue = JSON.parse(localStorage.getItem(key));
        } catch {
            // If the user has disabled the localStorage feature, we know nothing is available
            return null;
        }
        if ((cacheValue?.e ?? -1) < Date.now()) {
            Storage.removeItem(key); // Remove from the cache as it's expired
            return null;
        }
        // If the input was stringified, convert it back and return
        return (cacheValue.c ? JSON.parse(cacheValue.v) : cacheValue.v);
    }

    // Remove a single object from local storage
    static removeItem(key) {
        try {
            localStorage.removeItem(key);
        } catch {
            // Silently ignore if the user has disabled the localStorage feature
        }
    }

    // Auto-remove objects from local storage that have expired
    static purge() {
        Object.entries(localStorage ?? {}).forEach(([key, value]) => {
            var cacheValue = JSON.parse(value);
            if ((cacheValue?.e ?? -1) < Date.now()) {
                Storage.removeItem(key);
            }
        });
    }
}

// Auto-purge on startup
get('dom:load').push(() => {
    Storage.purge();
});
