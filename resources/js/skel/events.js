/**
 * Methods to deal with JavaScript events
 */
class Events {
    // Attach an event to a DOM object
    static attach(widget, inEvent, inFunction) {
        widget?.addEventListener(inEvent, inFunction, false);
    }

    // Remove said event
    static remove(widget, inEvent, inFunction) {
        widget?.removeEventListener(inEvent, inFunction, false);
    }

    // Cancel a triggered event
    static cancel(e) {
        e.preventDefault();
        e.stopPropagation();
    }

    // Fire an event
    static fire(evtClass, memo) {
        document.dispatchEvent((new CustomEvent(`debear:${evtClass}`, { 'detail': memo })));
    }
}

// Default page loader
Events.attach(document, 'DOMContentLoaded', (de) => DOM.onLoad(de));
