/**
 * JavaScript Google Analytics methods
 */
function gtag() {
    dataLayer.push(arguments);
}

// Instantiate
class Analytics { // eslint-disable-line no-unused-vars
    static setup(urchins, opt) {
        // Set up the global tag
        window.dataLayer = window.dataLayer || [];

        // Add our custom options
        var gtagConfig = opt;
        gtagConfig.anonymize_ip = true;
        gtagConfig.force_ssl = true;
        gtagConfig.send_page_view = this.isEnabled();

        // Load
        gtag('js', new Date());
        urchins.forEach(id => gtag('config', id, gtagConfig));
    }

    /* Senders */
    // Send a (manual) PageView event
    static sendPageView() {
        if (!this.isEnabled()) {
            this.preserve('pageview');
            return;
        }
        gtag('event', 'page_view');
    }

    /* Event Preserver */
    static preserve(evt) {
        this.preservedEvents = this.preservedEvents || [];
        this.preservedEvents.push(evt);
    }

    static processPreserved() {
        this.preservedEvents?.forEach(
            function (evt) {
                if (Input.isString(evt)) {
                    evt = { 'type': evt };
                }
                // Then decide how to proceed
                switch (evt.type) {
                case 'pageview':
                    this.sendPageView();
                    break;
                }
            }.bind(this)
        );
        // Unset, as we've already sent it
        this.preservedEvents = [];
    }

    /* Status Getters/Setters */
    static isEnabled() {
        return this.enabled ?? true;
    }

    static disable() {
        this.enabled = false;
    }

    static enable() {
        this.enabled = true;
        // Any in-page events to now send?
        this.processPreserved();
    }
}
