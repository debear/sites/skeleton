/**
 * Methods to interact with the DOM
 */
class DOM { // eslint-disable-line no-unused-vars
    /*
     * Selector Getters
     */
    // Shorthand version of element.querySelector()
    static child(selector) {
        return document.querySelector(selector);
    }
    // Shorthand version of element.querySelectorAll()
    static children(selector) {
        return document.querySelectorAll(selector);
    }

    /*
     * Determine if an element (or selector to a single element) is being displayed or not
     */
    static isDisplayed(eleRef) {
        return ((Input.isString(eleRef) ? DOM.child(eleRef) : eleRef).offsetHeight > 0);
    }

    /*
     * Get the value of an attribute from its DOM, which could be named in multiple places
     */
    static getAttribute(ele, field, opt) {
        opt = opt ?? { };
        // As named
        if (DOM.hasAttribute(ele, field)) {
            return Input.isTrue(opt['has-no-value']) ? true : ele.getAttribute(field);
        }
        // As a data attribute
        if (DOM.hasAttribute(ele, `data-${field}`)) {
            return ele.getAttribute(`data-${field}`) !== 'false' ? ele.getAttribute(`data-${field}`) : false;
        }
        // Does not have either
        return opt.default ?? null;
    }

    /*
     * Update an attribute on a DOM element
     */
    static setAttribute(ele, field, value) {
        ele.setAttribute(`data-${field}`, value);
    }

    /*
     * Whether or not an element has a particular attribute set
     */
    static hasAttribute(ele, field) {
        return ele.hasAttribute(field);
    }

    /**
     * Trigger a CSS animation defined within a CSS class
     */
    static triggerTransition(ele, css) {
        ele.classList.remove(css);
        void ele.offsetWidth; // Trigger a transition restart
        ele.classList.add(css);
    }

    /*
     * Get the code of a key press
     */
    static getKeyPress(e) {
        return e.keyCode ?? e.which;
    }

    /*
     * Submit a "form" when pressing ENTER on a button
     */
    static submitOnEnter(e, fieldName) {
        if (DOM.getKeyPress(e) != 13) {
            return false;
        }
        // Enter key pressed
        DOM.child(`#${fieldName}`).onclick();
        return true;
    }

    // Perform a tabindex action on an element
    static performTabindex(ele, backwards) {
        backwards = backwards ?? false;
        var numEle = DOM.children('[data-tabindex]').length;
        var baseTabindex = parseInt(ele.dataset.tabindex); var proc = false;
        // One less than max because when we get back to numEle, we're going to focus ourself again!
        for (var i = 1; !proc && i < numEle; i++) {
            var test = (backwards ? baseTabindex - i : baseTabindex + i);
            // We need to be careful and loop at the boundaries
            if (backwards && test < 1) {
                test += numEle;
            } else if (!backwards && test > numEle) {
                test -= numEle;
            }
            var target = DOM.child(`[data-tabindex="${test}"]`);
            // If the element _isn't_ disabled and is visible, we can use it...
            if (!target.disabled && Input.isDefined(target.offsetParent)) {
                proc = true;
                // If we're dealing with a dropdown, perform some extra magic
                if (target.classList.contains('dropdown')) {
                    get('dropdowns')[target.dataset.id].focus();
                } else {
                    target.focus();
                }
            }
        }
    }

    // Common tasks to run on page load
    static onLoad(de) {
        // Start with page-specific additions
        get('dom:load').forEach((fn) => fn(de));

        // Viewport determination
        Events.attach(window, 'resize', DOM.setViewportTags);
        Events.attach(document, 'debear:viewport:mobile', DOM.setFooterHeight);
        DOM.setViewportTags();

        // Offline/Online Toggling
        Events.attach(window, 'offline', () => DOM.child('#device_offline').classList.remove('invisible'));
        Events.attach(window, 'online', () => DOM.child('#device_offline').classList.add('invisible'));

        // Tag parsing
        DOM.children('a[data-emnom]').forEach((emAnc) => {
            var emNom = DOM.getAttribute(emAnc, 'data-emnom');
            var emDom = DOM.getAttribute(emAnc, 'data-emdom');
            // Link interpolation?
            var eleNom = emAnc.querySelector('emnom');
            if (Input.isDefined(eleNom)) {
                emAnc.replaceChild(document.createTextNode(`${emNom}@${emDom}`), eleNom);
                emAnc.querySelector('emdom').remove();
            }
            // Appropriate event listener
            Events.attach(emAnc, 'click', () => { window.location.href = `mailto:${emNom}@${emDom}` });
        });
    }

    /*
     * Page handling
     */
    // Redirect the page to the given URL
    static redirectPage(url) {
        window.location.href = url;
    }

    /*
     * Add the appropriate viewport size tags to the <body element
     */
    static setViewportTags() {
        var body = DOM.child('body');
        // Remove all possible existing options
        body.classList.remove(
            'viewport-mobile',
            'viewport-tablet-portrait',
            'viewport-tablet-landscape',
            'viewport-tablet',
            'viewport-desktop'
        );
        var evtType = [];
        // Then determine the appropriate option(s)
        if (document.documentElement.clientWidth <= 480) {
            // Mobile
            evtType.push('mobile');
        } else if (document.documentElement.clientWidth <= 1001) {
            // Tablet... but which?
            evtType.push('tablet');
            if (document.documentElement.clientWidth <= 768) {
                // Portrait
                evtType.push('tablet-portrait');
            } else {
                // Landscape
                evtType.push('tablet-landscape');
            }
        } else {
            // Desktop
            evtType.push('desktop');
        }
        // Then
        evtType.forEach((size) => {
            body.classList.add(`viewport-${size}`);
            Events.fire('viewport:set');
            Events.fire(`viewport:${size}`);
        });
    }

    static setFooterHeight() {
        // Proceed only if has container (e.g., modal)
        if (Input.isDefined(DOM.child('container'))) {
            window.setTimeout(() => { DOM.child('container').style.paddingBottom = DOM.child('footer').clientHeight + 'px', 200 });
        }
    }
}

set('dom:load', []);
